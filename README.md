<p align="center">
<a>
<img src='https://gitee.com/zhi-yu-yan/zhi/raw/master/%E5%9B%BE%E5%83%8F/%E7%9F%A5%E5%BF%83%E7%BC%96%E8%AF%91%E5%99%A8.png' width="600px" align="center" alt='logo'/>
</a>
</p>

# TCC-Tiny C Compiler C语言编译器中文版 

![](https://img.shields.io/badge/名称-C语言编译器中文版-brightgreen) 
![](https://img.shields.io/badge/版本-1.0.1-brightgreen) 
![](https://img.shields.io/badge/C标准-C99-brightgreen) 
<a target="_blank" href="https://license.coscl.org.cn/MulanPSL2">
		<img src="https://img.shields.io/:许可证-MulanPSL2-blue.svg" />
</a>
![](https://img.shields.io/badge/作者-法布里斯·贝拉-brightgreen) 
![](https://img.shields.io/badge/翻译-位中原-brightgreen)
<br />
<a target="_blank" href='https://gitee.com/zhi-yu-yan/zhi/stargazers'>
<img src='https://gitee.com/zhi-yu-yan/zhi/badge/star.svg?theme=gvp' alt='star'/>
</a>
<p align="center">
<a href="http://qm.qq.com/cgi-bin/qm/qr?_wv=1027&k=RjQQnTUOonpHyE8lBtsSgbkZR4VcoAwT&authKey=W5m05M9zjC2ukOH5y8PvY54%2B84uBSiQtcShrArnIQldFu25zb69toHkpC7FDThnk&noverify=0&group_code=500558920">
<img src="https://img.shields.io/badge/QQ交流群-500558920-brightgreen"/></a>
</p>

___

#### 整体架构图
![输入图片说明](%E7%9F%A5%E5%BF%83%E7%BC%96%E8%AF%91%E5%99%A8%E6%9E%B6%E6%9E%84%E5%9B%BE.png)

#### 介绍
中文版TCC(Tiny C Compiler)是一款完整的可以商业使用的全中文编译器，代码全部开源，可以完美的实现汉语编程，同时也是学习编译原理，编译器开发，链接器开发等少有的完整的编译器的源代码参考对象。“main()”函数修改为“开始（）”几乎可以运行所有的C99源代码。可以编译Linux内核源码，可以编译其他C语言编译器源码，目前完全自举（自展）向前升级完善。特别声明在商业使用的时候一定要加上原作者-法布里斯·贝拉（FabriceBellard）的版权说明。本人血液里深爱中文，所以汉化了一款编译器供自己使用，同时分享出来供具有相同爱好的同胞使用。我是“小学生”，不喜勿喷，请大神嘴下留情！母语中文汉字就是源代码，源代码就是注释，亲切！大小驼峰，空格，下滑杠都是为了解决英文的缺陷而出现的，而中文就不需要这些。汉语编程，中文编程，华语编程加油！加油 :v: ！真希望有一天，咱们中国的孩子学习编程再也没有语言的障碍。到那个时候，编程就是个烂大街的活。咱们农民也可以编程，是真正名副其实的码农了 :smile: 。编译器源代码下载。
    同步项目知音编辑器源码：[https://gitee.com/zhi-yu-yan/zhiyin](https://gitee.com/zhi-yu-yan/zhiyin)
项目交流koukou群：500558920
中文编程官网：[www.880.xin](http://www.880.xin) 
#### 实例

1.  `#导入 <stdio.h>`
2.  `#导入 <标准库.h>`
3.  `整数 开始(整数 参数数量 ，字符 **参数数组)`
4.  `{`
5.  `   打印("您好！中文编程。"）;`
6.  `   返回 0;`
7.  `}`

#### 知语言和C语言关键词对照表：

知语言                C语言


1. 无                   void 
1. 短整数               short 
1. 整数                  int 
1. 长整数               long 
1. 浮点                  float 
1. 双精度               double 
1. 字符                 char 
1. 布尔                 _Bool 
1. 如果                  if 
1. 否则                  else 
1. 选择                  switch 
1. 分支                 case 
1. 执行                 do 
1. 判断                  while 
1. 循环                  for 
1. 继续                 continue 
1. 转到                  goto 
1. 跳出                  break 
1. 返回                  return 
1. 外部                  extern 
1. 静态                  static 
1. 无符号               unsigned 
1. 有符号               signed 
1. 取类型               typeof 
1. 取大小               sizeof 
1. 常量                  const 
1. 寄存器               register 
1. 内联                  inline 
1. 易变                  volatile 
1. 结构体                struct 
1. 共用体                union 
1. 枚举                  enum 
1. 类型定义             typedef 
1. 默认                  default 
1. 自动                  auto 
1. 限制                  restrict 
1. 断言                  assert 
1. 导入                  include 
1. 导入下一个          include_next 
1. 定义                  define 
1. 如果已定义           ifdef 
1. 如果未定义           ifndef 
1. 否则如果             elif 
1. 结束如果             endif 
1. 已定义                defined 
1. 结束定义             undef 
1. 错误                  error 
1. 警告                  warning 
1. 行号                  line 
1. 指示                  pragma

#### 安装教程

1.  “TCC编译器安装包.rar”下载下来之后解压到您想要存放的文件夹目录，然后设置系统环境变量，在“path”变量下添加"zhi.exe"所在文件夹路径即可.命令行输入命令“zhi”,即可验证是否 安装成功。
![输入图片说明](https://images.gitee.com/uploads/images/2021/0909/220114_d545fbb2_1882229.jpeg "00.JPG")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0909/220130_1e8848c2_1882229.jpeg "01.JPG")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0909/220144_f49fa3b8_1882229.jpeg "02.JPG")
2.  打开"win"文件夹（文件夹“备份”只是“win”目录的备份，可以忽略或删除），运行“bianyi.bat”批处理文件（需要先完成第一步“知心编译器安装包”的安装。本编译器整体汉化之后仅支持自举编译），即可编译出新的“TCC编译器安装包.rar”（整个“win”目录）。
![输入图片说明](https://images.gitee.com/uploads/images/2021/0909/221157_23932101_1882229.jpeg "00.JPG")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0909/221225_d0105b07_1882229.jpeg "02.JPG")
验证成果：使用编译出来的新的zhi.exe可执行文件来编译源码"win.z"(“.z”是知心编译的专用后缀扩展名称.当然也可以改成"win.c")得到一个视窗.
![输入图片说明](https://images.gitee.com/uploads/images/2021/0909/222000_cf202621_1882229.jpeg "03.JPG")
#### 使用说明

1.  命令行：zhi hello.c 或zhi hello -o hello.exe 即可编译获取hello.exe可以执行文件
2.  命令行：zhi -run hello.c或zhi -run hello.z 无需编译即可直接运行*.c或*.z脚本源文件。
3.  命令行：zhi -v 显示知心编译器版本信息
4.  命令行：zhi -h 获取知心编译器简明使用教程
5.  命令行：zhi -about 获取知心编译器相关信息
6.  命令行：bianyi.bat 在Windows平台使用此批处理文件，可以完成知心编译器自己编译自己（编译器自举）
7.  C语言的源码（C99标准）只需要修改“main(){}”为“开始(){}”即可完美运行编译。

#### Windows视窗范例使用教程

1.  下载“知心编译器安装包”，解压到想要安装的目录，配置好系统环境变量。
2.  下载“winAPI完整版”，解压覆盖到上一步的安装目录。
3.  下载“windows视窗范例”，解压随意的目录。
windows视窗范例解压后的目录：
![windows视窗范例解压后的目录](https://images.gitee.com/uploads/images/2021/0910/113113_7742ae01_1882229.jpeg "00.jpg")
运行批处理文件开始编译windows视窗：
![运行批处理文件开始编译windows视窗](https://images.gitee.com/uploads/images/2021/0910/113128_188774e5_1882229.jpeg "02.jpg")
windows视窗编译之后的效果：
![windows视窗编译之后的效果](https://images.gitee.com/uploads/images/2021/0910/113142_88ba651c_1882229.jpeg "01.jpg")
windows视窗升级之后编译效果图([https://gitee.com/zhi-yu-yan/zhiyin](https://gitee.com/zhi-yu-yan/zhiyin))：
![知音编辑器源码编译后展示图](00.JPG)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

#### 兄弟项目推荐

1.  TCC(Tiny CC):https://repo.or.cz/tinycc.git/shortlog/refs/heads/mob
2.  chibicc:     https://github.com/rui314/chibicc
3.  LCC:         https://github.com/drh/lcc
4.  9cc:         https://github.com/rui314/9cc



