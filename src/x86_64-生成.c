/****************************************************************************************************
 * 名称：知心编译器x86-64代码生成器
 * 版权(C) 2001-2022 Shinichiro Hamaji
 * 版权(C) 2022-2023 位中原
 * 网址：www.880.xin
 ****************************************************************************************************/
#ifdef TARGET_DEFS_ONLY

/* 可用寄存器的数量 */
#define NB_REGS         25
#define NB_ASM_REGS     16
#define CONFIG_ZHI_ASM

/* 一个寄存器可以属于几个类。类的排序必须从更一般到更精确（请参见生成值2（）对其进行假设的代码）。 */
#define RC_INT     0x0001 /* 通用整数寄存器 */
#define RC_FLOAT   0x0002 /* 通用浮点寄存器 */
#define RC_RAX     0x0004
#define RC_RCX     0x0008
#define RC_RDX     0x0010
#define RC_ST0     0x0080 /* 只适用于长双精度 */
#define RC_R8      0x0100
#define RC_R9      0x0200
#define RC_R10     0x0400
#define RC_R11     0x0800
#define RC_XMM0    0x1000
#define RC_XMM1    0x2000
#define RC_XMM2    0x4000
#define RC_XMM3    0x8000
#define RC_XMM4    0x10000
#define RC_XMM5    0x20000
#define RC_XMM6    0x40000
#define RC_XMM7    0x80000
#define RC_IRET    RC_RAX /* 函数返回: integer register */
#define RC_IRE2    RC_RDX /* 函数返回: second integer register */
#define RC_FRET    RC_XMM0 /* 函数返回: float register */
#define RC_FRE2    RC_XMM1 /* 函数返回: second float register */

/* pretty names for the registers */
enum {
    TREG_RAX = 0,
    TREG_RCX = 1,
    TREG_RDX = 2,
    TREG_RSP = 4,
    TREG_RSI = 6,
    TREG_RDI = 7,

    TREG_R8  = 8,
    TREG_R9  = 9,
    TREG_R10 = 10,
    TREG_R11 = 11,

    TREG_XMM0 = 16,
    TREG_XMM1 = 17,
    TREG_XMM2 = 18,
    TREG_XMM3 = 19,
    TREG_XMM4 = 20,
    TREG_XMM5 = 21,
    TREG_XMM6 = 22,
    TREG_XMM7 = 23,

    TREG_ST0 = 24,

    TREG_MEM = 0x20
};

#define REX_BASE(reg) (((reg) >> 3) & 1)
#define REG_VALUE(reg) ((reg) & 7)

/* return registers for function */
#define REG_IRET TREG_RAX /* single word int return register */
#define REG_IRE2 TREG_RDX /* second word return register (for long long) */
#define REG_FRET TREG_XMM0 /* float return register */
#define REG_FRE2 TREG_XMM1 /* second float return register */

/* 如果必须以相反的顺序评估函数参数，则定义 */
#define 反转函数参数

/* 指针大小，以字节为单位 */
#define 宏_指针大小 8

/* 长双精度 大小和对齐方式，以字节为单位 */
#define 长双精度大小  16
#define 长双精度对齐单位 16
/* 最大对齐（用于对齐属性支持） */
#define 最大对齐     16

/* 定义是否需要在调用方显式扩展返回值（用于与非 ZHI 编译器接口）*/
#define 显式扩展返回值
/******************************************************/
#else /* ! TARGET_DEFS_ONLY */
/******************************************************/
#define 全局使用
#include "zhi.h"
#include <assert.h>

静态数据 const char * const target_machine_defs =
    "__x86_64__\0"
    "__amd64__\0"
    ;

静态数据 const int reg_classes[NB_REGS] = {
    /* eax */ RC_INT | RC_RAX,
    /* ecx */ RC_INT | RC_RCX,
    /* edx */ RC_INT | RC_RDX,
    0,
    0,
    0,
    0,
    0,
    RC_R8,
    RC_R9,
    RC_R10,
    RC_R11,
    0,
    0,
    0,
    0,
    /* xmm0 */ RC_FLOAT | RC_XMM0,
    /* xmm1 */ RC_FLOAT | RC_XMM1,
    /* xmm2 */ RC_FLOAT | RC_XMM2,
    /* xmm3 */ RC_FLOAT | RC_XMM3,
    /* xmm4 */ RC_FLOAT | RC_XMM4,
    /* xmm5 */ RC_FLOAT | RC_XMM5,
    /* 包含 xmm6 和 xmm7，因此 生成值() 可以在它们上使用，但它们没有用 RC_FLOAT 标记，因为它们是保存在 Windows 上的被调用者 */
    RC_XMM6,
    RC_XMM7,
    /* st0 */ RC_ST0
};

static unsigned long func_sub_sp_offset;
static int func_ret_sub;

#if defined(启用边界检查M)
static addr_t func_bound_offset;
static unsigned long func_bound_ind;
静态数据 int 函数_边界_添加_结语;
#endif

#ifdef 目标_PE
static int func_scratch, func_alloca;
#endif

/* XXX: make it faster ? */
静态函数 void g(int c)
{
    int ind1;
    if (无需生成代码)
        return;
    ind1 = 指令在代码节位置 + 1;
    if (ind1 > 当前代码节->data_allocated)
        节_重新分配内存且内容为0(当前代码节, ind1);
    当前代码节->数据[指令在代码节位置] = c;
    指令在代码节位置 = ind1;
}

静态函数 void o(unsigned int c)
{
    while (c) {
        g(c);
        c = c >> 8;
    }
}

静态函数 void gen_le16(int v)
{
    g(v);
    g(v >> 8);
}

静态函数 void gen_le32(int c)
{
    g(c);
    g(c >> 8);
    g(c >> 16);
    g(c >> 24);
}

静态函数 void gen_le64(int64_t c)
{
    g(c);
    g(c >> 8);
    g(c >> 16);
    g(c >> 24);
    g(c >> 32);
    g(c >> 40);
    g(c >> 48);
    g(c >> 56);
}

static void orex(int ll, int r, int r2, int b)
{
    if ((r & 存储类型_掩码) >= 存储类型_VC常量)
        r = 0;
    if ((r2 & 存储类型_掩码) >= 存储类型_VC常量)
        r2 = 0;
    if (ll || REX_BASE(r) || REX_BASE(r2))
        o(0x40 | REX_BASE(r) | (REX_BASE(r2) << 2) | (ll << 3));
    o(b);
}

/* 输出一个符号并修补对它的所有调用
 * t:
 * a:代码索引
 * */
静态函数 void 生成符号_地址(int t, int a)
{
    while (t)
    {
        unsigned char *ptr = 当前代码节->数据 + t;
        uint32_t n = read32le(ptr); /* 下一个值 */
        write32le(ptr, a < 0 ? -a : a - t - 4);
        t = n;
    }
}

static int is64_type(int t)
{
    return ((t & 数据类型_基本类型) == 数据类型_指针 || (t & 数据类型_基本类型) == 数据类型_函数 || (t & 数据类型_基本类型) == 数据类型_长长整数);
}

/* 指令 + 4 字节数据。 返回数据的地址 */
static int oad(int c, int s)
{
    int t;
    if (无需生成代码)
        return s;
    o(c);
    t = 指令在代码节位置;
    gen_le32(s);
    return t;
}

/* 生成 jmp 到标签 */
#define 生成到标签的跳转2(instr,lbl) oad(instr,lbl)

静态函数 void gen_addr32(int r, 符号ST *sym, int c)
{
    if (r & 存储类型_符号)
    {
    	添加新的重定位条目(当前代码节, sym, 指令在代码节位置, R_X86_64_32S, c), c=0;
    }
    gen_le32(c);
}

/* 如果 'r & vt_sym' 为真，则输出带有重定位的常量 */
静态函数 void gen_addr64(int r, 符号ST *sym, int64_t c)
{
    if (r & 存储类型_符号)
        添加新的重定位条目(当前代码节, sym, 指令在代码节位置, R_X86_64_64, c), c=0;
    gen_le64(c);
}

/* output constant with relocation if 'r & 存储类型_符号' is true */
静态函数 void gen_addrpc32(int r, 符号ST *sym, int c)
{
    if (r & 存储类型_符号)
        添加新的重定位条目(当前代码节, sym, 指令在代码节位置, R_X86_64_PC32, c-4), c=4;
    gen_le32(c-4);
}

/* 输出获取了带有重新定位的地址 */
static void gen_gotpcrel(int r, 符号ST *sym, int c)
{
#ifdef 目标_PE
    错_误("内部错误: no GOT on PE: %s %x %x | %02x %02x %02x\n",
        取单词字符串(sym->v, NULL), c, r,
        当前代码节->数据[指令在代码节位置-3],
        当前代码节->数据[指令在代码节位置-2],
        当前代码节->数据[指令在代码节位置-1]
        );
#endif
    添加新的重定位条目(当前代码节, sym, 指令在代码节位置, R_X86_64_GOTPCREL, -4);
    gen_le32(0);
    if (c) {
        /* we use add c, %xxx for displacement */
        orex(1, r, 0, 0x81);
        o(0xc0 + REG_VALUE(r));
        gen_le32(c);
    }
}

static void gen_modrm_impl(int op_reg, int r, 符号ST *sym, int c, int is_got)
{
    op_reg = REG_VALUE(op_reg) << 3;
    if ((r & 存储类型_掩码) == 存储类型_VC常量) {
        /* constant memory reference */
	if (!(r & 存储类型_符号)) {
	    /* Absolute memory reference */
	    o(0x04 | op_reg); /* [sib] | destreg */
	    oad(0x25, c);     /* disp32 */
	} else {
	    o(0x05 | op_reg); /* (%rip)+disp32 | destreg */
	    if (is_got) {
		gen_gotpcrel(r, sym, c);
	    } else {
		gen_addrpc32(r, sym, c);
	    }
	}
    } else if ((r & 存储类型_掩码) == 存储类型_局部) {
        /* currently, we use only ebp as base */
        if (c == (char)c) {
            /* short reference */
            o(0x45 | op_reg);
            g(c);
        } else {
            oad(0x85 | op_reg, c);
        }
    } else if ((r & 存储类型_掩码) >= TREG_MEM) {
        if (c) {
            g(0x80 | op_reg | REG_VALUE(r));
            gen_le32(c);
        } else {
            g(0x00 | op_reg | REG_VALUE(r));
        }
    } else {
        g(0x00 | op_reg | REG_VALUE(r));
    }
}

/* generate a modrm reference. 'op_reg' contains the additional 3
   opcode bits */
static void gen_modrm(int op_reg, int r, 符号ST *sym, int c)
{
    gen_modrm_impl(op_reg, r, sym, c, 0);
}

/* generate a modrm reference. 'op_reg' contains the additional 3
   opcode bits */
static void 生成modrm64(int opcode, int op_reg, int r, 符号ST *sym, int c)
{
    int is_got;
    is_got = (op_reg & TREG_MEM) && !(sym->类型.数据类型 & 存储类型_静态);
    orex(1, r, op_reg, opcode);
    gen_modrm_impl(op_reg, r, sym, c, is_got);
}


/* load 'r' from value 'sv' */
void load(int r, 栈值ST *sv)
{
    int v, t, ft, fc, fr;
    栈值ST v1;

#ifdef 目标_PE
    栈值ST v2;
    sv = pe_getimport(sv, &v2);
#endif

    fr = sv->寄存qi;
    ft = sv->类型.数据类型 & ~存储类型_明确有无符号;
    fc = sv->c.i;
    if (fc != sv->c.i && (fr & 存储类型_符号))
      错_误("加载中的64位加数");

    ft &= ~(存储类型_易变 | 存储类型_常量);

#ifndef 目标_PE
    /* we use indirect access via got */
    if ((fr & 存储类型_掩码) == 存储类型_VC常量 && (fr & 存储类型_符号) &&
        (fr & 存储类型_左值) && !(sv->sym->类型.数据类型 & 存储类型_静态)) {
        /* use the result register as a temporal register */
        int tr = r | TREG_MEM;
        if (是_浮点(ft)) {
            /* we cannot use float registers as a temporal register */
            tr = get_reg(RC_INT) | TREG_MEM;
        }
        生成modrm64(0x8b, tr, fr, sv->sym, 0);

        /* load from the temporal register */
        fr = tr | 存储类型_左值;
    }
#endif

    v = fr & 存储类型_掩码;
    if (fr & 存储类型_左值) {
        int b, ll;
        if (v == 存储类型_LLOCAL) {
            v1.类型.数据类型 = 数据类型_指针;
            v1.寄存qi = 存储类型_局部 | 存储类型_左值;
            v1.c.i = fc;
            fr = r;
            if (!(reg_classes[fr] & (RC_INT|RC_R11)))
                fr = get_reg(RC_INT);
            load(fr, &v1);
        }
	if (fc != sv->c.i) {
	    /* If the addends doesn't fit into a 32bit signed
	       we must use a 64bit move.  We've checked above
	       that this doesn't have a sym associated.  */
	    v1.类型.数据类型 = 数据类型_长长整数;
	    v1.寄存qi = 存储类型_VC常量;
	    v1.c.i = sv->c.i;
	    fr = r;
	    if (!(reg_classes[fr] & (RC_INT|RC_R11)))
	        fr = get_reg(RC_INT);
	    load(fr, &v1);
	    fc = 0;
	}
        ll = 0;
	/* Like GCC we can load from small enough properly sized
	   structs and unions as well.
	   XXX maybe move to generic operand handling, but should
	   occur only with asm, so 汇编.c might also be a better place */
	if ((ft & 数据类型_基本类型) == 数据类型_结构体) {
	    int 对齐;
	    switch (类型_大小(&sv->类型, &对齐)) {
		case 1: ft = 数据类型_字节; break;
		case 2: ft = 数据类型_短整数; break;
		case 4: ft = 数据类型_整数; break;
		case 8: ft = 数据类型_长长整数; break;
		default:
		    错_误("寄存器加载的聚合类型无效");
		    break;
	    }
	}
        if ((ft & 数据类型_基本类型) == 数据类型_浮点) {
            b = 0x6e0f66;
            r = REG_VALUE(r); /* movd */
        } else if ((ft & 数据类型_基本类型) == 数据类型_双精度) {
            b = 0x7e0ff3; /* movq */
            r = REG_VALUE(r);
        } else if ((ft & 数据类型_基本类型) == 数据类型_长双精度) {
            b = 0xdb, r = 5; /* fldt */
        } else if ((ft & 值的数据类型_TYPE) == 数据类型_字节 || (ft & 值的数据类型_TYPE) == 数据类型_布尔) {
            b = 0xbe0f;   /* movsbl */
        } else if ((ft & 值的数据类型_TYPE) == (数据类型_字节 | 存储类型_无符号的)) {
            b = 0xb60f;   /* movzbl */
        } else if ((ft & 值的数据类型_TYPE) == 数据类型_短整数) {
            b = 0xbf0f;   /* movswl */
        } else if ((ft & 值的数据类型_TYPE) == (数据类型_短整数 | 存储类型_无符号的)) {
            b = 0xb70f;   /* movzwl */
        } else if ((ft & 值的数据类型_TYPE) == (数据类型_无类型)) {
            /* Can happen with zero 大小 structs */
            return;
        } else {
            assert(((ft & 数据类型_基本类型) == 数据类型_整数)
                   || ((ft & 数据类型_基本类型) == 数据类型_长长整数)
                   || ((ft & 数据类型_基本类型) == 数据类型_指针)
                   || ((ft & 数据类型_基本类型) == 数据类型_函数)
                );
            ll = is64_type(ft);
            b = 0x8b;
        }
        if (ll) {
            生成modrm64(b, r, fr, sv->sym, fc);
        } else {
            orex(ll, fr, r, b);
            gen_modrm(r, fr, sv->sym, fc);
        }
    } else {
        if (v == 存储类型_VC常量) {
            if (fr & 存储类型_符号) {
#ifdef 目标_PE
                orex(1,0,r,0x8d);
                o(0x05 + REG_VALUE(r) * 8); /* lea xx(%rip), r */
                gen_addrpc32(fr, sv->sym, fc);
#else
                if (sv->sym->类型.数据类型 & 存储类型_静态) {
                    orex(1,0,r,0x8d);
                    o(0x05 + REG_VALUE(r) * 8); /* lea xx(%rip), r */
                    gen_addrpc32(fr, sv->sym, fc);
                } else {
                    orex(1,0,r,0x8b);
                    o(0x05 + REG_VALUE(r) * 8); /* mov xx(%rip), r */
                    gen_gotpcrel(r, sv->sym, fc);
                }
#endif
            } else if (is64_type(ft)) {
                orex(1,r,0, 0xb8 + REG_VALUE(r)); /* mov $xx, r */
                gen_le64(sv->c.i);
            } else {
                orex(0,r,0, 0xb8 + REG_VALUE(r)); /* mov $xx, r */
                gen_le32(fc);
            }
        } else if (v == 存储类型_局部) {
            orex(1,0,r,0x8d); /* lea xxx(%ebp), r */
            gen_modrm(r, 存储类型_局部, sv->sym, fc);
        } else if (v == 存储类型_标志寄存器) {
	    if (fc & 0x100)
	      {
                v = 栈顶值->cmp_r;
                fc &= ~0x100;
	        /* This was a float compare.  If the parity bit is
		   set the result was unordered, meaning false for everything
		   except TOK_不等于, and true for TOK_不等于.  */
                orex(0, r, 0, 0xb0 + REG_VALUE(r)); /* mov $0/1,%al */
                g(v ^ fc ^ (v == TOK_不等于));
                o(0x037a + (REX_BASE(r) << 8));
              }
            orex(0,r,0, 0x0f); /* setxx %br */
            o(fc);
            o(0xc0 + REG_VALUE(r));
            orex(0,r,0, 0x0f);
            o(0xc0b6 + REG_VALUE(r) * 0x900); /* movzbl %al, %eax */
        } else if (v == 存储类型_JMP || v == 存储类型_JMPI) {
            t = v & 1;
            orex(0,r,0,0);
            oad(0xb8 + REG_VALUE(r), t); /* mov $1, r */
            o(0x05eb + (REX_BASE(r) << 8)); /* jmp after */
            生成符号(fc);
            orex(0,r,0,0);
            oad(0xb8 + REG_VALUE(r), t ^ 1); /* mov $0, r */
        } else if (v != r) {
            if ((r >= TREG_XMM0) && (r <= TREG_XMM7)) {
                if (v == TREG_ST0) {
                    /* gen_cvt_ftof(数据类型_双精度); */
                    o(0xf0245cdd); /* fstpl -0x10(%rsp) */
                    /* movsd -0x10(%rsp),%xmmN */
                    o(0x100ff2);
                    o(0x44 + REG_VALUE(r)*8); /* %xmmN */
                    o(0xf024);
                } else {
                    assert((v >= TREG_XMM0) && (v <= TREG_XMM7));
                    if ((ft & 数据类型_基本类型) == 数据类型_浮点) {
                        o(0x100ff3);
                    } else {
                        assert((ft & 数据类型_基本类型) == 数据类型_双精度);
                        o(0x100ff2);
                    }
                    o(0xc0 + REG_VALUE(v) + REG_VALUE(r)*8);
                }
            } else if (r == TREG_ST0) {
                assert((v >= TREG_XMM0) && (v <= TREG_XMM7));
                /* gen_cvt_ftof(数据类型_长双精度); */
                /* movsd %xmmN,-0x10(%rsp) */
                o(0x110ff2);
                o(0x44 + REG_VALUE(r)*8); /* %xmmN */
                o(0xf024);
                o(0xf02444dd); /* fldl -0x10(%rsp) */
            } else {
                orex(is64_type(ft), r, v, 0x89);
                o(0xc0 + REG_VALUE(r) + REG_VALUE(v) * 8); /* mov v, r */
            }
        }
    }
}

/* store register 'r' in lvalue 'v' */
void store(int r, 栈值ST *v)
{
    int fr, bt, ft, fc;
    int op64 = 0;
    /* store the REX prefix in this variable when PIC is enabled */
    int pic = 0;

#ifdef 目标_PE
    栈值ST v2;
    v = pe_getimport(v, &v2);
#endif

    fr = v->寄存qi & 存储类型_掩码;
    ft = v->类型.数据类型;
    fc = v->c.i;
    if (fc != v->c.i && (fr & 存储类型_符号))
      错_误("存储中的64位加数");
    ft &= ~(存储类型_易变 | 存储类型_常量);
    bt = ft & 数据类型_基本类型;

#ifndef 目标_PE
    /* we need to access the variable via got */
    if (fr == 存储类型_VC常量
        && (v->寄存qi & 存储类型_符号)
        && !(v->sym->类型.数据类型 & 存储类型_静态)) {
        /* mov xx(%rip), %r11 */
        o(0x1d8b4c);
        gen_gotpcrel(TREG_R11, v->sym, v->c.i);
        pic = is64_type(bt) ? 0x49 : 0x41;
    }
#endif

    /* XXX: incorrect if float reg to reg */
    if (bt == 数据类型_浮点) {
        o(0x66);
        o(pic);
        o(0x7e0f); /* movd */
        r = REG_VALUE(r);
    } else if (bt == 数据类型_双精度) {
        o(0x66);
        o(pic);
        o(0xd60f); /* movq */
        r = REG_VALUE(r);
    } else if (bt == 数据类型_长双精度) {
        o(0xc0d9); /* fld %st(0) */
        o(pic);
        o(0xdb); /* fstpt */
        r = 7;
    } else {
        if (bt == 数据类型_短整数)
            o(0x66);
        o(pic);
        if (bt == 数据类型_字节 || bt == 数据类型_布尔)
            orex(0, 0, r, 0x88);
        else if (is64_type(bt))
            op64 = 0x89;
        else
            orex(0, 0, r, 0x89);
    }
    if (pic) {
        /* xxx r, (%r11) where xxx is mov, movq, fld, or etc */
        if (op64)
            o(op64);
        o(3 + (r << 3));
    } else if (op64) {
        if (fr == 存储类型_VC常量 || fr == 存储类型_局部 || (v->寄存qi & 存储类型_左值)) {
            生成modrm64(op64, r, v->寄存qi, v->sym, fc);
        } else if (fr != r) {
            orex(1, fr, r, op64);
            o(0xc0 + fr + r * 8); /* mov r, fr */
        }
    } else {
        if (fr == 存储类型_VC常量 || fr == 存储类型_局部 || (v->寄存qi & 存储类型_左值)) {
            gen_modrm(r, v->寄存qi, v->sym, fc);
        } else if (fr != r) {
            o(0xc0 + fr + r * 8); /* mov r, fr */
        }
    }
}

/* 'is_jmp' is '1' if it is a jump */
static void gcall_or_jmp(int is_jmp)
{
    int r;
    if ((栈顶值->寄存qi & (存储类型_掩码 | 存储类型_左值)) == 存储类型_VC常量 &&
	((栈顶值->寄存qi & 存储类型_符号) && (栈顶值->c.i-4) == (int)(栈顶值->c.i-4))) {
        /* constant 当前模块的符号 case -> simple relocation */
#ifdef 目标_PE
        添加新的重定位条目(当前代码节, 栈顶值->sym, 指令在代码节位置 + 1, R_X86_64_PC32, (int)(栈顶值->c.i-4));
#else
        添加新的重定位条目(当前代码节, 栈顶值->sym, 指令在代码节位置 + 1, R_X86_64_PLT32, (int)(栈顶值->c.i-4));
#endif
        oad(0xe8 + is_jmp, 0); /* call/jmp im */
    } else {
        /* otherwise, indirect call */
        r = TREG_R11;
        load(r, 栈顶值);
        o(0x41); /* REX */
        o(0xff); /* call/jmp *r */
        o(0xd0 + REG_VALUE(r) + (is_jmp << 4));
    }
}

#if defined(启用边界检查M)

static void gen_bounds_call(int v)
{
    符号ST *sym = 外部_辅助_符号(v);
    oad(0xe8, 0);
#ifdef 目标_PE
    添加新的重定位条目(当前代码节, sym, 指令在代码节位置-4, R_X86_64_PC32, -4);
#else
    添加新的重定位条目(当前代码节, sym, 指令在代码节位置-4, R_X86_64_PLT32, -4);
#endif
}

#ifdef 目标_PE
# define TREG_FASTCALL_1 TREG_RCX
#else
# define TREG_FASTCALL_1 TREG_RDI
#endif

static void 生成函数绑定序言(void)
{
    /* leave some room for bound checking code */
    func_bound_offset = 局部边界_节->当前数据_偏移量;
    func_bound_ind = 指令在代码节位置;
    函数_边界_添加_结语 = 0;
    o(0x0d8d48 + ((TREG_FASTCALL_1 == TREG_RDI) * 0x300000)); /*lbound section pointer */
    gen_le32 (0);
    oad(0xb8, 0); /* call to function */
}

static void gen_bounds_epilog(void)
{
    addr_t saved_ind;
    addr_t *bounds_ptr;
    符号ST *sym_data;
    int offset_modified = func_bound_offset != 局部边界_节->当前数据_偏移量;

    if (!offset_modified && !函数_边界_添加_结语)
        return;

    /* add end of table info */
    bounds_ptr = 节_预留指定大小内存(局部边界_节, sizeof(addr_t));
    *bounds_ptr = 0;

    sym_data = 取指向某个节的静态符号(&字符_指针_类型, 局部边界_节, 
                           func_bound_offset, 局部边界_节->当前数据_偏移量);

    /* generate bound local allocation */
    if (offset_modified) {
        saved_ind = 指令在代码节位置;
        指令在代码节位置 = func_bound_ind;
        添加新的重定位条目(当前代码节, sym_data, 指令在代码节位置 + 3, R_X86_64_PC32, -4);
        指令在代码节位置 = 指令在代码节位置 + 7;
        gen_bounds_call(TOK___bound_local_new);
        指令在代码节位置 = saved_ind;
    }

    /* generate bound check local freeing */
    o(0x5250); /* save returned value, if any */
    添加新的重定位条目(当前代码节, sym_data, 指令在代码节位置 + 3, R_X86_64_PC32, -4);
    o(0x0d8d48 + ((TREG_FASTCALL_1 == TREG_RDI) * 0x300000)); /* lea xxx(%rip), %rcx/rdi */
    gen_le32 (0);
    gen_bounds_call(TOK___bound_local_delete);
    o(0x585a); /* restore returned value, if any */
}
#endif

#ifdef 目标_PE

#define REGN 4
static const uint8_t arg_regs[REGN] = {
    TREG_RCX, TREG_RDX, TREG_R8, TREG_R9
};

/* Prepare arguments in R10 and R11 rather than RCX and RDX
   because 生成值() will not ever use these */
static int arg_prepare_reg(int idx) {
  if (idx == 0 || idx == 1)
      /* idx=0: r10, idx=1: r11 */
      return idx + 10;
  else
      return idx >= 0 && idx < REGN ? arg_regs[idx] : 0;
}

/* Generate function call. The function address is pushed first, then
   all the parameters in call order. This functions pops all the
   parameters and the function address. */

static void gen_offs_sp(int b, int r, int d)
{
    orex(1,0,r & 0x100 ? 0 : r, b);
    if (d == (char)d) {
        o(0x2444 | (REG_VALUE(r) << 3));
        g(d);
    } else {
        o(0x2484 | (REG_VALUE(r) << 3));
        gen_le32(d);
    }
}

static int using_regs(int 大小)
{
    return !(大小 > 8 || (大小 & (大小 - 1)));
}

/* Return the number of registers needed to return the struct, or 0 if
   returning via struct pointer. */
静态函数 int gfunc_sret(类型ST *vt, int variadic, 类型ST *ret, int *ret_align, int *regsize)
{
    int 大小, 对齐;
    *ret_align = 1; // Never have to re-对齐 return values for x86-64
    *regsize = 8;
    大小 = 类型_大小(vt, &对齐);
    if (!using_regs(大小))
        return 0;
    if (大小 == 8)
        ret->数据类型 = 数据类型_长长整数;
    else if (大小 == 4)
        ret->数据类型 = 数据类型_整数;
    else if (大小 == 2)
        ret->数据类型 = 数据类型_短整数;
    else
        ret->数据类型 = 数据类型_字节;
    ret->引用符号 = NULL;
    return 1;
}

static int is_sse_float(int t) {
    int bt;
    bt = t & 数据类型_基本类型;
    return bt == 数据类型_双精度 || bt == 数据类型_浮点;
}

static int 生成函数_arg_大小(类型ST *类型) {
    int 对齐;
    if (类型->数据类型 & (数据类型_数组|存储类型_位域))
        return 8;
    return 类型_大小(类型, &对齐);
}

void g函数_调用(int nb_args)
{
    int 大小, r, args_size, i, d, bt, struct_size;
    int arg;

#ifdef 启用边界检查M
    if (全局虚拟机->使用_边界_检查器)
        生成函数参数_边界(nb_args);
#endif

    args_size = (nb_args < REGN ? REGN : nb_args) * 宏_指针大小;
    arg = nb_args;

    /* for struct arguments, we need to call memcpy and the function
       call breaks register passing arguments we are preparing.
       So, we process arguments which will be passed by stack first. */
    struct_size = args_size;
    for(i = 0; i < nb_args; i++) {
        栈值ST *sv;
        
        --arg;
        sv = &栈顶值[-i];
        bt = (sv->类型.数据类型 & 数据类型_基本类型);
        大小 = 生成函数_arg_大小(&sv->类型);

        if (using_regs(大小))
            continue; /* arguments smaller than 8 bytes passed in registers or on stack */

        if (bt == 数据类型_结构体) {
            /* 对齐 to stack 对齐 大小 */
            大小 = (大小 + 15) & ~15;
            /* generate structure store */
            r = get_reg(RC_INT);
            gen_offs_sp(0x8d, r, struct_size);
            struct_size += 大小;

            /* generate memcpy call */
            值压入栈(&sv->类型, r | 存储类型_左值, 0);
            值压入栈值(sv);
            存储栈顶值();
            --栈顶值;
        } else if (bt == 数据类型_长双精度) {
            生成值(RC_ST0);
            gen_offs_sp(0xdb, 0x107, struct_size);
            struct_size += 16;
        }
    }

    if (func_scratch < struct_size)
        func_scratch = struct_size;

    arg = nb_args;
    struct_size = args_size;

    for(i = 0; i < nb_args; i++) {
        --arg;
        bt = (栈顶值->类型.数据类型 & 数据类型_基本类型);

        大小 = 生成函数_arg_大小(&栈顶值->类型);
        if (!using_regs(大小)) {
            /* 对齐 to stack 对齐 大小 */
            大小 = (大小 + 15) & ~15;
            if (arg >= REGN) {
                d = get_reg(RC_INT);
                gen_offs_sp(0x8d, d, struct_size);
                gen_offs_sp(0x89, d, arg*8);
            } else {
                d = arg_prepare_reg(arg);
                gen_offs_sp(0x8d, d, struct_size);
            }
            struct_size += 大小;
        } else {
            if (is_sse_float(栈顶值->类型.数据类型)) {
		if (全局虚拟机->nosse)
		  错_误("SSE已禁用");
                if (arg >= REGN) {
                    生成值(RC_XMM0);
                    /* movq %xmm0, j*8(%rsp) */
                    gen_offs_sp(0xd60f66, 0x100, arg*8);
                } else {
                    /* Load directly to xmmN register */
                    生成值(RC_XMM0 << arg);
                    d = arg_prepare_reg(arg);
                    /* mov %xmmN, %rxx */
                    o(0x66);
                    orex(1,d,0, 0x7e0f);
                    o(0xc0 + arg*8 + REG_VALUE(d));
                }
            } else {
                if (bt == 数据类型_结构体) {
                    栈顶值->类型.引用符号 = NULL;
                    栈顶值->类型.数据类型 = 大小 > 4 ? 数据类型_长长整数 : 大小 > 2 ? 数据类型_整数
                        : 大小 > 1 ? 数据类型_短整数 : 数据类型_字节;
                }
                
                r = 生成值(RC_INT);
                if (arg >= REGN) {
                    gen_offs_sp(0x89, r, arg*8);
                } else {
                    d = arg_prepare_reg(arg);
                    orex(1,d,r,0x89); /* mov */
                    o(0xc0 + REG_VALUE(r) * 8 + REG_VALUE(d));
                }
            }
        }
        栈顶值--;
    }
    save_regs(0);
    /* Copy R10 and R11 into RCX and RDX, respectively */
    if (nb_args > 0) {
        o(0xd1894c); /* mov %r10, %rcx */
        if (nb_args > 1) {
            o(0xda894c); /* mov %r11, %rdx */
        }
    }
    
    gcall_or_jmp(0);

    if ((栈顶值->寄存qi & 存储类型_符号) && 栈顶值->sym->v == TOK_alloca) {
        /* need to add the "func_scratch" area after alloca */
        o(0x48); func_alloca = oad(0x05, func_alloca); /* add $NN, %rax */
#ifdef 启用边界检查M
        if (全局虚拟机->使用_边界_检查器)
            gen_bounds_call(TOK___bound_alloca_nr); /* new region */
#endif
    }
    栈顶值--;
}


#define 函数序言大小M 11

/* 生成“t”类型的函数序言 */
void 生成函数开头代码(符号ST *函数符号)
{
    类型ST *函数_类型= &函数符号->类型;
    int addr, reg_param_index, bt, 大小;
    符号ST *函数类型引用符号;
    类型ST *类型;

    func_ret_sub = 0;
    func_scratch = 32;
    func_alloca = 0;
    局部变量索引 = 0;

    addr = 宏_指针大小 * 2;
    指令在代码节位置 += 函数序言大小M;
    func_sub_sp_offset = 指令在代码节位置;
    reg_param_index = 0;

    函数类型引用符号 = 函数_类型->引用符号;

    /* 如果函数返回结构体，则添加隐式指针参数 */
    大小 = 生成函数_arg_大小(&函数_返回类型);
    if (!using_regs(大小))
    {
        生成modrm64(0x89, arg_regs[reg_param_index], 存储类型_局部, NULL, addr);
        func_vc = addr;
        reg_param_index++;
        addr += 8;
    }

    /* 定义参数 */
    while ((函数类型引用符号 = 函数类型引用符号->next) != NULL)
    {
        类型 = &函数类型引用符号->类型;
        bt = 类型->数据类型 & 数据类型_基本类型;
        大小 = 生成函数_arg_大小(类型);
        if (!using_regs(大小))
        {
            if (reg_param_index < REGN)
            {
                生成modrm64(0x89, arg_regs[reg_param_index], 存储类型_局部, NULL, addr);
            }
            将符号放入符号栈(函数类型引用符号->v & ~符号_字段, 类型, 存储类型_LLOCAL | 存储类型_左值, addr);
        } else
        {
            if (reg_param_index < REGN)
            {
                /* save arguments passed by register */
                if ((bt == 数据类型_浮点) || (bt == 数据类型_双精度))
                {
					if (全局虚拟机->nosse)
					{
						错_误("SSE已禁用");
					}
                    o(0xd60f66); /* movq */
                    gen_modrm(reg_param_index, 存储类型_局部, NULL, addr);
                } else
                {
                    生成modrm64(0x89, arg_regs[reg_param_index], 存储类型_局部, NULL, addr);
                }
            }
            将符号放入符号栈(函数类型引用符号->v & ~符号_字段, 类型, 存储类型_局部 | 存储类型_左值, addr);
        }
        addr += 8;
        reg_param_index++;
    }

    while (reg_param_index < REGN)
    {
        if (函数_可变参)
        {
            生成modrm64(0x89, arg_regs[reg_param_index], 存储类型_局部, NULL, addr);
            addr += 8;
        }
        reg_param_index++;
    }
#ifdef 启用边界检查M
    if (全局虚拟机->使用_边界_检查器)
    {
    	生成函数绑定序言();
    }
#endif
}

/* 生成函数结语 */
void 生成函数结尾代码(void)
{
    int v, saved_ind;

    /* 对齐 local 大小 to word & save local variables */
    func_scratch = (func_scratch + 15) & -16;
    局部变量索引 = (局部变量索引 & -16) - func_scratch;

#ifdef 启用边界检查M
    if (全局虚拟机->使用_边界_检查器)
        gen_bounds_epilog();
#endif

    o(0xc9); /* leave */
    if (func_ret_sub == 0) {
        o(0xc3); /* ret */
    } else {
        o(0xc2); /* ret n */
        g(func_ret_sub);
        g(func_ret_sub >> 8);
    }

    saved_ind = 指令在代码节位置;
    指令在代码节位置 = func_sub_sp_offset - 函数序言大小M;
    v = -局部变量索引;

    if (v >= 4096) {
        符号ST *sym = 外部_辅助_符号(TOK___chkstk);
        oad(0xb8, v); /* mov stacksize, %eax */
        oad(0xe8, 0); /* call __chkstk, (does the stackframe too) */
        添加新的重定位条目(当前代码节, sym, 指令在代码节位置-4, R_X86_64_PC32, -4);
        o(0x90); /* fill for 函数序言大小M = 11 bytes */
    } else {
        o(0xe5894855);  /* push %rbp, mov %rsp, %rbp */
        o(0xec8148);  /* sub rsp, stacksize */
        gen_le32(v);
    }

    /* add the "func_scratch" area after each alloca seen */
    生成符号_地址(func_alloca, -func_scratch);

    当前代码节->当前数据_偏移量 = saved_ind;
    pe_添加_展开_数据(指令在代码节位置, saved_ind, v);
    指令在代码节位置 = 当前代码节->当前数据_偏移量;
}

#else

static void gadd_sp(int val)
{
    if (val == (char)val) {
        o(0xc48348);
        g(val);
    } else {
        oad(0xc48148, val); /* add $xxx, %rsp */
    }
}

typedef enum X86_64_Mode {
  x86_64_mode_none,
  x86_64_mode_memory,
  x86_64_mode_integer,
  x86_64_mode_sse,
  x86_64_mode_x87
} X86_64_Mode;

static X86_64_Mode classify_x86_64_merge(X86_64_Mode a, X86_64_Mode b)
{
    if (a == b)
        return a;
    else if (a == x86_64_mode_none)
        return b;
    else if (b == x86_64_mode_none)
        return a;
    else if ((a == x86_64_mode_memory) || (b == x86_64_mode_memory))
        return x86_64_mode_memory;
    else if ((a == x86_64_mode_integer) || (b == x86_64_mode_integer))
        return x86_64_mode_integer;
    else if ((a == x86_64_mode_x87) || (b == x86_64_mode_x87))
        return x86_64_mode_memory;
    else
        return x86_64_mode_sse;
}

static X86_64_Mode classify_x86_64_inner(类型ST *ty)
{
    X86_64_Mode mode;
    符号ST *f;
    
    switch (ty->数据类型 & 数据类型_基本类型) {
    case 数据类型_无类型: return x86_64_mode_none;
    
    case 数据类型_整数:
    case 数据类型_字节:
    case 数据类型_短整数:
    case 数据类型_长长整数:
    case 数据类型_布尔:
    case 数据类型_指针:
    case 数据类型_函数:
        return x86_64_mode_integer;
    
    case 数据类型_浮点:
    case 数据类型_双精度: return x86_64_mode_sse;
    
    case 数据类型_长双精度: return x86_64_mode_x87;
      
    case 数据类型_结构体:
        f = ty->引用符号;

        mode = x86_64_mode_none;
        for (f = f->next; f; f = f->next)
            mode = classify_x86_64_merge(mode, classify_x86_64_inner(&f->类型));
        
        return mode;
    }
    assert(0);
    return 0;
}

static X86_64_Mode classify_x86_64_arg(类型ST *ty, 类型ST *ret, int *psize, int *palign, int *reg_count)
{
    X86_64_Mode mode;
    int 大小, 对齐, ret_t = 0;
    
    if (ty->数据类型 & (存储类型_位域|数据类型_数组)) {
        *psize = 8;
        *palign = 8;
        *reg_count = 1;
        ret_t = ty->数据类型;
        mode = x86_64_mode_integer;
    } else {
        大小 = 类型_大小(ty, &对齐);
        *psize = (大小 + 7) & ~7;
        *palign = (对齐 + 7) & ~7;
    
        if (大小 > 16) {
            mode = x86_64_mode_memory;
        } else {
            mode = classify_x86_64_inner(ty);
            switch (mode) {
            case x86_64_mode_integer:
                if (大小 > 8) {
                    *reg_count = 2;
                    ret_t = 数据类型_128位整数;
                } else {
                    *reg_count = 1;
                    if (大小 > 4)
                        ret_t = 数据类型_长长整数;
                    else if (大小 > 2)
                        ret_t = 数据类型_整数;
                    else if (大小 > 1)
                        ret_t = 数据类型_短整数;
                    else
                        ret_t = 数据类型_字节;
                    if ((ty->数据类型 & 数据类型_基本类型) == 数据类型_结构体 || (ty->数据类型 & 存储类型_无符号的))
                        ret_t |= 存储类型_无符号的;
                }
                break;
                
            case x86_64_mode_x87:
                *reg_count = 1;
                ret_t = 数据类型_长双精度;
                break;

            case x86_64_mode_sse:
                if (大小 > 8) {
                    *reg_count = 2;
                    ret_t = 数据类型_128位浮点;
                } else {
                    *reg_count = 1;
                    ret_t = (大小 > 4) ? 数据类型_双精度 : 数据类型_浮点;
                }
                break;
            default: break; /* nothing to be 完成 for x86_64_mode_memory and x86_64_mode_none*/
            }
        }
    }
    
    if (ret) {
        ret->引用符号 = NULL;
        ret->数据类型 = ret_t;
    }
    
    return mode;
}

静态函数 int classify_x86_64_va_arg(类型ST *ty)
{
    /* This definition must be synced with stdarg.h */
    enum __va_arg_type {
        __va_gen_reg, __va_float_reg, __va_stack
    };
    int 大小, 对齐, reg_count;
    X86_64_Mode mode = classify_x86_64_arg(ty, NULL, &大小, &对齐, &reg_count);
    switch (mode) {
    default: return __va_stack;
    case x86_64_mode_integer: return __va_gen_reg;
    case x86_64_mode_sse: return __va_float_reg;
    }
}

/* Return the number of registers needed to return the struct, or 0 if
   returning via struct pointer. */
静态函数 int gfunc_sret(类型ST *vt, int variadic, 类型ST *ret, int *ret_align, int *regsize)
{
    int 大小, 对齐, reg_count;
    *ret_align = 1; // Never have to re-对齐 return values for x86-64
    *regsize = 8;
    return (classify_x86_64_arg(vt, ret, &大小, &对齐, &reg_count) != x86_64_mode_memory);
}

#define REGN 6
static const uint8_t arg_regs[REGN] = {
    TREG_RDI, TREG_RSI, TREG_RDX, TREG_RCX, TREG_R8, TREG_R9
};

static int arg_prepare_reg(int idx) {
  if (idx == 2 || idx == 3)
      /* idx=2: r10, idx=3: r11 */
      return idx + 8;
  else
      return idx >= 0 && idx < REGN ? arg_regs[idx] : 0;
}

/* Generate function call. The function address is pushed first, then
   all the parameters in call order. This functions pops all the
   parameters and the function address. */
void g函数_调用(int nb_args)
{
    X86_64_Mode mode;
    类型ST 类型;
    int 大小, 对齐, r, args_size, stack_adjust, i, reg_count, k;
    int nb_reg_args = 0;
    int nb_sse_args = 0;
    int sse_reg, gen_reg;
    char *onstack = zhi_分配内存((nb_args + 1) * sizeof (char));

#ifdef 启用边界检查M
    if (全局虚拟机->使用_边界_检查器)
        生成函数参数_边界(nb_args);
#endif

    /* calculate the number of integer/float register arguments, remember
       arguments to be passed via stack (in onstack[]), and also remember
       if we have to 对齐 the stack pointer to 16 (onstack[i] == 2).  Needs
       to be 完成 in a left-to-right pass over arguments.  */
    stack_adjust = 0;
    for(i = nb_args - 1; i >= 0; i--) {
        mode = classify_x86_64_arg(&栈顶值[-i].类型, NULL, &大小, &对齐, &reg_count);
        if (大小 == 0) continue;
        if (mode == x86_64_mode_sse && nb_sse_args + reg_count <= 8) {
            nb_sse_args += reg_count;
	    onstack[i] = 0;
	} else if (mode == x86_64_mode_integer && nb_reg_args + reg_count <= REGN) {
            nb_reg_args += reg_count;
	    onstack[i] = 0;
	} else if (mode == x86_64_mode_none) {
	    onstack[i] = 0;
	} else {
	    if (对齐 == 16 && (stack_adjust &= 15)) {
		onstack[i] = 2;
		stack_adjust = 0;
	    } else
	      onstack[i] = 1;
	    stack_adjust += 大小;
	}
    }

    if (nb_sse_args && 全局虚拟机->nosse)
      错_误("SSE已禁用，但已传递浮点参数");

    /* fetch cpu flag before generating any code */
    if ((栈顶值->寄存qi & 存储类型_掩码) == 存储类型_标志寄存器)
      生成值(RC_INT);

    /* for struct arguments, we need to call memcpy and the function
       call breaks register passing arguments we are preparing.
       So, we process arguments which will be passed by stack first. */
    gen_reg = nb_reg_args;
    sse_reg = nb_sse_args;
    args_size = 0;
    stack_adjust &= 15;
    for (i = k = 0; i < nb_args;) {
	mode = classify_x86_64_arg(&栈顶值[-i].类型, NULL, &大小, &对齐, &reg_count);
	if (大小) {
            if (!onstack[i + k]) {
	        ++i;
	        continue;
	    }
            /* Possibly adjust stack to 对齐 SSE boundary.  We're processing
	       args from right to left while allocating happens left to right
	       (stack grows down), so the adjustment needs to happen _after_
	       an argument that requires it.  */
            if (stack_adjust) {
	        o(0x50); /* push %rax; aka sub $8,%rsp */
                args_size += 8;
	        stack_adjust = 0;
            }
	    if (onstack[i + k] == 2)
	        stack_adjust = 1;
        }

	vrotb(i+1);

	switch (栈顶值->类型.数据类型 & 数据类型_基本类型) {
	    case 数据类型_结构体:
		/* allocate the necessary 大小 on stack */
		o(0x48);
		oad(0xec81, 大小); /* sub $xxx, %rsp */
		/* generate structure store */
		r = get_reg(RC_INT);
		orex(1, r, 0, 0x89); /* mov %rsp, r */
		o(0xe0 + REG_VALUE(r));
		值压入栈(&栈顶值->类型, r | 存储类型_左值, 0);
		vswap();
		存储栈顶值();
		break;

	    case 数据类型_长双精度:
                生成值(RC_ST0);
                oad(0xec8148, 大小); /* sub $xxx, %rsp */
                o(0x7cdb); /* fstpt 0(%rsp) */
                g(0x24);
                g(0x00);
		break;

	    case 数据类型_浮点:
	    case 数据类型_双精度:
		assert(mode == x86_64_mode_sse);
		r = 生成值(RC_FLOAT);
		o(0x50); /* push $rax */
		/* movq %xmmN, (%rsp) */
		o(0xd60f66);
		o(0x04 + REG_VALUE(r)*8);
		o(0x24);
		break;

	    default:
		assert(mode == x86_64_mode_integer);
		/* simple 类型 */
		/* XXX: implicit cast ? */
		r = 生成值(RC_INT);
		orex(0,r,0,0x50 + REG_VALUE(r)); /* push r */
		break;
	}
	args_size += 大小;

	弹出堆栈值();
	--nb_args;
	k++;
    }

    zhi_释放(onstack);

    /* XXX This should be superfluous.  */
    save_regs(0); /* save used temporary registers */

    /* then, we prepare register passing arguments.
       Note that we cannot set RDX and RCX in this loop because 生成值()
       may break these temporary registers. Let's use R10 and R11
       instead of them */
    assert(gen_reg <= REGN);
    assert(sse_reg <= 8);
    for(i = 0; i < nb_args; i++) {
        mode = classify_x86_64_arg(&栈顶值->类型, &类型, &大小, &对齐, &reg_count);
        if (大小 == 0) continue;
        /* Alter stack entry 类型 so that 生成值() knows how to treat it */
        栈顶值->类型 = 类型;
        if (mode == x86_64_mode_sse) {
            if (reg_count == 2) {
                sse_reg -= 2;
                生成值(RC_FRET); /* Use pair load into xmm0 & xmm1 */
                if (sse_reg) { /* avoid redundant movaps %xmm0, %xmm0 */
                    /* movaps %xmm1, %xmmN */
                    o(0x280f);
                    o(0xc1 + ((sse_reg+1) << 3));
                    /* movaps %xmm0, %xmmN */
                    o(0x280f);
                    o(0xc0 + (sse_reg << 3));
                }
            } else {
                assert(reg_count == 1);
                --sse_reg;
                /* Load directly to register */
                生成值(RC_XMM0 << sse_reg);
            }
        } else if (mode == x86_64_mode_integer) {
            /* simple 类型 */
            /* XXX: implicit cast ? */
            int d;
            gen_reg -= reg_count;
            r = 生成值(RC_INT);
            d = arg_prepare_reg(gen_reg);
            orex(1,d,r,0x89); /* mov */
            o(0xc0 + REG_VALUE(r) * 8 + REG_VALUE(d));
            if (reg_count == 2) {
                d = arg_prepare_reg(gen_reg+1);
                orex(1,d,栈顶值->r2,0x89); /* mov */
                o(0xc0 + REG_VALUE(栈顶值->r2) * 8 + REG_VALUE(d));
            }
        }
        栈顶值--;
    }
    assert(gen_reg == 0);
    assert(sse_reg == 0);

    /* We shouldn't have many operands on the stack anymore, but the
       call address itself is still there, and it might be in %eax
       (or edx/ecx) currently, which the below writes would clobber.
       So evict all remaining operands here.  */
    save_regs(0);

    /* Copy R10 and R11 into RDX and RCX, respectively */
    if (nb_reg_args > 2) {
        o(0xd2894c); /* mov %r10, %rdx */
        if (nb_reg_args > 3) {
            o(0xd9894c); /* mov %r11, %rcx */
        }
    }

    if (栈顶值->类型.引用符号->f.函数_类型!= 函数原型_新) /* implies 函数原型_旧 or 函数原型_省略 */
        oad(0xb8, nb_sse_args < 8 ? nb_sse_args : 8); /* mov nb_sse_args, %eax */
    gcall_or_jmp(0);
    if (args_size)
        gadd_sp(args_size);
    栈顶值--;
}

#define 函数序言大小M 11

static void push_arg_reg(int i) {
    局部变量索引 -= 8;
    生成modrm64(0x89, arg_regs[i], 存储类型_局部, NULL, 局部变量索引);
}

/* 生成“t”类型的函数序言 */
void 生成函数开头代码(符号ST *函数符号)
{
    类型ST *函数_类型= &函数符号->类型;
    X86_64_Mode mode, ret_mode;
    int i, addr, 对齐, 大小, reg_count;
    int param_addr = 0, reg_param_index, sse_param_index;
    符号ST *sym;
    类型ST *类型;

    sym = 函数_类型->引用符号;
    addr = 宏_指针大小 * 2;
    局部变量索引 = 0;
    指令在代码节位置 += 函数序言大小M;
    func_sub_sp_offset = 指令在代码节位置;
    func_ret_sub = 0;
    ret_mode = classify_x86_64_arg(&函数_返回类型, NULL, &大小, &对齐, &reg_count);

    if (函数_可变参) {
        int seen_reg_num, seen_sse_num, seen_stack_size;
        seen_reg_num = ret_mode == x86_64_mode_memory;
        seen_sse_num = 0;
        /* frame pointer and return address */
        seen_stack_size = 宏_指针大小 * 2;
        /* count the number of seen parameters */
        sym = 函数_类型->引用符号;
        while ((sym = sym->next) != NULL) {
            类型 = &sym->类型;
            mode = classify_x86_64_arg(类型, NULL, &大小, &对齐, &reg_count);
            switch (mode) {
            default:
            stack_arg:
                seen_stack_size = ((seen_stack_size + 对齐 - 1) & -对齐) + 大小;
                break;
                
            case x86_64_mode_integer:
                if (seen_reg_num + reg_count > REGN)
		    goto stack_arg;
		seen_reg_num += reg_count;
                break;
                
            case x86_64_mode_sse:
                if (seen_sse_num + reg_count > 8)
		    goto stack_arg;
		seen_sse_num += reg_count;
                break;
            }
        }

        局部变量索引 -= 24;
        /* movl $0x????????, -0x18(%rbp) */
        o(0xe845c7);
        gen_le32(seen_reg_num * 8);
        /* movl $0x????????, -0x14(%rbp) */
        o(0xec45c7);
        gen_le32(seen_sse_num * 16 + 48);
	/* leaq $0x????????, %r11 */
	o(0x9d8d4c);
	gen_le32(seen_stack_size);
	/* movq %r11, -0x10(%rbp) */
	o(0xf05d894c);
	/* leaq $-192(%rbp), %r11 */
	o(0x9d8d4c);
	gen_le32(-176 - 24);
	/* movq %r11, -0x8(%rbp) */
	o(0xf85d894c);

        /* save all register passing arguments */
        for (i = 0; i < 8; i++) {
            局部变量索引 -= 16;
	    if (!全局虚拟机->nosse) {
		o(0xd60f66); /* movq */
		gen_modrm(7 - i, 存储类型_局部, NULL, 局部变量索引);
	    }
            /* movq $0, 局部变量索引+8(%rbp) */
            o(0x85c748);
            gen_le32(局部变量索引 + 8);
            gen_le32(0);
        }
        for (i = 0; i < REGN; i++) {
            push_arg_reg(REGN-1-i);
        }
    }

    sym = 函数_类型->引用符号;
    reg_param_index = 0;
    sse_param_index = 0;

    /* if the function returns a structure, then add an
       implicit pointer parameter */
    if (ret_mode == x86_64_mode_memory) {
        push_arg_reg(reg_param_index);
        func_vc = 局部变量索引;
        reg_param_index++;
    }
    /* define parameters */
    while ((sym = sym->next) != NULL) {
        类型 = &sym->类型;
        mode = classify_x86_64_arg(类型, NULL, &大小, &对齐, &reg_count);
        switch (mode) {
        case x86_64_mode_sse:
	    if (全局虚拟机->nosse)
	        错_误("已禁用SSE，但使用了浮点参数");
            if (sse_param_index + reg_count <= 8) {
                /* save arguments passed by register */
                局部变量索引 -= reg_count * 8;
                param_addr = 局部变量索引;
                for (i = 0; i < reg_count; ++i) {
                    o(0xd60f66); /* movq */
                    gen_modrm(sse_param_index, 存储类型_局部, NULL, param_addr + i*8);
                    ++sse_param_index;
                }
            } else {
                addr = (addr + 对齐 - 1) & -对齐;
                param_addr = addr;
                addr += 大小;
            }
            break;
            
        case x86_64_mode_memory:
        case x86_64_mode_x87:
            addr = (addr + 对齐 - 1) & -对齐;
            param_addr = addr;
            addr += 大小;
            break;
            
        case x86_64_mode_integer: {
            if (reg_param_index + reg_count <= REGN) {
                /* save arguments passed by register */
                局部变量索引 -= reg_count * 8;
                param_addr = 局部变量索引;
                for (i = 0; i < reg_count; ++i) {
                    生成modrm64(0x89, arg_regs[reg_param_index], 存储类型_局部, NULL, param_addr + i*8);
                    ++reg_param_index;
                }
            } else {
                addr = (addr + 对齐 - 1) & -对齐;
                param_addr = addr;
                addr += 大小;
            }
            break;
        }
	default: break; /* nothing to be 完成 for x86_64_mode_none */
        }
        将符号放入符号栈(sym->v & ~符号_字段, 类型,
                 存储类型_局部 | 存储类型_左值, param_addr);
    }

#ifdef 启用边界检查M
    if (全局虚拟机->使用_边界_检查器)
        生成函数绑定序言();
#endif
}

/* generate function epilog */
void 生成函数结尾代码(void)
{
    int v, saved_ind;

#ifdef 启用边界检查M
    if (全局虚拟机->使用_边界_检查器)
        gen_bounds_epilog();
#endif
    o(0xc9); /* leave */
    if (func_ret_sub == 0) {
        o(0xc3); /* ret */
    } else {
        o(0xc2); /* ret n */
        g(func_ret_sub);
        g(func_ret_sub >> 8);
    }
    /* 对齐 local 大小 to word & save local variables */
    v = (-局部变量索引 + 15) & -16;
    saved_ind = 指令在代码节位置;
    指令在代码节位置 = func_sub_sp_offset - 函数序言大小M;
    o(0xe5894855);  /* push %rbp, mov %rsp, %rbp */
    o(0xec8148);  /* sub rsp, stacksize */
    gen_le32(v);
    指令在代码节位置 = saved_ind;
}

#endif /* not PE */

静态函数 void gen_fill_nops(int bytes)
{
    while (bytes--)
      g(0x90);
}

/* 生成到标签的跳转 */
int 生成到标签的跳转(int t)
{
    return 生成到标签的跳转2(0xe9, t);
}

/* 生成一个到固定地址的跳转 */
void 生成跳转_到固定地址(int a)
{
    int r;
    r = a - 指令在代码节位置 - 2;
    if (r == (char)r) {
        g(0xeb);
        g(r);
    } else {
        oad(0xe9, a - 指令在代码节位置 - 5);
    }
}

静态函数 int 生成跳转_append(int n, int t)
{
    void *p;
    /* 在t中插入栈顶值->c跳转列表 */
    if (n)
    {
        uint32_t n1 = n, n2;
        while ((n2 = read32le(p = 当前代码节->数据 + n1)))
            n1 = n2;
        write32le(p, t);
        t = n;
    }
    return t;
}

静态函数 int 生成条件跳转(int op, int t)
{
        if (op & 0x100)
	  {
	    /* 这是一个浮动比较。如果设置了奇偶校验标志，则结果是无序的。除了！=这意味着错误，我们不会跳（在两个条件下）。
	     * 对于！=这意味着正确（或两者都正确）。注意倒置测试。如果结果是无序的，并且测试不是NE，我们需要跳转到目标，否则如果是无序的我们不想跳转。 */
            int v = 栈顶值->cmp_r;
            op &= ~0x100;
            if (op ^ v ^ (v != TOK_不等于))
              o(0x067a);  /* jp +6 */
	    else
	      {
	        g(0x0f);
		t = 生成到标签的跳转2(0x8a, t); /* jp t */
	      }
	  }
        g(0x0f);
        t = 生成到标签的跳转2(op - 16, t);
        return t;
}

/* 生成一个整数二进制操作码*/
void gen_opi(int op)
{
    int r, fr, opc, c;
    int ll, uu, cc;

    ll = is64_type(栈顶值[-1].类型.数据类型);
    uu = (栈顶值[-1].类型.数据类型 & 存储类型_无符号的) != 0;
    cc = (栈顶值->寄存qi & (存储类型_掩码 | 存储类型_左值 | 存储类型_符号)) == 存储类型_VC常量;

    switch(op) {
    case '+':
    case TOK_ADDC1: /* add with carry generation */
        opc = 0;
    gen_op8:
        if (cc && (!ll || (int)栈顶值->c.i == 栈顶值->c.i)) {
            /* constant case */
            vswap();
            r = 生成值(RC_INT);
            vswap();
            c = 栈顶值->c.i;
            if (c == (char)c) {
                /* XXX: generate inc and dec for smaller code ? */
                orex(ll, r, 0, 0x83);
                o(0xc0 | (opc << 3) | REG_VALUE(r));
                g(c);
            } else {
                orex(ll, r, 0, 0x81);
                oad(0xc0 | (opc << 3) | REG_VALUE(r), c);
            }
        } else {
            生成值2(RC_INT, RC_INT);
            r = 栈顶值[-1].寄存qi;
            fr = 栈顶值[0].寄存qi;
            orex(ll, r, fr, (opc << 3) | 0x01);
            o(0xc0 + REG_VALUE(r) + REG_VALUE(fr) * 8);
        }
        栈顶值--;
        if (op >= TOK_ULT && op <= TOK_大于)
            vset_VT_CMP(op);
        break;
    case '-':
    case TOK_SUBC1: /* sub with carry generation */
        opc = 5;
        goto gen_op8;
    case TOK_ADDC2: /* add with carry use */
        opc = 2;
        goto gen_op8;
    case TOK_SUBC2: /* sub with carry use */
        opc = 3;
        goto gen_op8;
    case '&':
        opc = 4;
        goto gen_op8;
    case '^':
        opc = 6;
        goto gen_op8;
    case '|':
        opc = 1;
        goto gen_op8;
    case '*':
        生成值2(RC_INT, RC_INT);
        r = 栈顶值[-1].寄存qi;
        fr = 栈顶值[0].寄存qi;
        orex(ll, fr, r, 0xaf0f); /* imul fr, r */
        o(0xc0 + REG_VALUE(fr) + REG_VALUE(r) * 8);
        栈顶值--;
        break;
    case TOK_左移:
        opc = 4;
        goto gen_shift;
    case TOK_SHR:
        opc = 5;
        goto gen_shift;
    case TOK_右移:
        opc = 7;
    gen_shift:
        opc = 0xc0 | (opc << 3);
        if (cc) {
            /* constant case */
            vswap();
            r = 生成值(RC_INT);
            vswap();
            orex(ll, r, 0, 0xc1); /* shl/shr/sar $xxx, r */
            o(opc | REG_VALUE(r));
            g(栈顶值->c.i & (ll ? 63 : 31));
        } else {
            /* we generate the shift in ecx */
            生成值2(RC_INT, RC_RCX);
            r = 栈顶值[-1].寄存qi;
            orex(ll, r, 0, 0xd3); /* shl/shr/sar %cl, r */
            o(opc | REG_VALUE(r));
        }
        栈顶值--;
        break;
    case TOK_UDIV:
    case TOK_UMOD:
        uu = 1;
        goto divmod;
    case '/':
    case '%':
    case TOK_PDIV:
        uu = 0;
    divmod:
        /* first operand must be in eax */
        /* XXX: need better constraint for second operand */
        生成值2(RC_RAX, RC_RCX);
        r = 栈顶值[-1].寄存qi;
        fr = 栈顶值[0].寄存qi;
        栈顶值--;
        save_reg(TREG_RDX);
        orex(ll, 0, 0, uu ? 0xd231 : 0x99); /* xor %edx,%edx : cqto */
        orex(ll, fr, 0, 0xf7); /* div fr, %eax */
        o((uu ? 0xf0 : 0xf8) + REG_VALUE(fr));
        if (op == '%' || op == TOK_UMOD)
            r = TREG_RDX;
        else
            r = TREG_RAX;
        栈顶值->寄存qi = r;
        break;
    default:
        opc = 7;
        goto gen_op8;
    }
}

void 生成长长整数操作(int op)
{
    gen_opi(op);
}

void vpush_const(int t, int v)
{
    类型ST ctype = { t | 存储类型_常量, 0 };
    类型符号值压入栈(&ctype, 外部_全局_符号(v, &ctype));
    栈顶值->寄存qi |= 存储类型_左值;
}

/* generate a floating point operation 'v = t1 op t2' instruction. The
   two operands are guaranteed to have the same floating point 类型 */
/* XXX: need to use ST1 too */
void gen_opf(int op)
{
    int a, ft, fc, swapped, r;
    int bt = 栈顶值->类型.数据类型 & 数据类型_基本类型;
    int float_type = bt == 数据类型_长双精度 ? RC_ST0 : RC_FLOAT;

    if (op == TOK_NEG) { /* 一元 minus */
        生成值(float_type);
        if (float_type == RC_ST0) {
            o(0xe0d9); /* fchs */
        } else {
            /* -0.0, in 运行时库.c */
            vpush_const(bt, bt == 数据类型_浮点 ? TOK___mzerosf : TOK___mzerodf);
            生成值(RC_FLOAT);
            if (bt == 数据类型_双精度)
                o(0x66);
            /* xorp[sd] %xmm1, %xmm0 */
            o(0xc0570f | (REG_VALUE(栈顶值[0].寄存qi) + REG_VALUE(栈顶值[-1].寄存qi)*8) << 16);
            栈顶值--;
        }
        return;
    }

    /* convert constants to memory references */
    if ((栈顶值[-1].寄存qi & (存储类型_掩码 | 存储类型_左值)) == 存储类型_VC常量) {
        vswap();
        生成值(float_type);
        vswap();
    }
    if ((栈顶值[0].寄存qi & (存储类型_掩码 | 存储类型_左值)) == 存储类型_VC常量)
        生成值(float_type);

    /* must put at least one value in the floating point register */
    if ((栈顶值[-1].寄存qi & 存储类型_左值) &&
        (栈顶值[0].寄存qi & 存储类型_左值)) {
        vswap();
        生成值(float_type);
        vswap();
    }
    swapped = 0;
    /* swap the stack if needed so that t1 is the register and t2 is
       the memory reference */
    if (栈顶值[-1].寄存qi & 存储类型_左值) {
        vswap();
        swapped = 1;
    }
    if ((栈顶值->类型.数据类型 & 数据类型_基本类型) == 数据类型_长双精度) {
        if (op >= TOK_ULT && op <= TOK_大于) {
            /* load on stack second operand */
            load(TREG_ST0, 栈顶值);
            save_reg(TREG_RAX); /* eax is used by FP comparison code */
            if (op == TOK_大于等于 || op == TOK_大于)
                swapped = !swapped;
            else if (op == TOK_等于 || op == TOK_不等于)
                swapped = 0;
            if (swapped)
                o(0xc9d9); /* fxch %st(1) */
            if (op == TOK_等于 || op == TOK_不等于)
                o(0xe9da); /* fucompp */
            else
                o(0xd9de); /* fcompp */
            o(0xe0df); /* fnstsw %ax */
            if (op == TOK_等于) {
                o(0x45e480); /* and $0x45, %ah */
                o(0x40fC80); /* cmp $0x40, %ah */
            } else if (op == TOK_不等于) {
                o(0x45e480); /* and $0x45, %ah */
                o(0x40f480); /* xor $0x40, %ah */
                op = TOK_不等于;
            } else if (op == TOK_大于等于 || op == TOK_小于等于) {
                o(0x05c4f6); /* test $0x05, %ah */
                op = TOK_等于;
            } else {
                o(0x45c4f6); /* test $0x45, %ah */
                op = TOK_等于;
            }
            栈顶值--;
            vset_VT_CMP(op);
        } else {
            /* no memory reference possible for long double operations */
            load(TREG_ST0, 栈顶值);
            swapped = !swapped;

            switch(op) {
            default:
            case '+':
                a = 0;
                break;
            case '-':
                a = 4;
                if (swapped)
                    a++;
                break;
            case '*':
                a = 1;
                break;
            case '/':
                a = 6;
                if (swapped)
                    a++;
                break;
            }
            ft = 栈顶值->类型.数据类型;
            fc = 栈顶值->c.i;
            o(0xde); /* fxxxp %st, %st(1) */
            o(0xc1 + (a << 3));
            栈顶值--;
        }
    } else {
        if (op >= TOK_ULT && op <= TOK_大于) {
            /* if saved lvalue, then we must reload it */
            r = 栈顶值->寄存qi;
            fc = 栈顶值->c.i;
            if ((r & 存储类型_掩码) == 存储类型_LLOCAL) {
                栈值ST v1;
                r = get_reg(RC_INT);
                v1.类型.数据类型 = 数据类型_指针;
                v1.寄存qi = 存储类型_局部 | 存储类型_左值;
                v1.c.i = fc;
                load(r, &v1);
                fc = 0;
                栈顶值->寄存qi = r = r | 存储类型_左值;
            }

            if (op == TOK_等于 || op == TOK_不等于) {
                swapped = 0;
            } else {
                if (op == TOK_小于等于 || op == TOK_小于)
                    swapped = !swapped;
                if (op == TOK_小于等于 || op == TOK_大于等于) {
                    op = 0x93; /* setae */
                } else {
                    op = 0x97; /* seta */
                }
            }

            if (swapped) {
                生成值(RC_FLOAT);
                vswap();
            }
            assert(!(栈顶值[-1].寄存qi & 存储类型_左值));
            
            if ((栈顶值->类型.数据类型 & 数据类型_基本类型) == 数据类型_双精度)
                o(0x66);
            if (op == TOK_等于 || op == TOK_不等于)
                o(0x2e0f); /* ucomisd */
            else
                o(0x2f0f); /* comisd */

            if (栈顶值->寄存qi & 存储类型_左值) {
                gen_modrm(栈顶值[-1].寄存qi, r, 栈顶值->sym, fc);
            } else {
                o(0xc0 + REG_VALUE(栈顶值[0].寄存qi) + REG_VALUE(栈顶值[-1].寄存qi)*8);
            }

            栈顶值--;
            vset_VT_CMP(op | 0x100);
            栈顶值->cmp_r = op;
        } else {
            assert((栈顶值->类型.数据类型 & 数据类型_基本类型) != 数据类型_长双精度);
            switch(op) {
            default:
            case '+':
                a = 0;
                break;
            case '-':
                a = 4;
                break;
            case '*':
                a = 1;
                break;
            case '/':
                a = 6;
                break;
            }
            ft = 栈顶值->类型.数据类型;
            fc = 栈顶值->c.i;
            assert((ft & 数据类型_基本类型) != 数据类型_长双精度);
            
            r = 栈顶值->寄存qi;
            /* if saved lvalue, then we must reload it */
            if ((栈顶值->寄存qi & 存储类型_掩码) == 存储类型_LLOCAL) {
                栈值ST v1;
                r = get_reg(RC_INT);
                v1.类型.数据类型 = 数据类型_指针;
                v1.寄存qi = 存储类型_局部 | 存储类型_左值;
                v1.c.i = fc;
                load(r, &v1);
                fc = 0;
                栈顶值->寄存qi = r = r | 存储类型_左值;
            }
            
            assert(!(栈顶值[-1].寄存qi & 存储类型_左值));
            if (swapped) {
                assert(栈顶值->寄存qi & 存储类型_左值);
                生成值(RC_FLOAT);
                vswap();
            }
            
            if ((ft & 数据类型_基本类型) == 数据类型_双精度) {
                o(0xf2);
            } else {
                o(0xf3);
            }
            o(0x0f);
            o(0x58 + a);
            
            if (栈顶值->寄存qi & 存储类型_左值) {
                gen_modrm(栈顶值[-1].寄存qi, r, 栈顶值->sym, fc);
            } else {
                o(0xc0 + REG_VALUE(栈顶值[0].寄存qi) + REG_VALUE(栈顶值[-1].寄存qi)*8);
            }

            栈顶值--;
        }
    }
}

/* convert integers to fp 't' 类型. Must handle 'int', 'unsigned int'
   and 'long long' cases. */
void gen_cvt_itof(int t)
{
    if ((t & 数据类型_基本类型) == 数据类型_长双精度) {
        save_reg(TREG_ST0);
        生成值(RC_INT);
        if ((栈顶值->类型.数据类型 & 数据类型_基本类型) == 数据类型_长长整数) {
            /* signed long long to float/double/long double (unsigned case
               is handled generically) */
            o(0x50 + (栈顶值->寄存qi & 存储类型_掩码)); /* push r */
            o(0x242cdf); /* fildll (%rsp) */
            o(0x08c48348); /* add $8, %rsp */
        } else if ((栈顶值->类型.数据类型 & (数据类型_基本类型 | 存储类型_无符号的)) ==
                   (数据类型_整数 | 存储类型_无符号的)) {
            /* unsigned int to float/double/long double */
            o(0x6a); /* push $0 */
            g(0x00);
            o(0x50 + (栈顶值->寄存qi & 存储类型_掩码)); /* push r */
            o(0x242cdf); /* fildll (%rsp) */
            o(0x10c48348); /* add $16, %rsp */
        } else {
            /* int to float/double/long double */
            o(0x50 + (栈顶值->寄存qi & 存储类型_掩码)); /* push r */
            o(0x2404db); /* fildl (%rsp) */
            o(0x08c48348); /* add $8, %rsp */
        }
        栈顶值->寄存qi = TREG_ST0;
    } else {
        int r = get_reg(RC_FLOAT);
        生成值(RC_INT);
        o(0xf2 + ((t & 数据类型_基本类型) == 数据类型_浮点?1:0));
        if ((栈顶值->类型.数据类型 & (数据类型_基本类型 | 存储类型_无符号的)) ==
            (数据类型_整数 | 存储类型_无符号的) ||
            (栈顶值->类型.数据类型 & 数据类型_基本类型) == 数据类型_长长整数) {
            o(0x48); /* REX */
        }
        o(0x2a0f);
        o(0xc0 + (栈顶值->寄存qi & 存储类型_掩码) + REG_VALUE(r)*8); /* cvtsi2sd */
        栈顶值->寄存qi = r;
    }
}

/* convert from one floating point 类型 to another */
void gen_cvt_ftof(int t)
{
    int ft, bt, tbt;

    ft = 栈顶值->类型.数据类型;
    bt = ft & 数据类型_基本类型;
    tbt = t & 数据类型_基本类型;
    
    if (bt == 数据类型_浮点) {
        生成值(RC_FLOAT);
        if (tbt == 数据类型_双精度) {
            o(0x140f); /* unpcklps */
            o(0xc0 + REG_VALUE(栈顶值->寄存qi)*9);
            o(0x5a0f); /* cvtps2pd */
            o(0xc0 + REG_VALUE(栈顶值->寄存qi)*9);
        } else if (tbt == 数据类型_长双精度) {
            save_reg(RC_ST0);
            /* movss %xmm0,-0x10(%rsp) */
            o(0x110ff3);
            o(0x44 + REG_VALUE(栈顶值->寄存qi)*8);
            o(0xf024);
            o(0xf02444d9); /* flds -0x10(%rsp) */
            栈顶值->寄存qi = TREG_ST0;
        }
    } else if (bt == 数据类型_双精度) {
        生成值(RC_FLOAT);
        if (tbt == 数据类型_浮点) {
            o(0x140f66); /* unpcklpd */
            o(0xc0 + REG_VALUE(栈顶值->寄存qi)*9);
            o(0x5a0f66); /* cvtpd2ps */
            o(0xc0 + REG_VALUE(栈顶值->寄存qi)*9);
        } else if (tbt == 数据类型_长双精度) {
            save_reg(RC_ST0);
            /* movsd %xmm0,-0x10(%rsp) */
            o(0x110ff2);
            o(0x44 + REG_VALUE(栈顶值->寄存qi)*8);
            o(0xf024);
            o(0xf02444dd); /* fldl -0x10(%rsp) */
            栈顶值->寄存qi = TREG_ST0;
        }
    } else {
        int r;
        生成值(RC_ST0);
        r = get_reg(RC_FLOAT);
        if (tbt == 数据类型_双精度) {
            o(0xf0245cdd); /* fstpl -0x10(%rsp) */
            /* movsd -0x10(%rsp),%xmm0 */
            o(0x100ff2);
            o(0x44 + REG_VALUE(r)*8);
            o(0xf024);
            栈顶值->寄存qi = r;
        } else if (tbt == 数据类型_浮点) {
            o(0xf0245cd9); /* fstps -0x10(%rsp) */
            /* movss -0x10(%rsp),%xmm0 */
            o(0x100ff3);
            o(0x44 + REG_VALUE(r)*8);
            o(0xf024);
            栈顶值->寄存qi = r;
        }
    }
}

/* convert fp to int 't' 类型 */
void gen_cvt_ftoi(int t)
{
    int ft, bt, 大小, r;
    ft = 栈顶值->类型.数据类型;
    bt = ft & 数据类型_基本类型;
    if (bt == 数据类型_长双精度) {
        gen_cvt_ftof(数据类型_双精度);
        bt = 数据类型_双精度;
    }

    生成值(RC_FLOAT);
    if (t != 数据类型_整数)
        大小 = 8;
    else
        大小 = 4;

    r = get_reg(RC_INT);
    if (bt == 数据类型_浮点) {
        o(0xf3);
    } else if (bt == 数据类型_双精度) {
        o(0xf2);
    } else {
        assert(0);
    }
    orex(大小 == 8, r, 0, 0x2c0f); /* cvttss2si or cvttsd2si */
    o(0xc0 + REG_VALUE(栈顶值->寄存qi) + REG_VALUE(r)*8);
    栈顶值->寄存qi = r;
}

// Generate sign extension from 32 to 64 bits:
静态函数 void gen_cvt_sxtw(void)
{
    int r = 生成值(RC_INT);
    /* x86_64 specific: movslq */
    o(0x6348);
    o(0xc0 + (REG_VALUE(r) << 3) + REG_VALUE(r));
}

/* char/short to int conversion */
静态函数 void gen_cvt_csti(int t)
{
    int r, sz, xl, ll;
    r = 生成值(RC_INT);
    sz = !(t & 存储类型_无符号的);
    xl = (t & 数据类型_基本类型) == 数据类型_短整数;
    ll = (栈顶值->类型.数据类型 & 数据类型_基本类型) == 数据类型_长长整数;
    orex(ll, r, 0, 0xc0b60f /* mov[sz] %a[xl], %eax */
        | (sz << 3 | xl) << 8
        | (REG_VALUE(r) << 3 | REG_VALUE(r)) << 16
        );
}

/* increment tcov counter */
静态函数 void gen_increment_tcov (栈值ST *sv)
{
   o(0x058348); /* addq $1, xxx(%rip) */
   添加新的重定位条目(当前代码节, sv->sym, 指令在代码节位置, R_X86_64_PC32, -5);
   gen_le32(0);
   o(1);
}

/* computed goto support */
void ggoto(void)
{
    gcall_or_jmp(1);
    栈顶值--;
}

/* Save the stack pointer onto the stack and return the 地点 of its address */
静态函数 void gen_vla_sp_save(int addr) {
    /* mov %rsp,addr(%rbp)*/
    生成modrm64(0x89, TREG_RSP, 存储类型_局部, NULL, addr);
}

/* 从堆栈上的某个位置恢复 SP */
静态函数 void gen_vla_sp_恢复(int addr) {
    生成modrm64(0x8b, TREG_RSP, 存储类型_局部, NULL, addr);
}

#ifdef 目标_PE
/* Save result of gen_vla_alloc onto the stack */
静态函数 void gen_vla_result(int addr) {
    /* mov %rax,addr(%rbp)*/
    生成modrm64(0x89, TREG_RAX, 存储类型_局部, NULL, addr);
}
#endif

/* Subtract from the stack pointer, and push the resulting value onto the stack */
静态函数 void gen_vla_alloc(类型ST *类型, int 对齐) {
    int use_call = 0;

#if defined(启用边界检查M)
    use_call = 全局虚拟机->使用_边界_检查器;
#endif
#ifdef 目标_PE	/* alloca does more than just adjust %rsp on Windows */
    use_call = 1;
#endif
    if (use_call)
    {
        推送_辅助_函数引用(TOK_alloca);
        vswap(); /* Move alloca 引用符号 past allocation 大小 */
        g函数_调用(1);
    }
    else {
        int r;
        r = 生成值(RC_INT); /* allocation 大小 */
        /* sub r,%rsp */
        o(0x2b48);
        o(0xe0 | REG_VALUE(r));
        /* We 对齐 to 16 bytes rather than 对齐 */
        /* and ~15, %rsp */
        o(0xf0e48348);
        弹出堆栈值();
    }
}


/* end of x86-64 code generator */
/*************************************************************/
#endif /* ! TARGET_DEFS_ONLY */
/******************************************************/
