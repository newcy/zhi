/****************************************************************************************************
 * 名称：词法分析
 * 描述：中文版C语言编译器。
 * 版权(C) 2001-2004 Fabrice Bellard
 * 版权(C) 2019-2023 位中原
 * 网址：www.880.xin
 ****************************************************************************************************/
#define 全局使用
#include "zhi.h"

静态数据 int 单词标志;
静态数据 int 解析标志;

静态数据 struct 缓冲文件S *文件;
静态数据 int 当前字符, 单词编码;
静态数据 常量值U 单词常量;
静态数据 const int *没宏替换的字符串指针;
静态数据 动态字符串ST 当前解析的字符串;

/* 显示基本信息 */
静态数据 int 单词_标识符;
静态数据 单词ST **标识符表;

/* ------------------------------------------------------------------------- */

static 单词ST *单词哈希表[哈希表容量];
static char 单词_缓存[字符串最大长度 + 1];
static 动态字符串ST 动态字符串_缓冲;
static 动态字符串ST 宏_等于_缓冲;
static 单词字符串ST 单词字符串_缓冲;
static unsigned char 标识符数字表[256 - 中_文件结尾];
static int pp_debug_tok, pp_debug_symv;
static int pp_once;
static int pp_expr;
static int 词法计数器;
static void tok_print(const char *msg, const int *字符串);

static struct 小分配器S *单词符号分配器;
static struct 小分配器S *单词字符串分配器;

static 单词字符串ST *宏_栈;

static const char 关键词表[] = 
#define 系统(id, 字符串) 字符串 "\0"
#include "单词.h"
;

/* 警告：此字符串的内容编码单词编号 */
static const unsigned char 单词_2个字符[] =
{
    '<','=', TOK_小于等于,
    '>','=', TOK_大于等于,
    '!','=', TOK_不等于,
    '&','&', TOK_逻辑与,
    '|','|', TOK_逻辑或,
    '+','+', TOK_自加,
    '-','-', TOK_自减,
    '=','=', TOK_等于,
    '<','<', TOK_左移,
    '>','>', TOK_右移,
    '+','=', TOK_加后赋值,
    '-','=', TOK_减后赋值,
    '*','=', TOK_乘后赋值,
    '/','=', TOK_除后赋值,
    '%','=', TOK_取模后赋值,
    '&','=', TOK_按位与后赋值,
    '^','=', TOK_按位异或后赋值,
    '|','=', TOK_按位或后赋值,
    '-','>', TOK_箭头,
    '.','.', TOK_两个点,
    '#','#', TOK_双井号,
    '#','#', TOK_PPJOIN,
    0
};

static void 取下个单词不宏替换(void);

静态函数 void 跳过(int c)
{
    if (单词编码 != c)
        错_误("应为 '%c'  (但获取到的是 \"%s\")", c, 取单词字符串(单词编码, &单词常量));
    取下个单词();
}

静态函数 void 应为(const char *msg)
{
    错_误("应为 %s ", msg);
}

/* ------------------------------------------------------------------------- */
/* 小对象的自定义分配器 */

#define USE_TAL

#ifndef USE_TAL
#  define 小分配器_free(al, p) zhi_释放(p)
#  define 小分配器_realloc(al, p, 大小) zhi_重新分配(p, 大小)
#  define 小分配器_新建(a,b,c)
#  define 小分配器_删除(a)
#else
#  if !defined(内存_调试)
#    define 小分配器_free(al, p) 小分配器_释放_执行(al, p)
#    define 小分配器_realloc(al, p, 大小) 小分配器_重新分配_执行(&al, p, 大小)
#    define TAL_DEBUG_PARAMS
#  else
#    define TAL_DEBUG 1
#    define 小分配器_free(al, p) 小分配器_释放_执行(al, p, __FILE__, __LINE__)
#    define 小分配器_realloc(al, p, 大小) 小分配器_重新分配_执行(&al, p, 大小, __FILE__, __LINE__)
#    define TAL_DEBUG_PARAMS , const char *文件, int line
#    define TAL_DEBUG_FILE_LEN 40
#  endif
#  define 单词符号表大小M     (768 * 1024) /*  标识符表中的单词分配器 */
#  define TOKSTR_TAL_SIZE     (768 * 1024) /* 单词字符串实例的分配器 */
#  define CSTR_TAL_SIZE       (256 * 1024) /* 动态字符串实例的分配器 */
#  define 单词符号分配限制    256 /* 更喜欢唯一的限制来区分分配器调试消息 */
#  define 单词字符串分配限制    128 /* 32 * sizeof(int) */
#  define CSTR_TAL_LIMIT      1024

typedef struct 小分配器S {
    unsigned  limit;
    unsigned  大小;
    uint8_t *缓冲;
    uint8_t *p;
    unsigned  nb_allocs;
    struct 小分配器S *next, *top;
#  ifdef TAL_INFO
    unsigned  nb_peak;
    unsigned  nb_total;
    unsigned  nb_missed;
    uint8_t *peak_p;
#  endif
} 小分配器S;

typedef struct 小分配器_header_t {
    unsigned  大小;
#  ifdef TAL_DEBUG
    int     当前行号;
    char    file_name[TAL_DEBUG_FILE_LEN + 1];
#  endif
} 小分配器_header_t;

/* ------------------------------------------------------------------------- */

static 小分配器S *小分配器_新建(小分配器S **pal, unsigned limit, unsigned 大小)
{
    小分配器S *al = 初始化内存(sizeof(小分配器S));
    al->p = al->缓冲 = zhi_分配内存(大小);
    al->limit = limit;
    al->大小 = 大小;
    if (pal)
    	*pal = al;
    return al;
}

static void 小分配器_删除(小分配器S *al)
{
    小分配器S *next;

tail_call:
    if (!al)
        return;
#  ifdef TAL_INFO
    fprintf(stderr, "限制=%5d, 大小=%5g MB, 顶值数=%6d, 总数=%8d, 遗失数=%6d, 用法=%5.1f%%\n",
            al->limit, al->大小 / 1024.0 / 1024.0, al->nb_peak, al->nb_total, al->nb_missed,
            (al->peak_p - al->缓冲) * 100.0 / al->大小);
#  endif
#  ifdef TAL_DEBUG
    if (al->nb_allocs > 0) {
        uint8_t *p;
        fprintf(stderr, "小分配器_调试: 内存泄露 %d 块(s) (限制= %d)\n",
                al->nb_allocs, al->limit);
        p = al->缓冲;
        while (p < al->p) {
            小分配器_header_t *header = (小分配器_header_t *)p;
            if (header->当前行号 > 0) {
                fprintf(stderr, "%s:%d: %d 个字节的内存块泄漏\n",
                        header->file_name, header->当前行号, header->大小);
            }
            p += header->大小 + sizeof(小分配器_header_t);
        }
#    if 内存_调试-0 == 2
        exit(2);
#    endif
    }
#  endif
    next = al->next;
    zhi_释放(al->缓冲);
    zhi_释放(al);
    al = next;
    goto tail_call;
}

static void 小分配器_释放_执行(小分配器S *al, void *p TAL_DEBUG_PARAMS)
{
    if (!p)
        return;
tail_call:
    if (al->缓冲 <= (uint8_t *)p && (uint8_t *)p < al->缓冲 + al->大小) {
#  ifdef TAL_DEBUG
        小分配器_header_t *header = (((小分配器_header_t *)p) - 1);
        if (header->当前行号 < 0)
        {
            fprintf(stderr, "%s:%d: TAL_DEBUG: double frees chunk from\n",文件, line);
            fprintf(stderr, "%s:%d: %d bytes\n",header->file_name, (int)-header->当前行号, (int)header->大小);
        } else
            header->当前行号 = -header->当前行号;
#  endif
        al->nb_allocs--;
        if (!al->nb_allocs)
            al->p = al->缓冲;
    } else if (al->next) {
        al = al->next;
        goto tail_call;
    }
    else
        zhi_释放(p);
}

static void *小分配器_重新分配_执行(小分配器S **pal, void *p, unsigned 大小 TAL_DEBUG_PARAMS)
{
    小分配器_header_t *header;
    void *ret;
    int is_own;
    unsigned adj_size = (大小 + 3) & -4;
    小分配器S *al = *pal;

tail_call:
    is_own = (al->缓冲 <= (uint8_t *)p && (uint8_t *)p < al->缓冲 + al->大小);
    if ((!p || is_own) && 大小 <= al->limit) {
        if (al->p - al->缓冲 + adj_size + sizeof(小分配器_header_t) < al->大小) {
            header = (小分配器_header_t *)al->p;
            header->大小 = adj_size;
#  ifdef TAL_DEBUG
            { int ofs = strlen(文件) - TAL_DEBUG_FILE_LEN;
            strncpy(header->file_name, 文件 + (ofs > 0 ? ofs : 0), TAL_DEBUG_FILE_LEN);
            header->file_name[TAL_DEBUG_FILE_LEN] = 0;
            header->当前行号 = line; }
#  endif
            ret = al->p + sizeof(小分配器_header_t);
            al->p += adj_size + sizeof(小分配器_header_t);
            if (is_own) {
                header = (((小分配器_header_t *)p) - 1);
                if (p) memcpy(ret, p, header->大小);
#  ifdef TAL_DEBUG
                header->当前行号 = -header->当前行号;
#  endif
            } else {
                al->nb_allocs++;
            }
#  ifdef TAL_INFO
            if (al->nb_peak < al->nb_allocs)
                al->nb_peak = al->nb_allocs;
            if (al->peak_p < al->p)
                al->peak_p = al->p;
            al->nb_total++;
#  endif
            return ret;
        } else if (is_own) {
            al->nb_allocs--;
            ret = 小分配器_realloc(*pal, 0, 大小);
            header = (((小分配器_header_t *)p) - 1);
            if (p) memcpy(ret, p, header->大小);
#  ifdef TAL_DEBUG
            header->当前行号 = -header->当前行号;
#  endif
            return ret;
        }
        if (al->next) {
            al = al->next;
        } else {
            小分配器S *bottom = al, *next = al->top ? al->top : al;

            al = 小分配器_新建(pal, next->limit, next->大小 * 2);
            al->next = next;
            bottom->top = al;
        }
        goto tail_call;
    }
    if (is_own) {
        al->nb_allocs--;
        ret = zhi_分配内存(大小);
        header = (((小分配器_header_t *)p) - 1);
        if (p) memcpy(ret, p, header->大小);
#  ifdef TAL_DEBUG
        header->当前行号 = -header->当前行号;
#  endif
    } else if (al->next) {
        al = al->next;
        goto tail_call;
    } else
        ret = zhi_重新分配(p, 大小);
#  ifdef TAL_INFO
    al->nb_missed++;
#  endif
    return ret;
}

#endif /* USE_TAL 结束 */


/* 分配一个新单词*/
static 单词ST *符号表插入新单词(单词ST **pts, const char *字符串, int 长度)
{
    单词ST *ts, **ptable;
    int i;

    if (单词_标识符 >= 第一个匿名符号) 
        错_误("内存已满（符号ST）");

    /* 如果需要，扩展单词表 */
    i = 单词_标识符 - TOK_标识符;
    if ((i % 单词分配增量) == 0) {
        ptable = zhi_重新分配(标识符表, (i + 单词分配增量) * sizeof(单词ST *));
        标识符表 = ptable;
    }

    ts = 小分配器_realloc(单词符号分配器, 0, sizeof(单词ST) + 长度);
    标识符表[i] = ts;
    ts->单词编码 = 单词_标识符++;
    ts->define符号 = NULL;
    ts->label符号 = NULL;
    ts->struct符号 = NULL;
    ts->标识符符号 = NULL;
    ts->长度 = 长度;
    ts->哈希冲突的其他单词 = NULL;
    memcpy(ts->字符串, 字符串, 长度);
    ts->字符串[长度] = '\0';
    *pts = ts;
    return ts;
}

#define 单词哈希初始值 1
#define 计算哈希地址(h, c) ((h) + ((h) << 5) + ((h) >> 27) + (c))


/* 找到一个单词，如果没有找到就添加它*/
静态函数 单词ST *插入单词(const char *字符串, int 长度)
{
    单词ST *ts, **pts;
    int i;
    unsigned int h;
    
    h = 单词哈希初始值;
    for(i=0;i<长度;i++)
        h = 计算哈希地址(h, ((unsigned char *)字符串)[i]);
    h &= (哈希表容量 - 1);

    pts = &单词哈希表[h];
    for(;;) {
        ts = *pts;
        if (!ts)
            break;
        if (ts->长度 == 长度 && !memcmp(ts->字符串, 字符串, 长度))
            return ts;
        pts = &(ts->哈希冲突的其他单词);
    }
    return 符号表插入新单词(pts, 字符串, 长度);
}

静态函数 int 标记分配常量(const char *字符串)
{
    return 插入单词(字符串, strlen(字符串))->单词编码;
}
静态函数 const char *取单词字符串(int v, 常量值U *cv)
{
    char *p;
    int i, 长度;

    动态字符串_重置(&动态字符串_缓冲);
    p = 动态字符串_缓冲.数据;

    switch(v) {
    case TOK_整数常量:
    case TOK_无符号整数常量:
    case TOK_长整数常量:
    case TOK_无符号长整数常量:
    case TOK_长长整数常量:
    case TOK_无符号长长整数常量:
        /* XXX: 不太准确，但仅用于测试  */
#ifdef _WIN32
        sprintf(p, "%u", (unsigned)cv->i);
#else
        sprintf(p, "%llu", (unsigned long long)cv->i);
#endif
        break;
    case TOK_LCHAR:
        动态字符串_追加一个字符(&动态字符串_缓冲, 'L');
    case TOK_字符常量:
        动态字符串_追加一个字符(&动态字符串_缓冲, '\'');
        动态字符串_增加字符(&动态字符串_缓冲, cv->i);
        动态字符串_追加一个字符(&动态字符串_缓冲, '\'');
        动态字符串_追加一个字符(&动态字符串_缓冲, '\0');
        break;
    case TOK_预处理数字:
    case TOK_预处理字符串:
        return (char*)cv->字符串.数据;
    case TOK_LSTR:
        动态字符串_追加一个字符(&动态字符串_缓冲, 'L');
    case TOK_字符串常量:
        动态字符串_追加一个字符(&动态字符串_缓冲, '\"');
        if (v == TOK_字符串常量) {
            长度 = cv->字符串.大小 - 1;
            for(i=0;i<长度;i++)
                动态字符串_增加字符(&动态字符串_缓冲, ((unsigned char *)cv->字符串.数据)[i]);
        } else {
            长度 = (cv->字符串.大小 / sizeof(nwchar_t)) - 1;
            for(i=0;i<长度;i++)
                动态字符串_增加字符(&动态字符串_缓冲, ((nwchar_t *)cv->字符串.数据)[i]);
        }
        动态字符串_追加一个字符(&动态字符串_缓冲, '\"');
        动态字符串_追加一个字符(&动态字符串_缓冲, '\0');
        break;

    case TOK_浮点常量:
        动态字符串_拼接(&动态字符串_缓冲, "<float>", 0);
        break;
    case TOK_双精度常量:
	动态字符串_拼接(&动态字符串_缓冲, "<double>", 0);
	break;
    case TOK_长双精度常量:
	动态字符串_拼接(&动态字符串_缓冲, "<long double>", 0);
	break;
    case TOK_行号:
	动态字符串_拼接(&动态字符串_缓冲, "<linenumber>", 0);
	break;

    /* 上面的标记有价值，下面的没有 */
    case TOK_小于:
        v = '<';
        goto addv;
    case TOK_大于:
        v = '>';
        goto addv;
    case TOK_三个点:
        return strcpy(p, "...");
    case TOK_左移后赋值:
        return strcpy(p, "<<=");
    case TOK_右移后赋值:
        return strcpy(p, ">>=");
    case TOK_文件结尾:
        return strcpy(p, "<eof>");
    default:
        if (v < TOK_标识符)
        {
            /* 在两个字节的表中搜索 */
            const unsigned char *q = 单词_2个字符;
            while (*q)
            {
                if (q[2] == v)
                {
                    *p++ = q[0];
                    *p++ = q[1];
                    *p = '\0';
                    return 动态字符串_缓冲.数据;
                }
                q += 3;
            }
        if (v >= 127) {
            sprintf(动态字符串_缓冲.数据, "<%02x>", v);
            return 动态字符串_缓冲.数据;
        }
        addv:
            *p++ = v;
    case 0: /* 无名的匿名符号 */
            *p = '\0';
        } else if (v < 单词_标识符) {
            return 标识符表[v - TOK_标识符]->字符串;
        } else if (v >= 第一个匿名符号) {
            /* 匿名符号的特殊名称 */
            sprintf(p, "L.%u", v - 第一个匿名符号);
        } else {
            /* 永远不应该发生 */
            return NULL;
        }
        break;
    }
    return 动态字符串_缓冲.数据;
}

/* 返回当前字符，处理结束块 如有必要（但不转义） */
static int 处理块结尾(void)
{
    缓冲文件ST *缓冲文件 = 文件;
    int 长;

    /* 只有在缓冲区真正结束时才尝试读取 */
    if (缓冲文件->缓冲源码串 >= 缓冲文件->缓冲结尾)
    {
        if (缓冲文件->文件描述 >= 0)
        {
#if defined(解析_调试)
            长 = 1;
#else
            长 = 输入输出缓存大小M;
#endif
            长 = read(缓冲文件->文件描述, 缓冲文件->缓冲, 长);
            if (长 < 0)
                长 = 0;
        } else
        {
            长 = 0;
        }
        总字节数 += 长;
        缓冲文件->缓冲源码串 = 缓冲文件->缓冲;
        缓冲文件->缓冲结尾 = 缓冲文件->缓冲 + 长;
        *缓冲文件->缓冲结尾 = 中_结尾;
    }
    if (缓冲文件->缓冲源码串 < 缓冲文件->缓冲结尾)
    {
        return 缓冲文件->缓冲源码串[0];
    } else
    {
        缓冲文件->缓冲源码串 = 缓冲文件->缓冲结尾;
        return 中_文件结尾;
    }
}

/* 从当前输入文件读取下一个字符并处理输入缓冲区的结尾 */
static inline void 取下个字符(void)
{
    当前字符 = *(++(文件->缓冲源码串));
    /* 缓冲区结束/文件结束 处理 */
    if (当前字符 == 中_结尾)
    {
    	当前字符 = 处理块结尾();
    }
}

/* 处理 '\[\r]\n' */
static int 处理转义符_无错误提示(void)
{
    while (当前字符 == '\\')
    {
        取下个字符();
        if (当前字符 == '\n')
        {
            文件->当前行号++;
            取下个字符();
        } else if (当前字符 == '\r')
        {
            取下个字符();
            if (当前字符 != '\n')
                goto 失败;
            文件->当前行号++;
            取下个字符();
        } else
        {
        失败:
            return 1;
        }
    }
    return 0;
}

static void 处理转义符(void)
{
    if (处理转义符_无错误提示())
        错_误("程序中的转义符 '\\' ");
}

/* 跳过转义并处理 \\n 情况。 杂散后如果字符不正确则输出错误 */
static int 处理转义符1(uint8_t *p)
{
    int c;

    文件->缓冲源码串 = p;
    if (p >= 文件->缓冲结尾)
    {
        c = 处理块结尾();
        if (c != '\\')
            return c;
        p = 文件->缓冲源码串;
    }
    当前字符 = *p;
    if (处理转义符_无错误提示())
    {
        if (!(解析标志 & 解析标志_转义))
            错_误("程序中的转义符 '\\' ");
        *--文件->缓冲源码串 = '\\';
    }
    p = 文件->缓冲源码串;
    c = *p;
    return c;
}

/* 只处理 EOB 情况，但不处理转义 */
#define PEEKC_EOB(c, p)\
{\
    p++;\
    c = *p;\
    if (c == '\\') {\
        文件->缓冲源码串 = p;\
        c = 处理块结尾();\
        p = 文件->缓冲源码串;\
    }\
}

/* 处理复杂的转义情况 */
#define PEEKC(c, p)\
{\
    p++;\
    c = *p;\
    if (c == '\\') {\
        c = 处理转义符1(p);\
        p = 文件->缓冲源码串;\
    }\
}

/* 使用 '\[\r]\n' 处理的输入。 请注意，此函数无法处理 '\' 之后的其他字符，因此您不能在字符串或注释中调用它 */
static void 取下个字符且处理转义(void)
{
    取下个字符();
    if (当前字符 == '\\') 
        处理转义符();
}

static uint8_t *解析单行注释(uint8_t *p)
{
    int c;

    p++;
    for(;;)
    {
        c = *p;
    重做:
        if (c == '\n' || c == 中_文件结尾)
        {
            break;
        } else if (c == '\\')
        {
            文件->缓冲源码串 = p;
            c = 处理块结尾();
            p = 文件->缓冲源码串;
            if (c == '\\')
            {
                PEEKC_EOB(c, p);
                if (c == '\n')
                {
                    文件->当前行号++;
                    PEEKC_EOB(c, p);
                } else if (c == '\r')
                {
                    PEEKC_EOB(c, p);
                    if (c == '\n')
                    {
                        文件->当前行号++;
                        PEEKC_EOB(c, p);
                    }
                }
            } else
            {
                goto 重做;
            }
        } else
        {
            p++;
        }
    }
    return p;
}

/* C comments */
static uint8_t *解析注释(uint8_t *p)
{
    int c;

    p++;
    for(;;) {
        /* fast 跳过 loop */
        for(;;) {
            c = *p;
            if (c == '\n' || c == '*' || c == '\\')
                break;
            p++;
            c = *p;
            if (c == '\n' || c == '*' || c == '\\')
                break;
            p++;
        }
        /* 现在我们可以处理所有的情况 */
        if (c == '\n') {
            文件->当前行号++;
            p++;
        } else if (c == '*') {
            p++;
            for(;;) {
                c = *p;
                if (c == '*') {
                    p++;
                } else if (c == '/') {
                    goto end_of_comment;
                } else if (c == '\\') {
                    文件->缓冲源码串 = p;
                    c = 处理块结尾();
                    p = 文件->缓冲源码串;
                    if (c == 中_文件结尾)
                        错_误("注释中意外的出现文件结尾");
                    if (c == '\\') {
                        /* 跳过 '\[\r]\n', otherwise just 跳过 the stray */
                        while (c == '\\') {
                            PEEKC_EOB(c, p);
                            if (c == '\n') {
                                文件->当前行号++;
                                PEEKC_EOB(c, p);
                            } else if (c == '\r') {
                                PEEKC_EOB(c, p);
                                if (c == '\n') {
                                    文件->当前行号++;
                                    PEEKC_EOB(c, p);
                                }
                            } else {
                                goto after_star;
                            }
                        }
                    }
                } else {
                    break;
                }
            }
        after_star: ;
        } else {
            /* stray, eob or eof */
            文件->缓冲源码串 = p;
            c = 处理块结尾();
            p = 文件->缓冲源码串;
            if (c == 中_文件结尾)
            {
                错_误("注释中意外的出现文件结尾");
            } else if (c == '\\') {
                p++;
            }
        }
    }
 end_of_comment:
    p++;
    return p;
}

静态函数 int 设置标识符(int c, int val)
{
    int 上个 = 标识符数字表[c - 中_文件结尾];
    标识符数字表[c - 中_文件结尾] = val;
    return 上个;
}

#define cinp 取下个字符且处理转义

static inline void 跳过空格(void)
{
    while (标识符数字表[当前字符 - 中_文件结尾] & 是_空格)
        cinp();
}

static inline int 检查空格(int t, int *spc) 
{
    if (t < 256 && (标识符数字表[t - 中_文件结尾] & 是_空格)) {
        if (*spc) 
            return 1;
        *spc = 1;
    } else 
        *spc = 0;
    return 0;
}

static uint8_t *解析字符串而不解释转义(uint8_t *p,int sep, 动态字符串ST *字符串)
{
    int c;
    p++;
    for(;;) {
        c = *p;
        if (c == sep) {
            break;
        } else if (c == '\\') {
            文件->缓冲源码串 = p;
            c = 处理块结尾();
            p = 文件->缓冲源码串;
            if (c == 中_文件结尾) {
            unterminated_string:
                /* XXX: 指示字符串开头的行号 */
                错_误("缺少终止 %c 字符", sep);
            } else if (c == '\\') {
                /* escape : just 跳过 \[\r]\n */
                PEEKC_EOB(c, p);
                if (c == '\n') {
                    文件->当前行号++;
                    p++;
                } else if (c == '\r') {
                    PEEKC_EOB(c, p);
                    if (c != '\n')
                        应为(" '\r'之后的 '\n'");
                    文件->当前行号++;
                    p++;
                } else if (c == 中_文件结尾) {
                    goto unterminated_string;
                } else {
                    if (字符串) {
                        动态字符串_追加一个字符(字符串, '\\');
                        动态字符串_追加一个字符(字符串, c);
                    }
                    p++;
                }
            }
        } else if (c == '\n') {
            文件->当前行号++;
            goto 动态字符串_增加字符;
        } else if (c == '\r') {
            PEEKC_EOB(c, p);
            if (c != '\n') {
                if (字符串)
                    动态字符串_追加一个字符(字符串, '\r');
            } else {
                文件->当前行号++;
                goto 动态字符串_增加字符;
            }
        } else {
        动态字符串_增加字符:
            if (字符串)
                动态字符串_追加一个字符(字符串, c);
            p++;
        }
    }
    p++;
    return p;
}

/* 跳过 块 of 代码 until #else, #elif or #endif. 跳过 also pairs of
   #if/#endif */
static void 预处理_跳过(void)
{
    int a, start_of_line, c, in_warn_or_error;
    uint8_t *p;

    p = 文件->缓冲源码串;
    a = 0;
redo_start:
    start_of_line = 1;
    in_warn_or_error = 0;
    for(;;) {
    重做_未开始:
        c = *p;
        switch(c) {
        case ' ':
        case '\t':
        case '\f':
        case '\v':
        case '\r':
            p++;
            goto 重做_未开始;
        case '\n':
            文件->当前行号++;
            p++;
            goto redo_start;
        case '\\':
            文件->缓冲源码串 = p;
            c = 处理块结尾();
            if (c == 中_文件结尾) {
                应为("#endif");
            } else if (c == '\\') {
                当前字符 = 文件->缓冲源码串[0];
                处理转义符_无错误提示();
            }
            p = 文件->缓冲源码串;
            goto 重做_未开始;
        /* 跳过 strings */
        case '\"':
        case '\'':
            if (in_warn_or_error)
                goto _default;
            p = 解析字符串而不解释转义(p, c, NULL);
            break;
        /* 跳过 comments */
        case '/':
            if (in_warn_or_error)
                goto _default;
            文件->缓冲源码串 = p;
            当前字符 = *p;
            取下个字符且处理转义();
            p = 文件->缓冲源码串;
            if (当前字符 == '*') {
                p = 解析注释(p);
            } else if (当前字符 == '/') {
                p = 解析单行注释(p);
            }
            break;
        case '#':
            p++;
            if (start_of_line) {
                文件->缓冲源码串 = p;
                取下个单词不宏替换();
                p = 文件->缓冲源码串;
                if (a == 0 && 
                    (单词编码 == 中_否则 || 单词编码 == 英_否则 || 单词编码 == 英_否则如果 || 单词编码 == 中_否则如果 || 单词编码 == 英_结束如果 || 单词编码 == 中_结束如果))
                    goto 结_束;
                if (单词编码 == 中_如果 || 单词编码 == 英_如果 || 单词编码 == 英_如果已定义 || 单词编码 == 中_如果已定义 || 单词编码 == 英_如果未定义 || 单词编码 == 中_如果未定义)
                    a++;
                else if (单词编码 == 英_结束如果 || 单词编码 == 中_结束如果)
                    a--;
                else if( 单词编码 == 英_错误 ||单词编码 == 中_错误 || 单词编码 == 英_警告 || 单词编码 == 中_警告)
                    in_warn_or_error = 1;
                else if (单词编码 == 英_换行)
                    goto redo_start;
                else if (解析标志 & 解析标志_汇编文件)
                    p = 解析单行注释(p - 1);
            }
#if !defined(目标_ARM)
            else if (解析标志 & 解析标志_汇编文件)
                p = 解析单行注释(p - 1);
#else
            /* ARM 程序集使用“#”作为常量 */
#endif
            break;
_default:
        default:
            p++;
            break;
        }
        start_of_line = 0;
    }
 结_束: ;
    文件->缓冲源码串 = p;
}
ST_INLN void 单词字符串_新建(单词字符串ST *s)
{
    s->字符串 = NULL;
    s->长度 = s->lastlen = 0;
    s->allocated_len = 0;
    s->最后_行号 = -1;
}

静态函数 单词字符串ST *单词字符串_分配(void)
{
    单词字符串ST *字符串 = 小分配器_realloc(单词字符串分配器, 0, sizeof *字符串);
    单词字符串_新建(字符串);
    return 字符串;
}

静态函数 int *单词字符串_复制(单词字符串ST *s)
{
    int *字符串;

    字符串 = 小分配器_realloc(单词字符串分配器, 0, s->长度 * sizeof(int));
    memcpy(字符串, s->字符串, s->长度 * sizeof(int));
    return 字符串;
}

静态函数 void 单词字符串_释放_字符串(int *字符串)
{
    小分配器_free(单词字符串分配器, 字符串);
}

静态函数 void 单词字符串_释放(单词字符串ST *字符串)
{
    单词字符串_释放_字符串(字符串->字符串);
    小分配器_free(单词字符串分配器, 字符串);
}

静态函数 int *单词字符串_重新分配(单词字符串ST *s, int new_size)
{
    int *字符串, 大小;

    大小 = s->allocated_len;
    if (大小 < 16)
        大小 = 16;
    while (大小 < new_size)
        大小 = 大小 * 2;
    if (大小 > s->allocated_len) {
        字符串 = 小分配器_realloc(单词字符串分配器, s->字符串, 大小 * sizeof(int));
        s->allocated_len = 大小;
        s->字符串 = 字符串;
    }
    return s->字符串;
}

静态函数 void 单词字符串_增加(单词字符串ST *s, int t)
{
    int 长度, *字符串;

    长度 = s->长度;
    字符串 = s->字符串;
    if (长度 >= s->allocated_len)
        字符串 = 单词字符串_重新分配(s, 长度 + 1);
    字符串[长度++] = t;
    s->长度 = 长度;
}

静态函数 void 开始_宏扩展(单词字符串ST *字符串, int alloc)
{
    字符串->alloc = alloc;
    字符串->上个 = 宏_栈;
    字符串->prev_ptr = 没宏替换的字符串指针;
    字符串->save_line_num = 文件->当前行号;
    没宏替换的字符串指针 = 字符串->字符串;
    宏_栈 = 字符串;
}

静态函数 void 结束_宏扩展(void)
{
    单词字符串ST *字符串 = 宏_栈;
    宏_栈 = 字符串->上个;
    没宏替换的字符串指针 = 字符串->prev_ptr;
    文件->当前行号 = 字符串->save_line_num;
    if (字符串->alloc != 0) {
        if (字符串->alloc == 2)
            字符串->字符串 = NULL; /* don't free */
        单词字符串_释放(字符串);
    }
}

static void 单词字符串_增加2(单词字符串ST *s, int t, 常量值U *cv)
{
    int 长度, *字符串;

    长度 = s->lastlen = s->长度;
    字符串 = s->字符串;

    /* allocate space for worst case */
    if (长度 + 单词最大大小M >= s->allocated_len)
        字符串 = 单词字符串_重新分配(s, 长度 + 单词最大大小M + 1);
    字符串[长度++] = t;
    switch(t) {
    case TOK_整数常量:
    case TOK_无符号整数常量:
    case TOK_字符常量:
    case TOK_LCHAR:
    case TOK_浮点常量:
    case TOK_行号:
#if 长整数大小 == 4
    case TOK_长整数常量:
    case TOK_无符号长整数常量:
#endif
        字符串[长度++] = cv->tab[0];
        break;
    case TOK_预处理数字:
    case TOK_预处理字符串:
    case TOK_字符串常量:
    case TOK_LSTR:
        {
            /* Insert the string into the int array. */
            size_t nb_words =
                1 + (cv->字符串.大小 + sizeof(int) - 1) / sizeof(int);
            if (长度 + nb_words >= s->allocated_len)
                字符串 = 单词字符串_重新分配(s, 长度 + nb_words + 1);
            字符串[长度] = cv->字符串.大小;
            memcpy(&字符串[长度 + 1], cv->字符串.数据, cv->字符串.大小);
            长度 += nb_words;
        }
        break;
    case TOK_双精度常量:
    case TOK_长长整数常量:
    case TOK_无符号长长整数常量:
#if 长整数大小 == 8
    case TOK_长整数常量:
    case TOK_无符号长整数常量:
#endif
#if 长双精度大小 == 8
    case TOK_长双精度常量:
#endif
        字符串[长度++] = cv->tab[0];
        字符串[长度++] = cv->tab[1];
        break;
#if 长双精度大小 == 12
    case TOK_长双精度常量:
        字符串[长度++] = cv->tab[0];
        字符串[长度++] = cv->tab[1];
        字符串[长度++] = cv->tab[2];
#elif 长双精度大小 == 16
    case TOK_长双精度常量:
        字符串[长度++] = cv->tab[0];
        字符串[长度++] = cv->tab[1];
        字符串[长度++] = cv->tab[2];
        字符串[长度++] = cv->tab[3];
#elif 长双精度大小 != 8
#error add long double 大小 support
#endif
        break;
    default:
        break;
    }
    s->长度 = 长度;
}

静态函数 void 在单词字符串s中添加当前解析单词(单词字符串ST *s)
{
    常量值U cval;

    /* save line number info */
    if (文件->当前行号 != s->最后_行号) {
        s->最后_行号 = 文件->当前行号;
        cval.i = s->最后_行号;
        单词字符串_增加2(s, TOK_行号, &cval);
    }
    单词字符串_增加2(s, 单词编码, &单词常量);
}

static inline void 从整数数组中获取标记并递增指针(int *t, const int **pp, 常量值U *cv)
{
    const int *p = *pp;
    int n, *tab;

    tab = cv->tab;
    switch(*t = *p++) {
#if 长整数大小 == 4
    case TOK_长整数常量:
#endif
    case TOK_整数常量:
    case TOK_字符常量:
    case TOK_LCHAR:
    case TOK_行号:
        cv->i = *p++;
        break;
#if 长整数大小 == 4
    case TOK_无符号长整数常量:
#endif
    case TOK_无符号整数常量:
        cv->i = (unsigned)*p++;
        break;
    case TOK_浮点常量:
	tab[0] = *p++;
	break;
    case TOK_字符串常量:
    case TOK_LSTR:
    case TOK_预处理数字:
    case TOK_预处理字符串:
        cv->字符串.大小 = *p++;
        cv->字符串.数据 = p;
        p += (cv->字符串.大小 + sizeof(int) - 1) / sizeof(int);
        break;
    case TOK_双精度常量:
    case TOK_长长整数常量:
    case TOK_无符号长长整数常量:
#if 长整数大小 == 8
    case TOK_长整数常量:
    case TOK_无符号长整数常量:
#endif
        n = 2;
        goto copy;
    case TOK_长双精度常量:
#if 长双精度大小 == 16
        n = 4;
#elif 长双精度大小 == 12
        n = 3;
#elif 长双精度大小 == 8
        n = 2;
#else
# error add long double 大小 support
#endif
    copy:
        do
            *tab++ = *p++;
        while (--n);
        break;
    default:
        break;
    }
    *pp = p;
}
# define TOK_GET(t,p,c) do { \
    int _t = **(p); \
    if (TOK_哈希_值(_t)) \
        从整数数组中获取标记并递增指针(t, p, c); \
    else \
        *(t) = _t, ++*(p); \
    } while (0)


static int macro_is_equal(const int *a, const int *b)
{
    常量值U cv;
    int t;

    if (!a || !b)
    {
    	return 1;
    }
    while (*a && *b)
    {
        /* 第一次预分配宏_等于_缓冲，下次只重置位置开始 */
        动态字符串_重置(&宏_等于_缓冲);
        TOK_GET(&t, &a, &cv);
        动态字符串_拼接(&宏_等于_缓冲, 取单词字符串(t, &cv), 0);
        TOK_GET(&t, &b, &cv);
        if (strcmp(宏_等于_缓冲.数据, 取单词字符串(t, &cv)))
        {
        	return 0;
        }
    }
    return !(*a || *b);
}

/* 定义处理 */
ST_INLN void 推进define(int v, int macro_type, int *字符串, 符号ST *first_arg)
{
    符号ST *s, *o;

    o = 查找define(v);
    s = 将符号放入符号栈2(&define_栈, v, macro_type, 0);
    s->d = 字符串;
    s->next = first_arg;
    标识符表[v - TOK_标识符]->define符号 = s;

    if (o && !macro_is_equal(o->d, s->d))
	zhi_警告("%s 重复定义", 取单词字符串(v, NULL));
}

/* 未定义一个定义符号。 它的名字只是设置为零 */
静态函数 void define_undef(符号ST *s)
{
    int v = s->v;
    if (v >= TOK_标识符 && v < 单词_标识符)
        标识符表[v - TOK_标识符]->define符号 = NULL;
}

ST_INLN 符号ST *查找define(int v)
{
    v -= TOK_标识符;
    if ((unsigned)v >= (unsigned)(单词_标识符 - TOK_标识符))
        return NULL;
    return 标识符表[v]->define符号;
}

/* free define stack until top reaches 'b' */
静态函数 void 释放define(符号ST *b)
{
    while (define_栈 != b) {
        符号ST *top = define_栈;
        define_栈 = top->上个;
        单词字符串_释放_字符串(top->d);
        define_undef(top);
        sym_free(top);
    }
}

/* label lookup */
静态函数 符号ST *查找标签(int v)
{
    v -= TOK_标识符;
    if ((unsigned)v >= (unsigned)(单词_标识符 - TOK_标识符))
        return NULL;
    return 标识符表[v]->label符号;
}

静态函数 符号ST *标签推送(符号ST **ptop, int v, int flags)
{
    符号ST *s, **ps;
    s = 将符号放入符号栈2(ptop, v, 0, 0);
    s->寄存qi = flags;
    ps = &标识符表[v - TOK_标识符]->label符号;
    if (ptop == &全局_标签_栈) {
        /* modify the top most local identifier, so that
           标识符符号 will point to 's' when popped */
        while (*ps != NULL)
            ps = &(*ps)->定义的前一个同名符号;
    }
    s->定义的前一个同名符号 = *ps;
    *ps = s;
    return s;
}

/* 弹出标签，直到到达最后一个元素。查看是否有未定义的标签。如果使用了“&&label”，则定义符号。 */
静态函数 void 弹出标签(符号ST **ptop, 符号ST *slast, int keep)
{
    符号ST *s, *s1;
    for(s = *ptop; s != slast; s = s1)
    {
        s1 = s->上个;
        if (s->寄存qi == 标签_已声明)
        {
            zhi_警告_c(警告_所有)("标签 '%s' 已声明但未使用", 取单词字符串(s->v, NULL));
        } else if (s->寄存qi == 标签_向前的)
        {
        	错_误("标签 '%s' 已使用但未定义",取单词字符串(s->v, NULL));
        } else
        {
            if (s->c)
            {
                /* 定义相应的符号。 1的尺寸被放. */
                取外部符号(s, 当前代码节, s->jnext, 1);
            }
        }
        /* 移除标签 */
        if (s->寄存qi != 标签_GONE)
            标识符表[s->v - TOK_标识符]->label符号 = s->定义的前一个同名符号;
        if (!keep)
            sym_free(s);
        else
            s->寄存qi = 标签_GONE;
    }
    if (!keep)
        *ptop = slast;
}

/* 为 zhi -dt -run 伪造第 n 个“#if defined test_...” */
static void maybe_run_test(虚拟机ST *s)
{
    const char *p;
    if (s->include栈指针 != s->include栈)
        return;
    p = 取单词字符串(单词编码, NULL);
    if (0 != memcmp(p, "test_", 5))
        return;
    if (0 != --s->运行_测试)
        return;
    fprintf(s->标准的输出文件, &"\n[%s]\n"[!(s->dflag & 32)], p), fflush(s->标准的输出文件);
    推进define(单词编码, MACRO_OBJ, NULL, NULL);
}

/* 评估 #if/#elif 的表达式 */
static int 表达式_预处理(void)
{
    int c, t;
    单词字符串ST *字符串;
    
    字符串 = 单词字符串_分配();
    pp_expr = 1;
    while (单词编码 != 英_换行 && 单词编码 != TOK_文件结尾) {
        取下个单词(); /* do macro subst */
      重做:
        if (单词编码 == 英_已定义 || 单词编码 == 中_已定义) {
            取下个单词不宏替换();
            t = 单词编码;
            if (t == 英_左小括号) 
                取下个单词不宏替换();
            if (单词编码 < TOK_标识符)
                应为("identifier");
            if (全局虚拟机->运行_测试)
                maybe_run_test(全局虚拟机);
            c = 查找define(单词编码) != 0;
            if (t == 英_左小括号) {
                取下个单词不宏替换();
                if (单词编码 != 英_右小括号)
                    应为("英_右小括号");
            }
            单词编码 = TOK_整数常量;
            单词常量.i = c;
        } else if (1 && 单词编码 == TOK___HAS_INCLUDE) {
            取下个单词();  /* XXX check if correct to use expansion */
            跳过(英_左小括号);
            while (单词编码 != 英_右小括号 && 单词编码 != TOK_文件结尾)
              取下个单词();
            if (单词编码 != 英_右小括号)
              应为("英_右小括号");
            单词编码 = TOK_整数常量;
            单词常量.i = 0;
        } else if (单词编码 >= TOK_标识符) {
            /* 如果未定义宏，用零替换，检查类似函数 */
            t = 单词编码;
            单词编码 = TOK_整数常量;
            单词常量.i = 0;
            在单词字符串s中添加当前解析单词(字符串);
            取下个单词();
            if (单词编码 == 英_左小括号)
                错_误("类似函数宏 '%s' 未定义",取单词字符串(t, NULL));
            goto 重做;
        }
        在单词字符串s中添加当前解析单词(字符串);
    }
    pp_expr = 0;
    单词字符串_增加(字符串, -1); /* simulate end of 文件 */
    单词字符串_增加(字符串, 0);
    /* now evaluate C constant expression */
    开始_宏扩展(字符串, 1);
    取下个单词();
    c = 常量表达式解析();
    结束_宏扩展();
    return c != 0;
}


/* 在#define之后解析 */
静态函数 void 解析_define(void)
{
    符号ST *s, *first, **ps;
    int v, t, varg, is_vaargs, spc;
    int 保存_解析标志 = 解析标志;

    v = 单词编码;
    if (v < TOK_标识符 || v == 英_已定义 || v == 中_已定义)
        错_误("无效的宏名称 '%s'", 取单词字符串(单词编码, &单词常量));
    /* XXX: 应该检查是否有相同的宏（ANSI） */
    first = NULL;
    t = MACRO_OBJ;
    /* 我们必须像不在asm模式下一样解析整个定义，特别是不能忽略带有“#”的行注释。此外，对于函数宏，必须在分析参数列表时不使用“”是一个ID字符。  */
    解析标志 = ((解析标志 & ~解析标志_汇编文件) | 解析标志_空格);
    /* 英_左小括号必须刚好在macro_FUNC的宏定义之后 */
    取下个单词不宏替换();
    解析标志 &= ~解析标志_空格;
    if (单词编码 == 英_左小括号) {
        int dotid = 设置标识符('.', 0);
        取下个单词不宏替换();
        ps = &first;
        if (单词编码 != 英_右小括号) for (;;) {
            varg = 单词编码;
            取下个单词不宏替换();
            is_vaargs = 0;
            if (varg == TOK_三个点) {
                varg = 英_可变参数;
                is_vaargs = 1;
            } else if (单词编码 == TOK_三个点 && GNU_C_扩展) {
                is_vaargs = 1;
                取下个单词不宏替换();
            }
            if (varg < TOK_标识符)
        bad_list:
                错_误("错误的宏参数列表");
            s = 将符号放入符号栈2(&define_栈, varg | 符号_字段, is_vaargs, 0);
            *ps = s;
            ps = &s->next;
            if (单词编码 == 英_右小括号)
                break;
            if (单词编码 != 英_逗号 || is_vaargs)
                goto bad_list;
            取下个单词不宏替换();
        }
        解析标志 |= 解析标志_空格;
        取下个单词不宏替换();
        t = MACRO_FUNC;
        设置标识符('.', dotid);
    }

    单词字符串_缓冲.长度 = 0;
    spc = 2;
    解析标志 |= 解析标志_转义 | 解析标志_空格 | 解析标志_换行;
    /* 应解析宏定义的主体，以便像文件模式确定的那样解析标识符（即，在asm模式中，“.”是ID字符）。但应该保留“#”，而不是将其视为行注释前导，所以仍然不要在解析令牌中设置ASM_FILE。 */
    while (单词编码 != 英_换行 && 单词编码 != TOK_文件结尾)
    {
        /* 删除##周围和“#”之后的空格 */
        if (TOK_双井号 == 单词编码)
        {
            if (2 == spc)
                goto bad_twosharp;
            if (1 == spc)
                --单词字符串_缓冲.长度;
            spc = 3;
	    单词编码 = TOK_PPJOIN;
        } else if ('#' == 单词编码)
        {
            spc = 4;
        } else if (检查空格(单词编码, &spc))
        {
            goto 跳过;
        }
        单词字符串_增加2(&单词字符串_缓冲, 单词编码, &单词常量);
    跳过:
        取下个单词不宏替换();
    }

    解析标志 = 保存_解析标志;
    if (spc == 1)
        --单词字符串_缓冲.长度; /* 删除尾部空格 */
    单词字符串_增加(&单词字符串_缓冲, 0);
    if (3 == spc)
bad_twosharp:
        错_误("'##'不能出现在宏的任一端");
    推进define(v, t, 单词字符串_复制(&单词字符串_缓冲), first);
}

static 导入文件缓存ST *搜索include缓存(虚拟机ST *s1, const char *文件名, int add)
{
    const unsigned char *s;
    unsigned int h;
    导入文件缓存ST *e;
    int i;

    h = 单词哈希初始值;
    s = (unsigned char *) 文件名;
    while (*s) {
#ifdef _WIN32
        h = 计算哈希地址(h, toup(*s));
#else
        h = 计算哈希地址(h, *s);
#endif
        s++;
    }
    h &= (CACHED_INCLUDES_HASH_SIZE - 1);

    i = s1->包含在ifndef宏中的文件[h];
    for(;;) {
        if (i == 0)
            break;
        e = s1->包含的缓存文件[i - 1];
        if (0 == PATHCMP(e->文件名, 文件名))
            return e;
        i = e->哈希冲突的其他单词;
    }
    if (!add)
        return NULL;

    e = zhi_分配内存(sizeof(导入文件缓存ST) + strlen(文件名));
    strcpy(e->文件名, 文件名);
    e->ifndef宏 = e->once = 0;
    动态数组_追加元素(&s1->包含的缓存文件, &s1->包含的缓存文件数量, e);
    /* add in hash table */
    e->哈希冲突的其他单词 = s1->包含在ifndef宏中的文件[h];
    s1->包含在ifndef宏中的文件[h] = s1->包含的缓存文件数量;
#ifdef INCLUDE调试
    printf("adding cached '%s'\n", 文件名);
#endif
    return e;
}

static void 解析pragma(虚拟机ST *s1)
{
    取下个单词不宏替换();
    if (单词编码 == TOK_push_macro || 单词编码 == TOK_pop_macro) {
        int t = 单词编码, v;
        符号ST *s;

        if (取下个单词(), 单词编码 != 英_左小括号)
            goto pragma_err;
        if (取下个单词(), 单词编码 != TOK_字符串常量)
            goto pragma_err;
        v = 插入单词(单词常量.字符串.数据, 单词常量.字符串.大小 - 1)->单词编码;
        if (取下个单词(), 单词编码 != 英_右小括号)
            goto pragma_err;
        if (t == TOK_push_macro) {
            while (NULL == (s = 查找define(v)))
                推进define(v, 0, NULL, NULL);
            s->类型.引用符号 = s; /* set push boundary */
        } else {
            for (s = define_栈; s; s = s->上个)
                if (s->v == v && s->类型.引用符号 == s) {
                    s->类型.引用符号 = NULL;
                    break;
                }
        }
        if (s)
            标识符表[v - TOK_标识符]->define符号 = s->d ? s : NULL;
        else
            zhi_警告("不平衡#pragma pop_macro");
        pp_debug_tok = t, pp_debug_symv = v;

    } else if (单词编码 == TOK_once) {
        搜索include缓存(s1, 文件->文件名, 1)->once = pp_once;

    } else if (s1->输出类型 == 输出_预处理) {
        /* zhi -E: keep pragmas below unchanged */
        退回当前单词(' ');
        if(单词编码=英_指示)
        {
        	退回当前单词(英_指示);
        }else if (单词编码=中_指示)
        {
        	退回当前单词(中_指示);
        }

        退回当前单词('#');
        退回当前单词(英_换行);

    } else if (单词编码 == TOK_pack) {
        /* This may be:
           #pragma pack(1) // set
           #pragma pack() // reset to default
           #pragma pack(push) // push current
           #pragma pack(push,1) // push & set
           #pragma pack(pop) // restore previous */
        取下个单词();
        跳过(英_左小括号);
        if (单词编码 == TOK_ASM_pop) {
            取下个单词();
            if (s1->包_栈_ptr <= s1->包_栈) {
            stk_error:
                错_误("包外的堆栈");
            }
            s1->包_栈_ptr--;
        } else {
            int val = 0;
            if (单词编码 != 英_右小括号)
            {
                if (单词编码 == TOK_ASM_push)
                {
                    取下个单词();
                    if (s1->包_栈_ptr >= s1->包_栈 + 包栈大小M - 1)
                        goto stk_error;
//                    s1->包_栈_ptr++;
//                    跳过(英_逗号);
                      val = *s1->包_栈_ptr++;
                      if (单词编码 != 英_逗号)
                           goto pack_set;
                      取下个单词();
                }
                if (单词编码 != TOK_整数常量)
                    goto pragma_err;
                val = 单词常量.i;
                if (val < 1 || val > 16 || (val & (val - 1)) != 0)
                    goto pragma_err;
                取下个单词();
            }
        pack_set:
            *s1->包_栈_ptr = val;
        }
        if (单词编码 != 英_右小括号)
            goto pragma_err;

    } else if (单词编码 == 英_注解 || 单词编码 == 中_注解) {
        char *p; int t;
        取下个单词();
        跳过(英_左小括号);
        t = 单词编码;
        取下个单词();
        跳过(英_逗号);
        if (单词编码 != TOK_字符串常量)
            goto pragma_err;
        p = 字符串增加一个宽度((char *)单词常量.字符串.数据);
        取下个单词();
        if (单词编码 != 英_右小括号)
            goto pragma_err;
        if (t == TOK_lib) {
            动态数组_追加元素(&s1->pragma_库数组, &s1->pragma_库数量, p);
        } else {
            if (t == TOK_option)
                设置选项(s1, p);
            zhi_释放(p);
        }

    } else
        zhi_警告_c(警告_不支持)("#pragma %s 忽略", 取单词字符串(单词编码, &单词常量));
    return;

pragma_err:
    错_误("异常（格式错误的） #pragma 指令");
    return;
}

/* 如果文件开头的第一个非空格标记“是_文件开头”为真 */
静态函数 void 预处理(int 是_文件开头)
{
    虚拟机ST *s1 = 全局虚拟机;
    int i, c, n, 保存_解析标志;
    char buf[1024], *q;
    符号ST *s;

    保存_解析标志 = 解析标志;
    解析标志 = 解析标志_预处理
        | 解析标志_数字
        | 解析标志_单词字符串
        | 解析标志_换行
        | (解析标志 & 解析标志_汇编文件)
        ;

    取下个单词不宏替换();
 重做:
    switch(单词编码) {
    case 英_定义:
    case 中_定义:
        pp_debug_tok = 单词编码;
        取下个单词不宏替换();
        pp_debug_symv = 单词编码;
        解析_define();
        break;
    case 英_结束定义:
    case 中_结束定义:
        pp_debug_tok = 单词编码;
        取下个单词不宏替换();
        pp_debug_symv = 单词编码;
        s = 查找define(单词编码);
        /* undefine symbol by putting an invalid name */
        if (s)
            define_undef(s);
        break;
    case 英_导入:
    case 中_导入:
    case 英_导入下一个:
    case 中_导入下一个:
        当前字符 = 文件->缓冲源码串[0];
        /* XXX: incorrect if comments : use 取下个单词不宏替换 with a special mode */
        跳过空格();
        if (当前字符 == '<') {
            c = '>';
            goto read_name;
        } else if (当前字符 == '\"') {
            c = 当前字符;
        read_name:
            取下个字符();
            q = buf;
            while (当前字符 != c && 当前字符 != '\n' && 当前字符 != 中_文件结尾) {
                if ((q - buf) < sizeof(buf) - 1)
                    *q++ = 当前字符;
                if (当前字符 == '\\') {
                    if (处理转义符_无错误提示() == 0)
                        --q;
                } else
                    取下个字符();
            }
            *q = '\0';
            取下个字符且处理转义();
        } else {
	    int 长度;
            /* 计算的#include：将所有内容连接到换行，结果必须是两个接受的表单之一。不要在此处将词法单元转换为单词。  */
	    解析标志 = (解析标志_预处理
			   | 解析标志_换行
			   | (解析标志 & 解析标志_汇编文件));
            取下个单词();
            buf[0] = '\0';
	    while (单词编码 != 英_换行) {
		截取后拼接(buf, sizeof(buf), 取单词字符串(单词编码, &单词常量));
		取下个单词();
	    }
	    长度 = strlen(buf);
	    /* check syntax and remove '<>|""' */
	    if ((长度 < 2 || ((buf[0] != '"' || buf[长度-1] != '"') &&
			     (buf[0] != '<' || buf[长度-1] != '>'))))
	        错_误("'#include'应为\"文件\" 或 <文件>");
	    c = buf[长度-1];
	    memmove(buf, buf + 1, 长度 - 2);
	    buf[长度 - 2] = '\0';
        }

        if (s1->include栈指针 >= s1->include栈 + INCLUDE_栈_大小)
            错_误("#include 递归太深");
        i = (单词编码 == 英_导入下一个 || 单词编码 == 中_导入下一个) ? 文件->下一个包含文件的索引 + 1 : 0;
        n = 2 + s1->导入路径数量 + s1->系统导入路径数量;
        for (; i < n; ++i) {
            char buf1[sizeof 文件->文件名];
            导入文件缓存ST *e;
            const char *path;

            if (i == 0) {
                /* 检查绝对包含路径 */
                if (!IS_ABSPATH(buf))
                    continue;
                buf1[0] = 0;

            } else if (i == 1)
            {
                /* 如果“header.h”，则在文件的目录中搜索 */
                if (c != '\"')
                    continue;
                /* https://savannah.nongnu.org/bugs/index.php?50847 */
                path = 文件->真文件名;
                复制指定字节数字符串(buf1, path, 取文件名含扩展名(path) - path);

            } else {
                /* 在所有包含路径中搜索 */
                int j = i - 2, k = j - s1->导入路径数量;
                path = k < 0 ? s1->导入路径数组[j] : s1->系统导入路径数组[k];
                截取前n个字符(buf1, sizeof(buf1), path);
                截取后拼接(buf1, sizeof(buf1), "/");
            }

            截取后拼接(buf1, sizeof(buf1), buf);
            e = 搜索include缓存(s1, buf1, 0);
            if (e && (查找define(e->ifndef宏) || e->once == pp_once)) {
                /* 无需解析包含，因为定义了“ifndef 宏”（或有 #pragma 一次） */
#ifdef INCLUDE调试
                printf("%s: 跳过缓存的 %s\n", 文件->文件名, buf1);
#endif
                goto include_done;
            }

            if (zhi_打开一个新文件(s1, buf1) < 0)
            {
            	continue;
            }
            /* 将上一个文件压入堆栈 */
            *s1->include栈指针++ = 文件->上个;
            文件->下一个包含文件的索引 = i;
#ifdef INCLUDE调试
            printf("%s: 包含 %s\n", 文件->上个->文件名, 文件->文件名);
#endif
            /* 更新目标依赖 */
            if (s1->生成_依赖)
            {
                缓冲文件ST *缓冲文件 = 文件;
                while (i == 1 && (缓冲文件 = 缓冲文件->上个))
                    i = 缓冲文件->下一个包含文件的索引;
                /* 跳过系统包含文件 */
                if (s1->包含的系统依赖 || n - i > s1->系统导入路径数量)
                    动态数组_追加元素(&s1->目标_依赖项数组, &s1->目标_依赖项数量 , 字符串增加一个宽度(buf1));
            }
            /* add include 文件 debug info */
            zhi_调试_include的开始(全局虚拟机);
            单词标志 |= 单词标志_文件开始之前 | 单词标志_行开始之前;
            当前字符 = 文件->缓冲源码串[0];
            goto 结_束;
        }
        错_误("未找到包含文件“%s”", buf);
include_done:
        break;
    case 英_如果未定义:
    case 中_如果未定义:
        c = 1;
        goto 执行_ifdef;
    case 英_如果:
    case 中_如果:
        c = 表达式_预处理();
        goto 执行_如果;
    case 英_如果已定义:
    case 中_如果已定义:
        c = 0;
    执行_ifdef:
        取下个单词不宏替换();
        if (单词编码 < TOK_标识符)
            错_误(" '#if%sdef'的无效参数", c ? "n" : "");
        if (是_文件开头)
        {
            if (c)
            {
#ifdef INCLUDE调试
                printf("#ifndef %s\n", 取单词字符串(单词编码, NULL));
#endif
                文件->ifndef宏 = 单词编码;
            }
        }
        c = (查找define(单词编码) != 0) ^ c;
    执行_如果:
        if (s1->ifdef_栈_ptr >= s1->ifdef_栈 + IFDEF_栈_大小)
            错_误("内存已满 (ifdef)");
        *s1->ifdef_栈_ptr++ = c;
        goto 测试_跳过;
    case 英_否则:
    case 中_否则:
        if (s1->ifdef_栈_ptr == s1->ifdef_栈)
            错_误("#else 不匹配#if");
        if (s1->ifdef_栈_ptr[-1] & 2)
            错_误("#else 之后 #else");
        c = (s1->ifdef_栈_ptr[-1] ^= 3);
        goto test_else;
    case 英_否则如果:
    case 中_否则如果:
        if (s1->ifdef_栈_ptr == s1->ifdef_栈)
            错_误("#elif 没有匹配 #if");
        c = s1->ifdef_栈_ptr[-1];
        if (c > 1)
            错_误("#elif 在  #else 之后");
        /* 最后一个#if/#elif 表达式为真：我们跳过 */
        if (c == 1)
        {
            c = 0;
        } else
        {
            c = 表达式_预处理();
            s1->ifdef_栈_ptr[-1] = c;
        }
    test_else:
        if (s1->ifdef_栈_ptr == 文件->ifdef_栈_ptr + 1)
            文件->ifndef宏 = 0;
    测试_跳过:
        if (!(c & 1)) {
            预处理_跳过();
            是_文件开头 = 0;
            goto 重做;
        }
        break;
    case 英_结束如果:
    case 中_结束如果:
        if (s1->ifdef_栈_ptr <= 文件->ifdef_栈_ptr)
            错_误("#endif 没有匹配  #if");
        s1->ifdef_栈_ptr--;
        /* '#ifndef macro' was at the start of 文件. Now we check if
           an '#endif' is exactly at the end of 文件 */
        if (文件->ifndef宏 &&
            s1->ifdef_栈_ptr == 文件->ifdef_栈_ptr) {
            文件->保存的ifndef宏 = 文件->ifndef宏;
            /* need to set to zero to avoid false matches if another
               #ifndef at middle of 文件 */
            文件->ifndef宏 = 0;
            while (单词编码 != 英_换行)
                取下个单词不宏替换();
            单词标志 |= 单词标志_ENDIF;
            goto 结_束;
        }
        break;
    case TOK_预处理数字:
        n = strtoul((char*)单词常量.字符串.数据, &q, 10);
        goto _line_num;
    case 英_行号:
    case 中_行号:
        取下个单词();
        if (单词编码 != TOK_整数常量)
    _line_err:
            错_误(" #line 格式错误");
        n = 单词常量.i;
    _line_num:
        取下个单词();
        if (单词编码 != 英_换行) {
            if (单词编码 == TOK_字符串常量) {
                if (文件->真文件名 == 文件->文件名)
                    文件->真文件名 = 字符串增加一个宽度(文件->文件名);
                /* 从实际文件预编文件目录 */
                截取前n个字符(buf, sizeof buf, 文件->真文件名);
                *取文件名含扩展名(buf) = 0;
                截取后拼接(buf, sizeof buf, (char *)单词常量.字符串.数据);
                zhi_debug_putfile(s1, buf);
            } else if (解析标志 & 解析标志_汇编文件)
                break;
            else
                goto _line_err;
            --n;
        }
        if (文件->文件描述 > 0)
            总行数 += 文件->当前行号 - n;
        文件->当前行号 = n;
        break;
    case 英_错误:
    case 中_错误:
    case 英_警告:
    case 中_警告:
        c = 单词编码;
        当前字符 = 文件->缓冲源码串[0];
        跳过空格();
        q = buf;
        while (当前字符 != '\n' && 当前字符 != 中_文件结尾) {
            if ((q - buf) < sizeof(buf) - 1)
                *q++ = 当前字符;
            if (当前字符 == '\\') {
                if (处理转义符_无错误提示() == 0)
                    --q;
            } else
                取下个字符();
        }
        *q = '\0';
        if (c == 英_错误 || c == 中_错误)
            错_误("#error %s", buf);
        else
            zhi_警告("#warning %s", buf);
        break;
    case 英_指示:
    case 中_指示:
        解析pragma(s1);
        break;
    case 英_换行:
        goto 结_束;
    default:
        /* 忽略“S”文件中的气体行注释。 */
        if (保存_解析标志 & 解析标志_汇编文件)
            goto ignore;
        if (单词编码 == 英_叹号 && 是_文件开头)
            /* 英_叹号 is ignored at beginning to allow C scripts. */
            goto ignore;
        zhi_警告("忽略未知的预处理指令: #%s", 取单词字符串(单词编码, &单词常量));
    ignore:
        文件->缓冲源码串 = 解析单行注释(文件->缓冲源码串 - 1);
        goto 结_束;
    }
    /* ignore other 预处理 commands or #! for C scripts */
    while (单词编码 != 英_换行)
        取下个单词不宏替换();
 结_束:
    解析标志 = 保存_解析标志;
}

/* 评估字符串中的转义码. */
static void 解析转义字符串(动态字符串ST *outstr, const uint8_t *buf, int is_long)
{
    int c, n, i;
    const uint8_t *p;

    p = buf;
    for(;;) {
        c = *p;
        if (c == '\0')
            break;
        if (c == '\\') {
            p++;
            /* escape */
            c = *p;
            switch(c) {
            case '0': case '1': case '2': case '3':
            case '4': case '5': case '6': case '7':
                /* 最多三个八进制数字 */
                n = c - '0';
                p++;
                c = *p;
                if (isoct(c)) {
                    n = n * 8 + c - '0';
                    p++;
                    c = *p;
                    if (isoct(c)) {
                        n = n * 8 + c - '0';
                        p++;
                    }
                }
                c = n;
                goto add_char_nonext;
            case 'x': i = 0; goto parse_hex_or_ucn;
            case 'u': i = 4; goto parse_hex_or_ucn;
            case 'U': i = 8; goto parse_hex_or_ucn;
    parse_hex_or_ucn:
                p++;
                n = 0;
                do {
                    c = *p;
                    if (c >= 'a' && c <= 'f')
                        c = c - 'a' + 10;
                    else if (c >= 'A' && c <= 'F')
                        c = c - 'A' + 10;
                    else if (是数字(c))
                        c = c - '0';
                    else if (i > 0)
                        应为("通用字符名称中的更多十六进制数字");
                    else {
                        c = n;
                        goto add_char_nonext;
                    }
                    n = n * 16 + c;
                    p++;
                } while (--i);
                动态字符串_添加一个扩展为utf8的unicode字符(outstr, n);
                continue;
            case 'a':
                c = '\a';
                break;
            case 'b':
                c = '\b';
                break;
            case 'f':
                c = '\f';
                break;
            case 'n':
                c = '\n';
                break;
            case 'r':
                c = '\r';
                break;
            case 't':
                c = '\t';
                break;
            case 'v':
                c = '\v';
                break;
            case 'e':
                if (!GNU_C_扩展)
                    goto invalid_escape;
                c = 27;
                break;
            case '\'':
            case '\"':
            case '\\': 
            case 英_问号:
                break;
            default:
            invalid_escape:
                if (c >= 英_叹号 && c <= '~')
                    zhi_警告("未知溢出序列: \'\\%c\'", c);
                else
                    zhi_警告("未知溢出序列: \'\\x%x\'", c);
                break;
            }
        } else if (is_long && c >= 0x80) {
            /* assume we are processing UTF-8 sequence */
            /* reference: The Unicode Standard, Version 10.0, ch3.9 */

            int cont; /* count of continuation bytes */
            int 跳过; /* how many bytes should 跳过 when error occurred */
            int i;

            /* decode leading byte */
            if (c < 0xC2) {
	            跳过 = 1; goto invalid_utf8_sequence;
            } else if (c <= 0xDF) {
	            cont = 1; n = c & 0x1f;
            } else if (c <= 0xEF) {
	            cont = 2; n = c & 0xf;
            } else if (c <= 0xF4) {
	            cont = 3; n = c & 0x7;
            } else {
	            跳过 = 1; goto invalid_utf8_sequence;
            }

            /* decode continuation bytes */
            for (i = 1; i <= cont; i++) {
                int l = 0x80, h = 0xBF;

                /* adjust limit for second byte */
                if (i == 1) {
                    switch (c) {
                    case 0xE0: l = 0xA0; break;
                    case 0xED: h = 0x9F; break;
                    case 0xF0: l = 0x90; break;
                    case 0xF4: h = 0x8F; break;
                    }
                }

                if (p[i] < l || p[i] > h) {
                    跳过 = i; goto invalid_utf8_sequence;
                }

                n = (n << 6) | (p[i] & 0x3f);
            }

            /* advance pointer */
            p += 1 + cont;
            c = n;
            goto add_char_nonext;

            /* error handling */
        invalid_utf8_sequence:
            zhi_警告("格式错误的UTF-8子序列，以以下字符开头： \'\\x%x\'", c);
            c = 0xFFFD;
            p += 跳过;
            goto add_char_nonext;

        }
        p++;
    add_char_nonext:
        if (!is_long)
            动态字符串_追加一个字符(outstr, c);
        else {
#ifdef 目标_PE
            /* store as UTF-16 */
            if (c < 0x10000) {
                动态字符串_追加一个宽字符(outstr, c);
            } else {
                c -= 0x10000;
                动态字符串_追加一个宽字符(outstr, (c >> 10) + 0xD800);
                动态字符串_追加一个宽字符(outstr, (c & 0x3FF) + 0xDC00);
            }
#else
            动态字符串_追加一个宽字符(outstr, c);
#endif
        }
    }
    /* add a trailing '\0' */
    if (!is_long)
        动态字符串_追加一个字符(outstr, '\0');
    else
        动态字符串_追加一个宽字符(outstr, '\0');
}

static void 解析_字符串(const char *s, int 长度)
{
    uint8_t buf[1000], *p = buf;
    int is_long, sep;

    if ((is_long = *s == 'L'))
        ++s, --长度;
    sep = *s++;
    长度 -= 2;
    if (长度 >= sizeof buf)
        p = zhi_分配内存(长度 + 1);
    memcpy(p, s, 长度);
    p[长度] = 0;

    动态字符串_重置(&当前解析的字符串);
    解析转义字符串(&当前解析的字符串, p, is_long);
    if (p != buf)
        zhi_释放(p);

    if (sep == '\'') {
        int char_size, i, n, c;
        /* XXX: make it portable */
        if (!is_long)
            单词编码 = TOK_字符常量, char_size = 1;
        else
            单词编码 = TOK_LCHAR, char_size = sizeof(nwchar_t);
        n = 当前解析的字符串.大小 / char_size - 1;
        if (n < 1)
            错_误("空的字符常量");
        if (n > 1)
            zhi_警告_c(警告_所有)("多字符字符常数");
        for (c = i = 0; i < n; ++i) {
            if (is_long)
                c = ((nwchar_t *)当前解析的字符串.数据)[i];
            else
                c = (c << 8) | ((char *)当前解析的字符串.数据)[i];
        }
        单词常量.i = c;
    } else {
        单词常量.字符串.大小 = 当前解析的字符串.大小;
        单词常量.字符串.数据 = 当前解析的字符串.数据;
        if (!is_long)
            单词编码 = TOK_字符串常量;
        else
            单词编码 = TOK_LSTR;
    }
}

/* 我们使用 64 位数字 */
#define 六4位数字大小 2

/* bn = (bn << shift) | or_val */
static void bn_lshift(unsigned int *bn, int shift, int or_val)
{
    int i;
    unsigned int v;
    for(i=0;i<六4位数字大小;i++)
    {
        v = bn[i];
        bn[i] = (v << shift) | or_val;
        or_val = v >> (32 - shift);
    }
}

static void 六十四位数字0(unsigned int *bn)
{
    int i;
    for(i=0;i<六4位数字大小;i++)
    {
        bn[i] = 0;
    }
}

/* 解析空终止字符串 'p' 中的数字并在当前标记中返回它 */
static void 解析数字(const char *p)
{
    int b, t, shift, frac_bits, s, exp_val, 当前字符;
    char *q;
    unsigned int bn[六4位数字大小];
    double d;
    q = 单词_缓存;
    当前字符 = *p++;
    t = 当前字符;
    当前字符 = *p++;
    *q++ = t;
    b = 10;
    if (t == '.')
    {
        goto float_frac_parse;
    } else if (t == '0')
    {
        if (当前字符 == 'x' || 当前字符 == 'X')
        {
            q--;
            当前字符 = *p++;
            b = 16;
        } else if (全局虚拟机->zhi_扩展 && (当前字符 == 'b' || 当前字符 == 'B'))
        {
            q--;
            当前字符 = *p++;
            b = 2;
        }
    }
    /* 解析所有数字。 由于浮点常量，现阶段无法检查八进制数 */
    while (1)
    {
        if (当前字符 >= 'a' && 当前字符 <= 'f')
            t = 当前字符 - 'a' + 10;
        else if (当前字符 >= 'A' && 当前字符 <= 'F')
            t = 当前字符 - 'A' + 10;
        else if (是数字(当前字符))
            t = 当前字符 - '0';
        else
            break;
        if (t >= b)
            break;
        if (q >= 单词_缓存 + 字符串最大长度)
        {
        num_too_long:
            错_误("数字太长");
        }
        *q++ = 当前字符;
        当前字符 = *p++;
    }
    if (当前字符 == '.' || ((当前字符 == 'e' || 当前字符 == 'E') && b == 10) || ((当前字符 == 'p' || 当前字符 == 'P') && (b == 16 || b == 2)))
    {
        if (b != 10)
        {
            /* 注意：strtox 应该支持六进制数，但非 ISOC99 的 libcs 不支持，所以我们更喜欢手工做 */
            /* 十六进制或二进制浮点数 */
            /* XXX: 处理溢出 */
            *q = '\0';
            if (b == 16)
                shift = 4;
            else 
                shift = 1;
            六十四位数字0(bn);
            q = 单词_缓存;
            while (1)
            {
                t = *q++;
                if (t == '\0')
                {
                    break;
                } else if (t >= 'a')
                {
                    t = t - 'a' + 10;
                } else if (t >= 'A')
                {
                    t = t - 'A' + 10;
                } else
                {
                    t = t - '0';
                }
                bn_lshift(bn, shift, t);
            }
            frac_bits = 0;
            if (当前字符 == '.')
            {
                当前字符 = *p++;
                while (1)
                {
                    t = 当前字符;
                    if (t >= 'a' && t <= 'f')
                    {
                        t = t - 'a' + 10;
                    } else if (t >= 'A' && t <= 'F')
                    {
                        t = t - 'A' + 10;
                    } else if (t >= '0' && t <= '9')
                    {
                        t = t - '0';
                    } else
                    {
                        break;
                    }
                    if (t >= b)
                        错_误("无效数字");
                    bn_lshift(bn, shift, t);
                    frac_bits += shift;
                    当前字符 = *p++;
                }
            }
            if (当前字符 != 'p' && 当前字符 != 'P')
                应为("指数");
            当前字符 = *p++;
            s = 1;
            exp_val = 0;
            if (当前字符 == '+')
            {
                当前字符 = *p++;
            } else if (当前字符 == '-')
            {
                s = -1;
                当前字符 = *p++;
            }
            if (当前字符 < '0' || 当前字符 > '9')
                应为("指数位数");
            while (当前字符 >= '0' && 当前字符 <= '9')
            {
                exp_val = exp_val * 10 + 当前字符 - '0';
                当前字符 = *p++;
            }
            exp_val = exp_val * s;
            
            /* 现在我们可以生成数字 */
            /* XXX: 应该直接修补浮点数 */
            d = (double)bn[1] * 4294967296.0 + (double)bn[0];
            d = ldexp(d, exp_val - frac_bits);
            t = toup(当前字符);
            if (t == 'F')
            {
                当前字符 = *p++;
                单词编码 = TOK_浮点常量;
                /* float : 应该处理溢出*/
                单词常量.f = (float)d;
            } else if (t == 'L')
            {
                当前字符 = *p++;
#ifdef 为长双精度使用双精度M
                单词编码 = TOK_双精度常量;
                单词常量.d = d;
#else
                单词编码 = TOK_长双精度常量;
                /* XXX: 不够大 */
                单词常量.ld = (long double)d;
#endif
            } else
            {
                单词编码 = TOK_双精度常量;
                单词常量.d = d;
            }
        } else
        {
            /* 十进制浮点数 */
            if (当前字符 == '.')
            {
                if (q >= 单词_缓存 + 字符串最大长度)
                    goto num_too_long;
                *q++ = 当前字符;
                当前字符 = *p++;
            float_frac_parse:
                while (当前字符 >= '0' && 当前字符 <= '9')
                {
                    if (q >= 单词_缓存 + 字符串最大长度)
                        goto num_too_long;
                    *q++ = 当前字符;
                    当前字符 = *p++;
                }
            }
            if (当前字符 == 'e' || 当前字符 == 'E')
            {
                if (q >= 单词_缓存 + 字符串最大长度)
                    goto num_too_long;
                *q++ = 当前字符;
                当前字符 = *p++;
                if (当前字符 == '-' || 当前字符 == '+')
                {
                    if (q >= 单词_缓存 + 字符串最大长度)
                        goto num_too_long;
                    *q++ = 当前字符;
                    当前字符 = *p++;
                }
                if (当前字符 < '0' || 当前字符 > '9')
                    应为("指数数字");
                while (当前字符 >= '0' && 当前字符 <= '9')
                {
                    if (q >= 单词_缓存 + 字符串最大长度)
                        goto num_too_long;
                    *q++ = 当前字符;
                    当前字符 = *p++;
                }
            }
            *q = '\0';
            t = toup(当前字符);
            errno = 0;
            if (t == 'F')
            {
                当前字符 = *p++;
                单词编码 = TOK_浮点常量;
                单词常量.f = strtof(单词_缓存, NULL);
            } else if (t == 'L')
            {
                当前字符 = *p++;
#ifdef 为长双精度使用双精度M
                单词编码 = TOK_双精度常量;
                单词常量.d = strtod(单词_缓存, NULL);
#else
                单词编码 = TOK_长双精度常量;
                单词常量.ld = strtold(单词_缓存, NULL);
#endif
            } else
            {
                单词编码 = TOK_双精度常量;
                单词常量.d = strtod(单词_缓存, NULL);
            }
        }
    } else
    {
        unsigned long long n, n1;
        int lcount, ucount, ov = 0;
        const char *p1;

        /* 整数 */
        *q = '\0';
        q = 单词_缓存;
        if (b == 10 && *q == '0')
        {
            b = 8;
            q++;
        }
        n = 0;
        while(1)
        {
            t = *q++;
            /* 除了基数为 10 / 8 的错误，不需要检查 */
            if (t == '\0')
                break;
            else if (t >= 'a')
                t = t - 'a' + 10;
            else if (t >= 'A')
                t = t - 'A' + 10;
            else
                t = t - '0';
            if (t >= b)
                错_误("无效数字");
            n1 = n;
            n = n * b + t;
            /* 检测溢出 */
            if (n1 >= 0x1000000000000000ULL && n / b != n1)
                ov = 1;
        }

        /* 根据常量后缀确定常量的类型必须具有的特征（无符号和/或 64 位） */
        lcount = ucount = 0;
        p1 = p;
        for(;;)
        {
            t = toup(当前字符);
            if (t == 'L')
            {
                if (lcount >= 2)
                    错_误("整数常量中的三个'l'");
                if (lcount && *(p - 1) != 当前字符)
                    错_误("不正确的整数后缀: %s", p1);
                lcount++;
                当前字符 = *p++;
            } else if (t == 'U')
            {
                if (ucount >= 1)
                    错_误("整数常量中的两个 'u'");
                ucount++;
                当前字符 = *p++;
            } else
            {
                break;
            }
        }

        /* 确定它是否需要 64 位和/或无符号才能适合 */
        if (ucount == 0 && b == 10)
        {
            if (lcount <= (长整数大小 == 4))
            {
                if (n >= 0x80000000U)
                    lcount = (长整数大小 == 4) + 1;
            }
            if (n >= 0x8000000000000000ULL)
                ov = 1, ucount = 1;
        } else {
            if (lcount <= (长整数大小 == 4))
            {
                if (n >= 0x100000000ULL)
                    lcount = (长整数大小 == 4) + 1;
                else if (n >= 0x80000000U)
                    ucount = 1;
            }
            if (n >= 0x8000000000000000ULL)
                ucount = 1;
        }

        if (ov)
            zhi_警告("整数常量溢出");

        单词编码 = TOK_整数常量;
	if (lcount)
	{
            单词编码 = TOK_长整数常量;
            if (lcount == 2)
                单词编码 = TOK_长长整数常量;
	}
	if (ucount)
	    ++单词编码; /* TOK_CU... */
        单词常量.i = n;
    }
    if (当前字符)
        错_误("无效数字");
}


#define PARSE2(c1, tok1, c2, tok2)              \
    case c1:                                    \
        PEEKC(c, p);                            \
        if (c == c2) {                          \
            p++;                                \
            单词编码 = tok2;                         \
        } else {                                \
            单词编码 = tok1;                         \
        }                                       \
        break;

static inline void 取下个单词不宏替换1(void)
{
    int t, c, is_long, 长度;
    单词ST *ts;
    uint8_t *p, *p1;
    unsigned int h;

    p = 文件->缓冲源码串;
 重做_未开始:
    c = *p;
    switch(c)
    {
    case ' ':
    case '\t': /*tab键，相当于8个空格的位置*/
        单词编码 = c;
        p++;
 可能空格:
        if (解析标志 & 解析标志_空格)
            goto 保持单词标志;
        while (标识符数字表[*p - 中_文件结尾] & 是_空格)
            ++p;
        goto 重做_未开始;
    case '\f':/*换页*/
    case '\v':/*垂直tab键*/
    case '\r':/*回车键*/
        p++;
        goto 重做_未开始;
    case '\\':
        /* 首先看看它是否实际上是缓冲区的结尾 */
        c = 处理转义符1(p);
        p = 文件->缓冲源码串;
        if (c == '\\')
            goto 解析简单的;
        if (c != 中_文件结尾)
            goto 重做_未开始;
        {
            虚拟机ST *s1 = 全局虚拟机;
            if ((解析标志 & 解析标志_换行)
                && !(单词标志 & 单词标志_文件结尾))
            {
                单词标志 |= 单词标志_文件结尾;
                单词编码 = 英_换行;
                goto 保持单词标志;
            } else if (!(解析标志 & 解析标志_预处理))
            {
                单词编码 = TOK_文件结尾;
            } else if (s1->ifdef_栈_ptr != 文件->ifdef_栈_ptr)
            {
                错_误("缺失 #endif");
            } else if (s1->include栈指针 == s1->include栈)
            {
                /* no include left : 文件结尾. */
                单词编码 = TOK_文件结尾;
            } else
            {
                单词标志 &= ~单词标志_文件结尾;
                /* 弹出包含文件 */
                
                /* 测试上一个“#endif”是否在文件开头的#ifdef之后 */
                if (单词标志 & 单词标志_ENDIF)
                {
#ifdef INCLUDE调试
                    printf("#endif %s\n", 取单词字符串(文件->保存的ifndef宏, NULL));
#endif
                    搜索include缓存(s1, 文件->文件名, 1)
                        ->ifndef宏 = 文件->保存的ifndef宏;
                    单词标志 &= ~单词标志_ENDIF;
                }

                添加包含文件调试信息的结束(全局虚拟机);
                /* 弹出包含堆栈 */
                关闭打开的文件();
                s1->include栈指针--;
                p = 文件->缓冲源码串;
                if (p == 文件->缓冲)
                    单词标志 = 单词标志_文件开始之前|单词标志_行开始之前;
                goto 重做_未开始;
            }
        }
        break;

    case '\n':
        文件->当前行号++;
        单词标志 |= 单词标志_行开始之前;
        p++;
maybe_newline:
        if (0 == (解析标志 & 解析标志_换行))
            goto 重做_未开始;
        单词编码 = 英_换行;
        goto 保持单词标志;

    case '#':
        /* XXX: 简化 */
        PEEKC(c, p);
        if ((单词标志 & 单词标志_行开始之前) && 
            (解析标志 & 解析标志_预处理))
        {
            文件->缓冲源码串 = p;
            预处理(单词标志 & 单词标志_文件开始之前);
            p = 文件->缓冲源码串;
            goto maybe_newline;
        } else
        {
            if (c == '#')
            {
                p++;
                单词编码 = TOK_双井号;
            } else
            {
#if !defined(目标_ARM)
                if (解析标志 & 解析标志_汇编文件)
                {
                    p = 解析单行注释(p - 1);
                    goto 重做_未开始;
                } else
#endif
                {
                    单词编码 = '#';
                }
            }
        }
        break;
    
    /* 不解析asm时，允许美元启动标识符 */
    case '$':
        if (!(标识符数字表[c - 中_文件结尾] & 是_标识符)
         || (解析标志 & 解析标志_汇编文件))
            goto 解析简单的;

    case 'a': case 'b': case 'c': case 'd':
    case 'e': case 'f': case 'g': case 'h':
    case 'i': case 'j': case 'k': case 'l':
    case 'm': case 'n': case 'o': case 'p':
    case 'q': case 'r': case 's': case 't':
    case 'u': case 'v': case 'w': case 'x':
    case 'y': case 'z': 
    case 'A': case 'B': case 'C': case 'D':
    case 'E': case 'F': case 'G': case 'H':
    case 'I': case 'J': case 'K': 
    case 'M': case 'N': case 'O': case 'P':
    case 'Q': case 'R': case 'S': case 'T':
    case 'U': case 'V': case 'W': case 'X':
    case 'Y': case 'Z': 
    case '_':
    解析标识符_快:
        p1 = p;
        h = 单词哈希初始值;
        h = 计算哈希地址(h, c);
        while (c = *++p, 标识符数字表[c - 中_文件结尾] & (是_标识符|是_数字))
            h = 计算哈希地址(h, c);
        长度 = p - p1;
        if (c != '\\')
        {
            单词ST **pts;
            /* 快速案例：没有发现流浪，所以我们有完整的令牌，我们已经散列它 */
            h &= (哈希表容量 - 1);
            pts = &单词哈希表[h];
            for(;;)
            {
                ts = *pts;
                if (!ts)
                    break;
                if (ts->长度 == 长度 && !memcmp(ts->字符串, p1, 长度))
                    goto token_found;
                pts = &(ts->哈希冲突的其他单词);
            }
            ts = 符号表插入新单词(pts, (char *) p1, 长度);
        token_found: ;
        } else
        {
            /* 较慢的情况 */
            动态字符串_重置(&当前解析的字符串);
            动态字符串_拼接(&当前解析的字符串, (char *) p1, 长度);
            p--;
            PEEKC(c, p);
        解析标识符_慢:
            while (标识符数字表[c - 中_文件结尾] & (是_标识符|是_数字))
            {
                动态字符串_追加一个字符(&当前解析的字符串, c);
                PEEKC(c, p);
            }
            ts = 插入单词(当前解析的字符串.数据, 当前解析的字符串.大小);
        }
        单词编码 = ts->单词编码;
        break;
    case 'L':
        t = p[1];
        if (t != '\\' && t != '\'' && t != '\"')
        {
            /* fast case */
            goto 解析标识符_快;
        } else {
            PEEKC(c, p);
            if (c == '\'' || c == '\"')
            {
                is_long = 1;
                goto str_const;
            } else
            {
                动态字符串_重置(&当前解析的字符串);
                动态字符串_追加一个字符(&当前解析的字符串, 'L');
                goto 解析标识符_慢;
            }
        }
        break;

    case '0': case '1': case '2': case '3':
    case '4': case '5': case '6': case '7':
    case '8': case '9':
        t = c;
        PEEKC(c, p);
        /* 在第一个数字之后，接受数字alpha'.'或在前缀为“eEpP”时签名 */
    parse_num:
        动态字符串_重置(&当前解析的字符串);
        for(;;)
        {
            动态字符串_追加一个字符(&当前解析的字符串, t);
            if (!((标识符数字表[c - 中_文件结尾] & (是_标识符|是_数字)) || c == '.' || ((c == '+' || c == '-') && (((t == 'e' || t == 'E') && !(解析标志 & 解析标志_汇编文件 && ((char*)当前解析的字符串.数据)[0] == '0' && toup(((char*)当前解析的字符串.数据)[1]) == 'X')) || t == 'p' || t == 'P'))))
                break;
            t = c;
            PEEKC(c, p);
        }
        /* 我们添加一个尾随 '\0' 以方便解析 */
        动态字符串_追加一个字符(&当前解析的字符串, '\0');
        单词常量.字符串.大小 = 当前解析的字符串.大小;
        单词常量.字符串.数据 = 当前解析的字符串.数据;
        单词编码 = TOK_预处理数字;
        break;

    case '.':
        /* 特殊的点处理，因为它也可以开始一个数字 */
        PEEKC(c, p);
        if (是数字(c))
        {
            t = '.';
            goto parse_num;
        } else if ((标识符数字表['.' - 中_文件结尾] & 是_标识符) && (标识符数字表[c - 中_文件结尾] & (是_标识符|是_数字)))
        {
            *--p = c = '.';
            goto 解析标识符_快;
        } else if (c == '.')
        {
            PEEKC(c, p);
            if (c == '.')
            {
                p++;
                单词编码 = TOK_三个点;
            } else {
                *--p = '.'; /* 可能下溢到 文件->unget[] */
                单词编码 = '.';
            }
        } else
        {
            单词编码 = '.';
        }
        break;
    case '\'':
    case '\"':
        is_long = 0;
    str_const:
        动态字符串_重置(&当前解析的字符串);
        if (is_long)
            动态字符串_追加一个字符(&当前解析的字符串, 'L');
        动态字符串_追加一个字符(&当前解析的字符串, c);
        p = 解析字符串而不解释转义(p, c, &当前解析的字符串);
        动态字符串_追加一个字符(&当前解析的字符串, c);
        动态字符串_追加一个字符(&当前解析的字符串, '\0');
        单词常量.字符串.大小 = 当前解析的字符串.大小;
        单词常量.字符串.数据 = 当前解析的字符串.数据;
        单词编码 = TOK_预处理字符串;
        break;

    case '<':
        PEEKC(c, p);
        if (c == '=') {
            p++;
            单词编码 = TOK_小于等于;
        } else if (c == '<') {
            PEEKC(c, p);
            if (c == '=') {
                p++;
                单词编码 = TOK_左移后赋值;
            } else {
                单词编码 = TOK_左移;
            }
        } else {
            单词编码 = TOK_小于;
        }
        break;
    case '>':
        PEEKC(c, p);
        if (c == '=') {
            p++;
            单词编码 = TOK_大于等于;
        } else if (c == '>') {
            PEEKC(c, p);
            if (c == '=') {
                p++;
                单词编码 = TOK_右移后赋值;
            } else {
                单词编码 = TOK_右移;
            }
        } else {
            单词编码 = TOK_大于;
        }
        break;
        
    case '&':
        PEEKC(c, p);
        if (c == '&') {
            p++;
            单词编码 = TOK_逻辑与;
        } else if (c == '=') {
            p++;
            单词编码 = TOK_按位与后赋值;
        } else {
            单词编码 = '&';
        }
        break;
        
    case '|':
        PEEKC(c, p);
        if (c == '|') {
            p++;
            单词编码 = TOK_逻辑或;
        } else if (c == '=') {
            p++;
            单词编码 = TOK_按位或后赋值;
        } else {
            单词编码 = '|';
        }
        break;

    case '+':
        PEEKC(c, p);
        if (c == '+') {
            p++;
            单词编码 = TOK_自加;
        } else if (c == '=') {
            p++;
            单词编码 = TOK_加后赋值;
        } else {
            单词编码 = '+';
        }
        break;
        
    case '-':
        PEEKC(c, p);
        if (c == '-') {
            p++;
            单词编码 = TOK_自减;
        } else if (c == '=') {
            p++;
            单词编码 = TOK_减后赋值;
        } else if (c == '>') {
            p++;
            单词编码 = TOK_箭头;
        } else {
            单词编码 = '-';
        }
        break;

    PARSE2(英_叹号, 英_叹号, '=', TOK_不等于)
    PARSE2('=', '=', '=', TOK_等于)
    PARSE2('*', '*', '=', TOK_乘后赋值)
    PARSE2('%', '%', '=', TOK_取模后赋值)
    PARSE2('^', '^', '=', TOK_按位异或后赋值)
        
        /* 注释 or operator */
    case '/':
        PEEKC(c, p);
        if (c == '*') {
            p = 解析注释(p);
            /* 注释替换为空白 */
            单词编码 = ' ';
            goto 可能空格;
        } else if (c == '/') {
            p = 解析单行注释(p);
            单词编码 = ' ';
            goto 可能空格;
        } else if (c == '=') {
            p++;
            单词编码 = TOK_除后赋值;
        } else {
            单词编码 = '/';
        }
        break;
        
        /* simple tokens */
    case 英_左小括号:
    case 英_右小括号:
    case 英_左中括号:
    case 英_右中括号:
    case 英_左大括号:
    case 英_右大括号:
    case 英_逗号:
    case 英_分号:
    case 英_冒号:
    case 英_问号:
    case '~':
    case '@': /* 仅在汇编程序中使用 */
    解析简单的:
        单词编码 = c;
        p++;
        break;
    default:
        if (c >= 0x80 && c <= 0xFF) /* utf8 标识符 */
        	goto 解析标识符_快;
        if (解析标志 & 解析标志_汇编文件)
            goto 解析简单的;
        错_误("无法识别的字符 \\x%02x", c);
        break;
    }
    单词标志 = 0;
保持单词标志:
    文件->缓冲源码串 = p;
#if defined(解析_调试)
    printf("token = %d %s\n", 单词编码, 取单词字符串(单词编码, &单词常量));
#endif
}

static void 宏替换(
    单词字符串ST *tok_str,
    符号ST **嵌套_列表,
    const int *macro_str
    );

/* 将 macro_str 中替换列表中的参数替换为 args（字段 d）中的值并返回分配的字符串 */
static int *宏参数替换(符号ST **嵌套_列表, const int *macro_str, 符号ST *args)
{
    int t, t0, t1, spc;
    const int *st;
    符号ST *s;
    常量值U cval;
    单词字符串ST 字符串;
    动态字符串ST cstr;

    单词字符串_新建(&字符串);
    t0 = t1 = 0;
    while(1) {
        TOK_GET(&t, &macro_str, &cval);
        if (!t)
            break;
        if (t == '#') {
            /* stringize */
            TOK_GET(&t, &macro_str, &cval);
            if (!t)
                goto bad_stringy;
            s = 查找符号并返回去关联结构(args, t);
            if (s) {
                动态字符串_新建(&cstr);
                动态字符串_追加一个字符(&cstr, '\"');
                st = s->d;
                spc = 0;
                while (*st >= 0) {
                    TOK_GET(&t, &st, &cval);
                    if (t != TOK_占位符
                     && t != TOK_不宏替换
                     && 0 == 检查空格(t, &spc)) {
                        const char *s = 取单词字符串(t, &cval);
                        while (*s) {
                            if (t == TOK_预处理字符串 && *s != '\'')
                                动态字符串_增加字符(&cstr, *s);
                            else
                                动态字符串_追加一个字符(&cstr, *s);
                            ++s;
                        }
                    }
                }
                cstr.大小 -= spc;
                动态字符串_追加一个字符(&cstr, '\"');
                动态字符串_追加一个字符(&cstr, '\0');
#ifdef PP_DEBUG
                printf("\nstringize: <%s>\n", (char *)cstr.数据);
#endif
                /* add string */
                cval.字符串.大小 = cstr.大小;
                cval.字符串.数据 = cstr.数据;
                单词字符串_增加2(&字符串, TOK_预处理字符串, &cval);
                动态字符串_释放(&cstr);
            } else {
        bad_stringy:
                应为(" '#'之后的宏参数");
            }
        } else if (t >= TOK_标识符) {
            s = 查找符号并返回去关联结构(args, t);
            if (s) {
                int l0 = 字符串.长度;
                st = s->d;
                /* if '##' is present before or after, no arg substitution */
                if (*macro_str == TOK_PPJOIN || t1 == TOK_PPJOIN) {
                    /* special case for var arg macros : ## eats the 英_逗号
                       if empty VA_ARGS variable. */
                    if (t1 == TOK_PPJOIN && t0 == 英_逗号 && GNU_C_扩展 && s->类型.数据类型) {
                        if (*st <= 0) {
                            /* suppress 英_逗号 '##' */
                            字符串.长度 -= 2;
                        } else {
                            /* suppress '##' and add variable */
                            字符串.长度--;
                            goto add_var;
                        }
                    }
                } else {
            add_var:
		    if (!s->next) {
			/* Expand arguments tokens and store them.  In most
			   cases we could also re-expand each argument if
			   used multiple times, but not if the argument
			   contains the __COUNTER__ macro.  */
			单词字符串ST str2;
			将符号放入符号栈2(&s->next, s->v, s->类型.数据类型, 0);
			单词字符串_新建(&str2);
			宏替换(&str2, 嵌套_列表, st);
			单词字符串_增加(&str2, 0);
			s->next->d = str2.字符串;
		    }
		    st = s->next->d;
                }
                for(;;) {
                    int t2;
                    TOK_GET(&t2, &st, &cval);
                    if (t2 <= 0)
                        break;
                    单词字符串_增加2(&字符串, t2, &cval);
                }
                if (字符串.长度 == l0) /* 扩展为空字符串 */
                    单词字符串_增加(&字符串, TOK_占位符);
            } else {
                单词字符串_增加(&字符串, t);
            }
        } else {
            单词字符串_增加2(&字符串, t, &cval);
        }
        t0 = t1, t1 = t;
    }
    单词字符串_增加(&字符串, 0);
    return 字符串.字符串;
}

static char const 月份名称[12][4] =
{
    "一月", "二月", "三月", "四月", "五月", "六月",
    "七月", "八月", "九月", "十月", "冬月", "腊月"
};

static int 粘贴单词(int t1, 常量值U *v1, int t2, 常量值U *v2)
{
    动态字符串ST cstr;
    int n, ret = 1;

    动态字符串_新建(&cstr);
    if (t1 != TOK_占位符)
        动态字符串_拼接(&cstr, 取单词字符串(t1, v1), -1);
    n = cstr.大小;
    if (t2 != TOK_占位符)
        动态字符串_拼接(&cstr, 取单词字符串(t2, v2), -1);
    动态字符串_追加一个字符(&cstr, '\0');

    新建源文件缓冲区(全局虚拟机, ":paste:", cstr.大小);
    memcpy(文件->缓冲, cstr.数据, cstr.大小);
    单词标志 = 0;
    for (;;) {
        取下个单词不宏替换1();
        if (0 == *文件->缓冲源码串)
            break;
        if (是空格(单词编码))
            continue;
        zhi_警告("粘贴 \"%.*s\" 和 \"%s\" 不会给出有效的预处理单词", n, (char *)cstr.数据, (char*)cstr.数据 + n);
        ret = 0;
        break;
    }
    关闭打开的文件();
    动态字符串_释放(&cstr);
    return ret;
}

/* 处理“##”运算符。 如果没有看到“##”，则返回 NULL。 否则返回结果字符串（必须释放）。 */
static inline int *双井号宏(const int *ptr0)
{
    int t;
    常量值U cval;
    单词字符串ST macro_str1;
    int start_of_nosubsts = -1;
    const int *ptr;
    /* 我们搜索第一个 '##' */
    for (ptr = ptr0;;) {
        TOK_GET(&t, &ptr, &cval);
        if (t == TOK_PPJOIN)
            break;
        if (t == 0)
            return NULL;
    }
    单词字符串_新建(&macro_str1);
    //tok_print(" $$$", ptr0);
    for (ptr = ptr0;;) {
        TOK_GET(&t, &ptr, &cval);
        if (t == 0)
            break;
        if (t == TOK_PPJOIN)
            continue;
        while (*ptr == TOK_PPJOIN) {
            int t1; 常量值U cv1;
            /* given 'a##b', remove nosubsts preceding 'a' */
            if (start_of_nosubsts >= 0)
                macro_str1.长度 = start_of_nosubsts;
            /* given 'a##b', remove nosubsts preceding 'b' */
            while ((t1 = *++ptr) == TOK_不宏替换)
                ;
            if (t1 && t1 != TOK_PPJOIN) {
                TOK_GET(&t1, &ptr, &cv1);
                if (t != TOK_占位符 || t1 != TOK_占位符) {
                    if (粘贴单词(t, &cval, t1, &cv1)) {
                        t = 单词编码, cval = 单词常量;
                    } else {
                        单词字符串_增加2(&macro_str1, t, &cval);
                        t = t1, cval = cv1;
                    }
                }
            }
        }
        if (t == TOK_不宏替换) {
            if (start_of_nosubsts < 0)
                start_of_nosubsts = macro_str1.长度;
        } else {
            start_of_nosubsts = -1;
        }
        单词字符串_增加2(&macro_str1, t, &cval);
    }
    单词字符串_增加(&macro_str1, 0);
    //tok_print(" ###", macro_str1.字符串);
    return macro_str1.字符串;
}

/* 从函数宏调用中查看或读取 [ws_str == NULL] 下一个标记，如有必要，将宏级别向上移动到文件 */
static int 下一个参数流(符号ST **嵌套_列表, 单词字符串ST *ws_str)
{
    int t;
    const int *p;
    符号ST *sa;

    for (;;) {
        if (没宏替换的字符串指针) {
            p = 没宏替换的字符串指针, t = *p;
            if (ws_str) {
                while (是空格(t) || 英_换行 == t || TOK_占位符 == t)
                    单词字符串_增加(ws_str, t), t = *++p;
            }
            if (t == 0) {
                结束_宏扩展();
                /* also, end of 域S for nested defined symbol */
                sa = *嵌套_列表;
                while (sa && sa->v == 0)
                    sa = sa->上个;
                if (sa)
                    sa->v = 0;
                continue;
            }
        } else {
            当前字符 = 处理块结尾();
            if (ws_str) {
                while (是空格(当前字符) || 当前字符 == '\n' || 当前字符 == '/') {
                    if (当前字符 == '/') {
                        int c;
                        uint8_t *p = 文件->缓冲源码串;
                        PEEKC(c, p);
                        if (c == '*') {
                            p = 解析注释(p);
                            文件->缓冲源码串 = p - 1;
                        } else if (c == '/') {
                            p = 解析单行注释(p);
                            文件->缓冲源码串 = p - 1;
                        } else
                            break;
                        当前字符 = ' ';
                    }
                    if (当前字符 == '\n')
                        文件->当前行号++;
                    if (!(当前字符 == '\f' || 当前字符 == '\v' || 当前字符 == '\r'))
                        单词字符串_增加(ws_str, 当前字符);
                    cinp();
                }
            }
            t = 当前字符;
        }

        if (ws_str)
            return t;
        取下个单词不宏替换();
        return 单词编码;
    }
}

/* 用宏“s”对当前标记进行宏替换，并将结果添加到 (tok_str,tok_len)。 '嵌套_列表' 是我们为了避免递归而进入的所有宏的列表。
 * 如果不需要完成替换，则返回非零 */
static int 宏替换_当前单词(单词字符串ST *tok_str,符号ST **嵌套_列表,符号ST *s)
{
    符号ST *args, *sa, *sa1;
    int parlevel, t, t1, spc;
    单词字符串ST 字符串;
    char *cstrval;
    常量值U cval;
    动态字符串ST cstr;
    char buf[32];

    /* 如果符号是宏，则准备替换 */
    /* 特殊宏 */
    if (单词编码 == TOK___LINE__ || 单词编码 == TOK___COUNTER__)
    {
        t = 单词编码 == TOK___LINE__ ? 文件->当前行号 : 词法计数器++;
        snprintf(buf, sizeof(buf), "%d", t);
        cstrval = buf;
        t1 = TOK_预处理数字;
        goto add_cstr1;
    } else if (单词编码 == TOK___FILE__)
    {
        cstrval = 文件->文件名;
        goto add_cstr;
    } else if (单词编码 == TOK___DATE__ || 单词编码 == TOK___TIME__)
    {
        time_t ti;
        struct tm *tm;

        time(&ti);
        tm = localtime(&ti);
        if (单词编码 == TOK___DATE__)
        {
            snprintf(buf, sizeof(buf), "%s %2d %d",月份名称[tm->tm_mon], tm->tm_mday, tm->tm_year + 1900);
        } else
        {
            snprintf(buf, sizeof(buf), "%02d:%02d:%02d",tm->tm_hour, tm->tm_min, tm->tm_sec);
        }
        cstrval = buf;
    add_cstr:
        t1 = TOK_字符串常量;
    add_cstr1:
        动态字符串_新建(&cstr);
        动态字符串_拼接(&cstr, cstrval, 0);
        cval.字符串.大小 = cstr.大小;
        cval.字符串.数据 = cstr.数据;
        单词字符串_增加2(tok_str, t1, &cval);
        动态字符串_释放(&cstr);
    } else if (s->d)
    {
        int 保存_解析标志 = 解析标志;
	    int *joined_str = NULL;
        int *mstr = s->d;

        if (s->类型.数据类型 == MACRO_FUNC)
        {
            /* 宏名称和参数列表之间的空格*/
            单词字符串ST ws_str;
            单词字符串_新建(&ws_str);
            spc = 0;
            解析标志 |= 解析标志_空格 | 解析标志_换行 | 解析标志_转义;
            /* 从参数流中获取下一个标记 */
            t = 下一个参数流(嵌套_列表, &ws_str);
            if (t != 英_左小括号)
            {
                /* 毕竟不是宏替换，恢复宏标记加上我们读过的所有空格。 有意不合并空格以保留换行符。 */
                解析标志 = 保存_解析标志;
                单词字符串_增加(tok_str, 单词编码);
                if (解析标志 & 解析标志_空格)
                {
                    int i;
                    for (i = 0; i < ws_str.长度; i++)
                        单词字符串_增加(tok_str, ws_str.字符串[i]);
                }
                单词字符串_释放_字符串(ws_str.字符串);
                return 0;
            } else {
                单词字符串_释放_字符串(ws_str.字符串);
            }
	    do {
		取下个单词不宏替换(); /* eat 英_左小括号 */
	    } while (单词编码 == TOK_占位符 || 是空格(单词编码));
            /* 参数宏 */
            args = NULL;
            sa = s->next;
            /* NOTE: 允许空参数，除非没有参数 */
            for(;;)
            {
                do
                {
                    下一个参数流(嵌套_列表, NULL);
                } while (单词编码 == TOK_占位符 || 是空格(单词编码) || 英_换行 == 单词编码);
    empty_arg:
                /* handle '()' case */
                if (!args && !sa && 单词编码 == 英_右小括号)
                    break;
                if (!sa)
                    错_误("宏 '%s' 与太多参数一起使用",
                          取单词字符串(s->v, 0));
                单词字符串_新建(&字符串);
                parlevel = spc = 0;
                /* NOTE: 非零 sa->数据类型 表示 VA_ARGS */
                while ((parlevel > 0 || (单词编码 != 英_右小括号 && (单词编码 != 英_逗号 || sa->类型.数据类型))))
                {
                    if (单词编码 == TOK_文件结尾 || 单词编码 == 0)
                        break;
                    if (单词编码 == 英_左小括号)
                        parlevel++;
                    else if (单词编码 == 英_右小括号)
                        parlevel--;
                    if (单词编码 == 英_换行)
                        单词编码 = ' ';
                    if (!检查空格(单词编码, &spc))
                        单词字符串_增加2(&字符串, 单词编码, &单词常量);
                    下一个参数流(嵌套_列表, NULL);
                }
                if (parlevel)
                    应为(")");
                字符串.长度 -= spc;
                单词字符串_增加(&字符串, -1);
                单词字符串_增加(&字符串, 0);
                sa1 = 将符号放入符号栈2(&args, sa->v & ~符号_字段, sa->类型.数据类型, 0);
                sa1->d = 字符串.字符串;
                sa = sa->next;
                if (单词编码 == 英_右小括号)
                {
                    /* special case for gcc var args: add an empty
                       var arg argument if it is omitted */
                    if (sa && sa->类型.数据类型 && GNU_C_扩展)
                        goto empty_arg;
                    break;
                }
                if (单词编码 != 英_逗号)
                    应为(",");
            }
            if (sa)
            {
                错_误("宏 '%s' 使用的参数太少",
                      取单词字符串(s->v, 0));
            }

            /* 现在替换每个 arg */
            mstr = 宏参数替换(嵌套_列表, mstr, args);
            /* 释放内存 */
            sa = args;
            while (sa)
            {
                sa1 = sa->上个;
                单词字符串_释放_字符串(sa->d);
                if (sa->next)
                {
                    单词字符串_释放_字符串(sa->next->d);
                    sym_free(sa->next);
                }
                sym_free(sa);
                sa = sa1;
            }
            解析标志 = 保存_解析标志;
        }

        将符号放入符号栈2(嵌套_列表, s->v, 0, 0);
        解析标志 = 保存_解析标志;
        joined_str = 双井号宏(mstr);
        宏替换(tok_str, 嵌套_列表, joined_str ? joined_str : mstr);

        /* 弹出嵌套定义的符号 */
        sa1 = *嵌套_列表;
        *嵌套_列表 = sa1->上个;
        sym_free(sa1);
	if (joined_str)
	    单词字符串_释放_字符串(joined_str);
        if (mstr != s->d)
            单词字符串_释放_字符串(mstr);
    }
    return 0;
}

/* 对 macro_str 进行宏替换并将结果添加到 (tok_str,tok_len)。 '封装_列表'是我们为了避免递归而进入的所有宏的列表. */
static void 宏替换(单词字符串ST *tok_str,符号ST **嵌套_列表,const int *macro_str)
{
    符号ST *s;
    int t, spc, 不宏替换;
    常量值U cval;
    
    spc = 不宏替换 = 0;

    while (1)
    {
        TOK_GET(&t, &macro_str, &cval);
        if (t <= 0)
            break;

        if (t >= TOK_标识符 && 0 == 不宏替换)
        {
            s = 查找define(t);
            if (s == NULL)
                goto 不替换;

            /* 如果嵌套替换，则什么都不做 */
            if (查找符号并返回去关联结构(*嵌套_列表, t))
            {
                /* 并将其标记为 ‘TOK_不宏替换’，因此它不会再次被取代 */
                单词字符串_增加2(tok_str, TOK_不宏替换, NULL);
                goto 不替换;
            }

            {
                单词字符串ST *字符串 = 单词字符串_分配();
                字符串->字符串 = (int*)macro_str;
                开始_宏扩展(字符串, 2);

                单词编码 = t;
                宏替换_当前单词(tok_str, 嵌套_列表, s);

                if (宏_栈 != 字符串)
                {
                    /* 已经通过读取函数宏参数完成 */
                    break;
                }

                macro_str = 没宏替换的字符串指针;
                结束_宏扩展 ();
            }
            if (tok_str->长度)
                spc = 是空格(t = tok_str->字符串[tok_str->lastlen]);
        } else {
不替换:
            if (!检查空格(t, &spc))
                单词字符串_增加2(tok_str, t, &cval);

            if (不宏替换) {
                if (不宏替换 > 1 && (spc || (++不宏替换 == 3 && t == 英_左小括号)))
                    continue;
                不宏替换 = 0;
            }
            if (t == TOK_不宏替换)
                不宏替换 = 1;
        }
        /* GCC 支持“定义”作为宏替换的结果 */
        if (t == 英_已定义 && pp_expr)
            不宏替换 = 2;
    }
}

/* 返回没有宏替换的下一个单词。. 可以从 “没宏替换的字符串指针 ” 读取输入
    */
static void 取下个单词不宏替换(void)
{
    int t;
    if (没宏替换的字符串指针)
    {
 重做:
        t = *没宏替换的字符串指针;
        if (TOK_哈希_值(t))
        {
            从整数数组中获取标记并递增指针(&单词编码, &没宏替换的字符串指针, &单词常量);
            if (t == TOK_行号)
            {
                文件->当前行号 = 单词常量.i;
                goto 重做;
            }
        } else
        {
            没宏替换的字符串指针++;
            if (t < TOK_标识符)
            {
                if (!(解析标志 & 解析标志_空格)
                    && (标识符数字表[t - 中_文件结尾] & 是_空格))
                    goto 重做;
            }
            单词编码 = t;
        }
    } else
    {
        取下个单词不宏替换1();
    }
}

/* 使用宏替换返回下一个单词 */
静态函数 void 取下个单词(void)
{
    int t;
 重做:
    取下个单词不宏替换();
    t = 单词编码;
    if (没宏替换的字符串指针)
    {
        if (!TOK_哈希_值(t))
        {
            if (t == TOK_不宏替换 || t == TOK_占位符)
            {
                /* 丢弃预处理器标记 */
                goto 重做;
            } else if (t == 0)
            {
                /* 宏结束或 unget 单词字符串 */
                结束_宏扩展();
                goto 重做;
            } else if (t == '\\')
            {
                if (!(解析标志 & 解析标志_转义))
                {
                	错_误("转义程序中的 '\\' ");
                }
            }
            return;
        }
    } else if (t >= TOK_标识符 && (解析标志 & 解析标志_预处理))
    {
        /* 如果从文件中读取，请尝试替换宏 */
        符号ST *s = 查找define(t);
        if (s)
        {
            符号ST *嵌套_列表 = NULL;
            单词字符串_缓冲.长度 = 0;
            宏替换_当前单词(&单词字符串_缓冲, &嵌套_列表, s);
            单词字符串_增加(&单词字符串_缓冲, 0);
            开始_宏扩展(&单词字符串_缓冲, 0);
            goto 重做;
        }
        return;
    }
    /* 将预处理器标记转换为 C 标记 */
    if (t == TOK_预处理数字)
    {
        if  (解析标志 & 解析标志_数字)
        {
        	解析数字((char *)单词常量.字符串.数据);
        }
    } else if (t == TOK_预处理字符串)
    {
        if (解析标志 & 解析标志_单词字符串)
        {
        	解析_字符串((char *)单词常量.字符串.数据, 单词常量.字符串.大小 - 1);
        }
    }
}

/* 退回当前单词并将当前单词设置为“last_tok”。 仅处理标签的标识符案例。 */
ST_INLN void 退回当前单词(int last_tok)
{

    单词字符串ST *字符串 = 单词字符串_分配();
    单词字符串_增加2(字符串, 单词编码, &单词常量);
    单词字符串_增加(字符串, 0);
    开始_宏扩展(字符串, 1);
    单词编码 = last_tok;
}

/* ------------------------------------------------------------------------- */
/* 初始化预处理器 */

static const char * const 目标系统定义 =
#ifdef 目标_PE
    "_WIN32\0"
# if 宏_指针大小 == 8
    "_WIN64\0"
# endif
#else
# if defined 目标_MACHO
    "__APPLE__\0"
# elif 目标系统_FreeBSD
    "__FreeBSD__ 12\0"
# elif 目标系统_FreeBSD_kernel
    "__FreeBSD_kernel__\0"
# elif 目标系统_NetBSD
    "__NetBSD__\0"
# elif 目标系统_OpenBSD
    "__OpenBSD__\0"
# else
    "__linux__\0"
    "__linux\0"
# endif
    "__unix__\0"
    "__unix\0"
#endif
    ;

static void putdef(动态字符串ST *cs, const char *p)
{
    动态字符串_printf(cs, "#define %s%s\n", p, &" 1"[!!strchr(p, ' ')*2]);
}

static void 预定义(虚拟机ST *s1, 动态字符串ST *cs, int 是_汇编)
{
    int a, b, c;
    const char *defs[] = { target_machine_defs, 目标系统定义, NULL };
    const char *p;

    sscanf(版本号, "%d.%d.%d", &a, &b, &c);
    动态字符串_printf(cs, "#define __ZHIXIN__ %d\n", a*10000 + b*100 + c);
    for (a = 0; defs[a]; ++a)
        for (p = defs[a]; *p; p = strchr(p, 0) + 1)
            putdef(cs, p);
#ifdef 目标_ARM
    if (s1->浮点_abi == ARM_HARD_FLOAT)
      putdef(cs, "__ARM_PCS_VFP");
#endif
    if (是_汇编)
      putdef(cs, "__ASSEMBLER__");
    if (s1->输出类型 == 输出_预处理)
      putdef(cs, "__ZHI_PP__");
    if (s1->输出类型 == 输出_内存)
      putdef(cs, "__ZHI_RUN__");
    if (s1->字符_是_无符号的)
      putdef(cs, "__CHAR_UNSIGNED__");
    if (s1->optimize > 0)
      putdef(cs, "__OPTIMIZE__");
    if (s1->option_pthread)
      putdef(cs, "_REENTRANT");
    if (s1->前导下划线)
      putdef(cs, "__leading_underscore");
#ifdef 启用边界检查M
    if (s1->使用_边界_检查器)
      putdef(cs, "__BOUNDS_CHECKING_ON");
#endif
    动态字符串_printf(cs, "#define __SIZEOF_POINTER__ %d\n", 宏_指针大小);
    动态字符串_printf(cs, "#define __SIZEOF_LONG__ %d\n", 长整数大小);
    if (!是_汇编) {
      putdef(cs, "__STDC__");
      动态字符串_printf(cs, "#define __STDC_VERSION__ %dL\n", s1->知语言版本);
      动态字符串_拼接(cs,
        /* load more predefs and __builtins */
#if CONFIG_ZHI_PREDEFS
        #include "zhidefs_.h" /* include as strings */
#else
        "#include <zhidefs.h>\n" /* load at runtime */
#endif
        , -1);
    }
    动态字符串_printf(cs, "#define __BASE_FILE__ \"%s\"\n", 文件->文件名);
}
静态函数 void 插入关键词和标识符(虚拟机ST *s)
{
    int i, c;
    const char *p, *r;
    /* 初始化标识符表 */
    for(i = 中_文件结尾; i<128; i++)
    {
    	设置标识符(i,是空格(i) ? 是_空格 : 是标识符(i) ? 是_标识符 : 是数字(i) ? 是_数字 : 0);
    }
    for(i = 128; i<256; i++)
    {
    	设置标识符(i, 是_标识符);
    }
    小分配器_新建(&单词符号分配器, 单词符号分配限制, 单词符号表大小M);
    小分配器_新建(&单词字符串分配器, 单词字符串分配限制, TOKSTR_TAL_SIZE);

    memset(单词哈希表, 0, 哈希表容量 * sizeof(单词ST *));
    memset(s->包含在ifndef宏中的文件, 0, sizeof s->包含在ifndef宏中的文件);

    动态字符串_新建(&动态字符串_缓冲);
    动态字符串_重新分配(&动态字符串_缓冲, 字符串最大长度);
    单词字符串_新建(&单词字符串_缓冲);
    单词字符串_重新分配(&单词字符串_缓冲, 单词字符串最大大小);

    单词_标识符 = TOK_标识符;
    p = 关键词表;
    while (*p)
    {
        r = p;
        for(;;)
        {
            c = *r++;
            if (c == '\0')
                break;
        }
        插入单词(p, r - p - 1);
        p = r;
    }

    /* 我们为一些特殊的宏添加了虚拟定义以加快测试速度并 defined() */
    推进define(TOK___LINE__, MACRO_OBJ, NULL, NULL);
    推进define(TOK___FILE__, MACRO_OBJ, NULL, NULL);
    推进define(TOK___DATE__, MACRO_OBJ, NULL, NULL);
    推进define(TOK___TIME__, MACRO_OBJ, NULL, NULL);
    推进define(TOK___COUNTER__, MACRO_OBJ, NULL, NULL);
}
静态函数 void 词法分析初始化(虚拟机ST *s1, int 源文件类型)
{
    int 是_汇编 = !!(源文件类型 & (源文件类型_仅汇编|源文件类型_汇编预处理));
    动态字符串ST cstr;

    插入关键词和标识符(s1);

    s1->include栈指针 = s1->include栈;
    s1->ifdef_栈_ptr = s1->ifdef_栈;
    文件->ifdef_栈_ptr = s1->ifdef_栈_ptr;
    pp_expr = 0;
    词法计数器 = 0;
    pp_debug_tok = pp_debug_symv = 0;
    pp_once++;
    s1->包_栈[0] = 0;
    s1->包_栈_ptr = s1->包_栈;

    设置标识符('$', !是_汇编 && s1->标识符中允许使用美元符 ? 是_标识符 : 0);
    设置标识符('.', 是_汇编 ? 是_标识符 : 0);

    if (!(源文件类型 & 源文件类型_仅汇编))
    {
        动态字符串_新建(&cstr);
        预定义(s1, &cstr, 是_汇编);
        if (s1->命令行_defines.大小)
        {
        	动态字符串_拼接(&cstr, s1->命令行_defines.数据, s1->命令行_defines.大小);
        }

        if (s1->命令行_include.大小)
        {
        	 动态字符串_拼接(&cstr, s1->命令行_include.数据, s1->命令行_include.大小);
        }
        *s1->include栈指针++ = 文件;
        新建源文件缓冲区(s1, "<command line>", cstr.大小);
        memcpy(文件->缓冲, cstr.数据, cstr.大小);
        动态字符串_释放(&cstr);
    }

    解析标志 = 是_汇编 ? 解析标志_汇编文件 : 0;
    单词标志 = 单词标志_行开始之前 | 单词标志_文件开始之前;
}

/* 清理 from error/setjmp */
静态函数 void 预处理_结束(虚拟机ST *s1)
{
    while (宏_栈)
        结束_宏扩展();
    没宏替换的字符串指针 = NULL;
    while (文件)
        关闭打开的文件();
    zhipp_delete(s1);
}



静态函数 void zhipp_delete(虚拟机ST *s)
{
    int i, n;

    动态数组_重置(&s->包含的缓存文件, &s->包含的缓存文件数量);

    /* 释放标记 */
    n = 单词_标识符 - TOK_标识符;
    if (n > 总标识符数)
        总标识符数 = n;
    for(i = 0; i < n; i++)
        小分配器_free(单词符号分配器, 标识符表[i]);
    zhi_释放(标识符表);
    标识符表 = NULL;

    /* free static buffers */
    动态字符串_释放(&当前解析的字符串);
    动态字符串_释放(&动态字符串_缓冲);
    动态字符串_释放(&宏_等于_缓冲);
    单词字符串_释放_字符串(单词字符串_缓冲.字符串);

    /* free allocators */
    小分配器_删除(单词符号分配器);
    单词符号分配器 = NULL;
    小分配器_删除(单词字符串分配器);
    单词字符串分配器 = NULL;
}

/* ------------------------------------------------------------------------- */
/* zhi -E [-P[1]] [-dD} support */

static void tok_print(const char *msg, const int *字符串)
{
    FILE *fp;
    int t, s = 0;
    常量值U cval;

    fp = 全局虚拟机->标准的输出文件;
    fprintf(fp, "%s", msg);
    while (字符串) {
	TOK_GET(&t, &字符串, &cval);
	if (!t)
	    break;
	fprintf(fp, &" %s"[s], 取单词字符串(t, &cval)), s = 1;
    }
    fprintf(fp, "\n");
}

static void pp_line(虚拟机ST *s1, 缓冲文件ST *f, int level)
{
    int d = f->当前行号 - f->最后打印_的行;

    if (s1->dflag & 4)
	return;

    if (s1->Pflag == 行_格式_NONE) {
        ;
    } else if (level == 0 && f->最后打印_的行 && d < 8) {
	while (d > 0)
	    fputs("\n", s1->标准的输出文件), --d;
    } else if (s1->Pflag == 行_格式_STD) {
	fprintf(s1->标准的输出文件, "#line %d \"%s\"\n", f->当前行号, f->文件名);
    } else {
	fprintf(s1->标准的输出文件, "# %d \"%s\"%s\n", f->当前行号, f->文件名,
	    level > 0 ? " 1" : level < 0 ? " 2" : "");
    }
    f->最后打印_的行 = f->当前行号;
}

static void define_print(虚拟机ST *s1, int v)
{
    FILE *fp;
    符号ST *s;

    s = 查找define(v);
    if (NULL == s || NULL == s->d)
        return;

    fp = s1->标准的输出文件;
    fprintf(fp, "#define %s", 取单词字符串(v, NULL));
    if (s->类型.数据类型 == MACRO_FUNC) {
        符号ST *a = s->next;
        fprintf(fp,"(");
        if (a)
            for (;;) {
                fprintf(fp,"%s", 取单词字符串(a->v & ~符号_字段, NULL));
                if (!(a = a->next))
                    break;
                fprintf(fp,",");
            }
        fprintf(fp,")");
    }
    tok_print("", s->d);
}

static void pp_debug_defines(虚拟机ST *s1)
{
    int v, t;
    const char *vs;
    FILE *fp;

    t = pp_debug_tok;
    if (t == 0)
        return;

    文件->当前行号--;
    pp_line(s1, 文件, 0);
    文件->最后打印_的行 = ++文件->当前行号;

    fp = s1->标准的输出文件;
    v = pp_debug_symv;
    vs = 取单词字符串(v, NULL);
    if (t == 英_定义 || t == 中_定义) {
        define_print(s1, v);
    } else if (t == 英_结束定义 || t == 中_结束定义) {
        fprintf(fp, "#undef %s\n", vs);
    } else if (t == TOK_push_macro) {
        fprintf(fp, "#pragma push_macro(\"%s\")\n", vs);
    } else if (t == TOK_pop_macro) {
        fprintf(fp, "#pragma pop_macro(\"%s\")\n", vs);
    }
    pp_debug_tok = 0;
}

static void 调试_内置函数(虚拟机ST *s1)
{
    int v;
    for (v = TOK_标识符; v < 单词_标识符; ++v)
        define_print(s1, v);
}

/* 在标记 a 和 b 之间添加一个空格以避免不必要的文本粘贴 */
static int 添加一个空格(int a, int b)
{
    return 'E' == a ? '+' == b || '-' == b
        : '+' == a ? TOK_自加 == b || '+' == b
        : '-' == a ? TOK_自减 == b || '-' == b
        : a >= TOK_标识符 ? b >= TOK_标识符
	: a == TOK_预处理数字 ? b >= TOK_标识符
        : 0;
}

/* 也许像 0x1e 这样的十六进制 */
static int pp_check_he0xE(int t, const char *p)
{
    if (t == TOK_预处理数字 && toup(strchr(p, 0)[-1]) == 'E')
        return 'E';
    return t;
}

/* 预处理当前文件 */
静态函数 int zhi_预处理(虚拟机ST *s1)
{
    缓冲文件ST **iptr;
    int token_seen, spcs, level;
    const char *p;
    char white[400];

    解析标志 = 解析标志_预处理 | (解析标志 & 解析标志_汇编文件) | 解析标志_换行 | 解析标志_空格 | 解析标志_转义 ;
    /* 归功于 Fabrice Bellard 的初始修订，以证明其能够自行编译和运行，前提是所有数字都以小数形式给出。 zhi -E -P10 就可以了。 */
    if (s1->Pflag == 行_格式_P10)
        解析标志 |= 解析标志_数字, s1->Pflag = 1;

    if (s1->统计_编译信息) {
	/* 预处理基本信息 */
	do 取下个单词(); while (单词编码 != TOK_文件结尾);
	return 0;
    }

    if (s1->dflag & 1) {
        调试_内置函数(s1);
        s1->dflag &= ~1;
    }

    token_seen = 英_换行, spcs = 0, level = 0;
    if (文件->上个)
        pp_line(s1, 文件->上个, level++);
    pp_line(s1, 文件, level);
    for (;;) {
        iptr = s1->include栈指针;
        取下个单词();
        if (单词编码 == TOK_文件结尾)
            break;

        level = s1->include栈指针 - iptr;
        if (level) {
            if (level > 0)
                pp_line(s1, *iptr, 0);
            pp_line(s1, 文件, level);
        }
        if (s1->dflag & 7) {
            pp_debug_defines(s1);
            if (s1->dflag & 4)
                continue;
        }

        if (是空格(单词编码)) {
            if (spcs < sizeof white - 1)
                white[spcs++] = 单词编码;
            continue;
        } else if (单词编码 == 英_换行) {
            spcs = 0;
            if (token_seen == 英_换行)
                continue;
            ++文件->最后打印_的行;
        } else if (token_seen == 英_换行) {
            pp_line(s1, 文件, 0);
        } else if (spcs == 0 && 添加一个空格(token_seen, 单词编码)) {
            white[spcs++] = ' ';
        }

        white[spcs] = 0, fputs(white, s1->标准的输出文件), spcs = 0;
        fputs(p = 取单词字符串(单词编码, &单词常量), s1->标准的输出文件);
        token_seen = pp_check_he0xE(单词编码, p);
    }
    return 0;
}

/* ------------------------------------------------------------------------- */
