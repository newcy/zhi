/* ------------------------------------------------------------- */
/* for linking rt_printline and the signal/exception handler
   from 运行.c into executables. */

#define CONFIG_ZHI_BACKTRACE_ONLY
#include "../运行.c"

int (*__rt_error)(void*, void*, const char *, va_list);

#ifndef _WIN32
# define __declspec(n)
#endif

__declspec(dllexport)
void __bt_init(运行时上下文ST *p, int num_callers)
{
    __attribute__((weak)) int main();
    __attribute__((weak)) void __bound_init(void*, int);
    struct 运行时上下文S *rc = &g_rtctxt;
    //fprintf(stderr, "__bt_init %d %p %p\n", num_callers, p->stab_sym, p->bounds_start), fflush(stderr);
    if (num_callers) {
        memcpy(rc, p, offsetof(运行时上下文ST, next));
        rc->num_callers = num_callers - 1;
        rc->top_func = main;
        __rt_error = _rt_error;
        设置异常处理();
    } else {
        p->next = rc->next, rc->next = p;
    }
}

/* copy a string and truncate it. */
static char *截取前n个字符(char *buf, size_t buf_size, const char *s)
{
    int l = strlen(s);
    if (l >= buf_size)
        l = buf_size - 1;
    memcpy(buf, s, l);
    buf[l] = 0;
    return buf;
}
