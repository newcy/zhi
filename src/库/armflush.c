/* armflush.c - flush the instruction cache

   __clear_cache is used in ����.c,  It is a built-in
   intrinsic with gcc.  However zhi in order to compile
   itself needs this function */

#ifdef __ZHIXIN__

/* syscall wrapper */
unsigned _zhisyscall(unsigned syscall_nr, ...);

/* arm-zhi supports only fake asm currently */
__asm__(
    ".global _zhisyscall\n"
    "_zhisyscall:\n"
    "push    {r7, lr}\n\t"
    "mov     r7, r0\n\t"
    "mov     r0, r1\n\t"
    "mov     r1, r2\n\t"
    "mov     r2, r3\n\t"
    "svc     #0\n\t"
    "pop     {r7, pc}"
    );

/* from unistd.h: */
#if defined(__thumb__) || defined(__ARM_EABI__)
# define __NR_SYSCALL_BASE      0x0
#else
# define __NR_SYSCALL_BASE      0x900000
#endif
#define __ARM_NR_BASE           (__NR_SYSCALL_BASE+0x0f0000)
#define __ARM_NR_cacheflush     (__ARM_NR_BASE+2)

#define syscall _zhisyscall

#else

#define _GNU_SOURCE
#include <unistd.h>
#include <sys/syscall.h>
#include <stdio.h>

#endif

/* Flushing for zhirun */
void __clear_cache(void *beginning, void *end)
{
/* __ARM_NR_cacheflush is kernel private and should not be used in user space.
 * However, there is no ARM asm parser in zhi so we use it for now */
    syscall(__ARM_NR_cacheflush, beginning, end, 0);
}
