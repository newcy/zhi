/****************************************************************************************************
 * 项目：z工具箱.c - 额外的工具和对-m32/64的支持。
 * 描述：这个程序是用来制作的运行时库.a而没有ar；
 * Copyright (c) 2007 Timppa
 ****************************************************************************************************/
#include "zhi.h"

#define ARFMAG "`\n"

typedef struct {
    char ar_name[16];
    char ar_date[12];
    char ar_uid[6];
    char ar_gid[6];
    char ar_mode[8];
    char ar_size[10];
    char ar_fmag[2];
} ArHdr;

static unsigned long le2belong(unsigned long ul) {
    return ((ul & 0xFF0000)>>8)+((ul & 0xFF000000)>>24) + ((ul & 0xFF)<<24)+((ul & 0xFF00)<<8);
}

/* 如果s包含list中的任何字符，则返回1，否则返回0 */
static int 包含任何字符(const char *s, const char *list) {
  const char *l;
  for (; *s; s++) {
      for (l = list; *l; l++) {
          if (*s == *l)
              return 1;
      }
  }
  return 0;
}

static int ar_用法(int ret) {
    fprintf(stderr, "用法: zhi -ar [rcsv] 库 文件...\n");
    fprintf(stderr, "创建库（不支持[abdioptxN]）.\n");
    return ret;
}

静态函数 int ar工具(虚拟机ST *s1, int argc, char **argv)
{
    static const ArHdr arhdr_init = {
        "/               ",
        "            ",
        "0     ",
        "0     ",
        "0       ",
        "          ",
        ARFMAG
        };

    ArHdr arhdr = arhdr_init;
    ArHdr arhdro = arhdr_init;

    FILE *fi, *fh = NULL, *fo = NULL;
    ElfW(Ehdr) *ehdr;
    ElfW(Shdr) *shdr;
    ElfW(Sym) *sym;
    int i, fsize, i_lib, i_obj;
    char *buf, *shstr, *符号表节 = NULL, *strtab = NULL;
    int symtabsize = 0;//, strtabsize = 0;
    char *anames = NULL;
    int *afpos = NULL;
    int istrlen, strpos = 0, fpos = 0, funccnt = 0, funcmax, hofs;
    char tfile[260], stmp[20];
    char *文件, *name;
    int ret = 2;
    const char *ops_conflict = "habdioptxN";  // 不受支持，但如果忽视，则具有破坏性
    int 显示详情 = 0;

    i_lib = 0; i_obj = 0;  // 将保存lib和第一个obj的索引
    for (i = 1; i < argc; i++) {
        const char *a = argv[i];
        if (*a == '-' && strstr(a, "."))
            ret = 1; // -x、 y始终无效（与gnu ar相同）
        if ((*a == '-') || (i == 1 && !strstr(a, "."))) {  // options argument
            if (包含任何字符(a, ops_conflict))
                ret = 1;
            if (strstr(a, "v"))
                显示详情 = 1;
        } else {  // 库 or obj files: don't abort - keep validating all args.
            if (!i_lib)  // first 文件 is the 库
                i_lib = i;
            else if (!i_obj)  // second 文件 is the first obj
                i_obj = i;
        }
    }

    if (!i_obj)  // i_obj implies also i_lib. we require both.
        ret = 1;

    if (ret == 1)
        return ar_用法(ret);

    if ((fh = fopen(argv[i_lib], "wb")) == NULL)
    {
        fprintf(stderr, "zhi: ar: 无法打开文件 %s \n", argv[i_lib]);
        goto 结_束;
    }

    sprintf(tfile, "%s.tmp", argv[i_lib]);
    if ((fo = fopen(tfile, "wb+")) == NULL)
    {
        fprintf(stderr, "zhi: ar: 无法创建临时文件 %s\n", tfile);
        goto 结_束;
    }

    funcmax = 250;
    afpos = zhi_重新分配(NULL, funcmax * sizeof *afpos); // 250 func
    memcpy(&arhdro.ar_mode, "100666", 6);

    // i_obj = first input object 文件
    while (i_obj < argc)
    {
        if (*argv[i_obj] == '-') {  // by now, all options start with '-'
            i_obj++;
            continue;
        }
        if ((fi = fopen(argv[i_obj], "rb")) == NULL) {
            fprintf(stderr, "zhi: ar: 无法打开文件 %s \n", argv[i_obj]);
            goto 结_束;
        }
        if (显示详情)
            printf("a - %s\n", argv[i_obj]);

        fseek(fi, 0, SEEK_END);
        fsize = ftell(fi);
        fseek(fi, 0, SEEK_SET);
        buf = zhi_分配内存(fsize + 1);
        fread(buf, fsize, 1, fi);
        fclose(fi);

        // elf header
        ehdr = (ElfW(Ehdr) *)buf;
        if (ehdr->e_ident[4] != ELFCLASSW)
        {
            fprintf(stderr, "zhi: ar: 不支持的Elf类型: %s\n", argv[i_obj]);
            goto 结_束;
        }

        shdr = (ElfW(Shdr) *) (buf + ehdr->e_shoff + ehdr->e_shstrndx * ehdr->e_shentsize);
        shstr = (char *)(buf + shdr->文件偏移量);
        for (i = 0; i < ehdr->e_shnum; i++)
        {
            shdr = (ElfW(Shdr) *) (buf + ehdr->e_shoff + i * ehdr->e_shentsize);
            if (!shdr->文件偏移量)
                continue;
            if (shdr->sh_type == SHT_符号表)
            {
                符号表节 = (char *)(buf + shdr->文件偏移量);
                symtabsize = shdr->sh_size;
            }
            if (shdr->sh_type == SHT_字符串表)
            {
                if (!strcmp(shstr + shdr->sh_name, ".字符串表"))
                {
                    strtab = (char *)(buf + shdr->文件偏移量);
                    //strtabsize = shdr->sh_size;
                }
            }
        }

        if (符号表节 && symtabsize)
        {
            int nsym = symtabsize / sizeof(ElfW(Sym));
            //printf("符号表节: info 大小 shndx name\n");
            for (i = 1; i < nsym; i++)
            {
                sym = (ElfW(Sym) *) (符号表节 + i * sizeof(ElfW(Sym)));
                if (sym->st_shndx &&
                    (sym->st_info == 0x10
                    || sym->st_info == 0x11
                    || sym->st_info == 0x12
                    )) {
                    //printf("符号表节: %2Xh %4Xh %2Xh %s\n", sym->st_info, sym->st_size, sym->st_shndx, strtab + sym->st_name);
                    istrlen = strlen(strtab + sym->st_name)+1;
                    anames = zhi_重新分配(anames, strpos+istrlen);
                    strcpy(anames + strpos, strtab + sym->st_name);
                    strpos += istrlen;
                    if (++funccnt >= funcmax) {
                        funcmax += 250;
                        afpos = zhi_重新分配(afpos, funcmax * sizeof *afpos); // 250 func more
                    }
                    afpos[funccnt] = fpos;
                }
            }
        }

        文件 = argv[i_obj];
        for (name = strchr(文件, 0);
             name > 文件 && name[-1] != '/' && name[-1] != '\\';
             --name);
        istrlen = strlen(name);
        if (istrlen >= sizeof(arhdro.ar_name))
            istrlen = sizeof(arhdro.ar_name) - 1;
        memset(arhdro.ar_name, ' ', sizeof(arhdro.ar_name));
        memcpy(arhdro.ar_name, name, istrlen);
        arhdro.ar_name[istrlen] = '/';
        sprintf(stmp, "%-10d", fsize);
        memcpy(&arhdro.ar_size, stmp, 10);
        fwrite(&arhdro, sizeof(arhdro), 1, fo);
        fwrite(buf, fsize, 1, fo);
        zhi_释放(buf);
        i_obj++;
        fpos += (fsize + sizeof(arhdro));
    }
    hofs = 8 + sizeof(arhdr) + strpos + (funccnt+1) * sizeof(int);
    fpos = 0;
    if ((hofs & 1)) // 对齐
        hofs++, fpos = 1;
    // write header
    fwrite("!<arch>\n", 8, 1, fh);
    sprintf(stmp, "%-10d", (int)(strpos + (funccnt+1) * sizeof(int)));
    memcpy(&arhdr.ar_size, stmp, 10);
    fwrite(&arhdr, sizeof(arhdr), 1, fh);
    afpos[0] = le2belong(funccnt);
    for (i=1; i<=funccnt; i++)
        afpos[i] = le2belong(afpos[i] + hofs);
    fwrite(afpos, (funccnt+1) * sizeof(int), 1, fh);
    fwrite(anames, strpos, 1, fh);
    if (fpos)
        fwrite("", 1, 1, fh);
    // write objects
    fseek(fo, 0, SEEK_END);
    fsize = ftell(fo);
    fseek(fo, 0, SEEK_SET);
    buf = zhi_分配内存(fsize + 1);
    fread(buf, fsize, 1, fo);
    fwrite(buf, fsize, 1, fh);
    zhi_释放(buf);
    ret = 0;
结_束:
    if (anames)
        zhi_释放(anames);
    if (afpos)
        zhi_释放(afpos);
    if (fh)
        fclose(fh);
    if (fo)
        fclose(fo), remove(tfile);
    return ret;
}

/* -------------------------------------------------------------- */
/*
 *tiny_impdef创建导出定义文件 （.def）从dll在微软视窗上。用法：tiny_impdef库。dll[-o 输出文件]”
 *  Copyright (c) 2005,2007 grischka
 */

#ifdef 目标_PE

静态函数 int 创建导出定义文件(虚拟机ST *s1, int argc, char **argv)
{
    int ret, v, i;
    char infile[260];
    char 输出文件名[260];

    const char *文件;
    char *p, *q;
    FILE *fp, *op;

#ifdef _WIN32
    char path[260];
#endif

    infile[0] = 输出文件名[0] = 0;
    fp = op = NULL;
    ret = 1;
    p = NULL;
    v = 0;

    for (i = 1; i < argc; ++i) {
        const char *a = argv[i];
        if ('-' == a[0]) {
            if (0 == strcmp(a, "-v")) {
                v = 1;
            } else if (0 == strcmp(a, "-o")) {
                if (++i == argc)
                    goto usage;
                strcpy(输出文件名, argv[i]);
            } else
                goto usage;
        } else if (0 == infile[0])
            strcpy(infile, a);
        else
            goto usage;
    }

    if (0 == infile[0]) {
usage:
        fprintf(stderr,
            "usage: zhi -impdef library.dll [-v] [-o outputfile]\n"
            "从 dll 创建导出定义文件 (.def)\n"
            );
        goto 结_束;
    }

    if (0 == 输出文件名[0]) {
        strcpy(输出文件名, 取文件名含扩展名(infile));
        q = strrchr(输出文件名, '.');
        if (NULL == q)
            q = strchr(输出文件名, 0);
        strcpy(q, ".def");
    }

    文件 = infile;
#ifdef _WIN32
    if (SearchPath(NULL, 文件, ".dll", sizeof path, path, NULL))
        文件 = path;
#endif
    ret = zhi_获取_dll导出(文件, &p);
    if (ret || !p) {
        fprintf(stderr, "zhi: impdef: %s '%s'\n",
            ret == -1 ? "找不到文件" :
            ret ==  1 ? "看不懂符号" :
            ret ==  0 ? "未找到任何符号在" :
            "未知的文件类型", 文件);
        ret = 1;
        goto 结_束;
    }

    if (v)
        printf("-> %s\n", 文件);

    op = fopen(输出文件名, "wb");
    if (NULL == op) {
        fprintf(stderr, "zhi: impdef: 无法创建输出文件: %s\n", 输出文件名);
        goto 结_束;
    }

    fprintf(op, "LIBRARY %s\n\nEXPORTS\n", 取文件名含扩展名(文件));
    for (q = p, i = 0; *q; ++i) {
        fprintf(op, "%s\n", q);
        q += strlen(q) + 1;
    }

    if (v)
        printf("<- %s (%d symbol%s)\n", 输出文件名, i, &"s"[i<2]);

    ret = 0;

结_束:
    if (p)
        zhi_释放(p);
    if (fp)
        fclose(fp);
    if (op)
        fclose(op);
    return ret;
}

#endif /* 目标_PE */

/* -------------------------------------------------------------- */
/*
 *  Copyright (c) 2001-2004 Fabrice Bellard
 */

/*使用zhi-m32/-m64重新执行i386/x86_64交叉编译器： */

#if !defined 目标_I386 && !defined 目标_X86_64

静态函数 void 交叉工具(虚拟机ST *s1, char **argv, int option)
{
    错_误("-m%d 未实现", option);
}

#else
#ifdef _WIN32
#include <process.h>

static char *str_replace(const char *字符串, const char *p, const char *r)
{
    const char *s, *s0;
    char *d, *d0;
    int sl, pl, rl;

    sl = strlen(字符串);
    pl = strlen(p);
    rl = strlen(r);
    for (d0 = NULL;; d0 = zhi_分配内存(sl + 1)) {
        for (d = d0, s = 字符串; s0 = s, s = strstr(s, p), s; s += pl) {
            if (d) {
                memcpy(d, s0, sl = s - s0), d += sl;
                memcpy(d, r, rl), d += rl;
            } else
                sl += rl - pl;
        }
        if (d) {
            strcpy(d, s0);
            return d0;
        }
    }
}

static int execvp_win32(const char *prog, char **argv)
{
    int ret; char **p;
    /* 将所有替换为“\” */
    for (p = argv; *p; ++p)
        if (strchr(*p, '"'))
            *p = str_replace(*p, "\"", "\\\"");
    ret = _spawnvp(P_NOWAIT, prog, (const char *const*)argv);
    if (-1 == ret)
        return ret;
    _cwait(&ret, ret, WAIT_CHILD);
    exit(ret);
}
#define execvp execvp_win32
#endif /* _WIN32 */

静态函数 void 交叉工具(虚拟机ST *s1, char **argv, int target)
{
    char program[4096];
    char *a0 = argv[0];
    int prefix = 取文件名含扩展名(a0) - a0;

    snprintf(program, sizeof program,
        "%.*s%s"
#ifdef 目标_PE
        "-win32"
#endif
        "-zhi"
#ifdef _WIN32
        ".exe"
#endif
        , prefix, a0, target == 64 ? "x86_64" : "i386");

    if (strcmp(a0, program))
        execvp(argv[0] = program, argv);
    错_误("无法运行 '%s'", program);
}

#endif /* 目标_I386 && 目标_X86_64 */
/* -------------------------------------------------------------- */
/* 启用命令行通配符扩展（zhi-o x.exe*.c）*/

#ifdef _WIN32
const int _CRT_glob = 1;
#ifndef _CRT_glob
const int _dowildcard = 1;
#endif
#endif

/* -------------------------------------------------------------- */
/* 生成  xxx.d 文件 */

static char *escape_target_dep(const char *s) {
    char *res = zhi_分配内存(strlen(s) * 2 + 1);
    int j;
    for (j = 0; *s; s++, j++) {
        if (是空格(*s)) {
            res[j++] = '\\';
        }
        res[j] = *s;
    }
    res[j] = '\0';
    return res;
}

静态函数 void 生成make依赖(虚拟机ST *s1, const char *target, const char *文件名)
{
    FILE *depout;
    char buf[1024], *escaped_target;
    int i, k;

    if (!文件名) {
        /* 自动计算文件名: dir/文件.o -> dir/文件.d */
        snprintf(buf, sizeof buf, "%.*s.d",
            (int)(取文件扩展名(target) - target), target);
        文件名 = buf;
    }

    if (s1->显示详情)
        printf("<- %s\n", 文件名);

    if(!strcmp(文件名, "-"))
        depout = fdopen(1, "w");
    else
        /* XXX 返回错误代码，而不是 错误（） ? */
        depout = fopen(文件名, "w");
    if (!depout)
        错_误("无法打开 '%s'", 文件名);
    fprintf(depout, "%s:", target);
    for (i = 0; i<s1->目标_依赖项数量; ++i) {
        for (k = 0; k < i; ++k)
            if (0 == strcmp(s1->目标_依赖项数组[i], s1->目标_依赖项数组[k]))
                goto next;
        escaped_target = escape_target_dep(s1->目标_依赖项数组[i]);
        fprintf(depout, " \\\n  %s", escaped_target);
        zhi_释放(escaped_target);
    next:;
    }
    fprintf(depout, "\n");
    fclose(depout);
}


/**************************************************zhi.c--开始**************************************************/

static const char 帮助列表[] =
	"————————————————————————————————————————————————\n"
    "知心编译器 "版本号" - 版权(C) 2001-2006 Fabrice Bellard 版权(C) 2019-2023 位中原\n"
	"————————————————————————————————————————————————\n"
    "用法: zhi [选项...] [-o 输出文件] [-c] 输入文件(s)...\n"
    "     zhi [选项...] -run 输入文件 [参数...]\n"
	"————————————————————————————————————————————————\n"
    "常用选项:\n"
    "  -c              仅编译 - 生成一个目标文件\n"
    "  -o              设置输出文件名\n"
    "  -run            运行编译源码\n"
    "  -fflag          设置或重置（带有 'no-' 前缀）'flag'（见 zhi -h）\n"
    "  -std=c99        符合 ISO 1999 C 标准（默认）.\n"
    "  -std=c11        符合 ISO 2011 C 标准.\n"
    "  -Wwarning       设置或重置（带有 'no-' 前缀）'警告'（见 zhi -h）\n"
    "  -w              禁用所有警告\n"
    "  -v --version    显示版本\n"
    "  -vv             显示搜索路径或加载的文件\n"
    "  -h              显示编译器帮助信息\n"
	"  -about          显示编译器简介\n"
    "  -bench          显示编译统计信息\n"
    "  -               使用标准输入管道作为 infile\n"
    "  @listfile       从列表文件中读取参数\n"
	"————————————————————————————————————————————————\n"
    "预处理器选项:\n"
    "  -Idir           添加包含路径“dir”\n"
    "  -Dsym[=val]     用值 'val' 定义 'sym'\n"
    "  -Usym           未定义的'符号'\n"
    "  -E              仅预处理\n"
    "  -C              保持注释（尚未实施）\n"
	"————————————————————————————————————————————————\n"
    "链接器选项:\n"
    "  -Ldir           添加库路径'dir'\n"
    "  -llib           链接动态或静态库“lib”\n"
    "  -r              生成（可重定位）目标文件\n"
    "  -shared         生成共享库/dll\n"
    "  -rdynamic       将所有全局符号导出到动态链接器\n"
    "  -soname         为运行时使用的共享库设置名称\n"
    "  -Wl,-opt[=val]  设置链接器选项（参见 zhi -h）\n"
	"————————————————————————————————————————————————\n"
    "调试器选项:\n"
    "  -g              生成运行时调试信息\n"
#ifdef 启用边界检查M
    "  -b              使用内置内存和边界检查器进行编译（暗示 -g）\n"
#endif
#ifdef 启用内置堆栈回溯M
    "  -bt[N]          链接与回溯（堆栈转储）支持 [显示最多 N 个调用者]\n"
#endif
	"————————————————————————————————————————————————\n"
    "杂项:\n"
    "  -x[c|a|b|n]     指定下一个 infile 的类型 (C,ASM,BIN,NONE)\n"
    "  -nostdinc       不使用标准系统包含路径\n"
    "  -nostdlib       不要与标准 crt 和库链接\n"
    "  -Bdir           设置zhi的私有include/library目录\n"
    "  -M[M]D          生成make依赖文件【忽略系统文件】\n"
    "  -M[M]           如上所述但没有其他输出\n"
    "  -MF 文件        指定依赖项文件名\n"
#if defined(目标_I386) || defined(目标_X86_64)
    "  -m32/64         遵循 i386/x86_64 交叉编译器\n"
#endif
	"————————————————————————————————————————————————\n"
    "工具:\n"
    "  create library  : zhi -ar [rcsv] 库.a 文件\n"
#ifdef 目标_PE
    "  create def 文件 : zhi -impdef 库.dll [-v] [-o 库.def]\n"
#endif
	"————————————————————————————————————————————————\n"
    "特殊选项:\n"
    "  -P -P1                        带 -E：无/替代 #line 输出\n"
    "  -dD -dM                       带 -E: 输出 #define 指令\n"
    "  -pthread                      与 -D_REENTRANT 和 -lpthread 相同\n"
    "  -On                           与 n 的 -D__OPTIMIZE__ 相同 > 0\n"
    "  -Wp,-opt                      与 -opt 相同\n"
    "  -include 文件                 在每个输入文件上方包含“文件”\n"
    "  -isystem dir                  将“dir”添加到系统包含路径\n"
    "  -static                       链接到静态库（不推荐）\n"
    "  -dumpversion                  打印版本\n"
    "  -print-search-dirs            打印搜索路径\n"
    "  -dt                           使用 -run/-E: 自动定义 'test_...' 宏\n"
	"————————————————————————————————————————————————\n"
    "忽略的选项:\n"
    "  -arch -C --param -pedantic -pipe -s -traditional\n"
	"————————————————————————————————————————————————\n"
    "-W[no-]...                      警告:\n"
    "  all                           打开一些 (*) 警告\n"
    "  error[=warning]               警告后停止（任何或指定）\n"
    "  write-strings                 字符串是常量\n"
    "  unsupported                   警告忽略的选项、编译指示等\n"
    "  implicit-function-declaration 警告丢失原型 (*)\n"
    "  discarded-限定符                删除 const 时发出警告 (*)\n"
	"————————————————————————————————————————————————\n"
    "-f[no-]... 标志:\n"
    "  unsigned-char                 默认字符是无符号的\n"
    "  signed-char                   默认字符有符号的\n"
    "  common                        使用公共节而不是 bss\n"
    "  leading-underscore            修饰外部符号\n"
    "  ms-extensions                 允许在结构体中使用匿名结构体\n"
    "  dollars-in-identifiers        允许在 C 符号中使用 '$'\n"
    "  test-coverage                 创建代码覆盖代码\n"
	"————————————————————————————————————————————————\n"
    "-m... 特定选项:\n"
    "  ms-bitfields                  使用 MSVC 位域布局\n"
#ifdef 目标_ARM
    "  float-abi                     hard/softfp on arm\n"
#endif
#ifdef 目标_X86_64
    "  no-sse                        在 x86_64 上禁用浮动\n"
#endif
	"————————————————————————————————————————————————\n"
    "-Wl,... 链接器选项:\n"
    "  -nostdlib                     do 不与标准 crt/libs 链接\n"
    "  -[no-]whole-archive           完全/仅根据需要加载库\n"
    "  -export-all-symbols           与 -rdynamic 一样\n"
    "  -export-dynamic               与 -rdynamic 一样\n"
    "  -image-base= -Ttext=          设置可执行文件的基地址\n"
    "  -section-alignment=           在可执行文件中设置节对齐\n"
#ifdef 目标_PE
    "  -文件-alignment=              设置PE文件对齐\n"
    "  -stack=                       设置 PE 堆栈保留\n"
    "  -large-address-aware          设置相关PE选项\n"
    "  -subsystem=[console/windows]  设置 PE 子系统\n"
    "  -oformat=[pe-* binary]        设置可执行输出格式\n"
	"————————————————————————————————————————————————\n"
    "预定义宏:\n"
    "  zhi -E -dM - < nul\n"
#else
    "  -rpath=                       设置动态库搜索路径\n"
    "  -enable-new-dtags             设置 DT_RUNPATH 而不是 DT_RPATH\n"
    "  -soname=                      设置 DT_SONAME 精灵标签\n"
    "  -Bsymbolic                    设置 DT_SYMBOLIC elf精灵标签\n"
    "  -oformat=[elf32/64-* binary]  设置可执行输出格式\n"
    "  -init= -fini= -as-needed -O   (忽略)\n"
    "预定义宏:\n"
    "  zhi -E -dM - < /dev/null\n"
#endif
    "另请参见手册以获取更多详细信息.\n"
	"————————————————————————————————————————————————\n"
    ;
static const char 关于[] =
	"————————————————————————————————————————————————\n"
	"知心编译器-全中文开源编译器，为汉语编程爱好者提供更好更高效的编译器。\n"
	"码云：https://gitee.com/zhi-yu-yan/zhi.git\n"
	"官网：www.880.xin\n"
	"邮箱：121118882@qq.com\n"
	"————————————————————————————————————————————————\n"
	;
static const char 含目标机器的版本信息[] =
    "知心编译器 "版本号
#ifdef ZHI_GITHASH
    " "ZHI_GITHASH
#endif
    " ("
#ifdef 目标_I386
        "i386"
#elif defined 目标_X86_64
        "x86_64"
#elif defined 目标_C67
        "C67"
#elif defined 目标_ARM
        "ARM"
# ifdef ZHI_ARM_EABI
        " eabi"
#  ifdef ZHI_ARM_HARDFLOAT
        "hf"
#  endif
# endif
#elif defined 目标_ARM64
        "AArch64"
#elif defined 目标_RISCV64
        "riscv64"
#endif
#ifdef 目标_PE
        " Windows"
#elif defined(目标_MACHO)
        " Darwin"
#elif 目标系统_FreeBSD || 目标系统_FreeBSD_kernel
        " FreeBSD"
#elif 目标系统_OpenBSD
        " OpenBSD"
#elif 目标系统_NetBSD
        " NetBSD"
#else
        " Linux"
#endif
    ")\n"
    ;

static void 打印目录(const char *msg, char **paths, int 路径数量)
{
    int i;
    printf("%s:\n%s", msg, 路径数量 ? "" : "  -\n");
    for(i = 0; i < 路径数量; i++)
        printf("  %s\n", paths[i]);
}

static void 打印查找目录(虚拟机ST *s)
{
    printf("安装目录: %s\n", s->zhi的安装目录);
    /* 打印目录("programs", NULL, 0); */
    打印目录("导入目录", s->系统导入路径数组, s->系统导入路径数量);
    打印目录("库目录", s->库_路径数组, s->库_路径_数量);
#ifdef 目标_PE
    printf("运行时库:\n  %s/库/"运行时库M"\n", s->zhi的安装目录);
#else
    printf("运行时库:\n  %s/"运行时库M"\n", s->zhi的安装目录);
    打印目录("crt", s->目标文件路径数组, s->目标文件路径数量);
    printf("elfinterp:\n  %s\n",  默认ELF解释器M(s));
#endif
}

static void 设置运行环境(虚拟机ST *s)
{
    char * path;

    path = getenv("C_INCLUDE_PATH");/*创建系统环境变量：C_INCLUDE_PATH ，值填上：安装目录\include*/
    if(path != NULL) {
        添加系统导入路径(s, path);
    }
    path = getenv("CPATH");/*创建系统环境变量：CPATH ，值填上：安装目录\include*/
    if(path != NULL) {
        添加_导入路径(s, path);
    }
    path = getenv("LIBRARY_PATH");/*创建系统环境变量：LIBRARY_PATH ，值填上：安装目录\库 */
    if(path != NULL) {
        zhi_添加库路径(s, path);
    }
}

static char *默认_输出文件(虚拟机ST *s, const char *first_file)
{
    char buf[1024];
    char *扩展名;
    const char *name = "a";

    if (first_file && strcmp(first_file, "-"))
        name = 取文件名含扩展名(first_file);
    snprintf(buf, sizeof(buf), "%s", name);
    扩展名 = 取文件扩展名(buf);
#ifdef 目标_PE
    if (s->输出类型 == 输出_DLL)
        strcpy(扩展名, ".dll");
    else
    if (s->输出类型 == 输出_EXE)
        strcpy(扩展名, ".exe");
    else
#endif
    if ((s->只是_依赖 || s->输出类型 == 输出_OBJ) && !s->选项_r && *扩展名)
        strcpy(扩展名, ".o");
    else
        strcpy(buf, "a.out");
    return 字符串增加一个宽度(buf);
}

static unsigned 取_时间(void)
{
#ifdef _WIN32
    return GetTickCount();
#else
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_sec*1000 + (tv.tv_usec+500)/1000;
#endif
}
/**************************************************zhi.c--结束**************************************************/
