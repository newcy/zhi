/****************************************************************************************************
 * 项目：token可以翻译成“单词符号”，简称单词。
 * 描述：系统(单词编码,单词字符串)
 ****************************************************************************************************/
/**************************************************关键词--开始**************************************************/

/*数据类型（只在语法分析解析声明说明符的时候用到一次）*/
系统(英_无, "void")
系统(英_短整数, "short")
系统(英_整数, "int")
系统(英_长整数, "long")
系统(英_浮点, "float")
系统(英_双精度, "double")
系统(英_字符, "char")
系统(英_布尔, "_Bool")

系统(中_无, "无")
系统(中_短整数, "短整数")
系统(中_整数,  "整数")
系统(中_长整数, "长整数")
系统(中_浮点, "浮点")
系统(中_双精度, "双精度")
系统(中_字符, "字符")
系统(中_布尔, "布尔")
/*流程控制（基本上都在语法分析块中使用）*/
系统(英_如果, "if")
系统(英_否则, "else")
系统(英_选择, "switch")
系统(英_分支, "case")
系统(英_执行, "do")
系统(英_判断, "while")
系统(英_循环, "for")
系统(英_继续, "continue")
系统(英_转到, "goto")
系统(英_跳出, "break")
系统(英_返回, "return")

系统(中_如果, "如果")
系统(中_否则, "否则")
系统(中_选择, "选择")
系统(中_分支, "分支")
系统(中_执行, "执行")
系统(中_判断, "判断")
系统(中_循环, "循环")
系统(中_继续, "继续")
系统(中_转到, "转到")
系统(中_跳出, "跳出")
系统(中_返回, "返回")
/*其他*/
系统(英_外部, "extern")
系统(英_静态, "static")
系统(英_无符号, "unsigned")
系统(英_有符号, "signed")
系统(英_取类型, "typeof")
系统(英_取大小, "sizeof")
系统(英_常量, "const")
系统(英_寄存器, "register")
系统(英_内联, "inline")
系统(英_易变, "volatile")
系统(英_结构体, "struct")
系统(英_共用体, "union")
系统(英_枚举, "enum")
系统(英_类型定义, "typedef")
系统(英_默认, "default")
系统(英_自动, "auto")
系统(英_限制, "restrict")
系统(英_静态_断言, "_Static_assert")

系统(中_外部, "外部")
系统(中_静态, "静态")
系统(中_无符号, "无符号")
系统(中_有符号, "有符号")
系统(中_取类型, "取类型")
系统(中_取大小, "取大小")
系统(中_常量, "常量")
系统(中_寄存器, "寄存器")
系统(中_内联, "内联")
系统(中_易变, "易变")
系统(中_结构体, "结构体")
系统(中_共用体, "共用体")
系统(中_枚举, "枚举")
系统(中_类型定义, "类型定义")
系统(中_默认, "默认")
系统(中_自动, "自动")
系统(中_限制, "限制")
系统(中_静态_断言, "静态断言")

系统(英__原子, "_Atomic")
系统(TOK_INLINE2, "__inline") /* gcc keyword */
系统(TOK_INLINE3, "__inline__") /* gcc keyword */
系统(TOK_RESTRICT2, "__restrict")
系统(TOK_RESTRICT3, "__restrict__")
系统(英_扩展, "__extension__") /* gcc keyword */
系统(TOK_GENERIC, "_Generic")
系统(TOK_ATTRIBUTE1, "__attribute")
系统(TOK_ATTRIBUTE2, "__attribute__")
系统(TOK_ALIGNOF1, "__alignof")
系统(TOK_ALIGNOF2, "__alignof__")
系统(TOK_ALIGNOF3, "_Alignof")
系统(英_对齐, "_Alignas")
系统(TOK_LABEL, "__label__")
系统(TOK_ASM1, "asm")
系统(TOK_ASM2, "__asm")
系统(TOK_ASM3, "__asm__")
#ifdef 目标_ARM64
     系统(TOK_UINT128, "__uint128_t")
#endif
/**************************************************关键词--结束**************************************************/


/*********************************************************************/
/* 以下不是关键字。 包括它们以简化解析 */
/* 仅预处理器 */

系统(英_导入, "include")
系统(英_导入下一个, "include_next")
系统(英_定义, "define")
系统(英_如果已定义, "ifdef")
系统(英_如果未定义, "ifndef")
系统(英_否则如果, "elif")
系统(英_结束如果, "endif")
系统(英_已定义, "defined")
系统(英_结束定义, "undef")
系统(英_错误, "error")
系统(英_警告, "warning")
系统(英_行号, "line")
系统(英_指示, "pragma")

系统(中_导入, "导入")
系统(中_导入下一个, "导入下一个")
系统(中_定义, "定义")
系统(中_如果已定义, "如果已定义")
系统(中_如果未定义, "如果未定义")
系统(中_否则如果, "否则如果")
系统(中_结束如果, "结束如果")
系统(中_已定义, "已定义")
系统(中_结束定义, "结束定义")
系统(中_错误, "错误")
系统(中_警告, "警告")
系统(中_行号, "行号")
系统(中_指示, "指示")

系统(TOK___LINE__, "__LINE__")
系统(TOK___FILE__, "__FILE__")
系统(TOK___DATE__, "__DATE__")
系统(TOK___TIME__, "__TIME__")
系统(TOK___FUNCTION__, "__FUNCTION__")
系统(英_可变参数, "__VA_ARGS__")
系统(TOK___COUNTER__, "__COUNTER__")
系统(TOK___HAS_INCLUDE, "__has_include")

/* 特殊标识符 */
系统(TOK___FUNC__, "__func__")

/* 特殊浮点值 */
系统(TOK___NAN__, "__nan__")
系统(TOK___SNAN__, "__snan__")
系统(TOK___INF__, "__inf__")
#if defined 目标_X86_64
     系统(TOK___mzerosf, "__mzerosf") /* -0.0 */
     系统(TOK___mzerodf, "__mzerodf") /* -0.0 */
#endif

/* 属性标识符 */
/* XXX: 一般处理所有单词，因为速度并不重要 */
系统(英_节, "section")
系统(TOK_SECTION2, "__section__")
系统(TOK_ALIGNED1, "aligned")
系统(TOK_ALIGNED2, "__aligned__")
系统(TOK_PACKED1, "packed")
系统(TOK_PACKED2, "__packed__")
系统(TOK_WEAK1, "weak")
系统(TOK_WEAK2, "__weak__")
系统(英_别名1, "alias")
系统(英_别名2, "__alias__")
系统(TOK_UNUSED1, "unused")
系统(TOK_UNUSED2, "__unused__")
系统(TOK_CDECL1, "cdecl")
系统(TOK_CDECL2, "__cdecl")
系统(TOK_CDECL3, "__cdecl__")
系统(TOK_STDCALL1, "stdcall")
系统(TOK_STDCALL2, "__stdcall")
系统(TOK_STDCALL3, "__stdcall__")
系统(TOK_FASTCALL1, "fastcall")
系统(TOK_FASTCALL2, "__fastcall")
系统(TOK_FASTCALL3, "__fastcall__")
系统(TOK_REGPARM1, "regparm")
系统(TOK_REGPARM2, "__regparm__")
系统(TOK_CLEANUP1, "清理")
系统(TOK_CLEANUP2, "__cleanup__")
系统(TOK_CONSTRUCTOR1, "constructor")
系统(TOK_CONSTRUCTOR2, "__constructor__")
系统(TOK_DESTRUCTOR1, "destructor")
系统(TOK_DESTRUCTOR2, "__destructor__")
系统(TOK_ALWAYS_INLINE1, "always_inline")
系统(TOK_ALWAYS_INLINE2, "__always_inline__")

系统(TOK_MODE, "__mode__")
系统(TOK_MODE_QI, "__QI__")
系统(TOK_MODE_DI, "__DI__")
系统(TOK_MODE_HI, "__HI__")
系统(TOK_MODE_SI, "__SI__")
系统(TOK_MODE_word, "__word__")

系统(TOK_DLLEXPORT, "dllexport")
系统(TOK_DLLIMPORT, "dllimport")
系统(TOK_NODECORATE, "节点")
系统(TOK_NORETURN1, "noreturn")
系统(TOK_NORETURN2, "__noreturn__")
系统(TOK_NORETURN3, "_Noreturn")
系统(TOK_VISIBILITY1, "能见度")
系统(TOK_VISIBILITY2, "__visibility__")

系统(TOK_builtin_types_compatible_p, "__builtin_types_compatible_p")
系统(TOK_builtin_choose_expr, "__builtin_choose_expr")
系统(TOK_builtin_constant_p, "__builtin_constant_p")
系统(TOK_builtin_frame_address, "__builtin_frame_address")
系统(TOK_builtin_return_address, "__builtin_return_address")
系统(TOK_builtin_expect, "__builtin_expect")
     /*系统(TOK_builtin_va_list, "__builtin_va_list")*/
#if defined 目标_PE && defined 目标_X86_64
     系统(TOK_builtin_va_start, "__builtin_va_start")
#elif defined 目标_X86_64
     系统(TOK_builtin_va_arg_types, "__builtin_va_arg_types")
#elif defined 目标_ARM64
     系统(TOK_builtin_va_start, "__builtin_va_start")
     系统(TOK_builtin_va_arg, "__builtin_va_arg")
#elif defined 目标_RISCV64
     系统(TOK_builtin_va_start, "__builtin_va_start")
#endif

/* 原子操作 */
#define DEF_ATOMIC(ID) 系统(TOK_##__##ID, "__"#ID)
     DEF_ATOMIC(atomic_store)
     DEF_ATOMIC(atomic_load)
     DEF_ATOMIC(atomic_exchange)
     DEF_ATOMIC(atomic_compare_exchange)
     DEF_ATOMIC(atomic_fetch_add)
     DEF_ATOMIC(atomic_fetch_sub)
     DEF_ATOMIC(atomic_fetch_or)
     DEF_ATOMIC(atomic_fetch_xor)
     DEF_ATOMIC(atomic_fetch_and)

/* pragma */
     系统(TOK_pack, "pack")
#if !defined(目标_I386) && !defined(目标_X86_64) && \
    !defined(目标_ARM) && !defined(目标_ARM64)
     /* 已经为汇编器定义 */
     系统(TOK_ASM_push, "push")
     系统(TOK_ASM_pop, "pop")
#endif
     系统(英_注解, "comment")
	 系统(中_注解, "注解")
     系统(TOK_lib, "库")
     系统(TOK_push_macro, "push_macro")
     系统(TOK_pop_macro, "pop_macro")
     系统(TOK_once, "once")
     系统(TOK_option, "option")

/* 内置函数或变量 */
#ifndef ZHI_ARM_EABI
     系统(TOK_memcpy, "memcpy")
     系统(TOK_memmove, "memmove")
     系统(TOK_memset, "memset")
     系统(TOK___divdi3, "__divdi3")
     系统(TOK___moddi3, "__moddi3")
     系统(TOK___udivdi3, "__udivdi3")
     系统(TOK___umoddi3, "__umoddi3")
     系统(TOK___ashrdi3, "__ashrdi3")
     系统(TOK___lshrdi3, "__lshrdi3")
     系统(TOK___ashldi3, "__ashldi3")
     系统(TOK___floatundisf, "__floatundisf")
     系统(TOK___floatundidf, "__floatundidf")
# ifndef ZHI_ARM_VFP
     系统(TOK___floatundixf, "__floatundixf")
     系统(TOK___fixunsxfdi, "__fixunsxfdi")
# endif
     系统(TOK___fixunssfdi, "__fixunssfdi")
     系统(TOK___fixunsdfdi, "__fixunsdfdi")
#endif

#if defined 目标_ARM
# ifdef ZHI_ARM_EABI
     系统(TOK_memcpy, "__aeabi_memcpy")
     系统(TOK_memmove, "__aeabi_memmove")
     系统(TOK_memmove4, "__aeabi_memmove4")
     系统(TOK_memmove8, "__aeabi_memmove8")
     系统(TOK_memset, "__aeabi_memset")
     系统(TOK___aeabi_ldivmod, "__aeabi_ldivmod")
     系统(TOK___aeabi_uldivmod, "__aeabi_uldivmod")
     系统(TOK___aeabi_idivmod, "__aeabi_idivmod")
     系统(TOK___aeabi_uidivmod, "__aeabi_uidivmod")
     系统(TOK___divsi3, "__aeabi_idiv")
     系统(TOK___udivsi3, "__aeabi_uidiv")
     系统(TOK___floatdisf, "__aeabi_l2f")
     系统(TOK___floatdidf, "__aeabi_l2d")
     系统(TOK___fixsfdi, "__aeabi_f2lz")
     系统(TOK___fixdfdi, "__aeabi_d2lz")
     系统(TOK___ashrdi3, "__aeabi_lasr")
     系统(TOK___lshrdi3, "__aeabi_llsr")
     系统(TOK___ashldi3, "__aeabi_llsl")
     系统(TOK___floatundisf, "__aeabi_ul2f")
     系统(TOK___floatundidf, "__aeabi_ul2d")
     系统(TOK___fixunssfdi, "__aeabi_f2ulz")
     系统(TOK___fixunsdfdi, "__aeabi_d2ulz")
# else
     系统(TOK___modsi3, "__modsi3")
     系统(TOK___umodsi3, "__umodsi3")
     系统(TOK___divsi3, "__divsi3")
     系统(TOK___udivsi3, "__udivsi3")
     系统(TOK___floatdisf, "__floatdisf")
     系统(TOK___floatdidf, "__floatdidf")
#  ifndef ZHI_ARM_VFP
     系统(TOK___floatdixf, "__floatdixf")
     系统(TOK___fixunssfsi, "__fixunssfsi")
     系统(TOK___fixunsdfsi, "__fixunsdfsi")
     系统(TOK___fixunsxfsi, "__fixunsxfsi")
     系统(TOK___fixxfdi, "__fixxfdi")
#  endif
     系统(TOK___fixsfdi, "__fixsfdi")
     系统(TOK___fixdfdi, "__fixdfdi")
# endif
#endif

#if defined 目标_C67
     系统(TOK__divi, "_divi")
     系统(TOK__divu, "_divu")
     系统(TOK__divf, "_divf")
     系统(TOK__divd, "_divd")
     系统(TOK__remi, "_remi")
     系统(TOK__remu, "_remu")
#endif

#if defined 目标_I386
     系统(TOK___fixsfdi, "__fixsfdi")
     系统(TOK___fixdfdi, "__fixdfdi")
     系统(TOK___fixxfdi, "__fixxfdi")
#endif

#if defined 目标_I386 || defined 目标_X86_64
     系统(TOK_alloca, "alloca")
#endif

#if defined 目标_PE
     系统(TOK___chkstk, "__chkstk")
#endif
#if defined 目标_ARM64 || defined 目标_RISCV64
     系统(TOK___arm64_clear_cache, "__arm64_clear_cache")
     系统(TOK___addtf3, "__addtf3")
     系统(TOK___subtf3, "__subtf3")
     系统(TOK___multf3, "__multf3")
     系统(TOK___divtf3, "__divtf3")
     系统(TOK___extendsftf2, "__extendsftf2")
     系统(TOK___extenddftf2, "__extenddftf2")
     系统(TOK___trunctfsf2, "__trunctfsf2")
     系统(TOK___trunctfdf2, "__trunctfdf2")
     系统(TOK___fixtfsi, "__fixtfsi")
     系统(TOK___fixtfdi, "__fixtfdi")
     系统(TOK___fixunstfsi, "__fixunstfsi")
     系统(TOK___fixunstfdi, "__fixunstfdi")
     系统(TOK___floatsitf, "__floatsitf")
     系统(TOK___floatditf, "__floatditf")
     系统(TOK___floatunsitf, "__floatunsitf")
     系统(TOK___floatunditf, "__floatunditf")
     系统(TOK___eqtf2, "__eqtf2")
     系统(TOK___netf2, "__netf2")
     系统(TOK___lttf2, "__lttf2")
     系统(TOK___letf2, "__letf2")
     系统(TOK___gttf2, "__gttf2")
     系统(TOK___getf2, "__getf2")
#endif

/* 边界检查符号 */
#ifdef 启用边界检查M
     系统(TOK___bound_ptr_add, "__bound_ptr_add")
     系统(TOK___bound_ptr_indir1, "__bound_ptr_indir1")
     系统(TOK___bound_ptr_indir2, "__bound_ptr_indir2")
     系统(TOK___bound_ptr_indir4, "__bound_ptr_indir4")
     系统(TOK___bound_ptr_indir8, "__bound_ptr_indir8")
     系统(TOK___bound_ptr_indir12, "__bound_ptr_indir12")
     系统(TOK___bound_ptr_indir16, "__bound_ptr_indir16")
     系统(TOK___bound_main_arg, "__bound_main_arg")
     系统(TOK___bound_local_new, "__bound_local_new")
     系统(TOK___bound_local_delete, "__bound_local_delete")
     系统(TOK___bound_setjmp, "__bound_setjmp")
     系统(TOK___bound_longjmp, "__bound_longjmp")
     系统(TOK___bound_new_region, "__bound_new_region")
# ifdef 目标_PE
#  ifdef 目标_X86_64
     系统(TOK___bound_alloca_nr, "__bound_alloca_nr")
#  endif
# else
     系统(TOK_sigsetjmp, "sigsetjmp")
     系统(TOK___sigsetjmp, "__sigsetjmp")
     系统(TOK_siglongjmp, "siglongjmp")
# endif
     系统(TOK_setjmp, "setjmp")
     系统(TOK__setjmp, "_setjmp")
     系统(TOK_longjmp, "longjmp")
#endif


/*********************************************************************/
/* 小汇编器 */
#define DEF_ASM(x) 系统(TOK_ASM_ ## x, #x)
#define DEF_ASMDIR(x) 系统(TOK_ASMDIR_ ## x, "." #x)
#define TOK_ASM_int 英_整数

#define TOK_ASMDIR_FIRST TOK_ASMDIR_byte
#define TOK_ASMDIR_LAST TOK_ASMDIR_section

 DEF_ASMDIR(byte)       /* 必须是第一个指令 */
 DEF_ASMDIR(word)
 DEF_ASMDIR(对齐)
 DEF_ASMDIR(balign)
 DEF_ASMDIR(p2align)
 DEF_ASMDIR(set)
 DEF_ASMDIR(跳过)
 DEF_ASMDIR(space)
 DEF_ASMDIR(string)
 DEF_ASMDIR(asciz)
 DEF_ASMDIR(ascii)
 DEF_ASMDIR(文件)
 DEF_ASMDIR(globl)
 DEF_ASMDIR(global)
 DEF_ASMDIR(weak)
 DEF_ASMDIR(hidden)
 DEF_ASMDIR(ident)
 DEF_ASMDIR(大小)
 DEF_ASMDIR(类型)
 DEF_ASMDIR(代码)
 DEF_ASMDIR(数据)
 DEF_ASMDIR(bss)
 DEF_ASMDIR(previous)
 DEF_ASMDIR(pushsection)
 DEF_ASMDIR(popsection)
 DEF_ASMDIR(fill)
 DEF_ASMDIR(rept)
 DEF_ASMDIR(endr)
 DEF_ASMDIR(org)
 DEF_ASMDIR(quad)
#if defined(目标_I386)
 DEF_ASMDIR(code16)
 DEF_ASMDIR(code32)
#elif defined(目标_X86_64)
 DEF_ASMDIR(code64)
#endif
 DEF_ASMDIR(short)
 DEF_ASMDIR(long)
 DEF_ASMDIR(int)
 DEF_ASMDIR(section)    /* 必须是最后一个指令 */

#if defined 目标_I386 || defined 目标_X86_64
#include "i386-单词.h"
#endif

#if defined 目标_ARM || defined 目标_ARM64
#include "arm-单词.h"
#endif

#if defined 目标_RISCV64
#include "riscv64-单词.h"
#endif

#undef 系统
