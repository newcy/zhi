/****************************************************************************************************
 * 名称：处理ELF文件
 * Copyright (c) 2001-2022 Fabrice Bellard
 ****************************************************************************************************/
#include "zhi.h"
/* 定义它以在重定位处理期间获得一些调试输出。  */
#undef 调试_重定位
/********************************************************/
/* elf版本信息 */
struct ELF符号版本S {
    char *库;
    char *version;
    int out_index;
    int prev_same_lib;
};

#define 符号版本_数量     s1->符号版本_数量
#define 符号_版本        s1->符号_版本
#define 符号数量_到_版本   s1->符号数量_到_版本
#define 符号_到_版本      s1->符号_到_版本
#define dt_版本需要节       s1->dt_版本需要节
#define 符号版本_节      s1->符号版本_节
#define 需要的版本_节     s1->需要的版本_节

/* 指示该节不应链接到其他节的特殊标志 */
#define 特殊标志_私有 0x80000000
#define 特殊标志_动态符号表节 0x40000000

/* ------------------------------------------------------------------------- */
/* 新建数据节，代码节，符号表节等*/
静态函数 void 新建数据代码符号表节(虚拟机ST *s)
{
    虚拟机ST *s1 = s;
    /* 现在没有节 数量为0*/
    动态数组_追加元素(&s->节数组, &s->节的数量, NULL);
    /* 创建标准节 */
    代码_节 = 节_新建(s, ".代码", SHT_程序数据, SHF_执行时占用内存 | SHF_可执行);
    数据_节 = 节_新建(s, ".数据", SHT_程序数据, SHF_执行时占用内存 | SHF_可写);
#ifdef 目标_PE
    只读数据_节 = 节_新建(s, ".rdata", SHT_程序数据, SHF_执行时占用内存);
#else
    /* 创建 ro 数据段（使用 GNU_RELRO 完成重定位后制作 ro） */
    只读数据_节 = 节_新建(s, ".数据.ro", SHT_程序数据, SHF_执行时占用内存 | SHF_可写);
#endif
    未初始化或初始化为0的数据_节 = 节_新建(s, ".bss", SHT_无数据, SHF_执行时占用内存 | SHF_可写);
    完全未初始化数据_节 = 节_新建(s, ".common", SHT_无数据, 特殊标志_私有);
    完全未初始化数据_节->ELF节数量 = SHN_COMMON;
    /* 总是为链接阶段生成符号 */
    节符号表_数组 = 新建符号表节(s, ".符号表节", SHT_符号表, 0,".字符串表",".哈希表", 特殊标志_私有);
    s->符号表节 = 节符号表_数组;
    /* 动态符号的私有符号表 */
    s->临时动态符号表_节 = 新建符号表节(s, ".动态符号表", SHT_符号表, 特殊标志_私有|特殊标志_动态符号表节,".动态字符串表",".动态哈希表", 特殊标志_私有);
    获取符号属性(s, 0, 1);
}

#ifdef 启用边界检查M
静态函数 void elf边界新建(虚拟机ST *s)
{
    虚拟机ST *s1 = s;
    /* 创建边界部分（使用 GNU_RELRO 完成重定位后生成 ro） */
    全局边界_节 = 节_新建(s, ".bounds",SHT_程序数据, SHF_执行时占用内存 | SHF_可写);
    局部边界_节 = 节_新建(s, ".lbounds",SHT_程序数据, SHF_执行时占用内存 | SHF_可写);
}
#endif

静态函数 void elf符号表新建(虚拟机ST *s)
{
    虚拟机ST *s1 = s;
    int shf = 0;
#ifdef 启用内置堆栈回溯M
    /* 包括具有独立回溯支持的刺戳信息 */
    if (s->执行_回溯 && s->输出类型 != 输出_内存)
        shf = SHF_执行时占用内存 | SHF_可写; // SHF_可写 needed for musl/SELINUX
#endif
    调试节_数组 = 节_新建(s, ".stab", SHT_程序数据, shf);
    调试节_数组->sh_entsize = sizeof(字符串表符号ST);
    调试节_数组->sh_地址对齐 = sizeof ((字符串表符号ST*)0)->符号_值;
    调试节_数组->link = 节_新建(s, ".stabstr", SHT_字符串表, shf);
    /* put first entry */
    放置符号表调试信息(s, "", 0, 0, 0, 0);
}

static void 释放节(节ST *s)
{
    zhi_释放(s->数据);
}

静态函数 void elf删除(虚拟机ST *s1)
{
    int i;

#ifndef ELF_OBJ_ONLY
    /* free symbol versions */
    for (i = 0; i < 符号版本_数量; i++) {
        zhi_释放(符号_版本[i].version);
        zhi_释放(符号_版本[i].库);
    }
    zhi_释放(符号_版本);
    zhi_释放(符号_到_版本);
#endif

    /* free all 节数组 */
    for(i = 1; i < s1->节的数量; i++)
        释放节(s1->节数组[i]);
    动态数组_重置(&s1->节数组, &s1->节的数量);

    for(i = 0; i < s1->私有节的数量; i++)
        释放节(s1->私有节数组[i]);
    动态数组_重置(&s1->私有节数组, &s1->私有节的数量);

    /* free any loaded DLLs */
#ifdef 运行脚本M
    for ( i = 0; i < s1->加载的DLL数量; i++) {
        DLL引用ST *引用符号 = s1->加载的DLL数组[i];
        if ( 引用符号->handle )
# ifdef _WIN32
            FreeLibrary((HMODULE)引用符号->handle);
# else
            dlclose(引用符号->handle);
# endif
    }
#endif
    /* free loaded dlls array */
    动态数组_重置(&s1->加载的DLL数组, &s1->加载的DLL数量);
    zhi_释放(s1->符号属性表);

    节符号表_数组 = NULL; /* for 运行.c:rt_printline() */
}

静态函数 void 设置节数据状态(虚拟机ST *s1)
{
    节ST *节; int i;
    for (i = 1; i < s1->节的数量; i++)
    {
        节 = s1->节数组[i];
        节->文件偏移量 = 节->当前数据_偏移量;
    }
    /* 在编译期间禁用符号哈希散列 */
    节 = s1->符号表节, 节->重定位 = 节->hash, 节->hash = NULL;
#if defined 目标_X86_64 && defined 目标_PE
    s1->uw_sym = 0;
#endif
}

/* 在编译结束时，将任何 UNDEF 符号转换为全局，并与以前存在的符号合并 */
静态函数 void elf结束文件(虚拟机ST *s1)
{
    节ST *s = s1->符号表节;
    int first_sym, nb_syms, *tr, i;

    first_sym = s->文件偏移量 / sizeof (ElfSym);
    nb_syms = s->当前数据_偏移量 / sizeof (ElfSym) - first_sym;
    s->当前数据_偏移量 = s->文件偏移量;
    s->link->当前数据_偏移量 = s->link->文件偏移量;
    s->hash = s->重定位, s->重定位 = NULL;
    tr = 初始化内存(nb_syms * sizeof *tr);

    for (i = 0; i < nb_syms; ++i) {
        ElfSym *sym = (ElfSym*)s->数据 + first_sym + i;
        if (sym->st_shndx == SHN_未定义
            && ELFW(ST_BIND)(sym->st_info) == STB_LOCAL)
            sym->st_info = ELFW(ST_INFO)(STB_GLOBAL, ELFW(ST_TYPE)(sym->st_info));
        tr[i] = 设置elf符号(s, sym->st_value, sym->st_size, sym->st_info,
            sym->st_other, sym->st_shndx, (char*)s->link->数据 + sym->st_name);
    }
    /* now update relocations */
    for (i = 1; i < s1->节的数量; i++) {
        节ST *sr = s1->节数组[i];
        if (sr->sh_type == SHT_RELX && sr->link == s) {
            ElfW_Rel *rel = (ElfW_Rel*)(sr->数据 + sr->文件偏移量);
            ElfW_Rel *rel_end = (ElfW_Rel*)(sr->数据 + sr->当前数据_偏移量);
            for (; rel < rel_end; ++rel) {
                int n = ELFW(R_SYM)(rel->r_info) - first_sym;
                //if (n < 0) 错_误("内部：重新定位中的符号索引无效");
                rel->r_info = ELFW(R_INFO)(tr[n], ELFW(R_TYPE)(rel->r_info));
            }
        }
    }
    zhi_释放(tr);

    /* record 代码/数据/bss output for -bench info */
    for (i = 0; i < 4; ++i) {
        s = s1->节数组[i + 1];
        s1->总输出[i] += s->当前数据_偏移量 - s->文件偏移量;
    }
}
/***********************************************************
 * 功能:			新建节
 * name:			节名称
 * sh_type:	节类型
 * 返回值:			新增加节
 **********************************************************/
静态函数 节ST *节_新建(虚拟机ST *s1, const char *name, int sh_type, int sh_flags)
{
    节ST *sec;
    sec = 初始化内存(sizeof(节ST) + strlen(name));
    sec->s1 = s1;
    strcpy(sec->name, name);
    sec->sh_type = sh_type;
    sec->sh_flags = sh_flags;
    switch(sh_type)
    {
    case SHT_GNU_版本符号表:
        sec->sh_地址对齐 = 2;
        break;
    case SHT_符号哈希表:
    case SHT_REL:
    case SHT_带加数的重定位条目:
    case SHT_DYNSYM:
    case SHT_符号表:
    case SHT_动态链接信息:
    case SHT_GNU_verneed:
    case SHT_GNU_verdef:
        sec->sh_地址对齐 = 宏_指针大小;
        break;
    case SHT_字符串表:
        sec->sh_地址对齐 = 1;
        break;
    default:
        sec->sh_地址对齐 =  宏_指针大小; /* gcc/pcc 默认对齐 */
        break;
    }
    if (sh_flags & 特殊标志_私有)
    {
        动态数组_追加元素(&s1->私有节数组, &s1->私有节的数量, sec);
    } else
    {
        sec->ELF节数量 = s1->节的数量;
        动态数组_追加元素(&s1->节数组, &s1->节的数量, sec);
    }

    return sec;
}

静态函数 节ST *新建符号表节(虚拟机ST *s1,const char *符号表名称, int sh_type, int sh_flags,const char *strtab_name,const char *hash_name, int hash_sh_flags)
{
    节ST *符号表节, *strtab, *hash;
    int *ptr, nb_buckets;

    符号表节 = 节_新建(s1, 符号表名称, sh_type, sh_flags);
    符号表节->sh_entsize = sizeof(ElfW(Sym));
    strtab = 节_新建(s1, strtab_name, SHT_字符串表, sh_flags);
    放置elf字符串(strtab, "");
    符号表节->link = strtab;
    取符号索引(符号表节, 0, 0, 0, 0, 0, NULL);

    nb_buckets = 1;

    hash = 节_新建(s1, hash_name, SHT_符号哈希表, hash_sh_flags);
    hash->sh_entsize = sizeof(int);
    符号表节->hash = hash;
    hash->link = 符号表节;

    ptr = 节_预留指定大小内存(hash, (2 + nb_buckets + 1) * sizeof(int));
    ptr[0] = nb_buckets;
    ptr[1] = 1;
    memset(ptr + 2, 0, (nb_buckets + 1) * sizeof(int));
    return 符号表节;
}

/***********************************************************
 * 功能:		给节数据重新分配内存,并将内容初始化为0
 * sec:			重新分配内存的节
 * new_size:	节数据新长度
 **********************************************************/
静态函数 void 节_重新分配内存且内容为0(节ST *sec, unsigned long new_size)
{
    unsigned long 大小;
    unsigned char *数据;

    大小 = sec->data_allocated;
    if (大小 == 0)
        大小 = 1;
    while (大小 < new_size)
        大小 = 大小 * 2;
    数据 = zhi_重新分配(sec->数据, 大小);
    memset(数据 + sec->data_allocated, 0, 大小 - sec->data_allocated);
    sec->数据 = 数据;
    sec->data_allocated = 大小;
}

/* 保留至少 '大小' 个字节，在从当前偏移量的 'sec' 节中按 '对齐' 对齐，并返回对齐的偏移量*/
静态函数 size_t 节_预留指定大小内存且按照align对齐(节ST *sec, addr_t 大小, int 对齐)
{
    size_t offset, offset1;

    offset = (sec->当前数据_偏移量 + 对齐 - 1) & -对齐;
    offset1 = offset + 大小;
    if (sec->sh_type != SHT_无数据 && offset1 > sec->data_allocated)
        节_重新分配内存且内容为0(sec, offset1);
    sec->当前数据_偏移量 = offset1;
    if (对齐 > sec->sh_地址对齐)
        sec->sh_地址对齐 = 对齐;
    return offset;
}

/***********************************************************
 * 功能:		给节数据预留至少size大小的内存空间
 * sec:			预留内存空间的节
 * 大小:	预留的空间大小
 * 返回值:		预留内存空间的首地址
 **********************************************************/
静态函数 void *节_预留指定大小内存(节ST *sec, addr_t 大小)
{
    size_t 偏移量 = 节_预留指定大小内存且按照align对齐(sec, 大小, 1);
    return sec->数据 + 偏移量;
}

#ifndef ELF_OBJ_ONLY
/* 从节开始至少保留“大小”字节 */
static void 节_预留(节ST *sec, unsigned long 大小)
{
    if (大小 > sec->data_allocated)
        节_重新分配内存且内容为0(sec, 大小);
    if (大小 > sec->当前数据_偏移量)
        sec->当前数据_偏移量 = 大小;
}
#endif
/* 返回对节的引用，如果它不存在则创建它 */
static 节ST *查找_节_创建 (虚拟机ST *s1, const char *name, int create)
{
    节ST *sec;
    int i;
    for(i = 1; i < s1->节的数量; i++)
    {
        sec = s1->节数组[i];
        if (!strcmp(name, sec->name))
            return sec;
    }
    /* 为程序数据创建节 */
    return create ? 节_新建(s1, name, SHT_程序数据, SHF_执行时占用内存) : NULL;
}

/* ------------------------------------------------------------------------- */

静态函数 int 放置elf字符串(节ST *s, const char *sym)
{
    int offset, 长度;
    char *ptr;

    长度 = strlen(sym) + 1;
    offset = s->当前数据_偏移量;
    ptr = 节_预留指定大小内存(s, 长度);
    memmove(ptr, sym, 长度);
    return offset;
}

/* elf符号哈希函数 */
static unsigned long elf_哈希(const unsigned char *name)
{
    unsigned long h = 0, g;

    while (*name) {
        h = (h << 4) + *name++;
        g = h & 0xf0000000;
        if (g)
            h ^= g >> 24;
        h &= ~g;
    }
    return h;
}

/* 重建s段的哈希表 */
/* 注意：我们确实分解了哈希表代码以加快速度 */
static void 重建节的哈希表(节ST *s, unsigned int nb_buckets)
{
    ElfW(Sym) *sym;
    int *ptr, *hash, nb_syms, 符号_索引, h;
    unsigned char *strtab;

    strtab = s->link->数据;
    nb_syms = s->当前数据_偏移量 / sizeof(ElfW(Sym));

    if (!nb_buckets)
        nb_buckets = ((int*)s->hash->数据)[0];

    s->hash->当前数据_偏移量 = 0;
    ptr = 节_预留指定大小内存(s->hash, (2 + nb_buckets + nb_syms) * sizeof(int));
    ptr[0] = nb_buckets;
    ptr[1] = nb_syms;
    ptr += 2;
    hash = ptr;
    memset(hash, 0, (nb_buckets + 1) * sizeof(int));
    ptr += nb_buckets + 1;

    sym = (ElfW(Sym) *)s->数据 + 1;
    for(符号_索引 = 1; 符号_索引 < nb_syms; 符号_索引++) {
        if (ELFW(ST_BIND)(sym->st_info) != STB_LOCAL) {
            h = elf_哈希(strtab + sym->st_name) % nb_buckets;
            *ptr = hash[h];
            hash[h] = 符号_索引;
        } else {
            *ptr = 0;
        }
        ptr++;
        sym++;
    }
}

/* 返回符号编号 */
静态函数 int 取符号索引(节ST *s, addr_t value, unsigned long 大小, int info, int other, int shndx, const char *name)
{
    int name_offset, 符号_索引;
    int nbuckets, h;
    ElfW(Sym) *sym;
    节ST *hs;

    sym = 节_预留指定大小内存(s, sizeof(ElfW(Sym)));
    if (name && name[0])
    {
    	name_offset = 放置elf字符串(s->link, name);
    }else
    {
    	name_offset = 0;
    }
    /* XXX: 字节序 */
    sym->st_name = name_offset;
    sym->st_value = value;
    sym->st_size = 大小;
    sym->st_info = info;
    sym->st_other = other;
    sym->st_shndx = shndx;
    符号_索引 = sym - (ElfW(Sym) *)s->数据;
    hs = s->hash;
    if (hs)
    {
        int *ptr, *base;
        ptr = 节_预留指定大小内存(hs, sizeof(int));
        base = (int *)hs->数据;
        /* 只添加全局或弱符号. */
        if (ELFW(ST_BIND)(info) != STB_LOCAL)
        {
            /* 添加另一个哈希条目 */
            nbuckets = base[0];
            h = elf_哈希((unsigned char *)s->link->数据 + name_offset) % nbuckets;
            *ptr = base[2 + h];
            base[2 + h] = 符号_索引;
            base[1]++;
            /* 我们调整哈希表的大小 */
            hs->nb_hashed_syms++;
            if (hs->nb_hashed_syms > 2 * nbuckets)
            {
                重建节的哈希表(s, 2 * nbuckets);
            }
        } else
        {
            *ptr = 0;
            base[1]++;
        }
    }
    return 符号_索引;
}

静态函数 int 查找elf符号(节ST *s, const char *name)
{
    ElfW(Sym) *sym;
    节ST *hs;
    int nbuckets, 符号_索引, h;
    const char *name1;

    hs = s->hash;
    if (!hs)
        return 0;
    nbuckets = ((int *)hs->数据)[0];
    h = elf_哈希((unsigned char *) name) % nbuckets;
    符号_索引 = ((int *)hs->数据)[2 + h];
    while (符号_索引 != 0) {
        sym = &((ElfW(Sym) *)s->数据)[符号_索引];
        name1 = (char *) s->link->数据 + sym->st_name;
        if (!strcmp(name, name1))
            return 符号_索引;
        符号_索引 = ((int *)hs->数据)[2 + nbuckets + 符号_索引];
    }
    return 0;
}

/* 返回 elf 符号值，如果 'err' 非零则信号错误，如果 FORC 则修饰名称*/
静态函数 addr_t 获取符号地址(虚拟机ST *s1, const char *name, int err, int forc)
{
    int 符号_索引;
    ElfW(Sym) *sym;
    char buf[256];
    if (forc && s1->前导下划线
#ifdef 目标_PE
        && !strchr(name, '@')/*查找在name中第一次出现@的位置*/
#endif
        )
    {
        buf[0] = '_';
        截取前n个字符(buf + 1, sizeof(buf) - 1, name);
        name = buf;
    }
    符号_索引 = 查找elf符号(s1->符号表节, name);
    sym = &((ElfW(Sym) *)s1->符号表节->数据)[符号_索引];
    if (!符号_索引 || sym->st_shndx == SHN_未定义)
    {
        if (err)
        {
        	错_误(" '%s' 未定义", name);
        }
        return (addr_t)-1;
    }
    return sym->st_value;
}

/* 返回符号值，未找到则返回NULL */
核心库接口 void *取符号值(虚拟机ST *s, const char *name)
{
    addr_t addr = 获取符号地址(s, name, 0, 1);
    return addr == -1 ? NULL : (void*)(uintptr_t)addr;
}

静态函数 void 列出elf符号名称和值(虚拟机ST *s, void *ctx,
    void (*symbol_cb)(void *ctx, const char *name, const void *val))
{
    ElfW(Sym) *sym;
    节ST *符号表节;
    int 符号_索引, end_sym;
    const char *name;
    unsigned char sym_vis, sym_bind;

    符号表节 = s->符号表节;
    end_sym = 符号表节->当前数据_偏移量 / sizeof (ElfSym);
    for (符号_索引 = 0; 符号_索引 < end_sym; ++符号_索引) {
        sym = &((ElfW(Sym) *)符号表节->数据)[符号_索引];
        if (sym->st_value) {
            name = (char *) 符号表节->link->数据 + sym->st_name;
            sym_bind = ELFW(ST_BIND)(sym->st_info);
            sym_vis = ELFW(ST_VISIBILITY)(sym->st_other);
            if (sym_bind == STB_GLOBAL && sym_vis == STV_DEFAULT)
                symbol_cb(ctx, name, (void*)(uintptr_t)sym->st_value);
        }
    }
}

核心库接口 void zhi_列出elf符号名称和值(虚拟机ST *s, void *ctx,
    void (*symbol_cb)(void *ctx, const char *name, const void *val))
{
    列出elf符号名称和值(s, ctx, symbol_cb);
}

#ifndef ELF_OBJ_ONLY
static void 版本_添加 (虚拟机ST *s1)
{
    int i;
    ElfW(Sym) *sym;
    ElfW(Verneed) *vn = NULL;
    节ST *符号表节;
    int 符号_索引, end_sym, nb_versions = 2, nb_entries = 0;
    ElfW(Half) *versym;
    const char *name;

    if (0 == 符号版本_数量)
        return;
    符号版本_节 = 节_新建(s1, ".gnu.version", SHT_GNU_版本符号表, SHF_执行时占用内存);
    符号版本_节->sh_entsize = sizeof(ElfW(Half));
    符号版本_节->link = s1->动态符号节;

    /* add needed symbols */
    符号表节 = s1->动态符号节;
    end_sym = 符号表节->当前数据_偏移量 / sizeof (ElfSym);
    versym = 节_预留指定大小内存(符号版本_节, end_sym * sizeof(ElfW(Half)));
    for (符号_索引 = 0; 符号_索引 < end_sym; ++符号_索引) {
        int dllindex, verndx;
        sym = &((ElfW(Sym) *)符号表节->数据)[符号_索引];
        name = (char *) 符号表节->link->数据 + sym->st_name;
        dllindex = 查找elf符号(s1->临时动态符号表_节, name);
        verndx = (dllindex && dllindex < 符号数量_到_版本)
                 ? 符号_到_版本[dllindex] : -1;
        if (verndx >= 0) {
            if (!符号_版本[verndx].out_index)
              符号_版本[verndx].out_index = nb_versions++;
            versym[符号_索引] = 符号_版本[verndx].out_index;
        } else
          versym[符号_索引] = 0;
    }
    /* 生成 verneed 部分，但不会在它为空时生成。 一些动态链接器即使在 DTVERNEEDNUM 和节大小为零时也会查看它们的内容。  */
    if (nb_versions > 2) {
        需要的版本_节 = 节_新建(s1, ".gnu.version_r",
                                      SHT_GNU_verneed, SHF_执行时占用内存);
        需要的版本_节->link = s1->动态符号节->link;
        for (i = 符号版本_数量; i-- > 0;) {
            struct ELF符号版本S *sv = &符号_版本[i];
            int n_same_libs = 0, 上个;
            size_t vnofs;
            ElfW(Vernaux) *vna = 0;
            if (sv->out_index < 1)
              continue;
            vnofs = 节_预留指定大小内存且按照align对齐(需要的版本_节, sizeof(*vn), 1);
            vn = (ElfW(Verneed)*)(需要的版本_节->数据 + vnofs);
            vn->vn_version = 1;
            vn->vn_file = 放置elf字符串(需要的版本_节->link, sv->库);
            vn->vn_aux = sizeof (*vn);
            do {
                上个 = sv->prev_same_lib;
                if (sv->out_index > 0) {
                    vna = 节_预留指定大小内存(需要的版本_节, sizeof(*vna));
                    vna->vna_hash = elf_哈希 ((const unsigned char *)sv->version);
                    vna->vna_flags = 0;
                    vna->vna_other = sv->out_index;
                    sv->out_index = -2;
                    vna->vna_name = 放置elf字符串(需要的版本_节->link, sv->version);
                    vna->vna_next = sizeof (*vna);
                    n_same_libs++;
                }
                if (上个 >= 0)
                  sv = &符号_版本[上个];
            } while(上个 >= 0);
            vna->vna_next = 0;
            vn = (ElfW(Verneed)*)(需要的版本_节->数据 + vnofs);
            vn->vn_cnt = n_same_libs;
            vn->vn_next = sizeof(*vn) + n_same_libs * sizeof(*vna);
            nb_entries++;
        }
        if (vn)
          vn->vn_next = 0;
        需要的版本_节->sh_info = nb_entries;
    }
    dt_版本需要节 = nb_entries;
}
#endif

/* 添加elf符号：检查它是否已经定义并修补它。 返回符号索引。 注意 ELF节数量 可以是 SHN_未定义。 */
静态函数 int 设置elf符号(节ST *s, addr_t value, unsigned long 大小,int info, int other, int shndx, const char *name)
{
    虚拟机ST *s1 = s->s1;
    ElfW(Sym) *esym;
    int sym_bind, 符号_索引, sym_type, esym_bind;
    unsigned char sym_vis, esym_vis, new_vis;
    sym_bind = ELFW(ST_BIND)(info);
    sym_type = ELFW(ST_TYPE)(info);
    sym_vis = ELFW(ST_VISIBILITY)(other);

    if (sym_bind != STB_LOCAL)
    {
        /* 我们搜索全局或弱符号*/
        符号_索引 = 查找elf符号(s, name);
        if (!符号_索引)
            goto do_def;
        esym = &((ElfW(Sym) *)s->数据)[符号_索引];
        if (esym->st_value == value && esym->st_size == 大小 && esym->st_info == info
            && esym->st_other == other && esym->st_shndx == shndx)
            return 符号_索引;
        if (esym->st_shndx != SHN_未定义)
        {
            esym_bind = ELFW(ST_BIND)(esym->st_info);
            /* 传播最受限制的可见性 */
            /* STV_DEFAULT(0)<STV_PROTECTED(3)<STV_HIDDEN(2)<STV_INTERNAL(1) */
            esym_vis = ELFW(ST_VISIBILITY)(esym->st_other);
            if (esym_vis == STV_DEFAULT)
            {
                new_vis = sym_vis;
            } else if (sym_vis == STV_DEFAULT)
            {
                new_vis = esym_vis;
            } else
            {
                new_vis = (esym_vis < sym_vis) ? esym_vis : sym_vis;
            }
            esym->st_other = (esym->st_other & ~ELFW(ST_VISIBILITY)(-1)) | new_vis;
            other = esym->st_other; /* 如果我们必须修补 esym */
            if (shndx == SHN_未定义)
            {
                /* 如果相应的符号已经定义，则忽略添加未定义的符号 */
            } else if (sym_bind == STB_GLOBAL && esym_bind == STB_WEAK)
            {
                /* 全局覆盖弱，所以补丁 */
                goto do_patch;
            } else if (sym_bind == STB_WEAK && esym_bind == STB_GLOBAL)
            {
                /* weak is ignored if already global */
            } else if (sym_bind == STB_WEAK && esym_bind == STB_WEAK)
            {
                /* keep first-found weak definition, ignore subsequents */
            } else if (sym_vis == STV_HIDDEN || sym_vis == STV_INTERNAL)
            {
                /* ignore hidden symbols after */
            } else if ((esym->st_shndx == SHN_COMMON
                            || esym->st_shndx == 未初始化或初始化为0的数据_节->ELF节数量)
                        && (shndx < SHN_LORESERVE
                            && shndx != 未初始化或初始化为0的数据_节->ELF节数量))
            {
                /* 数据 symbol gets 优先级 over common/bss */
                goto do_patch;
            } else if (shndx == SHN_COMMON || shndx == 未初始化或初始化为0的数据_节->ELF节数量)
            {
                /* 数据 symbol keeps 优先级 over common/bss */
            } else if (s->sh_flags & 特殊标志_动态符号表节)
            {
                /* we accept that two DLL define the same symbol */
	    } else if (esym->st_other & ST_ASM_SET)
	    {
		/* If the existing symbol came from an asm .set
		   we can override.  */
		goto do_patch;
            } else
            {
                错误_不中止("'%s' 定义两次", name);
            }
        } else
        {
        do_patch:
            esym->st_info = ELFW(ST_INFO)(sym_bind, sym_type);
            esym->st_shndx = shndx;
            s1->新的未定义符号 = 1;
            esym->st_value = value;
            esym->st_size = 大小;
            esym->st_other = other;
        }
    } else {
    do_def:
        符号_索引 = 取符号索引(s, value, 大小,ELFW(ST_INFO)(sym_bind, sym_type), other,shndx, name);
    }
    return 符号_索引;
}

/* put relocation */
静态函数 void 添加新的elf重定位信息(节ST *符号表节, 节ST *s, unsigned long offset,
                            int 类型, int symbol, addr_t addend)
{
    虚拟机ST *s1 = s->s1;
    char buf[256];
    节ST *sr;
    ElfW_Rel *rel;

    sr = s->重定位;
    if (!sr) {
        /* 如果没有重定位节，则创建它 */
        snprintf(buf, sizeof(buf), REL_SECTION_FMT, s->name);
        /* 如果分配了符号表节，那么我们认为重定位也是 */
        sr = 节_新建(s->s1, buf, SHT_RELX, 符号表节->sh_flags);
        sr->sh_entsize = sizeof(ElfW_Rel);
        sr->link = 符号表节;
        sr->sh_info = s->ELF节数量;
        s->重定位 = sr;
    }
    rel = 节_预留指定大小内存(sr, sizeof(ElfW_Rel));
    rel->r_offset = offset;
    rel->r_info = ELFW(R_INFO)(symbol, 类型);
#if SHT_RELX == SHT_带加数的重定位条目
    rel->r_addend = addend;
#endif
    if (SHT_RELX != SHT_带加数的重定位条目 && addend)
        错_误("REL 架构上的非零加数");
}

静态函数 void elf重定位(节ST *符号表节, 节ST *s, unsigned long offset,
                           int 类型, int symbol)
{
    添加新的elf重定位信息(符号表节, s, offset, 类型, symbol, 0);
}

静态函数 void 放置符号表调试信息(虚拟机ST *s1, const char *字符串, int 类型, int other, int desc,
                      unsigned long value)
{
    字符串表符号ST *sym;

    unsigned offset;
    if (类型 == N_SLINE
        && (offset = 调试节_数组->当前数据_偏移量)
        && (sym = (字符串表符号ST*)(调试节_数组->数据 + offset) - 1)
        && sym->符号_类型 == 类型
        && sym->符号_值 == value) {
        /* 只需更新前一个条目中的 line_number */
        sym->描述_字段 = desc;
        return;
    }

    sym = 节_预留指定大小内存(调试节_数组, sizeof(字符串表符号ST));
    if (字符串) {
        sym->字符串_索引 = 放置elf字符串(调试节_数组->link, 字符串);
    } else {
        sym->字符串_索引 = 0;
    }
    sym->符号_类型 = 类型;
    sym->其他_信息 = other;
    sym->描述_字段 = desc;
    sym->符号_值 = value;
}

静态函数 void 放置_符号表调试_重定位(虚拟机ST *s1, const char *字符串, int 类型, int other, int desc,
                        unsigned long value, 节ST *sec, int 符号_索引)
{
    elf重定位(节符号表_数组, 调试节_数组,
                  调试节_数组->当前数据_偏移量 + 8,
                  sizeof ((字符串表符号ST*)0)->符号_值 == 宏_指针大小 ? R_DATA_PTR : R_DATA_32,
                  符号_索引);
    放置符号表调试信息(s1, 字符串, 类型, other, desc, value);
}

静态函数 void put_stabn(虚拟机ST *s1, int 类型, int other, int desc, int value)
{
    放置符号表调试信息(s1, NULL, 类型, other, desc, value);
}

静态函数 struct 额外符号属性S *获取符号属性(虚拟机ST *s1, int index, int alloc)
{
    int n;
    struct 额外符号属性S *tab;

    if (index >= s1->符号属性数量) {
        if (!alloc)
            return s1->符号属性表;
        /* find immediately bigger power of 2 and reallocate array */
        n = 1;
        while (index >= n)
            n *= 2;
        tab = zhi_重新分配(s1->符号属性表, n * sizeof(*s1->符号属性表));
        s1->符号属性表 = tab;
        memset(s1->符号属性表 + s1->符号属性数量, 0,
               (n - s1->符号属性数量) * sizeof(*s1->符号属性表));
        s1->符号属性数量 = n;
    }
    return &s1->符号属性表[index];
}

/* 在ELF文件符号表中，本地符号必须出现在全局符号和弱符号的下方。由于ZHI无法在生成代码时对其进行排序，因此我们必须在之后进行排序。还修改了所有重定位表，以考虑符号表排序*/
static void 符号表排序(虚拟机ST *s1, 节ST *s)
{
    int *old_to_new_syms;
    ElfW(Sym) *new_syms;
    int nb_syms, i;
    ElfW(Sym) *p, *q;
    ElfW_Rel *rel;
    节ST *sr;
    int 类型, 符号_索引;

    nb_syms = s->当前数据_偏移量 / sizeof(ElfW(Sym));
    new_syms = zhi_分配内存(nb_syms * sizeof(ElfW(Sym)));
    old_to_new_syms = zhi_分配内存(nb_syms * sizeof(int));

    /* first pass for local symbols */
    p = (ElfW(Sym) *)s->数据;
    q = new_syms;
    for(i = 0; i < nb_syms; i++) {
        if (ELFW(ST_BIND)(p->st_info) == STB_LOCAL) {
            old_to_new_syms[i] = q - new_syms;
            *q++ = *p;
        }
        p++;
    }
    /* 在节标题中保存本地符号的数量 */
    if( s->sh_size )    /* this 'if' makes IDA happy */
        s->sh_info = q - new_syms;

    /* then second pass for non local symbols */
    p = (ElfW(Sym) *)s->数据;
    for(i = 0; i < nb_syms; i++) {
        if (ELFW(ST_BIND)(p->st_info) != STB_LOCAL) {
            old_to_new_syms[i] = q - new_syms;
            *q++ = *p;
        }
        p++;
    }

    /* we copy the new symbols to the old */
    memcpy(s->数据, new_syms, nb_syms * sizeof(ElfW(Sym)));
    zhi_释放(new_syms);

    /* now we modify all the relocations */
    for(i = 1; i < s1->节的数量; i++) {
        sr = s1->节数组[i];
        if (sr->sh_type == SHT_RELX && sr->link == s) {
            for_each_elem(sr, 0, rel, ElfW_Rel) {
                符号_索引 = ELFW(R_SYM)(rel->r_info);
                类型 = ELFW(R_TYPE)(rel->r_info);
                符号_索引 = old_to_new_syms[符号_索引];
                rel->r_info = ELFW(R_INFO)(符号_索引, 类型);
            }
        }
    }

    zhi_释放(old_to_new_syms);
}

/* 重新定位 symbol table, resolve undefined symbols if do_resolve is
   true and output error if undefined symbol. */
静态函数 void 重定位符号地址(虚拟机ST *s1, 节ST *符号表节, int do_resolve)
{
    ElfW(Sym) *sym;
    int sym_bind, ELF节数量;
    const char *name;

    for_each_elem(符号表节, 1, sym, ElfW(Sym)) {
        ELF节数量 = sym->st_shndx;
        if (ELF节数量 == SHN_未定义) {
            name = (char *) s1->符号表节->link->数据 + sym->st_name;
            /* Use ld.so to resolve symbol for us (for zhi -run) */
            if (do_resolve) {
#if defined 运行脚本M && !defined 目标_PE
                /* dlsym() needs the undecorated name.  */
                void *addr = dlsym(RTLD_DEFAULT, &name[s1->前导下划线]);
#if 目标系统_OpenBSD || 目标系统_FreeBSD || 目标系统_NetBSD
		if (addr == NULL) {
		    int i;
		    for (i = 0; i < s1->加载的DLL数量; i++)
                        if ((addr = dlsym(s1->加载的DLL数组[i]->handle, name)))
			    break;
		}
#endif
                if (addr) {
                    sym->st_value = (addr_t) addr;
#ifdef 调试_重定位
		    printf ("relocate_sym: %s -> 0x%lx\n", name, sym->st_value);
#endif
                    goto found;
                }
#endif
            /* if dynamic symbol exist, it will be used in 重新定位节 */
            } else if (s1->动态符号节 && 查找elf符号(s1->动态符号节, name))
                goto found;
            /* XXX: _fp_hw seems to be part of the ABI, so we ignore
               it */
            if (!strcmp(name, "_fp_hw"))
                goto found;
            /* only weak symbols are accepted to be undefined. Their
               value is zero */
            sym_bind = ELFW(ST_BIND)(sym->st_info);
            if (sym_bind == STB_WEAK)
                sym->st_value = 0;
            else
                错误_不中止("undefined symbol '%s'", name);
        } else if (ELF节数量 < SHN_LORESERVE) {
            /* add section base */
            sym->st_value += s1->节数组[sym->st_shndx]->sh_addr;
        }
    found: ;
    }
}

/* 通过在相关的重新定位节中应用重新定位来重新定位给定的节（取决于CPU） */
static void 重新定位节(虚拟机ST *s1, 节ST *s, 节ST *sr)
{
    ElfW_Rel *rel;
    ElfW(Sym) *sym;
    int 类型, 符号_索引;
    unsigned char *ptr;
    addr_t tgt, addr;

    qrel = (ElfW_Rel *)sr->数据;
    for_each_elem(sr, 0, rel, ElfW_Rel) {
        ptr = s->数据 + rel->r_offset;
        符号_索引 = ELFW(R_SYM)(rel->r_info);
        sym = &((ElfW(Sym) *)节符号表_数组->数据)[符号_索引];
        类型 = ELFW(R_TYPE)(rel->r_info);
        tgt = sym->st_value;
#if SHT_RELX == SHT_带加数的重定位条目
        tgt += rel->r_addend;
#endif
        addr = s->sh_addr + rel->r_offset;
        重新定位(s1, rel, 类型, ptr, addr, tgt);
    }
#ifndef ELF_OBJ_ONLY
    /* if the relocation is allocated, we change its symbol table */
    if (sr->sh_flags & SHF_执行时占用内存) {
        sr->link = s1->动态符号节;
        if (s1->输出类型 == 输出_DLL) {
            size_t r = (uint8_t*)qrel - sr->数据;
            if (sizeof ((字符串表符号ST*)0)->符号_值 < 宏_指针大小
                && 0 == strcmp(s->name, ".stab"))
                r = 0; /* cannot apply 64bit relocation to 32bit value */
            sr->当前数据_偏移量 = sr->sh_size = r;
        }
    }
#endif
}

静态函数 void 重定位_所有节(虚拟机ST *s1)
{
    int i;
    节ST *s, *sr;

    for (i = 1; i < s1->节的数量; ++i) {
        sr = s1->节数组[i];
        if (sr->sh_type != SHT_RELX)
            continue;
        s = s1->节数组[sr->sh_info];
#ifndef 目标_MACHO
        if (s != s1->got
            || s1->静态_链接
            || s1->输出类型 == 输出_内存)
#endif
        {
            重新定位节(s1, s, sr);
        }
#ifndef ELF_OBJ_ONLY
        if (sr->sh_flags & SHF_执行时占用内存) {
            ElfW_Rel *rel;
            /* 重新定位 relocation table in 'sr' */
            for_each_elem(sr, 0, rel, ElfW_Rel)
                rel->r_offset += s->sh_addr;
        }
#endif
    }
}

#ifndef ELF_OBJ_ONLY
/* count the number of dynamic relocations so that we can reserve
   their space */
static int prepare_dynamic_rel(虚拟机ST *s1, 节ST *sr)
{
    int count = 0;
#if defined(目标_I386) || defined(目标_X86_64) || \
    defined(目标_ARM) || defined(目标_ARM64) || \
    defined(目标_RISCV64)
    ElfW_Rel *rel;
    for_each_elem(sr, 0, rel, ElfW_Rel) {
        int 符号_索引 = ELFW(R_SYM)(rel->r_info);
        int 类型 = ELFW(R_TYPE)(rel->r_info);
        switch(类型) {
#if defined(目标_I386)
        case R_386_32:
            if (!获取符号属性(s1, 符号_索引, 0)->dyn_索引
                && ((ElfW(Sym)*)节符号表_数组->数据 + 符号_索引)->st_shndx == SHN_未定义) {
                /* don't fixup unresolved (weak) symbols */
                rel->r_info = ELFW(R_INFO)(符号_索引, R_386_RELATIVE);
                break;
            }
#elif defined(目标_X86_64)
        case R_X86_64_32:
        case R_X86_64_32S:
        case R_X86_64_64:
#elif defined(目标_ARM)
        case R_ARM_ABS32:
        case R_ARM_TARGET1:
#elif defined(目标_ARM64)
        case R_AARCH64_ABS32:
        case R_AARCH64_ABS64:
#elif defined(目标_RISCV64)
        case R_RISCV_32:
        case R_RISCV_64:
#endif
            count++;
            break;
#if defined(目标_I386)
        case R_386_PC32:
#elif defined(目标_X86_64)
        case R_X86_64_PC32:
	{
	    ElfW(Sym) *sym = &((ElfW(Sym) *)节符号表_数组->数据)[符号_索引];

            /* Hidden defined symbols can and must be resolved locally.
               We're misusing a PLT32 重定位 for this, as that's always
               resolved to its address even in shared libs.  */
	    if (sym->st_shndx != SHN_未定义 &&
		ELFW(ST_VISIBILITY)(sym->st_other) == STV_HIDDEN) {
                rel->r_info = ELFW(R_INFO)(符号_索引, R_X86_64_PLT32);
	        break;
	    }
	}
#elif defined(目标_ARM64)
        case R_AARCH64_PREL32:
#endif
            if (获取符号属性(s1, 符号_索引, 0)->dyn_索引)
                count++;
            break;
        default:
            break;
        }
    }
#endif
    return count;
}
#endif

#if !defined(ELF_OBJ_ONLY) || (defined(目标_MACHO) && defined 运行脚本M)
static void build_got(虚拟机ST *s1)
{
    /* if no got, then create it */
    s1->got = 节_新建(s1, ".got", SHT_程序数据, SHF_执行时占用内存 | SHF_可写);
    s1->got->sh_entsize = 4;
    设置elf符号(节符号表_数组, 0, 4, ELFW(ST_INFO)(STB_GLOBAL, STT_OBJECT),
                0, s1->got->ELF节数量, "_GLOBAL_OFFSET_TABLE_");
    /* keep space for _DYNAMIC pointer and two dummy got entries */
    节_预留指定大小内存(s1->got, 3 * 宏_指针大小);
}

/* 创建一个 GOT 和（用于函数调用）与 s1->符号表节中的符号对应的 PLT 条目。 在为 GOT 重定位创建动态符号表条目时，为相应的符号元数据使用“大小”和“信息”。 返回 GOT 或（如果有）PLT 条目的偏移量。 */
static struct 额外符号属性S * put_got_entry(虚拟机ST *s1, int dyn_reloc_type,
                                       int 符号_索引)
{
    int need_plt_entry;
    const char *name;
    ElfW(Sym) *sym;
    struct 额外符号属性S *attr;
    unsigned got_偏移;
    char plt_name[100];
    int 长度;
    节ST *s_rel;

    need_plt_entry = (dyn_reloc_type == R_JMP_SLOT);
    attr = 获取符号属性(s1, 符号_索引, 1);

    /* 如果一个函数被调用并且它的地址被调用，则创建 2 个 GOT 条目，一个用于获取地址 (GOT)，另一个用于 PLT 条目 (PLTGOT)。  */
    if (need_plt_entry ? attr->plt_偏移 : attr->got_偏移)
        return attr;

    s_rel = s1->got;
    if (need_plt_entry) {
        if (!s1->plt) {
            s1->plt = 节_新建(s1, ".plt", SHT_程序数据, SHF_执行时占用内存 | SHF_可执行);
            s1->plt->sh_entsize = 4;
        }
        s_rel = s1->plt;
    }

    /* create the GOT entry */
    got_偏移 = s1->got->当前数据_偏移量;
    节_预留指定大小内存(s1->got, 宏_指针大小);

    /* 创建将在 GOT 条目中插入感兴趣的对象或函数的地址的 GOT 重定位。 这是内存输出的静态重定位（dlsym 将为我们提供符号的地址）和动态重定位（可执行文件和 DLL）。 对于具有 *_JUMP_SLOT 重定位类型（与 PLT 条目关联的类型）的 GOT 条目，重定位应该延迟完成，但目前由于未知原因在加载时完成。 */

    sym = &((ElfW(Sym) *) 节符号表_数组->数据)[符号_索引];
    name = (char *) 节符号表_数组->link->数据 + sym->st_name;
    //printf("sym %d %s\n", need_plt_entry, name);

    if (s1->动态符号节) {
	if (ELFW(ST_BIND)(sym->st_info) == STB_LOCAL) {
	    /* 黑客警报。 我们不想为 STB_LOCAL 符号发出动态符号和基于符号的重定位，而是希望直接解析它们。 此时符号值还不是最终的，所以我们必须推迟它。
	     * 无论如何，我们稍后将不得不创建一个 RELATIVE 关系，因此我们滥用重定位槽来走私符号引用，直到 fill_local_got_entries。 并不是说 sembel 索引相对于节的符号表，而不是 s1->symbel 节！
	     * 尽管如此，我们使用 s1->dyn_sym 以便如果这是第一次调用 got->relection 正确创建。 另请注意，通常不会为 .got 创建 RELATIVE reloc，因此这些类型用作以后的标记（并且也保留用于最终输出，这没关系，因为 get 只是普通数据）。*/
	    elf重定位(s1->动态符号节, s1->got, got_偏移, R_RELATIVE,
			  符号_索引);
	} else {
	    if (0 == attr->dyn_索引)
                attr->dyn_索引 = 设置elf符号(s1->动态符号节, sym->st_value,
                                              sym->st_size, sym->st_info, 0,
                                              sym->st_shndx, name);
	    elf重定位(s1->动态符号节, s_rel, got_偏移, dyn_reloc_type,
			  attr->dyn_索引);
	}
    } else {
        elf重定位(节符号表_数组, s1->got, got_偏移, dyn_reloc_type,
                      符号_索引);
    }

    if (need_plt_entry) {
        attr->plt_偏移 = create_plt_entry(s1, got_偏移, attr);

        /* create a symbol 'sym@plt' for the PLT jump vector */
        长度 = strlen(name);
        if (长度 > sizeof plt_name - 5)
            长度 = sizeof plt_name - 5;
        memcpy(plt_name, name, 长度);
        strcpy(plt_name + 长度, "@plt");
        attr->plt_sym = 取符号索引(s1->符号表节, attr->plt_偏移, sym->st_size,
            ELFW(ST_INFO)(STB_GLOBAL, STT_FUNC), 0, s1->plt->ELF节数量, plt_name);
    } else {
        attr->got_偏移 = got_偏移;
    }

    return attr;
}

/* 构建 GOT 和 PLT 条目 */
/* 两次通过，因为 R_JMP_SLOT 应该成为第一个。 某些目标（arm、arm64）不允许混合使用 R_JMP_SLOT 和 R_GLOB_DAT。 */
静态函数 void build_got_entries(虚拟机ST *s1)
{
    节ST *s;
    ElfW_Rel *rel;
    ElfW(Sym) *sym;
    int i, 类型, gotplt_entry, reloc_type, 符号_索引;
    struct 额外符号属性S *attr;
    int pass = 0;

重做:
    for(i = 1; i < s1->节的数量; i++) {
        s = s1->节数组[i];
        if (s->sh_type != SHT_RELX)
            continue;
        /* no need to handle got relocations */
        if (s->link != 节符号表_数组)
            continue;
        for_each_elem(s, 0, rel, ElfW_Rel) {
            类型 = ELFW(R_TYPE)(rel->r_info);
            gotplt_entry = gotplt_entry_type(类型);
            if (gotplt_entry == -1)
                错_误 ("Unknown relocation 类型 for got: %d", 类型);
            符号_索引 = ELFW(R_SYM)(rel->r_info);
            sym = &((ElfW(Sym) *)节符号表_数组->数据)[符号_索引];

            if (gotplt_entry == NO_GOTPLT_ENTRY) {
                continue;
            }

            /* 自动创建PLT/GOT [entry] 如果它是未定义的引用（在运行时解析），或者符号是绝对的，可能是由zhi_添加_符号创建的，因此在64位目标上可能离应用程序代码太远。  */
            if (gotplt_entry == AUTO_GOTPLT_ENTRY) {
                if (sym->st_shndx == SHN_未定义) {
                    ElfW(Sym) *esym;
		    int dynindex;
                    if (s1->输出类型 == 输出_DLL && ! PCRELATIVE_DLLPLT)
                        continue;
		    /* UNDEF 符号的重定位通常需要转移到可执行文件或共享对象中。 如果那已经完成，则 AUTO_GOTPLT_ENTRY 将不存在。 但是 ZHI 不会这样做（至少对于 exe），因此我们需要在本地解决所有此类重定位。 这意味着 DLL 中函数的 PLT 插槽和数据符号的 COPY relocs。 COPY relocs 是在 绑定可执行文件符号 中生成的（并调整为定义的符号），对于函数，我们生成了一个函数类型的动态符号。 */
		    if (s1->动态符号节) {
			/* 动态符号节 isn't set for -run :-/  */
			dynindex = 获取符号属性(s1, 符号_索引, 0)->dyn_索引;
			esym = (ElfW(Sym) *)s1->动态符号节->数据 + dynindex;
			if (dynindex
			    && (ELFW(ST_TYPE)(esym->st_info) == STT_FUNC
				|| (ELFW(ST_TYPE)(esym->st_info) == STT_NOTYPE
				    && ELFW(ST_TYPE)(sym->st_info) == STT_FUNC)))
			    goto jmp_slot;
		    }
                } else if (sym->st_shndx == SHN_ABS) {
                    if (sym->st_value == 0) /* from zhi_add_btstub() */
                        continue;
#ifndef 目标_ARM
                    if (宏_指针大小 != 8)
                        continue;
#endif
                    /* from zhi_添加_符号(): 64位平台这些需要通过.got */
                } else
                    continue;
            }

#ifdef 目标_X86_64
            if ((类型 == R_X86_64_PLT32 || 类型 == R_X86_64_PC32) &&
		sym->st_shndx != SHN_未定义 &&
                (ELFW(ST_VISIBILITY)(sym->st_other) != STV_DEFAULT ||
		 ELFW(ST_BIND)(sym->st_info) == STB_LOCAL ||
		 s1->输出类型 == 输出_EXE)) {
		if (pass != 0)
		    continue;
                rel->r_info = ELFW(R_INFO)(符号_索引, R_X86_64_PC32);
                continue;
            }
#endif
            reloc_type = code_reloc(类型);
            if (reloc_type == -1)
                错_误 ("Unknown relocation 类型: %d", 类型);

            if (reloc_type != 0) {
        jmp_slot:
	        if (pass != 0)
                    continue;
                reloc_type = R_JMP_SLOT;
            } else {
	        if (pass != 1)
                    continue;
                reloc_type = R_GLOB_DAT;
            }

            if (!s1->got)
                build_got(s1);

            if (gotplt_entry == BUILD_GOT_ONLY)
                continue;

            attr = put_got_entry(s1, reloc_type, 符号_索引);

            if (reloc_type == R_JMP_SLOT)
                rel->r_info = ELFW(R_INFO)(attr->plt_sym, 类型);
        }
    }
    if (++pass < 2)
        goto 重做;

    /* .rel.plt refers to .got actually */
    if (s1->plt && s1->plt->重定位)
        s1->plt->重定位->sh_info = s1->got->ELF节数量;

}
#endif

静态函数 int 设置全局符号(虚拟机ST *s1, const char *name, 节ST *sec, addr_t offs)
{
    int shn = sec ? sec->ELF节数量 : offs || !name ? SHN_ABS : SHN_未定义;
    if (sec && offs == -1)
        offs = sec->当前数据_偏移量;
    return 设置elf符号(节符号表_数组, offs, 0,ELFW(ST_INFO)(name ? STB_GLOBAL : STB_LOCAL, STT_NOTYPE), 0, shn, name);
}

static void add_init_array_defines(虚拟机ST *s1, const char *section_name)
{
    节ST *s;
    addr_t end_offset;
    char buf[1024];
    s = 查找_节_创建(s1, section_name, 0);
    if (!s) {
        end_offset = 0;
        s = 数据_节;
    } else {
        end_offset = s->当前数据_偏移量;
    }
    snprintf(buf, sizeof(buf), "__%s_start", section_name + 1);
    设置全局符号(s1, buf, s, 0);
    snprintf(buf, sizeof(buf), "__%s_end", section_name + 1);
    设置全局符号(s1, buf, s, end_offset);
}

#ifndef 目标_PE
static void 添加支持(虚拟机ST *s1, const char *文件名)
{
    char buf[1024];
    snprintf(buf, sizeof(buf), "%s/%s", s1->zhi的安装目录, 文件名);
    编译器(s1, buf);
}
#endif

静态函数 void add_array (虚拟机ST *s1, const char *sec, int c)
{
    节ST *s;
    s = 查找_节_创建(s1, sec, 1);
    s->sh_flags |= SHF_可写;
#ifndef 目标_PE
    s->sh_type = sec[1] == 'i' ? SHT_INIT_ARRAY : SHT_FINI_ARRAY;
#endif
    elf重定位 (s1->符号表节, s, s->当前数据_偏移量, R_DATA_PTR, c);
    节_预留指定大小内存(s, 宏_指针大小);
}

#ifdef 启用边界检查M
静态函数 void 添加_边界检查(虚拟机ST *s1)
{
    if (0 == s1->使用_边界_检查器)
        return;
    节_预留指定大小内存(全局边界_节, sizeof(addr_t));
}
#endif

/* set symbol to STB_LOCAL and resolve. The point is to not export it as
   a dynamic symbol to allow so's to have one each with a different value. */
static void set_local_sym(虚拟机ST *s1, const char *name, 节ST *s, int offset)
{
    int c = 查找elf符号(s1->符号表节, name);
    if (c) {
        ElfW(Sym) *esym = (ElfW(Sym)*)s1->符号表节->数据 + c;
        esym->st_info = ELFW(ST_INFO)(STB_LOCAL, STT_NOTYPE);
        esym->st_value = offset;
        esym->st_shndx = s->ELF节数量;
    }
}

#ifdef 启用内置堆栈回溯M
static void put_ptr(虚拟机ST *s1, 节ST *s, int offs)
{
    int c;
    c = 设置全局符号(s1, NULL, s, offs);
    s = 数据_节;
    elf重定位 (s1->符号表节, s, s->当前数据_偏移量, R_DATA_PTR, c);
    节_预留指定大小内存(s, 宏_指针大小);
}

静态函数 void zhi_add_btstub(虚拟机ST *s1)
{
    节ST *s;
    int n, o;
    动态字符串ST cstr;

    s = 数据_节;
    /* Align to 宏_指针大小 */
    节_预留指定大小内存(s, -s->当前数据_偏移量 & (宏_指针大小 - 1));
    o = s->当前数据_偏移量;
    /* create (part of) a struct 运行时上下文S (see 运行.c) */
    put_ptr(s1, 调试节_数组, 0);
    put_ptr(s1, 调试节_数组, -1);
    put_ptr(s1, 调试节_数组->link, 0);
    节_预留指定大小内存(s, 3 * 宏_指针大小);
    /* prog_base : local nameless symbol with offset 0 at SHN_ABS */
    put_ptr(s1, NULL, 0);
    n = 2 * 宏_指针大小;
#ifdef 启用边界检查M
    if (s1->使用_边界_检查器) {
        put_ptr(s1, 全局边界_节, 0);
        n -= 宏_指针大小;
    }
#endif
    节_预留指定大小内存(s, n);
    动态字符串_新建(&cstr);
    动态字符串_printf(&cstr,
        "extern void __bt_init(),__bt_init_dll();"
        "static void *__rt_info[];"
        "__attribute__((constructor)) static void __bt_init_rt(){");
#ifdef 目标_PE
    if (s1->输出类型 == 输出_DLL)
#ifdef 启用边界检查M
        动态字符串_printf(&cstr, "__bt_init_dll(%d);", s1->使用_边界_检查器);
#else
        动态字符串_printf(&cstr, "__bt_init_dll(0);");
#endif
#endif
    动态字符串_printf(&cstr, "__bt_init(__rt_info,%d);}",
        s1->输出类型 == 输出_DLL ? 0 : s1->返回调用数量 + 1);
    开始编译(s1, s1->源文件类型, cstr.数据, -1);
    动态字符串_释放(&cstr);
    set_local_sym(s1, &"___rt_info"[!s1->前导下划线], s, o);
}
#endif

static void 测试覆盖_添加文件(虚拟机ST *s1, const char *文件名)
{
    动态字符串ST cstr;
    void *ptr;
    char wd[1024];

    if (测试节覆盖_数组 == NULL)
        return;
    节_预留指定大小内存(测试节覆盖_数组, 1);
    write32le (测试节覆盖_数组->数据, 测试节覆盖_数组->当前数据_偏移量);

    动态字符串_新建 (&cstr);
    if (文件名[0] == '/')
        动态字符串_printf (&cstr, "%s.tcov", 文件名);
    else {
        getcwd (wd, sizeof(wd));
        动态字符串_printf (&cstr, "%s/%s.tcov", wd, 文件名);
    }
    ptr = 节_预留指定大小内存(测试节覆盖_数组, cstr.大小 + 1);
    strcpy((char *)ptr, cstr.数据);
    unlink((char *)ptr);
#ifdef _WIN32
    斜杠规范((char *)ptr);
#endif
    动态字符串_释放 (&cstr);

    动态字符串_新建(&cstr);
    动态字符串_printf(&cstr,
        "extern char *__tcov_data[];"
        "extern void __store_test_coverage ();"
        "__attribute__((destructor)) static void __tcov_exit() {"
        "__store_test_coverage(__tcov_data);"
        "}");
    开始编译(s1, s1->源文件类型, cstr.数据, -1);
    动态字符串_释放(&cstr);
    set_local_sym(s1, &"___tcov_data"[!s1->前导下划线], 测试节覆盖_数组, 0);
}

#ifndef 目标_PE
静态函数 void zhi_添加运行时库(虚拟机ST *s1)
{
    s1->源文件类型 = 0;
#ifdef 启用边界检查M
    添加_边界检查(s1);
#endif
    加载静态链接库(s1);
    /* add libc */
    if (!s1->不添加标准库) {
        if (s1->option_pthread)
            添加库_有错误提示(s1, "pthread");
        添加库_有错误提示(s1, "c");
#ifdef ZHI_LIBGCC
        if (!s1->静态_链接) {
            if (ZHI_LIBGCC[0] == '/')
                编译器(s1, ZHI_LIBGCC);
            else
                zhi_添加_dll(s1, ZHI_LIBGCC, 0);
        }
#endif
#if 目标_ARM && 目标系统_FreeBSD
        添加库_有错误提示(s1, "gcc_s"); // unwind code
#endif
#ifdef 启用边界检查M
        if (s1->使用_边界_检查器 && s1->输出类型 != 输出_DLL) {
            添加库_有错误提示(s1, "pthread");
#if !目标系统_OpenBSD && !目标系统_NetBSD
            添加库_有错误提示(s1, "dl");
#endif
            添加支持(s1, "bcheck.o");
	    if (s1->静态_链接)
                添加库_有错误提示(s1, "c");
        }
#endif
#ifdef 启用内置堆栈回溯M
        if (s1->执行_回溯) {
            if (s1->输出类型 == 输出_EXE)
                添加支持(s1, "bt-exe.o");
            if (s1->输出类型 != 输出_DLL)
                添加支持(s1, "bt-log.o");
            if (s1->输出类型 != 输出_内存)
                zhi_add_btstub(s1);
        }
#endif
        if (运行时库M[0])
            添加支持(s1, 运行时库M);

#if 目标系统_OpenBSD || 目标系统_FreeBSD || 目标系统_NetBSD
        /* add crt end if not memory output */
	if (s1->输出类型 != 输出_内存) {
	    if (s1->输出类型 == 输出_DLL)
	        zhi_add_crt(s1, "crtendS.o");
	    else
	        zhi_add_crt(s1, "crtend.o");
#if 目标系统_FreeBSD || 目标系统_NetBSD
            zhi_add_crt(s1, "crtn.o");
#endif
        }
#elif !defined(目标_MACHO)
        /* add crt end if not memory output */
        if (s1->输出类型 != 输出_内存)
            zhi_add_crt(s1, "crtn.o");
#endif
    }
}
#endif

/* 添加各种标准链接器符号（必须在节数组填充后完成（例如分配公共符号后）） */
static void zhi_添加链接器符号(虚拟机ST *s1)
{
    char buf[1024];
    int i;
    节ST *s;

    设置全局符号(s1, "_etext", 代码_节, -1);
    设置全局符号(s1, "_edata", 数据_节, -1);
    设置全局符号(s1, "_end", 未初始化或初始化为0的数据_节, -1);
#if 目标系统_OpenBSD
    设置全局符号(s1, "__executable_start", NULL, ELF_START_ADDR);
#endif
#ifdef 目标_RISCV64
    /* XXX should be .sdata+0x800, not .数据+0x800 */
    设置全局符号(s1, "__global_pointer$", 数据_节, 0x800);
#endif
    /* horrible new standard ldscript defines */
#ifndef 目标_PE
    add_init_array_defines(s1, ".preinit_array");
#endif
    add_init_array_defines(s1, ".init_array");
    add_init_array_defines(s1, ".fini_array");
    /* add start and stop symbols for 节数组 whose name can be
       expressed in C */
    for(i = 1; i < s1->节的数量; i++) {
        s = s1->节数组[i];
        if ((s->sh_flags & SHF_执行时占用内存)
            && (s->sh_type == SHT_程序数据
                || s->sh_type == SHT_字符串表)) {
            const char *p;
            /* check if section name can be expressed in C */
            p = s->name;
            for(;;) {
                int c = *p;
                if (!c)
                    break;
                if (!是标识符(c) && !是数字(c))
                    goto next_sec;
                p++;
            }
            snprintf(buf, sizeof(buf), "__start_%s", s->name);
            设置全局符号(s1, buf, s, 0);
            snprintf(buf, sizeof(buf), "__stop_%s", s->name);
            设置全局符号(s1, buf, s, -1);
        }
    next_sec: ;
    }
}

静态函数 void 解析_公共_符号(虚拟机ST *s1)
{
    ElfW(Sym) *sym;
    /* 在 BSS 中分配公共符号。  */
    for_each_elem(节符号表_数组, 1, sym, ElfW(Sym))
    {
        if (sym->st_shndx == SHN_COMMON)
        {
            /* 符号对齐在 SHN_COMMON 的 st_value 中*/
	    sym->st_value = 节_预留指定大小内存且按照align对齐(未初始化或初始化为0的数据_节, sym->st_size,sym->st_value);
            sym->st_shndx = 未初始化或初始化为0的数据_节->ELF节数量;
        }
    }
    /* 现在分配链接器提供的符号它们的值。  */
    zhi_添加链接器符号(s1);
}

#ifndef ELF_OBJ_ONLY

静态函数 void fill_got_entry(虚拟机ST *s1, ElfW_Rel *rel)
{
    int 符号_索引 = ELFW(R_SYM) (rel->r_info);
    ElfW(Sym) *sym = &((ElfW(Sym) *) 节符号表_数组->数据)[符号_索引];
    struct 额外符号属性S *attr = 获取符号属性(s1, 符号_索引, 0);
    unsigned offset = attr->got_偏移;

    if (0 == offset)
        return;
    节_预留(s1->got, offset + 宏_指针大小);
#if 宏_指针大小 == 8
    write64le(s1->got->数据 + offset, sym->st_value);
#else
    write32le(s1->got->数据 + offset, sym->st_value);
#endif
}

/* Perform relocation to GOT or PLT entries */
静态函数 void fill_got(虚拟机ST *s1)
{
    节ST *s;
    ElfW_Rel *rel;
    int i;

    for(i = 1; i < s1->节的数量; i++) {
        s = s1->节数组[i];
        if (s->sh_type != SHT_RELX)
            continue;
        /* no need to handle got relocations */
        if (s->link != 节符号表_数组)
            continue;
        for_each_elem(s, 0, rel, ElfW_Rel) {
            switch (ELFW(R_TYPE) (rel->r_info)) {
                case R_X86_64_GOT32:
                case R_X86_64_GOTPCREL:
		case R_X86_64_GOTPCRELX:
		case R_X86_64_REX_GOTPCRELX:
                case R_X86_64_PLT32:
                    fill_got_entry(s1, rel);
                    break;
            }
        }
    }
}

/* See put_got_entry for a description.  This is the second stage
   where GOT references to local defined symbols are rewritten.  */
static void fill_local_got_entries(虚拟机ST *s1)
{
    ElfW_Rel *rel;
    if (!s1->got->重定位)
        return;
    for_each_elem(s1->got->重定位, 0, rel, ElfW_Rel) {
	if (ELFW(R_TYPE)(rel->r_info) == R_RELATIVE) {
	    int 符号_索引 = ELFW(R_SYM) (rel->r_info);
	    ElfW(Sym) *sym = &((ElfW(Sym) *) 节符号表_数组->数据)[符号_索引];
	    struct 额外符号属性S *attr = 获取符号属性(s1, 符号_索引, 0);
	    unsigned offset = attr->got_偏移;
	    if (offset != rel->r_offset - s1->got->sh_addr)
	      错误_不中止("huh");
	    rel->r_info = ELFW(R_INFO)(0, R_RELATIVE);
#if SHT_RELX == SHT_带加数的重定位条目
	    rel->r_addend = sym->st_value;
#else
	    /* All our REL architectures also happen to be 32bit LE.  */
	    write32le(s1->got->数据 + offset, sym->st_value);
#endif
	}
    }
}

/* 绑定可执行文件的符号：从共享库中导出的符号中解析未定义的符号，并将非本地定义的符号导出到共享库（如果在命令行上指定了-导出所有符号 开关） */
static void 绑定可执行文件符号(虚拟机ST *s1)
{
    const char *name;
    int 符号_索引, index;
    ElfW(Sym) *sym, *esym;
    int 类型;

    /* Resolve undefined symbols from dynamic symbols. When there is a match:
       - if STT_FUNC or STT_GNU_IFUNC symbol -> add it in PLT
       - if STT_OBJECT symbol -> add it in .bss section with suitable 重定位 */
    for_each_elem(节符号表_数组, 1, sym, ElfW(Sym)) {
        if (sym->st_shndx == SHN_未定义) {
            name = (char *) 节符号表_数组->link->数据 + sym->st_name;
            符号_索引 = 查找elf符号(s1->临时动态符号表_节, name);
            if (符号_索引) {
                esym = &((ElfW(Sym) *)s1->临时动态符号表_节->数据)[符号_索引];
                类型 = ELFW(ST_TYPE)(esym->st_info);
                if ((类型 == STT_FUNC) || (类型 == STT_GNU_IFUNC)) {
                    /* Indirect functions shall have STT_FUNC 类型 in executable
                     * 动态符号节 section. Indeed, a dlsym call following a lazy
                     * resolution would pick the symbol value from the
                     * executable 动态符号节 entry which would contain the address
                     * of the function wanted by the caller of dlsym instead of
                     * the address of the function that would return that
                     * address */
                    int dynindex = 取符号索引(s1->动态符号节, 0, esym->st_size,ELFW(ST_INFO)(STB_GLOBAL,STT_FUNC), 0, 0,name);
		    int index = sym - (ElfW(Sym) *) 节符号表_数组->数据;
		    获取符号属性(s1, index, 1)->dyn_索引 = dynindex;
                } else if (类型 == STT_OBJECT) {
                    unsigned long offset;
                    ElfW(Sym) *动态符号节;
                    offset = 未初始化或初始化为0的数据_节->当前数据_偏移量;
                    /* XXX: which alignment ? */
                    offset = (offset + 16 - 1) & -16;
                    设置elf符号 (s1->符号表节, offset, esym->st_size,esym->st_info, 0, 未初始化或初始化为0的数据_节->ELF节数量, name);
                    index = 取符号索引(s1->动态符号节, offset, esym->st_size,esym->st_info, 0, 未初始化或初始化为0的数据_节->ELF节数量,name);

                    /* Ensure R_COPY works for weak symbol aliases */
                    if (ELFW(ST_BIND)(esym->st_info) == STB_WEAK) {
                        for_each_elem(s1->临时动态符号表_节, 1, 动态符号节, ElfW(Sym)) {
                            if ((动态符号节->st_value == esym->st_value) && (ELFW(ST_BIND)(动态符号节->st_info) == STB_GLOBAL))
                            {
                                char *dynname = (char *) s1->临时动态符号表_节->link->数据 + 动态符号节->st_name;
                                取符号索引(s1->动态符号节, offset, 动态符号节->st_size,动态符号节->st_info, 0,未初始化或初始化为0的数据_节->ELF节数量, dynname);
                                break;
                            }
                        }
                    }

                    elf重定位(s1->动态符号节, 未初始化或初始化为0的数据_节,offset, R_COPY, index);
                    offset += esym->st_size;
                    未初始化或初始化为0的数据_节->当前数据_偏移量 = offset;
                }
            } else {
                /* STB_WEAK undefined symbols are accepted */
                /* XXX: _fp_hw seems to be part of the ABI, so we ignore it */
                if (ELFW(ST_BIND)(sym->st_info) == STB_WEAK ||
                    !strcmp(name, "_fp_hw")) {
                } else {
                    错误_不中止("undefined symbol '%s'", name);
                }
            }
        } else if (s1->导出所有符号 && ELFW(ST_BIND)(sym->st_info) != STB_LOCAL) {
            /* if -导出所有符号 option, then export all non local symbols */
            name = (char *) 节符号表_数组->link->数据 + sym->st_name;
            设置elf符号(s1->动态符号节, sym->st_value, sym->st_size, sym->st_info,
                        0, sym->st_shndx, name);
        }
    }
}

/* Bind symbols of libraries: export all non local symbols of executable that
   are referenced by shared libraries. The reason is that the dynamic loader
   search symbol first in executable and then in libraries. Therefore a
   reference to a symbol already defined by a library can still be resolved by
   a symbol in the executable. */
static void 绑定库符号(虚拟机ST *s1)
{
    const char *name;
    int 符号_索引;
    ElfW(Sym) *sym, *esym;

    for_each_elem(s1->临时动态符号表_节, 1, esym, ElfW(Sym)) {
        name = (char *) s1->临时动态符号表_节->link->数据 + esym->st_name;
        符号_索引 = 查找elf符号(节符号表_数组, name);
        sym = &((ElfW(Sym) *)节符号表_数组->数据)[符号_索引];
        if (符号_索引 && sym->st_shndx != SHN_未定义
            && ELFW(ST_BIND)(sym->st_info) != STB_LOCAL) {
            设置elf符号(s1->动态符号节, sym->st_value, sym->st_size,
                sym->st_info, 0, sym->st_shndx, name);
        } else if (esym->st_shndx == SHN_未定义) {
            /* weak symbols can stay undefined */
            if (ELFW(ST_BIND)(esym->st_info) != STB_WEAK)
                zhi_警告("未定义动态符号 '%s'", name);
        }
    }
}

/* Export all non local symbols. This is used by shared libraries so that the
   non local symbols they define can resolve a reference in another shared
   library or in the executable. Correspondingly, it allows undefined local
   symbols to be resolved by other shared libraries or by the executable. */
static void 导出全局符号(虚拟机ST *s1)
{
    int dynindex, index;
    const char *name;
    ElfW(Sym) *sym;

    for_each_elem(节符号表_数组, 1, sym, ElfW(Sym)) {
        if (ELFW(ST_BIND)(sym->st_info) != STB_LOCAL) {
	    name = (char *) 节符号表_数组->link->数据 + sym->st_name;
	    dynindex = 取符号索引(s1->动态符号节, sym->st_value, sym->st_size,
				   sym->st_info, 0, sym->st_shndx, name);
	    index = sym - (ElfW(Sym) *) 节符号表_数组->数据;
            获取符号属性(s1, index, 1)->dyn_索引 = dynindex;
        }
    }
}

/* decide if an unallocated section should be output. */
static int set_sec_sizes(虚拟机ST *s1)
{
    int i;
    节ST *s;
    int textrel = 0;
    int file_type = s1->输出类型;

    /* Allocate strings for section names */
    for(i = 1; i < s1->节的数量; i++) {
        s = s1->节数组[i];
        if (s->sh_type == SHT_RELX && !(s->sh_flags & SHF_执行时占用内存)) {
            /* when generating a DLL, we include relocations but
               we may patch them */
            if (file_type == 输出_DLL
                && (s1->节数组[s->sh_info]->sh_flags & SHF_执行时占用内存)) {
                int count = prepare_dynamic_rel(s1, s);
                if (count) {
                    /* allocate the section */
                    s->sh_flags |= SHF_执行时占用内存;
                    s->sh_size = count * sizeof(ElfW_Rel);
                    if (!(s1->节数组[s->sh_info]->sh_flags & SHF_可写))
                        textrel = 1;
                }
            }
        } else if ((s->sh_flags & SHF_执行时占用内存)
#ifdef 目标_ARM
                   || s->sh_type == SHT_ARM_ATTRIBUTES
#endif
                   || s1->做_调试) {
            s->sh_size = s->当前数据_偏移量;
        }

#ifdef 目标_ARM
        /* XXX: Suppress stack unwinding section. */
        if (s->sh_type == SHT_ARM_EXIDX) {
            s->sh_flags = 0;
            s->sh_size = 0;
        }
#endif

    }
    return textrel;
}


/* Info to be copied in dynamic section */
struct dyn_inf {
    节ST *dynamic;
    节ST *dynstr;
    unsigned long 当前数据_偏移量;
    addr_t rel_addr;
    addr_t rel_size;
};

/* Info for GNU_RELRO */
struct ro_inf {
   addr_t 文件偏移量;
   addr_t sh_addr;
   addr_t sh_size;
};

static void alloc_sec_names(
    虚拟机ST *s1, int is_obj
    );

static int layout_any_sections(
    虚拟机ST *s1, int file_offset, int *sec_order, int is_obj
    );

/* Assign 节数组 to segments and decide how are 节数组 laid out when loaded
   in memory. This function also fills corresponding program headers. */
static int layout_sections(虚拟机ST *s1, ElfW(Phdr) *phdr,
			   int phnum, int phfill,
                           节ST *interp,
                           struct ro_inf *roinf, int *sec_order)
{
    int i, file_offset;
    节ST *s;

    file_offset = 0;
    if (s1->输出格式 == 输出格式_ELF)
        file_offset = sizeof(ElfW(Ehdr)) + phnum * sizeof(ElfW(Phdr));

    {
        unsigned long s_align;
        long long tmp;
        addr_t addr;
        ElfW(Phdr) *ph;
        int j, k, f, file_type = s1->输出类型;

        s_align = ELF_PAGE_SIZE;
        if (s1->节_对齐)
            s_align = s1->节_对齐;

        if (s1->has_代码节地址) {
            int a_offset, p_offset;
            addr = s1->代码节地址;
            /* we ensure that (addr % ELF_PAGE_SIZE) == file_offset %
               ELF_PAGE_SIZE */
            a_offset = (int) (addr & (s_align - 1));
            p_offset = file_offset & (s_align - 1);
            if (a_offset < p_offset)
                a_offset += s_align;
            file_offset += (a_offset - p_offset);
        } else {
            if (file_type == 输出_DLL)
                addr = 0;
            else
                addr = ELF_START_ADDR;
            /* compute address after headers */
            addr += (file_offset & (s_align - 1));
        }

        ph = &phdr[0];
        /* Leave one program headers for the program interpreter and one for
           the program header table itself if needed. These are 完成 later as
           they require section layout to be 完成 first. */
        if (interp)
            ph += 2;

        /* read only segment mapping for GNU_RELRO */
	roinf->文件偏移量 = roinf->sh_addr = roinf->sh_size = 0;

        for(j = 0; j < phfill; j++) {
            ph->p_type = j == 2 ? PT_TLS : PT_LOAD;
            if (j == 0)
                ph->p_flags = PF_R | PF_X;
            else
                ph->p_flags = PF_R | PF_W;
            ph->p_align = j == 2 ? 4 : s_align;

            /* Decide the layout of 节数组 loaded in memory. This must
               be 完成 before program headers are filled since they contain
               info about the layout. We do the following ordering: interp,
               symbol tables, relocations, progbits, nobits */
            /* XXX: do faster and simpler sorting */
	    f = -1;
            for(k = 0; k < 7; k++) {
                for(i = 1; i < s1->节的数量; i++) {
                    s = s1->节数组[i];
                    /* compute if section should be included */
                    if (j == 0) {
                        if ((s->sh_flags & (SHF_执行时占用内存 | SHF_可写 | SHF_TLS)) !=
                            SHF_执行时占用内存)
                            continue;
                    } else if (j == 1) {
                        if ((s->sh_flags & (SHF_执行时占用内存 | SHF_可写 | SHF_TLS)) !=
                            (SHF_执行时占用内存 | SHF_可写))
                            continue;
                    } else  {
                        if ((s->sh_flags & (SHF_执行时占用内存 | SHF_可写 | SHF_TLS)) !=
                            (SHF_执行时占用内存 | SHF_可写 | SHF_TLS))
                            continue;
                    }
                    if (s == interp) {
                        if (k != 0)
                            continue;
                    } else if ((s->sh_type == SHT_DYNSYM ||
                                s->sh_type == SHT_字符串表 ||
                                s->sh_type == SHT_符号哈希表)
                               && !strstr(s->name, ".stab"))  {
                        if (k != 1)
                            continue;
                    } else if (s->sh_type == SHT_RELX) {
                        if (s1->plt && s == s1->plt->重定位) {
                            if (k != 3)
                                continue;
                        } else {
                            if (k != 2)
                                continue;
                        }
                    } else if (s->sh_type == SHT_无数据) {
                        if (k != 6)
                            continue;
                    } else if ((s == 只读数据_节
#ifdef 启用边界检查M
		                || s == 全局边界_节
                                || s == 局部边界_节
#endif
                                ) && (s->sh_flags & SHF_可写)) {
                        if (k != 4)
                            continue;
			/* Align next section on page 大小.
			   This is needed to remap roinf section ro. */
			f = 1;
                    } else {
                        if (k != 5)
                            continue;
		    }
                    *sec_order++ = i;

                    /* section matches: we 对齐 it and add its 大小 */
                    tmp = addr;
		    if (f-- == 0)
			s->sh_地址对齐 = 页大小M;
                    addr = (addr + s->sh_地址对齐 - 1) &
                        ~(s->sh_地址对齐 - 1);
                    file_offset += (int) ( addr - tmp );
                    s->文件偏移量 = file_offset;
                    s->sh_addr = addr;

                    /* update program header infos */
                    if (ph->p_offset == 0) {
                        ph->p_offset = file_offset;
                        ph->p_vaddr = addr;
                        ph->p_paddr = ph->p_vaddr;
                    }

                    if (k == 4) {
                        if (roinf->sh_size == 0) {
                            roinf->文件偏移量 = s->文件偏移量;
                            roinf->sh_addr = s->sh_addr;
			}
                        roinf->sh_size = (addr - roinf->sh_addr) + s->sh_size;
		    }

                    addr += s->sh_size;
                    if (s->sh_type != SHT_无数据)
                        file_offset += s->sh_size;
                }
            }
	    if (j == 0) {
		/* Make the first PT_LOAD segment include the program
		   headers itself (and the ELF header as well), it'll
		   come out with same memory use but will make various
		   tools like binutils strip work better.  */
		ph->p_offset &= ~(ph->p_align - 1);
		ph->p_vaddr &= ~(ph->p_align - 1);
		ph->p_paddr &= ~(ph->p_align - 1);
	    }
            ph->p_filesz = file_offset - ph->p_offset;
            ph->p_memsz = addr - ph->p_vaddr;
            ph++;
            if (j == 0) {
                if (s1->输出格式 == 输出格式_ELF) {
                    /* if in the middle of a page, we duplicate the page in
                       memory so that one copy is RX and the other is RW */
                    if ((addr & (s_align - 1)) != 0)
                        addr += s_align;
                } else {
                    addr = (addr + s_align - 1) & ~(s_align - 1);
                    file_offset = (file_offset + s_align - 1) & ~(s_align - 1);
                }
            }
        }
    }

    /* all other 节数组 come after */
    return layout_any_sections(s1, file_offset, sec_order, 0);
}

/* put dynamic tag */
static void put_dt(节ST *dynamic, int dt, addr_t val)
{
    ElfW(Dyn) *dyn;
    dyn = 节_预留指定大小内存(dynamic, sizeof(ElfW(Dyn)));
    dyn->d_tag = dt;
    dyn->d_un.d_val = val;
}

static void fill_unloadable_phdr(ElfW(Phdr) *phdr, int phnum, 节ST *interp,
                                 节ST *dynamic, 节ST *note, struct ro_inf *roinf)
{
    ElfW(Phdr) *ph;

    /* if interpreter, then add corresponding program header */
    if (interp) {
        ph = &phdr[0];

        ph->p_type = PT_PHDR;
        ph->p_offset = sizeof(ElfW(Ehdr));
        ph->p_filesz = ph->p_memsz = phnum * sizeof(ElfW(Phdr));
        ph->p_vaddr = interp->sh_addr - ph->p_filesz;
        ph->p_paddr = ph->p_vaddr;
        ph->p_flags = PF_R | PF_X;
        ph->p_align = 4; /* interp->sh_地址对齐; */
        ph++;

        ph->p_type = PT_INTERP;
        ph->p_offset = interp->文件偏移量;
        ph->p_vaddr = interp->sh_addr;
        ph->p_paddr = ph->p_vaddr;
        ph->p_filesz = interp->sh_size;
        ph->p_memsz = interp->sh_size;
        ph->p_flags = PF_R;
        ph->p_align = interp->sh_地址对齐;
    }

    if (note) {
        ph = &phdr[phnum - 2 - (roinf != NULL)];

        ph->p_type = PT_NOTE;
        ph->p_offset = note->文件偏移量;
        ph->p_vaddr = note->sh_addr;
        ph->p_paddr = ph->p_vaddr;
        ph->p_filesz = note->sh_size;
        ph->p_memsz = note->sh_size;
        ph->p_flags = PF_R;
        ph->p_align = note->sh_地址对齐;
    }

    /* if dynamic section, then add corresponding program header */
    if (dynamic) {
        ph = &phdr[phnum - 1 - (roinf != NULL)];

        ph->p_type = PT_DYNAMIC;
        ph->p_offset = dynamic->文件偏移量;
        ph->p_vaddr = dynamic->sh_addr;
        ph->p_paddr = ph->p_vaddr;
        ph->p_filesz = dynamic->sh_size;
        ph->p_memsz = dynamic->sh_size;
        ph->p_flags = PF_R | PF_W;
        ph->p_align = dynamic->sh_地址对齐;
    }

    if (roinf) {
        ph = &phdr[phnum - 1];

        ph->p_type = PT_GNU_RELRO;
        ph->p_offset = roinf->文件偏移量;
        ph->p_vaddr = roinf->sh_addr;
        ph->p_paddr = ph->p_vaddr;
        ph->p_filesz = roinf->sh_size;
        ph->p_memsz = roinf->sh_size;
        ph->p_flags = PF_R;
        ph->p_align = 1;
    }
}

/* Fill the dynamic section with tags describing the address and 大小 of
   节数组 */
static void fill_dynamic(虚拟机ST *s1, struct dyn_inf *dyninf)
{
    节ST *dynamic = dyninf->dynamic;
    节ST *s;

    /* put dynamic section entries */
    put_dt(dynamic, DT_HASH, s1->动态符号节->hash->sh_addr);
    put_dt(dynamic, DT_STRTAB, dyninf->dynstr->sh_addr);
    put_dt(dynamic, DT_SYMTAB, s1->动态符号节->sh_addr);
    put_dt(dynamic, DT_STRSZ, dyninf->dynstr->当前数据_偏移量);
    put_dt(dynamic, DT_SYMENT, sizeof(ElfW(Sym)));
#if 宏_指针大小 == 8
    put_dt(dynamic, DT_RELA, dyninf->rel_addr);
    put_dt(dynamic, DT_RELASZ, dyninf->rel_size);
    put_dt(dynamic, DT_RELAENT, sizeof(ElfW_Rel));
    if (s1->plt && s1->plt->重定位) {
        put_dt(dynamic, DT_PLTGOT, s1->got->sh_addr);
        put_dt(dynamic, DT_PLTRELSZ, s1->plt->重定位->当前数据_偏移量);
        put_dt(dynamic, DT_JMPREL, s1->plt->重定位->sh_addr);
        put_dt(dynamic, DT_PLTREL, DT_RELA);
    }
    put_dt(dynamic, DT_RELACOUNT, 0);
#else
    put_dt(dynamic, DT_REL, dyninf->rel_addr);
    put_dt(dynamic, DT_RELSZ, dyninf->rel_size);
    put_dt(dynamic, DT_RELENT, sizeof(ElfW_Rel));
    if (s1->plt && s1->plt->重定位) {
        put_dt(dynamic, DT_PLTGOT, s1->got->sh_addr);
        put_dt(dynamic, DT_PLTRELSZ, s1->plt->重定位->当前数据_偏移量);
        put_dt(dynamic, DT_JMPREL, s1->plt->重定位->sh_addr);
        put_dt(dynamic, DT_PLTREL, DT_REL);
    }
    put_dt(dynamic, DT_RELCOUNT, 0);
#endif
    if (符号版本_节 && 需要的版本_节) {
	/* The dynamic linker can not handle VERSYM without VERNEED */
        put_dt(dynamic, DT_VERSYM, 符号版本_节->sh_addr);
        put_dt(dynamic, DT_VERNEED, 需要的版本_节->sh_addr);
        put_dt(dynamic, DT_VERNEEDNUM, dt_版本需要节);
    }
    s = 查找_节_创建 (s1, ".preinit_array", 0);
    if (s && s->当前数据_偏移量) {
        put_dt(dynamic, DT_PREINIT_ARRAY, s->sh_addr);
        put_dt(dynamic, DT_PREINIT_ARRAYSZ, s->当前数据_偏移量);
    }
    s = 查找_节_创建 (s1, ".init_array", 0);
    if (s && s->当前数据_偏移量) {
        put_dt(dynamic, DT_INIT_ARRAY, s->sh_addr);
        put_dt(dynamic, DT_INIT_ARRAYSZ, s->当前数据_偏移量);
    }
    s = 查找_节_创建 (s1, ".fini_array", 0);
    if (s && s->当前数据_偏移量) {
        put_dt(dynamic, DT_FINI_ARRAY, s->sh_addr);
        put_dt(dynamic, DT_FINI_ARRAYSZ, s->当前数据_偏移量);
    }
    s = 查找_节_创建 (s1, ".init", 0);
    if (s && s->当前数据_偏移量) {
        put_dt(dynamic, DT_INIT, s->sh_addr);
    }
    s = 查找_节_创建 (s1, ".fini", 0);
    if (s && s->当前数据_偏移量) {
        put_dt(dynamic, DT_FINI, s->sh_addr);
    }
    if (s1->做_调试)
        put_dt(dynamic, DT_DEBUG, 0);
    put_dt(dynamic, DT_NULL, 0);
}

/* Remove gaps between RELX 节数组.
   These gaps are a result of final_sections_reloc. Here some relocs are removed.
   The gaps are then filled with 0 in zhi_output_elf. The 0 is intepreted as
   R_...NONE 重定位. This does work on most targets but on OpenBSD/arm64 this
   is illegal. OpenBSD/arm64 does not support R_...NONE 重定位. */
static void update_reloc_sections(虚拟机ST *s1, struct dyn_inf *dyninf)
{
    int i;
    unsigned long file_offset = 0;
    节ST *s;
    节ST *relocplt = s1->plt ? s1->plt->重定位 : NULL;

    /* dynamic relocation table information, for .dynamic section */
    dyninf->rel_addr = dyninf->rel_size = 0;

    for(i = 1; i < s1->节的数量; i++) {
        s = s1->节数组[i];
	if (s->sh_type == SHT_RELX && s != relocplt) {
	    if (dyninf->rel_size == 0) {
		dyninf->rel_addr = s->sh_addr;
		file_offset = s->文件偏移量;
	    }
	    else {
		s->sh_addr = dyninf->rel_addr + dyninf->rel_size;
		s->文件偏移量 = file_offset + dyninf->rel_size;
	    }
	    dyninf->rel_size += s->sh_size;
	}
    }
}

#endif /* ndef ELF_OBJ_ONLY */

/* Create an ELF 文件 on disk.
   This function handle ELF specific layout requirements */
static void zhi_output_elf(虚拟机ST *s1, FILE *f, int phnum, ElfW(Phdr) *phdr,
                           int file_offset, int *sec_order)
{
    int i, shnum, offset, 大小, file_type;
    节ST *s;
    ElfW(Ehdr) ehdr;
    ElfW(Shdr) shdr, *sh;

    file_type = s1->输出类型;
    shnum = s1->节的数量;

    memset(&ehdr, 0, sizeof(ehdr));

    if (phnum > 0) {
        ehdr.e_phentsize = sizeof(ElfW(Phdr));
        ehdr.e_phnum = phnum;
        ehdr.e_phoff = sizeof(ElfW(Ehdr));
    }

    /* 对齐 to 4 */
    file_offset = (file_offset + 3) & -4;

    /* fill header */
    ehdr.e_ident[0] = ELFMAG0;
    ehdr.e_ident[1] = ELFMAG1;
    ehdr.e_ident[2] = ELFMAG2;
    ehdr.e_ident[3] = ELFMAG3;
    ehdr.e_ident[4] = ELFCLASSW;
    ehdr.e_ident[5] = ELFDATA2LSB;
    ehdr.e_ident[6] = EV_CURRENT;
#if 目标系统_FreeBSD || 目标系统_FreeBSD_kernel
    ehdr.e_ident[EI_OSABI] = ELFOSABI_FREEBSD;
#endif
#ifdef 目标_ARM
#ifdef ZHI_ARM_EABI
    ehdr.e_ident[EI_OSABI] = 0;
    ehdr.e_flags = EF_ARM_EABI_VER4;
    if (file_type == 输出_EXE || file_type == 输出_DLL)
        ehdr.e_flags |= EF_ARM_HASENTRY;
    if (s1->浮点_abi == ARM_HARD_FLOAT)
        ehdr.e_flags |= EF_ARM_VFP_FLOAT;
    else
        ehdr.e_flags |= EF_ARM_SOFT_FLOAT;
#else
    ehdr.e_ident[EI_OSABI] = ELFOSABI_ARM;
#endif
#elif defined 目标_RISCV64
    ehdr.e_flags = EF_RISCV_FLOAT_ABI_DOUBLE;
#endif
    switch(file_type) {
    default:
    case 输出_EXE:
        ehdr.e_type = ET_EXEC;
        ehdr.e_entry = 获取符号地址(s1, "_start", 1, 0);
        break;
    case 输出_DLL:
        ehdr.e_type = ET_DYN;
        ehdr.e_entry = 代码_节->sh_addr; /* XXX: is it correct ? */
        break;
    case 输出_OBJ:
        ehdr.e_type = ET_REL;
        break;
    }
    ehdr.e_machine = EM_目标机;
    ehdr.e_version = EV_CURRENT;
    ehdr.e_shoff = file_offset;
    ehdr.e_ehsize = sizeof(ElfW(Ehdr));
    ehdr.e_shentsize = sizeof(ElfW(Shdr));
    ehdr.e_shnum = shnum;
    ehdr.e_shstrndx = shnum - 1;

    fwrite(&ehdr, 1, sizeof(ElfW(Ehdr)), f);
    if (phdr)
        fwrite(phdr, 1, phnum * sizeof(ElfW(Phdr)), f);
    offset = sizeof(ElfW(Ehdr)) + phnum * sizeof(ElfW(Phdr));

    符号表排序(s1, 节符号表_数组);
    for(i = 1; i < s1->节的数量; i++) {
        s = s1->节数组[sec_order[i]];
        if (s->sh_type != SHT_无数据) {
            while (offset < s->文件偏移量) {
                fputc(0, f);
                offset++;
            }
            大小 = s->sh_size;
            if (大小)
                fwrite(s->数据, 1, 大小, f);
            offset += 大小;
        }
    }

    /* output section headers */
    while (offset < ehdr.e_shoff) {
        fputc(0, f);
        offset++;
    }

    for(i = 0; i < s1->节的数量; i++) {
        sh = &shdr;
        memset(sh, 0, sizeof(ElfW(Shdr)));
        s = s1->节数组[i];
        if (s) {
            sh->sh_name = s->sh_name;
            sh->sh_type = s->sh_type;
            sh->sh_flags = s->sh_flags;
            sh->sh_entsize = s->sh_entsize;
            sh->sh_info = s->sh_info;
            if (s->link)
                sh->sh_link = s->link->ELF节数量;
            sh->sh_地址对齐 = s->sh_地址对齐;
            sh->sh_addr = s->sh_addr;
            sh->文件偏移量 = s->文件偏移量;
            sh->sh_size = s->sh_size;
        }
        fwrite(sh, 1, sizeof(ElfW(Shdr)), f);
    }
}

static void 输出二进制文件(虚拟机ST *s1, FILE *f,
                              const int *sec_order)
{
    节ST *s;
    int i, offset, 大小;

    offset = 0;
    for(i=1;i<s1->节的数量;i++) {
        s = s1->节数组[sec_order[i]];
        if (s->sh_type != SHT_无数据 &&
            (s->sh_flags & SHF_执行时占用内存)) {
            while (offset < s->文件偏移量) {
                fputc(0, f);
                offset++;
            }
            大小 = s->sh_size;
            fwrite(s->数据, 1, 大小, f);
            offset += 大小;
        }
    }
}

/* 写一个 elf, coff 或 "二进制" 文件 */
static int 写ELF文件(虚拟机ST *s1, const char *文件名, int phnum,
                              ElfW(Phdr) *phdr, int file_offset, int *sec_order)
{
    int 文件描述, mode, file_type;
    FILE *f;

    file_type = s1->输出类型;
    if (file_type == 输出_OBJ)
        mode = 0666;
    else
        mode = 0777;
    unlink(文件名);
    文件描述 = open(文件名, O_WRONLY | O_CREAT | O_TRUNC | O_BINARY, mode);
    if (文件描述 < 0) {
        错误_不中止("could not write '%s'", 文件名);
        return -1;
    }
    f = fdopen(文件描述, "wb");
    if (s1->显示详情)
        printf("<- %s\n", 文件名);

#ifdef 目标_COFF
    if (s1->输出格式 == 输出格式_COFF)
        输出coff文件(s1, f);
    else
#endif
    if (s1->输出格式 == 输出格式_ELF)
        zhi_output_elf(s1, f, phnum, phdr, file_offset, sec_order);
    else
        输出二进制文件(s1, f, sec_order);
    fclose(f);

    return 0;
}

#ifndef ELF_OBJ_ONLY
/* Sort section headers by assigned sh_addr, remove 节数组
   that we aren't going to output.  */
static void tidy_section_headers(虚拟机ST *s1, int *sec_order)
{
    int i, nnew, l, *backmap;
    节ST **snew, *s;
    ElfW(Sym) *sym;

    snew = zhi_分配内存(s1->节的数量 * sizeof(snew[0]));
    backmap = zhi_分配内存(s1->节的数量 * sizeof(backmap[0]));
    for (i = 0, nnew = 0, l = s1->节的数量; i < s1->节的数量; i++) {
	s = s1->节数组[sec_order[i]];
	if (!i || s->sh_name) {
	    backmap[sec_order[i]] = nnew;
	    snew[nnew] = s;
	    ++nnew;
	} else {
	    backmap[sec_order[i]] = 0;
	    snew[--l] = s;
	}
    }
    for (i = 0; i < nnew; i++) {
	s = snew[i];
	if (s) {
	    s->ELF节数量 = i;
            if (s->sh_type == SHT_RELX)
		s->sh_info = backmap[s->sh_info];
	}
    }

    for_each_elem(节符号表_数组, 1, sym, ElfW(Sym))
	if (sym->st_shndx != SHN_未定义 && sym->st_shndx < SHN_LORESERVE)
	    sym->st_shndx = backmap[sym->st_shndx];
    if ( !s1->静态_链接 ) {
        for_each_elem(s1->动态符号节, 1, sym, ElfW(Sym))
	    if (sym->st_shndx != SHN_未定义 && sym->st_shndx < SHN_LORESERVE)
	        sym->st_shndx = backmap[sym->st_shndx];
    }
    for (i = 0; i < s1->节的数量; i++)
	sec_order[i] = i;
    zhi_释放(s1->节数组);
    s1->节数组 = snew;
    s1->节的数量 = nnew;
    zhi_释放(backmap);
}

#ifdef 目标_ARM
static void create_arm_attribute_section(虚拟机ST *s1)
{
   // Needed for DLL support.
    static const unsigned char arm_attr[] = {
        0x41,                            // 'A'
        0x2c, 0x00, 0x00, 0x00,          // 大小 0x2c
        'a', 'e', 'a', 'b', 'i', 0x00,   // "aeabi"
        0x01, 0x22, 0x00, 0x00, 0x00,    // 'File Attributes', 大小 0x22
        0x05, 0x36, 0x00,                // 'CPU_name', "6"
        0x06, 0x06,                      // 'CPU_arch', 'v6'
        0x08, 0x01,                      // 'ARM_ISA_use', 'Yes'
        0x09, 0x01,                      // 'THUMB_ISA_use', 'Thumb-1'
        0x0a, 0x02,                      // 'FP_arch', 'VFPv2'
        0x12, 0x04,                      // 'ABI_PCS_wchar_t', 4
        0x14, 0x01,                      // 'ABI_FP_denormal', 'Needed'
        0x15, 0x01,                      // 'ABI_FP_exceptions', 'Needed'
        0x17, 0x03,                      // 'ABI_FP_number_model', 'IEEE 754'
        0x18, 0x01,                      // 'ABI_align_needed', '8-byte'
        0x19, 0x01,                      // 'ABI_align_preserved', '8-byte, except leaf SP'
        0x1a, 0x02,                      // 'ABI_enum_size', 'int'
        0x1c, 0x01,                      // 'ABI_VFP_args', 'VFP registers'
        0x22, 0x01                       // 'CPU_unaligned_access', 'v6'
    };
    节ST *attr = 节_新建(s1, ".ARM.attributes", SHT_ARM_ATTRIBUTES, 0);
    unsigned char *ptr = 节_预留指定大小内存(attr, sizeof(arm_attr));
    attr->sh_地址对齐 = 1;
    memcpy(ptr, arm_attr, sizeof(arm_attr));
    if (s1->浮点_abi != ARM_HARD_FLOAT) {
        ptr[26] = 0x00; // 'FP_arch', 'No'
        ptr[41] = 0x1e; // 'ABI_optimization_goals'
        ptr[42] = 0x06; // 'Aggressive Debug'
    }
}
#endif

#if 目标系统_OpenBSD || 目标系统_NetBSD
static 节ST *create_bsd_note_section(虚拟机ST *s1,
					const char *name,
					const char *value)
{
    节ST *s = 查找_节_创建 (s1, name, 1);

    if (s->当前数据_偏移量 == 0) {
        char *ptr = 节_预留指定大小内存(s, sizeof(ElfW(Nhdr)) + 8 + 4);
        ElfW(Nhdr) *note = (ElfW(Nhdr) *) ptr;

        s->sh_type = SHT_NOTE;
        note->n_namesz = 8;
        note->n_descsz = 4;
        note->符号_类型 = ELF_NOTE_OS_GNU;
	strcpy (ptr + sizeof(ElfW(Nhdr)), value);
    }
    return s;
}
#endif

/* 输出一个elf, coff 或 binary 文件 */
/* XXX: 抑制不需要的 节数组 */
static int 生成ELF文件(虚拟机ST *s1, const char *文件名)
{
    int i, ret, phnum, phfill, shnum, file_type, file_offset, *sec_order;
    struct dyn_inf dyninf = {0};
    struct ro_inf roinf;
    ElfW(Phdr) *phdr;
    节ST *interp, *dynamic, *dynstr, *note;
    struct ro_inf *roinf_use = NULL;
    int textrel;

    file_type = s1->输出类型;
    s1->错误_数量 = 0;
    ret = -1;
    phdr = NULL;
    sec_order = NULL;
    interp = dynamic = dynstr = note = NULL;

#ifdef 目标_ARM
    create_arm_attribute_section (s1);
#endif

#if 目标系统_OpenBSD
    note = create_bsd_note_section (s1, ".note.openbsd.ident", "OpenBSD");
#endif

#if 目标系统_NetBSD
    note = create_bsd_note_section (s1, ".note.netbsd.ident", "NetBSD");
#endif

    {
        /* if linking, also link in runtime libraries (libc, libgcc, etc.) */
        zhi_添加运行时库(s1);
	解析_公共_符号(s1);

        if (!s1->静态_链接) {
            if (file_type == 输出_EXE) {
                char *ptr;
                /* allow override the dynamic loader */
                const char *elfint = getenv("LD_SO");
                if (elfint == NULL)
                    elfint = 默认ELF解释器M(s1);
                /* add interpreter section only if executable */
                interp = 节_新建(s1, ".interp", SHT_程序数据, SHF_执行时占用内存);
                interp->sh_地址对齐 = 1;
                ptr = 节_预留指定大小内存(interp, 1 + strlen(elfint));
                strcpy(ptr, elfint);
            }

            /* add dynamic symbol table */
            s1->动态符号节 = 新建符号表节(s1, ".动态符号节", SHT_DYNSYM, SHF_执行时占用内存,
                                    ".dynstr",
                                    ".hash", SHF_执行时占用内存);
	    /* Number of local symbols (readelf complains if not set) */
	    s1->动态符号节->sh_info = 1;
            dynstr = s1->动态符号节->link;
            /* add dynamic section */
            dynamic = 节_新建(s1, ".dynamic", SHT_动态链接信息,
                                  SHF_执行时占用内存 | SHF_可写);
            dynamic->link = dynstr;
            dynamic->sh_entsize = sizeof(ElfW(Dyn));

            build_got(s1);

            if (file_type == 输出_EXE) {
                绑定可执行文件符号(s1);
                if (s1->错误_数量)
                    goto 结_束;
                绑定库符号(s1);
            } else {
                /* shared library case: simply export all global symbols */
                导出全局符号(s1);
            }
        }
        build_got_entries(s1);
	版本_添加 (s1);
    }

    textrel = set_sec_sizes(s1);
    alloc_sec_names(s1, 0);

    if (!s1->静态_链接) {
        int i;
        /* add a list of needed dlls */
        for(i = 0; i < s1->加载的DLL数量; i++) {
            DLL引用ST *dllref = s1->加载的DLL数组[i];
            if (dllref->level == 0)
                put_dt(dynamic, DT_NEEDED, 放置elf字符串(dynstr, dllref->name));
        }

        if (s1->rpath)
            put_dt(dynamic, s1->enable_new_dtags ? DT_RUNPATH : DT_RPATH,
                   放置elf字符串(dynstr, s1->rpath));

        if (file_type == 输出_DLL) {
            if (s1->soname)
                put_dt(dynamic, DT_SONAME, 放置elf字符串(dynstr, s1->soname));
            /* XXX: currently, since we do not handle PIC code, we
               must 重新定位 the readonly segments */
            if (textrel)
                put_dt(dynamic, DT_TEXTREL, 0);
        }

        if (s1->当前模块的符号)
            put_dt(dynamic, DT_SYMBOLIC, 0);

        dyninf.dynamic = dynamic;
        dyninf.dynstr = dynstr;
        /* remember offset and reserve space for 2nd call below */
        dyninf.当前数据_偏移量 = dynamic->当前数据_偏移量;
        fill_dynamic(s1, &dyninf);
        dynamic->sh_size = dynamic->当前数据_偏移量;
        dynstr->sh_size = dynstr->当前数据_偏移量;
    }

    for (i = 1; i < s1->节的数量 &&
                !(s1->节数组[i]->sh_flags & SHF_TLS); i++);
    phfill = 2 + (i < s1->节的数量);

    /* compute number of program headers */
    if (file_type == 输出_DLL)
        phnum = 3;
    else if (s1->静态_链接)
        phnum = 3;
    else {
        phnum = 5 + (i < s1->节的数量);
    }

    phnum += note != NULL;
#if !目标系统_FreeBSD && !目标系统_NetBSD
    /* GNU_RELRO */
    phnum++, roinf_use = &roinf;
#endif

    /* allocate program segment headers */
    phdr = 初始化内存(phnum * sizeof(ElfW(Phdr)));
    /* compute number of 节数组 */
    shnum = s1->节的数量;
    /* this array is used to reorder 节数组 in the output 文件 */
    sec_order = zhi_分配内存(sizeof(int) * shnum);
    sec_order[0] = 0;

    /* compute section to program header mapping */
    file_offset = layout_sections(s1, phdr, phnum, phfill, interp, &roinf, sec_order + 1);

    /* Fill remaining program header and finalize relocation related to dynamic
       linking. */
    {
        fill_unloadable_phdr(phdr, phnum, interp, dynamic, note, roinf_use);
        if (dynamic) {
            ElfW(Sym) *sym;

            /* put in GOT the dynamic section address and 重新定位 PLT */
            write32le(s1->got->数据, dynamic->sh_addr);
            if (file_type == 输出_EXE
                || (RELOCATE_DLLPLT && file_type == 输出_DLL))
                relocate_plt(s1);

            /* 重新定位 symbols in .动态符号节 now that final addresses are known */
            for_each_elem(s1->动态符号节, 1, sym, ElfW(Sym)) {
                if (sym->st_shndx != SHN_未定义 && sym->st_shndx < SHN_LORESERVE) {
                    /* do symbol relocation */
                    sym->st_value += s1->节数组[sym->st_shndx]->sh_addr;
                }
            }
        }

        /* if building executable or DLL, then 重新定位 each section
           except the GOT which is already relocated */
        重定位符号地址(s1, s1->符号表节, 0);
        ret = -1;
        if (s1->错误_数量 != 0)
            goto 结_束;
        重定位_所有节(s1);
        if (dynamic) {
	    update_reloc_sections (s1, &dyninf);
            dynamic->当前数据_偏移量 = dyninf.当前数据_偏移量;
            fill_dynamic(s1, &dyninf);
	}
	tidy_section_headers(s1, sec_order);

        /* Perform relocation to GOT or PLT entries */
        if (file_type == 输出_EXE && s1->静态_链接)
            fill_got(s1);
        else if (s1->got)
            fill_local_got_entries(s1);
    }
    /* Create the ELF 文件 with name '文件名' */
    ret = 写ELF文件(s1, 文件名, phnum, phdr, file_offset, sec_order);
    s1->节的数量 = shnum;

 结_束:
    zhi_释放(sec_order);
    zhi_释放(phdr);
    return ret;
}
#endif /* ndef ELF_OBJ_ONLY */

/* Allocate strings for section names */
static void alloc_sec_names(虚拟机ST *s1, int is_obj)
{
    int i;
    节ST *s, *strsec;

    strsec = 节_新建(s1, ".shstrtab", SHT_字符串表, 0);
    放置elf字符串(strsec, "");
    for(i = 1; i < s1->节的数量; i++) {
        s = s1->节数组[i];
        if (is_obj)
            s->sh_size = s->当前数据_偏移量;
	if (s == strsec || s->sh_size || (s->sh_flags & SHF_执行时占用内存))
            s->sh_name = 放置elf字符串(strsec, s->name);
    }
    strsec->sh_size = strsec->当前数据_偏移量;
}

static int layout_any_sections(虚拟机ST *s1, int file_offset, int *sec_order, int is_obj)
{
    int i;
    节ST *s;
    for(i = 1; i < s1->节的数量; i++) {
        s = s1->节数组[i];
        if (!is_obj && (s->sh_flags & SHF_执行时占用内存))
            continue;
        *sec_order++ = i;
        file_offset = (file_offset + s->sh_地址对齐 - 1) &
            ~(s->sh_地址对齐 - 1);
        s->文件偏移量 = file_offset;
        if (s->sh_type != SHT_无数据)
            file_offset += s->sh_size;
    }
    return file_offset;
}

/* 输出一个 elf .o 文件 */
static int elf_output_obj(虚拟机ST *s1, const char *文件名)
{
    int ret, file_offset;
    int *sec_order;
    s1->错误_数量 = 0;

    /* Allocate strings for section names */
    alloc_sec_names(s1, 1);

    /* this array is used to reorder 节数组 in the output 文件 */
    sec_order = zhi_分配内存(sizeof(int) * s1->节的数量);
    sec_order[0] = 0;
    file_offset = layout_any_sections(s1, sizeof (ElfW(Ehdr)), sec_order + 1, 1);

    /* Create the ELF 文件 with name '文件名' */
    ret = 写ELF文件(s1, 文件名, 0, NULL, file_offset, sec_order);
    zhi_释放(sec_order);
    return ret;
}
/* 输出可执行文件、库文件或目标文件。 之前不要调用 zhi_重定位()。 */
核心库接口 int 链接器(虚拟机ST *s, const char *输出文件名)
{
    if (s->测试_覆盖率)
    {
    	测试覆盖_添加文件(s, 输出文件名);
    }

    if (s->输出类型 == 输出_OBJ)
    {
    	return elf_output_obj(s, 输出文件名);
    }

#ifdef 目标_PE
    return  生成PE文件(s, 输出文件名);
#elif 目标_MACHO
    return 生成MACHO文件(s, 输出文件名);
#else
    return 生成ELF文件(s, 输出文件名);
#endif
}
/*文件内容全部读到buf指针所指的内存中*/
静态函数 ssize_t 全读(int 文件描述, void *buf, size_t count) {
    char *cbuf = buf;
    size_t rnum = 0;
    while (1)
    {
        ssize_t num = read(文件描述, cbuf, count-rnum);/*文件描述 所指的文件传送count-rnum个字节到cbuf指针所指的内存中. 若参数count-rnum为0, 则read()不会有作用并返回0. 返回值为实际读取到的字节数, 如果返回0, 表示已到达文件尾或是无可读取的数据,此外文件读写位置会随读取到的字节移动.*/
        if (num < 0) return num;
        if (num == 0) return rnum;
        rnum += num;
        cbuf += num;
    }
}

静态函数 void *load_data(int 文件描述, unsigned long file_offset, unsigned long 大小)
{
    void *数据;

    数据 = zhi_分配内存(大小);
    lseek(文件描述, file_offset, SEEK_SET);
    全读(文件描述, 数据, 大小);
    return 数据;
}

typedef struct 节合并信息S {
    节ST *s;            /* 对应现有节 */
    unsigned long offset;  /* 现有节中新节的偏移量 */
    uint8_t 节_新建;       /* 如果添加了节's'，则为真 */
    uint8_t 链接一次;         /* 如果链接一次节为真 */
} 节合并信息S;

静态函数 int 取目标文件类型(int 文件描述, ElfW(Ehdr) *h)
{
    int 大小 = 全读(文件描述, h, sizeof *h);
    if (大小 == sizeof *h && 0 == memcmp(h, ELFMAG, 4))
    {
        if (h->e_type == ET_REL)
            return 目标文件_类型_REL;
        if (h->e_type == ET_DYN)
            return 目标文件_类型_DYN;
    } else if (大小 >= 8)
    {
        if (0 == memcmp(h, ARMAG, 8))
            return 目标文件_类型_AR;
#ifdef 目标_COFF
        if (((struct filehdr*)h)->f_目标机器类型 == COFF_C67_MAGIC)
            return 目标文件_类型_C67;
#endif
    }
    return 0;
}

/* load an object 文件 and merge it with current files */
/* XXX: handle correctly stab (debug) info */
静态函数 int 加载目标文件(虚拟机ST *s1,
                                int 文件描述, unsigned long file_offset)
{
    ElfW(Ehdr) ehdr;
    ElfW(Shdr) *shdr, *sh;
    int 大小, i, j, offset, offseti, nb_syms, 符号_索引, ret, seencompressed;
    char *strsec, *strtab;
    int stab_index, stabstr_index;
    int *old_to_new_syms;
    char *sh_name, *name;
    节合并信息S *sm_table, *sm;
    ElfW(Sym) *sym, *符号表节;
    ElfW_Rel *rel;
    节ST *s;

    lseek(文件描述, file_offset, SEEK_SET);
    if (取目标文件类型(文件描述, &ehdr) != 目标文件_类型_REL)
        goto fail1;
    /* test CPU specific stuff */
    if (ehdr.e_ident[5] != ELFDATA2LSB ||
        ehdr.e_machine != EM_目标机) {
    fail1:
        错误_不中止("invalid object 文件");
        return -1;
    }
    /* read 节数组 */
    shdr = load_data(文件描述, file_offset + ehdr.e_shoff,
                     sizeof(ElfW(Shdr)) * ehdr.e_shnum);
    sm_table = 初始化内存(sizeof(节合并信息S) * ehdr.e_shnum);

    /* load section names */
    sh = &shdr[ehdr.e_shstrndx];
    strsec = load_data(文件描述, file_offset + sh->文件偏移量, sh->sh_size);

    /* load 符号表节 and strtab */
    old_to_new_syms = NULL;
    符号表节 = NULL;
    strtab = NULL;
    nb_syms = 0;
    seencompressed = 0;
    stab_index = stabstr_index = 0;

    for(i = 1; i < ehdr.e_shnum; i++) {
        sh = &shdr[i];
        if (sh->sh_type == SHT_符号表) {
            if (符号表节) {
                错误_不中止("object must contain only one 符号表节");
            失败:
                ret = -1;
                goto 结_束;
            }
            nb_syms = sh->sh_size / sizeof(ElfW(Sym));
            符号表节 = load_data(文件描述, file_offset + sh->文件偏移量, sh->sh_size);
            sm_table[i].s = 节符号表_数组;

            /* now load strtab */
            sh = &shdr[sh->sh_link];
            strtab = load_data(文件描述, file_offset + sh->文件偏移量, sh->sh_size);
        }
	if (sh->sh_flags & SHF_COMPRESSED)
	    seencompressed = 1;
    }

    /* now examine each section and try to merge its content with the
       ones in memory */
    for(i = 1; i < ehdr.e_shnum; i++) {
        /* no need to examine section name strtab */
        if (i == ehdr.e_shstrndx)
            continue;
        sh = &shdr[i];
	if (sh->sh_type == SHT_RELX)
	  sh = &shdr[sh->sh_info];
        /* ignore 节数组 types we do not handle (plus relocs to those) */
        if (sh->sh_type != SHT_程序数据 &&
#ifdef ZHI_ARM_EABI
            sh->sh_type != SHT_ARM_EXIDX &&
#endif
#if 目标系统_OpenBSD || 目标系统_FreeBSD || 目标系统_NetBSD
            sh->sh_type != SHT_X86_64_UNWIND &&
#endif
            sh->sh_type != SHT_NOTE &&
            sh->sh_type != SHT_无数据 &&
            sh->sh_type != SHT_PREINIT_ARRAY &&
            sh->sh_type != SHT_INIT_ARRAY &&
            sh->sh_type != SHT_FINI_ARRAY &&
            strcmp(strsec + sh->sh_name, ".stabstr")
            )
            continue;
	if (seencompressed
	    && !strncmp(strsec + sh->sh_name, ".debug_", sizeof(".debug_")-1))
	  continue;

	sh = &shdr[i];
        sh_name = strsec + sh->sh_name;
        if (sh->sh_地址对齐 < 1)
            sh->sh_地址对齐 = 1;
        /* find corresponding section, if any */
        for(j = 1; j < s1->节的数量;j++) {
            s = s1->节数组[j];
            if (!strcmp(s->name, sh_name)) {
                if (!strncmp(sh_name, ".gnu.linkonce",
                             sizeof(".gnu.linkonce") - 1)) {
                    /* if a 'linkonce' section is already present, we
                       do not add it again. It is a little tricky as
                       symbols can still be defined in
                       it. */
                    sm_table[i].链接一次 = 1;
                    goto next;
                }
                if (调试节_数组) {
                    if (s == 调试节_数组)
                        stab_index = i;
                    if (s == 调试节_数组->link)
                        stabstr_index = i;
                }
                goto found;
            }
        }
        /* not found: create new section */
        s = 节_新建(s1, sh_name, sh->sh_type, sh->sh_flags & ~SHF_小组);
        /* take as much info as possible from the section. sh_link and
           sh_info will be updated later */
        s->sh_地址对齐 = sh->sh_地址对齐;
        s->sh_entsize = sh->sh_entsize;
        sm_table[i].节_新建 = 1;
    found:
        if (sh->sh_type != s->sh_type) {
#if 目标系统_OpenBSD || 目标系统_FreeBSD || 目标系统_NetBSD
            if (strcmp (s->name, ".eh_frame"))
#endif
            {
                错误_不中止("invalid section 类型");
                goto 失败;
	    }
        }
        /* 对齐 start of section */
        s->当前数据_偏移量 += -s->当前数据_偏移量 & (sh->sh_地址对齐 - 1);
        if (sh->sh_地址对齐 > s->sh_地址对齐)
            s->sh_地址对齐 = sh->sh_地址对齐;
        sm_table[i].offset = s->当前数据_偏移量;
        sm_table[i].s = s;
        /* concatenate 节数组 */
        大小 = sh->sh_size;
        if (sh->sh_type != SHT_无数据) {
            unsigned char *ptr;
            lseek(文件描述, file_offset + sh->文件偏移量, SEEK_SET);
            ptr = 节_预留指定大小内存(s, 大小);
            全读(文件描述, ptr, 大小);
        } else {
            s->当前数据_偏移量 += 大小;
        }
    next: ;
    }

    /* gr 重新定位 stab strings */
    if (stab_index && stabstr_index) {
        字符串表符号ST *a, *b;
        unsigned o;
        s = sm_table[stab_index].s;
        a = (字符串表符号ST *)(s->数据 + sm_table[stab_index].offset);
        b = (字符串表符号ST *)(s->数据 + s->当前数据_偏移量);
        o = sm_table[stabstr_index].offset;
        while (a < b) {
            if (a->字符串_索引)
                a->字符串_索引 += o;
            a++;
        }
    }

    /* second short pass to update sh_link and sh_info fields of new
       节数组 */
    for(i = 1; i < ehdr.e_shnum; i++) {
        s = sm_table[i].s;
        if (!s || !sm_table[i].节_新建)
            continue;
        sh = &shdr[i];
        if (sh->sh_link > 0)
            s->link = sm_table[sh->sh_link].s;
        if (sh->sh_type == SHT_RELX) {
            s->sh_info = sm_table[sh->sh_info].s->ELF节数量;
            /* update backward link */
            s1->节数组[s->sh_info]->重定位 = s;
        }
    }

    /* resolve symbols */
    old_to_new_syms = 初始化内存(nb_syms * sizeof(int));

    sym = 符号表节 + 1;
    for(i = 1; i < nb_syms; i++, sym++) {
        if (sym->st_shndx != SHN_未定义 &&
            sym->st_shndx < SHN_LORESERVE) {
            sm = &sm_table[sym->st_shndx];
            if (sm->链接一次) {
                /* if a symbol is in a link once section, we use the
                   already defined symbol. It is very important to get
                   correct relocations */
                if (ELFW(ST_BIND)(sym->st_info) != STB_LOCAL) {
                    name = strtab + sym->st_name;
                    符号_索引 = 查找elf符号(节符号表_数组, name);
                    if (符号_索引)
                        old_to_new_syms[i] = 符号_索引;
                }
                continue;
            }
            /* if no corresponding section added, no need to add symbol */
            if (!sm->s)
                continue;
            /* convert section number */
            sym->st_shndx = sm->s->ELF节数量;
            /* offset value */
            sym->st_value += sm->offset;
        }
        /* add symbol */
        name = strtab + sym->st_name;
        符号_索引 = 设置elf符号(节符号表_数组, sym->st_value, sym->st_size,
                                sym->st_info, sym->st_other,
                                sym->st_shndx, name);
        old_to_new_syms[i] = 符号_索引;
    }

    /* third pass to patch relocation entries */
    for(i = 1; i < ehdr.e_shnum; i++) {
        s = sm_table[i].s;
        if (!s)
            continue;
        sh = &shdr[i];
        offset = sm_table[i].offset;
        大小 = sh->sh_size;
        switch(s->sh_type) {
        case SHT_RELX:
            /* take relocation offset information */
            offseti = sm_table[sh->sh_info].offset;
	    for (rel = (ElfW_Rel *) s->数据 + (offset / sizeof(*rel));
		 rel < (ElfW_Rel *) s->数据 + ((offset + 大小) / sizeof(*rel));
		 rel++) {
                int 类型;
                unsigned 符号_索引;
                /* convert symbol index */
                类型 = ELFW(R_TYPE)(rel->r_info);
                符号_索引 = ELFW(R_SYM)(rel->r_info);
                /* NOTE: only one 符号表节 assumed */
                if (符号_索引 >= nb_syms)
                    goto invalid_reloc;
                符号_索引 = old_to_new_syms[符号_索引];
                /* ignore 链接一次 in rel section. */
                if (!符号_索引 && !sm_table[sh->sh_info].链接一次
#ifdef 目标_ARM
                    && 类型 != R_ARM_V4BX
#elif defined 目标_RISCV64
                    && 类型 != R_RISCV_ALIGN
                    && 类型 != R_RISCV_RELAX
#endif
                   ) {
                invalid_reloc:
                    错误_不中止("Invalid relocation entry [%2d] '%s' @ %.8x",
                        i, strsec + sh->sh_name, (int)rel->r_offset);
                    goto 失败;
                }
                rel->r_info = ELFW(R_INFO)(符号_索引, 类型);
                /* offset the relocation offset */
                rel->r_offset += offseti;
#ifdef 目标_ARM
                /* Jumps and branches from a Thumb code to a PLT entry need
                   special handling since PLT entries are ARM code.
                   Unconditional bl instructions referencing PLT entries are
                   handled by converting these instructions into blx
                   instructions. Other case of instructions referencing a PLT
                   entry require to add a Thumb stub before the PLT entry to
                   switch to ARM mode. We set bit plt_thumb_stub of the
                   attribute of a symbol to indicate such a case. */
                if (类型 == R_ARM_THM_JUMP24)
                    获取符号属性(s1, 符号_索引, 1)->plt_thumb_stub = 1;
#endif
            }
            break;
        default:
            break;
        }
    }

    ret = 0;
 结_束:
    zhi_释放(符号表节);
    zhi_释放(strtab);
    zhi_释放(old_to_new_syms);
    zhi_释放(sm_table);
    zhi_释放(strsec);
    zhi_释放(shdr);
    return ret;
}

typedef struct ArchiveHeader {
    char ar_name[16];           /* name of this member */
    char ar_date[12];           /* 文件 mtime */
    char ar_uid[6];             /* owner uid; printed as decimal */
    char ar_gid[6];             /* owner gid; printed as decimal */
    char ar_mode[8];            /* 文件 mode, printed as octal   */
    char ar_size[10];           /* 文件 大小, printed as decimal */
    char ar_fmag[2];            /* should contain ARFMAG */
} ArchiveHeader;

#define ARFMAG "`\n"

static unsigned long long get_be(const uint8_t *b, int n)
{
    unsigned long long ret = 0;
    while (n)
        ret = (ret << 8) | *b++, --n;
    return ret;
}

static int read_ar_header(int 文件描述, int offset, ArchiveHeader *hdr)
{
    char *p, *e;
    int 长度;
    lseek(文件描述, offset, SEEK_SET);
    长度 = 全读(文件描述, hdr, sizeof(ArchiveHeader));
    if (长度 != sizeof(ArchiveHeader))
        return 长度 ? -1 : 0;
    p = hdr->ar_name;
    for (e = p + sizeof hdr->ar_name; e > p && e[-1] == ' ';)
        --e;
    *e = '\0';
    hdr->ar_size[sizeof hdr->ar_size-1] = 0;
    return 长度;
}

/* load only the objects which resolve undefined symbols */
static int zhi_load_alacarte(虚拟机ST *s1, int 文件描述, int 大小, int entrysize)
{
    int i, bound, nsyms, 符号_索引, 长度, ret = -1;
    unsigned long long off;
    uint8_t *数据;
    const char *ar_names, *p;
    const uint8_t *ar_index;
    ElfW(Sym) *sym;
    ArchiveHeader hdr;

    数据 = zhi_分配内存(大小);
    if (全读(文件描述, 数据, 大小) != 大小)
        goto 结_束;
    nsyms = get_be(数据, entrysize);
    ar_index = 数据 + entrysize;
    ar_names = (char *) ar_index + nsyms * entrysize;

    do {
        bound = 0;
        for (p = ar_names, i = 0; i < nsyms; i++, p += strlen(p)+1) {
            节ST *s = 节符号表_数组;
            符号_索引 = 查找elf符号(s, p);
            if (!符号_索引)
                continue;
            sym = &((ElfW(Sym) *)s->数据)[符号_索引];
            if(sym->st_shndx != SHN_未定义)
                continue;
            off = get_be(ar_index + i * entrysize, entrysize);
            长度 = read_ar_header(文件描述, off, &hdr);
            if (长度 <= 0 || memcmp(hdr.ar_fmag, ARFMAG, 2)) {
                错误_不中止("invalid archive");
                goto 结_束;
            }
            off += 长度;
            if (s1->显示详情 == 2)
                printf("   -> %s\n", hdr.ar_name);
            if (加载目标文件(s1, 文件描述, off) < 0)
                goto 结_束;
            ++bound;
        }
    } while(bound);
    ret = 0;
 结_束:
    zhi_释放(数据);
    return ret;
}

/* load a '.a' 文件 */
静态函数 int 加载存档文件(虚拟机ST *s1, int 文件描述, int alacarte)
{
    ArchiveHeader hdr;
    /* char magic[8]; */
    int 大小, 长度;
    unsigned long file_offset;
    ElfW(Ehdr) ehdr;

    /* 跳过 magic which was already checked */
    /* 全读(文件描述, magic, sizeof(magic)); */
    file_offset = sizeof ARMAG - 1;

    for(;;) {
        长度 = read_ar_header(文件描述, file_offset, &hdr);
        if (长度 == 0)
            return 0;
        if (长度 < 0) {
            错误_不中止("invalid archive");
            return -1;
        }
        file_offset += 长度;
        大小 = strtol(hdr.ar_size, NULL, 0);
        /* 对齐 to even */
        大小 = (大小 + 1) & ~1;
        if (alacarte) {
            /* coff symbol table : we handle it */
            if (!strcmp(hdr.ar_name, "/"))
                return zhi_load_alacarte(s1, 文件描述, 大小, 4);
            if (!strcmp(hdr.ar_name, "/SYM64/"))
                return zhi_load_alacarte(s1, 文件描述, 大小, 8);
        } else if (取目标文件类型(文件描述, &ehdr) == 目标文件_类型_REL) {
            if (s1->显示详情 == 2)
                printf("   -> %s\n", hdr.ar_name);
            if (加载目标文件(s1, 文件描述, file_offset) < 0)
                return -1;
        }
        file_offset += 大小;
    }
}

#ifndef ELF_OBJ_ONLY
/* Set LV[I] to the global index of sym-version (LIB,VERSION).  Maybe resizes
   LV, maybe create a new entry for (LIB,VERSION).  */
static void set_ver_to_ver(虚拟机ST *s1, int *n, int **lv, int i, char *库, char *version)
{
    while (i >= *n) {
        *lv = zhi_重新分配(*lv, (*n + 1) * sizeof(**lv));
        (*lv)[(*n)++] = -1;
    }
    if ((*lv)[i] == -1) {
        int v, prev_same_lib = -1;
        for (v = 0; v < 符号版本_数量; v++) {
            if (strcmp(符号_版本[v].库, 库))
              continue;
            prev_same_lib = v;
            if (!strcmp(符号_版本[v].version, version))
              break;
        }
        if (v == 符号版本_数量) {
            符号_版本 = zhi_重新分配 (符号_版本,
                                        (v + 1) * sizeof(*符号_版本));
            符号_版本[v].库 = 字符串增加一个宽度(库);
            符号_版本[v].version = 字符串增加一个宽度(version);
            符号_版本[v].out_index = 0;
            符号_版本[v].prev_same_lib = prev_same_lib;
            符号版本_数量++;
        }
        (*lv)[i] = v;
    }
}

/* Associates symbol SYM_INDEX (in dynsymtab) with sym-version index
   VERNDX.  */
static void
set_sym_version(虚拟机ST *s1, int 符号_索引, int verndx)
{
    if (符号_索引 >= 符号数量_到_版本) {
        int newelems = 符号_索引 ? 符号_索引 * 2 : 1;
        符号_到_版本 = zhi_重新分配(符号_到_版本,
                                     newelems * sizeof(*符号_到_版本));
        memset(符号_到_版本 + 符号数量_到_版本, -1,
               (newelems - 符号数量_到_版本) * sizeof(*符号_到_版本));
        符号数量_到_版本 = newelems;
    }
    if (符号_到_版本[符号_索引] < 0)
      符号_到_版本[符号_索引] = verndx;
}

struct versym_info {
    int nb_versyms;
    ElfW(Verdef) *verdef;
    ElfW(Verneed) *verneed;
    ElfW(Half) *versym;
    int nb_local_ver, *local_ver;
};


static void store_version(虚拟机ST *s1, struct versym_info *v, char *dynstr)
{
    char *库, *version;
    uint32_t next;
    int i;

#define	DEBUG_VERSION 0

    if (v->versym && v->verdef) {
      ElfW(Verdef) *vdef = v->verdef;
      库 = NULL;
      do {
        ElfW(Verdaux) *verdaux =
	  (ElfW(Verdaux) *) (((char *) vdef) + vdef->vd_aux);

#if DEBUG_VERSION
	printf ("verdef: version:%u flags:%u index:%u, hash:%u\n",
	        vdef->vd_version, vdef->vd_flags, vdef->vd_ndx,
		vdef->vd_hash);
#endif
	if (vdef->vd_cnt) {
          version = dynstr + verdaux->vda_name;

	  if (库 == NULL)
	    库 = version;
	  else
            set_ver_to_ver(s1, &v->nb_local_ver, &v->local_ver, vdef->vd_ndx,
                           库, version);
#if DEBUG_VERSION
	  printf ("  verdaux(%u): %s\n", vdef->vd_ndx, version);
#endif
	}
        next = vdef->vd_next;
        vdef = (ElfW(Verdef) *) (((char *) vdef) + next);
      } while (next);
    }
    if (v->versym && v->verneed) {
      ElfW(Verneed) *vneed = v->verneed;
      do {
        ElfW(Vernaux) *vernaux =
	  (ElfW(Vernaux) *) (((char *) vneed) + vneed->vn_aux);

        库 = dynstr + vneed->vn_file;
#if DEBUG_VERSION
	printf ("verneed: %u %s\n", vneed->vn_version, 库);
#endif
	for (i = 0; i < vneed->vn_cnt; i++) {
	  if ((vernaux->vna_other & 0x8000) == 0) { /* hidden */
              version = dynstr + vernaux->vna_name;
              set_ver_to_ver(s1, &v->nb_local_ver, &v->local_ver, vernaux->vna_other,
                             库, version);
#if DEBUG_VERSION
	    printf ("  vernaux(%u): %u %u %s\n",
		    vernaux->vna_other, vernaux->vna_hash,
		    vernaux->vna_flags, version);
#endif
	  }
	  vernaux = (ElfW(Vernaux) *) (((char *) vernaux) + vernaux->vna_next);
	}
        next = vneed->vn_next;
        vneed = (ElfW(Verneed) *) (((char *) vneed) + next);
      } while (next);
    }

#if DEBUG_VERSION
    for (i = 0; i < v->nb_local_ver; i++) {
      if (v->local_ver[i] > 0) {
        printf ("%d: 库: %s, version %s\n",
		i, 符号_版本[v->local_ver[i]].库,
                符号_版本[v->local_ver[i]].version);
      }
    }
#endif
}

/* load a DLL and all referenced DLLs. 'level = 0' means that the DLL
   is referenced by the user (so it should be added as DT_NEEDED in
   the generated ELF 文件) */
静态函数 int zhi_load_dll(虚拟机ST *s1, int 文件描述, const char *文件名, int level)
{
    ElfW(Ehdr) ehdr;
    ElfW(Shdr) *shdr, *sh, *sh1;
    int i, j, nb_syms, nb_dts, sym_bind, ret;
    ElfW(Sym) *sym, *动态符号节;
    ElfW(Dyn) *dt, *dynamic;

    char *dynstr;
    int 符号_索引;
    const char *name, *soname;
    DLL引用ST *dllref;
    struct versym_info v;

    全读(文件描述, &ehdr, sizeof(ehdr));

    /* test CPU specific stuff */
    if (ehdr.e_ident[5] != ELFDATA2LSB ||
        ehdr.e_machine != EM_目标机) {
        错误_不中止("bad architecture");
        return -1;
    }

    /* read 节数组 */
    shdr = load_data(文件描述, ehdr.e_shoff, sizeof(ElfW(Shdr)) * ehdr.e_shnum);

    /* load dynamic section and dynamic symbols */
    nb_syms = 0;
    nb_dts = 0;
    dynamic = NULL;
    动态符号节 = NULL; /* avoid warning */
    dynstr = NULL; /* avoid warning */
    memset(&v, 0, sizeof v);

    for(i = 0, sh = shdr; i < ehdr.e_shnum; i++, sh++) {
        switch(sh->sh_type) {
        case SHT_动态链接信息:
            nb_dts = sh->sh_size / sizeof(ElfW(Dyn));
            dynamic = load_data(文件描述, sh->文件偏移量, sh->sh_size);
            break;
        case SHT_DYNSYM:
            nb_syms = sh->sh_size / sizeof(ElfW(Sym));
            动态符号节 = load_data(文件描述, sh->文件偏移量, sh->sh_size);
            sh1 = &shdr[sh->sh_link];
            dynstr = load_data(文件描述, sh1->文件偏移量, sh1->sh_size);
            break;
        case SHT_GNU_verdef:
	    v.verdef = load_data(文件描述, sh->文件偏移量, sh->sh_size);
	    break;
        case SHT_GNU_verneed:
	    v.verneed = load_data(文件描述, sh->文件偏移量, sh->sh_size);
	    break;
        case SHT_GNU_版本符号表:
            v.nb_versyms = sh->sh_size / sizeof(ElfW(Half));
	    v.versym = load_data(文件描述, sh->文件偏移量, sh->sh_size);
	    break;
        default:
            break;
        }
    }

    /* compute the real library name */
    soname = 取文件名含扩展名(文件名);

    for(i = 0, dt = dynamic; i < nb_dts; i++, dt++) {
        if (dt->d_tag == DT_SONAME) {
            soname = dynstr + dt->d_un.d_val;
        }
    }

    /* if the dll is already loaded, do not load it */
    for(i = 0; i < s1->加载的DLL数量; i++) {
        dllref = s1->加载的DLL数组[i];
        if (!strcmp(soname, dllref->name)) {
            /* but update level if needed */
            if (level < dllref->level)
                dllref->level = level;
            ret = 0;
            goto 结_束;
        }
    }

    if (v.nb_versyms != nb_syms)
        zhi_释放 (v.versym), v.versym = NULL;
    else
        store_version(s1, &v, dynstr);

    /* add the dll and its level */
    添加DLL引用(s1, soname)->level = level;

    /* add dynamic symbols in dynsym_section */
    for(i = 1, sym = 动态符号节 + 1; i < nb_syms; i++, sym++) {
        sym_bind = ELFW(ST_BIND)(sym->st_info);
        if (sym_bind == STB_LOCAL)
            continue;
        name = dynstr + sym->st_name;
        符号_索引 = 设置elf符号(s1->临时动态符号表_节, sym->st_value, sym->st_size,
                                sym->st_info, sym->st_other, sym->st_shndx, name);
        if (v.versym) {
            ElfW(Half) vsym = v.versym[i];
            if ((vsym & 0x8000) == 0 && vsym > 0 && vsym < v.nb_local_ver)
                set_sym_version(s1, 符号_索引, v.local_ver[vsym]);
        }
    }

    /* load all referenced DLLs */
    for(i = 0, dt = dynamic; i < nb_dts; i++, dt++) {
        switch(dt->d_tag) {
        case DT_NEEDED:
            name = dynstr + dt->d_un.d_val;
            for(j = 0; j < s1->加载的DLL数量; j++) {
                dllref = s1->加载的DLL数组[j];
                if (!strcmp(name, dllref->name))
                    goto already_loaded;
            }
            if (zhi_添加_dll(s1, name, 标志_被引用_DLL) < 0) {
                错误_不中止("referenced dll '%s' not found", name);
                ret = -1;
                goto 结_束;
            }
        already_loaded:
            break;
        }
    }
    ret = 0;
 结_束:
    zhi_释放(dynstr);
    zhi_释放(动态符号节);
    zhi_释放(dynamic);
    zhi_释放(shdr);
    zhi_释放(v.local_ver);
    zhi_释放(v.verdef);
    zhi_释放(v.verneed);
    zhi_释放(v.versym);
    return ret;
}

#define LD_TOK_NAME 256
#define LD_TOK_EOF  (-1)

static int ld_inp(虚拟机ST *s1)
{
    char b;
    if (s1->cc != -1) {
        int c = s1->cc;
        s1->cc = -1;
        return c;
    }
    if (1 == read(s1->文件描述, &b, 1))
        return b;
    return 中_文件结尾;
}

/* return next ld script token */
static int ld_next(虚拟机ST *s1, char *name, int name_size)
{
    int c, d, 当前字符;
    char *q;

 重做:
    当前字符 = ld_inp(s1);
    switch(当前字符) {
    case ' ':
    case '\t':
    case '\f':
    case '\v':
    case '\r':
    case '\n':
        goto 重做;
    case '/':
        当前字符 = ld_inp(s1);
        if (当前字符 == '*') { /* comment */
            for (d = 0;; d = 当前字符) {
                当前字符 = ld_inp(s1);
                if (当前字符 == 中_文件结尾 || (当前字符 == '/' && d == '*'))
                    break;
            }
            goto 重做;
        } else {
            q = name;
            *q++ = '/';
            goto parse_name;
        }
        break;
    case '\\':
    /* case 'a' ... 'z': */
    case 'a':
       case 'b':
       case 'c':
       case 'd':
       case 'e':
       case 'f':
       case 'g':
       case 'h':
       case 'i':
       case 'j':
       case 'k':
       case 'l':
       case 'm':
       case 'n':
       case 'o':
       case 'p':
       case 'q':
       case 'r':
       case 's':
       case 't':
       case 'u':
       case 'v':
       case 'w':
       case 'x':
       case 'y':
       case 'z':
    /* case 'A' ... 'z': */
    case 'A':
       case 'B':
       case 'C':
       case 'D':
       case 'E':
       case 'F':
       case 'G':
       case 'H':
       case 'I':
       case 'J':
       case 'K':
       case 'L':
       case 'M':
       case 'N':
       case 'O':
       case 'P':
       case 'Q':
       case 'R':
       case 'S':
       case 'T':
       case 'U':
       case 'V':
       case 'W':
       case 'X':
       case 'Y':
       case 'Z':
    case '_':
    case '.':
    case '$':
    case '~':
        q = name;
    parse_name:
        for(;;) {
            if (!((当前字符 >= 'a' && 当前字符 <= 'z') ||
                  (当前字符 >= 'A' && 当前字符 <= 'Z') ||
                  (当前字符 >= '0' && 当前字符 <= '9') ||
                  strchr("/.-_+=$:\\,~", 当前字符)))
                break;
            if ((q - name) < name_size - 1) {
                *q++ = 当前字符;
            }
            当前字符 = ld_inp(s1);
        }
        s1->cc = 当前字符;
        *q = '\0';
        c = LD_TOK_NAME;
        break;
    case 中_文件结尾:
        c = LD_TOK_EOF;
        break;
    default:
        c = 当前字符;
        break;
    }
    return c;
}

static int ld_add_file(虚拟机ST *s1, const char 文件名[])
{
    if (文件名[0] == '/') {
        if (系统根目录M[0] == '\0'
            && 打开文件并进入编译(s1, 文件名, 标志类型_BIN) == 0)
            return 0;
        文件名 = 取文件名含扩展名(文件名);
    }
    return zhi_添加_dll(s1, 文件名, 0);
}

static int ld_add_file_list(虚拟机ST *s1, const char *cmd, int as_needed)
{
    char 文件名[1024], libname[1024];
    int t, group, nblibs = 0, ret = 0;
    char **libs = NULL;

    group = !strcmp(cmd, "GROUP");
    if (!as_needed)
        s1->新的未定义符号 = 0;
    t = ld_next(s1, 文件名, sizeof(文件名));
    if (t != 英_左小括号) {
        错误_不中止("( expected");
        ret = -1;
        goto lib_parse_error;
    }
    t = ld_next(s1, 文件名, sizeof(文件名));
    for(;;) {
        libname[0] = '\0';
        if (t == LD_TOK_EOF) {
            错误_不中止("unexpected end of 文件");
            ret = -1;
            goto lib_parse_error;
        } else if (t == 英_右小括号) {
            break;
        } else if (t == '-') {
            t = ld_next(s1, 文件名, sizeof(文件名));
            if ((t != LD_TOK_NAME) || (文件名[0] != 'l')) {
                错误_不中止("library name expected");
                ret = -1;
                goto lib_parse_error;
            }
            截取前n个字符(libname, sizeof libname, &文件名[1]);
            if (s1->静态_链接) {
                snprintf(文件名, sizeof 文件名, "库%s.a", libname);
            } else {
                snprintf(文件名, sizeof 文件名, "库%s.so", libname);
            }
        } else if (t != LD_TOK_NAME) {
            错误_不中止("文件名 expected");
            ret = -1;
            goto lib_parse_error;
        }
        if (!strcmp(文件名, "AS_NEEDED")) {
            ret = ld_add_file_list(s1, cmd, 1);
            if (ret)
                goto lib_parse_error;
        } else {
            /* TODO: Implement AS_NEEDED support. Ignore it for now */
            if (!as_needed) {
                ret = ld_add_file(s1, 文件名);
                if (ret)
                    goto lib_parse_error;
                if (group) {
                    /* Add the 文件名 *and* the libname to avoid future conversions */
                    动态数组_追加元素(&libs, &nblibs, 字符串增加一个宽度(文件名));
                    if (libname[0] != '\0')
                        动态数组_追加元素(&libs, &nblibs, 字符串增加一个宽度(libname));
                }
            }
        }
        t = ld_next(s1, 文件名, sizeof(文件名));
        if (t == 英_逗号) {
            t = ld_next(s1, 文件名, sizeof(文件名));
        }
    }
    if (group && !as_needed) {
        while (s1->新的未定义符号) {
            int i;
            s1->新的未定义符号 = 0;
            for (i = 0; i < nblibs; i ++)
                ld_add_file(s1, libs[i]);
        }
    }
lib_parse_error:
    动态数组_重置(&libs, &nblibs);
    return ret;
}

/* 解释 GNU ldscripts（ld:俗称链接器。就是吃进去object files，输出可执行文件。） 的一个子集来处理虚拟的 libc.so 文件 */
静态函数 int 加载链接器脚本(虚拟机ST *s1, int 文件描述)
{
    char cmd[64];
    char 文件名[1024];
    int t, ret;

    s1->文件描述 = 文件描述;
    s1->cc = -1;
    for(;;) {
        t = ld_next(s1, cmd, sizeof(cmd));
        if (t == LD_TOK_EOF)
            return 0;
        else if (t != LD_TOK_NAME)
            return -1;
        if (!strcmp(cmd, "INPUT") ||
            !strcmp(cmd, "GROUP")) {
            ret = ld_add_file_list(s1, cmd, 0);
            if (ret)
                return ret;
        } else if (!strcmp(cmd, "OUTPUT_FORMAT") ||
                   !strcmp(cmd, "TARGET")) {
            /* ignore some commands */
            t = ld_next(s1, cmd, sizeof(cmd));
            if (t != 英_左小括号) {
                错误_不中止("( expected");
                return -1;
            }
            for(;;) {
                t = ld_next(s1, 文件名, sizeof(文件名));
                if (t == LD_TOK_EOF) {
                    错误_不中止("unexpected end of 文件");
                    return -1;
                } else if (t == 英_右小括号) {
                    break;
                }
            }
        } else {
            return -1;
        }
    }
    return 0;
}
#endif /* !ELF_OBJ_ONLY */
