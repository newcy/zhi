/****************************************************************************************************
 * 描述：zhi头文件，集中来声明原型以及全局变量、库函数等。
 * 版权(C) 2001-2004 Fabrice Bellard
 * 版权(C) 2019-2023 位中原
 * 网址：www.880.xin
 ****************************************************************************************************/
#ifndef ZHI_H
#define ZHI_H

#define _GNU_SOURCE
#define _DARWIN_C_SOURCE
#include "配置.h"

#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
/* gnu 头文件用于 #define __attribute__ 为非 gcc 编译器清空 */
#ifdef __ZHIXIN__
# undef __attribute__
#endif
#include <string.h>
#include <errno.h>
#include <math.h>
#include <fcntl.h>
#include <setjmp.h>
#include <time.h>

#ifndef _WIN32
# include <unistd.h>
# include <sys/time.h>
# ifndef 配置_ZHI_静态
#  include <dlfcn.h>
# endif
/* XXX: 需要定义它以在非 ISOC99 上下文中使用它们 */
extern float strtof (const char *__nptr, char **__endptr);
extern long double strtold (const char *__nptr, char **__endptr);
#endif

typedef struct 虚拟机S 虚拟机ST;

#ifdef _WIN32
# define WIN32_LEAN_AND_MEAN 1
# include <windows.h>
# include <io.h> /* open, close etc. */
# include <direct.h> /* getcwd */
# include <malloc.h> /* alloca */
# ifdef __GNUC__
#  include <stdint.h>
# endif
# define inline __inline
# define snprintf _snprintf
# define vsnprintf _vsnprintf
# ifndef __GNUC__
#  define strtold (long double)strtod
#  define strtof (float)strtod
#  ifdef _WIN64
#   define strtoll _strtoi64
#   define strtoull _strtoui64
#  else
#   define strtoll strtol
#   define strtoull strtoul
#  endif
# endif
# ifdef 核心作为动态库M
#  define 核心库接口 __declspec(dllexport)
#  define 公用函数 核心库接口
# endif
# define 取下个字符 next_inp /* 取下个字符（inp）是 msvc/mingw 上的一个内在函数 */
# ifdef _MSC_VER     /*_MSC_VER的意思就是：Microsoft的C编译器的版本。*/
#  pragma warning (disable : 4244)  // 从 'uint64_t' 到 'int' 的转换，可能丢失数据
#  pragma warning (disable : 4267)  // 从“size_t”到“int”的转换，可能丢失数据
#  pragma warning (disable : 4996)  // 不推荐使用此项的 POSIX 名称。 相反，使用符合 ISO C 和 C++ 的名称
#  pragma warning (disable : 4018)  // 有符号/无符号不匹配
#  pragma warning (disable : 4146)  // 一元减运算符应用于无符号类型，结果仍然无符号
#  define ssize_t intptr_t
#  ifdef _X86_
#   define __i386__ 1
#  endif
#  ifdef _AMD64_
#   define __x86_64__ 1
#  endif
# endif
# ifndef va_copy
#  define va_copy(a,b) a = b
# endif
# undef 配置_ZHI_静态
#endif

#ifndef 页大小M
# ifdef _SC_PAGESIZE
#   define 页大小M sysconf(_SC_PAGESIZE)
# else
#   define 页大小M 4096
# endif
#endif

#ifndef O_BINARY
# define O_BINARY 0
#endif

#ifndef offsetof
#define offsetof(类型, field) ((size_t) &((类型 *)0)->field)
#endif

#ifndef countof
#define countof(tab) (sizeof(tab) / sizeof((tab)[0]))
#endif

#ifdef _MSC_VER
# define NORETURN __declspec(noreturn)
# define ALIGNED(x) __declspec(对齐(x))
# define PRINTF_LIKE(x,y)
#else
# define NORETURN __attribute__((noreturn))
# define ALIGNED(x) __attribute__((aligned(x)))
# define PRINTF_LIKE(x,y) __attribute__ ((format (printf, (x), (y))))
#endif

#ifdef _WIN32
# define 是斜杠(c) (c == '/' || c == '\\')
# define IS_ABSPATH(p) (是斜杠(p[0]) || (p[0] && p[1] == 英_冒号 && 是斜杠(p[2])))
# define PATHCMP stricmp
# define 路径分割M ";"
#else
# define 是斜杠(c) (c == '/')
# define IS_ABSPATH(p) 是斜杠(p[0])
# define PATHCMP strcmp
# define 路径分割M ":"
#endif

/* -------------------------------------------- */
/* 默认目标是 I386 */
#if !defined(目标_I386) && !defined(目标_ARM) && !defined(目标_ARM64) && !defined(目标_C67) && !defined(目标_X86_64) && !defined(目标_RISCV64)
# if defined __x86_64__
#  define 目标_X86_64
# elif defined __arm__
#  define 目标_ARM
#  define ZHI_ARM_EABI
#  define ZHI_ARM_VFP
#  define ZHI_ARM_HARDFLOAT
# elif defined __aarch64__
#  define 目标_ARM64
# elif defined __riscv
#  define 目标_RISCV64
# else
#  define 目标_I386
# endif
# ifdef _WIN32
#  define 目标_PE 1
# endif
# ifdef __APPLE__
#  define 目标_MACHO 1
# endif
#endif

/* 仅本编译器支持 -run */
#if defined _WIN32 == defined 目标_PE && defined __APPLE__ == defined 目标_MACHO
# if defined __i386__ && defined 目标_I386
#  define 运行脚本M
# elif defined __x86_64__ && defined 目标_X86_64
#  define 运行脚本M
# elif defined __arm__ && defined 目标_ARM
#  define 运行脚本M
# elif defined __aarch64__ && defined 目标_ARM64
#  define 运行脚本M
# elif defined __riscv && defined __LP64__ && defined 目标_RISCV64
#  define 运行脚本M
# endif
#endif

#if !defined 运行脚本M || (defined 启用内置堆栈回溯M && 启用内置堆栈回溯M==0)
# undef 启用内置堆栈回溯M
#else
# define 启用内置堆栈回溯M 1
#endif

#if defined 启用边界检查M && 启用边界检查M==0
#  undef 启用边界检查M
#else
#  define 启用边界检查M 1
#endif

#if defined 目标系统_OpenBSD || defined 目标系统_FreeBSD || defined 目标系统_NetBSD || defined 目标系统_FreeBSD_kernel
# define 目标系统_BSD 1
#elif !(defined 目标_PE || defined 目标_MACHO)
# define 目标系统_Linux 1
#endif

#if defined 目标_PE || defined 目标_MACHO
# define ELF_OBJ_ONLY /* 创建 elf .o 但本机可执行文件 */
#endif

/* 除了由 mingw-GCC 制作的交叉编译器外，window 和 macos 上没有十字节长的双打 */
#if defined 目标_PE \
    || (defined 目标_MACHO && defined 目标_ARM64) \
    || (defined _WIN32 && !defined __GNUC__)
# define 为长双精度使用双精度M 1
#endif

/* ------------ 路径配置 ------------ */
/*Linux 的软件安装目录是也是有讲究的，理解这一点，在对系统管理是有益的
 * /usr：系统级的目录，可以理解为C:/Windows/，/usr/lib理解为C:/Windows/System32。
 * /usr/local：用户级的程序目录，可以理解为C:/Progrem Files/。用户自己编译的软件默认会安装到这个目录下。
 * /opt：用户级的程序目录，可以理解为D:/Software，opt有可选的意思，这里可以用于放置第三方大型软件（或游戏），当你不需要时，直接rm -rf掉即可。在硬盘容量不够时，也可将/opt单独挂载到其他磁盘上使用。
 * 源码放哪里？/usr/src：系统级的源码目录。/usr/local/src：用户级的源码目录.
 * */
#ifndef 系统根目录M
# define 系统根目录M ""
#endif
#if !defined 编译文件夹M && !defined _WIN32
# define 编译文件夹M "/usr/local/库/zhi"
#endif
#ifndef CONFIG_LDDIR
# define CONFIG_LDDIR "库"
#endif
#ifdef CONFIG_TRIPLET
# define USE_TRIPLET(s) s "/" CONFIG_TRIPLET
# define ALSO_TRIPLET(s) USE_TRIPLET(s) ":" s
#else
# define USE_TRIPLET(s) s
# define ALSO_TRIPLET(s) s
#endif

/* 找到 crt1.o、crti.o 和 crtn.o 的路径 */
#ifndef CONFIG_ZHI_CRTPREFIX
# define CONFIG_ZHI_CRTPREFIX USE_TRIPLET(系统根目录M "/usr/" CONFIG_LDDIR)
#endif

#ifndef 用户导入路径M
# define 用户导入路径M "/usr/头文件"
#endif

/* 以下: {B}被 编译文件夹M (rsp. -B 选项) 取代*/

/* 系统include路径（设置系统的头文件路径） */
#ifndef 头文件路径M
# if defined 目标_PE || defined _WIN32
#  define 头文件路径M "{B}/头文件"路径分割M"{B}/头文件/winapi"
# else
#  define 头文件路径M \
        "{B}/include" \
    ":" ALSO_TRIPLET(系统根目录M "/usr/local/include") \
    ":" ALSO_TRIPLET(系统根目录M 用户导入路径M)
# endif
#endif

/* 库查找路径 */
#ifndef 库路径M
# ifdef 目标_PE
#  define 库路径M "{B}/库"
# else
#  define 库路径M \
        ALSO_TRIPLET(系统根目录M "/usr/" CONFIG_LDDIR) \
    ":" ALSO_TRIPLET(系统根目录M "/" CONFIG_LDDIR) \
    ":" ALSO_TRIPLET(系统根目录M "/usr/local/" CONFIG_LDDIR)
# endif
#endif

/* name of ELF interpreter（ELF 解释器的名称） */
#ifndef ELF解释器M
# if 目标系统_FreeBSD
#  define ELF解释器M "/libexec/ld-elf.so.1"
# elif 目标系统_FreeBSD_kernel
#  if defined(目标_X86_64)
#   define ELF解释器M "/库/ld-kfreebsd-x86-64.so.1"
#  else
#   define ELF解释器M "/库/ld.so.1"
#  endif
# elif 目标系统_DragonFly
#  define ELF解释器M "/usr/libexec/ld-elf.so.2"
# elif 目标系统_NetBSD
#  define ELF解释器M "/usr/libexec/ld.elf_so"
# elif 目标系统_OpenBSD
#  define ELF解释器M "/usr/libexec/ld.so"
# elif defined __GNU__
#  define ELF解释器M "/库/ld.so"
# elif defined(目标_PE)
#  define ELF解释器M "-"
# elif defined(ZHI_UCLIBC)
#  define ELF解释器M "/库/ld-uClibc.so.0" /* is there a uClibc for x86_64 ? */
# elif defined 目标_ARM64
#  if defined(ZHI_MUSL)
#   define ELF解释器M "/库/ld-musl-aarch64.so.1"
#  else
#   define ELF解释器M "/库/ld-linux-aarch64.so.1"
#  endif
# elif defined(目标_X86_64)
#  if defined(ZHI_MUSL)
#   define ELF解释器M "/库/ld-musl-x86_64.so.1"
#  else
#   define ELF解释器M "/lib64/ld-linux-x86-64.so.2"
#  endif
# elif defined(目标_RISCV64)
#  define ELF解释器M "/库/ld-linux-riscv64-lp64d.so.1"
# elif !defined(ZHI_ARM_EABI)
#  if defined(ZHI_MUSL)
#   if defined(目标_I386)
#     define ELF解释器M "/库/ld-musl-i386.so.1"
#    else
#     define ELF解释器M "/库/ld-musl-arm.so.1"
#    endif
#  else
#   define ELF解释器M "/库/ld-linux.so.2"
#  endif
# endif
#endif

/* var elf_interp dans *-gen.c */
#ifdef ELF解释器M
# define 默认ELF解释器M(s) ELF解释器M
#else
# define 默认ELF解释器M(s) 默认_elf解释器(s)
#endif

/* (target specific) 运行时库.a */
#ifndef 运行时库M
# define 运行时库M "运行时库.a"
#endif

/*  与 CONFIG_USE_LIBGCC 一起使用的库，而不是 运行时库.a*/
#if defined CONFIG_USE_LIBGCC && !defined ZHI_LIBGCC
#define ZHI_LIBGCC USE_TRIPLET(系统根目录M "/" CONFIG_LDDIR) "/libgcc_s.so.1"
#endif

/* -------------------------------------------- */

#include "核心.h"
#include "Elf.h"
#include "stab.h"

/* -------------------------------------------- */

#ifndef 公用函数 /* 在zhi.c中使用但不在 核心.h 中的函数 */
#  define 公用函数
#endif

#ifndef 是源码M
#  define 是源码M 1
#endif

/* 支持从线程使用 核心.c */
#ifndef 配置_信号量_锁
# define 配置_信号量_锁 1
#endif

#if 是源码M
#  define ST_INLN static inline
#  define 静态函数 static
#  define 静态数据 static
#else
#  define ST_INLN
#  define 静态函数
#  define 静态数据 extern
#endif

#ifdef 配置函数M /* 配置所有函数 */
# define static
#endif

/* -------------------------------------------- */
/* 包含目标特定的定义 */

#define TARGET_DEFS_ONLY
#ifdef 目标_I386
# include "i386-生成.c"
# include "i386-链接.c"
#elif defined 目标_X86_64
# include "x86_64-生成.c"
# include "x86_64-链接.c"
#else
#error unknown target
#endif
#undef TARGET_DEFS_ONLY

/* -------------------------------------------- */

#if 宏_指针大小 == 8
# define ELFCLASSW ELFCLASS64
# define ElfW(类型) Elf##64##_##类型
# define ELFW(类型) ELF##64##_##类型
# define ElfW_Rel ElfW(Rela)
# define SHT_RELX SHT_带加数的重定位条目
# define REL_SECTION_FMT ".rela%s"
#else
# define ELFCLASSW ELFCLASS32
# define ElfW(类型) Elf##32##_##类型
# define ELFW(类型) ELF##32##_##类型
# define ElfW_Rel ElfW(Rel)
# define SHT_RELX SHT_REL
# define REL_SECTION_FMT ".rel%s"
#endif
/* 目标地址类型 */
#define addr_t ElfW(Addr)
#define ElfSym ElfW(Sym)

#if 宏_指针大小 == 8 && !defined 目标_PE
# define 长整数大小 8
#else
# define 长整数大小 4
#endif

/* -------------------------------------------- */

#define INCLUDE_栈_大小  32
#define IFDEF_栈_大小    64
#define 栈值大小         256
#define 字符串最大长度     1024
#define 单词字符串最大大小     256
#define 包栈大小M     8

#define 哈希表容量       16384     /* 必须是二的幂*/
#define 单词分配增量      512  /* 必须是二的幂 */
#define 单词最大大小M        4 /* 存储在字符串中时以 int 为单位的单词最大大小 */

typedef struct 单词S
{
    struct 单词S *哈希冲突的其他单词;
    struct 符号S *define符号;
    struct 符号S *label符号;
    struct 符号S *struct符号;
    struct 符号S *标识符符号;
    int 单词编码;
    int 长度;
    char 字符串[1];
} 单词ST;

#ifdef 目标_PE
typedef unsigned short nwchar_t;
#else
typedef int nwchar_t;
#endif

typedef struct 动态字符串S
{
    int  大小;  /* 字符串长度 */
    void *数据; /*指向字符串的指针 */
    int  容量;  /*包含该字符串的缓冲区长度*/
} 动态字符串ST;

typedef struct 类型S
{
    int 数据类型;            /*数据类型*/
    struct 符号S *引用符号;
} 类型ST;

typedef union 常量值U
{
    long double ld;
    double d;
    float f;
    uint64_t i;
    struct {
        const void *数据;
        int 大小;
    } 字符串;
    int tab[长双精度大小/4];
} 常量值U;

typedef struct 栈值S
{
    类型ST 类型;
    unsigned short 寄存qi;      /* 寄存qi + 标志 */
    unsigned short r2;     /* 第二个寄存器, 用于“long long”类型。 如果不使用，设置为VT_VC常量 */
    union {
      struct { int jtrue, jfalse; }; /* forward jmps */
      常量值U c;         /* constant, if 存储类型_VC常量 */
    };
    union {
      struct { unsigned short cmp_op, cmp_r; }; /* 存储类型_标志寄存器 operation */
      struct 符号S *sym;  /* symbol, if (存储类型_符号 | 存储类型_VC常量), or if */
    };                  /* result of 一元() for an identifier. */

} 栈值ST;

struct 符号属性S {
    unsigned short
    aligned     : 5, /* 对齐为 log2+1（0 == 未指定）*/
    packed      : 1,
    weak        : 1,
    能见度  : 2,
    dllexport   : 1,
    节点  : 1,
    dllimport   : 1,
    addrtaken   : 1,
    xxxx        : 3; /* 未使用 */
};

/* 用于解析的函数属性或临时属性*/
struct 函数属性S {
    unsigned
    函数调用约定   : 3,
    函数_类型  : 2,
    函数_不返回 : 1,
    函数_构造函数   : 1,
    函数_析构函数   : 1,
    函数_参数   : 8,
    函数_始终内联 : 1,
    xxxx        : 15;
};

/* 符号管理：符号表符号的存储结构 */
typedef struct 符号S {
    int v; /* 符号的单词编码 */
    unsigned short 寄存qi; /* 符号存储类型：关联寄存器或 存储类型_VC常量/存储类型_局部 和 LVAL 类型 */
    struct 符号属性S a;
    union
	{
        struct
		{
            int c; /* 符号关联值。相关数字或elf符号索引 */
            union
			{
                int 作用域范围; /* 适用于局部的 域 级别 */
                int jnext; /* 下一个跳转标签 */
                struct 函数属性S f; /* 函数属性 */
                int auxtype; /* 位域访问类型 */
            };
        };
        long long enum_val; /* enum constant if IS_ENUM_VAL */
        int *d; /* define token stream */
        struct 符号S *ncl; /* next 清理 */
    };
    类型ST 类型; /* 关联类型。符号的数据类型 */
    union
	{
        struct 符号S *next; /* 关联的其他符号：下一个相关符号（用于字段和异常） */
        struct 符号S *cleanupstate; /* in defined labels */
        int asm_label; /* 关联的 asm 标签 */
    };
    struct 符号S *上个; /* 在堆栈的前一个符号 */
    struct 符号S *定义的前一个同名符号; /* 指向前一定义的同名符号 */
} 符号ST;

/* 节定义 */
typedef struct 节S {
    unsigned long 当前数据_偏移量;
    unsigned char *数据;
    unsigned long data_allocated; /* 用于 realloc() 处理 */
    虚拟机ST *s1;
    int sh_name;             /* elf section name (only used during output) */
    int ELF节数量;              /* elf section number */
    int sh_type;             /* elf section 类型 */
    int sh_flags;            /* elf section flags */
    int sh_info;             /* elf section info */
    int sh_地址对齐;        /* elf section alignment */
    int sh_entsize;          /* elf entry 大小 */
    unsigned long sh_size;   /* section 大小 (only used during output) */
    addr_t sh_addr;          /* address at which the section is relocated */
    unsigned long 文件偏移量; /* 文件 offset */
    int nb_hashed_syms;      /* used to resize the hash table */
    struct 节S *link;    /* link to another section */
    struct 节S *重定位;   /* corresponding section for relocation, if any */
    struct 节S *hash;    /* hash table for symbols */
    struct 节S *上个;    /* previous section on section stack */
    char name[1];           /* section name */
} 节ST;

typedef struct DLL引用S {
    int level;
    void *handle;
    char name[1];
} DLL引用ST;

/* -------------------------------------------------- */

#define 符号_结构体     0x40000000 /* struct/union/enum symbol space */
#define 符号_字段      0x20000000 /* struct/union field symbol space */
#define 第一个匿名符号 0x10000000

/* 存储在'符号ST->f.函数_类型' 字段中 */
#define 函数原型_新       1 /* ansi函数原型 */
#define 函数原型_旧       2 /* 旧函数原型 */
#define 函数原型_省略  3 /* ansi函数原型与。。。 */

/* 存储在 '符号ST->f.函数调用约定' 字段中 */
enum 函数调用约定E
{
FUNC_CDECL     =0, /* 标准c调用 */
FUNC_STDCALL   =1, /* pascal c 调用 */
FUNC_FASTCALL1 =2,/*  %eax 中的第一个参数*/
FUNC_FASTCALL2 =3, /* %eax, %edx 中的第一个参数*/
FUNC_FASTCALL3 =4, /* %eax, %edx, %ecx 中的第一个参数*/
FUNC_FASTCALLW =5 /* %ecx, %edx 中的第一个参数*/
};
/* 宏的字段 '符号ST.数据类型' */
#define MACRO_OBJ      0 /* object like macro */
#define MACRO_FUNC     1 /* function like macro */

/* field '符号ST.寄存qi' for C labels */
#define 标签_已定义  0 /* 标签已定义*/
#define 标签_向前的  1 /* 标签是向前定义的 */
#define 标签_已声明 2 /* 标签已声明但从未使用 */
#define 标签_GONE     3 /* 标签不在 域S 中, 但也尚未从 局部_标签_栈 (stmt exprs) 中弹出 */

/* 解析初始声明符列表() 的类型 */
#define 声明符_抽象           1
#define 声明符_直接    2

#define 输入输出缓存大小M 8192
/*读取源文件的缓冲文件数据结构*/
typedef struct 缓冲文件S {
    uint8_t *缓冲源码串;   /*源码字符串，FILE *源码字符串;源码字符串 = fopen(源文件,"rb"); currentChar=getc(源码字符串);*/
    uint8_t *缓冲结尾;
    int 文件描述;
    struct 缓冲文件S *上个;/*上一个缓冲文件*/
    int 当前行号;
    int 最后打印_的行;    /* zhi -E: last printed line */
    int ifndef宏;  /* #ifndef macro / #endif search */
    int 保存的ifndef宏; /* saved ifndef宏 */
    int *ifdef_栈_ptr; /* ifdef_栈 value at the start of the 文件 */
    int 下一个包含文件的索引; /* next search path */
    char 文件名[1024];    /* 文件名 */
    char *真文件名; /* # line 指令未修改文件名 */
    unsigned char unget[4];
    unsigned char 缓冲[1]; /* 中_结尾 字符的额外大小 */
} 缓冲文件ST;

#define 中_结尾   '\\'       /* 缓冲区结尾或文件中的“\0”字符 */
#define 中_文件结尾   (-1)   /* end of 文件 */

/* 用于记录词法单元 */
typedef struct 单词字符串S {
    int *字符串;
    int 长度;
    int lastlen;
    int allocated_len;
    int 最后_行号;
    int save_line_num;
    /* 用于将令牌字符串与 begin/end_宏扩展() 链接*/
    struct 单词字符串S *上个;
    const int *prev_ptr;
    char alloc;
} 单词字符串ST;

typedef struct GNUC属性S {
    struct 符号属性S a;
    struct 函数属性S f;
    struct 节S *section;
    符号ST *清理_函数;
    int 别名_目标; /* token */
    int asm_label; /* 关联asm标签 */
    char 属性_模式; /* __attribute__((__mode__(...))) */
} GNUC属性ST;

typedef struct 内联函数S {
    单词字符串ST *函数体字符串; /*函数体字符串*/
    符号ST *sym;          /*函数名称*/
    char 文件名[1];
} 内联函数ST;

/* 包含文件缓存，用于更快地查找文件，并且如果包含文件受#ifndef ... #endif保护，还可以消除包含 */
typedef struct 导入文件缓存S {
    int ifndef宏;
    int once;
    int 哈希冲突的其他单词; /* -1 if none */
    char 文件名[1]; /* #include中指定的路径 */
} 导入文件缓存ST;

#define CACHED_INCLUDES_HASH_SIZE 32

#ifdef CONFIG_ZHI_ASM
typedef struct 表达式值S {
    uint64_t v;
    符号ST *sym;
    int pcrel;
} 表达式值ST;

#define MAX_ASM_OPERANDS 30
typedef struct 汇编操作数S {
    int id; /* GCC 3 optional identifier (0 if number only supported */
    char *constraint;
    char asm_str[16]; /* computed asm string for operand */
    栈值ST *vt; /* C value of the expression */
    int ref_index; /* if >= 0, gives reference to a output constraint */
    int input_index; /* if >= 0, gives reference to an input constraint */
    int priority; /* priority, used to assign registers */
    int reg; /* if >= 0, register number used for this operand */
    int is_llong; /* true if double register value */
    int is_memory; /* true if memory operand */
    int is_rw;     /* for '+' modifier */
} 汇编操作数ST;
#endif

/* 额外符号属性（不在符号表中*/
struct 额外符号属性S {
    unsigned got_偏移;
    unsigned plt_偏移;
    int plt_sym;
    int dyn_索引;
#ifdef 目标_ARM
    unsigned char plt_thumb_stub:1;
#endif
};

struct 虚拟机S {
    unsigned char 显示详情;
    unsigned char 不添加标准头;
    unsigned char 不添加标准库;
    unsigned char bss不使用通用符号; /* 如果为真,则.bss 数据不使用通用符号 */
    unsigned char 静态_链接;
    unsigned char 导出所有符号;
    unsigned char 当前模块的符号; /* 如果为true，则首先解析当前模块中的符号 */
    unsigned char 源文件类型; /* 编译文件类型 (NONE,C,ASM) */
    unsigned char optimize; /* 仅用于#define __OPTIMIZE__*/
    unsigned char option_pthread; /* -pthread option */
    unsigned char enable_new_dtags; /* -Wl,--enable-new-dtags */
    unsigned int  知语言版本; /* 支持的 C ISO 版本，199901（默认），201112，... */

    /* C 语言选项 */
    unsigned char 字符_是_无符号的;
    unsigned char 前导下划线;
    unsigned char ms_扩展; /* 允许没有标识符的嵌套命名结构表现得像未命名 */
    unsigned char 标识符中允许使用美元符;
    unsigned char 模拟MS算法对齐位域;

    /* 警告开关 */
    unsigned char 警告_无;
    unsigned char 警告_所有;
    unsigned char 警告_错误;    /* 异常类型 */
    unsigned char 警告_写_字符串;
    unsigned char 警告_不支持;
    unsigned char 隐式函数声明警告;
    unsigned char 警告丢弃限定符;
    #define 警告模式_开启  1 /* warning is on (-Woption) */
    unsigned char warn_num; /*zhi_警告_c() 的临时变量*/

    unsigned char 选项_r; /* option -r */
    unsigned char 统计_编译信息; /* option -bench */
    unsigned char 只是_依赖; /* option -M  */
    unsigned char 生成_依赖; /* option -MD  */
    unsigned char 包含的系统依赖; /* option -MD  */

    /* 使用调试符号编译（如果在执行过程中出错，则使用它们）*/
    unsigned char 做_调试;
    unsigned char 执行_回溯;
#ifdef 启用边界检查M
    /* 使用内置内存和边界检查器进行编译 */
    unsigned char 使用_边界_检查器;
#endif
    unsigned char 测试_覆盖率;  /* 生成测试覆盖率代码 */

    /* 使用 GNU C 扩展 */
    unsigned char GNU_C_扩展;
    /* 使用 zhi 扩展 */
    unsigned char zhi_扩展;

    unsigned char dflag; /* -dX value */
    unsigned char Pflag; /* -P switch (LINE_MACRO_OUTPUT_FORMAT) */

#ifdef 目标_X86_64
    unsigned char nosse; /* For -mno-sse support. */
#endif
#ifdef 目标_ARM
    unsigned char 浮点_abi; /* 生成代码的浮动 ABI*/
#endif

    unsigned char has_代码节地址;
    addr_t 代码节地址;
    unsigned 节_对齐;
#ifdef 目标_I386
    int 分割大小; /* 32.在i386汇编器中可以是16（.code16） */
#endif

    char *zhi的安装目录;
    char *soname; /* 在命令行中指定 (-soname) */
    char *rpath; /* 在命令行中指定 (-Wl,-rpath=) */

    char *init_symbol; /* 在加载时调用的符号（当前未使用）*/
    char *fini_symbol; /* 在卸载时调用的符号（当前未使用） */

    /* 输出类型，见 输出_XXX */
    int 输出类型;
    /* 输出格式, 见 输出_格式_xxx */
    int 输出格式;
    /* nth test to run with -dt -run */
    int 运行_测试;

    /* 所有加载的 dll 的数组（包括加载的 dll 引用的那些） */
    DLL引用ST **加载的DLL数组;
    int 加载的DLL数量;

    /* include paths */
    char **导入路径数组;
    int 导入路径数量;

    char **系统导入路径数组;
    int 系统导入路径数量;

    /* library paths */
    char **库_路径数组;
    int 库_路径_数量;

    /* crt?.o object path */
    char **目标文件路径数组;
    int 目标文件路径数量;

    /* -D / -U options */
    动态字符串ST 命令行_defines;
    /* -include options */
    动态字符串ST 命令行_include;

    /* error handling */
    void *错误_不透明;
    void (*错误_函数)(void *不透明, const char *msg);
    int 错误_设置_jmp_已启用; /*异常处理：设置跳转启用*/
    jmp_buf 错误_jmp_buf;
    int 错误_数量;

    /* 用于预处理的输出文件 (-E)*/
    FILE *标准的输出文件;

    /* 对于 -MD/-MF：为此编译收集的依赖项 */
    char **目标_依赖项数组;
    int 目标_依赖项数量;

    /* 汇编 */
    缓冲文件ST *include栈[INCLUDE_栈_大小];  /*缓冲文件数组*/
    缓冲文件ST **include栈指针;

    int ifdef_栈[IFDEF_栈_大小];
    int *ifdef_栈_ptr;

    /* 包含在#ifndef MACRO 中的文件 */
    int 包含在ifndef宏中的文件[CACHED_INCLUDES_HASH_SIZE];
    导入文件缓存ST **包含的缓存文件;
    int 包含的缓存文件数量;

    /* #pragma pack stack */
    int 包_栈[包栈大小M];
    int *包_栈_ptr;
    char **pragma_库数组;
    int pragma_库数量;

    /* 内联函数存储为单词列表，只有在被引用时才最后编译 */
    struct 内联函数S **内联函数数组;
    int 内联函数数量;

    /* 节数组 */
    节ST **节数组;
    int 节的数量; /* 节数，包括第一个虚拟节 */

    节ST **私有节数组;
    int 私有节的数量;

    /* got & plt 处理 */
    节ST *got;
    节ST *plt;

    /* 预定义节数组 */
    节ST *代码_节, *数据_节, *只读数据_节, *未初始化或初始化为0的数据_节;
    节ST *完全未初始化数据_节;
    节ST *当前代码节; /* 生成函数代码的当前节 */
#ifdef 启用边界检查M
    /* 绑定检查相关节 */
    节ST *全局边界_节; /* 包含全局数据绑定描述 */
    节ST *局部边界_节; /* 包含局部数据绑定描述 */
#endif
    节ST *测试节覆盖_数组;
    节ST *节符号表_数组;
    节ST *调试节_数组;
    int 新的未定义符号;/* 自上次 新的未定义符号() 以来是否有新的未定义符号 */
    节ST *临时动态符号表_节; /* 临时动态符号节（用于 dll 加载） */
    节ST *动态符号节; /* 导出动态符号节 */
    节ST *符号表节;/* copy of the global 节符号表_数组 variable */
    struct 额外符号属性S *符号属性表;/* extra attributes (eg. GOT/PLT value) for 符号表节 symbols */
    int 符号属性数量;
    ElfW_Rel *qrel;/* ptr to next 重定位 entry reused */
    #define qrel s1->qrel

#ifdef 目标_RISCV64
    struct pcrel_hi { addr_t addr, val; } last_hi;
    #define last_hi s1->last_hi
#endif

#ifdef 目标_PE
    /* PE info */
    int PE_子系统;
    unsigned PE特性;
    unsigned pe_文件_对齐;
    unsigned PE栈大小;
    addr_t pe_优先装载地址;
# ifdef 目标_X86_64
    节ST *uw_pdata;
    int uw_sym;
    unsigned uw_offs;
# endif
#endif

#ifndef ELF_OBJ_ONLY
    int 符号版本_数量;
    struct ELF符号版本S *符号_版本;
    int 符号数量_到_版本;
    int *符号_到_版本;
    int dt_版本需要节;
    节ST *符号版本_节;
    节ST *需要的版本_节;
#endif

#ifdef 运行脚本M
    const char *运行时_主函数;
    void **运行时_内存;
    int 运行时_内存_数量;
#endif

#ifdef 启用内置堆栈回溯M
    int 返回调用数量;
#endif

    int 总标识符数;
    int 总行数;
    int 总字节数;
    int 总输出[4];

    /* 选项 -dnum（用于一般开发目的） */
    int g_debug;

    /* used by 加载链接器脚本 */
    int 文件描述, cc;
    const char *当前源文件名;/* 用于目标文件的 警告/错误 */
    /* 仅用于 main 和解析_命令行参数 */
    struct 文件S **源文件列表; /* 在命令行上看到的文件 */
    int 文件_数量;
    int 库_数量;
    char *输出文件名; /* output 文件名 */
    char *指定_依赖; /* option -MF */
    int argc;
    char **argv;
};

struct 文件S {
    char 类型;
    char 名称[1];
};



enum 符号的存储类型E
{
存储类型_VC常量        =0x0030,  /* vc 中的常量（必须是第一个非寄存器值）*/
存储类型_LLOCAL       =0x0031,  /* 寄存器溢出存放栈中 */
存储类型_JMP          =0x0034,  /* value is the consequence of jmp true (even) */
存储类型_JMPI         =0x0035,  /* value is the consequence of jmp false (odd) */
存储类型_必须转换        =0x0C00,  /* 值必须被强制转换为正确的（用于存储在整数寄存器中的字符/short) */
存储类型_必须边界检查     =0x4000,  /* 在取消引用值之前必须进行边界检查 */
存储类型_有边界的        =0x8000,  /* 值是有界的。 包围函数调用点的地址在vc中 */
存储类型_无符号的        =0x0010,  /* 无符号类型 */
存储类型_明确有无符号      =0x0020,  /* 明确 有符号或无符号 */
存储类型_局部           =0x0032,  /* 栈偏移量（栈中变量） */
存储类型_标志寄存器       =0x0033,  /* 该值存储在处理器标志中（在 vc 中）。使用标志寄存器 */
存储类型_掩码           =0x003f,
存储类型_位域           =0x0080,  /* 位域修饰符 */
存储类型_左值           =0x0100,
存储类型_符号           =0x0200,
存储类型_变长数组        =0x0400,  /* VLA 类型 (also has 数据类型_指针 and 数据类型_数组) */
存储类型_常量           =0x0100,  /* 常量修饰符 */
存储类型_易变           =0x0200,  /* 易变修饰符 */
/* 贮存 */
存储类型_外部           =0x00001000,  /* 外部定义 */
存储类型_静态           =0x00002000,  /* 静态变量 */
存储类型_别名           =0x00004000,  /* 别名定义 */
存储类型_内联           =0x00008000,  /* 内联定义 */
};
/* 数据类型 类型ST->t的值*/
enum 符号的数据类型E
{
数据类型_基本类型       =0x000f , /* 基本类型掩码 */
数据类型_无类型        =0 , /* void 类型 */
数据类型_字节         = 1 , /* 有符号字节类型 */
数据类型_短整数        = 2 , /* short 类型 */
数据类型_整数         =3 , /* 整数类型 */
数据类型_长整数       =0x0800,  /* long 类型 (also has 数据类型_整数 rsp. 数据类型_长长整数) */
数据类型_长长整数      = 4 , /* 64 位整数 */
数据类型_指针         =  5,
数据类型_函数         =  6,
数据类型_结构体        = 7,  /* struct/union definition */
数据类型_数组         =0x0040,  /* 数组类型 (也可以是 数据类型_指针) */
数据类型_浮点         =  8 , /* IEEE float */
数据类型_双精度        =9,  /* IEEE double */
数据类型_长双精度       = 10,  /* IEEE long double */
数据类型_布尔         =11 , /* ISOC99 boolean 类型 */
数据类型_128位整数     =13 , /* 128 位整数。 仅用于 x86-64 ABI */
数据类型_128位浮点     =14  /* 128 位浮点数。 仅用于 x86-64 ABI */
};

#define 值的数据类型_结构体_SHIFT 20     /* 位域的移位的值(32 - 2*6) */
#define 值的数据类型_结构体_MASK (((1U << (6+6)) - 1) << 值的数据类型_结构体_SHIFT | 存储类型_位域)
#define BIT_POS(t) (((t) >> 值的数据类型_结构体_SHIFT) & 0x3f)
#define BIT_SIZE(t) (((t) >> (值的数据类型_结构体_SHIFT + 6)) & 0x3f)

#define 值的数据类型_共用体    (1 << 值的数据类型_结构体_SHIFT | 数据类型_结构体)
#define 值的数据类型_枚举     (2 << 值的数据类型_结构体_SHIFT) /* integral 类型 is an enum really */
#define 值的数据类型_枚举_VAL (3 << 值的数据类型_结构体_SHIFT) /* integral 类型 is an enum constant really */

#define 是_枚举(t) ((t & 值的数据类型_结构体_MASK) == 值的数据类型_枚举)
#define IS_ENUM_VAL(t) ((t & 值的数据类型_结构体_MASK) == 值的数据类型_枚举_VAL)
#define IS_UNION(t) ((t & (值的数据类型_结构体_MASK|数据类型_基本类型)) == 值的数据类型_共用体)

#define 值的数据类型_原子   存储类型_易变

/* 类型 mask (except 贮存) */
#define 值的数据类型_贮存 (存储类型_外部 | 存储类型_静态 | 存储类型_别名 | 存储类型_内联)
#define 值的数据类型_TYPE (~(值的数据类型_贮存|值的数据类型_结构体_MASK))

/* 符号首先由 asm.c 创建 */
#define 值的数据类型_ASM (数据类型_无类型 | 1 << 值的数据类型_结构体_SHIFT)
#define 值的数据类型_ASM_FUNC (值的数据类型_ASM | 2 << 值的数据类型_结构体_SHIFT)
#define IS_ASM_SYM(sym) (((sym)->类型.数据类型 & (数据类型_基本类型 | 值的数据类型_ASM)) == 值的数据类型_ASM)

/* 一般：设置/获取位掩码 M 的伪位域值 */
#define BFVAL(M,N) ((unsigned)((M) & ~((M) << 1)) * (N))
#define BFGET(X,M) (((X) & (M)) / BFVAL(M,1))
#define BFSET(X,M,N) ((X) = ((X) & ~(M)) | BFVAL(M,N))

/* 标记值 */

/* 条件操作 */
#define TOK_逻辑与  0x90
#define TOK_逻辑或   0x91
/* 警告：以下比较标记取决于 i386 汇编代码 */
#define TOK_ULT 0x92
#define TOK_UGE 0x93
#define TOK_等于  0x94
#define TOK_不等于  0x95
#define TOK_ULE 0x96
#define TOK_UGT 0x97
#define TOK_Nset 0x98
#define TOK_Nclear 0x99
#define TOK_小于  0x9c
#define TOK_大于等于  0x9d
#define TOK_小于等于  0x9e
#define TOK_大于  0x9f

#define TOK_ISCOND(t) (t >= TOK_逻辑与 && t <= TOK_大于)

#define TOK_自减     0x80 /* -- */
#define TOK_MID     0x81 /* inc/dec, to void constant */
#define TOK_自加     0x82 /* ++ */
#define TOK_UDIV    0x83 /* unsigned division */
#define TOK_UMOD    0x84 /* unsigned modulo */
#define TOK_PDIV    0x85 /* fast division with undefined rounding for pointers */
#define TOK_UMULL   0x86 /* unsigned 32x32 -> 64 mul */
#define TOK_ADDC1   0x87 /* add with carry generation */
#define TOK_ADDC2   0x88 /* add with carry use */
#define TOK_SUBC1   0x89 /* add with carry generation */
#define TOK_SUBC2   0x8a /* add with carry use */
#define TOK_左移     '<' /* shift left */
#define TOK_右移     '>' /* signed shift right */
#define TOK_SHR     0x8b /* unsigned shift right */
#define TOK_NEG     TOK_MID /* 一元减法运算 (for floats) */

#define TOK_箭头   0xa0 /* -> */
#define TOK_三个点    0xa1
#define TOK_两个点 0xa2 /* C++ token ? */
#define TOK_双井号 0xa3 /* 起到链接作用 */
#define TOK_占位符 0xa4 /* C99 中定义的占位符单词 */
#define TOK_不宏替换 0xa5 /* 表示以下单词已被 pp'd */
#define TOK_PPJOIN  0xa6 /* A '##' in the right position to mean pasting */

/* 赋值运算符 */
#define TOK_加后赋值   0xb0
#define TOK_减后赋值   0xb1
#define TOK_乘后赋值   0xb2
#define TOK_除后赋值   0xb3
#define TOK_取模后赋值   0xb4
#define TOK_按位与后赋值   0xb5
#define TOK_按位或后赋值    0xb6
#define TOK_按位异或后赋值   0xb7
#define TOK_左移后赋值   0xb8
#define TOK_右移后赋值   0xb9

#define TOK_分配(t) (t >= TOK_加后赋值 && t <= TOK_右移后赋值)
#define TOK_分配_OP(t) ("+-*/%&|^<>"[t - TOK_加后赋值])

/* 携带值的单词（在额外的单词字符串空间/单词常量中） --> */
#define TOK_字符常量   0xc0
#define TOK_LCHAR   0xc1
#define TOK_整数常量    0xc2
#define TOK_无符号整数常量   0xc3
#define TOK_长长整数常量  0xc4
#define TOK_无符号长长整数常量 0xc5
#define TOK_长整数常量   0xc6
#define TOK_无符号长整数常量  0xc7
#define TOK_字符串常量     0xc8
#define TOK_LSTR    0xc9
#define TOK_浮点常量  0xca
#define TOK_双精度常量 0xcb
#define TOK_长双精度常量 0xcc
#define TOK_预处理数字   0xcd
#define TOK_预处理字符串   0xce
#define TOK_行号 0xcf

#define TOK_哈希_值(t) (t >= TOK_字符常量 && t <= TOK_行号)

#define TOK_文件结尾       (-1)
#define 英_换行  10

#define TOK_标识符 256      /*关键词编码在此之后*/

enum 关键词编码E {
    TOK_LAST = TOK_标识符 - 1
#define 系统(id, 字符串) ,id
#include "单词.h"
};

/* keywords: 单词编码 >= TOK_标识符 && 单词编码 < TOK_UIDENT */
#define TOK_UIDENT 英_定义     /*也可以使用：中_定义*/

/*标点符号,为以后中英文标点符号管理做准备*/
/*英文标点符号*/
#define 英_分号     ';'
#define 英_逗号     ','
#define 英_左小括号  '('
#define 英_右小括号  ')'
#define 英_左中括号  '['
#define 英_右中括号  ']'
#define 英_左大括号  '{'
#define 英_右大括号  '}'
#define 英_冒号     ':'
#define 英_问号     '?'
#define 英_叹号     '!'

/* ------------ 核心.c ------------ */
静态数据 虚拟机ST *全局虚拟机;

/* zhi主函数当前使用的公共函数 */
静态函数 char *截取前n个字符(char *buf, size_t buf_size, const char *s);
静态函数 char *截取后拼接(char *buf, size_t buf_size, const char *s);
静态函数 char *复制指定字节数字符串(char *out, const char *in, size_t num);
公用函数 char *取文件名含扩展名(const char *name);
公用函数 char *取文件扩展名 (const char *name);

#ifndef 内存_调试
公用函数 void zhi_释放(void *ptr);
公用函数 void *zhi_分配内存(unsigned long 大小);
公用函数 void *初始化内存(unsigned long 大小);
公用函数 void *zhi_重新分配(void *ptr, unsigned long 大小);
公用函数 char *字符串增加一个宽度(const char *字符串);
#else
#define zhi_释放(ptr)           zhi_free_debug(ptr)
#define zhi_分配内存(大小)        zhi_malloc_debug(大小, __FILE__, __LINE__)
#define 初始化内存(大小)       zhi_mallocz_debug(大小, __FILE__, __LINE__)
#define zhi_重新分配(ptr,大小)   zhi_realloc_debug(ptr, 大小, __FILE__, __LINE__)
#define 字符串增加一个宽度(字符串)         zhi_strdup_debug(字符串, __FILE__, __LINE__)
公用函数 void zhi_free_debug(void *ptr);
公用函数 void *zhi_malloc_debug(unsigned long 大小, const char *文件, int line);
公用函数 void *zhi_mallocz_debug(unsigned long 大小, const char *文件, int line);
公用函数 void *zhi_realloc_debug(void *ptr, unsigned long 大小, const char *文件, int line);
公用函数 char *zhi_strdup_debug(const char *字符串, const char *文件, int line);
#endif

#define free(p) use_zhi_free(p)
#define malloc(s) use_zhi_malloc(s)
#define realloc(p, s) use_zhi_realloc(p, s)
#undef strdup
#define strdup(s) use_zhi_strdup(s)
公用函数 void _zhi_错误_不终止(const char *fmt, ...) PRINTF_LIKE(1,2);
公用函数 NORETURN void _zhi_错误(const char *fmt, ...) PRINTF_LIKE(1,2);
公用函数 void _zhi_警告(const char *fmt, ...) PRINTF_LIKE(1,2);
#define zhi_internal_error(msg) 错_误("内部编译错误\n"\
        "%s:%d: in %s(): " msg, __FILE__,__LINE__,__FUNCTION__)

/* other utilities */

静态函数 void 动态数组_追加元素(void *ptab, int *nb_ptr, void *数据);
静态函数 void 动态数组_重置(void *pp, int *n);

static void 动态字符串_重新分配(动态字符串ST *s, int 新大小);
ST_INLN void 动态字符串_追加一个字符(动态字符串ST *cstr, int 当前字符);
ST_INLN void 动态字符串_添加一个扩展为utf8的unicode字符(动态字符串ST *s, int 当前字符);
静态函数 void 动态字符串_拼接(动态字符串ST *cstr, const char *字符串, int 长度);
静态函数 void 动态字符串_追加一个宽字符(动态字符串ST *cstr, int 当前字符);
静态函数 void 动态字符串_新建(动态字符串ST *cstr);
静态函数 void 动态字符串_释放(动态字符串ST *cstr);
静态函数 int 动态字符串_printf(动态字符串ST *cs, const char *fmt, ...) PRINTF_LIKE(2,3);
静态函数 int 动态字符串_vprintf(动态字符串ST *cstr, const char *fmt, va_list ap);
静态函数 void 动态字符串_重置(动态字符串ST *cstr);
static void 动态字符串_增加字符(动态字符串ST *s, int c);

静态函数 void 新建源文件缓冲区(虚拟机ST *s1, const char *文件名, int 初始化长度);
静态函数 int zhi_打开一个新文件(虚拟机ST *s1, const char *文件名);
静态函数 void 关闭打开的文件(void);

静态函数 int 打开文件并进入编译(虚拟机ST *s1, const char *文件名, int flags);
/* flags: */
#define 标志_打印_错误         0x10 /* 如果找不到文件则打印错误 */
#define 标志_被引用_DLL        0x20 /* 从另一个 dll 加载引用的 dll */
#define 标志类型_BIN         0x40 /* 要添加的文件是二进制的 */
#define 标志_从存档加载所有对象   0x80 /* 从存档加载所有对象 */
/* s->源文件类型: */
#define 源文件类型_NONE   0
#define 源文件类型_Z      1
#define 源文件类型_仅汇编    2
#define 源文件类型_汇编预处理  4
#define 源文件类型_LIB    8
#define 源文件类型_MASK   (15 | 标志类型_BIN)
/* 从 取目标文件类型(...) 获取值*/
#define 目标文件_类型_REL 1
#define 目标文件_类型_DYN 2
#define 目标文件_类型_AR  3
#define 目标文件_类型_C67 4

#ifndef ELF_OBJ_ONLY
静态函数 int zhi_add_crt(虚拟机ST *s, const char *文件名);
#endif
#ifndef 目标_MACHO
静态函数 int zhi_添加_dll(虚拟机ST *s, const char *文件名, int flags);
#endif
#ifdef 启用边界检查M
静态函数 void 添加_边界检查(虚拟机ST *s1);
#endif
#ifdef 启用内置堆栈回溯M
静态函数 void zhi_add_btstub(虚拟机ST *s1);
#endif
静态函数 void 加载静态链接库(虚拟机ST *s1);
公用函数 int 添加库_有错误提示(虚拟机ST *s, const char *f);
公用函数 void 打印编译信息(虚拟机ST *s, unsigned 总时间);
公用函数 int 解析命令行参数(虚拟机ST *s, int *argc, char ***argv, int optind);
#ifdef _WIN32
静态函数 char *斜杠规范(char *path);
#endif
静态函数 DLL引用ST *添加DLL引用(虚拟机ST *s1, const char *dllname);
静态函数 char *zhi_load_text(int 文件描述);

/* 解析命令行参数返回代码: */
#define 选项返回_HELP 1
#define 选项返回_ABOUT 2
#define 选项返回_V 3
#define 选项返回_打印目录 4
#define 选项返回_AR 5
#define 选项返回_IMPDEF 6
#define 选项返回_M32 32
#define 选项返回_M64 64

/* ------------ 词法分析.c ------------ */

静态数据 struct 缓冲文件S *文件;
静态数据 int 当前字符, 单词编码;
静态数据 常量值U 单词常量;
静态数据 const int *没宏替换的字符串指针;
静态数据 int 解析标志;
静态数据 int 单词标志;
静态数据 动态字符串ST 当前解析的字符串;

静态数据 int 单词_标识符;
静态数据 单词ST **标识符表;

#define 单词标志_行开始之前   0x0001
#define 单词标志_文件开始之前   0x0002
#define 单词标志_ENDIF 0x0004 /* a endif was found matching starting #ifdef */
#define 单词标志_文件结尾   0x0008 /* end of 文件 */

#define 解析标志_预处理 0x0001 /* 激活预处理 */
#define 解析标志_数字    0x0002 /* 返回数字而不是 TOK_预处理数字 */
#define 解析标志_换行   0x0004 /* 换行作为标记返回。 换行也在 eof 返回 */
#define 解析标志_汇编文件 0x0008 /* we processing an asm 文件: '#' can be used for line comment, etc. */
#define 解析标志_空格     0x0010 /* 取下个单词() returns space tokens (for -E) */
#define 解析标志_转义 0x0020 /* 取下个单词() returns '\\' token */
#define 解析标志_单词字符串    0x0040 /* return parsed strings instead of TOK_预处理字符串 */

/* 标识符数字表 flags: */
#define 是_空格 1
#define 是_标识符  2
#define 是_数字 4

enum 行输出格式E {
    行_格式_GCC,
    行_格式_NONE,
    行_格式_STD,
    行_格式_P10 = 11
};

静态函数 单词ST *插入单词(const char *字符串, int 长度);
静态函数 int 标记分配常量(const char *字符串);
静态函数 const char *取单词字符串(int v, 常量值U *cv);
静态函数 void 开始_宏扩展(单词字符串ST *字符串, int alloc);
静态函数 void 结束_宏扩展(void);
静态函数 int 设置标识符(int c, int val);
ST_INLN void 单词字符串_新建(单词字符串ST *s);
静态函数 单词字符串ST *单词字符串_分配(void);
静态函数 void 单词字符串_释放(单词字符串ST *s);
静态函数 void 单词字符串_释放_字符串(int *字符串);
静态函数 void 单词字符串_增加(单词字符串ST *s, int t);
静态函数 void 在单词字符串s中添加当前解析单词(单词字符串ST *s);
ST_INLN void 推进define(int v, int macro_type, int *字符串, 符号ST *first_arg);
静态函数 void define_undef(符号ST *s);
ST_INLN 符号ST *查找define(int v);
静态函数 void 释放define(符号ST *b);
静态函数 符号ST *查找标签(int v);
静态函数 符号ST *标签推送(符号ST **ptop, int v, int flags);
静态函数 void 弹出标签(符号ST **ptop, 符号ST *slast, int keep);
静态函数 void 解析_define(void);
静态函数 void 预处理(int 是_文件开头);
静态函数 void 取下个单词(void);
ST_INLN void 退回当前单词(int last_tok);
静态函数 void 词法分析初始化(虚拟机ST *s1, int 源文件类型);
静态函数 void 预处理_结束(虚拟机ST *s1);
静态函数 void zhipp_delete(虚拟机ST *s);
静态函数 int zhi_预处理(虚拟机ST *s1);
静态函数 void 跳过(int c);
静态函数 NORETURN void 应为(const char *msg);

/* 不包括换行符的空格 */
static inline int 是空格(int 当前字符) {
    return 当前字符 == ' ' || 当前字符 == '\t' || 当前字符 == '\v' || 当前字符 == '\f' || 当前字符 == '\r';
}
static inline int 是标识符(int c) {
    return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '_';
}
static inline int 是数字(int c) {
    return c >= '0' && c <= '9';
}
static inline int isoct(int c) {
    return c >= '0' && c <= '7';
}
static inline int toup(int c) {
    return (c >= 'a' && c <= 'z') ? c - 'a' + 'A' : c;
}

/* ------------ 语法分析.c ------------ */

#define SYM_POOL_NB (8192 / sizeof(符号ST))

静态数据 符号ST *全局符号栈;
静态数据 符号ST *局部符号栈;
静态数据 符号ST *局部_标签_栈;
静态数据 符号ST *全局_标签_栈;
静态数据 符号ST *define_栈;
静态数据 类型ST 整数_类型, 函数_旧_类型, 字符_指针_类型;
静态数据 栈值ST *栈顶值;
静态数据 int 返回符号, 匿名符号索引, 指令在代码节位置, 局部变量索引;
静态数据 char 调试_模式;

静态数据 int 需要_常量; /* 如果需要常量，则为true */
静态数据 int 无需生成代码; /* 如果表达式不需要代码生成，则为true */
静态数据 int 全局_表达式;  /* 如果必须全局分配复合文字（在初始化程序解析期间使用），则为true */
静态数据 类型ST 函数_返回类型; /* 当前函数返回类型（由返回指令使用） */
静态数据 int 函数_可变参; /* 如果当前函数的参数是可变的，则为true */
静态数据 int func_vc;
静态数据 const char *函数名;

静态函数 void 翻译单元信息开始(虚拟机ST *s1);
静态函数 void 翻译单元信息结束(虚拟机ST *s1);
静态函数 void zhi_调试_include的开始(虚拟机ST *s1);
静态函数 void 添加包含文件调试信息的结束(虚拟机ST *s1);
静态函数 void zhi_debug_putfile(虚拟机ST *s1, const char *文件名);

静态函数 void 语法分析初始化(虚拟机ST *s1);
静态函数 int 语法分析开始(虚拟机ST *s1);
静态函数 void 语法分析_完成(虚拟机ST *s1);
静态函数 void 检查栈值(void);

ST_INLN int 是_浮点(int t);
静态函数 int ieee_finite(double d);
静态函数 int 精确_2幂加1(int i);
静态函数 void 测试_左值(void);

静态函数 ElfSym *elfsym(符号ST *);
静态函数 void update_storage(符号ST *sym);
静态函数 void 取外部符号2(符号ST *sym, int ELF节数量, addr_t value, unsigned long 大小, int can_add_underscore);
静态函数 void 取外部符号(符号ST *sym, 节ST *section, addr_t value, unsigned long 大小);
#if 宏_指针大小 == 4
静态函数 void greloc(节ST *s, 符号ST *sym, unsigned long offset, int 类型);
#endif
静态函数 void 添加新的重定位条目(节ST *s, 符号ST *sym, unsigned long offset, int 类型, addr_t addend);

ST_INLN void sym_free(符号ST *sym);
静态函数 符号ST *将符号放入符号栈(int v, 类型ST *类型, int r, int c);
静态函数 void 符号弹出(符号ST **ptop, 符号ST *b, int keep);
静态函数 符号ST *将符号放入符号栈2(符号ST **ps, int v, int t, int c);
静态函数 符号ST *查找符号并返回去关联结构(符号ST *s, int v);
ST_INLN 符号ST *查找标识符(int v);
ST_INLN 符号ST *结构体符号_查找(int v);
ST_INLN 符号ST *类符号_查找(int v);

静态函数 符号ST *将符号放入全局符号表(int v, int t, int c);
静态函数 符号ST *外部_全局_符号(int v, 类型ST *类型);
静态函数 符号ST *外部_辅助_符号(int v);
静态函数 void 推送_辅助_函数引用(int v);
静态函数 void 值压入栈(类型ST *类型, int r, int v);
静态函数 void vset_VT_CMP(int op);
静态函数 void 整数常量压入栈(int v);
静态函数 void 值压入栈值(栈值ST *v);
静态函数 void 类型符号值压入栈(类型ST *类型, 符号ST *sym);
静态函数 void vswap(void);
静态函数 void vrote(栈值ST *e, int n);
静态函数 void vrott(int n);
静态函数 void vrotb(int n);
静态函数 void 弹出堆栈值(void);
#if 宏_指针大小 == 4
静态函数 void 堆栈64位扩展为2个整数(void);
#endif
#ifdef 目标_ARM
静态函数 int get_reg_ex(int rc, int rc2);
#endif
静态函数 void save_reg(int r);
静态函数 void save_reg_upstack(int r, int n);
静态函数 int get_reg(int rc);
静态函数 void save_regs(int n);
静态函数 void gaddrof(void);
静态函数 int 生成值(int rc);
静态函数 void 生成值2(int rc1, int rc2);
静态函数 void gen_op(int op);
静态函数 int 类型_大小(类型ST *类型, int *a);
静态函数 void 生成指针类型(类型ST *类型);
静态函数 void 存储栈顶值(void);
静态函数 void inc(int post, int c);
静态函数 void 解析多个字符串 (动态字符串ST *astr, const char *msg);
静态函数 void 解析_汇编_字符串(动态字符串ST *astr);
静态函数 void 间接(void);
静态函数 void 一元(void);
静态函数 void 生成表达式(void);
静态函数 int 常量表达式解析(void);
#if defined 启用边界检查M || defined 目标_C67
静态函数 符号ST *取指向某个节的静态符号(类型ST *类型, 节ST *sec, unsigned long offset, unsigned long 大小);
#endif
#if defined 目标_X86_64 && !defined 目标_PE
静态函数 int classify_x86_64_va_arg(类型ST *ty);
#endif
#ifdef 启用边界检查M
静态函数 void 生成函数参数_边界(int nb_args);
静态数据 int 函数_边界_添加_结语;
#endif

/* ------------ Elf.c ------------ */

#define 输出格式_ELF    0 /* 默认输出格式: ELF */
#define 输出格式_BINARY 1 /* binary image output */
#define 输出格式_COFF   2 /* COFF */

#define ARMAG  "!<arch>\012"    /* For COFF and a.out archives */

typedef struct
{
    unsigned int 字符串_索引;         /* 索引到名称的字符串表中 */
    unsigned char 符号_类型;
    unsigned char 其他_信息;        /*杂项信息（通常为空） */
    unsigned short 描述_字段;
    unsigned int 符号_值;
} 字符串表符号ST;

静态函数 void 新建数据代码符号表节(虚拟机ST *s);
静态函数 void elf删除(虚拟机ST *s);
静态函数 void elf符号表新建(虚拟机ST *s);
静态函数 void 设置节数据状态(虚拟机ST *s1);
静态函数 void elf结束文件(虚拟机ST *s1);
#ifdef 启用边界检查M
静态函数 void elf边界新建(虚拟机ST *s);
#endif
静态函数 节ST *节_新建(虚拟机ST *s1, const char *name, int sh_type, int sh_flags);
静态函数 void 节_重新分配内存且内容为0(节ST *sec, unsigned long new_size);
静态函数 size_t 节_预留指定大小内存且按照align对齐(节ST *sec, addr_t 大小, int 对齐);
静态函数 void *节_预留指定大小内存(节ST *sec, addr_t 大小);
static 节ST *查找_节_创建 (虚拟机ST *s1, const char *name, int create);
静态函数 节ST *新建符号表节(虚拟机ST *s1, const char *符号表名称, int sh_type, int sh_flags, const char *strtab_name, const char *hash_name, int hash_sh_flags);

静态函数 int 放置elf字符串(节ST *s, const char *sym);
静态函数 int 取符号索引(节ST *s, addr_t value, unsigned long 大小, int info, int other, int shndx, const char *name);
静态函数 int 设置elf符号(节ST *s, addr_t value, unsigned long 大小, int info, int other, int shndx, const char *name);
静态函数 int 查找elf符号(节ST *s, const char *name);
静态函数 void elf重定位(节ST *符号表节, 节ST *s, unsigned long offset, int 类型, int symbol);
静态函数 void 添加新的elf重定位信息(节ST *符号表节, 节ST *s, unsigned long offset, int 类型, int symbol, addr_t addend);

静态函数 void 放置符号表调试信息(虚拟机ST *s1, const char *字符串, int 类型, int other, int desc, unsigned long value);
静态函数 void 放置_符号表调试_重定位(虚拟机ST *s1, const char *字符串, int 类型, int other, int desc, unsigned long value, 节ST *sec, int 符号_索引);
静态函数 void put_stabn(虚拟机ST *s1, int 类型, int other, int desc, int value);

静态函数 void 解析_公共_符号(虚拟机ST *s1);
静态函数 void 重定位符号地址(虚拟机ST *s1, 节ST *符号表节, int do_resolve);
静态函数 void 重定位_所有节(虚拟机ST *s1);

静态函数 ssize_t 全读(int 文件描述, void *buf, size_t count);
静态函数 void *load_data(int 文件描述, unsigned long file_offset, unsigned long 大小);
静态函数 int 取目标文件类型(int 文件描述, ElfW(Ehdr) *h);
静态函数 int 加载目标文件(虚拟机ST *s1, int 文件描述, unsigned long file_offset);
静态函数 int 加载存档文件(虚拟机ST *s1, int 文件描述, int alacarte);
静态函数 void add_array(虚拟机ST *s1, const char *sec, int c);

#if !defined(ELF_OBJ_ONLY) || (defined(目标_MACHO) && defined 运行脚本M)
静态函数 void build_got_entries(虚拟机ST *s1);
#endif
静态函数 struct 额外符号属性S *获取符号属性(虚拟机ST *s1, int index, int alloc);
静态函数 addr_t 获取符号地址(虚拟机ST *s, const char *name, int err, int forc);
静态函数 void 列出elf符号名称和值(虚拟机ST *s, void *ctx,
    void (*symbol_cb)(void *ctx, const char *name, const void *val));
静态函数 int 设置全局符号(虚拟机ST *s1, const char *name, 节ST *sec, addr_t offs);

/* Browse each elem of 类型 <类型> in section <sec> starting at elem <startoff>
   using variable <elem> */
#define for_each_elem(sec, startoff, elem, 类型) \
    for (elem = (类型 *) sec->数据 + startoff; \
         elem < (类型 *) (sec->数据 + sec->当前数据_偏移量); elem++)

#ifndef ELF_OBJ_ONLY
静态函数 int zhi_load_dll(虚拟机ST *s1, int 文件描述, const char *文件名, int level);
静态函数 int 加载链接器脚本(虚拟机ST *s1, int 文件描述);
#endif
#ifndef 目标_PE
静态函数 void zhi_添加运行时库(虚拟机ST *s1);
#endif

/* ------------ xxx-link.c ------------ */

/* 是否生成 GOT/PLT 条目以及何时生成。 NO_GOTPLT_ENTRY 是第一个，以便未知重定位不会创建 GOT 或 PLT 条目 */
enum gotplt_entry {
    NO_GOTPLT_ENTRY,	/* never generate (eg. GLOB_DAT & JMP_SLOT relocs) */
    BUILD_GOT_ONLY,	/* only build GOT (eg. TPOFF relocs) */
    AUTO_GOTPLT_ENTRY,	/* generate if sym is UNDEF */
    ALWAYS_GOTPLT_ENTRY	/* always generate (eg. PLTOFF relocs) */
};

#if !defined(ELF_OBJ_ONLY) || defined(目标_MACHO)
静态函数 int code_reloc (int reloc_type);
静态函数 int gotplt_entry_type (int reloc_type);
#if !defined(目标_MACHO) || defined 运行脚本M
静态函数 unsigned create_plt_entry(虚拟机ST *s1, unsigned got_偏移, struct 额外符号属性S *attr);
静态函数 void relocate_plt(虚拟机ST *s1);
#endif
#endif
静态函数 void 重新定位(虚拟机ST *s1, ElfW_Rel *rel, int 类型, unsigned char *ptr, addr_t addr, addr_t val);

/* ------------ xxx-gen.c ------------ */
静态数据 const char * const target_machine_defs;
静态数据 const int reg_classes[NB_REGS];

静态函数 void 生成符号_地址(int t, int a);
静态函数 void 生成符号(int t);
静态函数 void load(int r, 栈值ST *sv);
静态函数 void store(int r, 栈值ST *v);
静态函数 int gfunc_sret(类型ST *vt, int variadic, 类型ST *ret, int *对齐, int *regsize);
静态函数 void g函数_调用(int nb_args);
静态函数 void 生成函数开头代码(符号ST *函数符号);
静态函数 void 生成函数结尾代码(void);
静态函数 void gen_fill_nops(int);
静态函数 int 生成到标签的跳转(int t);
静态函数 void 生成跳转_到固定地址(int a);
静态函数 int 生成条件跳转(int op, int t);
静态函数 int 生成跳转_append(int n, int t);
静态函数 void gen_opi(int op);
静态函数 void gen_opf(int op);
静态函数 void gen_cvt_ftoi(int t);
静态函数 void gen_cvt_itof(int t);
静态函数 void gen_cvt_ftof(int t);
静态函数 void ggoto(void);
#ifndef 目标_C67
静态函数 void o(unsigned int c);
#endif
静态函数 void gen_vla_sp_save(int addr);
静态函数 void gen_vla_sp_恢复(int addr);
静态函数 void gen_vla_alloc(类型ST *类型, int 对齐);

static inline uint16_t read16le(unsigned char *p) {
    return p[0] | (uint16_t)p[1] << 8;
}
static inline void write16le(unsigned char *p, uint16_t x) {
    p[0] = x & 255;  p[1] = x >> 8 & 255;
}
static inline uint32_t read32le(unsigned char *p) {
  return read16le(p) | (uint32_t)read16le(p + 2) << 16;
}
static inline void write32le(unsigned char *p, uint32_t x) {
    write16le(p, x);  write16le(p + 2, x >> 16);
}
static inline void add32le(unsigned char *p, int32_t x) {
    write32le(p, read32le(p) + x);
}
static inline uint64_t read64le(unsigned char *p) {
  return read32le(p) | (uint64_t)read32le(p + 4) << 32;
}
static inline void write64le(unsigned char *p, uint64_t x) {
    write32le(p, x);  write32le(p + 4, x >> 32);
}
static inline void add64le(unsigned char *p, int64_t x) {
    write64le(p, read64le(p) + x);
}

/* ------------ i386-生成.c ------------ */
#if defined 目标_I386 || defined 目标_X86_64 || defined 目标_ARM
静态函数 void g(int c);
静态函数 void gen_le16(int c);
静态函数 void gen_le32(int c);
#endif
#if defined 目标_I386 || defined 目标_X86_64
静态函数 void gen_addr32(int r, 符号ST *sym, int c);
静态函数 void gen_addrpc32(int r, 符号ST *sym, int c);
静态函数 void gen_cvt_csti(int t);
静态函数 void gen_increment_tcov (栈值ST *sv);
#endif

/* ------------ x86_64-生成.c ------------ */
#ifdef 目标_X86_64
静态函数 void gen_addr64(int r, 符号ST *sym, int64_t c);
静态函数 void 生成长长整数操作(int op);
#ifdef 目标_PE
静态函数 void gen_vla_result(int addr);
#endif
静态函数 void gen_cvt_sxtw(void);
静态函数 void gen_cvt_csti(int t);
#endif

/* ------------ arm-生成.c ------------ */
#ifdef 目标_ARM
#if defined(ZHI_ARM_EABI) && !defined(ELF解释器M)
公用函数 const char *默认_elf解释器(虚拟机ST *s);
#endif
静态函数 void arm_init(虚拟机ST *s);
静态函数 void gen_increment_tcov (栈值ST *sv);
#endif

/* ------------ arm64-生成.c ------------ */
#ifdef 目标_ARM64
静态函数 void 生成长长整数操作(int op);
静态函数 void 生成函数_返回(类型ST *函数_类型);
静态函数 void gen_va_start(void);
静态函数 void gen_va_arg(类型ST *t);
静态函数 void gen_clear_cache(void);
静态函数 void gen_cvt_sxtw(void);
静态函数 void gen_cvt_csti(int t);
静态函数 void gen_increment_tcov (栈值ST *sv);
#endif

/* ------------ riscv64-生成.c ------------ */
#ifdef 目标_RISCV64
静态函数 void 生成长长整数操作(int op);
//静态函数 void 生成函数_返回(类型ST *函数_类型);
静态函数 void gen_va_start(void);
静态函数 void arch_transfer_ret_regs(int);
静态函数 void gen_cvt_sxtw(void);
静态函数 void gen_increment_tcov (栈值ST *sv);
#endif

/* ------------ c67-生成.c ------------ */
#ifdef 目标_C67
#endif

/* ------------ Coff.c ------------ */

#ifdef 目标_COFF
静态函数 int 输出coff文件(虚拟机ST *s1, FILE *f);
静态函数 int 加载coff文件(虚拟机ST * s1, int 文件描述);
#endif

/* ------------ 汇编.c ------------ */
静态函数 void asm_instr(void);
静态函数 void asm_全局_instr(void);
静态函数 int 汇编当前文件(虚拟机ST *s1, int 执行_预处理);
#ifdef CONFIG_ZHI_ASM
静态函数 int find_constraint(汇编操作数ST *operands, int nb_operands, const char *name, const char **pp);
静态函数 符号ST* get_asm_sym(int name, 符号ST *csym);
静态函数 void asm_expr(虚拟机ST *s1, 表达式值ST *pe);
静态函数 int asm_int_expr(虚拟机ST *s1);
/* ------------ i386-汇编.c ------------ */
静态函数 void gen_expr32(表达式值ST *pe);
#ifdef 目标_X86_64
静态函数 void gen_expr64(表达式值ST *pe);
#endif
静态函数 void asm_opcode(虚拟机ST *s1, int opcode);
静态函数 int asm_parse_regvar(int t);
静态函数 void asm_计算约束(汇编操作数ST *operands, int nb_operands, int nb_outputs, const uint8_t *clobber_regs, int *pout_reg);
静态函数 void subst_asm_operand(动态字符串ST *add_str, 栈值ST *sv, int modifier);
静态函数 void asm_gen_code(汇编操作数ST *operands, int nb_operands, int nb_outputs, int is_output, uint8_t *clobber_regs, int out_reg);
静态函数 void asm_clobber(uint8_t *clobber_regs, const char *字符串);
#endif

/* ------------ Pe.c -------------- */
#ifdef 目标_PE
静态函数 int pe_加载_文件(虚拟机ST *s1, int 文件描述, const char *文件名);
静态函数 int 生成PE文件(虚拟机ST * s1, const char *文件名);
静态函数 int pe_putimport(虚拟机ST *s1, int dllindex, const char *name, addr_t value);
#if defined 目标_I386 || defined 目标_X86_64
静态函数 栈值ST *pe_getimport(栈值ST *sv, 栈值ST *v2);
#endif
#ifdef 目标_X86_64
静态函数 void pe_添加_展开_数据(unsigned start, unsigned end, unsigned stack);
#endif
公用函数 int zhi_获取_dll导出(const char *文件名, char **pp);
/* symbol properties stored in Elf32_Sym->st_other */
# define ST_PE_EXPORT 0x10
# define ST_PE_IMPORT 0x20
# define ST_PE_STDCALL 0x40
#endif
#define ST_ASM_SET 0x04

/* ------------ Macho.c ----------------- */
#ifdef 目标_MACHO
静态函数 int 生成MACHO文件(虚拟机ST * s1, const char *文件名);
静态函数 int macho_load_dll(虚拟机ST *s1, int 文件描述, const char *文件名, int lev);
静态函数 int macho_load_tbd(虚拟机ST *s1, int 文件描述, const char *文件名, int lev);
#ifdef 运行脚本M
静态函数 void zhi_add_macos_sdkpath(虚拟机ST* s);
静态函数 const char* macho_tbd_soname(const char* 文件名);
#endif
#endif
/* ------------ 运行.c ----------------- */
#ifdef 运行脚本M
#ifdef 配置_ZHI_静态
#define RTLD_LAZY       0x001
#define RTLD_NOW        0x002
#define RTLD_GLOBAL     0x100
#define RTLD_DEFAULT    NULL
/* dummy function for profiling */
静态函数 void *dlopen(const char *文件名, int flag);
静态函数 void dlclose(void *p);
静态函数 const char *dlerror(void);
静态函数 void *dlsym(void *handle, const char *symbol);
#endif
静态函数 void zhi_run_free(虚拟机ST *s1);
#endif

/********************************************************/
#if 配置_信号量_锁
#if defined _WIN32
typedef struct { int init; CRITICAL_SECTION cr; } ZHI信号量S;  /*CRITICAL_SECTION:同步不同线程的代码段*/
#elif defined __APPLE__
#include <dispatch/dispatch.h>
typedef struct { int init; dispatch_semaphore_t sem; } ZHI信号量S;
#else
#include <semaphore.h>
typedef struct { int init; sem_t sem; } ZHI信号量S;
#endif
静态函数 void 等待_信号量(ZHI信号量S *p);
静态函数 void 发出_信号量(ZHI信号量S *p);
#define ZHI_信号量(s) ZHI信号量S s
#define WAIT_SEM 等待_信号量
#define POST_SEM 发出_信号量
#else
#define ZHI_信号量(s)
#define WAIT_SEM(p)
#define POST_SEM(p)
#endif

/********************************************************/
#undef 静态数据
#if 是源码M
#  define 静态数据 static
#else
#  define 静态数据
#endif
/********************************************************/

#define 代码_节        ZHI_虚拟机ST_变量(代码_节)
#define 数据_节        ZHI_虚拟机ST_变量(数据_节)
#define 只读数据_节      ZHI_虚拟机ST_变量(只读数据_节)
#define 未初始化或初始化为0的数据_节         ZHI_虚拟机ST_变量(未初始化或初始化为0的数据_节)
#define 完全未初始化数据_节      ZHI_虚拟机ST_变量(完全未初始化数据_节)
#define 当前代码节    ZHI_虚拟机ST_变量(当前代码节)
#define 全局边界_节      ZHI_虚拟机ST_变量(全局边界_节)
#define 局部边界_节     ZHI_虚拟机ST_变量(局部边界_节)
#define 测试节覆盖_数组        ZHI_虚拟机ST_变量(测试节覆盖_数组)
#define 节符号表_数组      ZHI_虚拟机ST_变量(节符号表_数组)
#define 调试节_数组        ZHI_虚拟机ST_变量(调试节_数组)
#define stabstr_section     调试节_数组->link
#define GNU_C_扩展             ZHI_虚拟机ST_变量(GNU_C_扩展)
#define 错误_不中止   ZHI_设置_虚拟机ST(_zhi_错误_不终止)
#define 错_误           ZHI_设置_虚拟机ST(_zhi_错误)
#define zhi_警告         ZHI_设置_虚拟机ST(_zhi_警告)

#define 总标识符数        ZHI_虚拟机ST_变量(总标识符数)
#define 总行数         ZHI_虚拟机ST_变量(总行数)
#define 总字节数         ZHI_虚拟机ST_变量(总字节数)

公用函数 void 进入编译状态(虚拟机ST *s1);
公用函数 void 退出_虚拟机ST(虚拟机ST *s1);

/* 取决于switch的条件警告 */
#define zhi_警告_c(sw) ZHI_设置_虚拟机ST((\
    全局虚拟机->warn_num = offsetof(虚拟机ST, sw) \
    - offsetof(虚拟机ST, 警告_无), _zhi_警告))

/********************************************************/
#endif /* ZHI_H 结束*/

#undef ZHI_虚拟机ST_变量
#undef ZHI_设置_虚拟机ST

#ifdef 全局使用
# define ZHI_虚拟机ST_变量(sym) 全局虚拟机->sym
# define ZHI_设置_虚拟机ST(fn) fn
# undef 全局使用
#else
# define ZHI_虚拟机ST_变量(sym) s1->sym
# define ZHI_设置_虚拟机ST(fn) (英_可变参数,fn)
/* 实际上，我们可以通过使用__VA_ARGS__来避免"进入虚拟机ST(s1)"遭到黑客入侵，只是某些编译器不支持该状态。原来是“进入虚拟机ST(s1)或zhi_enter_state(s1)”换成了“英_可变参数” */
#endif
