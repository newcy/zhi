/****************************************************************************************************
 * 名称：知心编译器的 X86 代码生成器
 * Copyright (c) 2001-2022 Fabrice Bellard
 ****************************************************************************************************/
#ifdef TARGET_DEFS_ONLY

/* number of available registers */
#define NB_REGS         5
#define NB_ASM_REGS     8
#define CONFIG_ZHI_ASM

/* a register can belong to several classes. The classes must be
   sorted from more general to more precise (see 生成值2() code which does
   assumptions on it). */
#define RC_INT     0x0001 /* generic integer register */
#define RC_FLOAT   0x0002 /* generic float register */
#define RC_EAX     0x0004
#define RC_ST0     0x0008 
#define RC_ECX     0x0010
#define RC_EDX     0x0020
#define RC_EBX     0x0040

#define RC_IRET    RC_EAX /* 函数返回: integer register */
#define RC_IRE2    RC_EDX /* 函数返回: second integer register */
#define RC_FRET    RC_ST0 /* 函数返回: float register */

/* 寄存器的漂亮名字 */
enum {
    TREG_EAX = 0,
    TREG_ECX,
    TREG_EDX,
    TREG_EBX,
    TREG_ST0,
    TREG_ESP = 4
};

/* 函数的返回寄存器 */
#define REG_IRET TREG_EAX /* 单字int返回寄存器 */
#define REG_IRE2 TREG_EDX /* 第二个字返回寄存器（用于long long） */
#define REG_FRET TREG_ST0 /* 浮点返回寄存器 */

/* 如果函数参数必须按相反顺序求值，则定义 */
#define 反转函数参数

/* 如果结构作为指针传递，则定义。否则，结构将直接推到堆栈上。#define FUNC_STRUCT_PARAM_AS_PTR */
/* 指针大小，以字节为单位*/
#define 宏_指针大小 4

/* long double 大小 and alignment, in bytes */
#define 长双精度大小  12
#define 长双精度对齐单位 4
/* maximum alignment (for aligned attribute support) */
#define 最大对齐     8

/* define if return values need to be extended explicitely
   at caller side (for interfacing with non-ZHI compilers) */
#define 显式扩展返回值

/******************************************************/
#else /* ! TARGET_DEFS_ONLY */
/******************************************************/
#define 全局使用
#include "zhi.h"

静态数据 const char * const target_machine_defs =
    "__i386__\0"
    "__i386\0"
    ;

/* define to 1/0 to [not] have EBX as 4th register */
#define USE_EBX 0

静态数据 const int reg_classes[NB_REGS] = {
    /* eax */ RC_INT | RC_EAX,
    /* ecx */ RC_INT | RC_ECX,
    /* edx */ RC_INT | RC_EDX,
    /* ebx */ (RC_INT | RC_EBX) * USE_EBX,
    /* st0 */ RC_FLOAT | RC_ST0,
};

static unsigned long func_sub_sp_offset;
static int func_ret_sub;
#ifdef 启用边界检查M
static addr_t func_bound_offset;
static unsigned long func_bound_ind;
静态数据 int 函数_边界_添加_结语;
static void 生成函数绑定序言(void);
static void gen_bounds_epilog(void);
#endif

/* XXX: make it faster ? */
静态函数 void g(int c)
{
    int ind1;
    if (无需生成代码)
        return;
    ind1 = 指令在代码节位置 + 1;
    if (ind1 > 当前代码节->data_allocated)
        节_重新分配内存且内容为0(当前代码节, ind1);
    当前代码节->数据[指令在代码节位置] = c;
    指令在代码节位置 = ind1;
}

静态函数 void o(unsigned int c)
{
    while (c) {
        g(c);
        c = c >> 8;
    }
}

静态函数 void gen_le16(int v)
{
    g(v);
    g(v >> 8);
}

静态函数 void gen_le32(int c)
{
    g(c);
    g(c >> 8);
    g(c >> 16);
    g(c >> 24);
}

/* 输出一个符号并修补对它的所有调用 */
静态函数 void 生成符号_地址(int t, int a)
{
    while (t) {
        unsigned char *ptr = 当前代码节->数据 + t;
        uint32_t n = read32le(ptr); /* next value */
        write32le(ptr, a - t - 4);
        t = n;
    }
}

/* instruction + 4 bytes 数据. Return the address of the 数据 */
static int oad(int c, int s)
{
    int t;
    if (无需生成代码)
        return s;
    o(c);
    t = 指令在代码节位置;
    gen_le32(s);
    return t;
}

静态函数 void gen_fill_nops(int bytes)
{
    while (bytes--)
      g(0x90);
}

/* generate jmp to a label */
#define 生成到标签的跳转2(instr,lbl) oad(instr,lbl)

/* output constant with relocation if 'r & 存储类型_符号' is true */
静态函数 void gen_addr32(int r, 符号ST *sym, int c)
{
    if (r & 存储类型_符号)
        greloc(当前代码节, sym, 指令在代码节位置, R_386_32);
    gen_le32(c);
}

静态函数 void gen_addrpc32(int r, 符号ST *sym, int c)
{
    if (r & 存储类型_符号)
        greloc(当前代码节, sym, 指令在代码节位置, R_386_PC32);
    gen_le32(c - 4);
}

/* generate a modrm reference. 'op_reg' contains the additional 3
   opcode bits */
static void gen_modrm(int op_reg, int r, 符号ST *sym, int c)
{
    op_reg = op_reg << 3;
    if ((r & 存储类型_掩码) == 存储类型_VC常量) {
        /* constant memory reference */
        o(0x05 | op_reg);
        gen_addr32(r, sym, c);
    } else if ((r & 存储类型_掩码) == 存储类型_局部) {
        /* currently, we use only ebp as base */
        if (c == (char)c) {
            /* short reference */
            o(0x45 | op_reg);
            g(c);
        } else {
            oad(0x85 | op_reg, c);
        }
    } else {
        g(0x00 | op_reg | (r & 存储类型_掩码));
    }
}

/* load 'r' from value 'sv' */
静态函数 void load(int r, 栈值ST *sv)
{
    int v, t, ft, fc, fr;
    栈值ST v1;

#ifdef 目标_PE
    栈值ST v2;
    sv = pe_getimport(sv, &v2);
#endif

    fr = sv->寄存qi;
    ft = sv->类型.数据类型 & ~存储类型_明确有无符号;
    fc = sv->c.i;

    ft &= ~(存储类型_易变 | 存储类型_常量);

    v = fr & 存储类型_掩码;
    if (fr & 存储类型_左值) {
        if (v == 存储类型_LLOCAL) {
            v1.类型.数据类型 = 数据类型_整数;
            v1.寄存qi = 存储类型_局部 | 存储类型_左值;
            v1.c.i = fc;
            v1.sym = NULL;
            fr = r;
            if (!(reg_classes[fr] & RC_INT))
                fr = get_reg(RC_INT);
            load(fr, &v1);
        }
        if ((ft & 数据类型_基本类型) == 数据类型_浮点) {
            o(0xd9); /* flds */
            r = 0;
        } else if ((ft & 数据类型_基本类型) == 数据类型_双精度) {
            o(0xdd); /* fldl */
            r = 0;
        } else if ((ft & 数据类型_基本类型) == 数据类型_长双精度) {
            o(0xdb); /* fldt */
            r = 5;
        } else if ((ft & 值的数据类型_TYPE) == 数据类型_字节 || (ft & 值的数据类型_TYPE) == 数据类型_布尔) {
            o(0xbe0f);   /* movsbl */
        } else if ((ft & 值的数据类型_TYPE) == (数据类型_字节 | 存储类型_无符号的)) {
            o(0xb60f);   /* movzbl */
        } else if ((ft & 值的数据类型_TYPE) == 数据类型_短整数) {
            o(0xbf0f);   /* movswl */
        } else if ((ft & 值的数据类型_TYPE) == (数据类型_短整数 | 存储类型_无符号的)) {
            o(0xb70f);   /* movzwl */
        } else {
            o(0x8b);     /* movl */
        }
        gen_modrm(r, fr, sv->sym, fc);
    } else {
        if (v == 存储类型_VC常量) {
            o(0xb8 + r); /* mov $xx, r */
            gen_addr32(fr, sv->sym, fc);
        } else if (v == 存储类型_局部) {
            if (fc) {
                o(0x8d); /* lea xxx(%ebp), r */
                gen_modrm(r, 存储类型_局部, sv->sym, fc);
            } else {
                o(0x89);
                o(0xe8 + r); /* mov %ebp, r */
            }
        } else if (v == 存储类型_标志寄存器) {
            o(0x0f); /* setxx %br */
            o(fc);
            o(0xc0 + r);
            o(0xc0b60f + r * 0x90000); /* movzbl %al, %eax */
        } else if (v == 存储类型_JMP || v == 存储类型_JMPI) {
            t = v & 1;
            oad(0xb8 + r, t); /* mov $1, r */
            o(0x05eb); /* jmp after */
            生成符号(fc);
            oad(0xb8 + r, t ^ 1); /* mov $0, r */
        } else if (v != r) {
            o(0x89);
            o(0xc0 + r + v * 8); /* mov v, r */
        }
    }
}

/* store register 'r' in lvalue 'v' */
静态函数 void store(int r, 栈值ST *v)
{
    int fr, bt, ft, fc;

#ifdef 目标_PE
    栈值ST v2;
    v = pe_getimport(v, &v2);
#endif

    ft = v->类型.数据类型;
    fc = v->c.i;
    fr = v->寄存qi & 存储类型_掩码;
    ft &= ~(存储类型_易变 | 存储类型_常量);
    bt = ft & 数据类型_基本类型;
    /* XXX: incorrect if float reg to reg */
    if (bt == 数据类型_浮点) {
        o(0xd9); /* fsts */
        r = 2;
    } else if (bt == 数据类型_双精度) {
        o(0xdd); /* fstpl */
        r = 2;
    } else if (bt == 数据类型_长双精度) {
        o(0xc0d9); /* fld %st(0) */
        o(0xdb); /* fstpt */
        r = 7;
    } else {
        if (bt == 数据类型_短整数)
            o(0x66);
        if (bt == 数据类型_字节 || bt == 数据类型_布尔)
            o(0x88);
        else
            o(0x89);
    }
    if (fr == 存储类型_VC常量 ||
        fr == 存储类型_局部 ||
        (v->寄存qi & 存储类型_左值)) {
        gen_modrm(r, v->寄存qi, v->sym, fc);
    } else if (fr != r) {
        o(0xc0 + fr + r * 8); /* mov r, fr */
    }
}

static void gadd_sp(int val)
{
    if (val == (char)val) {
        o(0xc483);
        g(val);
    } else {
        oad(0xc481, val); /* add $xxx, %esp */
    }
}

#if defined 启用边界检查M || defined 目标_PE
static void gen_static_call(int v)
{
    符号ST *sym;

    sym = 外部_辅助_符号(v);
    oad(0xe8, -4);
    greloc(当前代码节, sym, 指令在代码节位置-4, R_386_PC32);
}
#endif

/* 'is_jmp' is '1' if it is a jump */
static void gcall_or_jmp(int is_jmp)
{
    int r;
    if ((栈顶值->寄存qi & (存储类型_掩码 | 存储类型_左值)) == 存储类型_VC常量 && (栈顶值->寄存qi & 存储类型_符号)) {
        /* constant and relocation case */
        greloc(当前代码节, 栈顶值->sym, 指令在代码节位置 + 1, R_386_PC32);
        oad(0xe8 + is_jmp, 栈顶值->c.i - 4); /* call/jmp im */
    } else {
        /* otherwise, indirect call */
        r = 生成值(RC_INT);
        o(0xff); /* call/jmp *r */
        o(0xd0 + r + (is_jmp << 4));
    }
}

static const uint8_t fastcall_regs[3] = { TREG_EAX, TREG_EDX, TREG_ECX };
static const uint8_t fastcallw_regs[2] = { TREG_ECX, TREG_EDX };

/* Return the number of registers needed to return the struct, or 0 if
   returning via struct pointer. */
静态函数 int gfunc_sret(类型ST *vt, int variadic, 类型ST *ret, int *ret_align, int *regsize)
{
#if defined(目标_PE) || 目标系统_FreeBSD || 目标系统_OpenBSD
    int 大小, 对齐;
    *ret_align = 1; // Never have to re-对齐 return values for x86
    *regsize = 4;
    大小 = 类型_大小(vt, &对齐);
    if (大小 > 8 || (大小 & (大小 - 1)))
        return 0;
    if (大小 == 8)
        ret->数据类型 = 数据类型_长长整数;
    else if (大小 == 4)
        ret->数据类型 = 数据类型_整数;
    else if (大小 == 2)
        ret->数据类型 = 数据类型_短整数;
    else
        ret->数据类型 = 数据类型_字节;
    ret->引用符号 = NULL;
    return 1;
#else
    *ret_align = 1; // Never have to re-对齐 return values for x86
    return 0;
#endif
}

/* Generate function call. The function address is pushed first, then
   all the parameters in call order. This functions pops all the
   parameters and the function address. */
静态函数 void g函数_调用(int nb_args)
{
    int 大小, 对齐, r, args_size, i, 函数调用约定;
    符号ST *函数符号;
    
#ifdef 启用边界检查M
    if (全局虚拟机->使用_边界_检查器)
        生成函数参数_边界(nb_args);
#endif

    args_size = 0;
    for(i = 0;i < nb_args; i++) {
        if ((栈顶值->类型.数据类型 & 数据类型_基本类型) == 数据类型_结构体) {
            大小 = 类型_大小(&栈顶值->类型, &对齐);
            /* 对齐 to stack 对齐 大小 */
            大小 = (大小 + 3) & ~3;
            /* allocate the necessary 大小 on stack */
#ifdef 目标_PE
            if (大小 >= 4096) {
                r = get_reg(RC_EAX);
                oad(0x68, 大小); // push 大小
                /* cannot call normal 'alloca' with bound checking */
                gen_static_call(标记分配常量("__alloca"));
                gadd_sp(4);
            } else
#endif
            {
                oad(0xec81, 大小); /* sub $xxx, %esp */
                /* generate structure store */
                r = get_reg(RC_INT);
                o(0xe089 + (r << 8)); /* mov %esp, r */
            }
            值压入栈(&栈顶值->类型, r | 存储类型_左值, 0);
            vswap();
            存储栈顶值();
            args_size += 大小;
        } else if (是_浮点(栈顶值->类型.数据类型)) {
            生成值(RC_FLOAT); /* only one float register */
            if ((栈顶值->类型.数据类型 & 数据类型_基本类型) == 数据类型_浮点)
                大小 = 4;
            else if ((栈顶值->类型.数据类型 & 数据类型_基本类型) == 数据类型_双精度)
                大小 = 8;
            else
                大小 = 12;
            oad(0xec81, 大小); /* sub $xxx, %esp */
            if (大小 == 12)
                o(0x7cdb);
            else
                o(0x5cd9 + 大小 - 4); /* fstp[s|l] 0(%esp) */
            g(0x24);
            g(0x00);
            args_size += 大小;
        } else {
            /* simple 类型 (currently always same 大小) */
            /* XXX: implicit cast ? */
            r = 生成值(RC_INT);
            if ((栈顶值->类型.数据类型 & 数据类型_基本类型) == 数据类型_长长整数) {
                大小 = 8;
                o(0x50 + 栈顶值->r2); /* push r */
            } else {
                大小 = 4;
            }
            o(0x50 + r); /* push r */
            args_size += 大小;
        }
        栈顶值--;
    }
    save_regs(0); /* save used temporary registers */
    函数符号 = 栈顶值->类型.引用符号;
    函数调用约定 = 函数符号->f.函数调用约定;
    /* fast call case */
    if ((函数调用约定 >= FUNC_FASTCALL1 && 函数调用约定 <= FUNC_FASTCALL3) ||
        函数调用约定 == FUNC_FASTCALLW) {
        int fastcall_nb_regs;
        const uint8_t *fastcall_regs_ptr;
        if (函数调用约定 == FUNC_FASTCALLW) {
            fastcall_regs_ptr = fastcallw_regs;
            fastcall_nb_regs = 2;
        } else {
            fastcall_regs_ptr = fastcall_regs;
            fastcall_nb_regs = 函数调用约定 - FUNC_FASTCALL1 + 1;
        }
        for(i = 0;i < fastcall_nb_regs; i++) {
            if (args_size <= 0)
                break;
            o(0x58 + fastcall_regs_ptr[i]); /* pop r */
            /* XXX: incorrect for struct/floats */
            args_size -= 4;
        }
    }
#if !defined(目标_PE) && !目标系统_FreeBSD || 目标系统_OpenBSD
    else if ((栈顶值->类型.引用符号->类型.数据类型 & 数据类型_基本类型) == 数据类型_结构体)
        args_size -= 4;
#endif

    gcall_or_jmp(0);

    if (args_size && 函数调用约定 != FUNC_STDCALL && 函数调用约定 != FUNC_FASTCALLW)
        gadd_sp(args_size);
    栈顶值--;
}

#ifdef 目标_PE
#define 函数序言大小M (10 + USE_EBX)
#else
#define 函数序言大小M (9 + USE_EBX)
#endif

/* generate function prolog of 类型 't' */
静态函数 void 生成函数开头代码(符号ST *函数符号)
{
    类型ST *函数_类型= &函数符号->类型;
    int addr, 对齐, 大小, 函数调用约定, fastcall_nb_regs;
    int param_index, param_addr;
    const uint8_t *fastcall_regs_ptr;
    符号ST *sym;
    类型ST *类型;

    sym = 函数_类型->引用符号;
    函数调用约定 = sym->f.函数调用约定;
    addr = 8;
    局部变量索引 = 0;
    func_vc = 0;

    if (函数调用约定 >= FUNC_FASTCALL1 && 函数调用约定 <= FUNC_FASTCALL3) {
        fastcall_nb_regs = 函数调用约定 - FUNC_FASTCALL1 + 1;
        fastcall_regs_ptr = fastcall_regs;
    } else if (函数调用约定 == FUNC_FASTCALLW) {
        fastcall_nb_regs = 2;
        fastcall_regs_ptr = fastcallw_regs;
    } else {
        fastcall_nb_regs = 0;
        fastcall_regs_ptr = NULL;
    }
    param_index = 0;

    指令在代码节位置 += 函数序言大小M;
    func_sub_sp_offset = 指令在代码节位置;
    /* if the function returns a structure, then add an
       implicit pointer parameter */
#if defined(目标_PE) || 目标系统_FreeBSD || 目标系统_OpenBSD
    大小 = 类型_大小(&函数_返回类型,&对齐);
    if (((函数_返回类型.数据类型 & 数据类型_基本类型) == 数据类型_结构体)
        && (大小 > 8 || (大小 & (大小 - 1)))) {
#else
    if ((函数_返回类型.数据类型 & 数据类型_基本类型) == 数据类型_结构体) {
#endif
        /* XXX: fastcall case ? */
        func_vc = addr;
        addr += 4;
        param_index++;
    }
    /* define parameters */
    while ((sym = sym->next) != NULL) {
        类型 = &sym->类型;
        大小 = 类型_大小(类型, &对齐);
        大小 = (大小 + 3) & ~3;
#ifdef FUNC_STRUCT_PARAM_AS_PTR
        /* structs are passed as pointer */
        if ((类型->数据类型 & 数据类型_基本类型) == 数据类型_结构体) {
            大小 = 4;
        }
#endif
        if (param_index < fastcall_nb_regs) {
            /* save FASTCALL register */
            局部变量索引 -= 4;
            o(0x89);     /* movl */
            gen_modrm(fastcall_regs_ptr[param_index], 存储类型_局部, NULL, 局部变量索引);
            param_addr = 局部变量索引;
        } else {
            param_addr = addr;
            addr += 大小;
        }
        将符号放入符号栈(sym->v & ~符号_字段, 类型,
                 存储类型_局部 | 存储类型_左值, param_addr);
        param_index++;
    }
    func_ret_sub = 0;
    /* pascal 类型 call or fastcall ? */
    if (函数调用约定 == FUNC_STDCALL || 函数调用约定 == FUNC_FASTCALLW)
        func_ret_sub = addr - 8;
#if !defined(目标_PE) && !目标系统_FreeBSD || 目标系统_OpenBSD
    else if (func_vc)
        func_ret_sub = 4;
#endif

#ifdef 启用边界检查M
    if (全局虚拟机->使用_边界_检查器)
        生成函数绑定序言();
#endif
}

/* generate function epilog */
静态函数 void 生成函数结尾代码(void)
{
    addr_t v, saved_ind;

#ifdef 启用边界检查M
    if (全局虚拟机->使用_边界_检查器)
        gen_bounds_epilog();
#endif

    /* 对齐 local 大小 to word & save local variables */
    v = (-局部变量索引 + 3) & -4;

#if USE_EBX
    o(0x8b);
    gen_modrm(TREG_EBX, 存储类型_局部, NULL, -(v+4));
#endif

    o(0xc9); /* leave */
    if (func_ret_sub == 0) {
        o(0xc3); /* ret */
    } else {
        o(0xc2); /* ret n */
        g(func_ret_sub);
        g(func_ret_sub >> 8);
    }
    saved_ind = 指令在代码节位置;
    指令在代码节位置 = func_sub_sp_offset - 函数序言大小M;
#ifdef 目标_PE
    if (v >= 4096) {
        oad(0xb8, v); /* mov stacksize, %eax */
        gen_static_call(TOK___chkstk); /* call __chkstk, (does the stackframe too) */
    } else
#endif
    {
        o(0xe58955);  /* push %ebp, mov %esp, %ebp */
        o(0xec81);  /* sub esp, stacksize */
        gen_le32(v);
#ifdef 目标_PE
        o(0x90);  /* adjust to 函数序言大小M */
#endif
    }
    o(0x53 * USE_EBX); /* push ebx */
    指令在代码节位置 = saved_ind;
}

/* generate a jump to a label */
静态函数 int 生成到标签的跳转(int t)
{
    return 生成到标签的跳转2(0xe9, t);
}

/* generate a jump to a fixed address */
静态函数 void 生成跳转_到固定地址(int a)
{
    int r;
    r = a - 指令在代码节位置 - 2;
    if (r == (char)r) {
        g(0xeb);
        g(r);
    } else {
        oad(0xe9, a - 指令在代码节位置 - 5);
    }
}

静态函数 int 生成跳转_append(int n, int t)
{
    void *p;
    /* insert 栈顶值->c jump list in t */
    if (n) {
        uint32_t n1 = n, n2;
        while ((n2 = read32le(p = 当前代码节->数据 + n1)))
            n1 = n2;
        write32le(p, t);
        t = n;
    }
    return t;
}

静态函数 int 生成条件跳转(int op, int t)
{
    g(0x0f);
    t = 生成到标签的跳转2(op - 16, t);
    return t;
}

静态函数 void gen_opi(int op)
{
    int r, fr, opc, c;

    switch(op) {
    case '+':
    case TOK_ADDC1: /* add with carry generation */
        opc = 0;
    gen_op8:
        if ((栈顶值->寄存qi & (存储类型_掩码 | 存储类型_左值 | 存储类型_符号)) == 存储类型_VC常量) {
            /* constant case */
            vswap();
            r = 生成值(RC_INT);
            vswap();
            c = 栈顶值->c.i;
            if (c == (char)c) {
                /* generate inc and dec for smaller code */
                if ((c == 1 || c == -1) && (op == '+' || op == '-')) {
                    opc = (c == 1) ^ (op == '+');
                    o (0x40 | (opc << 3) | r); // inc,dec
                } else {
                    o(0x83);
                    o(0xc0 | (opc << 3) | r);
                    g(c);
                }
            } else {
                o(0x81);
                oad(0xc0 | (opc << 3) | r, c);
            }
        } else {
            生成值2(RC_INT, RC_INT);
            r = 栈顶值[-1].寄存qi;
            fr = 栈顶值[0].寄存qi;
            o((opc << 3) | 0x01);
            o(0xc0 + r + fr * 8); 
        }
        栈顶值--;
        if (op >= TOK_ULT && op <= TOK_大于)
            vset_VT_CMP(op);
        break;
    case '-':
    case TOK_SUBC1: /* sub with carry generation */
        opc = 5;
        goto gen_op8;
    case TOK_ADDC2: /* add with carry use */
        opc = 2;
        goto gen_op8;
    case TOK_SUBC2: /* sub with carry use */
        opc = 3;
        goto gen_op8;
    case '&':
        opc = 4;
        goto gen_op8;
    case '^':
        opc = 6;
        goto gen_op8;
    case '|':
        opc = 1;
        goto gen_op8;
    case '*':
        生成值2(RC_INT, RC_INT);
        r = 栈顶值[-1].寄存qi;
        fr = 栈顶值[0].寄存qi;
        栈顶值--;
        o(0xaf0f); /* imul fr, r */
        o(0xc0 + fr + r * 8);
        break;
    case TOK_左移:
        opc = 4;
        goto gen_shift;
    case TOK_SHR:
        opc = 5;
        goto gen_shift;
    case TOK_右移:
        opc = 7;
    gen_shift:
        opc = 0xc0 | (opc << 3);
        if ((栈顶值->寄存qi & (存储类型_掩码 | 存储类型_左值 | 存储类型_符号)) == 存储类型_VC常量) {
            /* constant case */
            vswap();
            r = 生成值(RC_INT);
            vswap();
            c = 栈顶值->c.i & 0x1f;
            o(0xc1); /* shl/shr/sar $xxx, r */
            o(opc | r);
            g(c);
        } else {
            /* we generate the shift in ecx */
            生成值2(RC_INT, RC_ECX);
            r = 栈顶值[-1].寄存qi;
            o(0xd3); /* shl/shr/sar %cl, r */
            o(opc | r);
        }
        栈顶值--;
        break;
    case '/':
    case TOK_UDIV:
    case TOK_PDIV:
    case '%':
    case TOK_UMOD:
    case TOK_UMULL:
        /* first operand must be in eax */
        /* XXX: need better constraint for second operand */
        生成值2(RC_EAX, RC_ECX);
        r = 栈顶值[-1].寄存qi;
        fr = 栈顶值[0].寄存qi;
        栈顶值--;
        save_reg(TREG_EDX);
        /* save EAX too if used otherwise */
        save_reg_upstack(TREG_EAX, 1);
        if (op == TOK_UMULL) {
            o(0xf7); /* mul fr */
            o(0xe0 + fr);
            栈顶值->r2 = TREG_EDX;
            r = TREG_EAX;
        } else {
            if (op == TOK_UDIV || op == TOK_UMOD) {
                o(0xf7d231); /* xor %edx, %edx, div fr, %eax */
                o(0xf0 + fr);
            } else {
                o(0xf799); /* cltd, idiv fr, %eax */
                o(0xf8 + fr);
            }
            if (op == '%' || op == TOK_UMOD)
                r = TREG_EDX;
            else
                r = TREG_EAX;
        }
        栈顶值->寄存qi = r;
        break;
    default:
        opc = 7;
        goto gen_op8;
    }
}

/* generate a floating point operation 'v = t1 op t2' instruction. The
   two operands are guaranteed to have the same floating point 类型 */
/* XXX: need to use ST1 too */
静态函数 void gen_opf(int op)
{
    int a, ft, fc, swapped, r;

    if (op == TOK_NEG) { /* 一元 minus */
        生成值(RC_FLOAT);
        o(0xe0d9); /* fchs */
        return;
    }

    /* convert constants to memory references */
    if ((栈顶值[-1].寄存qi & (存储类型_掩码 | 存储类型_左值)) == 存储类型_VC常量) {
        vswap();
        生成值(RC_FLOAT);
        vswap();
    }
    if ((栈顶值[0].寄存qi & (存储类型_掩码 | 存储类型_左值)) == 存储类型_VC常量)
        生成值(RC_FLOAT);

    /* must put at least one value in the floating point register */
    if ((栈顶值[-1].寄存qi & 存储类型_左值) &&
        (栈顶值[0].寄存qi & 存储类型_左值)) {
        vswap();
        生成值(RC_FLOAT);
        vswap();
    }
    swapped = 0;
    /* swap the stack if needed so that t1 is the register and t2 is
       the memory reference */
    if (栈顶值[-1].寄存qi & 存储类型_左值) {
        vswap();
        swapped = 1;
    }
    if (op >= TOK_ULT && op <= TOK_大于) {
        /* load on stack second operand */
        load(TREG_ST0, 栈顶值);
        save_reg(TREG_EAX); /* eax is used by FP comparison code */
        if (op == TOK_大于等于 || op == TOK_大于)
            swapped = !swapped;
        else if (op == TOK_等于 || op == TOK_不等于)
            swapped = 0;
        if (swapped)
            o(0xc9d9); /* fxch %st(1) */
        if (op == TOK_等于 || op == TOK_不等于)
            o(0xe9da); /* fucompp */
        else
            o(0xd9de); /* fcompp */
        o(0xe0df); /* fnstsw %ax */
        if (op == TOK_等于) {
            o(0x45e480); /* and $0x45, %ah */
            o(0x40fC80); /* cmp $0x40, %ah */
        } else if (op == TOK_不等于) {
            o(0x45e480); /* and $0x45, %ah */
            o(0x40f480); /* xor $0x40, %ah */
            op = TOK_不等于;
        } else if (op == TOK_大于等于 || op == TOK_小于等于) {
            o(0x05c4f6); /* test $0x05, %ah */
            op = TOK_等于;
        } else {
            o(0x45c4f6); /* test $0x45, %ah */
            op = TOK_等于;
        }
        栈顶值--;
        vset_VT_CMP(op);
    } else {
        /* no memory reference possible for long double operations */
        if ((栈顶值->类型.数据类型 & 数据类型_基本类型) == 数据类型_长双精度) {
            load(TREG_ST0, 栈顶值);
            swapped = !swapped;
        }
        
        switch(op) {
        default:
        case '+':
            a = 0;
            break;
        case '-':
            a = 4;
            if (swapped)
                a++;
            break;
        case '*':
            a = 1;
            break;
        case '/':
            a = 6;
            if (swapped)
                a++;
            break;
        }
        ft = 栈顶值->类型.数据类型;
        fc = 栈顶值->c.i;
        if ((ft & 数据类型_基本类型) == 数据类型_长双精度) {
            o(0xde); /* fxxxp %st, %st(1) */
            o(0xc1 + (a << 3));
        } else {
            /* if saved lvalue, then we must reload it */
            r = 栈顶值->寄存qi;
            if ((r & 存储类型_掩码) == 存储类型_LLOCAL) {
                栈值ST v1;
                r = get_reg(RC_INT);
                v1.类型.数据类型 = 数据类型_整数;
                v1.寄存qi = 存储类型_局部 | 存储类型_左值;
                v1.c.i = fc;
                v1.sym = NULL;
                load(r, &v1);
                fc = 0;
            }

            if ((ft & 数据类型_基本类型) == 数据类型_双精度)
                o(0xdc);
            else
                o(0xd8);
            gen_modrm(a, r, 栈顶值->sym, fc);
        }
        栈顶值--;
    }
}

/* convert integers to fp 't' 类型. Must handle 'int', 'unsigned int'
   and 'long long' cases. */
静态函数 void gen_cvt_itof(int t)
{
    save_reg(TREG_ST0);
    生成值(RC_INT);
    if ((栈顶值->类型.数据类型 & 数据类型_基本类型) == 数据类型_长长整数) {
        /* signed long long to float/double/long double (unsigned case
           is handled generically) */
        o(0x50 + 栈顶值->r2); /* push r2 */
        o(0x50 + (栈顶值->寄存qi & 存储类型_掩码)); /* push r */
        o(0x242cdf); /* fildll (%esp) */
        o(0x08c483); /* add $8, %esp */
        栈顶值->r2 = 存储类型_VC常量;
    } else if ((栈顶值->类型.数据类型 & (数据类型_基本类型 | 存储类型_无符号的)) == 
               (数据类型_整数 | 存储类型_无符号的)) {
        /* unsigned int to float/double/long double */
        o(0x6a); /* push $0 */
        g(0x00);
        o(0x50 + (栈顶值->寄存qi & 存储类型_掩码)); /* push r */
        o(0x242cdf); /* fildll (%esp) */
        o(0x08c483); /* add $8, %esp */
    } else {
        /* int to float/double/long double */
        o(0x50 + (栈顶值->寄存qi & 存储类型_掩码)); /* push r */
        o(0x2404db); /* fildl (%esp) */
        o(0x04c483); /* add $4, %esp */
    }
    栈顶值->r2 = 存储类型_VC常量;
    栈顶值->寄存qi = TREG_ST0;
}

/* convert fp to int 't' 类型 */
静态函数 void gen_cvt_ftoi(int t)
{
    int bt = 栈顶值->类型.数据类型 & 数据类型_基本类型;
    if (bt == 数据类型_浮点)
        推送_辅助_函数引用(TOK___fixsfdi);
    else if (bt == 数据类型_长双精度)
        推送_辅助_函数引用(TOK___fixxfdi);
    else
        推送_辅助_函数引用(TOK___fixdfdi);
    vswap();
    g函数_调用(1);
    整数常量压入栈(0);
    栈顶值->寄存qi = REG_IRET;
    if ((t & 数据类型_基本类型) == 数据类型_长长整数)
        栈顶值->r2 = REG_IRE2;
}

/* convert from one floating point 类型 to another */
静态函数 void gen_cvt_ftof(int t)
{
    /* all we have to do on i386 is to put the float in a register */
    生成值(RC_FLOAT);
}

/* char/short to int conversion */
静态函数 void gen_cvt_csti(int t)
{
    int r, sz, xl;
    r = 生成值(RC_INT);
    sz = !(t & 存储类型_无符号的);
    xl = (t & 数据类型_基本类型) == 数据类型_短整数;
    o(0xc0b60f /* mov[sz] %a[xl], %eax */
        | (sz << 3 | xl) << 8
        | (r << 3 | r) << 16
        );
}

/* increment tcov counter */
静态函数 void gen_increment_tcov (栈值ST *sv)
{
   o(0x0583); /* addl $1, xxx */
   greloc(当前代码节, sv->sym, 指令在代码节位置, R_386_32);
   gen_le32(0);
   o(1);
   o(0x1583); /* addcl $0, xxx */
   greloc(当前代码节, sv->sym, 指令在代码节位置, R_386_32);
   gen_le32(4);
   g(0);
}

/* computed goto support */
静态函数 void ggoto(void)
{
    gcall_or_jmp(1);
    栈顶值--;
}

/* bound check support functions */
#ifdef 启用边界检查M

static void 生成函数绑定序言(void)
{
    /* leave some room for bound checking code */
    func_bound_offset = 局部边界_节->当前数据_偏移量;
    func_bound_ind = 指令在代码节位置;
    函数_边界_添加_结语 = 0;
    oad(0xb8, 0); /* lbound section pointer */
    oad(0xb8, 0); /* call to function */
}

static void gen_bounds_epilog(void)
{
    addr_t saved_ind;
    addr_t *bounds_ptr;
    符号ST *sym_data;
    int offset_modified = func_bound_offset != 局部边界_节->当前数据_偏移量;

    if (!offset_modified && !函数_边界_添加_结语)
        return;

    /* add end of table info */
    bounds_ptr = 节_预留指定大小内存(局部边界_节, sizeof(addr_t));
    *bounds_ptr = 0;

    sym_data = 取指向某个节的静态符号(&字符_指针_类型, 局部边界_节,
                           func_bound_offset, 局部边界_节->当前数据_偏移量);

    /* generate bound local allocation */
    if (offset_modified) {
        saved_ind = 指令在代码节位置;
        指令在代码节位置 = func_bound_ind;
        greloc(当前代码节, sym_data, 指令在代码节位置 + 1, R_386_32);
        指令在代码节位置 = 指令在代码节位置 + 5;
        gen_static_call(TOK___bound_local_new);
        指令在代码节位置 = saved_ind;
    }

    /* generate bound check local freeing */
    o(0x5250); /* save returned value, if any */
    greloc(当前代码节, sym_data, 指令在代码节位置 + 1, R_386_32);
    oad(0xb8, 0); /* mov %eax, xxx */
    gen_static_call(TOK___bound_local_delete);
    o(0x585a); /* restore returned value, if any */
}
#endif

/* Save the stack pointer onto the stack */
静态函数 void gen_vla_sp_save(int addr) {
    /* mov %esp,addr(%ebp)*/
    o(0x89);
    gen_modrm(TREG_ESP, 存储类型_局部, NULL, addr);
}

/* Restore the SP from a 地点 on the stack */
静态函数 void gen_vla_sp_恢复(int addr) {
    o(0x8b);
    gen_modrm(TREG_ESP, 存储类型_局部, NULL, addr);
}

/* Subtract from the stack pointer, and push the resulting value onto the stack */
静态函数 void gen_vla_alloc(类型ST *类型, int 对齐) {
    int use_call = 0;

#if defined(启用边界检查M)
    use_call = 全局虚拟机->使用_边界_检查器;
#endif
#ifdef 目标_PE    /* alloca does more than just adjust %rsp on Windows */
    use_call = 1;
#endif
    if (use_call)
    {
        推送_辅助_函数引用(TOK_alloca);
        vswap(); /* Move alloca 引用符号 past allocation 大小 */
        g函数_调用(1);
    }
    else {
        int r;
        r = 生成值(RC_INT); /* allocation 大小 */
        /* sub r,%rsp */
        o(0x2b);
        o(0xe0 | r);
        /* We 对齐 to 16 bytes rather than 对齐 */
        /* and ~15, %esp */
        o(0xf0e483);
        弹出堆栈值();
    }
}

/* end of X86 code generator */
/*************************************************************/
#endif
/*************************************************************/
