/****************************************************************************************************
 * 名称：windows使用的目标文件格式为COFF格式，使用的可执行文件格式为PE格式
 * Copyright (c) 2001-2022 TK ; Fabrice Bellard
 ****************************************************************************************************/
#include "zhi.h"

#define MAXNSCNS 255		/* MAXIMUM NUMBER OF SECTIONS         */
#define MAX_STR_TABLE 1000000
AOUTHDR o_filehdr;		/* OPTIONAL (A.OUT) FILE HEADER       */

SCNHDR section_header[MAXNSCNS];

#define MAX_FUNCS 1000
#define MAX_FUNC_NAME_LENGTH 128

int nFuncs;
char Func[MAX_FUNCS][MAX_FUNC_NAME_LENGTH];
char AssociatedFile[MAX_FUNCS][MAX_FUNC_NAME_LENGTH];
int LineNoFilePtr[MAX_FUNCS];
int EndAddress[MAX_FUNCS];
int LastLineNo[MAX_FUNCS];
int FuncEntries[MAX_FUNCS];

int OutputTheSection(节ST * sect);
short int GetCoffFlags(const char *s);
void 排序符号表(虚拟机ST *s1);
节ST *查找节(虚拟机ST * s1, const char *sname);

int C67_main_entry_point;

int 查找COFF符号索引(虚拟机ST * s1, const char *func_name);
int nb_syms;

typedef struct {
    long tag;
    long 大小;
    long fileptr;
    long nextsym;
    short int dummy;
} AUXFUNC;

typedef struct {
    long regmask;
    unsigned short lineno;
    unsigned short nentries;
    int localframe;
    int nextentry;
    short int dummy;
} AUXBF;

typedef struct {
    long dummy;
    unsigned short lineno;
    unsigned short dummy1;
    int dummy2;
    int dummy3;
    unsigned short dummy4;
} AUXEF;

静态函数 int 输出coff文件(虚拟机ST *s1, FILE *f)
{
    节ST *zhi_sect;
    SCNHDR *coff_sec;
    int file_pointer;
    char *Coff_str_table, *pCoff_str_table;
    int CoffTextSectionNo, coff_nb_syms;
    COFF文件头M 文件头;		/* FILE HEADER STRUCTURE              */
    节ST *stext, *sdata, *sbss;
    int i, NSectionsToOutput = 0;

    Coff_str_table = pCoff_str_table = NULL;

    stext = 查找节(s1, ".代码");
    sdata = 查找节(s1, ".数据");
    sbss = 查找节(s1, ".bss");

    nb_syms = 节符号表_数组->当前数据_偏移量 / sizeof(Elf32_Sym);
    coff_nb_syms = 查找COFF符号索引(s1, "XXXXXXXXXX1");

    文件头.f_目标机器类型 = COFF_C67_MAGIC;	/* magic number */
    文件头.f_日期时间戳 = 0;	/* time & date stamp */
    文件头.f_可选文件头大小 = sizeof(AOUTHDR);	/* sizeof(optional hdr) */
    文件头.f_文件属性标志 = 0x1143;	/* flags (copied from what code composer does) */
    文件头.f_TargetID = 0x99;	/* for C6x = 0x0099 */

    o_filehdr.magic = 0x0108;	/* see magic.h                          */
    o_filehdr.vstamp = 0x0190;	/* version stamp                        */
    o_filehdr.tsize = stext->当前数据_偏移量;	/* 代码 大小 in bytes, padded to FW bdry */
    o_filehdr.dsize = sdata->当前数据_偏移量;	/* initialized 数据 "  "                */
    o_filehdr.bsize = sbss->当前数据_偏移量;	/* uninitialized 数据 "   "             */
    o_filehdr.entrypt = C67_main_entry_point;	/* entry pt.                          */
    o_filehdr.text_start = stext->sh_addr;	/* 用于此文件的文本基础      */
    o_filehdr.data_start = sdata->sh_addr;	/* 用于此文件的数据基础      */


    // 创建所有节头

    file_pointer = FILHSZ + sizeof(AOUTHDR);

    CoffTextSectionNo = -1;

    for (i = 1; i < s1->节的数量; i++) {
	coff_sec = &section_header[i];
	zhi_sect = s1->节数组[i];

	if (OutputTheSection(zhi_sect)) {
	    NSectionsToOutput++;

	    if (CoffTextSectionNo == -1 && zhi_sect == stext)
		CoffTextSectionNo = NSectionsToOutput;	// rem which coff sect number the .代码 sect is

	    strcpy(coff_sec->s_name, zhi_sect->name);	/* section name */

	    coff_sec->s_paddr = zhi_sect->sh_addr;	/* physical address */
	    coff_sec->s_vaddr = zhi_sect->sh_addr;	/* virtual address */
	    coff_sec->s_size = zhi_sect->当前数据_偏移量;	/* section 大小 */
	    coff_sec->s_scnptr = 0;	/* 文件 ptr to raw 数据 for section */
	    coff_sec->s_relptr = 0;	/* 文件 ptr to relocation */
	    coff_sec->s_lnnoptr = 0;	/* 文件 ptr to line numbers */
	    coff_sec->s_nreloc = 0;	/* number of relocation entries */
	    coff_sec->s_flags = GetCoffFlags(coff_sec->s_name);	/* flags */
	    coff_sec->s_reserved = 0;	/* reserved byte */
	    coff_sec->s_page = 0;	/* memory page id */

	    file_pointer += sizeof(SCNHDR);
	}
    }

    文件头.f_节的数量 = NSectionsToOutput;	/* number of 节数组 */

    // now loop through and determine 文件 pointer locations
    // for the raw 数据


    for (i = 1; i < s1->节的数量; i++) {
	coff_sec = &section_header[i];
	zhi_sect = s1->节数组[i];

	if (OutputTheSection(zhi_sect)) {
	    // put raw 数据
	    coff_sec->s_scnptr = file_pointer;	/* 文件 ptr to raw 数据 for section */
	    file_pointer += coff_sec->s_size;
	}
    }

    // now loop through and determine 文件 pointer locations
    // for the relocation 数据

    for (i = 1; i < s1->节的数量; i++) {
	coff_sec = &section_header[i];
	zhi_sect = s1->节数组[i];

	if (OutputTheSection(zhi_sect)) {
	    // put relocations 数据
	    if (coff_sec->s_nreloc > 0) {
		coff_sec->s_relptr = file_pointer;	/* 文件 ptr to relocation */
		file_pointer += coff_sec->s_nreloc * sizeof(struct 重定位);
	    }
	}
    }

    // now loop through and determine 文件 pointer locations
    // for the line number 数据

    for (i = 1; i < s1->节的数量; i++) {
	coff_sec = &section_header[i];
	zhi_sect = s1->节数组[i];

	coff_sec->s_nlnno = 0;
	coff_sec->s_lnnoptr = 0;

	if (s1->做_调试 && zhi_sect == stext) {
	    // count how many line nos 数据

	    // also find association between source 文件 name and function
	    // so we can sort the symbol table


	    字符串表符号ST *sym, *sym_end;
	    char func_name[MAX_FUNC_NAME_LENGTH],
		last_func_name[MAX_FUNC_NAME_LENGTH];
	    unsigned long 函数_地址, last_pc, pc;
	    const char *include_文件数[INCLUDE_栈_大小];
	    int include_索引, 长度, 最后_行号;
	    const char *字符串, *p;

	    coff_sec->s_lnnoptr = file_pointer;	/* 文件 ptr to linno */


	    func_name[0] = '\0';
	    函数_地址 = 0;
	    include_索引 = 0;
	    last_func_name[0] = '\0';
	    last_pc = 0xffffffff;
	    最后_行号 = 1;
	    sym = (字符串表符号ST *) 调试节_数组->数据 + 1;
	    sym_end =
		(字符串表符号ST *) (调试节_数组->数据 +
			      调试节_数组->当前数据_偏移量);

	    nFuncs = 0;
	    while (sym < sym_end) {
		switch (sym->符号_类型) {
		    /* function start or end */
		case N_FUN:
		    if (sym->字符串_索引 == 0) {
			// end of function

			coff_sec->s_nlnno++;
			file_pointer += LINESZ;

			pc = sym->符号_值 + 函数_地址;
			func_name[0] = '\0';
			函数_地址 = 0;
			EndAddress[nFuncs] = pc;
			FuncEntries[nFuncs] =
			    (file_pointer -
			     LineNoFilePtr[nFuncs]) / LINESZ - 1;
			LastLineNo[nFuncs++] = 最后_行号 + 1;
		    } else {
			// beginning of function

			LineNoFilePtr[nFuncs] = file_pointer;
			coff_sec->s_nlnno++;
			file_pointer += LINESZ;

			字符串 =
			    (const char *) stabstr_section->数据 +
			    sym->字符串_索引;

			p = strchr(字符串, 英_冒号);
			if (!p) {
			    截取前n个字符(func_name, sizeof(func_name), 字符串);
			    截取前n个字符(Func[nFuncs], sizeof(func_name), 字符串);
			} else {
			    长度 = p - 字符串;
			    if (长度 > sizeof(func_name) - 1)
				长度 = sizeof(func_name) - 1;
			    memcpy(func_name, 字符串, 长度);
			    memcpy(Func[nFuncs], 字符串, 长度);
			    func_name[长度] = '\0';
			}

			// save the 文件 that it came in so we can sort later
			截取前n个字符(AssociatedFile[nFuncs], sizeof(func_name),
				include_文件数[include_索引 - 1]);

			函数_地址 = sym->符号_值;
		    }
		    break;

		    /* line number info */
		case N_SLINE:
		    pc = sym->符号_值 + 函数_地址;

		    last_pc = pc;
		    最后_行号 = sym->描述_字段;

		    /* XXX: slow! */
		    strcpy(last_func_name, func_name);

		    coff_sec->s_nlnno++;
		    file_pointer += LINESZ;
		    break;
		    /* include files */
		case N_BINCL:
		    字符串 =
			(const char *) stabstr_section->数据 + sym->字符串_索引;
		  add_incl:
		    if (include_索引 < INCLUDE_栈_大小) {
			include_文件数[include_索引++] = 字符串;
		    }
		    break;
		case N_EINCL:
		    if (include_索引 > 1)
			include_索引--;
		    break;
		case N_SO:
		    if (sym->字符串_索引 == 0) {
			include_索引 = 0;	/* 翻译单元结束 */
		    } else {
			字符串 =
			    (const char *) stabstr_section->数据 +
			    sym->字符串_索引;
			/* do not add path */
			长度 = strlen(字符串);
			if (长度 > 0 && 字符串[长度 - 1] != '/')
			    goto add_incl;
		    }
		    break;
		}
		sym++;
	    }
	}

    }

    文件头.f_符号表的文件指针 = file_pointer;	/* 文件 pointer to 符号表节 */

    if (s1->做_调试)
	文件头.f_符号表中的元素数目 = coff_nb_syms;	/* number of 符号表节 entries */
    else
	文件头.f_符号表中的元素数目 = 0;

    file_pointer += 文件头.f_符号表中的元素数目 * SYMNMLEN;

    // OK now we are all set to write the 文件


    fwrite(&文件头, FILHSZ, 1, f);
    fwrite(&o_filehdr, sizeof(o_filehdr), 1, f);

    // write section headers
    for (i = 1; i < s1->节的数量; i++) {
	coff_sec = &section_header[i];
	zhi_sect = s1->节数组[i];

	if (OutputTheSection(zhi_sect)) {
	    fwrite(coff_sec, sizeof(SCNHDR), 1, f);
	}
    }

    // write raw 数据
    for (i = 1; i < s1->节的数量; i++) {
	coff_sec = &section_header[i];
	zhi_sect = s1->节数组[i];

	if (OutputTheSection(zhi_sect)) {
	    fwrite(zhi_sect->数据, zhi_sect->当前数据_偏移量, 1, f);
	}
    }

    // write relocation 数据
    for (i = 1; i < s1->节的数量; i++) {
	coff_sec = &section_header[i];
	zhi_sect = s1->节数组[i];

	if (OutputTheSection(zhi_sect)) {
	    // put relocations 数据
	    if (coff_sec->s_nreloc > 0) {
		fwrite(zhi_sect->重定位,
		       coff_sec->s_nreloc * sizeof(struct 重定位), 1, f);
	    }
	}
    }


    // group the symbols in order of 文件名, func1, func2, etc
    // finally global symbols

    if (s1->做_调试)
	排序符号表(s1);

    // write line no 数据

    for (i = 1; i < s1->节的数量; i++) {
	coff_sec = &section_header[i];
	zhi_sect = s1->节数组[i];

	if (s1->做_调试 && zhi_sect == stext) {
	    // count how many line nos 数据


	    字符串表符号ST *sym, *sym_end;
	    char func_name[128], last_func_name[128];
	    unsigned long 函数_地址, last_pc, pc;
	    const char *include_文件数[INCLUDE_栈_大小];
	    int include_索引, 长度, 最后_行号;
	    const char *字符串, *p;

	    LINENO CoffLineNo;

	    func_name[0] = '\0';
	    函数_地址 = 0;
	    include_索引 = 0;
	    last_func_name[0] = '\0';
	    last_pc = 0;
	    最后_行号 = 1;
	    sym = (字符串表符号ST *) 调试节_数组->数据 + 1;
	    sym_end =
		(字符串表符号ST *) (调试节_数组->数据 +
			      调试节_数组->当前数据_偏移量);

	    while (sym < sym_end) {
		switch (sym->符号_类型) {
		    /* function start or end */
		case N_FUN:
		    if (sym->字符串_索引 == 0) {
			// end of function

			CoffLineNo.l_addr.l_paddr = last_pc;
			CoffLineNo.l_lnno = 最后_行号 + 1;
			fwrite(&CoffLineNo, 6, 1, f);

			pc = sym->符号_值 + 函数_地址;
			func_name[0] = '\0';
			函数_地址 = 0;
		    } else {
			// beginning of function

			字符串 =
			    (const char *) stabstr_section->数据 +
			    sym->字符串_索引;


			p = strchr(字符串, 英_冒号);
			if (!p) {
			    截取前n个字符(func_name, sizeof(func_name), 字符串);
			} else {
			    长度 = p - 字符串;
			    if (长度 > sizeof(func_name) - 1)
				长度 = sizeof(func_name) - 1;
			    memcpy(func_name, 字符串, 长度);
			    func_name[长度] = '\0';
			}
			函数_地址 = sym->符号_值;
			last_pc = 函数_地址;
			最后_行号 = -1;

			// output a function begin

			CoffLineNo.l_addr.l_symndx =
			    查找COFF符号索引(s1, func_name);
			CoffLineNo.l_lnno = 0;

			fwrite(&CoffLineNo, 6, 1, f);
		    }
		    break;

		    /* line number info */
		case N_SLINE:
		    pc = sym->符号_值 + 函数_地址;


		    /* XXX: slow! */
		    strcpy(last_func_name, func_name);

		    // output a line reference

		    CoffLineNo.l_addr.l_paddr = last_pc;

		    if (最后_行号 == -1) {
			CoffLineNo.l_lnno = sym->描述_字段;
		    } else {
			CoffLineNo.l_lnno = 最后_行号 + 1;
		    }

		    fwrite(&CoffLineNo, 6, 1, f);

		    last_pc = pc;
		    最后_行号 = sym->描述_字段;

		    break;

		    /* include files */
		case N_BINCL:
		    字符串 =
			(const char *) stabstr_section->数据 + sym->字符串_索引;
		  add_incl2:
		    if (include_索引 < INCLUDE_栈_大小) {
			include_文件数[include_索引++] = 字符串;
		    }
		    break;
		case N_EINCL:
		    if (include_索引 > 1)
			include_索引--;
		    break;
		case N_SO:
		    if (sym->字符串_索引 == 0) {
			include_索引 = 0;	/* 翻译单元结束 */
		    } else {
			字符串 =
			    (const char *) stabstr_section->数据 +
			    sym->字符串_索引;
			/* do not add path */
			长度 = strlen(字符串);
			if (长度 > 0 && 字符串[长度 - 1] != '/')
			    goto add_incl2;
		    }
		    break;
		}
		sym++;
	    }
	}
    }

    // write symbol table
    if (s1->做_调试) {
	int k;
	struct syment csym;
	AUXFUNC auxfunc;
	AUXBF auxbf;
	AUXEF auxef;
	int i;
	Elf32_Sym *p;
	const char *name;
	int nstr;
	int n = 0;

	Coff_str_table = (char *) zhi_分配内存(MAX_STR_TABLE);
	pCoff_str_table = Coff_str_table;
	nstr = 0;

	p = (Elf32_Sym *) 节符号表_数组->数据;


	for (i = 0; i < nb_syms; i++) {

	    name = 节符号表_数组->link->数据 + p->st_name;

	    for (k = 0; k < 8; k++)
		csym._n._n_name[k] = 0;

	    if (strlen(name) <= 8) {
		strcpy(csym._n._n_name, name);
	    } else {
		if (pCoff_str_table - Coff_str_table + strlen(name) >
		    MAX_STR_TABLE - 1)
		    错_误("字符串表太大");

		csym._n._n_n._n_zeroes = 0;
		csym._n._n_n._n_offset =
		    pCoff_str_table - Coff_str_table + 4;

		strcpy(pCoff_str_table, name);
		pCoff_str_table += strlen(name) + 1;	// 跳过 over null
		nstr++;
	    }

	    if (p->st_info == 4) {
		// put a 文件名 symbol
		csym.符号_值 = 33;	// ?????
		csym.n_scnum = N_DEBUG;
		csym.符号_类型 = 0;
		csym.n_sclass = C_FILE;
		csym.n_numaux = 0;
		fwrite(&csym, 18, 1, f);
		n++;

	    } else if (p->st_info == 0x12) {
		// find the function 数据

		for (k = 0; k < nFuncs; k++) {
		    if (strcmp(name, Func[k]) == 0)
			break;
		}

		if (k >= nFuncs) {
		    错_误("调试信息找不到函数", name);
		}
		// put a Function Name

		csym.符号_值 = p->st_value;	// physical address
		csym.n_scnum = CoffTextSectionNo;
		csym.符号_类型 = MKTYPE(T_INT, DT_FCN, 0, 0, 0, 0, 0);
		csym.n_sclass = C_EXT;
		csym.n_numaux = 1;
		fwrite(&csym, 18, 1, f);

		// now put aux info

		auxfunc.tag = 0;
		auxfunc.大小 = EndAddress[k] - p->st_value;
		auxfunc.fileptr = LineNoFilePtr[k];
		auxfunc.nextsym = n + 6;	// tktk
		auxfunc.dummy = 0;
		fwrite(&auxfunc, 18, 1, f);

		// put a .缓冲文件

		strcpy(csym._n._n_name, ".缓冲文件");
		csym.符号_值 = p->st_value;	// physical address
		csym.n_scnum = CoffTextSectionNo;
		csym.符号_类型 = 0;
		csym.n_sclass = C_FCN;
		csym.n_numaux = 1;
		fwrite(&csym, 18, 1, f);

		// now put aux info

		auxbf.regmask = 0;
		auxbf.lineno = 0;
		auxbf.nentries = FuncEntries[k];
		auxbf.localframe = 0;
		auxbf.nextentry = n + 6;
		auxbf.dummy = 0;
		fwrite(&auxbf, 18, 1, f);

		// put a .ef

		strcpy(csym._n._n_name, ".ef");
		csym.符号_值 = EndAddress[k];	// physical address  
		csym.n_scnum = CoffTextSectionNo;
		csym.符号_类型 = 0;
		csym.n_sclass = C_FCN;
		csym.n_numaux = 1;
		fwrite(&csym, 18, 1, f);

		// now put aux info

		auxef.dummy = 0;
		auxef.lineno = LastLineNo[k];
		auxef.dummy1 = 0;
		auxef.dummy2 = 0;
		auxef.dummy3 = 0;
		auxef.dummy4 = 0;
		fwrite(&auxef, 18, 1, f);

		n += 6;

	    } else {
		// try an put some 类型 info

		if ((p->st_other & 数据类型_基本类型) == 数据类型_双精度) {
		    csym.符号_类型 = T_DOUBLE;	// int
		    csym.n_sclass = C_EXT;
		} else if ((p->st_other & 数据类型_基本类型) == 数据类型_浮点) {
		    csym.符号_类型 = T_FLOAT;
		    csym.n_sclass = C_EXT;
		} else if ((p->st_other & 数据类型_基本类型) == 数据类型_整数) {
		    csym.符号_类型 = T_INT;	// int
		    csym.n_sclass = C_EXT;
		} else if ((p->st_other & 数据类型_基本类型) == 数据类型_短整数) {
		    csym.符号_类型 = T_SHORT;
		    csym.n_sclass = C_EXT;
		} else if ((p->st_other & 数据类型_基本类型) == 数据类型_字节) {
		    csym.符号_类型 = T_CHAR;
		    csym.n_sclass = C_EXT;
		} else {
		    csym.符号_类型 = T_INT;	// just mark as a label
		    csym.n_sclass = C_LABEL;
		}


		csym.符号_值 = p->st_value;
		csym.n_scnum = 2;
		csym.n_numaux = 1;
		fwrite(&csym, 18, 1, f);

		auxfunc.tag = 0;
		auxfunc.大小 = 0x20;
		auxfunc.fileptr = 0;
		auxfunc.nextsym = 0;
		auxfunc.dummy = 0;
		fwrite(&auxfunc, 18, 1, f);
		n++;
		n++;

	    }

	    p++;
	}
    }

    if (s1->做_调试) {
	// write string table

	// first write the 大小
	i = pCoff_str_table - Coff_str_table;
	fwrite(&i, 4, 1, f);

	// then write the strings
	fwrite(Coff_str_table, i, 1, f);

	zhi_释放(Coff_str_table);
    }

    return 0;
}



// 按文件名、func1、func2 等的顺序对符号进行分组
// 最终的全局符号

void 排序符号表(虚拟机ST *s1)
{
    int i, j, k, n = 0;
    Elf32_Sym *p, *p2, *NewTable;
    char *name, *name2;

    NewTable = (Elf32_Sym *) zhi_分配内存(nb_syms * sizeof(Elf32_Sym));

    p = (Elf32_Sym *) 节符号表_数组->数据;


    // find a 文件 symbol, copy it over
    // then scan the whole symbol list and copy any function
    // symbols that match the 文件 association

    for (i = 0; i < nb_syms; i++) {
	if (p->st_info == 4) {
	    name = (char *) 节符号表_数组->link->数据 + p->st_name;

	    // this is a 文件 symbol, copy it over

	    NewTable[n++] = *p;

	    p2 = (Elf32_Sym *) 节符号表_数组->数据;

	    for (j = 0; j < nb_syms; j++) {
		if (p2->st_info == 0x12) {
		    // this is a func symbol

		    name2 =
			(char *) 节符号表_数组->link->数据 + p2->st_name;

		    // find the function 数据 index

		    for (k = 0; k < nFuncs; k++) {
			if (strcmp(name2, Func[k]) == 0)
			    break;
		    }

		    if (k >= nFuncs) {
                        错_误("调试（排序）信息找不到函数: %s", name2);
		    }

		    if (strcmp(AssociatedFile[k], name) == 0) {
			// yes they match copy it over

			NewTable[n++] = *p2;
		    }
		}
		p2++;
	    }
	}
	p++;
    }

    // now all the 文件名 and func symbols should have been copied over
    // copy all the rest over (all except 文件 and funcs)

    p = (Elf32_Sym *) 节符号表_数组->数据;
    for (i = 0; i < nb_syms; i++) {
	if (p->st_info != 4 && p->st_info != 0x12) {
	    NewTable[n++] = *p;
	}
	p++;
    }

    if (n != nb_syms)
	错_误("内部编译器错误，调试信息");

    // copy it all back

    p = (Elf32_Sym *) 节符号表_数组->数据;
    for (i = 0; i < nb_syms; i++) {
	*p++ = NewTable[i];
    }

    zhi_释放(NewTable);
}


int 查找COFF符号索引(虚拟机ST *s1, const char *func_name)
{
    int i, n = 0;
    Elf32_Sym *p;
    char *name;

    p = (Elf32_Sym *) 节符号表_数组->数据;

    for (i = 0; i < nb_syms; i++) {

	name = (char *) 节符号表_数组->link->数据 + p->st_name;

	if (p->st_info == 4) {
	    //放一个文件名称符号
	    n++;
	} else if (p->st_info == 0x12) {

	    if (strcmp(func_name, name) == 0)
		return n;

	    n += 6;

	} else {
	    n += 2;
	}

	p++;
    }

    return n;			// 字符总数
}

int OutputTheSection(节ST * sect)
{
    const char *s = sect->name;

    if (!strcmp(s, ".代码"))
	return 1;
    else if (!strcmp(s, ".数据"))
	return 1;
    else
	return 0;
}

short int GetCoffFlags(const char *s)
{
    if (!strcmp(s, ".代码"))
	return STYP_TEXT | STYP_DATA | STYP_ALIGN | 0x400;
    else if (!strcmp(s, ".数据"))
	return STYP_DATA;
    else if (!strcmp(s, ".bss"))
	return STYP_BSS;
    else if (!strcmp(s, ".stack"))
	return STYP_BSS | STYP_ALIGN | 0x200;
    else if (!strcmp(s, ".cinit"))
	return STYP_COPY | STYP_DATA | STYP_ALIGN | 0x200;
    else
	return 0;
}

节ST *查找节(虚拟机ST * s1, const char *sname)
{
    节ST *s;
    int i;

    for (i = 1; i < s1->节的数量; i++) {
	s = s1->节数组[i];

	if (!strcmp(sname, s->name))
	    return s;
    }

    错_误("找不到 %s节", sname);
    return 0;
}

静态函数 int 加载coff文件(虚拟机ST * s1, int 文件描述)
{
// tktk 单词ST *ts;

    FILE *f;
    unsigned int str_size;
    char *Coff_str_table, *name;
    int i, k;
    struct syment csym;
    char name2[9];
    COFF文件头M 文件头;

    f = fdopen(文件描述, "rb");
    if (!f)
    {
		错_误("无法打开 .out 文件进行输入");
    }
    if (fread(&文件头, FILHSZ, 1, f) != 1)
    {
    	错_误("读取 .out 文件以进行输入时出错");
    }
    if (fread(&o_filehdr, sizeof(o_filehdr), 1, f) != 1)
    {
    	错_误("读取 .out 文件以进行输入时出错");
    }
    // 首先读取字符串表
    if (fseek(f, 文件头.f_符号表的文件指针 + 文件头.f_符号表中的元素数目 * SYMESZ, SEEK_SET))
    {
    	错_误("读取 .out 文件以进行输入时出错");
    }
    if (fread(&str_size, sizeof(int), 1, f) != 1)
    {
    	错_误("读取 .out 文件以进行输入时出错");
    }
    Coff_str_table = (char *) zhi_分配内存(str_size);

    if (fread(Coff_str_table, str_size - 4, 1, f) != 1)
    {
    	错_误("读取 .out 文件以进行输入时出错");
    }
    // 读取/处理所有符号
    // 寻找符号
    if (fseek(f, 文件头.f_符号表的文件指针, SEEK_SET))
    {
    	错_误("读取 .out 文件以进行输入时出错");
    }
    for (i = 0; i < 文件头.f_符号表中的元素数目; i++)
    {
		if (fread(&csym, SYMESZ, 1, f) != 1)
		{
			错_误("读取 .out 文件以进行输入时出错");
		}
		if (csym._n._n_n._n_zeroes == 0)
		{
			name = Coff_str_table + csym._n._n_n._n_offset - 4;
		} else
		{
			name = csym._n._n_name;

			if (name[7] != 0)
			{
			for (k = 0; k < 8; k++)
			{
				name2[k] = name[k];
			}
			name2[8] = 0;

			name = name2;
			}
		}

		if (((csym.符号_类型 & 0x30) == 0x20 && csym.n_sclass == 0x2) ||
				((csym.符号_类型 & 0x30) == 0x30 && csym.n_sclass == 0x2) ||
				(csym.符号_类型 == 0x4 && csym.n_sclass == 0x2) ||
				(csym.符号_类型 == 0x8 && csym.n_sclass == 0x2) ||	// structures
				(csym.符号_类型 == 0x18 && csym.n_sclass == 0x2) ||	// pointer to structure
				(csym.符号_类型 == 0x7 && csym.n_sclass == 0x2) ||	// doubles
				(csym.符号_类型 == 0x6 && csym.n_sclass == 0x2))	// floats
		{
			// 去掉任何前导下划线（其他主程序除外）

			if (name[0] == '_' && strcmp(name, "_main") != 0)
			name++;

			zhi_添加_符号(s1, name, (void*)(uintptr_t)csym.符号_值);
		}
		// 跳过任何辅助记录

		if (csym.n_numaux == 1)
		{
			if (fread(&csym, SYMESZ, 1, f) != 1)
			{
				错_误("读取 .out 文件以进行输入时出错");
			}
			i++;
		}
    }

    return 0;
}
