#ifndef _核心_H
#define _核心_H

#ifndef 核心库接口
# define 核心库接口
#endif

#ifdef __cplusplus    /* 告诉c++编译器 {}里面的代码以C语言的编译方式编译 */
extern "C" {
#endif

typedef void (*ZHI错误函数)(void *不透明, const char *msg);
核心库接口 虚拟机ST *新建虚拟机(void);
核心库接口 void 删除_虚拟机ST(虚拟机ST *s);
核心库接口 void 设置_库_路径(虚拟机ST *s, const char *path);
核心库接口 void zhi_设置_错误_函数(虚拟机ST *s, void *错误_不透明, ZHI错误函数 错误_函数);
核心库接口 ZHI错误函数 zhi_获取_错误_函数(虚拟机ST *s);
核心库接口 void *zhi_获取_错误_不透明(虚拟机ST *s);
核心库接口 void 设置选项(虚拟机ST *s, const char *字符串);

/**************************************************预处理--开始**************************************************/
核心库接口 int 添加_导入路径(虚拟机ST *s, const char *pathname);
核心库接口 int 添加系统导入路径(虚拟机ST *s, const char *pathname);
核心库接口 void zhi_定义_预处理符号(虚拟机ST *s, const char *sym, const char *value);
核心库接口 void zhi_结束定义_预处理符号(虚拟机ST *s, const char *sym);
/**************************************************预处理--结束**************************************************/
/**************************************************编译--开始**************************************************/
核心库接口 int 编译器(虚拟机ST *s, const char *文件名);
static int 开始编译(虚拟机ST *s1, int 源文件类型, const char *路径, int 文件描述);
/**************************************************编译--结束**************************************************/
/**************************************************链接命令--开始**************************************************/
核心库接口 int 设置输出类型(虚拟机ST *s, int 输出类型);
/*输出类型*/
#define 输出_内存   1 /* 输出将在内存中运行（默认）*/
#define 输出_EXE      2
#define 输出_DLL      3
#define 输出_OBJ      4  /*输出目标文件*/
#define 输出_预处理     5 /* 仅预处理 (内部使用) */
/**************************************************链接命令--结束**************************************************/
核心库接口 int zhi_添加库路径(虚拟机ST *s, const char *pathname);
核心库接口 int 添加库(虚拟机ST *s, const char *库文件名);
核心库接口 int zhi_添加_符号(虚拟机ST *s, const char *name, const void *val);
核心库接口 int 链接器(虚拟机ST *s, const char *文件名);
核心库接口 int 运行脚本(虚拟机ST *s, int argc, char **argv);
核心库接口 int zhi_重定位(虚拟机ST *s1, void *ptr);
#define 自动_重定位 (void*)1
核心库接口 void *取符号值(虚拟机ST *s, const char *name);
核心库接口 void zhi_列出elf符号名称和值(虚拟机ST *s, void *ctx,
    void (*symbol_cb)(void *ctx, const char *name, const void *val));

#ifdef __cplusplus
}
#endif

#endif /* _核心_H 结束*/
