/****************************************************************************************************
 * 名称：GAS汇编器
 * Copyright (c) 2001-2022 Fabrice Bellard
 ****************************************************************************************************/
#define 全局使用
#include "zhi.h"
#ifdef CONFIG_ZHI_ASM

static 节ST *last_text_section; /* to handle .previous asm directive */

静态函数 int asm_get_local_label_name(虚拟机ST *s1, unsigned int n)
{
    char buf[64];
    snprintf(buf, sizeof(buf), "L..%u", n);
    return 标记分配常量(buf);
}

static int 汇编_内部(虚拟机ST *s1, int 执行_预处理, int global);
static 符号ST* asm_new_label(虚拟机ST *s1, int label, int is_local);
static 符号ST* asm_new_label1(虚拟机ST *s1, int label, int is_local, int ELF节数量, int value);

/* 如果C名称前面有_，那么只有以_开头的asm标签才能通过删除第一个_在C中表示。开头没有_的ASM名称与C名称不对应，但我们也使用全局C符号表来跟踪ASM名称，因此我们需要将它们转换为与C名称没有冲突的名称，因此在前面加上一个“”但强制设置ELF asm名称。  */
static int asm2cname(int v, int *addeddot)
{
    const char *name;
    *addeddot = 0;
    if (!全局虚拟机->前导下划线)
      return v;
    name = 取单词字符串(v, NULL);
    if (!name)
      return v;
    if (name[0] == '_') {
        v = 标记分配常量(name + 1);
    } else if (!strchr(name, '.')) {
        char newname[256];
        snprintf(newname, sizeof newname, ".%s", name);
        v = 标记分配常量(newname);
        *addeddot = 1;
    }
    return v;
}

static 符号ST *asm_label_find(int v)
{
    符号ST *sym;
    int addeddot;
    v = asm2cname(v, &addeddot);
    sym = 查找标识符(v);
    while (sym && sym->作用域范围 && !(sym->类型.数据类型 & 存储类型_静态))
        sym = sym->定义的前一个同名符号;
    return sym;
}

static 符号ST *asm_label_push(int v)
{
    int addeddot, v2 = asm2cname(v, &addeddot);
    /* We always add 存储类型_外部, for sym definition that's tentative
       (for .set, removed for real defs), for mere references it's correct
       as is.  */
    符号ST *sym = 将符号放入全局符号表(v2, 值的数据类型_ASM | 存储类型_外部 | 存储类型_静态, 0);
    if (addeddot)
        sym->asm_label = v;
    return sym;
}

/* Return a symbol we can use inside the assembler, having name NAME.
   Symbols from asm and C source share a namespace.  If we generate
   an asm symbol it's also a (文件-global) C symbol, but it's
   either not accessible by name (like "L.123"), or its 类型 information
   is such that it's not usable without a proper C declaration.

   Sometimes we need symbols accessible by name from asm, which
   are anonymous in C, in this case CSYM can be used to transfer
   all information from that symbol to the (possibly newly created)
   asm symbol.  */
静态函数 符号ST* get_asm_sym(int name, 符号ST *csym)
{
    符号ST *sym = asm_label_find(name);
    if (!sym) {
	sym = asm_label_push(name);
	if (csym)
	  sym->c = csym->c;
    }
    return sym;
}

static 符号ST* asm_section_sym(虚拟机ST *s1, 节ST *sec)
{
    char buf[100]; int label; 符号ST *sym;
    snprintf(buf, sizeof buf, "L.%s", sec->name);
    label = 标记分配常量(buf);
    sym = asm_label_find(label);
    return sym ? sym : asm_new_label1(s1, label, 1, sec->ELF节数量, 0);
}

/* We do not use the C expression parser to handle symbols. Maybe the
   C expression parser could be tweaked to do so. */

static void asm_expr_unary(虚拟机ST *s1, 表达式值ST *pe)
{
    符号ST *sym;
    int op, label;
    uint64_t n;
    const char *p;

    switch(单词编码) {
    case TOK_预处理数字:
        p = 单词常量.字符串.数据;
        n = strtoull(p, (char **)&p, 0);
        if (*p == 'b' || *p == 'f') {
            /* backward or forward label */
            label = asm_get_local_label_name(s1, n);
            sym = asm_label_find(label);
            if (*p == 'b') {
                /* backward : find the last corresponding defined label */
                if (sym && (!sym->c || elfsym(sym)->st_shndx == SHN_未定义))
                    sym = sym->定义的前一个同名符号;
                if (!sym)
                    错_误("向后找不到本地标签'%d' ", (int)n);
            } else {
                /* forward */
                if (!sym || (sym->c && elfsym(sym)->st_shndx != SHN_未定义)) {
                    /* if the last label is defined, then define a new one */
		    sym = asm_label_push(label);
                }
            }
	    pe->v = 0;
	    pe->sym = sym;
	    pe->pcrel = 0;
        } else if (*p == '\0') {
            pe->v = n;
            pe->sym = NULL;
	    pe->pcrel = 0;
        } else {
            错_误("无效的数字语法");
        }
        取下个单词();
        break;
    case '+':
        取下个单词();
        asm_expr_unary(s1, pe);
        break;
    case '-':
    case '~':
        op = 单词编码;
        取下个单词();
        asm_expr_unary(s1, pe);
        if (pe->sym)
            错_误("标签的无效操作");
        if (op == '-')
            pe->v = -pe->v;
        else
            pe->v = ~pe->v;
        break;
    case TOK_字符常量:
    case TOK_LCHAR:
	pe->v = 单词常量.i;
	pe->sym = NULL;
	pe->pcrel = 0;
	取下个单词();
	break;
    case 英_左小括号:
        取下个单词();
        asm_expr(s1, pe);
        跳过(英_右小括号);
        break;
    case '.':
        pe->v = 指令在代码节位置;
        pe->sym = asm_section_sym(s1, 当前代码节);
        pe->pcrel = 0;
        取下个单词();
        break;
    default:
        if (单词编码 >= TOK_标识符) {
	    ElfSym *esym;
            /* label case : if the label was not found, add one */
	    sym = get_asm_sym(单词编码, NULL);
	    esym = elfsym(sym);
            if (esym && esym->st_shndx == SHN_ABS) {
                /* if absolute symbol, no need to put a symbol value */
                pe->v = esym->st_value;
                pe->sym = NULL;
		pe->pcrel = 0;
            } else {
                pe->v = 0;
                pe->sym = sym;
		pe->pcrel = 0;
            }
            取下个单词();
        } else {
            错_误("错误的表达式语法 [%s]", 取单词字符串(单词编码, &单词常量));
        }
        break;
    }
}
    
static void asm_expr_prod(虚拟机ST *s1, 表达式值ST *pe)
{
    int op;
    表达式值ST e2;

    asm_expr_unary(s1, pe);
    for(;;) {
        op = 单词编码;
        if (op != '*' && op != '/' && op != '%' && 
            op != TOK_左移 && op != TOK_右移)
            break;
        取下个单词();
        asm_expr_unary(s1, &e2);
        if (pe->sym || e2.sym)
            错_误("标签的无效操作");
        switch(op) {
        case '*':
            pe->v *= e2.v;
            break;
        case '/':  
            if (e2.v == 0) {
            div_error:
                错_误("除以零");
            }
            pe->v /= e2.v;
            break;
        case '%':  
            if (e2.v == 0)
                goto div_error;
            pe->v %= e2.v;
            break;
        case TOK_左移:
            pe->v <<= e2.v;
            break;
        default:
        case TOK_右移:
            pe->v >>= e2.v;
            break;
        }
    }
}

static void asm_expr_logic(虚拟机ST *s1, 表达式值ST *pe)
{
    int op;
    表达式值ST e2;

    asm_expr_prod(s1, pe);
    for(;;) {
        op = 单词编码;
        if (op != '&' && op != '|' && op != '^')
            break;
        取下个单词();
        asm_expr_prod(s1, &e2);
        if (pe->sym || e2.sym)
            错_误("标签的无效操作");
        switch(op) {
        case '&':
            pe->v &= e2.v;
            break;
        case '|':  
            pe->v |= e2.v;
            break;
        default:
        case '^':
            pe->v ^= e2.v;
            break;
        }
    }
}

static inline void asm_expr_sum(虚拟机ST *s1, 表达式值ST *pe)
{
    int op;
    表达式值ST e2;

    asm_expr_logic(s1, pe);
    for(;;) {
        op = 单词编码;
        if (op != '+' && op != '-')
            break;
        取下个单词();
        asm_expr_logic(s1, &e2);
        if (op == '+') {
            if (pe->sym != NULL && e2.sym != NULL)
                goto cannot_relocate;
            pe->v += e2.v;
            if (pe->sym == NULL && e2.sym != NULL)
                pe->sym = e2.sym;
        } else {
            pe->v -= e2.v;
            /* NOTE: we are less powerful than gas in that case
               because we store only one symbol in the expression */
	    if (!e2.sym) {
		/* OK */
	    } else if (pe->sym == e2.sym) { 
		/* OK */
		pe->sym = NULL; /* same symbols can be subtracted to NULL */
	    } else {
		ElfSym *esym1, *esym2;
		esym1 = elfsym(pe->sym);
		esym2 = elfsym(e2.sym);
		if (esym1 && esym1->st_shndx == esym2->st_shndx
		    && esym1->st_shndx != SHN_未定义) {
		    /* we also accept defined symbols in the same section */
		    pe->v += esym1->st_value - esym2->st_value;
		    pe->sym = NULL;
		} else if (esym2->st_shndx == 当前代码节->ELF节数量) {
		    /* When subtracting a defined symbol in current section
		       this actually makes the value PC-relative.  */
		    pe->v -= esym2->st_value - 指令在代码节位置 - 4;
		    pe->pcrel = 1;
		    e2.sym = NULL;
		} else {
cannot_relocate:
		    错_误("标签的无效操作");
		}
	    }
        }
    }
}

static inline void asm_expr_cmp(虚拟机ST *s1, 表达式值ST *pe)
{
    int op;
    表达式值ST e2;

    asm_expr_sum(s1, pe);
    for(;;) {
        op = 单词编码;
	if (op != TOK_等于 && op != TOK_不等于
	    && (op > TOK_大于 || op < TOK_ULE))
            break;
        取下个单词();
        asm_expr_sum(s1, &e2);
        if (pe->sym || e2.sym)
            错_误("标签的无效操作");
        switch(op) {
	case TOK_等于:
	    pe->v = pe->v == e2.v;
	    break;
	case TOK_不等于:
	    pe->v = pe->v != e2.v;
	    break;
	case TOK_小于:
	    pe->v = (int64_t)pe->v < (int64_t)e2.v;
	    break;
	case TOK_大于等于:
	    pe->v = (int64_t)pe->v >= (int64_t)e2.v;
	    break;
	case TOK_小于等于:
	    pe->v = (int64_t)pe->v <= (int64_t)e2.v;
	    break;
	case TOK_大于:
	    pe->v = (int64_t)pe->v > (int64_t)e2.v;
	    break;
        default:
            break;
        }
	/* GAS compare results are -1/0 not 1/0.  */
	pe->v = -(int64_t)pe->v;
    }
}

静态函数 void asm_expr(虚拟机ST *s1, 表达式值ST *pe)
{
    asm_expr_cmp(s1, pe);
}

静态函数 int asm_int_expr(虚拟机ST *s1)
{
    表达式值ST e;
    asm_expr(s1, &e);
    if (e.sym)
        应为("常量");
    return e.v;
}

static 符号ST* asm_new_label1(虚拟机ST *s1, int label, int is_local,
                           int ELF节数量, int value)
{
    符号ST *sym;
    ElfSym *esym;

    sym = asm_label_find(label);
    if (sym) {
	esym = elfsym(sym);
	/* A 存储类型_外部 symbol, even if it has a section is considered
	   overridable.  This is how we "define" .set targets.  Real
	   definitions won't have 存储类型_外部 set.  */
        if (esym && esym->st_shndx != SHN_未定义) {
            /* the label is already defined */
            if (IS_ASM_SYM(sym)
                && (is_local == 1 || (sym->类型.数据类型 & 存储类型_外部)))
                goto new_label;
            if (!(sym->类型.数据类型 & 存储类型_外部))
                错_误("汇编标签 '%s' 已经定义",
                          取单词字符串(label, NULL));
        }
    } else {
    new_label:
        sym = asm_label_push(label);
    }
    if (!sym->c)
      取外部符号2(sym, SHN_未定义, 0, 0, 1);
    esym = elfsym(sym);
    esym->st_shndx = ELF节数量;
    esym->st_value = value;
    if (is_local != 2)
        sym->类型.数据类型 &= ~存储类型_外部;
    return sym;
}

static 符号ST* asm_new_label(虚拟机ST *s1, int label, int is_local)
{
    return asm_new_label1(s1, label, is_local, 当前代码节->ELF节数量, 指令在代码节位置);
}

/* Set the value of LABEL to that of some expression (possibly
   involving other symbols).  LABEL can be overwritten later still.  */
static 符号ST* set_symbol(虚拟机ST *s1, int label)
{
    long n;
    表达式值ST e;
    符号ST *sym;
    ElfSym *esym;
    取下个单词();
    asm_expr(s1, &e);
    n = e.v;
    esym = elfsym(e.sym);
    if (esym)
	n += esym->st_value;
    sym = asm_new_label1(s1, label, 2, esym ? esym->st_shndx : SHN_ABS, n);
    elfsym(sym)->st_other |= ST_ASM_SET;
    return sym;
}

static void use_section1(虚拟机ST *s1, 节ST *sec)
{
    当前代码节->当前数据_偏移量 = 指令在代码节位置;
    当前代码节 = sec;
    指令在代码节位置 = 当前代码节->当前数据_偏移量;
}

static void use_section(虚拟机ST *s1, const char *name)
{
    节ST *sec;
    sec = 查找_节_创建(s1, name, 1);
    use_section1(s1, sec);
}

static void push_section(虚拟机ST *s1, const char *name)
{
    节ST *sec = 查找_节_创建(s1, name, 1);
    sec->上个 = 当前代码节;
    use_section1(s1, sec);
}

static void pop_section(虚拟机ST *s1)
{
    节ST *上个 = 当前代码节->上个;
    if (!上个)
        错_误(".popsection 没有 .pushsection");
    当前代码节->上个 = NULL;
    use_section1(s1, 上个);
}

static void asm_parse_directive(虚拟机ST *s1, int global)
{
    int n, offset, v, 大小, tok1;
    节ST *sec;
    uint8_t *ptr;

    /* assembler directive */
    sec = 当前代码节;
    switch(单词编码) {
    case TOK_ASMDIR_对齐:
    case TOK_ASMDIR_balign:
    case TOK_ASMDIR_p2align:
    case TOK_ASMDIR_跳过:
    case TOK_ASMDIR_space:
        tok1 = 单词编码;
        取下个单词();
        n = asm_int_expr(s1);
        if (tok1 == TOK_ASMDIR_p2align)
        {
            if (n < 0 || n > 30)
                错_误("无效的 p2align，必须在 0 到 30 之间");
            n = 1 << n;
            tok1 = TOK_ASMDIR_对齐;
        }
        if (tok1 == TOK_ASMDIR_对齐 || tok1 == TOK_ASMDIR_balign) {
            if (n < 0 || (n & (n-1)) != 0)
                错_误("对齐必须是 2 的正幂");
            offset = (指令在代码节位置 + n - 1) & -n;
            大小 = offset - 指令在代码节位置;
            /* 该节必须具有兼容的对齐方式 */
            if (sec->sh_地址对齐 < n)
                sec->sh_地址对齐 = n;
        } else {
	    if (n < 0)
	        n = 0;
            大小 = n;
        }
        v = 0;
        if (单词编码 == 英_逗号) {
            取下个单词();
            v = asm_int_expr(s1);
        }
    zero_pad:
        if (sec->sh_type != SHT_无数据) {
            sec->当前数据_偏移量 = 指令在代码节位置;
            ptr = 节_预留指定大小内存(sec, 大小);
            memset(ptr, v, 大小);
        }
        指令在代码节位置 += 大小;
        break;
    case TOK_ASMDIR_quad:
#ifdef 目标_X86_64
	大小 = 8;
	goto asm_data;
#else
        取下个单词();
        for(;;) {
            uint64_t vl;
            const char *p;

            p = 单词常量.字符串.数据;
            if (单词编码 != TOK_预处理数字) {
            error_constant:
                错_误("64位常量");
            }
            vl = strtoll(p, (char **)&p, 0);
            if (*p != '\0')
                goto error_constant;
            取下个单词();
            if (sec->sh_type != SHT_无数据) {
                /* XXX: endianness */
                gen_le32(vl);
                gen_le32(vl >> 32);
            } else {
                指令在代码节位置 += 8;
            }
            if (单词编码 != 英_逗号)
                break;
            取下个单词();
        }
        break;
#endif
    case TOK_ASMDIR_byte:
        大小 = 1;
        goto asm_data;
    case TOK_ASMDIR_word:
    case TOK_ASMDIR_short:
        大小 = 2;
        goto asm_data;
    case TOK_ASMDIR_long:
    case TOK_ASMDIR_int:
        大小 = 4;
    asm_data:
        取下个单词();
        for(;;) {
            表达式值ST e;
            asm_expr(s1, &e);
            if (sec->sh_type != SHT_无数据) {
                if (大小 == 4) {
                    gen_expr32(&e);
#ifdef 目标_X86_64
		} else if (大小 == 8) {
		    gen_expr64(&e);
#endif
                } else {
                    if (e.sym)
                        应为("常量");
                    if (大小 == 1)
                        g(e.v);
                    else
                        gen_le16(e.v);
                }
            } else {
                指令在代码节位置 += 大小;
            }
            if (单词编码 != 英_逗号)
                break;
            取下个单词();
        }
        break;
    case TOK_ASMDIR_fill:
        {
            int repeat, 大小, val, i, j;
            uint8_t repeat_buf[8];
            取下个单词();
            repeat = asm_int_expr(s1);
            if (repeat < 0) {
                错_误("repeat < 0; .fill 忽略");
                break;
            }
            大小 = 1;
            val = 0;
            if (单词编码 == 英_逗号) {
                取下个单词();
                大小 = asm_int_expr(s1);
                if (大小 < 0) {
                    错_误("大小 < 0; .fill 忽略");
                    break;
                }
                if (大小 > 8)
                    大小 = 8;
                if (单词编码 == 英_逗号) {
                    取下个单词();
                    val = asm_int_expr(s1);
                }
            }
            /* XXX: endianness */
            repeat_buf[0] = val;
            repeat_buf[1] = val >> 8;
            repeat_buf[2] = val >> 16;
            repeat_buf[3] = val >> 24;
            repeat_buf[4] = 0;
            repeat_buf[5] = 0;
            repeat_buf[6] = 0;
            repeat_buf[7] = 0;
            for(i = 0; i < repeat; i++) {
                for(j = 0; j < 大小; j++) {
                    g(repeat_buf[j]);
                }
            }
        }
        break;
    case TOK_ASMDIR_rept:
        {
            int repeat;
            单词字符串ST *init_str;
            取下个单词();
            repeat = asm_int_expr(s1);
            init_str = 单词字符串_分配();
            while (取下个单词(), 单词编码 != TOK_ASMDIR_endr) {
                if (单词编码 == 中_文件结尾)
                    错_误("我们在文件末尾找不到 .endr ");
                在单词字符串s中添加当前解析单词(init_str);
            }
            单词字符串_增加(init_str, -1);
            单词字符串_增加(init_str, 0);
            开始_宏扩展(init_str, 1);
            while (repeat-- > 0) {
                汇编_内部(s1, (解析标志 & 解析标志_预处理),
				      global);
                没宏替换的字符串指针 = init_str->字符串;
            }
            结束_宏扩展();
            取下个单词();
            break;
        }
    case TOK_ASMDIR_org:
        {
            unsigned long n;
	    表达式值ST e;
	    ElfSym *esym;
            取下个单词();
	    asm_expr(s1, &e);
	    n = e.v;
	    esym = elfsym(e.sym);
	    if (esym) {
		if (esym->st_shndx != 当前代码节->ELF节数量)
		  应为("常量或相同段（节）符号");
		n += esym->st_value;
	    }
            if (n < 指令在代码节位置)
                错_误("向后尝试 .org ");
            v = 0;
            大小 = n - 指令在代码节位置;
            goto zero_pad;
        }
        break;
    case TOK_ASMDIR_set:
	取下个单词();
	tok1 = 单词编码;
	取下个单词();
	/* Also accept '.set stuff', but don't do anything with this.
	   It's used in GAS to set various features like '.set mips16'.  */
	if (单词编码 == 英_逗号)
	    set_symbol(s1, tok1);
	break;
    case TOK_ASMDIR_globl:
    case TOK_ASMDIR_global:
    case TOK_ASMDIR_weak:
    case TOK_ASMDIR_hidden:
	tok1 = 单词编码;
	do { 
            符号ST *sym;
            取下个单词();
            sym = get_asm_sym(单词编码, NULL);
	    if (tok1 != TOK_ASMDIR_hidden)
                sym->类型.数据类型 &= ~存储类型_静态;
            if (tok1 == TOK_ASMDIR_weak)
                sym->a.weak = 1;
	    else if (tok1 == TOK_ASMDIR_hidden)
	        sym->a.能见度 = STV_HIDDEN;
            update_storage(sym);
            取下个单词();
	} while (单词编码 == 英_逗号);
	break;
    case TOK_ASMDIR_string:
    case TOK_ASMDIR_ascii:
    case TOK_ASMDIR_asciz:
        {
            const uint8_t *p;
            int i, 大小, t;

            t = 单词编码;
            取下个单词();
            for(;;)
            {
                if (单词编码 != TOK_字符串常量)
                    应为("字符串常量");
                p = 单词常量.字符串.数据;
                大小 = 单词常量.字符串.大小;
                if (t == TOK_ASMDIR_ascii && 大小 > 0)
                    大小--;
                for(i = 0; i < 大小; i++)
                    g(p[i]);
                取下个单词();
                if (单词编码 == 英_逗号) {
                    取下个单词();
                } else if (单词编码 != TOK_字符串常量) {
                    break;
                }
            }
	}
	break;
    case TOK_ASMDIR_代码:
    case TOK_ASMDIR_数据:
    case TOK_ASMDIR_bss:
	{ 
            char sname[64];
            tok1 = 单词编码;
            n = 0;
            取下个单词();
            if (单词编码 != 英_分号 && 单词编码 != 英_换行) {
		n = asm_int_expr(s1);
		取下个单词();
            }
            if (n)
                sprintf(sname, "%s%d", 取单词字符串(tok1, NULL), n);
            else
                sprintf(sname, "%s", 取单词字符串(tok1, NULL));
            use_section(s1, sname);
	}
	break;
    case TOK_ASMDIR_文件:
        {
            char 文件名[512];

            文件名[0] = '\0';
            取下个单词();
            if (单词编码 == TOK_字符串常量)
                截取后拼接(文件名, sizeof(文件名), 单词常量.字符串.数据);
            else
                截取后拼接(文件名, sizeof(文件名), 取单词字符串(单词编码, NULL));
            zhi_警告_c(警告_不支持)("忽略 .文件 %s", 文件名);
            取下个单词();
        }
        break;
    case TOK_ASMDIR_ident:
        {
            char ident[256];

            ident[0] = '\0';
            取下个单词();
            if (单词编码 == TOK_字符串常量)
                截取后拼接(ident, sizeof(ident), 单词常量.字符串.数据);
            else
                截取后拼接(ident, sizeof(ident), 取单词字符串(单词编码, NULL));
            zhi_警告_c(警告_不支持)("忽略 .ident %s", ident);
            取下个单词();
        }
        break;
    case TOK_ASMDIR_大小:
        { 
            符号ST *sym;

            取下个单词();
            sym = asm_label_find(单词编码);
            if (!sym) {
                错_误("没有找到标签： %s", 取单词字符串(单词编码, NULL));
            }
            /* XXX .大小 name,label2-label1 */
            zhi_警告_c(警告_不支持)("忽略 .大小 %s,*", 取单词字符串(单词编码, NULL));
            取下个单词();
            跳过(英_逗号);
            while (单词编码 != 英_换行 && 单词编码 != 英_分号 && 单词编码 != 中_文件结尾) {
                取下个单词();
            }
        }
        break;
    case TOK_ASMDIR_类型:
        { 
            符号ST *sym;
            const char *newtype;

            取下个单词();
            sym = get_asm_sym(单词编码, NULL);
            取下个单词();
            跳过(英_逗号);
            if (单词编码 == TOK_字符串常量) {
                newtype = 单词常量.字符串.数据;
            } else {
                if (单词编码 == '@' || 单词编码 == '%')
                    取下个单词();
                newtype = 取单词字符串(单词编码, NULL);
            }

            if (!strcmp(newtype, "function") || !strcmp(newtype, "STT_FUNC")) {
                sym->类型.数据类型 = (sym->类型.数据类型 & ~数据类型_基本类型) | 数据类型_函数;
            } else
                zhi_警告_c(警告_不支持)("已忽略将'%s'的类型从 0x%x 更改为 '%s' ",
                    取单词字符串(sym->v, NULL), sym->类型.数据类型, newtype);

            取下个单词();
        }
        break;
    case TOK_ASMDIR_pushsection:
    case TOK_ASMDIR_section:
        {
            char sname[256];
	    int old_nb_section = s1->节的数量;

	    tok1 = 单词编码;
            /* XXX: support more options */
            取下个单词();
            sname[0] = '\0';
            while (单词编码 != 英_分号 && 单词编码 != 英_换行 && 单词编码 != 英_逗号) {
                if (单词编码 == TOK_字符串常量)
                    截取后拼接(sname, sizeof(sname), 单词常量.字符串.数据);
                else
                    截取后拼接(sname, sizeof(sname), 取单词字符串(单词编码, NULL));
                取下个单词();
            }
            if (单词编码 == 英_逗号) {
                /* 跳过 section options */
                取下个单词();
                if (单词编码 != TOK_字符串常量)
                    应为("字符串常量");
                取下个单词();
                if (单词编码 == 英_逗号) {
                    取下个单词();
                    if (单词编码 == '@' || 单词编码 == '%')
                        取下个单词();
                    取下个单词();
                }
            }
            last_text_section = 当前代码节;
	    if (tok1 == TOK_ASMDIR_section)
	        use_section(s1, sname);
	    else
	        push_section(s1, sname);
	    /* If we just allocated a new section reset its alignment to
	       1.  节_新建 normally acts for GCC compatibility and
	       sets alignment to 宏_指针大小.  The assembler behaves different. */
	    if (old_nb_section != s1->节的数量)
	        当前代码节->sh_地址对齐 = 1;
        }
        break;
    case TOK_ASMDIR_previous:
        { 
            节ST *sec;
            取下个单词();
            if (!last_text_section)
                错_误("没有引用上一节");
            sec = 当前代码节;
            use_section1(s1, last_text_section);
            last_text_section = sec;
        }
        break;
    case TOK_ASMDIR_popsection:
	取下个单词();
	pop_section(s1);
	break;
#ifdef 目标_I386
    case TOK_ASMDIR_code16:
        {
            取下个单词();
            s1->分割大小 = 16;
        }
        break;
    case TOK_ASMDIR_code32:
        {
            取下个单词();
            s1->分割大小 = 32;
        }
        break;
#endif
#ifdef 目标_X86_64
    /* added for compatibility with GAS */
    case TOK_ASMDIR_code64:
        取下个单词();
        break;
#endif
    default:
        错_误("未知汇编程序指令 '.%s'", 取单词字符串(单词编码, NULL));
        break;
    }
}


/* assemble a 文件 */
static int 汇编_内部(虚拟机ST *s1, int 执行_预处理, int global)
{
    int opcode;
    int 保存_解析标志 = 解析标志;

    解析标志 = 解析标志_汇编文件 | 解析标志_单词字符串;
    if (执行_预处理)
        解析标志 |= 解析标志_预处理;
    for(;;) {
        取下个单词();
        if (单词编码 == TOK_文件结尾)
            break;
        解析标志 |= 解析标志_换行; /* XXX: suppress that hack */
    重做:
        if (单词编码 == '#') {
            /* horrible gas comment */
            while (单词编码 != 英_换行)
                取下个单词();
        } else if (单词编码 >= TOK_ASMDIR_FIRST && 单词编码 <= TOK_ASMDIR_LAST) {
            asm_parse_directive(s1, global);
        } else if (单词编码 == TOK_预处理数字) {
            const char *p;
            int n;
            p = 单词常量.字符串.数据;
            n = strtoul(p, (char **)&p, 10);
            if (*p != '\0')
                应为("英_冒号");
            /* new local label */
            asm_new_label(s1, asm_get_local_label_name(s1, n), 1);
            取下个单词();
            跳过(英_冒号);
            goto 重做;
        } else if (单词编码 >= TOK_标识符) {
            /* instruction or label */
            opcode = 单词编码;
            取下个单词();
            if (单词编码 == 英_冒号) {
                /* new label */
                asm_new_label(s1, opcode, 0);
                取下个单词();
                goto 重做;
            } else if (单词编码 == '=') {
		set_symbol(s1, opcode);
                goto 重做;
            } else {
                asm_opcode(s1, opcode);
            }
        }
        /* end of line */
        if (单词编码 != 英_分号 && 单词编码 != 英_换行)
            应为("行尾");
        解析标志 &= ~解析标志_换行; /* XXX: suppress that hack */
    }

    解析标志 = 保存_解析标志;
    return 0;
}

静态函数 int 汇编当前文件(虚拟机ST *s1, int 执行_预处理)
{
    int ret;
    翻译单元信息开始(s1);
    /* 默认节是代码节 */
    当前代码节 = 代码_节;
    指令在代码节位置 = 当前代码节->当前数据_偏移量;
    无需生成代码 = 0;
    ret = 汇编_内部(s1, 执行_预处理, 1);
    当前代码节->当前数据_偏移量 = 指令在代码节位置;
    翻译单元信息结束(s1);
    return ret;
}

/********************************************************************/
/* GCC inline asm support */

/* assemble the string '字符串' in the current C compilation unit without
   C preprocessing. NOTE: 字符串 is modified by modifying the '\0' at the
   end */
static void 内联_汇编(虚拟机ST *s1, char *字符串, int 长度, int global)
{
    const int *保存_宏_字符串指针 = 没宏替换的字符串指针;
    int dotid = 设置标识符('.', 是_标识符);
    int dolid = 设置标识符('$', 0);

    新建源文件缓冲区(s1, ":asm:", 长度);
    memcpy(文件->缓冲, 字符串, 长度);
    没宏替换的字符串指针 = NULL;
    汇编_内部(s1, 0, global);
    关闭打开的文件();

    设置标识符('$', dolid);
    设置标识符('.', dotid);
    没宏替换的字符串指针 = 保存_宏_字符串指针;
}

/* find a constraint by its number or id (gcc 3 extended
   syntax). return -1 if not found. Return in *pp in char after the
   constraint */
静态函数 int find_constraint(汇编操作数ST *operands, int nb_operands, 
                           const char *name, const char **pp)
{
    int index;
    单词ST *ts;
    const char *p;

    if (是数字(*name)) {
        index = 0;
        while (是数字(*name)) {
            index = (index * 10) + (*name) - '0';
            name++;
        }
        if ((unsigned)index >= nb_operands)
            index = -1;
    } else if (*name == 英_左中括号) {
        name++;
        p = strchr(name, 英_右中括号);
        if (p) {
            ts = 插入单词(name, p - name);
            for(index = 0; index < nb_operands; index++) {
                if (operands[index].id == ts->单词编码)
                    goto found;
            }
            index = -1;
        found:
            name = p + 1;
        } else {
            index = -1;
        }
    } else {
        index = -1;
    }
    if (pp)
        *pp = name;
    return index;
}

static void subst_asm_operands(汇编操作数ST *operands, int nb_operands, 
                               动态字符串ST *out_str, 动态字符串ST *in_str)
{
    int c, index, modifier;
    const char *字符串;
    汇编操作数ST *op;
    栈值ST sv;

    动态字符串_新建(out_str);
    字符串 = in_str->数据;
    for(;;) {
        c = *字符串++;
        if (c == '%') {
            if (*字符串 == '%') {
                字符串++;
                goto 动态字符串_增加字符;
            }
            modifier = 0;
            if (*字符串 == 'c' || *字符串 == 'n' ||
                *字符串 == 'b' || *字符串 == 'w' || *字符串 == 'h' || *字符串 == 'k' ||
		*字符串 == 'q' ||
		/* P in GCC would add "@PLT" to symbol refs in PIC mode,
		   and make literal operands not be decorated with '$'.  */
		*字符串 == 'P')
                modifier = *字符串++;
            index = find_constraint(operands, nb_operands, 字符串, &字符串);
            if (index < 0)
                错_误("%%之后的操作数引用无效");
            op = &operands[index];
            sv = *op->vt;
            if (op->reg >= 0) {
                sv.寄存qi = op->reg;
                if ((op->vt->寄存qi & 存储类型_掩码) == 存储类型_LLOCAL && op->is_memory)
                    sv.寄存qi |= 存储类型_左值;
            }
            subst_asm_operand(out_str, &sv, modifier);
        } else {
        动态字符串_增加字符:
            动态字符串_追加一个字符(out_str, c);
            if (c == '\0')
                break;
        }
    }
}


static void parse_asm_operands(汇编操作数ST *operands, int *nb_operands_ptr,
                               int is_output)
{
    汇编操作数ST *op;
    int nb_operands;

    if (单词编码 != 英_冒号) {
        nb_operands = *nb_operands_ptr;
        for(;;) {
	    动态字符串ST astr;
            if (nb_operands >= MAX_ASM_OPERANDS)
                错_误("asm操作数太多");
            op = &operands[nb_operands++];
            op->id = 0;
            if (单词编码 == 英_左中括号) {
                取下个单词();
                if (单词编码 < TOK_标识符)
                    应为("标识符");
                op->id = 单词编码;
                取下个单词();
                跳过(英_右中括号);
            }
	    解析多个字符串(&astr, "字符串常量");
            op->constraint = zhi_分配内存(astr.大小);
            strcpy(op->constraint, astr.数据);
	    动态字符串_释放(&astr);
            跳过(英_左小括号);
            生成表达式();
            if (is_output) {
                if (!(栈顶值->类型.数据类型 & 数据类型_数组))
                    测试_左值();
            } else {
                /* we want to avoid LLOCAL case, except when the 'm'
                   constraint is used. Note that it may come from
                   register 贮存, so we need to convert (reg)
                   case */
                if ((栈顶值->寄存qi & 存储类型_左值) &&
                    ((栈顶值->寄存qi & 存储类型_掩码) == 存储类型_LLOCAL ||
                     (栈顶值->寄存qi & 存储类型_掩码) < 存储类型_VC常量) &&
                    !strchr(op->constraint, 'm')) {
                    生成值(RC_INT);
                }
            }
            op->vt = 栈顶值;
            跳过(英_右小括号);
            if (单词编码 == 英_逗号) {
                取下个单词();
            } else {
                break;
            }
        }
        *nb_operands_ptr = nb_operands;
    }
}

/* parse the GCC asm() instruction */
静态函数 void asm_instr(void)
{
    动态字符串ST astr, astr1;
    汇编操作数ST operands[MAX_ASM_OPERANDS];
    int nb_outputs, nb_operands, i, must_subst, out_reg;
    uint8_t clobber_regs[NB_ASM_REGS];
    节ST *sec;

    /* since we always generate the asm() instruction, we can ignore
       volatile */
    if (单词编码 == 英_易变 || 单词编码 == 中_易变) {
        取下个单词();
    }
    解析_汇编_字符串(&astr);
    nb_operands = 0;
    nb_outputs = 0;
    must_subst = 0;
    memset(clobber_regs, 0, sizeof(clobber_regs));
    if (单词编码 == 英_冒号) {
        取下个单词();
        must_subst = 1;
        /* output args */
        parse_asm_operands(operands, &nb_operands, 1);
        nb_outputs = nb_operands;
        if (单词编码 == 英_冒号) {
            取下个单词();
            if (单词编码 != 英_右小括号) {
                /* input args */
                parse_asm_operands(operands, &nb_operands, 0);
                if (单词编码 == 英_冒号) {
                    /* clobber list */
                    /* XXX: handle registers */
                    取下个单词();
                    for(;;) {
                        if (单词编码 != TOK_字符串常量)
                            应为("字符串常量");
                        asm_clobber(clobber_regs, 单词常量.字符串.数据);
                        取下个单词();
                        if (单词编码 == 英_逗号) {
                            取下个单词();
                        } else {
                            break;
                        }
                    }
                }
            }
        }
    }
    跳过(英_右小括号);
    /* NOTE: we do not eat the 英_分号 so that we can restore the current
       token after the assembler parsing */
    if (单词编码 != 英_分号)
        应为("英_分号");
    
    /* save all values in the memory */
    save_regs(0);

    /* compute constraints */
    asm_计算约束(operands, nb_operands, nb_outputs, 
                            clobber_regs, &out_reg);

    /* substitute the operands in the asm string. No substitution is
       完成 if no operands (GCC behaviour) */
#ifdef ASM_DEBUG
    printf("asm: \"%s\"\n", (char *)astr.数据);
#endif
    if (must_subst) {
        subst_asm_operands(operands, nb_operands, &astr1, &astr);
        动态字符串_释放(&astr);
    } else {
        astr1 = astr;
    }
#ifdef ASM_DEBUG
    printf("subst_asm: \"%s\"\n", (char *)astr1.数据);
#endif

    /* generate loads */
    asm_gen_code(operands, nb_operands, nb_outputs, 0, 
                 clobber_regs, out_reg);    

    /* We don't allow switching section within inline asm to
       bleed out to surrounding code.  */
    sec = 当前代码节;
    /* assemble the string with zhi internal assembler */
    内联_汇编(全局虚拟机, astr1.数据, astr1.大小 - 1, 0);
    if (sec != 当前代码节) {
        zhi_警告("内联asm尝试更改当前节");
        use_section1(全局虚拟机, sec);
    }

    /* restore the current C token */
    取下个单词();

    /* store the output values if needed */
    asm_gen_code(operands, nb_operands, nb_outputs, 1, 
                 clobber_regs, out_reg);
    
    /* free everything */
    for(i=0;i<nb_operands;i++) {
        汇编操作数ST *op;
        op = &operands[i];
        zhi_释放(op->constraint);
        弹出堆栈值();
    }
    动态字符串_释放(&astr1);
}

静态函数 void asm_全局_instr(void)
{
    动态字符串ST astr;
    int 保存_无需代码_生成 = 无需生成代码;

    /* 始终发出全局 asm 块 */
    无需生成代码 = 0;
    取下个单词();
    解析_汇编_字符串(&astr);
    跳过(英_右小括号);
    /* 注意：我们不吃英_分号，以便我们可以在汇编程序解析后恢复当前令牌 */
    if (单词编码 != 英_分号)
        应为("英_分号");
    
#ifdef ASM_DEBUG
    printf("asm_global: \"%s\"\n", (char *)astr.数据);
#endif
    当前代码节 = 代码_节;
    指令在代码节位置 = 当前代码节->当前数据_偏移量;

    /* 使用 zhi 内部汇编器组装字符串 */
    内联_汇编(全局虚拟机, astr.数据, astr.大小 - 1, 1);
    
    当前代码节->当前数据_偏移量 = 指令在代码节位置;

    /* 恢复当前的 C 单词 */
    取下个单词();

    动态字符串_释放(&astr);
    无需生成代码 = 保存_无需代码_生成;
}

/********************************************************/
#else
静态函数 int 汇编当前文件(虚拟机ST *s1, int 执行_预处理)
{
    错_误("不支持asm");
}

静态函数 void asm_instr(void)
{
    错_误("不支持内联asm()");
}

静态函数 void asm_全局_instr(void)
{
    错_误("不支持内联asm()");
}
#endif /* CONFIG_ZHI_ASM */
