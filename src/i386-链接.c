
#ifdef TARGET_DEFS_ONLY

#define EM_目标机 EM_386

/* relocation 类型 for 32 bit 数据 relocation */
#define R_DATA_32   R_386_32
#define R_DATA_PTR  R_386_32
#define R_JMP_SLOT  R_386_JMP_SLOT
#define R_GLOB_DAT  R_386_GLOB_DAT
#define R_COPY      R_386_COPY
#define R_RELATIVE  R_386_RELATIVE

#define R_NUM       R_386_NUM

#define ELF_START_ADDR 0x08048000
#define ELF_PAGE_SIZE  0x1000

#define PCRELATIVE_DLLPLT 0
#define RELOCATE_DLLPLT 1

#else /* !TARGET_DEFS_ONLY */

#include "zhi.h"

#ifndef ELF_OBJ_ONLY
/* Returns 1 for a code relocation, 0 for a 数据 relocation. For unknown
   relocations, returns -1. */
int code_reloc (int reloc_type)
{
    switch (reloc_type) {
	case R_386_RELATIVE:
	case R_386_16:
        case R_386_32:
	case R_386_GOTPC:
	case R_386_GOTOFF:
	case R_386_GOT32:
	case R_386_GOT32X:
	case R_386_GLOB_DAT:
	case R_386_COPY:
	case R_386_TLS_GD:
	case R_386_TLS_LDM:
	case R_386_TLS_LDO_32:
	case R_386_TLS_LE:
            return 0;

	case R_386_PC16:
	case R_386_PC32:
	case R_386_PLT32:
	case R_386_JMP_SLOT:
            return 1;
    }
    return -1;
}

/* Returns an enumerator to describe whether and when the relocation needs a
   GOT and/or PLT entry to be created. See zhi.h for a description of the
   different values. */
int gotplt_entry_type (int reloc_type)
{
    switch (reloc_type) {
	case R_386_RELATIVE:
	case R_386_16:
	case R_386_GLOB_DAT:
	case R_386_JMP_SLOT:
	case R_386_COPY:
            return NO_GOTPLT_ENTRY;

        case R_386_32:
	    /* This relocations shouldn't normally need GOT or PLT
	       slots if it weren't for simplicity in the code generator.
	       See our caller for comments.  */
            return AUTO_GOTPLT_ENTRY;

	case R_386_PC16:
	case R_386_PC32:
            return AUTO_GOTPLT_ENTRY;

	case R_386_GOTPC:
	case R_386_GOTOFF:
            return BUILD_GOT_ONLY;

	case R_386_GOT32:
	case R_386_GOT32X:
	case R_386_PLT32:
	case R_386_TLS_GD:
	case R_386_TLS_LDM:
	case R_386_TLS_LDO_32:
	case R_386_TLS_LE:
            return ALWAYS_GOTPLT_ENTRY;
    }
    return -1;
}

静态函数 unsigned create_plt_entry(虚拟机ST *s1, unsigned got_偏移, struct 额外符号属性S *attr)
{
    节ST *plt = s1->plt;
    uint8_t *p;
    int modrm;
    unsigned plt_偏移, relofs;

    /* on i386 if we build a DLL, we add a %ebx offset */
    if (s1->输出类型 == 输出_DLL)
        modrm = 0xa3;
    else
        modrm = 0x25;

    /* empty PLT: create PLT0 entry that pushes the library identifier
       (GOT + 宏_指针大小) and jumps to ld.so resolution routine
       (GOT + 2 * 宏_指针大小) */
    if (plt->当前数据_偏移量 == 0) {
        p = 节_预留指定大小内存(plt, 16);
        p[0] = 0xff; /* pushl got + 宏_指针大小 */
        p[1] = modrm + 0x10;
        write32le(p + 2, 宏_指针大小);
        p[6] = 0xff; /* jmp *(got + 宏_指针大小 * 2) */
        p[7] = modrm;
        write32le(p + 8, 宏_指针大小 * 2);
    }
    plt_偏移 = plt->当前数据_偏移量;

    /* The PLT slot refers to the relocation entry it needs via offset.
       The 重定位 entry is created below, so its offset is the current
       当前数据_偏移量 */
    relofs = s1->plt->重定位 ? s1->plt->重定位->当前数据_偏移量 : 0;

    /* Jump to GOT entry where ld.so initially put the address of ip + 4 */
    p = 节_预留指定大小内存(plt, 16);
    p[0] = 0xff; /* jmp *(got + x) */
    p[1] = modrm;
    write32le(p + 2, got_偏移);
    p[6] = 0x68; /* push $xxx */
    write32le(p + 7, relofs - sizeof (ElfW_Rel));
    p[11] = 0xe9; /* jmp plt_start */
    write32le(p + 12, -(plt->当前数据_偏移量));
    return plt_偏移;
}

/* 重新定位 the PLT: compute addresses and offsets in the PLT now that final
   address for PLT and GOT are known (see fill_program_header) */
静态函数 void relocate_plt(虚拟机ST *s1)
{
    uint8_t *p, *p_end;

    if (!s1->plt)
      return;

    p = s1->plt->数据;
    p_end = p + s1->plt->当前数据_偏移量;

    if (s1->输出类型 != 输出_DLL && p < p_end) {
        add32le(p + 2, s1->got->sh_addr);
        add32le(p + 8, s1->got->sh_addr);
        p += 16;
        while (p < p_end) {
            add32le(p + 2, s1->got->sh_addr);
            p += 16;
        }
    }

    if (s1->plt->重定位) {
        ElfW_Rel *rel;
        int x = s1->plt->sh_addr + 16 + 6;
        p = s1->got->数据;
        for_each_elem(s1->plt->重定位, 0, rel, ElfW_Rel) {
            write32le(p + rel->r_offset, x);
            x += 16;
        }
    }
}
#endif

void 重新定位(虚拟机ST *s1, ElfW_Rel *rel, int 类型, unsigned char *ptr, addr_t addr, addr_t val)
{
    int 符号_索引, esym_index;

    符号_索引 = ELFW(R_SYM)(rel->r_info);

    switch (类型) {
        case R_386_32:
            if (s1->输出类型 == 输出_DLL) {
                esym_index = 获取符号属性(s1, 符号_索引, 0)->dyn_索引;
                qrel->r_offset = rel->r_offset;
                if (esym_index) {
                    qrel->r_info = ELFW(R_INFO)(esym_index, R_386_32);
                    qrel++;
                    return;
                } else {
                    qrel->r_info = ELFW(R_INFO)(0, R_386_RELATIVE);
                    qrel++;
                }
            }
            add32le(ptr, val);
            return;
        case R_386_PC32:
            if (s1->输出类型 == 输出_DLL) {
                /* DLL relocation */
                esym_index = 获取符号属性(s1, 符号_索引, 0)->dyn_索引;
                if (esym_index) {
                    qrel->r_offset = rel->r_offset;
                    qrel->r_info = ELFW(R_INFO)(esym_index, R_386_PC32);
                    qrel++;
                    return;
                }
            }
            add32le(ptr, val - addr);
            return;
        case R_386_PLT32:
            add32le(ptr, val - addr);
            return;
        case R_386_GLOB_DAT:
        case R_386_JMP_SLOT:
            write32le(ptr, val);
            return;
        case R_386_GOTPC:
            add32le(ptr, s1->got->sh_addr - addr);
            return;
        case R_386_GOTOFF:
            add32le(ptr, val - s1->got->sh_addr);
            return;
        case R_386_GOT32:
        case R_386_GOT32X:
            /* we load the got offset */
            add32le(ptr, 获取符号属性(s1, 符号_索引, 0)->got_偏移);
            return;
        case R_386_16:
            if (s1->输出格式 != 输出格式_BINARY) {
            output_file:
                错_误("只能生成 16 位二进制文件");
            }
            write16le(ptr, read16le(ptr) + val);
            return;
        case R_386_PC16:
            if (s1->输出格式 != 输出格式_BINARY)
                goto output_file;
            write16le(ptr, read16le(ptr) + val - addr);
            return;
        case R_386_RELATIVE:
#ifdef 目标_PE
            add32le(ptr, val - s1->pe_优先装载地址);
#endif
            /* do nothing */
            return;
        case R_386_COPY:
            /* This relocation must copy initialized 数据 from the library
            to the program .bss segment. Currently made like for ARM
            (to remove noise of default case). Is this true?
            */
            return;
        case R_386_TLS_GD:
            {
                static const unsigned char 应为[] = {
                    /* lea 0(,%ebx,1),%eax */
                    0x8d, 0x04, 0x1d, 0x00, 0x00, 0x00, 0x00,
                    /* call __tls_get_addr@PLT */
                    0xe8, 0xfc, 0xff, 0xff, 0xff };
                static const unsigned char replace[] = {
                    /* mov %gs:0,%eax */
                    0x65, 0xa1, 0x00, 0x00, 0x00, 0x00,
                    /* sub 0,%eax */
                    0x81, 0xe8, 0x00, 0x00, 0x00, 0x00 };

                if (memcmp (ptr-3, 应为, sizeof(应为)) == 0) {
                    ElfW(Sym) *sym;
                    节ST *sec;
                    int32_t x;

                    memcpy(ptr-3, replace, sizeof(replace));
                    rel[1].r_info = ELFW(R_INFO)(0, R_386_NONE);
                    sym = &((ElfW(Sym) *)节符号表_数组->数据)[符号_索引];
                    sec = s1->节数组[sym->st_shndx];
                    x = sym->st_value - sec->sh_addr - sec->当前数据_偏移量;
                    add32le(ptr + 5, -x);
                }
                else
                    错_误("意外的 R_386_TLS_GD 模式");
            }
            return;
        case R_386_TLS_LDM:
            {
                static const unsigned char 应为[] = {
                    /* lea 0(%ebx),%eax */
                    0x8d, 0x83, 0x00, 0x00, 0x00, 0x00,
                    /* call __tls_get_addr@PLT */
                    0xe8, 0xfc, 0xff, 0xff, 0xff };
                static const unsigned char replace[] = {
                    /* mov %gs:0,%eax */
                    0x65, 0xa1, 0x00, 0x00, 0x00, 0x00,
                    /* nop */
                    0x90,
                    /* lea 0(%esi,%eiz,1),%esi */
                    0x8d, 0x74, 0x26, 0x00 };

                if (memcmp (ptr-2, 应为, sizeof(应为)) == 0) {
                    memcpy(ptr-2, replace, sizeof(replace));
                    rel[1].r_info = ELFW(R_INFO)(0, R_386_NONE);
                }
                else
                    错_误("意外的 R_386_TLS_LDM 模式");
            }
            return;
        case R_386_TLS_LDO_32:
        case R_386_TLS_LE:
            {
                ElfW(Sym) *sym;
                节ST *sec;
                int32_t x;

                sym = &((ElfW(Sym) *)节符号表_数组->数据)[符号_索引];
                sec = s1->节数组[sym->st_shndx];
                x = val - sec->sh_addr - sec->当前数据_偏移量;
                add32le(ptr, x);
            }
            return;
        case R_386_NONE:
            return;
        default:
            fprintf(stderr,"FIXME:句柄重定位类型 %d 位于 %x [%p] 到 %x\n",
                类型, (unsigned)addr, ptr, (unsigned)val);
            return;
    }
}

#endif /* !TARGET_DEFS_ONLY */
