/****************************************************************************************************
 * 项目：核心库
 * 描述：核心库允许你把zhi作为动态代码生成的后端。
 ****************************************************************************************************/
#if !defined 是源码M || 是源码M
#include "词法分析.c"
#include "语法分析.c"
#include "汇编.c"
#include "Elf.c"
#include "运行.c"
#ifdef 目标_I386
#include "i386-生成.c"
#include "i386-链接.c"
#include "i386-汇编.c"
#elif defined(目标_ARM)
#include "arm-生成.c"
#include "arm-链接.c"
#include "arm-汇编.c"
#elif defined(目标_ARM64)
#include "arm64-生成.c"
#include "arm64-链接.c"
#include "arm-汇编.c"
#elif defined(目标_C67)
#include "c67-生成.c"
#include "c67-链接.c"
#include "Coff.c"
#elif defined(目标_X86_64)
#include "x86_64-生成.c"
#include "x86_64-链接.c"
#include "i386-汇编.c"
#elif defined(目标_RISCV64)
#include "riscv64-生成.c"
#include "riscv64-链接.c"
#include "riscv64-汇编.c"
#else
#error 未知目标机
#endif
#ifdef 目标_PE
#include "Pe.c"
#endif
#ifdef 目标_MACHO
#include "Macho.c"
#endif
#endif /*  !defined 是源码M || 是源码M 结束 */

#include "zhi.h"

/********************************************************/
/* 全局变量*/

/* XXX: 尽快摆脱这个（或者可能不） */
静态数据 虚拟机ST *全局虚拟机;
ZHI_信号量(static zhi_编译_信号量);

#ifdef 内存_调试
static int 虚拟机数量;
#endif

/********************************************************/
#ifdef _WIN32
静态函数 char *斜杠规范(char *路径)
{
    char *p;
    for (p = 路径; *p; ++p)
        if (*p == '\\')
            *p = '/';
    return 路径;
}

#if defined 核心作为动态库M && !defined 编译文件夹M
static HMODULE zhi_module;
BOOL WINAPI DllMain (HINSTANCE hDll, DWORD dwReason, LPVOID lpReserved)
{
    if (DLL_PROCESS_ATTACH == dwReason)
        zhi_module = hDll;
    return TRUE;
}
#else
#define zhi_module NULL /* NULL means executable itself */
#endif

#ifndef 编译文件夹M
/* 在win32上，我们假设lib和includes位于'zhi.exe'的位置 */
static inline char *配置_编译文件夹_w32(char *path)
{
    char *p;
    GetModuleFileName(zhi_module, path, MAX_PATH);
    p = 取文件名含扩展名(斜杠规范(strlwr(path)));
    if (p > path)
        --p;
    *p = 0;
    return path;
}
#define 编译文件夹M 配置_编译文件夹_w32(alloca(MAX_PATH))
#endif

#ifdef 目标_PE
static void zhi_添加系统文件夹(虚拟机ST *s)
{
    char buf[1000];
    GetSystemDirectory(buf, sizeof buf);
    zhi_添加库路径(s, 斜杠规范(buf));
}
#endif
#endif

/********************************************************/
#if 配置_信号量_锁
#if defined _WIN32
静态函数 void 等待_信号量(ZHI信号量S *p)/*等待信号,获取拥有权*/
{
    if (!p->init)
        InitializeCriticalSection(&p->cr), p->init = 1;/*初始化临界区*/
    EnterCriticalSection(&p->cr);                      /*进入临界区*/
}
静态函数 void 发出_信号量(ZHI信号量S *p)/*发出信号即释放拥有权*/
{
    LeaveCriticalSection(&p->cr);/*离开临界区*/
}
#elif defined __APPLE__
/* Half-compatible MacOS doesn't have non-shared (process local)
   semaphores.  Use the dispatch framework for lightweight locks.  */
静态函数 void 等待_信号量(ZHI信号量S *p)/*等待信号,获取拥有权*/
{
    if (!p->init)
        p->sem = 分派_信号量_创建(1), p->init = 1;
    分派_信号量_等待(p->sem, 调度_时间_永远);
}
静态函数 void 发出_信号量(ZHI信号量S *p)/*发出信号即释放拥有权*/
{
    发送_信号量_信号(p->sem);
}
#else
静态函数 void 等待_信号量(ZHI信号量S *p)/*等待信号,获取拥有权*/
{
    if (!p->init)
        信号量_初始化(&p->sem, 0, 1), p->init = 1;
    while (信号量_等待(&p->sem) < 0 && errno == EINTR);
}
静态函数 void 发出_信号量(ZHI信号量S *p)/*发出信号即释放拥有权*/
{
    信号量_发出(&p->sem);
}
#endif
#endif

公用函数 void 进入编译状态(虚拟机ST *s1)
{
    if (s1->错误_设置_jmp_已启用)
    {
    	return;
    }
    WAIT_SEM(&zhi_编译_信号量);
    全局虚拟机 = s1;
}

公用函数 void 退出_虚拟机ST(虚拟机ST *s1)
{
    if (s1->错误_设置_jmp_已启用)
    {
    	return;
    }
    全局虚拟机 = NULL;
    POST_SEM(&zhi_编译_信号量);
}

/********************************************************/
静态函数 char *截取前n个字符(char *buf, size_t 截取数, const char *被截取字符串)
{
    char *q, *q_end;
    int c;

    if (截取数 > 0) {
        q = buf;
        q_end = buf + 截取数 - 1;
        while (q < q_end) {
            c = *被截取字符串++;
            if (c == '\0')
                break;
            *q++ = c;
        }
        *q = '\0';
    }
    return buf;
}

/*截取字符串s前 大小-strlen(buf) 个字符，然后拼接到buf后面  */
静态函数 char *截取后拼接(char *buf, size_t buf_size, const char *s)
{
    size_t 长度;
    长度 = strlen(buf);
    if (长度 < buf_size)
        截取前n个字符(buf + 长度, buf_size - 长度, s);
    return buf;
}
/*
 * 功能：复制指定字节数内容.memcpy()的改进版.
*/
静态函数 char *复制指定字节数字符串(char *存储复制内容, const char *被复制内容, size_t 被复制字节数)
{
    memcpy(存储复制内容, 被复制内容, 被复制字节数);
    存储复制内容[被复制字节数] = '\0';
    return 存储复制内容;
}

/*
 * 功能：提取文件的基本名称（含扩展名）*/
公用函数 char *取文件名含扩展名(const char *全名)
{
    char *结尾指针 = strchr(全名, 0);/*0是字符串结束符.strchr(name, 0)返回字符串name的结束符指针*/
    while (结尾指针 > 全名 && !是斜杠(结尾指针[-1]))/*从字符串右侧开始一直循环查找到'/'为止*/
    {
    	--结尾指针;
    }
    return 结尾指针;
}

/* 提取文件的扩展名部分（如果没有扩展名，则返回指向字符串结尾的指针）
 */
公用函数 char *取文件扩展名 (const char *路径)
{
    char *文件名指针 = 取文件名含扩展名(路径);
    char *圆点指针 = strrchr(文件名指针, '.');/* 最后一次出现“.”的位置 */
    return 圆点指针 ? 圆点指针 : strchr(文件名指针, 0);
}

静态函数 char *zhi_load_text(int 文件描述)
{
    int 长度 = lseek(文件描述, 0, SEEK_END);
    char *buf = load_data(文件描述, 0, 长度 + 1);
    buf[长度] = 0;
    return buf;
}

/********************************************************/
/* memory management */

#undef free
#undef malloc
#undef realloc

#ifndef 内存_调试

公用函数 void zhi_释放(void *ptr)
{
    free(ptr);
}

公用函数 void *zhi_分配内存(unsigned long 大小)
{
    void *ptr;
    ptr = malloc(大小);
    if (!ptr && 大小)
        _zhi_错误("内存已满 (malloc)");
    return ptr;
}

公用函数 void *初始化内存(unsigned long 大小)
{
    void *ptr;
    ptr = zhi_分配内存(大小);
    if (大小)
        memset(ptr, 0, 大小);
    return ptr;
}

公用函数 void *zhi_重新分配(void *ptr, unsigned long 大小)
{
    void *ptr1;
    ptr1 = realloc(ptr, 大小);
    if (!ptr1 && 大小)
        _zhi_错误("内存已满 (realloc)");
    return ptr1;
}

公用函数 char *字符串增加一个宽度(const char *字符串)
{
    char *ptr;
    ptr = zhi_分配内存(strlen(字符串) + 1);
    strcpy(ptr, 字符串);
    return ptr;
}

#else

#define MEM_DEBUG_MAGIC1 0xFEEDDEB1
#define MEM_DEBUG_MAGIC2 0xFEEDDEB2
#define MEM_DEBUG_MAGIC3 0xFEEDDEB3
#define MEM_DEBUG_FILE_LEN 40
#define MEM_DEBUG_CHECK3(header) \
    ((mem_debug_header_t*)((char*)header + header->大小))->magic3
#define MEM_USER_PTR(header) \
    ((char *)header + offsetof(mem_debug_header_t, magic3))
#define MEM_HEADER_PTR(ptr) \
    (mem_debug_header_t *)((char*)ptr - offsetof(mem_debug_header_t, magic3))

struct mem_debug_header {
    unsigned magic1;
    unsigned 大小;
    struct mem_debug_header *上个;
    struct mem_debug_header *next;
    int 当前行号;
    char file_name[MEM_DEBUG_FILE_LEN + 1];
    unsigned magic2;
    ALIGNED(16) unsigned char magic3[4];
};

typedef struct mem_debug_header mem_debug_header_t;

static mem_debug_header_t *mem_debug_chain;
static unsigned 内存_当前大小;
static unsigned 内存_最大大小;

static mem_debug_header_t *malloc_check(void *ptr, const char *msg)
{
    mem_debug_header_t * header = MEM_HEADER_PTR(ptr);
    if (header->magic1 != MEM_DEBUG_MAGIC1 ||
        header->magic2 != MEM_DEBUG_MAGIC2 ||
        read32le(MEM_DEBUG_CHECK3(header)) != MEM_DEBUG_MAGIC3 ||
        header->大小 == (unsigned)-1) {
        fprintf(stderr, "%s check failed\n", msg);
        if (header->magic1 == MEM_DEBUG_MAGIC1)
            fprintf(stderr, "%s:%u: 块 allocated here.\n",
                header->file_name, header->当前行号);
        exit(1);
    }
    return header;
}

公用函数 void *zhi_malloc_debug(unsigned long 大小, const char *文件, int line)
{
    int ofs;
    mem_debug_header_t *header;

    header = malloc(sizeof(mem_debug_header_t) + 大小);
    if (!header)
        _zhi_错误("内存已满 (malloc)");

    header->magic1 = MEM_DEBUG_MAGIC1;
    header->magic2 = MEM_DEBUG_MAGIC2;
    header->大小 = 大小;
    write32le(MEM_DEBUG_CHECK3(header), MEM_DEBUG_MAGIC3);
    header->当前行号 = line;
    ofs = strlen(文件) - MEM_DEBUG_FILE_LEN;
    strncpy(header->file_name, 文件 + (ofs > 0 ? ofs : 0), MEM_DEBUG_FILE_LEN);
    header->file_name[MEM_DEBUG_FILE_LEN] = 0;

    header->next = mem_debug_chain;
    header->上个 = NULL;
    if (header->next)
        header->next->上个 = header;
    mem_debug_chain = header;

    内存_当前大小 += 大小;
    if (内存_当前大小 > 内存_最大大小)
        内存_最大大小 = 内存_当前大小;

    return MEM_USER_PTR(header);
}

公用函数 void zhi_free_debug(void *ptr)
{
    mem_debug_header_t *header;
    if (!ptr)
        return;
    header = malloc_check(ptr, "zhi_释放");
    内存_当前大小 -= header->大小;
    header->大小 = (unsigned)-1;
    if (header->next)
        header->next->上个 = header->上个;
    if (header->上个)
        header->上个->next = header->next;
    if (header == mem_debug_chain)
        mem_debug_chain = header->next;
    free(header);
}

公用函数 void *zhi_mallocz_debug(unsigned long 大小, const char *文件, int line)
{
    void *ptr;
    ptr = zhi_malloc_debug(大小,文件,line);
    memset(ptr, 0, 大小);
    return ptr;
}

公用函数 void *zhi_realloc_debug(void *ptr, unsigned long 大小, const char *文件, int line)
{
    mem_debug_header_t *header;
    int mem_debug_chain_update = 0;
    if (!ptr)
        return zhi_malloc_debug(大小, 文件, line);
    header = malloc_check(ptr, "zhi_重新分配");
    内存_当前大小 -= header->大小;
    mem_debug_chain_update = (header == mem_debug_chain);
    header = realloc(header, sizeof(mem_debug_header_t) + 大小);
    if (!header)
        _zhi_错误("内存已满 (realloc)");
    header->大小 = 大小;
    write32le(MEM_DEBUG_CHECK3(header), MEM_DEBUG_MAGIC3);
    if (header->next)
        header->next->上个 = header;
    if (header->上个)
        header->上个->next = header;
    if (mem_debug_chain_update)
        mem_debug_chain = header;
    内存_当前大小 += 大小;
    if (内存_当前大小 > 内存_最大大小)
        内存_最大大小 = 内存_当前大小;
    return MEM_USER_PTR(header);
}

公用函数 char *zhi_strdup_debug(const char *字符串, const char *文件, int line)
{
    char *ptr;
    ptr = zhi_malloc_debug(strlen(字符串) + 1, 文件, line);
    strcpy(ptr, 字符串);
    return ptr;
}

公用函数 void zhi_memcheck(void)
{
    if (内存_当前大小)
    {
        mem_debug_header_t *header = mem_debug_chain;
        fprintf(stderr, "内存_调试: mem_leak= %d bytes, 内存_最大大小= %d bytes\n",内存_当前大小, 内存_最大大小);
        while (header)
        {
            fprintf(stderr, "%s:%u: 错误: %u 个字节泄露\n",header->file_name, header->当前行号, header->大小);
            header = header->next;
        }
#if 内存_调试-0 == 2
        exit(2);
#endif
    }
}
#endif /* 内存_调试 */

#define free(p) use_zhi_free(p)
#define malloc(s) use_zhi_malloc(s)
#define realloc(p, s) use_zhi_realloc(p, s)
/**************************************************动态字符串--开始**************************************************/


/* 动态字符串ST*/
static void 动态字符串_重新分配(动态字符串ST *s, int 新大小)
{
    int 大小;
    大小 = s->容量;
    if (大小 < 8)
        大小 = 8;
    while (大小 < 新大小)
        大小 = 大小 * 2;
    s->数据 = zhi_重新分配(s->数据, 大小);
    s->容量 = 大小;
}

ST_INLN void 动态字符串_追加一个字符(动态字符串ST *s, int 新字符)
{
    int 大小;
    大小 = s->大小 + 1;
    if (大小 > s->容量)
        动态字符串_重新分配(s, 大小);
    ((unsigned char *)s->数据)[大小 - 1] = 新字符;
    s->大小 = 大小;
}

ST_INLN char *unicode转为utf8 (char *b, uint32_t Uc)
{
    if (Uc<0x80) *b++=Uc;
    else if (Uc<0x800) *b++=192+Uc/64, *b++=128+Uc%64;
    else if (Uc-0xd800u<0x800) return b;
    else if (Uc<0x10000) *b++=224+Uc/4096, *b++=128+Uc/64%64, *b++=128+Uc%64;
    else if (Uc<0x110000) *b++=240+Uc/262144, *b++=128+Uc/4096%64, *b++=128+Uc/64%64, *b++=128+Uc%64;
    return b;
}

ST_INLN void 动态字符串_添加一个扩展为utf8的unicode字符(动态字符串ST *s, int 当前字符)
{
    char buf[4], *e;
    e = unicode转为utf8(buf, (uint32_t)当前字符);
    动态字符串_拼接(s, buf, e - buf);
}

静态函数 void 动态字符串_拼接(动态字符串ST *s, const char *字符串, int 长度)
{
    int 大小;
    if (长度 <= 0)
        长度 = strlen(字符串) + 1 + 长度;
    大小 = s->大小 + 长度;
    if (大小 > s->容量)
        动态字符串_重新分配(s, 大小);
    memmove(((unsigned char *)s->数据) + s->大小, 字符串, 长度);
    s->大小 = 大小;
}

静态函数 void 动态字符串_追加一个宽字符(动态字符串ST *s, int 新字符)
{
    int 大小;
    大小 = s->大小 + sizeof(nwchar_t);
    if (大小 > s->容量)
        动态字符串_重新分配(s, 大小);
    *(nwchar_t *)(((unsigned char *)s->数据) + 大小 - sizeof(nwchar_t)) = 新字符;
    s->大小 = 大小;
}

静态函数 void 动态字符串_新建(动态字符串ST *s)
{
    memset(s, 0, sizeof(动态字符串ST));
}

静态函数 void 动态字符串_释放(动态字符串ST *s)
{
    zhi_释放(s->数据);
    动态字符串_新建(s);
}

静态函数 void 动态字符串_重置(动态字符串ST *s)
{
    s->大小 = 0;
}

静态函数 int 动态字符串_vprintf(动态字符串ST *s, const char *fmt, va_list ap)
{
    va_list v;
    int 长度, 大小 = 80;
    for (;;)
    {
        大小 += s->大小;
        if (大小 > s->容量)
            动态字符串_重新分配(s, 大小);
        大小 = s->容量 - s->大小;
        va_copy(v, ap);
        长度 = vsnprintf((char*)s->数据 + s->大小, 大小, fmt, v);
        va_end(v);
        if (长度 > 0 && 长度 < 大小)
            break;
        大小 *= 2;
    }
    s->大小 += 长度;
    return 长度;
}

静态函数 int 动态字符串_printf(动态字符串ST *s, const char *fmt, ...)
{
    va_list ap; int 长度;
    va_start(ap, fmt);
    长度 = 动态字符串_vprintf(s, fmt, ap);
    va_end(ap);
    return 长度;
}

static void 动态字符串_增加字符(动态字符串ST *s, int c)
{
    if (c == '\'' || c == '\"' || c == '\\')
    {
        动态字符串_追加一个字符(s, '\\');
    }
    if (c >= 32 && c <= 126) {
        动态字符串_追加一个字符(s, c);
    } else {
        动态字符串_追加一个字符(s, '\\');
        if (c == '\n') {
            动态字符串_追加一个字符(s, 'n');
        } else {
            动态字符串_追加一个字符(s, '0' + ((c >> 6) & 7));
            动态字符串_追加一个字符(s, '0' + ((c >> 3) & 7));
            动态字符串_追加一个字符(s, '0' + (c & 7));
        }
    }
}

/**************************************************动态字符串--结束**************************************************/
/**************************************************动态数组--开始**************************************************/



静态函数 void 动态数组_追加元素(void *ptab, int *nb_ptr, void *数据)
{
    int nb, nb_alloc;
    void **pp;

    nb = *nb_ptr;
    pp = *(void ***)ptab;
    /* 2 的每一个幂，我们将数组大小加倍 */
    if ((nb & (nb - 1)) == 0)
    {
        if (!nb)
            nb_alloc = 1;
        else
            nb_alloc = nb * 2;
        pp = zhi_重新分配(pp, nb_alloc * sizeof(void *));
        *(void***)ptab = pp;
    }
    pp[nb++] = 数据;
    *nb_ptr = nb;
}

静态函数 void 动态数组_重置(void *pp, int *n)
{
    void **p;
    for (p = *(void***)pp; *n; ++p, --*n)
        if (*p)
            zhi_释放(*p);
    zhi_释放(*(void**)pp);
    *(void**)pp = NULL;
}
/**************************************************动态数组--结束**************************************************/
static void zhi_分割_路径(虚拟机ST *s, void *p_ary, int *p_nb_ary, const char *in)
{
    const char *p;
    do {
        int c;
        动态字符串ST 字符串;

        动态字符串_新建(&字符串);
        for (p = in; c = *p, c != '\0' && c != 路径分割M[0]; ++p) {
            if (c == 英_左大括号 && p[1] && p[2] == 英_右大括号) {
                c = p[1], p += 2;
                if (c == 'B')
                    动态字符串_拼接(&字符串, s->zhi的安装目录, -1);
                if (c == 'f' && 文件) {
                    /* substitute current 文件's dir */
                    const char *f = 文件->真文件名;
                    const char *b = 取文件名含扩展名(f);
                    if (b > f)
                        动态字符串_拼接(&字符串, f, b - f - 1);
                    else
                        动态字符串_拼接(&字符串, ".", 1);
                }
            } else {
                动态字符串_追加一个字符(&字符串, c);
            }
        }
        if (字符串.大小) {
            动态字符串_追加一个字符(&字符串, '\0');
            动态数组_追加元素(p_ary, p_nb_ary, 字符串增加一个宽度(字符串.数据));
        }
        动态字符串_释放(&字符串);
        in = p+1;
    } while (*p);
}

/********************************************************/
/* 警告_...选项位 */
#define 警告模式_开启  1   /* 警告开启（-Woption） */
#define 警告模式_错误 2    /* 警告是一个错误（-Werror=option） */
#define 警告模式_不是错误 4 /* 警告不是错误（-Wno-error=option） */

/* 错误1() 模式 */
enum 异常级别E
{
	级别_警告,
	级别_错误不中止,
	级别_错误
};

static void 错误1(int 级别, const char *fmt, va_list ap)
{
    缓冲文件ST **pf, *f;
    虚拟机ST *虚 = 全局虚拟机;
    动态字符串ST cs;
    动态字符串_新建(&cs);
    if (虚 == NULL)
        goto 没文件;/* 仅当从 zhi_分配内存() 调用时才会发生: '内存不足' */
    退出_虚拟机ST(虚);
    if (级别 == 级别_警告)
    {
        if (虚->警告_错误)
        	级别 = 级别_错误;
        if (虚->warn_num)
        {
            int wopt = *(&虚->警告_无 + 虚->warn_num);/* 处理 zhi_警告_c(warn_option)(fmt, ...) */
            虚->warn_num = 0;
            if (0 == (wopt & 警告模式_开启))
                return;
            if (wopt & 警告模式_错误)
            	级别 = 级别_错误;
            if (wopt & 警告模式_不是错误)
            	级别 = 级别_警告;
        }
        if (虚->警告_无)
            return;
    }
    f = NULL;
    if (虚->错误_设置_jmp_已启用)
    {
        for (f = 文件; f && f->文件名[0] == 英_冒号; f = f->上个)/* 如果内联 ":asm:" 或标记 ":paste:" 则使用上层文件。我们在解析文件时被调用*/
        {
        	;
        }
    }
    if (f)
    {
        for(pf = 虚->include栈; pf < 虚->include栈指针; pf++)
        {
        	动态字符串_printf(&cs, "在文件 %s :第 %d 行的导入文件\n",(*pf)->文件名, (*pf)->当前行号);
        }
        动态字符串_printf(&cs, "在文件%s:第%d行: ",f->文件名, f->当前行号 - !!(单词标志 & 单词标志_行开始之前));
    } else if (虚->当前源文件名)
    {
        动态字符串_printf(&cs, "%s: ", 虚->当前源文件名);
    }

没文件:
    if (0 == cs.大小)
        动态字符串_printf(&cs, "知语言: ");
    动态字符串_printf(&cs, 级别 == 级别_警告 ? "警告: " : "错误: ");
    动态字符串_vprintf(&cs, fmt, ap);
    if (!虚 || !虚->错误_函数)
    {
        /* 默认情况：标准错误 */
        if (虚 && 虚->输出类型 == 输出_预处理 && 虚->标准的输出文件 == stdout)
            printf("\n"); /* 在 zhi -E 期间打印换行符 */
        fflush(stdout); /* 刷新 -v 输出 */
        fprintf(stderr, "%s\n", (char*)cs.数据);
        fflush(stderr); /* 立即打印错误/警告 (win32) */
    } else {
        虚->错误_函数(虚->错误_不透明, (char*)cs.数据);
    }
    动态字符串_释放(&cs);
    if (虚)
    {
        if (级别 != 级别_警告)
            虚->错误_数量++;
        if (级别 != 级别_错误)
            return;
        if (虚->错误_设置_jmp_已启用)
            longjmp(虚->错误_jmp_buf, 1);
    }
    exit(1);
}

核心库接口 void zhi_设置_错误_函数(虚拟机ST *s, void *错误_不透明, ZHI错误函数 错误_函数)
{
    s->错误_不透明 = 错误_不透明;
    s->错误_函数 = 错误_函数;
}

核心库接口 ZHI错误函数 zhi_获取_错误_函数(虚拟机ST *s)
{
    return s->错误_函数;
}

核心库接口 void *zhi_获取_错误_不透明(虚拟机ST *s)
{
    return s->错误_不透明;
}

/* error without aborting current compilation */
公用函数 void _zhi_错误_不终止(const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    错误1(级别_错误不中止, fmt, ap);
    va_end(ap);
}

公用函数 void _zhi_错误(const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);  /*获取可变参数列表的第一个参数的地址（ap是类型为va_list的指针，v是可变参数最左边的参数*/
    for (;;) 错误1(级别_错误, fmt, ap);
}

公用函数 void _zhi_警告(const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    错误1(级别_警告, fmt, ap);
    va_end(ap);
}

/********************************************************/
/* 输入输出层 */

静态函数 void 新建源文件缓冲区(虚拟机ST *s1, const char *路径, int 初始化长度)
{
    缓冲文件ST *缓冲文件;
    int buflen = 初始化长度 ? 初始化长度 : 输入输出缓存大小M;

    缓冲文件 = 初始化内存(sizeof(缓冲文件ST) + buflen);
    缓冲文件->缓冲源码串 = 缓冲文件->缓冲;
    缓冲文件->缓冲结尾 = 缓冲文件->缓冲 + 初始化长度;
    缓冲文件->缓冲结尾[0] = 中_结尾; /* 放置缓冲结尾符号 */
    截取前n个字符(缓冲文件->文件名, sizeof(缓冲文件->文件名), 路径);
#ifdef _WIN32
    斜杠规范(缓冲文件->文件名);
#endif
    缓冲文件->真文件名 = 缓冲文件->文件名;
    缓冲文件->当前行号 = 1;
    缓冲文件->ifdef_栈_ptr = s1->ifdef_栈_ptr;
    缓冲文件->文件描述 = -1;
    缓冲文件->上个 = 文件;
    文件 = 缓冲文件;
    单词标志 = 单词标志_行开始之前 | 单词标志_文件开始之前;
}

静态函数 void 关闭打开的文件(void)
{
    虚拟机ST *s1 = 全局虚拟机;
    缓冲文件ST *缓冲文件 = 文件;
    if (缓冲文件->文件描述 > 0) {
        close(缓冲文件->文件描述);
        总行数 += 缓冲文件->当前行号;
    }
    if (缓冲文件->真文件名 != 缓冲文件->文件名)
        zhi_释放(缓冲文件->真文件名);
    文件 = 缓冲文件->上个;
    zhi_释放(缓冲文件);
}


/* 功能：编译源文件。 如果有错误，则返回非零值。
*/
static int 开始编译(虚拟机ST *s1, int 源文件类型, const char *路径, int 文件描述)
{
    /* 这里进入代码部分，我们使用全局变量进行解析和代码生成（词法分析.c、语法分析.c、<target>-gen.c）。
     * 其他线程需要等我们完成。 或者，我们可以为那些全局变量使用线程本地存储，这可能有也可能没有优势 */

    进入编译状态(s1);
    s1->错误_设置_jmp_已启用 = 1;
    if (setjmp(s1->错误_jmp_buf) == 0) /*第一次调用 setjmp 会返回0 ，程序会记录下当前的位置和调用环境等信息，等程序出错时，我们调用 longjmp, 并设置第二个参数为非0的整数， 这时程序会终止后面的代码，直接跳转到第一次调用 setjmp 的位置，此时setjmp 的返回值不再是0， 而是返回调用 longjmp 设置的第二个参数。*/
    {
        s1->错误_数量 = 0;

        if (文件描述 == -1)
        {
            int 长度 = strlen(路径);
            新建源文件缓冲区(s1, "<string>", 长度);
            memcpy(文件->缓冲, 路径, 长度);
        } else
        {
            新建源文件缓冲区(s1, 路径, 0);
            文件->文件描述 = 文件描述;
        }

        设置节数据状态(s1);
        词法分析初始化(s1, 源文件类型);
        语法分析初始化(s1);
        if (s1->输出类型 == 输出_预处理)
        {
            zhi_预处理(s1);
        } else if (源文件类型 & (源文件类型_仅汇编 | 源文件类型_汇编预处理))
        {
            汇编当前文件(s1, !!(源文件类型 & 源文件类型_汇编预处理));
        } else
        {
            语法分析开始(s1);/* ★☆★★☆★★☆★★☆★★☆★★☆★★☆★★☆★★☆★★☆★★☆★★☆★真正的进入语法制导的编译流程，自此之前的都是准备工作★☆★★☆★★☆★★☆★★☆★★☆★★☆★★☆★★☆★★☆★★☆★★☆★  */
        }
    }
    语法分析_完成(s1);
    预处理_结束(s1);

    s1->错误_设置_jmp_已启用 = 0;
    退出_虚拟机ST(s1);

    elf结束文件(s1);
    return s1->错误_数量 != 0 ? -1 : 0;
}

/* 定义预处理器符号。 值可以是 NULL，符号可以是“sym=val” */
核心库接口 void zhi_定义_预处理符号(虚拟机ST *s1, const char *sym, const char *value)
{
    const char *eq;
    if (NULL == (eq = strchr(sym, '=')))
        eq = strchr(sym, 0);
    if (NULL == value)
        value = *eq ? eq + 1 : "1";
    动态字符串_printf(&s1->命令行_defines, "#define %.*s %s\n", (int)(eq-sym), sym, value);
}

/* 取消定义预处理器符号 */
核心库接口 void zhi_结束定义_预处理符号(虚拟机ST *s1, const char *sym)
{
    动态字符串_printf(&s1->命令行_defines, "#undef %s\n", sym);
}

/*
 * 功能：初始化虚拟机ST，新建elf,设置库路径。创建新的TCC编译上下文。
 * */
核心库接口 虚拟机ST *新建虚拟机(void)
{
    虚拟机ST *s;

    s = 初始化内存(sizeof(虚拟机ST));
    if (!s)
    {
    	return NULL;
    }
#ifdef 内存_调试
    ++虚拟机数量;
#endif

#undef GNU_C_扩展

    s->GNU_C_扩展 = 1;
    s->zhi_扩展 = 1;
    s->bss不使用通用符号 = 1;
    s->标识符中允许使用美元符 = 1;
    s->知语言版本 = 199901; /* 默认除非提供 -std=c11*/
    s->隐式函数声明警告 = 1;
    s->警告丢弃限定符 = 1;
    s->ms_扩展 = 1;

#ifdef CHAR_是无符号
    s->字符_是_无符号的 = 1;
#endif
#ifdef 目标_I386
    s->分割大小 = 32;
#endif
    /* 如果您想要在 Windows 上带有前导下划线的符号，请启用此功能：不过启用PE后会报一个 知语言: 错误: 未定义的符号 '__mzerodf' */
#if defined 目标_MACHO /* || defined 目标_PE */
    s->前导下划线 = 1;
#endif
#ifdef 目标_ARM
    s->浮点_abi = ARM_浮点_ABI;
#endif

    s->标准的输出文件 = stdout;
    /* 可能在 词法分析初始化()之前用在error()中 */
    s->include栈指针 = s->include栈;

    新建数据代码符号表节(s);

    设置_库_路径(s, 编译文件夹M);
    return s;
}
/* 功能：释放编译上下文。
 * */
核心库接口 void 删除_虚拟机ST(虚拟机ST *s1)
{
    /* free 节数组 */
    elf删除(s1);

    /* 释放库路径 */
    动态数组_重置(&s1->库_路径数组, &s1->库_路径_数量);
    动态数组_重置(&s1->目标文件路径数组, &s1->目标文件路径数量);

    /* 释放导入路径 */
    动态数组_重置(&s1->导入路径数组, &s1->导入路径数量);
    动态数组_重置(&s1->系统导入路径数组, &s1->系统导入路径数量);

    zhi_释放(s1->zhi的安装目录);
    zhi_释放(s1->soname);
    zhi_释放(s1->rpath);
    zhi_释放(s1->init_symbol);
    zhi_释放(s1->fini_symbol);
    zhi_释放(s1->输出文件名);
    zhi_释放(s1->指定_依赖);
    动态数组_重置(&s1->源文件列表, &s1->文件_数量);
    动态数组_重置(&s1->目标_依赖项数组, &s1->目标_依赖项数量);
    动态数组_重置(&s1->pragma_库数组, &s1->pragma_库数量);
    动态数组_重置(&s1->argv, &s1->argc);
    动态字符串_释放(&s1->命令行_defines);
    动态字符串_释放(&s1->命令行_include);
#ifdef 运行脚本M
    /* 释放运行时内存 */
    zhi_run_free(s1);
#endif

    zhi_释放(s1);
#ifdef 内存_调试
    if (0 == --虚拟机数量)
        zhi_memcheck();
#endif
}

核心库接口 int 设置输出类型(虚拟机ST *s, int 输出类型)
{
    s->输出类型 = 输出类型;
    if (!s->不添加标准头)
    {
        添加系统导入路径(s, 头文件路径M);
    }
#ifdef 启用边界检查M
    if (s->使用_边界_检查器)
    {
        /* 如果绑定检查，则添加相应的节 */
        elf边界新建(s);
    }
#endif
    if (s->做_调试)
    {
        /* 添加调试节区 */
        elf符号表新建(s);
    }
    if (输出类型 == 输出_OBJ)
    {
        /* 对象始终为elf */
        s->输出格式 = 输出格式_ELF;
        return 0;
    }
    zhi_添加库路径(s, 库路径M);

#ifdef 目标_PE
# ifdef _WIN32
    /* 允许直接与系统dll链接 */
    zhi_添加系统文件夹(s);
# endif
    /* 目标PE在“运行时库.a”中有自己的启动代码  */
    return 0;
#else
    /* paths for crt objects */
    zhi_分割_路径(s, &s->目标文件路径数组, &s->目标文件路径数量, CONFIG_ZHI_CRTPREFIX);
    /* add libc crt1/crti objects */
    if ((输出类型 == 输出_EXE || 输出类型 == 输出_DLL) && !s->不添加标准库)
    {
        if (输出类型 != 输出_DLL)
        {
        	zhi_add_crt(s, "crt1.o");
        }
        zhi_add_crt(s, "crti.o");
    }
    return 0;
#endif
}

核心库接口 int 添加_导入路径(虚拟机ST *s, const char *pathname)
{
    zhi_分割_路径(s, &s->导入路径数组, &s->导入路径数量, pathname);
    return 0;
}

核心库接口 int 添加系统导入路径(虚拟机ST *s, const char *pathname)
{
    zhi_分割_路径(s, &s->系统导入路径数组, &s->系统导入路径数量, pathname);
    return 0;
}

静态函数 DLL引用ST *添加DLL引用(虚拟机ST *s1, const char *dllname)
{
    DLL引用ST *引用符号 = 初始化内存(sizeof(DLL引用ST) + strlen(dllname));
    strcpy(引用符号->name, dllname);
    动态数组_追加元素(&s1->加载的DLL数组, &s1->加载的DLL数量, 引用符号);
    return 引用符号;
}

/*
 * 功能：通过open（）函数打开源文件，失败返回-1
 * */
static int 打开源文件(虚拟机ST *s1, const char *路径)
{
    int 文件描述;
    if (strcmp(路径, "-") == 0)
    {
    	文件描述 = 0, 路径 = "<stdin>";
    }
    else
    {
    	文件描述 = open(路径, O_RDONLY | O_BINARY);/*open() 的返回值是一个 int 类型的文件描述符，打开失败返回 -1*/
    }
    if ((s1->显示详情 == 2 && 文件描述 >= 0) || s1->显示详情 == 3)
    {
    	printf("%s %*s%s\n", 文件描述 < 0 ? "nf":"->",(int)(s1->include栈指针 - s1->include栈), "", 路径);
    }
    return 文件描述;
}

静态函数 int zhi_打开一个新文件(虚拟机ST *s1, const char *文件名)
{
    int 文件描述 = 打开源文件(s1, 文件名);
    if (文件描述 < 0)
    {
    	return -1;
    }
    新建源文件缓冲区(s1, 文件名, 0);
    文件->文件描述 = 文件描述;
    return 0;
}
静态函数 int 打开文件并进入编译(虚拟机ST *s1, const char *路径, int flags)
{
    int 文件描述, ret = -1;

    文件描述 = 打开源文件(s1, 路径);/* 正式打开源文件，开始进入编译流程。打开失败返回-1 */
    if (文件描述 < 0)
    {
        if (flags & 标志_打印_错误)
        {
        	错误_不中止("没有找到 '%s' 文件", 路径);
        }
        return ret;
    }
    s1->当前源文件名 = 路径;
    if (flags & 标志类型_BIN)
    {
        ElfW(Ehdr) ehdr;
        int 目标_类型;
        目标_类型 = 取目标文件类型(文件描述, &ehdr);
        lseek(文件描述, 0, SEEK_SET);/* SEEK_SET欲将读写位置移到文件开头；SEEK_END欲将读写位置移到文件尾；SEEK_CUR想要取得目前文件位置 */
        switch (目标_类型)
        {

        case 目标文件_类型_REL:
            ret = 加载目标文件(s1, 文件描述, 0);
            break;

        case 目标文件_类型_AR:
            ret = 加载存档文件(s1, 文件描述, !(flags & 标志_从存档加载所有对象));
            break;
        default:
            ret = pe_加载_文件(s1, 文件描述, 路径);
            if (ret < 0)
            {
            	错误_不中止("%s: 无法识别的文件类型", 路径);
            }
            break;
#ifdef 目标_COFF
        case 目标文件_类型_C67:
            ret = 加载coff文件(s1, 文件描述);
            break;
#endif
        }
        close(文件描述);
    } else
    {
        /* 更新目标依赖 */
        动态数组_追加元素(&s1->目标_依赖项数组, &s1->目标_依赖项数量, 字符串增加一个宽度(路径));
        ret = 开始编译(s1, flags, 路径, 文件描述);
    }
    s1->当前源文件名 = NULL;
    return ret;
}

/*根据源文件类型选择处理方式*/
核心库接口 int 编译器(虚拟机ST *s, const char *路径)
{
    int 源文件类型 = s->源文件类型;
    if (0 == (源文件类型 & 源文件类型_MASK))
    {
        /* 使用文件扩展名来检测源文件类型 */
        const char *扩展名 = 取文件扩展名(路径);
        if (扩展名[0])
        {
            扩展名++;/* 圆点的指针+1才是扩展名的指针 */
            if (!strcmp(扩展名, "S"))
            {
            	源文件类型 = 源文件类型_汇编预处理;
            }else if (!strcmp(扩展名, "s"))
            {
            	源文件类型 = 源文件类型_仅汇编;
            }else if (!PATHCMP(扩展名, "res") || !PATHCMP(扩展名, "z") || !PATHCMP(扩展名, "c") || !PATHCMP(扩展名, "h") || !PATHCMP(扩展名, "i") || !PATHCMP(扩展名, "o"))
            {
            	源文件类型 = 源文件类型_Z;
            }else
            {
            	 源文件类型 |= 标志类型_BIN;
            }
        } else
        {
            源文件类型 = 源文件类型_Z;
        }
    }
    return 打开文件并进入编译(s, 路径, 源文件类型 | 标志_打印_错误);
}
/* 相当于 -Lpath 选项 */
核心库接口 int zhi_添加库路径(虚拟机ST *s, const char *pathname)
{
    zhi_分割_路径(s, &s->库_路径数组, &s->库_路径_数量, pathname);
    return 0;
}

static int 添加库_内部(虚拟机ST *s, const char *fmt,const char *文件名, int flags, char **路径数组, int 路径数量)
{
    char buf[1024];
    int i;
    for(i = 0; i < 路径数量; i++)
    {
        snprintf(buf, sizeof(buf), fmt, 路径数组[i], 文件名);/*将可变参数(...)按照 format 格式化成字符串，并将字符串复制到 buf 中，sizeof(buf) 为要写入的字符的最大数目，超过 sizeof(buf) 会被截断。*/
        if (打开文件并进入编译(s, buf, flags | 标志类型_BIN) == 0)
            return 0;
    }
    return -1;
}

#ifndef 目标_MACHO
/* 找到并加载一个dll。 如果没有找到返回非零 */
/* XXX: 添加 '-rpath' 选项的支持 ? */
静态函数 int zhi_添加_dll(虚拟机ST *s, const char *文件名, int flags)
{
    return 添加库_内部(s, "%s/%s", 文件名, flags,
        s->库_路径数组, s->库_路径_数量);
}
#endif

#if !defined 目标_PE && !defined 目标_MACHO
静态函数 int zhi_add_crt(虚拟机ST *s1, const char *文件名)
{
    if (-1 == 添加库_内部(s1, "%s/%s",文件名, 0, s1->目标文件路径数组, s1->目标文件路径数量))
        错误_不中止("没有找到 '%s' 文件", 文件名);
    return 0;
}
#endif

/* 库名称与“-l”选项的参数相同 */
核心库接口 int 添加库(虚拟机ST *s, const char *库文件名)
{
#if defined 目标_PE
    static const char * const libs[] = { "%s/%s.def", "%s/库%s.def", "%s/%s.dll", "%s/库%s.dll", "%s/库%s.a", NULL };
    const char * const *pp = s->静态_链接 ? libs + 4 : libs;
#elif defined 目标_MACHO
    static const char * const libs[] = { "%s/库%s.dylib", "%s/库%s.tbd", "%s/库%s.a", NULL };
    const char * const *pp = s->静态_链接 ? libs + 2 : libs;
#elif defined 目标系统_OpenBSD
    static const char * const libs[] = { "%s/库%s.so.*", "%s/库%s.a", NULL };
    const char * const *pp = s->静态_链接 ? libs + 1 : libs;
#else
    static const char * const libs[] = { "%s/库%s.so", "%s/库%s.a", NULL };
    const char * const *pp = s->静态_链接 ? libs + 1 : libs;
#endif
    int flags = s->源文件类型 & 标志_从存档加载所有对象;
    while (*pp)
    {
        if (0 == 添加库_内部(s, *pp,库文件名, flags, s->库_路径数组, s->库_路径_数量))
            return 0;
        ++pp;
    }
    return -1;
}

公用函数 int 添加库_有错误提示(虚拟机ST *s1, const char *libname)
{
    int ret = 添加库(s1, libname);
    if (ret < 0)
        错误_不中止("未找到库 '%s'", libname);
    return ret;
}

/* 处理 #pragma comment(库,) */
静态函数 void 加载静态链接库(虚拟机ST *s1)
{
    for (int i = 0; i < s1->pragma_库数量; i++)
    {
    	添加库_有错误提示(s1, s1->pragma_库数组[i]);
    }
}
/* 给编译的程序添加一个符号 */
核心库接口 int zhi_添加_符号(虚拟机ST *s1, const char *name, const void *val)
{
#ifdef 目标_PE
    /* On x86_64 'val' might not be reachable with a 32bit offset.
       So it is handled here as if it were in a DLL. */
    pe_putimport(s1, 0, name, (uintptr_t)val);
#else
    char buf[256];
    if (s1->前导下划线) {
        buf[0] = '_';
        截取前n个字符(buf + 1, sizeof(buf) - 1, name);
        name = buf;
    }
    设置全局符号(s1, name, NULL, (addr_t)(uintptr_t)val); /* NULL: SHN_ABS */
#endif
    return 0;
}

核心库接口 void 设置_库_路径(虚拟机ST *s, const char *path)
{
    zhi_释放(s->zhi的安装目录);
    s->zhi的安装目录 = 字符串增加一个宽度(path);
}



/*两个值不相等则返回0，*/
static int strstart(const char *val, const char **字符串)
{
    const char *右, *左;
    右 = *字符串;
    左 = val;
    while (*左)
    {
        if (*右 != *左)
        {
        	return 0;
        }
        右++;
        左++;
    }
    *字符串 = 右;
    return 1;
}

/* 与 strstart 类似，但会自动考虑 ld 选项可以以双破折号或单破折号开头（例如 '--soname' 或 '-soname'） 参数可以单独给出或在 '=' 之后给出（例如 '-Wl,-soname ,x.so' 或 '-Wl,-soname=x.so') 你总是以 'option[=]' 形式提供 `val` (没有前导 -)
 */
static int 链接_选项(const char *字符串, const char *val, const char **ptr)
{
    const char *p, *q;
    int ret;

    /* 应该有 1 或 2 个破折号*/
    if (*字符串++ != '-')
        return 0;
    if (*字符串 == '-')
        字符串++;

    /* 那么字符串和值应该匹配（可能高达'='） */
    p = 字符串;
    q = val;

    ret = 1;
    if (q[0] == 英_问号) {
        ++q;
        if (strstart("no-", &p))
            ret = -1;
    }

    while (*q != '\0' && *q != '=') {
        if (*p != *q)
            return 0;
        p++;
        q++;
    }

    /* '=' near eos means 英_逗号 or '=' is ok */
    if (*q == '=') {
        if (*p == 0)
            *ptr = p;
        if (*p != 英_逗号 && *p != '=')
            return 0;
        p++;
    } else if (*p) {
        return 0;
    }
    *ptr = p;
    return ret;
}

static const char *跳过_链接器_参数(const char **字符串)
{
    const char *s1 = *字符串;
    const char *s2 = strchr(s1, 英_逗号);
    *字符串 = s2 ? s2++ : (s2 = s1 + strlen(s1));
    return s2;
}

static void 复制_链接器_参数(char **pp, const char *s, int sep)
{
    const char *q = s;
    char *p = *pp;
    int l = 0;
    if (p && sep)
        p[l = strlen(p)] = sep, ++l;
    跳过_链接器_参数(&q);
    复制指定字节数字符串(l + (*pp = zhi_重新分配(p, q - s + l + 1)), s, q - s);
}

/* set linker options */
static int zhi_设置_链接器(虚拟机ST *s, const char *option)
{
    虚拟机ST *s1 = s;
    while (*option) {

        const char *p = NULL;
        char *end = NULL;
        int ignoring = 0;
        int ret;

        if (链接_选项(option, "Bsymbolic", &p)) {
            s->当前模块的符号 = 1;
        } else if (链接_选项(option, "nostdlib", &p)) {
            s->不添加标准库 = 1;
        } else if (链接_选项(option, "fini=", &p)) {
            复制_链接器_参数(&s->fini_symbol, p, 0);
            ignoring = 1;
        } else if (链接_选项(option, "image-base=", &p)
                || 链接_选项(option, "Ttext=", &p)) {
            s->代码节地址 = strtoull(p, &end, 16);
            s->has_代码节地址 = 1;
        } else if (链接_选项(option, "init=", &p)) {
            复制_链接器_参数(&s->init_symbol, p, 0);
            ignoring = 1;
        } else if (链接_选项(option, "oformat=", &p)) {
#if defined(目标_PE)
            if (strstart("pe-", &p)) {
#elif 宏_指针大小 == 8
            if (strstart("elf64-", &p)) {
#else
            if (strstart("elf32-", &p)) {
#endif
                s->输出格式 = 输出格式_ELF;
            } else if (!strcmp(p, "binary")) {
                s->输出格式 = 输出格式_BINARY;
#ifdef 目标_COFF
            } else if (!strcmp(p, "coff")) {
                s->输出格式 = 输出格式_COFF;
#endif
            } else
                goto err;

        } else if (链接_选项(option, "as-needed", &p)) {
            ignoring = 1;
        } else if (链接_选项(option, "O", &p)) {
            ignoring = 1;
        } else if (链接_选项(option, "export-all-symbols", &p)) {
            s->导出所有符号 = 1;
        } else if (链接_选项(option, "export-dynamic", &p)) {
            s->导出所有符号 = 1;
        } else if (链接_选项(option, "rpath=", &p)) {
            复制_链接器_参数(&s->rpath, p, 英_冒号);
        } else if (链接_选项(option, "enable-new-dtags", &p)) {
            s->enable_new_dtags = 1;
        } else if (链接_选项(option, "section-alignment=", &p)) {
            s->节_对齐 = strtoul(p, &end, 16);
        } else if (链接_选项(option, "soname=", &p)) {
            复制_链接器_参数(&s->soname, p, 0);
#ifdef 目标_PE
        } else if (链接_选项(option, "large-address-aware", &p)) {
            s->PE特性 |= 0x20;
        } else if (链接_选项(option, "文件-alignment=", &p)) {
            s->pe_文件_对齐 = strtoul(p, &end, 16);
        } else if (链接_选项(option, "stack=", &p)) {
            s->PE栈大小 = strtoul(p, &end, 10);
        } else if (链接_选项(option, "subsystem=", &p)) {
#if defined(目标_I386) || defined(目标_X86_64)
            if (!strcmp(p, "native")) {
                s->PE_子系统 = 1;
            } else if (!strcmp(p, "console")) {
                s->PE_子系统 = 3;
            } else if (!strcmp(p, "gui") || !strcmp(p, "windows")) {
                s->PE_子系统 = 2;
            } else if (!strcmp(p, "posix")) {
                s->PE_子系统 = 7;
            } else if (!strcmp(p, "efiapp")) {
                s->PE_子系统 = 10;
            } else if (!strcmp(p, "efiboot")) {
                s->PE_子系统 = 11;
            } else if (!strcmp(p, "efiruntime")) {
                s->PE_子系统 = 12;
            } else if (!strcmp(p, "efirom")) {
                s->PE_子系统 = 13;
#elif defined(目标_ARM)
            if (!strcmp(p, "wince")) {
                s->PE_子系统 = 9;
#endif
            } else
                goto err;
#endif
        } else if (ret = 链接_选项(option, "?whole-archive", &p), ret) {
            if (ret > 0)
                s->源文件类型 |= 标志_从存档加载所有对象;
            else
                s->源文件类型 &= ~标志_从存档加载所有对象;
        } else if (链接_选项(option, "z=", &p)) {
            ignoring = 1;
        } else if (p) {
            return 0;
        } else {
    err:
            错_误("不支持的链接选项： '%s'", option);
        }
        if (ignoring)
            zhi_警告_c(警告_不支持)("不支持的链接选项： '%s'", option);
        option = 跳过_链接器_参数(&p);
    }
    return 1;
}

typedef struct
{
    const char *选项名称;
    uint16_t   选项索引;
    uint16_t   选项标记;
} Zhi选项;
/*命令行选项索引：Zhi选项.index*/
enum {
    选项_ignored = 0,
    选项_help,
    选项_about,
    选项_v,
    选项_I,
    选项_D,
    选项_U,
    选项_P,
    选项_L,
    选项_B,
    选项_l,
    选项_bench,
    选项_bt,
    选项_b,
    选项_ba,
    选项_g,
    选项_c,
    选项_dumpversion,
    选项_d,
    选项_static,
    选项_std,
    选项_shared,
    选项_soname,
    选项_o,
    选项_r,
    选项_Wl,
    选项_Wp,
    选项_W,
    选项_O,
    选项_m浮点_abi,
    选项_m,
    选项_f,
    选项_isystem,
    选项_iwithprefix,
    选项_include,
    选项_nostdinc,
    选项_nostdlib,
    选项_print_search_dirs,
    选项_rdynamic,
    选项_pthread,
    选项_run,
    选项_w,
    选项_E,
    选项_M,
    选项_MD,
    选项_MF,
    选项_MM,
    选项_MMD,
    选项_x,
    选项_ar,
    选项_impdef,
};

#define 选项_有参数 0x0001
#define 选项_不分开   0x0002 /* 选项和 参数之间不能有空格 */

static const Zhi选项 选项列表[] = {
    { "h",                 选项_help, 0 },
    { "-help",             选项_help, 0 },
    { "?",                 选项_help, 0 },
    { "about",             选项_about, 0 },
    { "v",                 选项_v, 选项_有参数 | 选项_不分开 },
    { "-version",          选项_v, 0 }, /* 处理为显示详情，也打印版本*/
    { "I",                 选项_I, 选项_有参数 },
    { "D",                 选项_D, 选项_有参数 },
    { "U",                 选项_U, 选项_有参数 },
    { "P",                 选项_P, 选项_有参数 | 选项_不分开 },
    { "L",                 选项_L, 选项_有参数 },
    { "B",                 选项_B, 选项_有参数 },
    { "l",                 选项_l, 选项_有参数 },
    { "bench",             选项_bench, 0 },
#ifdef 启用内置堆栈回溯M
    { "bt",                选项_bt, 选项_有参数 | 选项_不分开 },
#endif
#ifdef 启用边界检查M
    { "b",                 选项_b, 0 },
#endif
    { "g",                 选项_g, 选项_有参数 | 选项_不分开 },
    { "c",                 选项_c, 0 },
    { "dumpversion",       选项_dumpversion, 0},
    { "d",                 选项_d, 选项_有参数 | 选项_不分开 },
    { "static",            选项_static, 0 },
    { "std",               选项_std, 选项_有参数 | 选项_不分开 },
    { "shared",            选项_shared, 0 },
    { "soname",            选项_soname, 选项_有参数 },
    { "o",                 选项_o, 选项_有参数 },
    { "pthread",           选项_pthread, 0},
    { "run",               选项_run, 选项_有参数 | 选项_不分开 },
    { "rdynamic",          选项_rdynamic, 0 },
    { "r",                 选项_r, 0 },
    { "Wl,",               选项_Wl, 选项_有参数 | 选项_不分开 },
    { "Wp,",               选项_Wp, 选项_有参数 | 选项_不分开 },
    { "W",                 选项_W, 选项_有参数 | 选项_不分开 },
    { "O",                 选项_O, 选项_有参数 | 选项_不分开 },
#ifdef 目标_ARM
    { "mfloat-abi",        选项_m浮点_abi, 选项_有参数 },
#endif
    { "m",                 选项_m, 选项_有参数 | 选项_不分开 },
    { "f",                 选项_f, 选项_有参数 | 选项_不分开 },
    { "isystem",           选项_isystem, 选项_有参数 },
    { "include",           选项_include, 选项_有参数 },
    { "nostdinc",          选项_nostdinc, 0 },
    { "nostdlib",          选项_nostdlib, 0 },
    { "print-search-dirs", 选项_print_search_dirs, 0 },
    { "w",                 选项_w, 0 },
    { "E",                 选项_E, 0},
    { "M",                 选项_M, 0},
    { "MD",                选项_MD, 0},
    { "MF",                选项_MF, 选项_有参数 },
    { "MM",                选项_MM, 0},
    { "MMD",               选项_MMD, 0},
    { "x",                 选项_x, 选项_有参数 },
    { "ar",                选项_ar, 0},
#ifdef 目标_PE
    { "impdef",            选项_impdef, 0},
#endif
    /* 忽略（默默地，除非在 -Wunsupported 之后）*/
    { "arch",              0, 选项_有参数},
    { "C",                 0, 0 },
    { "-param",            0, 选项_有参数 },
    { "pedantic",          0, 0 },
    { "pipe",              0, 0 },
    { "s",                 0, 0 },
    { "traditional",       0, 0 },
    { NULL,                0, 0 },
};

typedef struct 标志定义S {
    uint16_t offset;
    uint16_t flags;
    const char *name;
} 标志定义S;

#define 警告_激活    0x0001 /* 使用-Wall，警告被激活 */
#define FD_反转 0x0002 /* 存储前反转值 */

static const 标志定义S options_W[] = {
    { offsetof(虚拟机ST, 警告_所有), 警告_激活, "all" },
    { offsetof(虚拟机ST, 警告_错误), 0, "error" },
    { offsetof(虚拟机ST, 警告_写_字符串), 0, "write-strings" },
    { offsetof(虚拟机ST, 警告_不支持), 0, "unsupported" },
    { offsetof(虚拟机ST, 隐式函数声明警告), 警告_激活, "implicit-function-declaration" },
    { offsetof(虚拟机ST, 警告丢弃限定符), 警告_激活, "discarded-限定符" },
    { 0, 0, NULL }
};

static const 标志定义S options_f[] = {
    { offsetof(虚拟机ST, 字符_是_无符号的), 0, "unsigned-char" },
    { offsetof(虚拟机ST, 字符_是_无符号的), FD_反转, "signed-char" },
    { offsetof(虚拟机ST, bss不使用通用符号), FD_反转, "common" },
    { offsetof(虚拟机ST, 前导下划线), 0, "leading-underscore" },
    { offsetof(虚拟机ST, ms_扩展), 0, "ms-extensions" },
    { offsetof(虚拟机ST, 标识符中允许使用美元符), 0, "dollars-in-identifiers" },
    { offsetof(虚拟机ST, 测试_覆盖率), 0, "test-coverage" },
    { 0, 0, NULL }
};

static const 标志定义S options_m[] = {
    { offsetof(虚拟机ST, 模拟MS算法对齐位域), 0, "ms-bitfields" },
#ifdef 目标_X86_64
    { offsetof(虚拟机ST, nosse), FD_反转, "sse" },
#endif
    { 0, 0, NULL }
};

static int set_flag(虚拟机ST *s, const 标志定义S *flags, const char *name)
{
    int value, mask, ret;
    const 标志定义S *p;
    const char *r;
    unsigned char *f;

    r = name, value = !strstart("no-", &r), mask = 0;

    /* when called with options_W, look for -W[no-]error=<option> */
    if ((flags->flags & 警告_激活) && strstart("error=", &r))
        value = value ? 警告模式_开启|警告模式_错误 : 警告模式_不是错误, mask = 警告模式_开启;

    for (ret = -1, p = flags; p->name; ++p) {
        if (ret) {
            if (strcmp(r, p->name))
                continue;
        } else {
            if (0 == (p->flags & 警告_激活))
                continue;
        }

        f = (unsigned char *)s + p->offset;
        *f = (*f & mask) | (value ^ !!(p->flags & FD_反转));

        if (ret) {
            ret = 0;
            if (strcmp(r, "all"))
                break;
        }
    }
    return ret;
}

static void 添加文件到文件列表(虚拟机ST *s, const char* 文件名, int 源文件类型)
{
    struct 文件S *f = zhi_分配内存(sizeof *f + strlen(文件名));
    f->类型 = 源文件类型;
    strcpy(f->名称, 文件名);
    动态数组_追加元素(&s->源文件列表, &s->文件_数量, f);
}

static int 参数_解析_生成argv(const char *r, int *argc, char ***argv)
{
    int ret = 0, q, c;
    动态字符串ST 字符串;
    for(;;)
    {
        while (c = (unsigned char)*r, c && c <= ' ')
          ++r;
        if (c == 0)
            break;
        q = 0;
        动态字符串_新建(&字符串);
        while (c = (unsigned char)*r, c) {
            ++r;
            if (c == '\\' && (*r == '"' || *r == '\\')) {
                c = *r++;
            } else if (c == '"') {
                q = !q;
                continue;
            } else if (q == 0 && c <= ' ') {
                break;
            }
            动态字符串_追加一个字符(&字符串, c);
        }
        动态字符串_追加一个字符(&字符串, 0);
        动态数组_追加元素(argv, argc, 字符串增加一个宽度(字符串.数据));
        动态字符串_释放(&字符串);
        ++ret;
    }
    return ret;
}

/* zhi @文件名称  */
static void 读取列表文件(虚拟机ST *s,const char *文件名, int optind, int *pargc, char ***pargv)
{
    虚拟机ST *s1 = s;
    int 文件描述, i;
    char *p;
    int argc = 0;
    char **argv = NULL;

    文件描述 = open(文件名, O_RDONLY | O_BINARY);
    if (文件描述 < 0)
        错_误("没有找到列表文件 '%s' ", 文件名);

    p = zhi_load_text(文件描述);
    for (i = 0; i < *pargc; ++i)
        if (i == optind)
            参数_解析_生成argv(p, &argc, &argv);
        else
            动态数组_追加元素(&argv, &argc, 字符串增加一个宽度((*pargv)[i]));

    zhi_释放(p);
    动态数组_重置(&s->argv, &s->argc);
    *pargc = s->argc = argc, *pargv = s->argv = argv;
}
/*解析命令行参数
 * optind:传递过来一个整数‘1’或‘0’
 * */
公用函数 int 解析命令行参数(虚拟机ST *s, int *pargc, char ***pargv, int optind)
{
    虚拟机ST *s1 = s;
    const Zhi选项 *选项指针;
    const char *选项参数, *选项;
    const char *run = NULL;
    int x;
    动态字符串ST 链接器参数; /* 收集 -Wl 选项 */
    int 工具 = 0, 参数开始 = 0, 无动作 = optind;
    char **argv = *pargv;
    int argc = *pargc;

    动态字符串_新建(&链接器参数);
/**************************************************argc>=2-开始**************************************************/


    /*如果argc>=2触发此处循环 */
    while (optind < argc)
    {
        选项 = argv[optind];/*选项为命令行参数的第二个参数*/

        if (选项[0] == '@' && 选项[1] != '\0')/* 1.处理参数列表文件*/
        {
            读取列表文件(s, 选项 + 1, optind, &argc, &argv);
            continue;
        }
        optind++;/*此时optind==2*/
        if (工具)/* 2.显示版本信息 */
        {
            if (选项[0] == '-' && 选项[1] == 'v' && 选项[2] == 0)
            {
            	++s->显示详情;
            }
            continue;
        }
重解析:
        if (选项[0] != '-' || 选项[1] == '\0')/* 把选项排除在外 */
        {
            if (选项[0] != '@')/* 把列表文件也排除在外 */
            {
            	添加文件到文件列表(s, 选项, s->源文件类型);/* 3.把源文件添加到编译器状态文件列表中*/
            }
            if (run)
            {
                设置选项(s, run);
                参数开始 = optind - 1;/*此时 参数开始==1*/
                break;
            }
            continue;
        }

        /* 在选项表查找选项 */
        for(选项指针 = 选项列表; ; ++选项指针)
        {
            const char *p1 = 选项指针->选项名称;
            const char *r1 = 选项 + 1;
            if (p1 == NULL)
            {
            	错_误("无效选项 -- '%s'", 选项);
            }
            if (!strstart(p1, &r1))
            {
            	continue;
            }
            选项参数 = r1;
            if (选项指针->选项标记 & 选项_有参数)
            {
                if (*r1 == '\0' && !(选项指针->选项标记 & 选项_不分开))
                {
                    if (optind >= argc)/*此时optind==2*/
                    {
                        arg_err:
                                错_误("选项 '%s' 缺少参数", 选项);
                    }
                    选项参数 = argv[optind++];/*此时optind==3*/
                }
            } else if (*r1 != '\0')
            {
            	continue;
            }
            break;
        }

        switch(选项指针->选项索引)
        {
        case 选项_help:
            x = 选项返回_HELP;
            goto 额外动作;
        case 选项_about:
            x = 选项返回_ABOUT;
            goto 额外动作;
        case 选项_I:
            添加_导入路径(s, 选项参数);
            break;
        case 选项_D:
            zhi_定义_预处理符号(s, 选项参数, NULL);
            break;
        case 选项_U:
            zhi_结束定义_预处理符号(s, 选项参数);
            break;
        case 选项_L:
            zhi_添加库路径(s, 选项参数);
            break;
        case 选项_B:
        	/* 设置zhi实用程序路径（主要用于zhi开发） */
            设置_库_路径(s, 选项参数);
            break;
        case 选项_l:
            添加文件到文件列表(s, 选项参数, 源文件类型_LIB | (s->源文件类型 & ~源文件类型_MASK));
            s->库_数量++;
            break;
        case 选项_pthread:
            s->option_pthread = 1;
            break;
        case 选项_bench:
            s->统计_编译信息 = 1;
            break;
#ifdef 启用内置堆栈回溯M
        case 选项_bt:
            s->返回调用数量 = atoi(选项参数);
            s->执行_回溯 = 1;
            s->做_调试 = 1;
            break;
#endif
#ifdef 启用边界检查M
        case 选项_b:
            s->使用_边界_检查器 = 1;
            s->执行_回溯 = 1;
            s->做_调试 = 1;
            break;
#endif
        case 选项_g:
            s->做_调试 = 1;
            break;
        case 选项_c:
            x = 输出_OBJ;
        配置输出类型:
            if (s->输出类型)
            {
            	zhi_警告("-%s: 覆盖已指定的编译器操作", 选项指针->选项名称);
            }
            s->输出类型 = x;
            break;
        case 选项_d:
            if (*选项参数 == 'D')
                s->dflag = 3;
            else if (*选项参数 == 'M')
                s->dflag = 7;
            else if (*选项参数 == 't')
                s->dflag = 16;
            else if (是数字(*选项参数))
                s->g_debug |= atoi(选项参数);
            else
                goto 不支持此选项;
            break;
        case 选项_static:
            s->静态_链接 = 1;
            break;
        case 选项_std:
            if (strcmp(选项参数, "=c11") == 0)
                s->知语言版本 = 201112;
            break;
        case 选项_shared:
            x = 输出_DLL;
            goto 配置输出类型;
        case 选项_soname:
            s->soname = 字符串增加一个宽度(选项参数);
            break;
        case 选项_o:
            if (s->输出文件名)
            {
                zhi_警告("多个 -o 选项");
                zhi_释放(s->输出文件名);
            }
            s->输出文件名 = 字符串增加一个宽度(选项参数);
            break;
        case 选项_r:
            /* 生成一个 .o 合并几个输出文件 */
            s->选项_r = 1;
            x = 输出_OBJ;
            goto 配置输出类型;
        case 选项_isystem:
            添加系统导入路径(s, 选项参数);
            break;
        case 选项_include:
            动态字符串_printf(&s->命令行_include, "#include \"%s\"\n", 选项参数);
            break;
        case 选项_nostdinc:
            s->不添加标准头 = 1;
            break;
        case 选项_nostdlib:
            s->不添加标准库 = 1;
            break;
        case 选项_run:
#ifndef 运行脚本M
            错_误("-run 在交叉编译器中不可用");
#endif
            run = 选项参数;
            x = 输出_内存;
            goto 配置输出类型;
        case 选项_v:
            do
            {
            	++s->显示详情;
            }while (*选项参数++ == 'v');
            ++无动作;
            break;
        case 选项_f:
            if (set_flag(s, options_f, 选项参数) < 0)
                goto 不支持此选项;
            break;
#ifdef 目标_ARM
        case 选项_m浮点_abi:
            /*zhi 还不支持软浮动*/
            if (!strcmp(选项参数, "softfp"))
            {
                s->浮点_abi = ARM_SOFTFP_FLOAT;
            } else if (!strcmp(选项参数, "hard"))
            {
            	s->浮点_abi = ARM_HARD_FLOAT;
            }else
            {
            	错_误("不支持的浮点abi '%s'", 选项参数);
            }
            break;
#endif
        case 选项_m:
            if (set_flag(s, options_m, 选项参数) < 0)
            {
                if (x = atoi(选项参数), x != 32 && x != 64)
                {
                	goto 不支持此选项;
                }
                if (宏_指针大小 != x/8)
                {
                	return x;
                }
                ++无动作;
            }
            break;
        case 选项_W:
            s->警告_无 = 0;
            if (选项参数[0] && set_flag(s, options_W, 选项参数) < 0)
            {
            	goto 不支持此选项;
            }
            break;
        case 选项_w:
            s->警告_无 = 1;
            break;
        case 选项_rdynamic:
            s->导出所有符号 = 1;
            break;
        case 选项_Wl:
            if (链接器参数.大小)
            {
            	--链接器参数.大小, 动态字符串_追加一个字符(&链接器参数, 英_逗号);
            }
            动态字符串_拼接(&链接器参数, 选项参数, 0);
            if (zhi_设置_链接器(s, 链接器参数.数据))
            {
            	动态字符串_释放(&链接器参数);
            }
            break;
        case 选项_Wp:
            选项 = 选项参数;
            goto 重解析;
        case 选项_E:
            x = 输出_预处理;
            goto 配置输出类型;
        case 选项_P:
            s->Pflag = atoi(选项参数) + 1;
            break;
        case 选项_M:
            s->包含的系统依赖 = 1;
            // fall through
        case 选项_MM:
            s->只是_依赖 = 1;
            if(!s->指定_依赖)
                s->指定_依赖 = 字符串增加一个宽度("-");
            // fall through
        case 选项_MMD:
            s->生成_依赖 = 1;
            break;
        case 选项_MD:
            s->生成_依赖 = 1;
            s->包含的系统依赖 = 1;
            break;
        case 选项_MF:
            s->指定_依赖 = 字符串增加一个宽度(选项参数);
            break;
        case 选项_dumpversion:
            printf ("%s\n", 版本号);
            exit(0);
            break;
        case 选项_x:
            x = 0;
            if (*选项参数 == 'c')
                x = 源文件类型_Z;
            else if (*选项参数 == 'a')
                x = 源文件类型_汇编预处理;
            else if (*选项参数 == 'b')
                x = 标志类型_BIN;
            else if (*选项参数 == 'n')
                x = 源文件类型_NONE;
            else
                zhi_警告("不支持的语言 '%s'", 选项参数);
            s->源文件类型 = x | (s->源文件类型 & ~源文件类型_MASK);
            break;
        case 选项_O:
            s->optimize = atoi(选项参数);
            break;
        case 选项_print_search_dirs:
            x = 选项返回_打印目录;
            goto 额外动作;
        case 选项_impdef:
            x = 选项返回_IMPDEF;
            goto 额外动作;
        case 选项_ar:
            x = 选项返回_AR;
        额外动作:
            参数开始 = optind - 1;/*此时optind==3；参数开始==2*/
            if (参数开始 != 无动作)
            {
            	错_误("无法在此处解析 %s ", 选项);
            }
            工具 = x;
            break;
        default:
不支持此选项:
            zhi_警告_c(警告_不支持)("不支持的选项 '%s'", 选项);
            break;
        }
    }
/**************************************************argc>=2-结束**************************************************/

    if (链接器参数.大小)
    {
        选项 = 链接器参数.数据;
        goto arg_err;
    }
    *pargc = argc - 参数开始;
    *pargv = argv + 参数开始;
    if (工具)
    {
    	return 工具;
    }
    if (optind != 无动作)/*此时optind==3*/
    {
    	return 0;
    }
    if (s->显示详情 == 2)
    {
    	return 选项返回_打印目录;
    }
    if (s->显示详情)
    {
    	return 选项返回_V;
    }
    return 选项返回_HELP;
}
核心库接口 void 设置选项(虚拟机ST *s, const char *r)
{
    char **argv = NULL;
    int argc = 0;
    参数_解析_生成argv(r, &argc, &argv);
    解析命令行参数(s, &argc, &argv, 0);
    动态数组_重置(&argv, &argc);
}

公用函数 void 打印编译信息(虚拟机ST *s1, unsigned 总时间)
{
    if (总时间 < 1)
        总时间 = 1;
    if (总字节数 < 1)
        总字节数 = 1;
    fprintf(stderr, "* %d 个标识符, %d 行, %d 个字节\n"
                    "* %0.3f 秒, %u 行/秒, %0.1f 兆字节(MB)/秒\n",
           总标识符数, 总行数, 总字节数,
           (double)总时间/1000,
           (unsigned)总行数*1000/总时间,
           (double)总字节数/1000/总时间);
    fprintf(stderr, "* 代码 %d, 数据.rw %d, 数据.ro %d, bss %d bytes\n",
           s1->总输出[0],
           s1->总输出[1],
           s1->总输出[2],
           s1->总输出[3]
           );
#ifdef 内存_调试
    fprintf(stderr, "已使用 * %d 个字节的内存\n", 内存_最大大小);
#endif
}
