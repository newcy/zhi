/****************************************************************************************************
 * 名称：语法分析，语义分析。
 * 描述：翻译单元（编译的最大单位是源文件统称为“翻译单元”） -> 外部声明列表（翻译单元 循环调用 外部声明函数 来分析 翻译单元 内的每一个 外部声明 ，） -> 外部声明（变量声明，变量定义，函数声明，函数定义等统称为“外部声明”）
 * 版权(C) 2001-2004 Fabrice Bellard
 * 版权(C) 2019-2023 位中原
 * 网址：www.880.xin
 ****************************************************************************************************/

#define 全局使用
#include "zhi.h"

/* 全局变量 */
静态数据 int 返回符号, 匿名符号索引, 指令在代码节位置, 局部变量索引;

静态数据 符号ST *全局符号栈;
静态数据 符号ST *局部符号栈;
静态数据 符号ST *define_栈;
静态数据 符号ST *全局_标签_栈;
静态数据 符号ST *局部_标签_栈;

static 符号ST *sym_free_first;
static void **sym_pools;
static int nb_sym_pools;

static 符号ST *所有_清理, *待处理_gotos;
static int 局部_范围;
static int in_sizeof;
static int in_通用的;
static int 节_符号;
静态数据 char 调试_模式;

静态数据 栈值ST *栈顶值;
static 栈值ST _栈值[1 + 栈值大小];
#define 栈值 (_栈值 + 1)

静态数据 int 需要_常量;
静态数据 int 无需生成代码;
#define 未赋值子表达式 0xffff
#define 无需静态数据输出 (无需生成代码 > 0)
#define 仅需静态数据输出 (无需生成代码 & 0xC0000000)

/* 自动代码抑制 ----> */
#define 代码_关() (无需生成代码 |= 0x20000000)
#define 代码_开() (无需生成代码 &= ~0x20000000)

static void zhi_tcov_块_开始(void);

/* 如果使用过，在标签处清除'不需要生成代码' */
静态函数 void 生成符号(int t)
{
	if (t)
	{
		生成符号_地址(t, 指令在代码节位置);
		代码_开();
	}
}
static int 生成索引(void)
{
	int t = 指令在代码节位置;
	代码_开();
	if (调试_模式)
	{
		zhi_tcov_块_开始();
	}

	return t;
}

/* 无条件跳转后设置'不需要生成代码' */
static void 生成跳转_地址_无需生成代码(int t)
{
	生成跳转_到固定地址(t);
	代码_关();
}
static int 生成跳转_无需生成代码(int t) { t = 生成到标签的跳转(t); 代码_关(); return t; }

/* 这些在此文件的末尾#undef'd */
#define 生成跳转_到固定地址 生成跳转_地址_无需生成代码
#define 生成到标签的跳转 生成跳转_无需生成代码
/* <---- */

静态数据 int 全局_表达式;  /* 如果复合文字必须全局分配（在初始化程序解析期间使用），则为 true */
静态数据 类型ST 函数_返回类型; /* 当前函数返回类型（由返回指令使用） */
静态数据 int 函数_可变参; /* 如果当前函数是可变参数则为真（由返回指令使用） */
静态数据 int func_vc;
static int 最后_行号, 新建_文件, 函数_索引; /* 调试信息控制 */
静态数据 const char *函数名;
静态数据 类型ST 整数_类型, 函数_旧_类型, 字符_类型, 字符_指针_类型;
static 动态字符串ST 初始_字符串;

#if 宏_指针大小 == 4
#define 值的数据类型_SIZE_T (数据类型_整数 | 存储类型_无符号的)
#define 值的数据类型_PTRDIFF_T 数据类型_整数
#elif 长整数大小 == 4
#define 值的数据类型_SIZE_T (数据类型_长长整数 | 存储类型_无符号的)
#define 值的数据类型_PTRDIFF_T 数据类型_长长整数
#else
#define 值的数据类型_SIZE_T (数据类型_长整数 | 数据类型_长长整数 | 存储类型_无符号的)
#define 值的数据类型_PTRDIFF_T (数据类型_长整数 | 数据类型_长长整数)
#endif

static struct 选择S
{/*选择（switch）数据结构定义*/
    struct case_t {
        int64_t v1, v2;
	int sym;
    } **p; int n; /* 分支范围列表 */
    int 默认_符号; /* 默认符号 */
    int *bsym;
    struct 域S *域S;
    struct 选择S *上个;
    栈值ST sv;
} *当前switch;

#define 最大_临时_局部_变量_数量 8
/*当前函数中堆栈上的临时局部变量列表。 */
static struct 临时局部变量S {
	int 地点; //栈上偏移量. Svalue.c.i
	short 大小;
	short 对齐;
} 临时局部变量数组[最大_临时_局部_变量_数量];
static int 临时局部变量数;

static struct 域S {
    struct 域S *上个;
    struct { int 局部变量索引, locorig, num; } vla;
    struct { 符号ST *s; int n; } cl;
    int *bsym, *csym;
    符号ST *lstk, *llstk;
} *当前域, *循环域, *根域;

typedef struct {
    节ST *sec;
    int 局部_偏移量;
    符号ST *弹性_数组_参考;
} 初始参数S;

#if 1
#define 优先_解析
static void 初始化_优先级(void);
#endif

/********************************************************/
/* stab debug support */

static const struct {
  int 类型;
  const char *name;
} 默认_调试[] = {
    {   数据类型_整数, "int:t1=r1;-2147483648;2147483647;" },
    {   数据类型_字节, "char:t2=r2;0;127;" },
#if 长整数大小 == 4
    {   数据类型_长整数 | 数据类型_整数, "long int:t3=r3;-2147483648;2147483647;" },
#else
    {   数据类型_长长整数 | 数据类型_长整数, "long int:t3=r3;-9223372036854775808;9223372036854775807;" },
#endif
    {   数据类型_整数 | 存储类型_无符号的, "unsigned int:t4=r4;0;037777777777;" },
#if 长整数大小 == 4
    {   数据类型_长整数 | 数据类型_整数 | 存储类型_无符号的, "long unsigned int:t5=r5;0;037777777777;" },
#else
    /* use octal instead of -1 so size_t works (-gstabs+ in gcc) */
    {   数据类型_长长整数 | 数据类型_长整数 | 存储类型_无符号的, "long unsigned int:t5=r5;0;01777777777777777777777;" },
#endif
    {   数据类型_128位整数, "__int128:t6=r6;0;-1;" },
    {   数据类型_128位整数 | 存储类型_无符号的, "__int128 unsigned:t7=r7;0;-1;" },
    {   数据类型_长长整数, "long long int:t8=r8;-9223372036854775808;9223372036854775807;" },
    {   数据类型_长长整数 | 存储类型_无符号的, "long long unsigned int:t9=r9;0;01777777777777777777777;" },
    {   数据类型_短整数, "short int:t10=r10;-32768;32767;" },
    {   数据类型_短整数 | 存储类型_无符号的, "short unsigned int:t11=r11;0;65535;" },
    {   数据类型_字节 | 存储类型_明确有无符号, "signed char:t12=r12;-128;127;" },
    {   数据类型_字节 | 存储类型_明确有无符号 | 存储类型_无符号的, "unsigned char:t13=r13;0;255;" },
    {   数据类型_浮点, "float:t14=r1;4;0;" },
    {   数据类型_双精度, "double:t15=r1;8;0;" },
#ifdef 为长双精度使用双精度M
    {   数据类型_双精度 | 数据类型_长整数, "long double:t16=r1;8;0;" },
#else
    {   数据类型_长双精度, "long double:t16=r1;16;0;" },
#endif
    {   -1, "_Float32:t17=r1;4;0;" },
    {   -1, "_Float64:t18=r1;8;0;" },
    {   -1, "_Float128:t19=r1;16;0;" },
    {   -1, "_Float32x:t20=r1;8;0;" },
    {   -1, "_Float64x:t21=r1;16;0;" },
    {   -1, "_Decimal32:t22=r1;4;0;" },
    {   -1, "_Decimal64:t23=r1;8;0;" },
    {   -1, "_Decimal128:t24=r1;16;0;" },
    /* if default char is unsigned */
    {   数据类型_字节 | 存储类型_无符号的, "unsigned char:t25=r25;0;255;" },
    /* boolean 类型 */
    {   数据类型_布尔, "bool:t26=r26;0;255;" },
    {   数据类型_无类型, "void:t27=27" },
};

static int 调试_下个_类型;

static struct debug_hash {
    int debug_type;
    符号ST *类型;
} *debug_hash;

static int n_debug_hash;

static struct debug_info {
    int start;
    int end;
    int n_sym;
    struct debug_sym {
        int 类型;
        unsigned long value;
        char *字符串;
        节ST *sec;
        int 符号_索引;
    } *sym;
    struct debug_info *child, *next, *last, *parent;
} *debug_info, *debug_info_root;

static struct {
    unsigned long offset;
    unsigned long last_file_name;
    unsigned long last_func_name;
    int 指令在代码节位置;
    int line;
} tcov_data;

/********************************************************/
static void 生成_转换(类型ST *类型);
static void 生成_转换_s(int t);
static inline 类型ST *返回指针类型(类型ST *类型);
static int 是兼容类型(类型ST *类型1, 类型ST *type2);
static int 解析声明说明符(类型ST *类型, GNUC属性ST *ad);
static 类型ST *解析初始声明符列表(类型ST *类型, GNUC属性ST *ad, int *v, int td);
static void 解析_表达式_类型(类型ST *类型);
static void 初始存储值(初始参数S *p, 类型ST *类型, unsigned long c);
static void 声明初始化(初始参数S *p, 类型ST *类型, unsigned long c, int flags);
static void 块(int 是_表达式);
static void 声明符初始化值分配(类型ST *类型, GNUC属性ST *ad, int r, int 初始哈希值, int v, int 域S);
static int 外部声明(int l, int 是_for_循环_初始化, 符号ST *);
static void 等于表达式(void);
static void vla_运行时类型大小(类型ST *类型, int *a);
static int 是兼容的非限定类型(类型ST *类型1, 类型ST *type2);
static inline int64_t 常量表达式64(void);
static void 把64位整数常量压入栈(int ty, unsigned long long v);
static void vpush(类型ST *类型);
static int 生成值测试(int inv, int t);
static void 生成内联函数(虚拟机ST *s);
static void 释放内联函数(虚拟机ST *s);
static void 跳过或保存块(单词字符串ST **字符串);
static void 堆栈条目转换为寄存器值复制到另个寄存器(void);
static int 取_临时_局部_变量(int 大小,int 对齐);
static void 清除_临时_局部_变量列表();
static void 转换_错误(类型ST *st, 类型ST *dt);

ST_INLN int 是_浮点(int t)
{
    int bt = t & 数据类型_基本类型;
    return bt == 数据类型_长双精度
        || bt == 数据类型_双精度
        || bt == 数据类型_浮点
        || bt == 数据类型_128位浮点;
}

static inline int 是_整数_基本类型(int bt)
{
    return bt == 数据类型_字节
        || bt == 数据类型_布尔
        || bt == 数据类型_短整数
        || bt == 数据类型_整数
        || bt == 数据类型_长长整数;
}

static int 基本类型_大小(int bt)
{
    return bt == 数据类型_字节 || bt == 数据类型_布尔 ? 1 :
        bt == 数据类型_短整数 ? 2 :
        bt == 数据类型_整数 ? 4 :
        bt == 数据类型_长长整数 ? 8 :
        bt == 数据类型_指针 ? 宏_指针大小 : 0;
}

static int 根据t返回函数返回寄存器(int t)
{
    if (!是_浮点(t))
        return REG_IRET;
#ifdef 目标_X86_64
    if ((t & 数据类型_基本类型) == 数据类型_长双精度)
        return TREG_ST0;
#endif
    return REG_FRET;
}

static int 返回第二个函数返回寄存器(int t)
{
    t &= 数据类型_基本类型;
#if 宏_指针大小 == 4
    if (t == 数据类型_长长整数)
        return REG_IRE2;
#elif defined 目标_X86_64
    if (t == 数据类型_128位整数)
        return REG_IRE2;
    if (t == 数据类型_128位浮点)
        return REG_FRE2;
#endif
    return 存储类型_VC常量;
}

/* 对于二字类型返回 true */
#define USING_TWO_WORDS(t) (返回第二个函数返回寄存器(t) != 存储类型_VC常量)

static void 将函数返回寄存器放入堆栈值(栈值ST *sv, int t)
{
    sv->寄存qi = 根据t返回函数返回寄存器(t), sv->r2 = 返回第二个函数返回寄存器(t);
}

static int 返回t的函数寄存器类(int t)
{
    return reg_classes[根据t返回函数返回寄存器(t)] & ~(RC_FLOAT | RC_INT);
}

static int 返回t的泛型寄存器类(int t)
{
    if (!是_浮点(t))
        return RC_INT;
#ifdef 目标_X86_64
    if ((t & 数据类型_基本类型) == 数据类型_长双精度)
        return RC_ST0;
    if ((t & 数据类型_基本类型) == 数据类型_128位浮点)
        return RC_FRET;
#elif defined 目标_RISCV64
    if ((t & 数据类型_基本类型) == 数据类型_长双精度)
        return RC_INT;
#endif
    return RC_FLOAT;
}

static int 返回对应于t和rc的第二个寄存器类(int t, int rc)
{
    if (!USING_TWO_WORDS(t))
        return 0;
#ifdef RC_IRE2
    if (rc == RC_IRET)
        return RC_IRE2;
#endif
#ifdef RC_FRE2
    if (rc == RC_FRET)
        return RC_FRE2;
#endif
    if (rc & RC_FLOAT)
        return RC_FLOAT;
    return RC_INT;
}

/*我们使用自己的“有限”函数来避免非标准数学库的潜在问题。XXX：依赖于endianness */
静态函数 int ieee_finite(double d)
{
    int p[4];
    memcpy(p, &d, sizeof(double));
    return ((unsigned)((p[1] | 0x800fffff) + 1)) >> 31;
}

/* 本地编译intel long double */
#if (defined __i386__ || defined __x86_64__) \
    && (defined 目标_I386 || defined 目标_X86_64)
# define ZHI_IS_NATIVE_387
#endif

静态函数 void 测试_左值(void)
{
    if (!(栈顶值->寄存qi & 存储类型_左值))
        应为("左值");
}

静态函数 void 检查栈值(void)
{
    if (栈顶值 != 栈值 - 1)
        错_误("内部编译器错误: 栈值 leak (%d)",
                  (int)(栈顶值 - 栈值 + 1));
}


/* ------------------------------------------------------------------------- */
静态函数 void 翻译单元信息开始(虚拟机ST *s1)
{
    if (s1->做_调试)
    {
        int i;
        char buf[512];
        /* 文件信息：完整路径+文件名 */
        节_符号 = 取符号索引(节符号表_数组,0,0,ELFW(ST_INFO)(STB_LOCAL, STT_SECTION),0,代码_节->ELF节数量,NULL);
        getcwd(buf, sizeof(buf));/*getcwd()会将当前工作目录的绝对路径复制到参数buf所指的内存空间中,参数sizeof(buf)为buf的空间大小。*/
#ifdef _WIN32
        斜杠规范(buf);
#endif
        截取后拼接(buf, sizeof(buf), "/");
        放置_符号表调试_重定位(s1,buf,N_SO,0,0, 代码_节->当前数据_偏移量,代码_节,节_符号);
        放置_符号表调试_重定位(s1,文件->上个 ? 文件->上个->文件名 : 文件->文件名,N_SO,0,0,代码_节->当前数据_偏移量,代码_节,节_符号);
        for (i = 0; i < sizeof (默认_调试) / sizeof (默认_调试[0]); i++)
        {
        	放置符号表调试信息(s1, 默认_调试[i].name, N_LSYM, 0, 0, 0);
        }
        新建_文件 = 最后_行号 = 0;
        函数_索引 = -1;
        调试_下个_类型 = sizeof(默认_调试) / sizeof(默认_调试[0]);
        debug_hash = NULL;
        n_debug_hash = 0;

        /* 我们目前正在“包括”<命令行> */
        zhi_调试_include的开始(s1);
    }
    /* 必须放置一个 STT_FILE 类型的elf符号，以便可以安全地使用 STB_LOCAL 符号*/
    取符号索引(节符号表_数组,0,0,ELFW(ST_INFO)(STB_LOCAL, STT_FILE),0,SHN_ABS,文件->文件名);
}

/* put 翻译单元信息结束 */
静态函数 void 翻译单元信息结束(虚拟机ST *s1)
{
    if (!s1->做_调试)
    {
    	return;
    }
    放置_符号表调试_重定位(s1, NULL, N_SO, 0, 0,代码_节->当前数据_偏移量, 代码_节, 节_符号);
    zhi_释放(debug_hash);
}

static 缓冲文件ST* put_new_file(虚拟机ST *s1)
{
    缓冲文件ST *f = 文件;
    /* 如果来自内联，则使用上部文件“：asm:” */
    if (f->文件名[0] == 英_冒号)
    {
    	f = f->上个;
    }
    if (f && 新建_文件)
    {
        放置_符号表调试_重定位(s1, f->文件名, N_SOL, 0, 0, 指令在代码节位置, 代码_节, 节_符号);
        新建_文件 = 最后_行号 = 0;
    }
    return f;
}

/* put 替代文件 */
静态函数 void zhi_debug_putfile(虚拟机ST *s1, const char *文件名)
{
    if (0 == strcmp(文件->文件名, 文件名))
        return;
    截取前n个字符(文件->文件名, sizeof(文件->文件名), 文件名);
    新建_文件 = 1;
}

/* begin of #include */
静态函数 void zhi_调试_include的开始(虚拟机ST *s1)
{
    if (!s1->做_调试)
        return;
    放置符号表调试信息(s1, 文件->文件名, N_BINCL, 0, 0, 0);
    新建_文件 = 1;
}

/* end of #include */
静态函数 void 添加包含文件调试信息的结束(虚拟机ST *s1)
{
    if (!s1->做_调试)
        return;
    put_stabn(s1, N_EINCL, 0, 0, 0);
    新建_文件 = 1;
}

/* generate line number info */
static void zhi_debug_line(虚拟机ST *s1)
{
    缓冲文件ST *f;
    if (!s1->做_调试
        || 当前代码节 != 代码_节
        || !(f = put_new_file(s1))
        || 最后_行号 == f->当前行号)
        return;
    if (函数_索引 != -1) {
        put_stabn(s1, N_SLINE, 0, f->当前行号, 指令在代码节位置 - 函数_索引);
    } else {
        /* from 汇编当前文件 */
        放置_符号表调试_重定位(s1, NULL, N_SLINE, 0, f->当前行号, 指令在代码节位置, 代码_节, 节_符号);
    }
    最后_行号 = f->当前行号;
}

static void zhi_debug_stabs (虚拟机ST *s1, const char *字符串, int 类型, unsigned long value,
                             节ST *sec, int 符号_索引)
{
    struct debug_sym *s;

    if (debug_info) {
        debug_info->sym =
            (struct debug_sym *)zhi_重新分配 (debug_info->sym,
                                             sizeof(struct debug_sym) *
                                             (debug_info->n_sym + 1));
        s = debug_info->sym + debug_info->n_sym++;
        s->类型 = 类型;
        s->value = value;
        s->字符串 = 字符串增加一个宽度(字符串);
        s->sec = sec;
        s->符号_索引 = 符号_索引;
    }
    else if (sec)
        放置_符号表调试_重定位 (s1, 字符串, 类型, 0, 0, value, sec, 符号_索引);
    else
        放置符号表调试信息 (s1, 字符串, 类型, 0, 0, value);
}

static void zhi_debug_stabn(虚拟机ST *s1, int 类型, int value)
{
    if (!s1->做_调试)
        return;
    if (类型 == N_LBRAC) {
        struct debug_info *info =
            (struct debug_info *) 初始化内存(sizeof (*info));

        info->start = value;
        info->parent = debug_info;
        if (debug_info) {
            if (debug_info->child) {
                if (debug_info->child->last)
                    debug_info->child->last->next = info;
                else
                    debug_info->child->next = info;
                debug_info->child->last = info;
            }
            else
                debug_info->child = info;
        }
        else
            debug_info_root = info;
        debug_info = info;
    }
    else {
        debug_info->end = value;
        debug_info = debug_info->parent;
    }
}

static void zhi_get_debug_info(虚拟机ST *s1, 符号ST *s, 动态字符串ST *result)
{
    int 类型;
    int n = 0;
    int debug_type = -1;
    符号ST *t = s;
    动态字符串ST 字符串;

    for (;;) {
        类型 = t->类型.数据类型 & ~(值的数据类型_贮存 | 存储类型_常量 | 存储类型_易变);
        if ((类型 & 数据类型_基本类型) != 数据类型_字节)
            类型 &= ~存储类型_明确有无符号;
        if (类型 == 数据类型_指针 || 类型 == (数据类型_指针 | 数据类型_数组))
            n++, t = t->类型.引用符号;
        else
            break;
    }
    if ((类型 & 数据类型_基本类型) == 数据类型_结构体) {
        int i;

        t = t->类型.引用符号;
        for (i = 0; i < n_debug_hash; i++) {
            if (t == debug_hash[i].类型) {
                debug_type = debug_hash[i].debug_type;
                break;
            }
        }
        if (debug_type == -1) {
            debug_type = ++调试_下个_类型;
            debug_hash = (struct debug_hash *)
                zhi_重新分配 (debug_hash,
                             (n_debug_hash + 1) * sizeof(*debug_hash));
            debug_hash[n_debug_hash].debug_type = debug_type;
            debug_hash[n_debug_hash++].类型 = t;
            动态字符串_新建 (&字符串);
            动态字符串_printf (&字符串, "%s:T%d=%c%d",
                         (t->v & ~符号_结构体) >= 第一个匿名符号
                         ? "" : 取单词字符串(t->v & ~符号_结构体, NULL),
                         debug_type,
                         IS_UNION (t->类型.数据类型) ? 'u' : 's',
                         t->c);
            while (t->next) {
                int pos, 大小, 对齐;

                t = t->next;
                动态字符串_printf (&字符串, "%s:",
                             (t->v & ~符号_字段) >= 第一个匿名符号
                             ? "" : 取单词字符串(t->v & ~符号_字段, NULL));
                zhi_get_debug_info (s1, t, &字符串);
                if (t->类型.数据类型 & 存储类型_位域) {
                    pos = t->c * 8 + BIT_POS(t->类型.数据类型);
                    大小 = BIT_SIZE(t->类型.数据类型);
                }
                else {
                    pos = t->c * 8;
                    大小 = 类型_大小(&t->类型, &对齐) * 8;
                }
                动态字符串_printf (&字符串, ",%d,%d;", pos, 大小);
            }
            动态字符串_printf (&字符串, ";");
            zhi_debug_stabs(s1, 字符串.数据, N_LSYM, 0, NULL, 0);
            动态字符串_释放 (&字符串);
        }
    }
    else if (是_枚举(类型)) {
        符号ST *e = t = t->类型.引用符号;

        debug_type = ++调试_下个_类型;
        动态字符串_新建 (&字符串);
        动态字符串_printf (&字符串, "%s:T%d=e",
                     (t->v & ~符号_结构体) >= 第一个匿名符号
                     ? "" : 取单词字符串(t->v & ~符号_结构体, NULL),
                     debug_type);
        while (t->next) {
            t = t->next;
            动态字符串_printf (&字符串, "%s:",
                         (t->v & ~符号_字段) >= 第一个匿名符号
                         ? "" : 取单词字符串(t->v & ~符号_字段, NULL));
            动态字符串_printf (&字符串, e->类型.数据类型 & 存储类型_无符号的 ? "%u," : "%d,",
                         (int)t->enum_val);
        }
        动态字符串_printf (&字符串, ";");
        zhi_debug_stabs(s1, 字符串.数据, N_LSYM, 0, NULL, 0);
        动态字符串_释放 (&字符串);
    }
    else if ((类型 & 数据类型_基本类型) != 数据类型_函数) {
        类型 &= ~值的数据类型_结构体_MASK;
        for (debug_type = 1;
             debug_type <= sizeof(默认_调试) / sizeof(默认_调试[0]);
             debug_type++)
            if (默认_调试[debug_type - 1].类型 == 类型)
                break;
        if (debug_type > sizeof(默认_调试) / sizeof(默认_调试[0]))
            return;
    }
    if (n > 0)
        动态字符串_printf (result, "%d=", ++调试_下个_类型);
    t = s;
    for (;;) {
        类型 = t->类型.数据类型 & ~(值的数据类型_贮存 | 存储类型_常量 | 存储类型_易变);
        if ((类型 & 数据类型_基本类型) != 数据类型_字节)
            类型 &= ~存储类型_明确有无符号;
        if (类型 == 数据类型_指针)
            动态字符串_printf (result, "%d=*", ++调试_下个_类型);
        else if (类型 == (数据类型_指针 | 数据类型_数组))
            动态字符串_printf (result, "%d=ar1;0;%d;",
                         ++调试_下个_类型, t->类型.引用符号->c - 1);
        else if (类型 == 数据类型_函数) {
            动态字符串_printf (result, "%d=f", ++调试_下个_类型);
            zhi_get_debug_info (s1, t->类型.引用符号, result);
            return;
        }
        else
            break;
        t = t->类型.引用符号;
    }
    动态字符串_printf (result, "%d", debug_type);
}

static void zhi_debug_finish (虚拟机ST *s1, struct debug_info *cur)
{
    while (cur) {
        int i;
        struct debug_info *next = cur->next;

        for (i = 0; i < cur->n_sym; i++) {
            struct debug_sym *s = &cur->sym[i];

            if (s->sec)
                放置_符号表调试_重定位(s1, s->字符串, s->类型, 0, 0, s->value,
                            s->sec, s->符号_索引);
            else
                放置符号表调试信息(s1, s->字符串, s->类型, 0, 0, s->value);
            zhi_释放 (s->字符串);
        }
        zhi_释放 (cur->sym);
        put_stabn(s1, N_LBRAC, 0, 0, cur->start);
        zhi_debug_finish (s1, cur->child);
        put_stabn(s1, N_RBRAC, 0, 0, cur->end);
        zhi_释放 (cur);
        cur = next;
    }
}

static void zhi_add_debug_info(虚拟机ST *s1, int param, 符号ST *s, 符号ST *e)
{
    动态字符串ST debug_str;
    if (!s1->做_调试)
        return;
    动态字符串_新建 (&debug_str);
    for (; s != e; s = s->上个) {
        if (!s->v || (s->寄存qi & 存储类型_掩码) != 存储类型_局部)
            continue;
        动态字符串_重置 (&debug_str);
        动态字符串_printf (&debug_str, "%s:%s", 取单词字符串(s->v, NULL), param ? "p" : "");
        zhi_get_debug_info(s1, s, &debug_str);
        zhi_debug_stabs(s1, debug_str.数据, param ? N_PSYM : N_LSYM, s->c, NULL, 0);
    }
    动态字符串_释放 (&debug_str);
}

/* put function symbol */
static void 取函数符号(虚拟机ST *s1, 符号ST *sym)
{
    动态字符串ST debug_str;
    缓冲文件ST *f;
    if (!s1->做_调试)
        return;
    debug_info_root = NULL;
    debug_info = NULL;
    zhi_debug_stabn(s1, N_LBRAC, 指令在代码节位置 - 函数_索引);
    if (!(f = put_new_file(s1)))
        return;
    动态字符串_新建 (&debug_str);
    动态字符串_printf(&debug_str, "%s:%c", 函数名, sym->类型.数据类型 & 存储类型_静态 ? 'f' : 'F');
    zhi_get_debug_info(s1, sym->类型.引用符号, &debug_str);
    放置_符号表调试_重定位(s1, debug_str.数据, N_FUN, 0, f->当前行号, 0, 当前代码节, sym->c);
    动态字符串_释放 (&debug_str);

    zhi_debug_line(s1);
}

/* put function 大小 */
static void 函数结束(虚拟机ST *s1, int 大小)
{
    if (!s1->做_调试)
        return;
    zhi_debug_stabn(s1, N_RBRAC, 大小);
    zhi_debug_finish (s1, debug_info_root);
}


static void zhi_debug_extern_sym(虚拟机ST *s1, 符号ST *sym, int ELF节数量, int sym_bind, int sym_type)
{
    节ST *s;
    动态字符串ST 字符串;

    if (!s1->做_调试)
        return;
    if (sym_type == STT_FUNC || sym->v >= 第一个匿名符号)
        return;
    s = s1->节数组[ELF节数量];

    动态字符串_新建 (&字符串);
    动态字符串_printf (&字符串, "%s:%c",
        取单词字符串(sym->v, NULL),
        sym_bind == STB_GLOBAL ? 'G' : 局部_范围 ? 'V' : 'S'
        );
    zhi_get_debug_info(s1, sym, &字符串);
    if (sym_bind == STB_GLOBAL)
        zhi_debug_stabs(s1, 字符串.数据, N_GSYM, 0, NULL, 0);
    else
        zhi_debug_stabs(s1, 字符串.数据,
            (sym->类型.数据类型 & 存储类型_静态) && 数据_节 == s
            ? N_STSYM : N_LCSYM, 0, s, sym->c);
    动态字符串_释放 (&字符串);
}

static void zhi_debug_typedef(虚拟机ST *s1, 符号ST *sym)
{
    动态字符串ST 字符串;
    if (!s1->做_调试)
    {
    	return;
    }
    动态字符串_新建 (&字符串);
    动态字符串_printf (&字符串, "%s:t",(sym->v & ~符号_字段) >= 第一个匿名符号 ? "" : 取单词字符串(sym->v & ~符号_字段, NULL));
    zhi_get_debug_info(s1, sym, &字符串);
    zhi_debug_stabs(s1, 字符串.数据, N_LSYM, 0, NULL, 0);
    动态字符串_释放 (&字符串);
}

/* ------------------------------------------------------------------------- */
/* 节的布局详见 ：库/tcov.c */

static void zhi_tcov_block_end(int line)
{
    if (全局虚拟机->测试_覆盖率 == 0)
    {
    	return;
    }
    if (tcov_data.offset)
    {
		void *ptr = 测试节覆盖_数组->数据 + tcov_data.offset;
		unsigned long long nline = line ? line : 文件->当前行号;

		write64le (ptr, (read64le (ptr) & 0xfffffffffull) | (nline << 36));
		tcov_data.offset = 0;
    }
}

static void zhi_tcov_块_开始(void)
{
    栈值ST sv;
    void *ptr;
    unsigned long last_offset = tcov_data.offset;

    zhi_tcov_block_end (0);
    if (全局虚拟机->测试_覆盖率 == 0 || 无需生成代码)
	return;

    if (tcov_data.last_file_name == 0 ||
	strcmp ((const char *)(测试节覆盖_数组->数据 + tcov_data.last_file_name),
		文件->真文件名) != 0) {
	char wd[1024];
	动态字符串ST cstr;

	if (tcov_data.last_func_name)
	    节_预留指定大小内存(测试节覆盖_数组, 1);
	if (tcov_data.last_file_name)
	    节_预留指定大小内存(测试节覆盖_数组, 1);
	tcov_data.last_func_name = 0;
	动态字符串_新建 (&cstr);
	if (文件->真文件名[0] == '/') {
	    tcov_data.last_file_name = 测试节覆盖_数组->当前数据_偏移量;
	    动态字符串_printf (&cstr, "%s", 文件->真文件名);
	}
	else {
	    getcwd (wd, sizeof(wd));
	    tcov_data.last_file_name = 测试节覆盖_数组->当前数据_偏移量 + strlen(wd) + 1;
	    动态字符串_printf (&cstr, "%s/%s", wd, 文件->真文件名);
	}
	ptr = 节_预留指定大小内存(测试节覆盖_数组, cstr.大小 + 1);
	strcpy((char *)ptr, cstr.数据);
#ifdef _WIN32
        斜杠规范((char *)ptr);
#endif
	动态字符串_释放 (&cstr);
    }
    if (tcov_data.last_func_name == 0 ||
	strcmp ((const char *)(测试节覆盖_数组->数据 + tcov_data.last_func_name),
		函数名) != 0) {
	size_t 长度;

	if (tcov_data.last_func_name)
	    节_预留指定大小内存(测试节覆盖_数组, 1);
	tcov_data.last_func_name = 测试节覆盖_数组->当前数据_偏移量;
	长度 = strlen (函数名);
	ptr = 节_预留指定大小内存(测试节覆盖_数组, 长度 + 1);
	strcpy((char *)ptr, 函数名);
	节_预留指定大小内存(测试节覆盖_数组, -测试节覆盖_数组->当前数据_偏移量 & 7);
	ptr = 节_预留指定大小内存(测试节覆盖_数组, 8);
	write64le (ptr, 文件->当前行号);
    }
    if (指令在代码节位置 == tcov_data.指令在代码节位置 && tcov_data.line == 文件->当前行号)
        tcov_data.offset = last_offset;
    else {
        符号ST label = {0};
        label.类型.数据类型 = 数据类型_长长整数 | 存储类型_静态;

        ptr = 节_预留指定大小内存(测试节覆盖_数组, 16);
        tcov_data.line = 文件->当前行号;
        write64le (ptr, (tcov_data.line << 8) | 0xff);
        取外部符号(&label, 测试节覆盖_数组,
		       ((unsigned char *)ptr - 测试节覆盖_数组->数据) + 8, 0);
        sv.类型 = label.类型;
        sv.寄存qi = 存储类型_符号 | 存储类型_左值 | 存储类型_VC常量;
        sv.r2 = 存储类型_VC常量;
        sv.c.i = 0;
        sv.sym = &label;
#if defined 目标_I386 || defined 目标_X86_64 || \
    defined 目标_ARM || defined 目标_ARM64 || \
    defined 目标_RISCV64
        gen_increment_tcov (&sv);
#else
        值压入栈值(&sv);
        inc(0, TOK_自加);
        弹出堆栈值();
#endif
        tcov_data.offset = (unsigned char *)ptr - 测试节覆盖_数组->数据;
        tcov_data.指令在代码节位置 = 指令在代码节位置;
    }
}



static void zhi_tcov_check_line(int start)
{
    if (全局虚拟机->测试_覆盖率 == 0)
	return;
    if (tcov_data.line != 文件->当前行号) {
        if ((tcov_data.line + 1) != 文件->当前行号) {
	    zhi_tcov_block_end (tcov_data.line);
	    if (start)
                zhi_tcov_块_开始 ();
	}
	else
	    tcov_data.line = 文件->当前行号;
    }
}

static void 测试覆盖_开始(void)
{
    if (全局虚拟机->测试_覆盖率 == 0)
	return;
    memset (&tcov_data, 0, sizeof (tcov_data));
    if (测试节覆盖_数组 == NULL) {
        测试节覆盖_数组 = 节_新建(全局虚拟机, ".tcov", SHT_程序数据,
				   SHF_执行时占用内存 | SHF_可写);
	节_预留指定大小内存(测试节覆盖_数组, 4); // pointer to executable name
    }
}

static void 测试覆盖_结束(void)
{
    if (全局虚拟机->测试_覆盖率 == 0)
	return;
    if (tcov_data.last_func_name)
        节_预留指定大小内存(测试节覆盖_数组, 1);
    if (tcov_data.last_file_name)
        节_预留指定大小内存(测试节覆盖_数组, 1);
}

/* ------------------------------------------------------------------------- */
/* 初始化 栈值 和类型。 对于 zhi -E 也必须这样做 */
静态函数 void 语法分析初始化(虚拟机ST *s1)
{
    栈顶值 = 栈值 - 1;
    memset(栈顶值, 0, sizeof *栈顶值);

    /* 定义一些经常使用的类型 */
    整数_类型.数据类型 = 数据类型_整数;

    字符_类型.数据类型 = 数据类型_字节;
    if (s1->字符_是_无符号的)
    {
    	字符_类型.数据类型 |= 存储类型_无符号的;
    }
    字符_指针_类型 = 字符_类型;
    生成指针类型(&字符_指针_类型);

    函数_旧_类型.数据类型 = 数据类型_函数;
    函数_旧_类型.引用符号 = 将符号放入符号栈(符号_字段, &整数_类型, 0, 0);
    函数_旧_类型.引用符号->f.函数调用约定 = FUNC_CDECL;
    函数_旧_类型.引用符号->f.函数_类型= 函数原型_旧;
#ifdef 优先_解析
    初始化_优先级();
#endif
    动态字符串_新建(&初始_字符串);
}

静态函数 int 语法分析开始(虚拟机ST *s1)
{
    当前代码节 = NULL;
    函数名 = "";
    匿名符号索引 = 第一个匿名符号;
    节_符号 = 0;
    需要_常量 = 0;
    无需生成代码 = 0x80000000;
    局部_范围 = 0;
    调试_模式 = s1->做_调试 | s1->测试_覆盖率 << 1;
    翻译单元信息开始(s1);
    测试覆盖_开始();
#ifdef INCLUDE调试
    printf("%s: **** 新文件\n", 文件->文件名);
#endif
    解析标志 = 解析标志_预处理 | 解析标志_数字 | 解析标志_单词字符串;
    取下个单词();/* 读取源代码的第一个词法单元 ，开始进行语法分析  */
    外部声明(存储类型_VC常量, 0, NULL);
    生成内联函数(s1);
    检查栈值();
    翻译单元信息结束(s1);
    测试覆盖_结束();
    return 0;
}

静态函数 void 语法分析_完成(虚拟机ST *s1)
{
    动态字符串_释放(&初始_字符串);
    释放内联函数(s1);
    符号弹出(&全局符号栈, NULL, 0);
    符号弹出(&局部符号栈, NULL, 0);
    /* 释放预处理宏 */
    释放define(NULL);
    /* 释放 sym_pools */
    动态数组_重置(&sym_pools, &nb_sym_pools);
    sym_free_first = NULL;
}

/* ------------------------------------------------------------------------- */
静态函数 ElfSym *elfsym(符号ST *s)
{
  if (!s || !s->c)
  {
	  return NULL;
  }
  return &((ElfSym *)节符号表_数组->数据)[s->c];
}

/* 将存储属性应用于elf符号 */
静态函数 void update_storage(符号ST *sym)
{
    ElfSym *esym;
    int sym_bind, old_sym_bind;

    esym = elfsym(sym);
    if (!esym)
        return;

    if (sym->a.能见度)
        esym->st_other = (esym->st_other & ~ELFW(ST_VISIBILITY)(-1))
            | sym->a.能见度;

    if (sym->类型.数据类型 & (存储类型_静态 | 存储类型_内联))
        sym_bind = STB_LOCAL;
    else if (sym->a.weak)
        sym_bind = STB_WEAK;
    else
        sym_bind = STB_GLOBAL;
    old_sym_bind = ELFW(ST_BIND)(esym->st_info);
    if (sym_bind != old_sym_bind) {
        esym->st_info = ELFW(ST_INFO)(sym_bind, ELFW(ST_TYPE)(esym->st_info));
    }

#ifdef 目标_PE
    if (sym->a.dllimport)
        esym->st_other |= ST_PE_IMPORT;
    if (sym->a.dllexport)
        esym->st_other |= ST_PE_EXPORT;
#endif
}

/* ------------------------------------------------------------------------- */
/* 更新 sym->c 使其指向 'section' 节中值为 'value' 的外部符号 */

静态函数 void 取外部符号2(符号ST *sym, int ELF节数量,addr_t value, unsigned long 大小,int can_add_underscore)
{
    int sym_type, sym_bind, info, other, t;
    ElfSym *esym;
    const char *name;
    char buf1[256];

    if (!sym->c) {
        name = 取单词字符串(sym->v, NULL);
        t = sym->类型.数据类型;
        if ((t & 数据类型_基本类型) == 数据类型_函数) {
            sym_type = STT_FUNC;
        } else if ((t & 数据类型_基本类型) == 数据类型_无类型) {
            sym_type = STT_NOTYPE;
            if ((t & (数据类型_基本类型|值的数据类型_ASM_FUNC)) == 值的数据类型_ASM_FUNC)
                sym_type = STT_FUNC;
        } else {
            sym_type = STT_OBJECT;
        }
        if (t & (存储类型_静态 | 存储类型_内联))
            sym_bind = STB_LOCAL;
        else
            sym_bind = STB_GLOBAL;
        other = 0;

#ifdef 目标_PE
        if (sym_type == STT_FUNC && sym->类型.引用符号) {
            符号ST *引用符号 = sym->类型.引用符号;
            if (引用符号->a.节点) {
                can_add_underscore = 0;
            }
            if (引用符号->f.函数调用约定 == FUNC_STDCALL && can_add_underscore) {
                sprintf(buf1, "_%s@%d", name, 引用符号->f.函数_参数 * 宏_指针大小);
                name = buf1;
                other |= ST_PE_STDCALL;
                can_add_underscore = 0;
            }
        }
#endif

        if (sym->asm_label) {
            name = 取单词字符串(sym->asm_label, NULL);
            can_add_underscore = 0;
        }

        if (全局虚拟机->前导下划线 && can_add_underscore) {
            buf1[0] = '_';
            截取前n个字符(buf1 + 1, sizeof(buf1) - 1, name);
            name = buf1;
        }

        info = ELFW(ST_INFO)(sym_bind, sym_type);
        sym->c = 取符号索引(节符号表_数组, value, 大小, info, other, ELF节数量, name);

        if (调试_模式)
            zhi_debug_extern_sym(全局虚拟机, sym, ELF节数量, sym_bind, sym_type);

    } else {
        esym = elfsym(sym);
        esym->st_value = value;
        esym->st_size = 大小;
        esym->st_shndx = ELF节数量;
    }
    update_storage(sym);
}

静态函数 void 取外部符号(符号ST *sym, 节ST *当前的代码节,addr_t 指令在代码节的位置, unsigned long 大小)
{
    int ELF节数量 = 当前的代码节 ? 当前的代码节->ELF节数量 : SHN_未定义;
    取外部符号2(sym, ELF节数量, 指令在代码节的位置, 大小, 1);
}

静态函数 void 添加新的重定位条目(节ST *s, 符号ST *sym, unsigned long offset, int 类型,addr_t addend)
{
    int c = 0;

    if (无需生成代码 && s == 当前代码节)
        return;

    if (sym) {
        if (0 == sym->c)
            取外部符号(sym, NULL, 0, 0);
        c = sym->c;
    }
    添加新的elf重定位信息(节符号表_数组, s, offset, 类型, c, addend);
}

#if 宏_指针大小 == 4
静态函数 void greloc(节ST *s, 符号ST *sym, unsigned long offset, int 类型)
{
    添加新的重定位条目(s, sym, offset, 类型, 0);
}
#endif

/* ------------------------------------------------------------------------- */
static 符号ST *__符号分配器(void)
{
    符号ST *sym_pool, *sym, *last_sym;
    int i;

    sym_pool = zhi_分配内存(SYM_POOL_NB * sizeof(符号ST));
    动态数组_追加元素(&sym_pools, &nb_sym_pools, sym_pool);

    last_sym = sym_free_first;
    sym = sym_pool;
    for(i = 0; i < SYM_POOL_NB; i++) {
        sym->next = last_sym;
        last_sym = sym;
        sym++;
    }
    sym_free_first = last_sym;
    return last_sym;
}

static inline 符号ST *符号分配内存(void)
{
    符号ST *sym;
#ifndef SYM_DEBUG
    sym = sym_free_first;
    if (!sym)
        sym = __符号分配器();
    sym_free_first = sym->next;
    return sym;
#else
    sym = zhi_分配内存(sizeof(符号ST));
    return sym;
#endif
}

ST_INLN void sym_free(符号ST *sym)
{
#ifndef SYM_DEBUG
    sym->next = sym_free_first;
    sym_free_first = sym;
#else
    zhi_释放(sym);
#endif
}

静态函数 符号ST *将符号放入符号栈2(符号ST **ps, int v, int t, int c)
{
    符号ST *s;

    s = 符号分配内存();
    memset(s, 0, sizeof *s);
    s->v = v;
    s->类型.数据类型 = t;
    s->c = c;
    /* 在栈内添加 */
    s->上个 = *ps;
    *ps = s;
    return s;
}

/* 找到一个符号并返回其关联结构。's’是符号堆栈的顶部 */
静态函数 符号ST *查找符号并返回去关联结构(符号ST *s, int v)
{
    while (s)
    {
        if (s->v == v)
        {
        	return s;
        }else if (s->v == -1)
        {
        	return NULL;
        }
        s = s->上个;
    }
    return NULL;
}

ST_INLN 符号ST *结构体符号_查找(int v)
{
    v -= TOK_标识符;
    if ((unsigned)v >= (unsigned)(单词_标识符 - TOK_标识符))
    {
    	return NULL;
    }
    return 标识符表[v]->struct符号;
}
/* structure lookup */
ST_INLN 符号ST *类符号_查找(int v)
{
    v -= TOK_标识符;
    if ((unsigned)v >= (unsigned)(单词_标识符 - TOK_标识符))
        return NULL;
    return 标识符表[v]->struct符号;
}
/* find an identifier */
ST_INLN 符号ST *查找标识符(int v)
{
    v -= TOK_标识符;
    if ((unsigned)v >= (unsigned)(单词_标识符 - TOK_标识符))
        return NULL;
    return 标识符表[v]->标识符符号;
}

static int 作用域范围(符号ST *s)
{
  if (IS_ENUM_VAL (s->类型.数据类型))
    return s->类型.引用符号->作用域范围;
  else
    return s->作用域范围;
}

/* 将给定的符号压入符号堆栈 */
静态函数 符号ST *将符号放入符号栈(int v, 类型ST *类型, int r, int c)
{
    符号ST *s, **ps;
    单词ST *ts;

    if (局部符号栈)
    {
    	ps = &局部符号栈;
    }else
    {
    	ps = &全局符号栈;
    }
    s = 将符号放入符号栈2(ps, v, 类型->数据类型, c);
    s->类型.引用符号 = 类型->引用符号;
    s->寄存qi = r;
    /* 不要记录字段或匿名符号 */
    if (!(v & 符号_字段) && (v & ~符号_结构体) < 第一个匿名符号)
    {
        /* 标记数组中的记录符号 */
        ts = 标识符表[(v & ~符号_结构体) - TOK_标识符];
        if (v & 符号_结构体)
        {
        	ps = &ts->struct符号;
        }else
        {
        	ps = &ts->标识符符号;
        }
        s->定义的前一个同名符号 = *ps;
        *ps = s;
        s->作用域范围 = 局部_范围;
        if (s->定义的前一个同名符号 && 作用域范围(s->定义的前一个同名符号) == s->作用域范围)
        {
        	错_误(" '%s'的重复声明", 取单词字符串(v & ~符号_结构体, NULL));
        }
    }
    return s;
}
静态函数 符号ST *将符号放入全局符号表(int v, int t, int c)
{
    符号ST *s, **ps;
    s = 将符号放入符号栈2(&全局符号栈, v, t, c);
    s->寄存qi = 存储类型_VC常量 | 存储类型_符号;
    /* 不记录匿名符号 */
    if (v < 第一个匿名符号)
    {
        ps = &标识符表[v - TOK_标识符]->标识符符号;
        /* 修改最上面的本地标识符，以便 标识符符号 在弹出时指向 's'； 从内联汇编调用时发生 */
        while (*ps != NULL && (*ps)->作用域范围)
        {
        	ps = &(*ps)->定义的前一个同名符号;
        }
        s->定义的前一个同名符号 = *ps;
        *ps = s;
    }
    return s;
}

/* 弹出符号直到顶部到达“b”。 如果 KEEP 不为零，则不要真正将它们从列表中弹出，而是从令牌数组中删除它们。  */
静态函数 void 符号弹出(符号ST **ptop, 符号ST *b, int keep)
{
    符号ST *s, *ss, **ps;
    单词ST *ts;
    int v;

    s = *ptop;
    while(s != b) {
        ss = s->上个;
        v = s->v;
        /* remove symbol in token array */
        /* XXX: simplify */
        if (!(v & 符号_字段) && (v & ~符号_结构体) < 第一个匿名符号) {
            ts = 标识符表[(v & ~符号_结构体) - TOK_标识符];
            if (v & 符号_结构体)
                ps = &ts->struct符号;
            else
                ps = &ts->标识符符号;
            *ps = s->定义的前一个同名符号;
        }
	if (!keep)
	    sym_free(s);
        s = ss;
    }
    if (!keep)
	*ptop = b;
}

/* ------------------------------------------------------------------------- */
static void vcheck_cmp(void)
{
    if (栈顶值->寄存qi == 存储类型_标志寄存器 && !无需生成代码)
    {
    	生成值(RC_INT);
    }
}

static void 常量压入栈(类型ST *类型, int r, 常量值U *vc)
{
    if (栈顶值 >= 栈值 + (栈值大小 - 1))
    {
    	错_误("内存已满（栈值）");
    }
    vcheck_cmp();
    栈顶值++;
    栈顶值->类型 = *类型;
    栈顶值->寄存qi = r;
    栈顶值->r2 = 存储类型_VC常量;
    栈顶值->c = *vc;
    栈顶值->sym = NULL;
}

静态函数 void vswap(void)
{
    栈值ST tmp;

    vcheck_cmp();
    tmp = 栈顶值[0];
    栈顶值[0] = 栈顶值[-1];
    栈顶值[-1] = tmp;
}

静态函数 void 弹出堆栈值(void)
{
    int v;
    v = 栈顶值->寄存qi & 存储类型_掩码;
#if defined(目标_I386) || defined(目标_X86_64)
    /* for x86, we need to pop the FP stack */
    if (v == TREG_ST0) {
        o(0xd8dd); /* fstp %st(0) */
    } else
#endif
    if (v == 存储类型_标志寄存器) {
        /* need to put correct jump if && or || without test */
        生成符号(栈顶值->jtrue);
        生成符号(栈顶值->jfalse);
    }
    栈顶值--;
}

/* push constant of 类型 "类型" with useless value */
static void vpush(类型ST *类型)
{
    值压入栈(类型, 存储类型_VC常量, 0);
}
static void 把64位整数常量压入栈(int ty, unsigned long long v)
{
    常量值U cval;
    类型ST ctype;
    ctype.数据类型 = ty;
    ctype.引用符号 = NULL;
    cval.i = v;
    常量压入栈(&ctype, 存储类型_VC常量, &cval);
}

静态函数 void 整数常量压入栈(int v)
{
    把64位整数常量压入栈(数据类型_整数, v);
}

static void 指针大小的常量压入栈(addr_t v)
{
    把64位整数常量压入栈(值的数据类型_SIZE_T, v);
}


static inline void 长长整数压入栈(long long v)
{
    把64位整数常量压入栈(数据类型_长长整数, v);
}

静态函数 void 值压入栈(类型ST *类型, int r, int v)
{
    常量值U cval;
    cval.i = v;
    常量压入栈(类型, r, &cval);
}

static void vseti(int r, int v)
{
    类型ST 类型;
    类型.数据类型 = 数据类型_整数;
    类型.引用符号 = NULL;
    值压入栈(&类型, r, v);
}

静态函数 void 值压入栈值(栈值ST *v)
{
    if (栈顶值 >= 栈值 + (栈值大小 - 1))
    {
    	错_误("内存已满（栈值）");
    }
    栈顶值++;
    *栈顶值 = *v;
}

static void vdup(void)
{
    值压入栈值(栈顶值);
}

/* rotate n first stack elements to the bottom
   I1 ... In -> I2 ... In I1 [top is right]
*/
静态函数 void vrotb(int n)
{
    int i;
    栈值ST tmp;

    vcheck_cmp();
    tmp = 栈顶值[-n + 1];
    for(i=-n+1;i!=0;i++)
        栈顶值[i] = 栈顶值[i+1];
    栈顶值[0] = tmp;
}

/* rotate the n elements before entry e towards the top
   I1 ... In ... -> In I1 ... I(n-1) ... [top is right]
 */
静态函数 void vrote(栈值ST *e, int n)
{
    int i;
    栈值ST tmp;

    vcheck_cmp();
    tmp = *e;
    for(i = 0;i < n - 1; i++)
        e[-i] = e[-i - 1];
    e[-n + 1] = tmp;
}

/* rotate n first stack elements to the top
   I1 ... In -> In I1 ... I(n-1)  [top is right]
 */
静态函数 void vrott(int n)
{
    vrote(栈顶值, n);
}

/* ------------------------------------------------------------------------- */
/* 栈顶值->寄存qi = 存储类型_标志寄存器 means CPU-flags have been set from comparison or test. */

/* called from generators to set the result from relational ops  */
静态函数 void vset_VT_CMP(int op)
{
    栈顶值->寄存qi = 存储类型_标志寄存器;
    栈顶值->cmp_op = op;
    栈顶值->jfalse = 0;
    栈顶值->jtrue = 0;
}

/* called once before asking generators to load 存储类型_标志寄存器 to a register */
static void vset_VT_JMP(void)
{
    int op = 栈顶值->cmp_op;

    if (栈顶值->jtrue || 栈顶值->jfalse) {
        /* 我们需要跳转到'mov$0，%R'或'mov$1，%R'*/
        int inv = op & (op < 2); /* small optimization */
        vseti(存储类型_JMP+inv, 生成值测试(inv, 0));
    } else {
        /* otherwise convert flags (rsp. 0/1) to register */
        栈顶值->c.i = op;
        if (op < 2) /* doesn't seem to happen */
            栈顶值->寄存qi = 存储类型_VC常量;
    }
}

/* Set CPU Flags, doesn't yet jump */
static void 设置_生成值测试(int inv, int t)
{
    int *p;

    if (栈顶值->寄存qi != 存储类型_标志寄存器) {
        整数常量压入栈(0);
        gen_op(TOK_不等于);
        if (栈顶值->寄存qi != 存储类型_标志寄存器) /* must be 存储类型_VC常量 then */
            vset_VT_CMP(栈顶值->c.i != 0);
    }

    p = inv ? &栈顶值->jfalse : &栈顶值->jtrue;
    *p = 生成跳转_append(*p, t);
}

/* 生成值测试
 *
 * 生成任何值的测试（跳转、比较和整数） */
static int 生成值测试(int inv, int t)
{
    int op, x, u;

    设置_生成值测试(inv, t);
    t = 栈顶值->jtrue, u = 栈顶值->jfalse;
    if (inv)
        x = u, u = t, t = x;
    op = 栈顶值->cmp_op;

    /* 跳到想要的目标 */
    if (op > 1)
        t = 生成条件跳转(op ^ inv, t);
    else if (op != inv)
        t = 生成到标签的跳转(t);
    /* 解决互补跳转到这里 */
    生成符号(u);

    栈顶值--;
    return t;
}

/* 生成零或非零测试 */
static void 生成零或非零测试(int op)
{
    if (栈顶值->寄存qi == 存储类型_标志寄存器) {
        int j;
        if (op == TOK_等于) {
            j = 栈顶值->jfalse;
            栈顶值->jfalse = 栈顶值->jtrue;
            栈顶值->jtrue = j;
            栈顶值->cmp_op ^= 1;
        }
    } else {
        整数常量压入栈(0);
        gen_op(op);
    }
}

/* ------------------------------------------------------------------------- */
/* 推送一个 TYPE 的符号值 */
静态函数 void 类型符号值压入栈(类型ST *类型, 符号ST *sym)
{
    常量值U cval;
    cval.i = 0;
    常量压入栈(类型, 存储类型_VC常量 | 存储类型_符号, &cval);
    栈顶值->sym = sym;
}

/* 返回指向某个节的静态符号 */
静态函数 符号ST *取指向某个节的静态符号(类型ST *类型, 节ST *sec, unsigned long offset, unsigned long 大小)
{
    int v;
    符号ST *sym;

    v = 匿名符号索引++;
    sym = 将符号放入符号栈(v, 类型, 存储类型_VC常量 | 存储类型_符号, 0);
    sym->类型.数据类型 |= 存储类型_静态;
    取外部符号(sym, sec, offset, 大小);
    return sym;
}

/* 通过添加虚拟符号推送对部分偏移的引用 */
static void 添加虚拟符号推送对节偏移的引用(类型ST *类型, 节ST *sec, unsigned long offset, unsigned long 大小)
{
    类型符号值压入栈(类型, 取指向某个节的静态符号(类型, sec, offset, 大小));
}

/* 定义对类型为 'u' 的符号 'v' 的新外部引用 */
静态函数 符号ST *外部_全局_符号(int v, 类型ST *类型)
{
    符号ST *s;

    s = 查找标识符(v);
    if (!s) {
        /* 推进参考 */
        s = 将符号放入全局符号表(v, 类型->数据类型 | 存储类型_外部, 0);
        s->类型.引用符号 = 类型->引用符号;
    } else if (IS_ASM_SYM(s)) {
        s->类型.数据类型 = 类型->数据类型 | (s->类型.数据类型 & 存储类型_外部);
        s->类型.引用符号 = 类型->引用符号;
        update_storage(s);
    }
    return s;
}

/* 创建一个没有类似于 asm 标签的特定类型的外部引用。 如果符号也从 C 中使用，这可以避免类型冲突 */
静态函数 符号ST *外部_辅助_符号(int v)
{
    类型ST ct = { 值的数据类型_ASM_FUNC, NULL };
    return 外部_全局_符号(v, &ct);
}

/*推送对辅助函数的引用（例如 memmove）*/
静态函数 void 推送_辅助_函数引用(int v)
{
    类型符号值压入栈(&函数_旧_类型, 外部_辅助_符号(v));
}

static void 合并_符号属性(struct 符号属性S *sa, struct 符号属性S *sa1)
{
    if (sa1->aligned && !sa->aligned)
      sa->aligned = sa1->aligned;
    sa->packed |= sa1->packed;
    sa->weak |= sa1->weak;
    if (sa1->能见度 != STV_DEFAULT) {
	int vis = sa->能见度;
	if (vis == STV_DEFAULT
	    || vis > sa1->能见度)
	  vis = sa1->能见度;
	sa->能见度 = vis;
    }
    sa->dllexport |= sa1->dllexport;
    sa->节点 |= sa1->节点;
    sa->dllimport |= sa1->dllimport;
}

static void 合并_函数属性(struct 函数属性S *fa, struct 函数属性S *fa1)
{
    if (fa1->函数调用约定 && !fa->函数调用约定)
    {
    	fa->函数调用约定 = fa1->函数调用约定;
    }
    if (fa1->函数_类型&& !fa->函数_类型)
    {
    	fa->函数_类型= fa1->函数_类型;
    }
    if (fa1->函数_参数 && !fa->函数_参数)
    {
    	fa->函数_参数 = fa1->函数_参数;
    }
    if (fa1->函数_不返回)
    {
    	fa->函数_不返回 = 1;
    }
    if (fa1->函数_构造函数)
    {
    	fa->函数_构造函数 = 1;
    }
    if (fa1->函数_析构函数)
    {
    	fa->函数_析构函数 = 1;
    }
}

static void 合并_属性(GNUC属性ST *ad, GNUC属性ST *ad1)
{
    合并_符号属性(&ad->a, &ad1->a);
    合并_函数属性(&ad->f, &ad1->f);

    if (ad1->section)
      ad->section = ad1->section;
    if (ad1->别名_目标)
      ad->别名_目标 = ad1->别名_目标;
    if (ad1->asm_label)
      ad->asm_label = ad1->asm_label;
    if (ad1->属性_模式)
      ad->属性_模式 = ad1->属性_模式;
}

static void 合并_类型属性(符号ST *sym, 类型ST *类型)
{
    if (!(类型->数据类型 & 存储类型_外部) || IS_ENUM_VAL(sym->类型.数据类型)) {
        if (!(sym->类型.数据类型 & 存储类型_外部))
            错_误(" '%s'的重复定义", 取单词字符串(sym->v, NULL));
        sym->类型.数据类型 &= ~存储类型_外部;
    }

    if (IS_ASM_SYM(sym)) {
        /* 如果两者都是静态的，则保持静态 */
        sym->类型.数据类型 = 类型->数据类型 & (sym->类型.数据类型 | ~存储类型_静态);
        sym->类型.引用符号 = 类型->引用符号;
    }

    if (!是兼容类型(&sym->类型, 类型)) {
        错_误("'%s'的重复定义类型不兼容", 取单词字符串(sym->v, NULL));

    } else if ((sym->类型.数据类型 & 数据类型_基本类型) == 数据类型_函数)
    {
        int static_proto = sym->类型.数据类型 & 存储类型_静态;
        /* 如果静态遵循非静态函数声明，则发出警告 */
        if ((类型->数据类型 & 存储类型_静态) && !static_proto
            /* XXX 这个内联测试不应该在这里。 在我们再次实现 gnu-inline 模式之前，它会消除由我们的变通方法引起的 mingw 警告。 */
            && !((类型->数据类型 | sym->类型.数据类型) & 存储类型_内联))
            zhi_警告("'%s' 的重定义忽略了静态",取单词字符串(sym->v, NULL));

        /* 如果两者都同意或者一个有静态，则设置“内联”*/
        if ((类型->数据类型 | sym->类型.数据类型) & 存储类型_内联) {
            if (!((类型->数据类型 ^ sym->类型.数据类型) & 存储类型_内联)
             || ((类型->数据类型 | sym->类型.数据类型) & 存储类型_静态))
                static_proto |= 存储类型_内联;
        }

        if (0 == (类型->数据类型 & 存储类型_外部)) {
            struct 函数属性S f = sym->类型.引用符号->f;
            /* 放置完整类型，使用原型中的静态 */
            sym->类型.数据类型 = (类型->数据类型 & ~(存储类型_静态|存储类型_内联)) | static_proto;
            sym->类型.引用符号 = 类型->引用符号;
            合并_函数属性(&sym->类型.引用符号->f, &f);
        } else {
            sym->类型.数据类型 &= ~存储类型_内联 | static_proto;
        }

        if (sym->类型.引用符号->f.函数_类型== 函数原型_旧
             && 类型->引用符号->f.函数_类型!= 函数原型_旧) {
            sym->类型.引用符号 = 类型->引用符号;
        }

    } else {
        if ((sym->类型.数据类型 & 数据类型_数组) && 类型->引用符号->c >= 0) {
            /* 如果在 extern 声明中省略，则设置数组大小 */
            sym->类型.引用符号->c = 类型->引用符号->c;
        }
        if ((类型->数据类型 ^ sym->类型.数据类型) & 存储类型_静态)
            zhi_警告("'%s'的重复定义，存储不匹配",
                取单词字符串(sym->v, NULL));
    }
}

/* Merge some 贮存 attributes.  */
static void patch_storage(符号ST *sym, GNUC属性ST *ad, 类型ST *类型)
{
    if (类型)
        合并_类型属性(sym, 类型);

#ifdef 目标_PE
    if (sym->a.dllimport != ad->a.dllimport)
        错_误(" '%s'的重复定义是不兼容DLL链接",
            取单词字符串(sym->v, NULL));
#endif
    合并_符号属性(&sym->a, &ad->a);
    if (ad->asm_label)
        sym->asm_label = ad->asm_label;
    update_storage(sym);
}

/* copy sym to other stack */
static 符号ST *sym_copy(符号ST *s0, 符号ST **ps)
{
    符号ST *s;
    s = 符号分配内存(), *s = *s0;
    s->上个 = *ps, *ps = s;
    if (s->v < 第一个匿名符号) {
        ps = &标识符表[s->v - TOK_标识符]->标识符符号;
        s->定义的前一个同名符号 = *ps, *ps = s;
    }
    return s;
}

/* copy s->类型.引用符号 to stack 'ps' for 数据类型_函数 and 数据类型_指针 */
static void sym_copy_ref(符号ST *s, 符号ST **ps)
{
    int bt = s->类型.数据类型 & 数据类型_基本类型;
    if (bt == 数据类型_函数 || bt == 数据类型_指针 || (bt == 数据类型_结构体 && s->作用域范围)) {
        符号ST **sp = &s->类型.引用符号;
        for (s = *sp, *sp = NULL; s; s = s->next) {
            符号ST *s2 = sym_copy(s, ps);
            sp = &(*sp = s2)->next;
            sym_copy_ref(s2, ps);
        }
    }
}

/* 定义对符号'v'的新外部引用 */
static 符号ST *外部_符号(int v, 类型ST *类型, int r, GNUC属性ST *ad)
{
    符号ST *s;

    /* 查找全局符号 */
    s = 查找标识符(v);
    while (s && s->作用域范围)
        s = s->定义的前一个同名符号;

    if (!s) {
        /* 前推引用 */
        s = 将符号放入全局符号表(v, 类型->数据类型, 0);
        s->寄存qi |= r;
        s->a = ad->a;
        s->asm_label = ad->asm_label;
        s->类型.引用符号 = 类型->引用符号;
        /* 将类型复制到全局堆栈 */
        if (局部符号栈)
            sym_copy_ref(s, &全局符号栈);
    } else {
        patch_storage(s, ad, 类型);
    }
    /* 在局部符号堆栈上推送变量（如果有的话） */
    if (局部符号栈 && (s->类型.数据类型 & 数据类型_基本类型) != 数据类型_函数)
        s = sym_copy(s, &局部符号栈);
    return s;
}

/* 将寄存器保存到(栈顶值 - n） 堆栈入口 */
静态函数 void save_regs(int n)
{
    栈值ST *p, *p1;
    for(p = 栈值, p1 = 栈顶值 - n; p <= p1; p++)
        save_reg(p->寄存qi);
}

/* 将r保存到内存堆栈，并将其标记为空闲 */
静态函数 void save_reg(int r)
{
    save_reg_upstack(r, 0);
}

/* 将r保存到内存堆栈中，并将其标记为空闲，如果看到(栈顶值 - n） 堆栈入口 */
静态函数 void save_reg_upstack(int r, int n)
{
    int l, 大小, 对齐, bt;
    栈值ST *p, *p1, sv;

    if ((r &= 存储类型_掩码) >= 存储类型_VC常量)
        return;
    if (无需生成代码)
        return;
    l = 0;
    for(p = 栈值, p1 = 栈顶值 - n; p <= p1; p++) {
        if ((p->寄存qi & 存储类型_掩码) == r || p->r2 == r) {
            /* 如果尚未完成，则必须在堆栈上保存值 */
            if (!l) {
                bt = p->类型.数据类型 & 数据类型_基本类型;
                if (bt == 数据类型_无类型)
                    continue;
                if ((p->寄存qi & 存储类型_左值) || bt == 数据类型_函数)
                    bt = 数据类型_指针;
                sv.类型.数据类型 = bt;
                大小 = 类型_大小(&sv.类型, &对齐);
                l = 取_临时_局部_变量(大小,对齐);
                sv.寄存qi = 存储类型_局部 | 存储类型_左值;
                sv.c.i = l;
                store(p->寄存qi & 存储类型_掩码, &sv);
#if defined(目标_I386) || defined(目标_X86_64)
                /* x86特定：如果保存，则需要弹出fp寄存器ST0 */
                if (r == TREG_ST0) {
                    o(0xd8dd); /* fstp %st(0) */
                }
#endif
                /* 特殊长箱 */
                if (p->r2 < 存储类型_VC常量 && USING_TWO_WORDS(bt)) {
                    sv.c.i += 宏_指针大小;
                    store(p->r2, &sv);
                }
            }
            /* 将该堆栈条目标记为保存在堆栈上 */
            if (p->寄存qi & 存储类型_左值) {
                /* 也清除有界标志，因为函数的重定位地址存储在p->c.i中 */
                p->寄存qi = (p->寄存qi & ~(存储类型_掩码 | 存储类型_有边界的)) | 存储类型_LLOCAL;
            } else {
                p->寄存qi = 存储类型_左值 | 存储类型_局部;
            }
            p->sym = NULL;
            p->r2 = 存储类型_VC常量;
            p->c.i = l;
        }
    }
}

#ifdef 目标_ARM
/* find a register of class 'rc2' with at most one reference on stack.
 * If none, call get_reg(rc) */
静态函数 int get_reg_ex(int rc, int rc2)
{
    int r;
    栈值ST *p;
    
    for(r=0;r<NB_REGS;r++) {
        if (reg_classes[r] & rc2) {
            int n;
            n=0;
            for(p = 栈值; p <= 栈顶值; p++) {
                if ((p->寄存qi & 存储类型_掩码) == r ||
                    p->r2 == r)
                    n++;
            }
            if (n <= 1)
                return r;
        }
    }
    return get_reg(rc);
}
#endif

/* find a free register of class 'rc'. If none, save one register */
静态函数 int get_reg(int rc)
{
    int r;
    栈值ST *p;

    /* find a free register */
    for(r=0;r<NB_REGS;r++) {
        if (reg_classes[r] & rc) {
            if (无需生成代码)
                return r;
            for(p=栈值;p<=栈顶值;p++) {
                if ((p->寄存qi & 存储类型_掩码) == r ||
                    p->r2 == r)
                    goto notfound;
            }
            return r;
        }
    notfound: ;
    }
    
    /* no register left : free the first one on the stack (VERY
       IMPORTANT to start from the bottom to ensure that we don't
       spill registers used in gen_opi()) */
    for(p=栈值;p<=栈顶值;p++) {
        /* look at second register (if long long) */
        r = p->r2;
        if (r < 存储类型_VC常量 && (reg_classes[r] & rc))
            goto save_found;
        r = p->寄存qi & 存储类型_掩码;
        if (r < 存储类型_VC常量 && (reg_classes[r] & rc)) {
        save_found:
            save_reg(r);
            return r;
        }
    }
    /* Should never comes here */
    return -1;
}

/* find a free temporary local variable (return the offset on stack) match the 大小 and 对齐. If none, add new temporary stack variable*/
static int 取_临时_局部_变量(int 大小,int 对齐){
	int i;
	struct 临时局部变量S *temp_var;
	int found_var;
	栈值ST *p;
	int r;
	char free;
	char found;
	found=0;
	for(i=0;i<临时局部变量数;i++){
		temp_var=&临时局部变量数组[i];
		if(temp_var->大小<大小||对齐!=temp_var->对齐){
			continue;
		}
		/*check if temp_var is free*/
		free=1;
		for(p=栈值;p<=栈顶值;p++) {
			r=p->寄存qi&存储类型_掩码;
			if(r==存储类型_局部||r==存储类型_LLOCAL){
				if(p->c.i==temp_var->地点){
					free=0;
					break;
				}
			}
		}
		if(free){
			found_var=temp_var->地点;
			found=1;
			break;
		}
	}
	if(!found){
		局部变量索引 = (局部变量索引 - 大小) & -对齐;
		if(临时局部变量数<最大_临时_局部_变量_数量){
			temp_var=&临时局部变量数组[i];
			temp_var->地点=局部变量索引;
			temp_var->大小=大小;
			temp_var->对齐=对齐;
			临时局部变量数++;
		}
		found_var=局部变量索引;
	}
	return found_var;
}

static void 清除_临时_局部_变量列表(){
	临时局部变量数=0;
}

/* move register 's' (of 类型 't') to 'r', and flush previous value of r to memory
   if needed */
static void move_reg(int r, int s, int t)
{
    栈值ST sv;

    if (r != s) {
        save_reg(r);
        sv.类型.数据类型 = t;
        sv.类型.引用符号 = NULL;
        sv.寄存qi = s;
        sv.c.i = 0;
        load(r, &sv);
    }
}

/* get address of 栈顶值 (栈顶值 MUST BE an lvalue) */
静态函数 void gaddrof(void)
{
    栈顶值->寄存qi &= ~存储类型_左值;
    /* tricky: if saved lvalue, then we can go back to lvalue */
    if ((栈顶值->寄存qi & 存储类型_掩码) == 存储类型_LLOCAL)
        栈顶值->寄存qi = (栈顶值->寄存qi & ~存储类型_掩码) | 存储类型_局部 | 存储类型_左值;
}

#ifdef 启用边界检查M
/* generate a bounded pointer addition */
static void gen_bounded_ptr_add(void)
{
    int save = (栈顶值[-1].寄存qi & 存储类型_掩码) == 存储类型_局部;
    if (save) {
      值压入栈值(&栈顶值[-1]);
      vrott(3);
    }
    推送_辅助_函数引用(TOK___bound_ptr_add);
    vrott(3);
    g函数_调用(2);
    栈顶值 -= save;
    整数常量压入栈(0);
    /* returned pointer is in REG_IRET */
    栈顶值->寄存qi = REG_IRET | 存储类型_有边界的;
    if (无需生成代码)
        return;
    /* relocation offset of the bounding function call point */
    栈顶值->c.i = (当前代码节->重定位->当前数据_偏移量 - sizeof(ElfW_Rel));
}

/* patch pointer addition in 栈顶值 so that pointer dereferencing is
   also tested */
static void gen_bounded_ptr_deref(void)
{
    addr_t func;
    int 大小, 对齐;
    ElfW_Rel *rel;
    符号ST *sym;

    if (无需生成代码)
        return;

    大小 = 类型_大小(&栈顶值->类型, &对齐);
    switch(大小) {
    case  1: func = TOK___bound_ptr_indir1; break;
    case  2: func = TOK___bound_ptr_indir2; break;
    case  4: func = TOK___bound_ptr_indir4; break;
    case  8: func = TOK___bound_ptr_indir8; break;
    case 12: func = TOK___bound_ptr_indir12; break;
    case 16: func = TOK___bound_ptr_indir16; break;
    default:
        /* may happen with struct member access */
        return;
    }
    sym = 外部_辅助_符号(func);
    if (!sym->c)
        取外部符号(sym, NULL, 0, 0);
    /* patch relocation */
    /* XXX: find a better solution ? */
    rel = (ElfW_Rel *)(当前代码节->重定位->数据 + 栈顶值->c.i);
    rel->r_info = ELFW(R_INFO)(sym->c, ELFW(R_TYPE)(rel->r_info));
}

/* generate lvalue bound code */
static void gbound(void)
{
    类型ST 类型1;

    栈顶值->寄存qi &= ~存储类型_必须边界检查;
    /* if lvalue, then use checking code before dereferencing */
    if (栈顶值->寄存qi & 存储类型_左值) {
        /* if not 存储类型_有边界的 value, then make one */
        if (!(栈顶值->寄存qi & 存储类型_有边界的)) {
            /* must save 类型 because we must set it to int to get pointer */
            类型1 = 栈顶值->类型;
            栈顶值->类型.数据类型 = 数据类型_指针;
            gaddrof();
            整数常量压入栈(0);
            gen_bounded_ptr_add();
            栈顶值->寄存qi |= 存储类型_左值;
            栈顶值->类型 = 类型1;
        }
        /* then check for dereferencing */
        gen_bounded_ptr_deref();
    }
}

/* 我们需要在开始将函数参数加载到寄存器之前调用 __bound_ptr_add */
静态函数 void 生成函数参数_边界(int nb_args)
{
    int i, v;
    栈值ST *sv;

    for (i = 1; i <= nb_args; ++i)
        if (栈顶值[1 - i].寄存qi & 存储类型_必须边界检查) {
            vrotb(i);
            gbound();
            vrott(i);
        }

    sv = 栈顶值 - nb_args;
    if (sv->寄存qi & 存储类型_符号) {
        v = sv->sym->v;
        if (v == TOK_setjmp
          || v == TOK__setjmp
#ifndef 目标_PE
          || v == TOK_sigsetjmp
          || v == TOK___sigsetjmp
#endif
          ) {
            推送_辅助_函数引用(TOK___bound_setjmp);
            值压入栈值(sv + 1);
            g函数_调用(1);
            函数_边界_添加_结语 = 1;
        }
#if defined 目标_I386 || defined 目标_X86_64
        if (v == TOK_alloca)
            函数_边界_添加_结语 = 1;
#endif
#if 目标系统_NetBSD
        if (v == TOK_longjmp) /* undo rename to __longjmp14 */
            sv->sym->asm_label = TOK___bound_longjmp;
#endif
    }
}

/* 为从 S 到 E 的局部符号添加边界（通过 ->上个） */
static void 添加_局部符号_边界(符号ST *s, 符号ST *e)
{
    for (; s != e; s = s->上个) {
        if (!s->v || (s->寄存qi & 存储类型_掩码) != 存储类型_局部)
          continue;
        /* Add arrays/structs/unions because we always take address */
        if ((s->类型.数据类型 & 数据类型_数组)
            || (s->类型.数据类型 & 数据类型_基本类型) == 数据类型_结构体
            || s->a.addrtaken) {
            /* add local bound info */
            int 对齐, 大小 = 类型_大小(&s->类型, &对齐);
            addr_t *bounds_ptr = 节_预留指定大小内存(局部边界_节,
                                                 2 * sizeof(addr_t));
            bounds_ptr[0] = s->c;
            bounds_ptr[1] = 大小;
        }
    }
}
#endif

/* 环绕符号弹出，也可能注册局部边界。  */
static void 弹出局部符号(符号ST *b, int keep)
{
#ifdef 启用边界检查M
    if (全局虚拟机->使用_边界_检查器 && !keep && (局部_范围 || !函数_可变参))
        添加_局部符号_边界(局部符号栈, b);
#endif
    if (调试_模式)
        zhi_add_debug_info (全局虚拟机, !局部_范围, 局部符号栈, b);
    符号弹出(&局部符号栈, b, keep);
}

static void incr_bf_adr(int o)
{
    栈顶值->类型 = 字符_指针_类型;
    gaddrof();
    指针大小的常量压入栈(o);
    gen_op('+');
    栈顶值->类型.数据类型 = 数据类型_字节 | 存储类型_无符号的;
    栈顶值->寄存qi |= 存储类型_左值;
}

/* 打包或其他未对齐位域的单字节加载模式 */
static void 打包或未对齐位域的单字节_加载(类型ST *类型, int bit_pos, int 位大小)
{
    int n, o, bits;
    save_reg_upstack(栈顶值->寄存qi, 1);
    把64位整数常量压入栈(类型->数据类型 & 数据类型_基本类型, 0); // B X
    bits = 0, o = bit_pos >> 3, bit_pos &= 7;
    do {
        vswap(); // X B
        incr_bf_adr(o);
        vdup(); // X B B
        n = 8 - bit_pos;
        if (n > 位大小)
            n = 位大小;
        if (bit_pos)
            整数常量压入栈(bit_pos), gen_op(TOK_SHR), bit_pos = 0; // X B Y
        if (n < 8)
            整数常量压入栈((1 << n) - 1), gen_op('&');
        生成_转换(类型);
        if (bits)
            整数常量压入栈(bits), gen_op(TOK_左移);
        vrotb(3); // B Y X
        gen_op('|'); // B X
        bits += n, 位大小 -= n, o = 1;
    } while (位大小);
    vswap(), 弹出堆栈值();
    if (!(类型->数据类型 & 存储类型_无符号的)) {
        n = ((类型->数据类型 & 数据类型_基本类型) == 数据类型_长长整数 ? 64 : 32) - bits;
        整数常量压入栈(n), gen_op(TOK_左移);
        整数常量压入栈(n), gen_op(TOK_右移);
    }
}

/* 用于打包或其他未对齐位域的单字节存储模式 */
static void 打包或未对齐位域的单字节_存储(int bit_pos, int 位大小)
{
    int bits, n, o, m, c;
    c = (栈顶值->寄存qi & (存储类型_掩码 | 存储类型_左值 | 存储类型_符号)) == 存储类型_VC常量;
    vswap(); // X B
    save_reg_upstack(栈顶值->寄存qi, 1);
    bits = 0, o = bit_pos >> 3, bit_pos &= 7;
    do {
        incr_bf_adr(o); // X B
        vswap(); //B X
        c ? vdup() : 堆栈条目转换为寄存器值复制到另个寄存器(); // B V X
        vrott(3); // X B V
        if (bits)
            整数常量压入栈(bits), gen_op(TOK_SHR);
        if (bit_pos)
            整数常量压入栈(bit_pos), gen_op(TOK_左移);
        n = 8 - bit_pos;
        if (n > 位大小)
            n = 位大小;
        if (n < 8) {
            m = ((1 << n) - 1) << bit_pos;
            整数常量压入栈(m), gen_op('&'); // X B V1
            值压入栈值(栈顶值-1); // X B V1 B
            整数常量压入栈(m & 0x80 ? ~m & 0x7f : ~m);
            gen_op('&'); // X B V1 B1
            gen_op('|'); // X B V2
        }
        vdup(), 栈顶值[-1] = 栈顶值[-2]; // X B B V2
        存储栈顶值(), 弹出堆栈值(); // X B
        bits += n, 位大小 -= n, bit_pos = 0, o = 1;
    } while (位大小);
    弹出堆栈值(), 弹出堆栈值();
}

static int adjust_bf(栈值ST *sv, int bit_pos, int 位大小)
{
    int t;
    if (0 == sv->类型.引用符号)
        return 0;
    t = sv->类型.引用符号->auxtype;
    if (t != -1 && t != 数据类型_结构体) {
        sv->类型.数据类型 = (sv->类型.数据类型 & ~(数据类型_基本类型 | 数据类型_长整数)) | t;
        sv->寄存qi |= 存储类型_左值;
    }
    return t;
}

/* 存储栈顶值属于'rc'类的寄存器。 左值被转换为值。 如果不能转换为寄存器值（如结构），则不能使用。 */
静态函数 int 生成值(int rc)
{
    int r, r2, r_ok, r2_ok, rc2, bt;
    int bit_pos, 位大小, 大小, 对齐;

    /* NOTE: get_reg can modify 栈值[] */
    if (栈顶值->类型.数据类型 & 存储类型_位域) {
        类型ST 类型;

        bit_pos = BIT_POS(栈顶值->类型.数据类型);
        位大小 = BIT_SIZE(栈顶值->类型.数据类型);
        /* remove bit field info to avoid loops */
        栈顶值->类型.数据类型 &= ~值的数据类型_结构体_MASK;

        类型.引用符号 = NULL;
        类型.数据类型 = 栈顶值->类型.数据类型 & 存储类型_无符号的;
        if ((栈顶值->类型.数据类型 & 数据类型_基本类型) == 数据类型_布尔)
            类型.数据类型 |= 存储类型_无符号的;

        r = adjust_bf(栈顶值, bit_pos, 位大小);

        if ((栈顶值->类型.数据类型 & 数据类型_基本类型) == 数据类型_长长整数)
            类型.数据类型 |= 数据类型_长长整数;
        else
            类型.数据类型 |= 数据类型_整数;

        if (r == 数据类型_结构体) {
            打包或未对齐位域的单字节_加载(&类型, bit_pos, 位大小);
        } else {
            int bits = (类型.数据类型 & 数据类型_基本类型) == 数据类型_长长整数 ? 64 : 32;
            /* cast to int to propagate signedness in following ops */
            生成_转换(&类型);
            /* generate shifts */
            整数常量压入栈(bits - (bit_pos + 位大小));
            gen_op(TOK_左移);
            整数常量压入栈(bits - 位大小);
            /* NOTE: transformed to SHR if unsigned */
            gen_op(TOK_右移);
        }
        r = 生成值(rc);
    } else {
        if (是_浮点(栈顶值->类型.数据类型) && 
            (栈顶值->寄存qi & (存储类型_掩码 | 存储类型_左值)) == 存储类型_VC常量) {
            /* CPUs usually cannot use float constants, so we store them
               generically in 数据 segment */
            初始参数S p = { 只读数据_节 };
            unsigned long offset;
            大小 = 类型_大小(&栈顶值->类型, &对齐);
            if (无需静态数据输出)
                大小 = 0, 对齐 = 1;
            offset = 节_预留指定大小内存且按照align对齐(p.sec, 大小, 对齐);
            添加虚拟符号推送对节偏移的引用(&栈顶值->类型, p.sec, offset, 大小);
	    vswap();
	    初始存储值(&p, &栈顶值->类型, offset);
	    栈顶值->寄存qi |= 存储类型_左值;
        }
#ifdef 启用边界检查M
        if (栈顶值->寄存qi & 存储类型_必须边界检查) 
            gbound();
#endif

        bt = 栈顶值->类型.数据类型 & 数据类型_基本类型;

#ifdef 目标_RISCV64
        /* XXX mega hack */
        if (bt == 数据类型_长双精度 && rc == RC_FLOAT)
          rc = RC_INT;
#endif
        rc2 = 返回对应于t和rc的第二个寄存器类(bt, rc);

        /* need to reload if:
           - constant
           - lvalue (need to dereference pointer)
           - already a register, but not in the right class */
        r = 栈顶值->寄存qi & 存储类型_掩码;
        r_ok = !(栈顶值->寄存qi & 存储类型_左值) && (r < 存储类型_VC常量) && (reg_classes[r] & rc);
        r2_ok = !rc2 || ((栈顶值->r2 < 存储类型_VC常量) && (reg_classes[栈顶值->r2] & rc2));

        if (!r_ok || !r2_ok) {
            if (!r_ok)
                r = get_reg(rc);
            if (rc2) {
                int load_type = (bt == 数据类型_128位浮点) ? 数据类型_双精度 : 值的数据类型_PTRDIFF_T;
                int original_type = 栈顶值->类型.数据类型;

                /* two register 类型 load :
                   expand to two words temporarily */
                if ((栈顶值->寄存qi & (存储类型_掩码 | 存储类型_左值)) == 存储类型_VC常量) {
                    /* load constant */
                    unsigned long long ll = 栈顶值->c.i;
                    栈顶值->c.i = ll; /* first word */
                    load(r, 栈顶值);
                    栈顶值->寄存qi = r; /* save register value */
                    整数常量压入栈(ll >> 32); /* second word */
                } else if (栈顶值->寄存qi & 存储类型_左值) {
                    /* We do not want to modifier the long long pointer here.
                       So we save any other instances down the stack */
                    save_reg_upstack(栈顶值->寄存qi, 1);
                    /* load from memory */
                    栈顶值->类型.数据类型 = load_type;
                    load(r, 栈顶值);
                    vdup();
                    栈顶值[-1].寄存qi = r; /* save register value */
                    /* increment pointer to get second word */
                    栈顶值->类型.数据类型 = 值的数据类型_PTRDIFF_T;
                    gaddrof();
                    指针大小的常量压入栈(宏_指针大小);
                    gen_op('+');
                    栈顶值->寄存qi |= 存储类型_左值;
                    栈顶值->类型.数据类型 = load_type;
                } else {
                    /* move registers */
                    if (!r_ok)
                        load(r, 栈顶值);
                    if (r2_ok && 栈顶值->r2 < 存储类型_VC常量)
                        goto 完成;
                    vdup();
                    栈顶值[-1].寄存qi = r; /* save register value */
                    栈顶值->寄存qi = 栈顶值[-1].r2;
                }
                /* Allocate second register. Here we rely on the fact that
                   get_reg() tries first to free r2 of an 栈值ST. */
                r2 = get_reg(rc2);
                load(r2, 栈顶值);
                弹出堆栈值();
                /* write second register */
                栈顶值->r2 = r2;
            完成:
                栈顶值->类型.数据类型 = original_type;
            } else {
                if (栈顶值->寄存qi == 存储类型_标志寄存器)
                    vset_VT_JMP();
                /* one register 类型 load */
                load(r, 栈顶值);
            }
        }
        栈顶值->寄存qi = r;
#ifdef 目标_C67
        /* uses register pairs for doubles */
        if (bt == 数据类型_双精度)
            栈顶值->r2 = r+1;
#endif
    }
    return r;
}

/* generate 栈顶值[-1] and 栈顶值[0] in resp. classes rc1 and rc2 */
静态函数 void 生成值2(int rc1, int rc2)
{
    /* generate more generic register first. But 存储类型_JMP or 存储类型_标志寄存器
       values must be generated first in all cases to avoid possible
       reload errors */
    if (栈顶值->寄存qi != 存储类型_标志寄存器 && rc1 <= rc2) {
        vswap();
        生成值(rc1);
        vswap();
        生成值(rc2);
        /* test if reload is needed for first register */
        if ((栈顶值[-1].寄存qi & 存储类型_掩码) >= 存储类型_VC常量) {
            vswap();
            生成值(rc1);
            vswap();
        }
    } else {
        生成值(rc2);
        vswap();
        生成值(rc1);
        vswap();
        /* test if reload is needed for first register */
        if ((栈顶值[0].寄存qi & 存储类型_掩码) >= 存储类型_VC常量) {
            生成值(rc2);
        }
    }
}

#if 宏_指针大小 == 4
/* 将堆栈上的 64 位扩展为两个整数 */
静态函数 void 堆栈64位扩展为2个整数(void)
{
    int u, v;
    u = 栈顶值->类型.数据类型 & (存储类型_明确有无符号 | 存储类型_无符号的);
    v = 栈顶值->寄存qi & (存储类型_掩码 | 存储类型_左值);
    if (v == 存储类型_VC常量) {
        vdup();
        栈顶值[0].c.i >>= 32;
    } else if (v == (存储类型_左值|存储类型_VC常量) || v == (存储类型_左值|存储类型_局部)) {
        vdup();
        栈顶值[0].c.i += 4;
    } else {
        生成值(RC_INT);
        vdup();
        栈顶值[0].寄存qi = 栈顶值[-1].r2;
        栈顶值[0].r2 = 栈顶值[-1].r2 = 存储类型_VC常量;
    }
    栈顶值[0].类型.数据类型 = 栈顶值[-1].类型.数据类型 = 数据类型_整数 | u;
}
#endif

#if 宏_指针大小 == 4
/* 从两个整数构建 long long */
static void 两个整数构建为长长整数(int t)
{
    生成值2(RC_INT, RC_INT);
    栈顶值[-1].r2 = 栈顶值[0].寄存qi;
    栈顶值[-1].类型.数据类型 = t;
    弹出堆栈值();
}
#endif

/* 将堆栈条目转换为寄存器并将其值复制到另一个寄存器中 */
static void 堆栈条目转换为寄存器值复制到另个寄存器(void)
{
    int t, rc, r;

    t = 栈顶值->类型.数据类型;
#if 宏_指针大小 == 4
    if ((t & 数据类型_基本类型) == 数据类型_长长整数) {
        if (t & 存储类型_位域) {
            生成值(RC_INT);
            t = 栈顶值->类型.数据类型;
        }
        堆栈64位扩展为2个整数();
        堆栈条目转换为寄存器值复制到另个寄存器();
        vswap();
        vrotb(3);
        堆栈条目转换为寄存器值复制到另个寄存器();
        vrotb(4);
        /* stack: H L L1 H1 */
        两个整数构建为长长整数(t);
        vrotb(3);
        vrotb(3);
        vswap();
        两个整数构建为长长整数(t);
        vswap();
        return;
    }
#endif
    /* 复制值 */
    rc = 返回t的泛型寄存器类(t);
    生成值(rc);
    r = get_reg(rc);
    vdup();
    load(r, 栈顶值);
    栈顶值->寄存qi = r;
}

#if 宏_指针大小 == 4
/* 生成与 CPU 无关（无符号）的 long long 操作 */
static void 生成长长整数操作(int op)
{
    int t, a, b, op1, c, i;
    int func;
    unsigned short reg_iret = REG_IRET;
    unsigned short reg_lret = REG_IRE2;
    栈值ST tmp;

    switch(op) {
    case '/':
    case TOK_PDIV:
        func = TOK___divdi3;
        goto gen_func;
    case TOK_UDIV:
        func = TOK___udivdi3;
        goto gen_func;
    case '%':
        func = TOK___moddi3;
        goto gen_mod_func;
    case TOK_UMOD:
        func = TOK___umoddi3;
    gen_mod_func:
#ifdef ZHI_ARM_EABI
        reg_iret = TREG_R2;
        reg_lret = TREG_R3;
#endif
    gen_func:
        /* call generic long long function */
        推送_辅助_函数引用(func);
        vrott(3);
        g函数_调用(2);
        整数常量压入栈(0);
        栈顶值->寄存qi = reg_iret;
        栈顶值->r2 = reg_lret;
        break;
    case '^':
    case '&':
    case '|':
    case '*':
    case '+':
    case '-':
        //pv("生成长长整数操作 A",0,2);
        t = 栈顶值->类型.数据类型;
        vswap();
        堆栈64位扩展为2个整数();
        vrotb(3);
        堆栈64位扩展为2个整数();
        /* stack: L1 H1 L2 H2 */
        tmp = 栈顶值[0];
        栈顶值[0] = 栈顶值[-3];
        栈顶值[-3] = tmp;
        tmp = 栈顶值[-2];
        栈顶值[-2] = 栈顶值[-3];
        栈顶值[-3] = tmp;
        vswap();
        /* stack: H1 H2 L1 L2 */
        //pv("生成长长整数操作 B",0,4);
        if (op == '*') {
            值压入栈值(栈顶值 - 1);
            值压入栈值(栈顶值 - 1);
            gen_op(TOK_UMULL);
            堆栈64位扩展为2个整数();
            /* stack: H1 H2 L1 L2 ML MH */
            for(i=0;i<4;i++)
                vrotb(6);
            /* stack: ML MH H1 H2 L1 L2 */
            tmp = 栈顶值[0];
            栈顶值[0] = 栈顶值[-2];
            栈顶值[-2] = tmp;
            /* stack: ML MH H1 L2 H2 L1 */
            gen_op('*');
            vrotb(3);
            vrotb(3);
            gen_op('*');
            /* stack: ML MH M1 M2 */
            gen_op('+');
            gen_op('+');
        } else if (op == '+' || op == '-') {
            /* XXX: add non carry method too (for MIPS or alpha) */
            if (op == '+')
                op1 = TOK_ADDC1;
            else
                op1 = TOK_SUBC1;
            gen_op(op1);
            /* stack: H1 H2 (L1 op L2) */
            vrotb(3);
            vrotb(3);
            gen_op(op1 + 1); /* TOK_xxxC2 */
        } else {
            gen_op(op);
            /* stack: H1 H2 (L1 op L2) */
            vrotb(3);
            vrotb(3);
            /* stack: (L1 op L2) H1 H2 */
            gen_op(op);
            /* stack: (L1 op L2) (H1 op H2) */
        }
        /* stack: L H */
        两个整数构建为长长整数(t);
        break;
    case TOK_右移:
    case TOK_SHR:
    case TOK_左移:
        if ((栈顶值->寄存qi & (存储类型_掩码 | 存储类型_左值 | 存储类型_符号)) == 存储类型_VC常量) {
            t = 栈顶值[-1].类型.数据类型;
            vswap();
            堆栈64位扩展为2个整数();
            vrotb(3);
            /* stack: L H shift */
            c = (int)栈顶值->c.i;
            /* constant: simpler */
            /* NOTE: all comments are for SHL. the other cases are
               完成 by swapping words */
            弹出堆栈值();
            if (op != TOK_左移)
                vswap();
            if (c >= 32) {
                /* stack: L H */
                弹出堆栈值();
                if (c > 32) {
                    整数常量压入栈(c - 32);
                    gen_op(op);
                }
                if (op != TOK_右移) {
                    整数常量压入栈(0);
                } else {
                    堆栈条目转换为寄存器值复制到另个寄存器();
                    整数常量压入栈(31);
                    gen_op(TOK_右移);
                }
                vswap();
            } else {
                vswap();
                堆栈条目转换为寄存器值复制到另个寄存器();
                /* stack: H L L */
                整数常量压入栈(c);
                gen_op(op);
                vswap();
                整数常量压入栈(32 - c);
                if (op == TOK_左移)
                    gen_op(TOK_SHR);
                else
                    gen_op(TOK_左移);
                vrotb(3);
                /* stack: L L H */
                整数常量压入栈(c);
                if (op == TOK_左移)
                    gen_op(TOK_左移);
                else
                    gen_op(TOK_SHR);
                gen_op('|');
            }
            if (op != TOK_左移)
                vswap();
            两个整数构建为长长整数(t);
        } else {
            /* XXX: should provide a faster fallback on x86 ? */
            switch(op) {
            case TOK_右移:
                func = TOK___ashrdi3;
                goto gen_func;
            case TOK_SHR:
                func = TOK___lshrdi3;
                goto gen_func;
            case TOK_左移:
                func = TOK___ashldi3;
                goto gen_func;
            }
        }
        break;
    default:
        /* compare operations */
        t = 栈顶值->类型.数据类型;
        vswap();
        堆栈64位扩展为2个整数();
        vrotb(3);
        堆栈64位扩展为2个整数();
        /* stack: L1 H1 L2 H2 */
        tmp = 栈顶值[-1];
        栈顶值[-1] = 栈顶值[-2];
        栈顶值[-2] = tmp;
        /* stack: L1 L2 H1 H2 */
        save_regs(4);
        /* compare high */
        op1 = op;
        /* when values are equal, we need to compare low words. since
           the jump is inverted, we invert the test too. */
        if (op1 == TOK_小于)
            op1 = TOK_小于等于;
        else if (op1 == TOK_大于)
            op1 = TOK_大于等于;
        else if (op1 == TOK_ULT)
            op1 = TOK_ULE;
        else if (op1 == TOK_UGT)
            op1 = TOK_UGE;
        a = 0;
        b = 0;
        gen_op(op1);
        if (op == TOK_不等于) {
            b = 生成值测试(0, 0);
        } else {
            a = 生成值测试(1, 0);
            if (op != TOK_等于) {
                /* generate non equal test */
                整数常量压入栈(0);
                vset_VT_CMP(TOK_不等于);
                b = 生成值测试(0, 0);
            }
        }
        /* compare low. Always unsigned */
        op1 = op;
        if (op1 == TOK_小于)
            op1 = TOK_ULT;
        else if (op1 == TOK_小于等于)
            op1 = TOK_ULE;
        else if (op1 == TOK_大于)
            op1 = TOK_UGT;
        else if (op1 == TOK_大于等于)
            op1 = TOK_UGE;
        gen_op(op1);
        设置_生成值测试(1, a);
        设置_生成值测试(0, b);
        break;
    }
}
#endif

static uint64_t 处理整数常量优化_sdiv(uint64_t a, uint64_t b)
{
    uint64_t x = (a >> 63 ? -a : a) / (b >> 63 ? -b : b);
    return (a ^ b) >> 63 ? -x : x;
}

static int 处理整数常量优化_lt(uint64_t a, uint64_t b)
{
    return (a ^ (uint64_t)1 << 63) < (b ^ (uint64_t)1 << 63);
}

/* 处理整数常量优化和各种与机器无关的选择 */
static void 处理整数常量优化(int op)
{
    栈值ST *v1 = 栈顶值 - 1;
    栈值ST *v2 = 栈顶值;
    int t1 = v1->类型.数据类型 & 数据类型_基本类型;
    int t2 = v2->类型.数据类型 & 数据类型_基本类型;
    int c1 = (v1->寄存qi & (存储类型_掩码 | 存储类型_左值 | 存储类型_符号)) == 存储类型_VC常量;
    int c2 = (v2->寄存qi & (存储类型_掩码 | 存储类型_左值 | 存储类型_符号)) == 存储类型_VC常量;
    uint64_t l1 = c1 ? v1->c.i : 0;
    uint64_t l2 = c2 ? v2->c.i : 0;
    int shm = (t1 == 数据类型_长长整数) ? 63 : 31;

    if (t1 != 数据类型_长长整数 && (宏_指针大小 != 8 || t1 != 数据类型_指针))
        l1 = ((uint32_t)l1 |
              (v1->类型.数据类型 & 存储类型_无符号的 ? 0 : -(l1 & 0x80000000)));
    if (t2 != 数据类型_长长整数 && (宏_指针大小 != 8 || t2 != 数据类型_指针))
        l2 = ((uint32_t)l2 |
              (v2->类型.数据类型 & 存储类型_无符号的 ? 0 : -(l2 & 0x80000000)));

    if (c1 && c2)
    {
        switch(op)
        {
        case '+': l1 += l2; break;
        case '-': l1 -= l2; break;
        case '&': l1 &= l2; break;
        case '^': l1 ^= l2; break;
        case '|': l1 |= l2; break;
        case '*': l1 *= l2; break;

        case TOK_PDIV:
        case '/':
        case '%':
        case TOK_UDIV:
        case TOK_UMOD:
            /* 如果除以零，则生成显式除法 */
            if (l2 == 0)
            {
                if (需要_常量 && !(无需生成代码 & 未赋值子表达式))
                {
                	错_误("以常数除以零");
                }
                goto general_case;
            }
            switch(op) {
            default: l1 = 处理整数常量优化_sdiv(l1, l2); break;
            case '%': l1 = l1 - l2 * 处理整数常量优化_sdiv(l1, l2); break;
            case TOK_UDIV: l1 = l1 / l2; break;
            case TOK_UMOD: l1 = l1 % l2; break;
            }
            break;
        case TOK_左移: l1 <<= (l2 & shm); break;
        case TOK_SHR: l1 >>= (l2 & shm); break;
        case TOK_右移:
            l1 = (l1 >> 63) ? ~(~l1 >> (l2 & shm)) : l1 >> (l2 & shm);
            break;
            /* tests */
        case TOK_ULT: l1 = l1 < l2; break;
        case TOK_UGE: l1 = l1 >= l2; break;
        case TOK_等于: l1 = l1 == l2; break;
        case TOK_不等于: l1 = l1 != l2; break;
        case TOK_ULE: l1 = l1 <= l2; break;
        case TOK_UGT: l1 = l1 > l2; break;
        case TOK_小于: l1 = 处理整数常量优化_lt(l1, l2); break;
        case TOK_大于等于: l1 = !处理整数常量优化_lt(l1, l2); break;
        case TOK_小于等于: l1 = !处理整数常量优化_lt(l2, l1); break;
        case TOK_大于: l1 = 处理整数常量优化_lt(l2, l1); break;
            /* logical */
        case TOK_逻辑与: l1 = l1 && l2; break;
        case TOK_逻辑或: l1 = l1 || l2; break;
        default:
            goto general_case;
        }
	if (t1 != 数据类型_长长整数 && (宏_指针大小 != 8 || t1 != 数据类型_指针))
	    l1 = ((uint32_t)l1 |
		(v1->类型.数据类型 & 存储类型_无符号的 ? 0 : -(l1 & 0x80000000)));
        v1->c.i = l1;
        栈顶值--;
    } else {
        /* 如果是可交换操作，则将 c2 设为常量 */
        if (c1 && (op == '+' || op == '&' || op == '^' || 
                   op == '|' || op == '*' || op == TOK_等于 || op == TOK_不等于)) {
            vswap();
            c2 = c1; //c = c1, c1 = c2, c2 = c;
            l2 = l1; //l = l1, l1 = l2, l2 = l;
        }
        if (!需要_常量 && c1 && ((l1 == 0 && (op == TOK_左移 || op == TOK_SHR || op == TOK_右移)) || (l1 == -1 && op == TOK_右移)))
        {
            /* 将 (0 << x)、(0 >> x) 和 (-1 >> x) 视为常数 */
            栈顶值--;
        } else if (!需要_常量 && c2 && ((l2 == 0 && (op == '&' || op == '*')) || (op == '|' && (l2 == -1 || (l2 == 0xFFFFFFFF && t2 != 数据类型_长长整数))) || (l2 == 1 && (op == '%' || op == TOK_UMOD))))
        {
            /* 将 (x & 0)、(x * 0)、(x | -1) 和 (x % 1) 视为常数 */
            if (l2 == 1)
            {
            	栈顶值->c.i = 0;
            }
            vswap();
            栈顶值--;
        } else if (c2 && (((op == '*' || op == '/' || op == TOK_UDIV || op == TOK_PDIV) && l2 == 1) || ((op == '+' || op == '-' || op == '|' || op == '^' || op == TOK_左移 || op == TOK_SHR || op == TOK_右移) &&
                           l2 == 0) || (op == '&' && (l2 == -1 || (l2 == 0xFFFFFFFF && t2 != 数据类型_长长整数)))))
        {
            /* 过滤掉像 x*1, x-0, x&-1... 这样的 NOP 操作 */
            栈顶值--;
        } else if (c2 && (op == '*' || op == TOK_PDIV || op == TOK_UDIV))
        {
            /* 尝试使用班次而不是 muls 或 divs */
            if (l2 > 0 && (l2 & (l2 - 1)) == 0)
            {
                int n = -1;
                while (l2)
                {
                    l2 >>= 1;
                    n++;
                }
                栈顶值->c.i = n;
                if (op == '*')
                    op = TOK_左移;
                else if (op == TOK_PDIV)
                    op = TOK_右移;
                else
                    op = TOK_SHR;
            }
            goto general_case;
        } else if (c2 && (op == '+' || op == '-') &&
                   (((栈顶值[-1].寄存qi & (存储类型_掩码 | 存储类型_左值 | 存储类型_符号)) == (存储类型_VC常量 | 存储类型_符号))
                    || (栈顶值[-1].寄存qi & (存储类型_掩码 | 存储类型_左值)) == 存储类型_局部)) {
            /* symbol + constant case */
            if (op == '-')
                l2 = -l2;
	    l2 += 栈顶值[-1].c.i;
	    /* The backends can't always deal with addends to symbols
	       larger than +-1<<31.  Don't construct such.  */
	    if ((int)l2 != l2)
	        goto general_case;
            栈顶值--;
            栈顶值->c.i = l2;
        } else {
        general_case:
                /* call low level op generator */
                if (t1 == 数据类型_长长整数 || t2 == 数据类型_长长整数 ||
                    (宏_指针大小 == 8 && (t1 == 数据类型_指针 || t2 == 数据类型_指针)))
                    生成长长整数操作(op);
                else
                    gen_opi(op);
        }
    }
}

#if defined 目标_X86_64 || defined 目标_I386
# define gen_negf gen_opf
#elif defined 目标_ARM
void gen_negf(int op)
{
    /* arm 将检测 0-x 并替换为 vneg */
    整数常量压入栈(0), vswap(), gen_op('-');
}
#else
/* XXX: 在 gen_opf() 中也为其他后端实现 */
void gen_negf(int op)
{
    /* 在IEEE negate(x) 不是subtract(0,x)。 没有 NaN，它是减法（-0，x），但有了它们，它实际上是一个符号翻转操作。
     * 我们通过位操作来实现这一点，并且必须为此做一些类型重新解释，而 ZHI 只能通过内存来做到这一点。  */

    int 对齐, 大小, bt;

    大小 = 类型_大小(&栈顶值->类型, &对齐);
    bt = 栈顶值->类型.数据类型 & 数据类型_基本类型;
    save_reg(生成值(返回t的泛型寄存器类(bt)));
    vdup();
    incr_bf_adr(大小 - 1);
    vdup();
    整数常量压入栈(0x80); /* flip sign */
    gen_op('^');
    存储栈顶值();
    弹出堆栈值();
}
#endif

/* 生成具有恒定传播的浮点运算 */
static void gen_opif(int op)
{
    int c1, c2;
    栈值ST *v1, *v2;
#if defined _MSC_VER && defined __x86_64__
    /* avoid bad optimization with f1 -= f2 for f1:-0.0, f2:0.0 */
    volatile
#endif
    long double f1, f2;

    v1 = 栈顶值 - 1;
    v2 = 栈顶值;
    if (op == TOK_NEG)
        v1 = v2;

    /* currently, we cannot do computations with forward symbols */
    c1 = (v1->寄存qi & (存储类型_掩码 | 存储类型_左值 | 存储类型_符号)) == 存储类型_VC常量;
    c2 = (v2->寄存qi & (存储类型_掩码 | 存储类型_左值 | 存储类型_符号)) == 存储类型_VC常量;
    if (c1 && c2) {
        if (v1->类型.数据类型 == 数据类型_浮点) {
            f1 = v1->c.f;
            f2 = v2->c.f;
        } else if (v1->类型.数据类型 == 数据类型_双精度) {
            f1 = v1->c.d;
            f2 = v2->c.d;
        } else {
            f1 = v1->c.ld;
            f2 = v2->c.ld;
        }
        /* NOTE: we only do constant propagation if finite number (not
           NaN or infinity) (ANSI spec) */
        if (!(ieee_finite(f1) || !ieee_finite(f2)) && !需要_常量)
            goto general_case;
        switch(op) {
        case '+': f1 += f2; break;
        case '-': f1 -= f2; break;
        case '*': f1 *= f2; break;
        case '/': 
            if (f2 == 0.0) {
                union { float f; unsigned u; } x1, x2, y;
		/* If not in initializer we need to potentially generate
		   FP exceptions at runtime, otherwise we want to fold.  */
                if (!需要_常量)
                    goto general_case;
                /* the run-time result of 0.0/0.0 on x87, also of other compilers
                   when used to compile the f1 /= f2 below, would be -nan */
                x1.f = f1, x2.f = f2;
                if (f1 == 0.0)
                    y.u = 0x7fc00000; /* nan */
                else
                    y.u = 0x7f800000; /* infinity */
                y.u |= (x1.u ^ x2.u) & 0x80000000; /* set sign */
                f1 = y.f;
                break;
            }
            f1 /= f2;
            break;
        case TOK_NEG:
            f1 = -f1;
            goto unary_result;
            /* XXX: also handles tests ? */
        default:
            goto general_case;
        }
        栈顶值--;
    unary_result:
        /* XXX: overflow test ? */
        if (v1->类型.数据类型 == 数据类型_浮点) {
            v1->c.f = f1;
        } else if (v1->类型.数据类型 == 数据类型_双精度) {
            v1->c.d = f1;
        } else {
            v1->c.ld = f1;
        }
    } else {
    general_case:
        if (op == TOK_NEG) {
            gen_negf(op);
        } else {
            gen_opf(op);
        }
    }
}

/* 打印一种类型。如果“varstr”不为空，则该变量也为空，以该类型打印 */
/* XXX: union */
/* XXX: 添加数组和函数指针 */
static void type_to_str(char *buf, int buf_size,类型ST *类型, const char *varstr)
{
    int bt, v, t;
    符号ST *s, *sa;
    char buf1[256];
    const char *tstr;
    t = 类型->数据类型;
    bt = t & 数据类型_基本类型;
    buf[0] = '\0';
    if (t & 存储类型_外部)
    {
    	截取后拼接(buf, buf_size, "extern ");
    }
    if (t & 存储类型_静态)
    {
    	截取后拼接(buf, buf_size, "static ");
    }
    if (t & 存储类型_别名)
    {
    	截取后拼接(buf, buf_size, "typedef ");
    }
    if (t & 存储类型_内联)
    {
    	截取后拼接(buf, buf_size, "inline ");
    }
    if (bt != 数据类型_指针)
    {
        if (t & 存储类型_易变)
        {
        	截取后拼接(buf, buf_size, "volatile ");
        }
        if (t & 存储类型_常量)
        {
        	截取后拼接(buf, buf_size, "const ");
        }
    }
    if (((t & 存储类型_明确有无符号) && bt == 数据类型_字节) || ((t & 存储类型_无符号的) && (bt == 数据类型_短整数 || bt == 数据类型_整数 || bt == 数据类型_长长整数) && !是_枚举(t) ))
    {
    	截取后拼接(buf, buf_size, (t & 存储类型_无符号的) ? "unsigned " : "signed ");
    }
    buf_size -= strlen(buf);
    buf += strlen(buf);

    switch(bt) {
    case 数据类型_无类型:
        tstr = "void";
        goto add_tstr;
    case 数据类型_布尔:
        tstr = "_Bool";
        goto add_tstr;
    case 数据类型_字节:
        tstr = "char";
        goto add_tstr;
    case 数据类型_短整数:
        tstr = "short";
        goto add_tstr;
    case 数据类型_整数:
        tstr = "int";
        goto maybe_long;
    case 数据类型_长长整数:
        tstr = "long long";
    maybe_long:
        if (t & 数据类型_长整数)
        {
        	tstr = "long";
        }
        if (!是_枚举(t))
        {
        	goto add_tstr;
        }
        tstr = "enum ";
        goto tstruct;
    case 数据类型_浮点:
        tstr = "float";
        goto add_tstr;
    case 数据类型_双精度:
        tstr = "double";
        if (!(t & 数据类型_长整数))
        {
        	goto add_tstr;
        }
    case 数据类型_长双精度:
        tstr = "long double";
    add_tstr:
        截取后拼接(buf, buf_size, tstr);
        break;
    case 数据类型_结构体:
        tstr = "struct ";
        if (IS_UNION(t))
        {
        	tstr = "union ";
        }
    tstruct:
        截取后拼接(buf, buf_size, tstr);
        v = 类型->引用符号->v & ~符号_结构体;
        if (v >= 第一个匿名符号)
        {
        	截取后拼接(buf, buf_size, "<anonymous>");
        }
        else
        {
        	截取后拼接(buf, buf_size, 取单词字符串(v, NULL));
        }
        break;
    case 数据类型_函数:
        s = 类型->引用符号;
        buf1[0]=0;
        if (varstr && '*' == *varstr) {
            截取后拼接(buf1, sizeof(buf1), "(");
            截取后拼接(buf1, sizeof(buf1), varstr);
            截取后拼接(buf1, sizeof(buf1), ")");
        }
        截取后拼接(buf1, buf_size, "(");
        sa = s->next;
        while (sa != NULL) {
            char buf2[256];
            type_to_str(buf2, sizeof(buf2), &sa->类型, NULL);
            截取后拼接(buf1, sizeof(buf1), buf2);
            sa = sa->next;
            if (sa)
                截取后拼接(buf1, sizeof(buf1), ", ");
        }
        if (s->f.函数_类型== 函数原型_省略)
            截取后拼接(buf1, sizeof(buf1), ", ...");
        截取后拼接(buf1, sizeof(buf1), ")");
        type_to_str(buf, buf_size, &s->类型, buf1);
        goto no_var;
    case 数据类型_指针:
        s = 类型->引用符号;
        if (t & 数据类型_数组) {
            if (varstr && '*' == *varstr)
                snprintf(buf1, sizeof(buf1), "(%s)[%d]", varstr, s->c);
            else
                snprintf(buf1, sizeof(buf1), "%s[%d]", varstr ? varstr : "", s->c);
            type_to_str(buf, buf_size, &s->类型, buf1);
            goto no_var;
        }
        截取前n个字符(buf1, sizeof(buf1), "*");
        if (t & 存储类型_常量)
            截取后拼接(buf1, buf_size, "const ");
        if (t & 存储类型_易变)
            截取后拼接(buf1, buf_size, "volatile ");
        if (varstr)
            截取后拼接(buf1, sizeof(buf1), varstr);
        type_to_str(buf, buf_size, &s->类型, buf1);
        goto no_var;
    }
    if (varstr) {
        截取后拼接(buf, buf_size, " ");
        截取后拼接(buf, buf_size, varstr);
    }
 no_var: ;
}

static void 类型不兼容_错误(类型ST* st, 类型ST* dt, const char* fmt)
{
    char buf1[256], buf2[256];
    type_to_str(buf1, sizeof(buf1), st, NULL);
    type_to_str(buf2, sizeof(buf2), dt, NULL);
    错_误(fmt, buf1, buf2);
}

static void 类型不兼容警告(类型ST* st, 类型ST* dt, const char* fmt)
{
    char buf1[256], buf2[256];
    type_to_str(buf1, sizeof(buf1), st, NULL);
    type_to_str(buf2, sizeof(buf2), dt, NULL);
    zhi_警告(fmt, buf1, buf2);
}

static int pointed_size(类型ST *类型)
{
    int 对齐;
    return 类型_大小(返回指针类型(类型), &对齐);
}

static void vla_runtime_pointed_size(类型ST *类型)
{
    int 对齐;
    vla_运行时类型大小(返回指针类型(类型), &对齐);
}

static inline int is_null_pointer(栈值ST *p)
{
    if ((p->寄存qi & (存储类型_掩码 | 存储类型_左值 | 存储类型_符号)) != 存储类型_VC常量)
        return 0;
    return ((p->类型.数据类型 & 数据类型_基本类型) == 数据类型_整数 && (uint32_t)p->c.i == 0) ||
        ((p->类型.数据类型 & 数据类型_基本类型) == 数据类型_长长整数 && p->c.i == 0) ||
        ((p->类型.数据类型 & 数据类型_基本类型) == 数据类型_指针 &&
         (宏_指针大小 == 4 ? (uint32_t)p->c.i == 0 : p->c.i == 0) &&
         ((返回指针类型(&p->类型)->数据类型 & 数据类型_基本类型) == 数据类型_无类型) &&
         0 == (返回指针类型(&p->类型)->数据类型 & (存储类型_常量 | 存储类型_易变))
         );
}

/* compare function types. OLD functions match any new functions */
static int is_compatible_func(类型ST *类型1, 类型ST *type2)
{
    符号ST *s1, *s2;

    s1 = 类型1->引用符号;
    s2 = type2->引用符号;
    if (s1->f.函数调用约定 != s2->f.函数调用约定)
        return 0;
    if (s1->f.函数_类型!= s2->f.函数_类型
        && s1->f.函数_类型!= 函数原型_旧
        && s2->f.函数_类型!= 函数原型_旧)
        return 0;
    for (;;) {
        if (!是兼容的非限定类型(&s1->类型, &s2->类型))
            return 0;
        if (s1->f.函数_类型== 函数原型_旧 || s2->f.函数_类型== 函数原型_旧 )
            return 1;
        s1 = s1->next;
        s2 = s2->next;
        if (!s1)
            return !s2;
        if (!s2)
            return 0;
    }
}

/* return true if 类型1 and type2 are the same.  If unqualified is
   true, 限定符 on the types are ignored.
 */
static int 对比类型(类型ST *类型1, 类型ST *type2, int unqualified)
{
    int bt1, t1, t2;

    t1 = 类型1->数据类型 & 值的数据类型_TYPE;
    t2 = type2->数据类型 & 值的数据类型_TYPE;
    if (unqualified) {
        /* strip 限定符 before comparing */
        t1 &= ~(存储类型_常量 | 存储类型_易变);
        t2 &= ~(存储类型_常量 | 存储类型_易变);
    }

    /* Default Vs explicit signedness only matters for char */
    if ((t1 & 数据类型_基本类型) != 数据类型_字节) {
        t1 &= ~存储类型_明确有无符号;
        t2 &= ~存储类型_明确有无符号;
    }
    /* XXX: bitfields ? */
    if (t1 != t2)
        return 0;

    if ((t1 & 数据类型_数组)
        && !(类型1->引用符号->c < 0
          || type2->引用符号->c < 0
          || 类型1->引用符号->c == type2->引用符号->c))
            return 0;

    /* test more complicated cases */
    bt1 = t1 & 数据类型_基本类型;
    if (bt1 == 数据类型_指针) {
        类型1 = 返回指针类型(类型1);
        type2 = 返回指针类型(type2);
        return 是兼容类型(类型1, type2);
    } else if (bt1 == 数据类型_结构体) {
        return (类型1->引用符号 == type2->引用符号);
    } else if (bt1 == 数据类型_函数) {
        return is_compatible_func(类型1, type2);
    } else if (是_枚举(类型1->数据类型) && 是_枚举(type2->数据类型)) {
        /* If both are enums then they must be the same, if only one is then
           t1 and t2 must be equal, which was checked above already.  */
        return 类型1->引用符号 == type2->引用符号;
    } else {
        return 1;
    }
}

/* Check if OP1 and OP2 can be "combined" with operation OP, the combined
   类型 is stored in DEST if non-null (except for pointer plus/minus) . */
static int combine_types(类型ST *dest, 栈值ST *op1, 栈值ST *op2, int op)
{
    类型ST *类型1 = &op1->类型, *type2 = &op2->类型, 类型;
    int t1 = 类型1->数据类型, t2 = type2->数据类型, bt1 = t1 & 数据类型_基本类型, bt2 = t2 & 数据类型_基本类型;
    int ret = 1;

    类型.数据类型 = 数据类型_无类型;
    类型.引用符号 = NULL;

    if (bt1 == 数据类型_无类型 || bt2 == 数据类型_无类型) {
        ret = op == 英_问号 ? 1 : 0;
        /* NOTE: as an extension, we accept void on only one side */
        类型.数据类型 = 数据类型_无类型;
    } else if (bt1 == 数据类型_指针 || bt2 == 数据类型_指针) {
        if (op == '+') ; /* Handled in caller */
        /* http://port70.net/~nsz/c/c99/n1256.html#6.5.15p6 */
        /* If one is a null ptr constant the result 类型 is the other.  */
        else if (is_null_pointer (op2)) 类型 = *类型1;
        else if (is_null_pointer (op1)) 类型 = *type2;
        else if (bt1 != bt2) {
            /* accept comparison or cond-expr between pointer and integer
               with a warning */
            if ((op == 英_问号 || TOK_ISCOND(op))
                && (是_整数_基本类型(bt1) || 是_整数_基本类型(bt2)))
              zhi_警告("%s中的指针/整数不匹配",op == 英_问号 ? "条件表达式" : "比较");
            else if (op != '-' || !是_整数_基本类型(bt2))
              ret = 0;
            类型 = *(bt1 == 数据类型_指针 ? 类型1 : type2);
        } else {
            类型ST *pt1 = 返回指针类型(类型1);
            类型ST *pt2 = 返回指针类型(type2);
            int pbt1 = pt1->数据类型 & 数据类型_基本类型;
            int pbt2 = pt2->数据类型 & 数据类型_基本类型;
            int newquals, copied = 0;
            if (pbt1 != 数据类型_无类型 && pbt2 != 数据类型_无类型
                && !对比类型(pt1, pt2, 1/*unqualif*/)) {
                if (op != 英_问号 && !TOK_ISCOND(op))
                  ret = 0;
                else
                  类型不兼容警告(类型1, type2,
                    op == 英_问号 ? "条件表达式中的指针类型不匹配('%s' 和 '%s')"
                     : "比较中指针类型不匹配('%s' 和 '%s')");
            }
            if (op == 英_问号) {
                /* pointers to void get preferred, otherwise the
                   pointed to types minus qualifs should be compatible */
                类型 = *((pbt1 == 数据类型_无类型) ? 类型1 : type2);
                /* combine qualifs */
                newquals = ((pt1->数据类型 | pt2->数据类型) & (存储类型_常量 | 存储类型_易变));
                if ((~返回指针类型(&类型)->数据类型 & (存储类型_常量 | 存储类型_易变))
                    & newquals)
                  {
                    /* copy the pointer target symbol */
                    类型.引用符号 = 将符号放入符号栈(符号_字段, &类型.引用符号->类型,
                                        0, 类型.引用符号->c);
                    copied = 1;
                    返回指针类型(&类型)->数据类型 |= newquals;
                  }
                /* pointers to incomplete arrays get converted to
                   pointers to completed ones if possible */
                if (pt1->数据类型 & 数据类型_数组
                    && pt2->数据类型 & 数据类型_数组
                    && 返回指针类型(&类型)->引用符号->c < 0
                    && (pt1->引用符号->c > 0 || pt2->引用符号->c > 0))
                  {
                    if (!copied)
                      类型.引用符号 = 将符号放入符号栈(符号_字段, &类型.引用符号->类型,
                                          0, 类型.引用符号->c);
                    返回指针类型(&类型)->引用符号 =
                        将符号放入符号栈(符号_字段, &返回指针类型(&类型)->引用符号->类型,
                                 0, 返回指针类型(&类型)->引用符号->c);
                    返回指针类型(&类型)->引用符号->c =
                        0 < pt1->引用符号->c ? pt1->引用符号->c : pt2->引用符号->c;
                  }
            }
        }
        if (TOK_ISCOND(op))
          类型.数据类型 = 值的数据类型_SIZE_T;
    } else if (bt1 == 数据类型_结构体 || bt2 == 数据类型_结构体) {
        if (op != 英_问号 || !对比类型(类型1, type2, 1))
          ret = 0;
        类型 = *类型1;
    } else if (是_浮点(bt1) || 是_浮点(bt2)) {
        if (bt1 == 数据类型_长双精度 || bt2 == 数据类型_长双精度) {
            类型.数据类型 = 数据类型_长双精度;
        } else if (bt1 == 数据类型_双精度 || bt2 == 数据类型_双精度) {
            类型.数据类型 = 数据类型_双精度;
        } else {
            类型.数据类型 = 数据类型_浮点;
        }
    } else if (bt1 == 数据类型_长长整数 || bt2 == 数据类型_长长整数) {
        /* cast to biggest op */
        类型.数据类型 = 数据类型_长长整数 | 数据类型_长整数;
        if (bt1 == 数据类型_长长整数)
          类型.数据类型 &= t1;
        if (bt2 == 数据类型_长长整数)
          类型.数据类型 &= t2;
        /* convert to unsigned if it does not fit in a long long */
        if ((t1 & (数据类型_基本类型 | 存储类型_无符号的 | 存储类型_位域)) == (数据类型_长长整数 | 存储类型_无符号的) ||
            (t2 & (数据类型_基本类型 | 存储类型_无符号的 | 存储类型_位域)) == (数据类型_长长整数 | 存储类型_无符号的))
          类型.数据类型 |= 存储类型_无符号的;
    } else {
        /* integer operations */
        类型.数据类型 = 数据类型_整数 | (数据类型_长整数 & (t1 | t2));
        /* convert to unsigned if it does not fit in an integer */
        if ((t1 & (数据类型_基本类型 | 存储类型_无符号的 | 存储类型_位域)) == (数据类型_整数 | 存储类型_无符号的) ||
            (t2 & (数据类型_基本类型 | 存储类型_无符号的 | 存储类型_位域)) == (数据类型_整数 | 存储类型_无符号的))
          类型.数据类型 |= 存储类型_无符号的;
    }
    if (dest)
      *dest = 类型;
    return ret;
}

/* generic gen_op: handles types problems */
静态函数 void gen_op(int op)
{
    int u, t1, t2, bt1, bt2, t;
    类型ST 类型1, combtype;

重做:
    t1 = 栈顶值[-1].类型.数据类型;
    t2 = 栈顶值[0].类型.数据类型;
    bt1 = t1 & 数据类型_基本类型;
    bt2 = t2 & 数据类型_基本类型;
        
    if (bt1 == 数据类型_函数 || bt2 == 数据类型_函数) {
	if (bt2 == 数据类型_函数) {
	    生成指针类型(&栈顶值->类型);
	    gaddrof();
	}
	if (bt1 == 数据类型_函数) {
	    vswap();
	    生成指针类型(&栈顶值->类型);
	    gaddrof();
	    vswap();
	}
	goto 重做;
    } else if (!combine_types(&combtype, 栈顶值 - 1, 栈顶值, op)) {
        错误_不中止("invalid operand types for binary operation");
        弹出堆栈值();
    } else if (bt1 == 数据类型_指针 || bt2 == 数据类型_指针) {
        /* at least one operand is a pointer */
        /* relational op: must be both pointers */
        if (TOK_ISCOND(op))
            goto std_op;
        /* if both pointers, then it must be the '-' op */
        if (bt1 == 数据类型_指针 && bt2 == 数据类型_指针) {
            if (op != '-')
                错_误("这里不能使用指针");
            if (栈顶值[-1].类型.数据类型 & 存储类型_变长数组) {
                vla_runtime_pointed_size(&栈顶值[-1].类型);
            } else {
                整数常量压入栈(pointed_size(&栈顶值[-1].类型));
            }
            vrott(3);
            处理整数常量优化(op);
            栈顶值->类型.数据类型 = 值的数据类型_PTRDIFF_T;
            vswap();
            gen_op(TOK_PDIV);
        } else {
            /* exactly one pointer : must be '+' or '-'. */
            if (op != '-' && op != '+')
                错_误("这里不能使用指针");
            /* Put pointer as first operand */
            if (bt2 == 数据类型_指针) {
                vswap();
                t = t1, t1 = t2, t2 = t;
            }
#if 宏_指针大小 == 4
            if ((栈顶值[0].类型.数据类型 & 数据类型_基本类型) == 数据类型_长长整数)
                /* XXX: truncate here because 生成长长整数操作 can't handle ptr + long long */
                生成_转换_s(数据类型_整数);
#endif
            类型1 = 栈顶值[-1].类型;
            if (栈顶值[-1].类型.数据类型 & 存储类型_变长数组)
                vla_runtime_pointed_size(&栈顶值[-1].类型);
            else {
                u = pointed_size(&栈顶值[-1].类型);
                if (u < 0)
                    错_误("未知数组元素大小");
#if 宏_指针大小 == 8
                长长整数压入栈(u);
#else
                /* XXX: cast to int ? (long long case) */
                整数常量压入栈(u);
#endif
            }
            gen_op('*');
#ifdef 启用边界检查M
            if (全局虚拟机->使用_边界_检查器 && !需要_常量) {
                /* if bounded pointers, we generate a special code to
                   test bounds */
                if (op == '-') {
                    整数常量压入栈(0);
                    vswap();
                    gen_op('-');
                }
                gen_bounded_ptr_add();
            } else
#endif
            {
                处理整数常量优化(op);
            }
            类型1.数据类型 &= ~数据类型_数组;
            /* put again 类型 if 处理整数常量优化() swaped operands */
            栈顶值->类型 = 类型1;
        }
    } else {
        /* floats can only be used for a few operations */
        if (是_浮点(combtype.数据类型)
            && op != '+' && op != '-' && op != '*' && op != '/'
            && !TOK_ISCOND(op))
            错_误("二元运算的无效操作数");
        else if (op == TOK_SHR || op == TOK_右移 || op == TOK_左移) {
            t = bt1 == 数据类型_长长整数 ? 数据类型_长长整数 : 数据类型_整数;
            if ((t1 & (数据类型_基本类型 | 存储类型_无符号的 | 存储类型_位域)) == (t | 存储类型_无符号的))
              t |= 存储类型_无符号的;
            t |= (数据类型_长整数 & t1);
            combtype.数据类型 = t;
        }
    std_op:
        t = t2 = combtype.数据类型;
        /* XXX: currently, some unsigned operations are explicit, so
           we modify them here */
        if (t & 存储类型_无符号的) {
            if (op == TOK_右移)
                op = TOK_SHR;
            else if (op == '/')
                op = TOK_UDIV;
            else if (op == '%')
                op = TOK_UMOD;
            else if (op == TOK_小于)
                op = TOK_ULT;
            else if (op == TOK_大于)
                op = TOK_UGT;
            else if (op == TOK_小于等于)
                op = TOK_ULE;
            else if (op == TOK_大于等于)
                op = TOK_UGE;
        }
        vswap();
        生成_转换_s(t);
        vswap();
        /* special case for shifts and long long: we keep the shift as
           an integer */
        if (op == TOK_SHR || op == TOK_右移 || op == TOK_左移)
            t2 = 数据类型_整数;
        生成_转换_s(t2);
        if (是_浮点(t))
            gen_opif(op);
        else
            处理整数常量优化(op);
        if (TOK_ISCOND(op)) {
            /* relational op: the result is an int */
            栈顶值->类型.数据类型 = 数据类型_整数;
        } else {
            栈顶值->类型.数据类型 = t;
        }
    }
    // Make sure that we have converted to an rvalue:
    if (栈顶值->寄存qi & 存储类型_左值)
        生成值(是_浮点(栈顶值->类型.数据类型 & 数据类型_基本类型) ? RC_FLOAT : RC_INT);
}

#if defined 目标_ARM64 || defined 目标_RISCV64 || defined 目标_ARM
#define gen_cvt_itof1 gen_cvt_itof
#else
/* generic itof for unsigned long long case */
static void gen_cvt_itof1(int t)
{
    if ((栈顶值->类型.数据类型 & (数据类型_基本类型 | 存储类型_无符号的)) == 
        (数据类型_长长整数 | 存储类型_无符号的)) {

        if (t == 数据类型_浮点)
            推送_辅助_函数引用(TOK___floatundisf);
#if 长双精度大小 != 8
        else if (t == 数据类型_长双精度)
            推送_辅助_函数引用(TOK___floatundixf);
#endif
        else
            推送_辅助_函数引用(TOK___floatundidf);
        vrott(2);
        g函数_调用(1);
        整数常量压入栈(0);
        将函数返回寄存器放入堆栈值(栈顶值, t);
    } else {
        gen_cvt_itof(t);
    }
}
#endif

#if defined 目标_ARM64 || defined 目标_RISCV64
#define gen_cvt_ftoi1 gen_cvt_ftoi
#else
/* generic ftoi for unsigned long long case */
static void gen_cvt_ftoi1(int t)
{
    int st;
    if (t == (数据类型_长长整数 | 存储类型_无符号的)) {
        /* not handled natively */
        st = 栈顶值->类型.数据类型 & 数据类型_基本类型;
        if (st == 数据类型_浮点)
            推送_辅助_函数引用(TOK___fixunssfdi);
#if 长双精度大小 != 8
        else if (st == 数据类型_长双精度)
            推送_辅助_函数引用(TOK___fixunsxfdi);
#endif
        else
            推送_辅助_函数引用(TOK___fixunsdfdi);
        vrott(2);
        g函数_调用(1);
        整数常量压入栈(0);
        将函数返回寄存器放入堆栈值(栈顶值, t);
    } else {
        gen_cvt_ftoi(t);
    }
}
#endif

/* char/short 的特殊延迟转换 */
static void 强制_字符短整数_转换(void)
{
    int sbt = BFGET(栈顶值->寄存qi, 存储类型_必须转换) == 2 ? 数据类型_长长整数 : 数据类型_整数;
    int dbt = 栈顶值->类型.数据类型;
    栈顶值->寄存qi &= ~存储类型_必须转换;
    栈顶值->类型.数据类型 = sbt;
    生成_转换_s(dbt == 数据类型_布尔 ? 数据类型_字节|存储类型_无符号的 : dbt);
    栈顶值->类型.数据类型 = dbt;
}

static void 生成_转换_s(int t)
{
    类型ST 类型;
    类型.数据类型 = t;
    类型.引用符号 = NULL;
    生成_转换(&类型);
}

/* cast '栈顶值' to '类型'. Casting to bitfields is forbidden. */
static void 生成_转换(类型ST *类型)
{
    int sbt, dbt, sf, df, c;
    int dbt_bt, sbt_bt, ds, ss, bits, trunc;

    /* special delayed cast for char/short */
    if (栈顶值->寄存qi & 存储类型_必须转换)
        强制_字符短整数_转换();

    /* bitfields first get cast to ints */
    if (栈顶值->类型.数据类型 & 存储类型_位域)
        生成值(RC_INT);

    dbt = 类型->数据类型 & (数据类型_基本类型 | 存储类型_无符号的);
    sbt = 栈顶值->类型.数据类型 & (数据类型_基本类型 | 存储类型_无符号的);
    if (sbt == 数据类型_函数)
        sbt = 数据类型_指针;

again:
    if (sbt != dbt) {
        sf = 是_浮点(sbt);
        df = 是_浮点(dbt);
        dbt_bt = dbt & 数据类型_基本类型;
        sbt_bt = sbt & 数据类型_基本类型;
        if (dbt_bt == 数据类型_无类型)
            goto 完成;
        if (sbt_bt == 数据类型_无类型) {
error:
            转换_错误(&栈顶值->类型, 类型);
        }

        c = (栈顶值->寄存qi & (存储类型_掩码 | 存储类型_左值 | 存储类型_符号)) == 存储类型_VC常量;
#if !defined 运行脚本M && !defined ZHI_IS_NATIVE_387
        c &= (dbt != 数据类型_长双精度) | !!无需生成代码;
#endif
        if (c) {
            /* constant case: we can do it now */
            /* XXX: in ISOC, cannot do it if error in convert */
            if (sbt == 数据类型_浮点)
                栈顶值->c.ld = 栈顶值->c.f;
            else if (sbt == 数据类型_双精度)
                栈顶值->c.ld = 栈顶值->c.d;

            if (df) {
                if (sbt_bt == 数据类型_长长整数) {
                    if ((sbt & 存储类型_无符号的) || !(栈顶值->c.i >> 63))
                        栈顶值->c.ld = 栈顶值->c.i;
                    else
                        栈顶值->c.ld = -(long double)-栈顶值->c.i;
                } else if(!sf) {
                    if ((sbt & 存储类型_无符号的) || !(栈顶值->c.i >> 31))
                        栈顶值->c.ld = (uint32_t)栈顶值->c.i;
                    else
                        栈顶值->c.ld = -(long double)-(uint32_t)栈顶值->c.i;
                }

                if (dbt == 数据类型_浮点)
                    栈顶值->c.f = (float)栈顶值->c.ld;
                else if (dbt == 数据类型_双精度)
                    栈顶值->c.d = (double)栈顶值->c.ld;
            } else if (sf && dbt == 数据类型_布尔) {
                栈顶值->c.i = (栈顶值->c.ld != 0);
            } else {
                if(sf)
                    栈顶值->c.i = 栈顶值->c.ld;
                else if (sbt_bt == 数据类型_长长整数 || (宏_指针大小 == 8 && sbt == 数据类型_指针))
                    ;
                else if (sbt & 存储类型_无符号的)
                    栈顶值->c.i = (uint32_t)栈顶值->c.i;
                else
                    栈顶值->c.i = ((uint32_t)栈顶值->c.i | -(栈顶值->c.i & 0x80000000));

                if (dbt_bt == 数据类型_长长整数 || (宏_指针大小 == 8 && dbt == 数据类型_指针))
                    ;
                else if (dbt == 数据类型_布尔)
                    栈顶值->c.i = (栈顶值->c.i != 0);
                else {
                    uint32_t m = dbt_bt == 数据类型_字节 ? 0xff :
                                 dbt_bt == 数据类型_短整数 ? 0xffff :
                                  0xffffffff;
                    栈顶值->c.i &= m;
                    if (!(dbt & 存储类型_无符号的))
                        栈顶值->c.i |= -(栈顶值->c.i & ((m >> 1) + 1));
                }
            }
            goto 完成;

        } else if (dbt == 数据类型_布尔
            && (栈顶值->寄存qi & (存储类型_掩码 | 存储类型_左值 | 存储类型_符号))
                == (存储类型_VC常量 | 存储类型_符号)) {
            /* addresses are considered non-zero (see zhitest.c:sinit23) */
            栈顶值->寄存qi = 存储类型_VC常量;
            栈顶值->c.i = 1;
            goto 完成;
        }

        /* cannot generate code for global or static initializers */
        if (仅需静态数据输出)
            goto 完成;

        /* 非常量情况：生成代码 */
        if (dbt == 数据类型_布尔) {
            生成零或非零测试(TOK_不等于);
            goto 完成;
        }

        if (sf || df) {
            if (sf && df) {
                /* 从 fp 转换为 fp */
                gen_cvt_ftof(dbt);
            } else if (df) {
                /* 将 int 转换为 fp */
                gen_cvt_itof1(dbt);
            } else {
                /* 将 fp 转换为 int */
                sbt = dbt;
                if (dbt_bt != 数据类型_长长整数 && dbt_bt != 数据类型_整数)
                    sbt = 数据类型_整数;
                gen_cvt_ftoi1(sbt);
                goto again; /* 可能需要字符/短cast */
            }
            goto 完成;
        }

        ds = 基本类型_大小(dbt_bt);
        ss = 基本类型_大小(sbt_bt);
        if (ds == 0 || ss == 0)
            goto error;

        if (是_枚举(类型->数据类型) && 类型->引用符号->c < 0)
            错_误("强制转换到不完整类型");

        /* 相同的大小，不需要符号转换 */
        if (ds == ss && ds >= 4)
            goto 完成;
        if (dbt_bt == 数据类型_指针 || sbt_bt == 数据类型_指针)
        {
            zhi_警告("在不同大小的指针和整数之间进行转换");
            if (sbt_bt == 数据类型_指针)
            {
                /* 放置整数类型以允许下面的逻辑运算 */
                栈顶值->类型.数据类型 = (宏_指针大小 == 8 ? 数据类型_长长整数 : 数据类型_整数);
            }
        }

        /* 处理器允许 { int a = 0, b = *(char*)&a; 这意味着如果我们转换为更小的宽度，我们可以更改类型并在以后阅读。 */
        #define ALLOW_SUBTYPE_ACCESS 1

        if (ALLOW_SUBTYPE_ACCESS && (栈顶值->寄存qi & 存储类型_左值))
        {
            /* 值仍在内存中 */
            if (ds <= ss)
                goto 完成;
            /* ss <= 4 here */
            if (ds <= 4 && !(dbt == (数据类型_短整数 | 存储类型_无符号的) && sbt == 数据类型_字节))
            {
                生成值(RC_INT);
                goto 完成; /* 不涉及 64 位 */
            }
        }
        生成值(RC_INT);

        trunc = 0;
#if 宏_指针大小 == 4
        if (ds == 8) {
            /* generate high word */
            if (sbt & 存储类型_无符号的) {
                整数常量压入栈(0);
                生成值(RC_INT);
            } else {
                堆栈条目转换为寄存器值复制到另个寄存器();
                整数常量压入栈(31);
                gen_op(TOK_右移);
            }
            两个整数构建为长长整数(dbt);
        } else if (ss == 8) {
            /* from long long: just take low order word */
            堆栈64位扩展为2个整数();
            弹出堆栈值();
        }
        ss = 4;

#elif 宏_指针大小 == 8
        if (ds == 8) {
            /* need to convert from 32bit to 64bit */
            if (sbt & 存储类型_无符号的) {
#if defined(目标_RISCV64)
                /* RISC-V keeps 32bit vals in registers sign-extended.
                   So here we need a zero-extension.  */
                trunc = 32;
#else
                goto 完成;
#endif
            } else {
                gen_cvt_sxtw();
                goto 完成;
            }
            ss = ds, ds = 4, dbt = sbt;
        } else if (ss == 8) {
            /* RISC-V keeps 32bit vals in registers sign-extended.
               So here we need a sign-extension for signed types and
               zero-extension. for unsigned types. */
#if !defined(目标_RISCV64)
            trunc = 32; /* zero upper 32 bits for non RISC-V targets */
#endif
        } else {
            ss = 4;
        }
#endif

        if (ds >= ss)
            goto 完成;
#if defined 目标_I386 || defined 目标_X86_64 || defined 目标_ARM64
        if (ss == 4) {
            gen_cvt_csti(dbt);
            goto 完成;
        }
#endif
        bits = (ss - ds) * 8;
        /* for unsigned, gen_op will convert SAR to SHR */
        栈顶值->类型.数据类型 = (ss == 8 ? 数据类型_长长整数 : 数据类型_整数) | (dbt & 存储类型_无符号的);
        整数常量压入栈(bits);
        gen_op(TOK_左移);
        整数常量压入栈(bits - trunc);
        gen_op(TOK_右移);
        整数常量压入栈(trunc);
        gen_op(TOK_SHR);
    }
完成:
    栈顶值->类型 = *类型;
    栈顶值->类型.数据类型 &= ~ ( 存储类型_常量 | 存储类型_易变 | 数据类型_数组 );
}

/* 返回在编译时已知的类型大小。 将对齐放在 'a' */
静态函数 int 类型_大小(类型ST *类型, int *a)
{
    符号ST *s;
    int bt;

    bt = 类型->数据类型 & 数据类型_基本类型;
    if (bt == 数据类型_结构体) {
        /* struct/union */
        s = 类型->引用符号;
        *a = s->寄存qi;
        return s->c;
    } else if (bt == 数据类型_指针) {
        if (类型->数据类型 & 数据类型_数组) {
            int ts;

            s = 类型->引用符号;
            ts = 类型_大小(&s->类型, a);

            if (ts < 0 && s->c < 0)
                ts = -ts;

            return ts * s->c;
        } else {
            *a = 宏_指针大小;
            return 宏_指针大小;
        }
    } else if (是_枚举(类型->数据类型) && 类型->引用符号->c < 0) {
        return -1; /* incomplete enum */
    } else if (bt == 数据类型_长双精度) {
        *a = 长双精度对齐单位;
        return 长双精度大小;
    } else if (bt == 数据类型_双精度 || bt == 数据类型_长长整数) {
#ifdef 目标_I386
#ifdef 目标_PE
        *a = 8;
#else
        *a = 4;
#endif
#elif defined(目标_ARM)
#ifdef ZHI_ARM_EABI
        *a = 8; 
#else
        *a = 4;
#endif
#else
        *a = 8;
#endif
        return 8;
    } else if (bt == 数据类型_整数 || bt == 数据类型_浮点) {
        *a = 4;
        return 4;
    } else if (bt == 数据类型_短整数) {
        *a = 2;
        return 2;
    } else if (bt == 数据类型_128位整数 || bt == 数据类型_128位浮点) {
        *a = 8;
        return 16;
    } else {
        /* char, void, function, _Bool */
        *a = 1;
        return 1;
    }
}

/* 在运行时在值堆栈顶部推送类型大小。 将对齐放在 'a' */
静态函数 void vla_运行时类型大小(类型ST *类型, int *a)
{
    if (类型->数据类型 & 存储类型_变长数组) {
        类型_大小(&类型->引用符号->类型, a);
        值压入栈(&整数_类型, 存储类型_局部|存储类型_左值, 类型->引用符号->c);
    } else {
        整数常量压入栈(类型_大小(类型, a));
    }
}

/* 返回 t 的指针类型 */
static inline 类型ST *返回指针类型(类型ST *类型)
{
    return &类型->引用符号->类型;
}

/* 修改类型，使其成为类型的指针。 */
静态函数 void 生成指针类型(类型ST *类型)
{
    符号ST *s;
    s = 将符号放入符号栈(符号_字段, 类型, 0, -1);
    类型->数据类型 = 数据类型_指针 | (类型->数据类型 & 值的数据类型_贮存);
    类型->引用符号 = s;
}

/* return true if 类型1 and type2 are exactly the same (including
   限定符). 
*/
static int 是兼容类型(类型ST *类型1, 类型ST *type2)
{
    return 对比类型(类型1,type2,0);
}

/* return true if 类型1 and type2 are the same (ignoring 限定符).
*/
static int 是兼容的非限定类型(类型ST *类型1, 类型ST *type2)
{
    return 对比类型(类型1,type2,1);
}

static void 转换_错误(类型ST *st, 类型ST *dt)
{
    类型不兼容_错误(st, dt, "无法将 '%s' 转换为 '%s'");
}

/* 验证以“dt”类型存储栈顶值的类型兼容性 */
static void 验证指定类型转换(类型ST *dt)
{
    类型ST *st, *类型1, *type2;
    int dbt, sbt, qualwarn, lvl;

    st = &栈顶值->类型; /* source 类型 */
    dbt = dt->数据类型 & 数据类型_基本类型;
    sbt = st->数据类型 & 数据类型_基本类型;
    if (dt->数据类型 & 存储类型_常量)
        zhi_警告("只读位置的分配");
    switch(dbt) {
    case 数据类型_无类型:
        if (sbt != dbt)
            错_误("无效（void）表达式的赋值");
        break;
    case 数据类型_指针:
        /* 指针的特殊情况 */
        /* “0”也可以是指针 */
        if (is_null_pointer(栈顶值))
            break;
        /* 接受带警告的整数强制转换的隐式指针 */
        if (是_整数_基本类型(sbt)) {
            zhi_警告("赋值使指针来自整数而不进行强制转换");
            break;
        }
        类型1 = 返回指针类型(dt);
        if (sbt == 数据类型_指针)
            type2 = 返回指针类型(st);
        else if (sbt == 数据类型_函数)
            type2 = st; /* 函数隐含地是函数指针 */
        else
            goto error;
        if (是兼容类型(类型1, type2))
            break;
        for (qualwarn = lvl = 0;; ++lvl) {
            if (((type2->数据类型 & 存储类型_常量) && !(类型1->数据类型 & 存储类型_常量)) ||
                ((type2->数据类型 & 存储类型_易变) && !(类型1->数据类型 & 存储类型_易变)))
                qualwarn = 1;
            dbt = 类型1->数据类型 & (数据类型_基本类型|数据类型_长整数);
            sbt = type2->数据类型 & (数据类型_基本类型|数据类型_长整数);
            if (dbt != 数据类型_指针 || sbt != 数据类型_指针)
                break;
            类型1 = 返回指针类型(类型1);
            type2 = 返回指针类型(type2);
        }
        if (!是兼容的非限定类型(类型1, type2)) {
            if ((dbt == 数据类型_无类型 || sbt == 数据类型_无类型) && lvl == 0) {
                /* void*可以匹配任何东西 */
            } else if (dbt == sbt
                && 是_整数_基本类型(sbt & 数据类型_基本类型)
                && 是_枚举(类型1->数据类型) + 是_枚举(type2->数据类型)
                    + !!((类型1->数据类型 ^ type2->数据类型) & 存储类型_无符号的) < 2) {
		/* 与GCC一样，默认情况下仅对指针目标签名的更改不发出警告。不过，请对不同的基类型发出警告，特别是对未签名的枚举和已签名的int目标。 */
            } else {
                zhi_警告("从不兼容的指针类型赋值");
                break;
            }
        }
        if (qualwarn)
            zhi_警告_c(警告丢弃限定符)("赋值从指针目标类型中丢弃限定符");
        break;
    case 数据类型_字节:
    case 数据类型_短整数:
    case 数据类型_整数:
    case 数据类型_长长整数:
        if (sbt == 数据类型_指针 || sbt == 数据类型_函数) {
            zhi_警告("赋值从指针生成整数，无需强制转换");
        } else if (sbt == 数据类型_结构体) {
            goto case_VT_结构体;
        }
        /* XXX: more tests */
        break;
    case 数据类型_结构体:
    case_VT_结构体:
        if (!是兼容的非限定类型(dt, st)) {
    error:
            转换_错误(st, dt);
        }
        break;
    }
}

static void 生成指定转换(类型ST *dt)
{
    验证指定类型转换(dt);
    生成_转换(dt);
}

/* 存储栈顶值 in lvalue 压入栈 */
静态函数 void 存储栈顶值(void)
{
    int sbt, dbt, ft, r, 大小, 对齐, 位大小, bit_pos, delayed_cast;

    ft = 栈顶值[-1].类型.数据类型;
    sbt = 栈顶值->类型.数据类型 & 数据类型_基本类型;
    dbt = ft & 数据类型_基本类型;

    验证指定类型转换(&栈顶值[-1].类型);

    if (sbt == 数据类型_结构体) {
        /* if structure, only generate pointer */
        /* structure assignment : generate memcpy */
        /* XXX: optimize if small 大小 */
            大小 = 类型_大小(&栈顶值->类型, &对齐);

            /* destination */
            vswap();
#ifdef 启用边界检查M
            if (栈顶值->寄存qi & 存储类型_必须边界检查)
                gbound(); /* check would be wrong after gaddrof() */
#endif
            栈顶值->类型.数据类型 = 数据类型_指针;
            gaddrof();

            /* address of memcpy() */
#ifdef ZHI_ARM_EABI
            if(!(对齐 & 7))
                推送_辅助_函数引用(TOK_memmove8);
            else if(!(对齐 & 3))
                推送_辅助_函数引用(TOK_memmove4);
            else
#endif
            /* Use memmove, rather than memcpy, as dest and src may be same: */
            推送_辅助_函数引用(TOK_memmove);

            vswap();
            /* source */
            值压入栈值(栈顶值 - 2);
#ifdef 启用边界检查M
            if (栈顶值->寄存qi & 存储类型_必须边界检查)
                gbound();
#endif
            栈顶值->类型.数据类型 = 数据类型_指针;
            gaddrof();
            /* 类型 大小 */
            整数常量压入栈(大小);
            g函数_调用(3);
        /* leave source on stack */

    } else if (ft & 存储类型_位域) {
        /* bitfield store handling */

        /* save lvalue as expression result (example: s.b = s.a = n;) */
        vdup(), 栈顶值[-1] = 栈顶值[-2];

        bit_pos = BIT_POS(ft);
        位大小 = BIT_SIZE(ft);
        /* remove bit field info to avoid loops */
        栈顶值[-1].类型.数据类型 = ft & ~值的数据类型_结构体_MASK;

        if (dbt == 数据类型_布尔) {
            生成_转换(&栈顶值[-1].类型);
            栈顶值[-1].类型.数据类型 = (栈顶值[-1].类型.数据类型 & ~数据类型_基本类型) | (数据类型_字节 | 存储类型_无符号的);
        }
        r = adjust_bf(栈顶值 - 1, bit_pos, 位大小);
        if (dbt != 数据类型_布尔) {
            生成_转换(&栈顶值[-1].类型);
            dbt = 栈顶值[-1].类型.数据类型 & 数据类型_基本类型;
        }
        if (r == 数据类型_结构体) {
            打包或未对齐位域的单字节_存储(bit_pos, 位大小);
        } else {
            unsigned long long mask = (1ULL << 位大小) - 1;
            if (dbt != 数据类型_布尔) {
                /* mask source */
                if (dbt == 数据类型_长长整数)
                    长长整数压入栈(mask);
                else
                    整数常量压入栈((unsigned)mask);
                gen_op('&');
            }
            /* shift source */
            整数常量压入栈(bit_pos);
            gen_op(TOK_左移);
            vswap();
            /* duplicate destination */
            vdup();
            vrott(3);
            /* load destination, mask and or with source */
            if (dbt == 数据类型_长长整数)
                长长整数压入栈(~(mask << bit_pos));
            else
                整数常量压入栈(~((unsigned)mask << bit_pos));
            gen_op('&');
            gen_op('|');
            /* store result */
            存储栈顶值();
            /* ... and discard */
            弹出堆栈值();
        }
    } else if (dbt == 数据类型_无类型) {
        --栈顶值;
    } else {
            /* optimize char/short casts */
            delayed_cast = 0;
            if ((dbt == 数据类型_字节 || dbt == 数据类型_短整数)
                && 是_整数_基本类型(sbt)
                ) {
                if ((栈顶值->寄存qi & 存储类型_必须转换)
                    && 基本类型_大小(dbt) > 基本类型_大小(sbt)
                    )
                    强制_字符短整数_转换();
                delayed_cast = 1;
            } else {
                生成_转换(&栈顶值[-1].类型);
            }

#ifdef 启用边界检查M
            /* bound check case */
            if (栈顶值[-1].寄存qi & 存储类型_必须边界检查) {
                vswap();
                gbound();
                vswap();
            }
#endif
            生成值(返回t的泛型寄存器类(dbt)); /* generate value */

            if (delayed_cast) {
                栈顶值->寄存qi |= BFVAL(存储类型_必须转换, (sbt == 数据类型_长长整数) + 1);
                //zhi_警告("deley cast %x -> %x", sbt, dbt);
                栈顶值->类型.数据类型 = ft & 值的数据类型_TYPE;
            }

            /* if lvalue was saved on stack, must read it */
            if ((栈顶值[-1].寄存qi & 存储类型_掩码) == 存储类型_LLOCAL) {
                栈值ST sv;
                r = get_reg(RC_INT);
                sv.类型.数据类型 = 值的数据类型_PTRDIFF_T;
                sv.寄存qi = 存储类型_局部 | 存储类型_左值;
                sv.c.i = 栈顶值[-1].c.i;
                load(r, &sv);
                栈顶值[-1].寄存qi = r | 存储类型_左值;
            }

            r = 栈顶值->寄存qi & 存储类型_掩码;
            /* two word case handling :
               store second register at word + 4 (or +8 for x86-64)  */
            if (USING_TWO_WORDS(dbt)) {
                int load_type = (dbt == 数据类型_128位浮点) ? 数据类型_双精度 : 值的数据类型_PTRDIFF_T;
                栈顶值[-1].类型.数据类型 = load_type;
                store(r, 栈顶值 - 1);
                vswap();
                /* convert to int to increment easily */
                栈顶值->类型.数据类型 = 值的数据类型_PTRDIFF_T;
                gaddrof();
                指针大小的常量压入栈(宏_指针大小);
                gen_op('+');
                栈顶值->寄存qi |= 存储类型_左值;
                vswap();
                栈顶值[-1].类型.数据类型 = load_type;
                /* XXX: it works because r2 is spilled last ! */
                store(栈顶值->r2, 栈顶值 - 1);
            } else {
                /* single word */
                store(r, 栈顶值 - 1);
            }
        vswap();
        栈顶值--; /* NOT 弹出堆栈值() because on x86 it would flush the fp stack */
    }
}

/* post 定义 POST/PRE 添加。 c 是单词 ++ 或 -- */
静态函数 void inc(int post, int c)
{
    测试_左值();
    vdup(); /* save lvalue */
    if (post) {
        堆栈条目转换为寄存器值复制到另个寄存器(); /* duplicate value */
        vrotb(3);
        vrotb(3);
    }
    整数常量压入栈(c - TOK_MID); 
    gen_op('+');
    存储栈顶值(); /* store value */
    if (post)
        弹出堆栈值(); /* 如果 post op，返回保存的值 */
}

静态函数 void 解析多个字符串 (动态字符串ST *astr, const char *msg)
{
    /* 读字符串 */
    if (单词编码 != TOK_字符串常量)
    {
    	应为(msg);
    }
    动态字符串_新建(astr);
    while (单词编码 == TOK_字符串常量)
    {
        /* XXX: 是否也添加\0处理？*/
        动态字符串_拼接(astr, 单词常量.字符串.数据, -1);
        取下个单词();
    }
    动态字符串_追加一个字符(astr, '\0');
}

/* 如果 I >= 1 并且是 2 的幂，则返回 log2(i)+1。 如果 I 为 0，则返回 0。 */
静态函数 int 精确_2幂加1(int i)
{
  int ret;
  if (!i)
    return 0;
  for (ret = 1; i >= 1 << 8; ret += 8)
    i >>= 8;
  if (i >= 1 << 4)
    ret += 4, i >>= 4;
  if (i >= 1 << 2)
    ret += 2, i >>= 2;
  if (i >= 1 << 1)
    ret++;
  return ret;
}

/* 解析 __attribute__((...)) GNUC扩展. 如果没有 __attribute__((...))修饰，就会直接跳过此解析*/
static void 解析属性(GNUC属性ST *属性)
{
    int t, n;
    动态字符串ST 属性的字符串;
    
重做:
    if (单词编码 != TOK_ATTRIBUTE1 && 单词编码 != TOK_ATTRIBUTE2)
    {
    	return;
    }
    取下个单词();
    跳过(英_左小括号);
    跳过(英_左小括号);
    while (单词编码 != 英_右小括号)
    {
        if (单词编码 < TOK_标识符)
        {
        	应为("属性名");
        }
        t = 单词编码;
        取下个单词();
        switch(t)
        {
		case TOK_CLEANUP1:
		case TOK_CLEANUP2:
		{
			符号ST *s;
			跳过(英_左小括号);
			s = 查找标识符(单词编码);
			if (!s)
			{
				zhi_警告_c(隐式函数声明警告)("函数 '%s'的隐式声明", 取单词字符串(单词编码, &单词常量));
				s = 外部_全局_符号(单词编码, &函数_旧_类型);
			} else if ((s->类型.数据类型 & 数据类型_基本类型) != 数据类型_函数)
			{
				错_误("'%s' 未声明为函数", 取单词字符串(单词编码, &单词常量));
			}
			属性->清理_函数 = s;
			取下个单词();
				跳过(英_右小括号);
			break;
		}
        case TOK_CONSTRUCTOR1:
        case TOK_CONSTRUCTOR2:
            属性->f.函数_构造函数 = 1;
            break;
        case TOK_DESTRUCTOR1:
        case TOK_DESTRUCTOR2:
            属性->f.函数_析构函数 = 1;
            break;
        case TOK_ALWAYS_INLINE1:
        case TOK_ALWAYS_INLINE2:
            属性->f.函数_始终内联 = 1;
            break;
        case 英_节:
        case TOK_SECTION2:
            跳过(英_左小括号);
	        解析多个字符串(&属性的字符串, "节名");
            属性->section = 查找_节_创建(全局虚拟机, (char *)属性的字符串.数据, 1);
            跳过(英_右小括号);
	        动态字符串_释放(&属性的字符串);
            break;
        case 英_别名1:
        case 英_别名2:
            跳过(英_左小括号);
	        解析多个字符串(&属性的字符串, "alias(\"target\")");
            属性->别名_目标 = /* 将字符串另存为词法单元，以备后用 */
            插入单词((char*)属性的字符串.数据, 属性的字符串.大小-1)->单词编码;
            跳过(英_右小括号);
	        动态字符串_释放(&属性的字符串);
            break;
	case TOK_VISIBILITY1:
	case TOK_VISIBILITY2:
            跳过(英_左小括号);
	    解析多个字符串(&属性的字符串,"能见度(\"default|hidden|internal|protected\")");
	    if (!strcmp (属性的字符串.数据, "default"))
	        属性->a.能见度 = STV_DEFAULT;
	    else if (!strcmp (属性的字符串.数据, "hidden"))
	        属性->a.能见度 = STV_HIDDEN;
	    else if (!strcmp (属性的字符串.数据, "internal"))
	        属性->a.能见度 = STV_INTERNAL;
	    else if (!strcmp (属性的字符串.数据, "protected"))
	        属性->a.能见度 = STV_PROTECTED;
	    else
                应为("可见性(\"默认|隐藏|内部|受保护\")");
            跳过(英_右小括号);
	    动态字符串_释放(&属性的字符串);
            break;
        case TOK_ALIGNED1:
        case TOK_ALIGNED2:
            if (单词编码 == 英_左小括号)
            {
                取下个单词();
                n = 常量表达式解析();
                if (n <= 0 || (n & (n - 1)) != 0) 
                {
                	错_误("对齐必须是2的正幂");
                }
                跳过(英_右小括号);
            } else
            {
                n = 最大对齐;
            }
            属性->a.aligned = 精确_2幂加1(n);
	    if (n != 1 << (属性->a.aligned - 1))
	      错_误(" %d 的对齐大于已实现的对齐", n);
            break;
        case TOK_PACKED1:
        case TOK_PACKED2:
            属性->a.packed = 1;
            break;
        case TOK_WEAK1:
        case TOK_WEAK2:
            属性->a.weak = 1;
            break;
        case TOK_UNUSED1:
        case TOK_UNUSED2:
            /* 目前，无需处理它，因为zhi不跟踪未使用的对象 */
            break;
        case TOK_NORETURN1:
        case TOK_NORETURN2:
            属性->f.函数_不返回 = 1;
            break;
        case TOK_CDECL1:
        case TOK_CDECL2:
        case TOK_CDECL3:
            属性->f.函数调用约定 = FUNC_CDECL;
            break;
        case TOK_STDCALL1:
        case TOK_STDCALL2:
        case TOK_STDCALL3:
            属性->f.函数调用约定 = FUNC_STDCALL;
            break;
#ifdef 目标_I386
        case TOK_REGPARM1:
        case TOK_REGPARM2:
            跳过(英_左小括号);
            n = 常量表达式解析();
            if (n > 3) 
                n = 3;
            else if (n < 0)
                n = 0;
            if (n > 0)
                属性->f.函数调用约定 = FUNC_FASTCALL1 + n - 1;
            跳过(英_右小括号);
            break;
        case TOK_FASTCALL1:
        case TOK_FASTCALL2:
        case TOK_FASTCALL3:
            属性->f.函数调用约定 = FUNC_FASTCALLW;
            break;            
#endif
        case TOK_MODE:
            跳过(英_左小括号);
            switch(单词编码) {
                case TOK_MODE_DI:
                    属性->属性_模式 = 数据类型_长长整数 + 1;
                    break;
                case TOK_MODE_QI:
                    属性->属性_模式 = 数据类型_字节 + 1;
                    break;
                case TOK_MODE_HI:
                    属性->属性_模式 = 数据类型_短整数 + 1;
                    break;
                case TOK_MODE_SI:
                case TOK_MODE_word:
                    属性->属性_模式 = 数据类型_整数 + 1;
                    break;
                default:
                    zhi_警告("__mode__(%s) 不支持\n", 取单词字符串(单词编码, NULL));
                    break;
            }
            取下个单词();
            跳过(英_右小括号);
            break;
        case TOK_DLLEXPORT:
            属性->a.dllexport = 1;
            break;
        case TOK_NODECORATE:
            属性->a.节点 = 1;
            break;
        case TOK_DLLIMPORT:
            属性->a.dllimport = 1;
            break;
        default:
            zhi_警告_c(警告_不支持)("'%s' 属性被忽略", 取单词字符串(t, NULL));
            /* 跳过参数 */
            if (单词编码 == 英_左小括号) {
                int parenthesis = 0;
                do {
                    if (单词编码 == 英_左小括号) 
                        parenthesis++;
                    else if (单词编码 == 英_右小括号) 
                        parenthesis--;
                    取下个单词();
                } while (parenthesis && 单词编码 != -1);
            }
            break;
        }
        if (单词编码 != 英_逗号)
            break;
        取下个单词();
    }
    跳过(英_右小括号);
    跳过(英_右小括号);
    goto 重做;
}

static 符号ST * 查找_字段 (类型ST *类型, int v, int *cumofs)
{
    符号ST *s = 类型->引用符号;
    v |= 符号_字段;
    while ((s = s->next) != NULL) {
	if ((s->v & 符号_字段) &&
	    (s->类型.数据类型 & 数据类型_基本类型) == 数据类型_结构体 &&
	    (s->v & ~符号_字段) >= 第一个匿名符号) {
	    符号ST *ret = 查找_字段 (&s->类型, v, cumofs);
	    if (ret) {
                *cumofs += s->c;
	        return ret;
            }
	}
	if (s->v == v)
	  break;
    }
    return s;
}

static void 检查_字段 (类型ST *类型, int check)
{
    符号ST *s = 类型->引用符号;

    while ((s = s->next) != NULL) {
        int v = s->v & ~符号_字段;
        if (v < 第一个匿名符号) {
            单词ST *ts = 标识符表[v - TOK_标识符];
            if (check && (ts->单词编码 & 符号_字段))
                错_误("重复成员 '%s'", 取单词字符串(v, NULL));
            ts->单词编码 ^= 符号_字段;
        } else if ((s->类型.数据类型 & 数据类型_基本类型) == 数据类型_结构体)
            检查_字段 (&s->类型, check);
    }
}

static void 结构体布局(类型ST *类型, GNUC属性ST *ad)
{
    int 大小, 对齐, maxalign, offset, c, bit_pos, 位大小;
    int packed, a, bt, prevbt, prev_位大小;
    int pcc = !全局虚拟机->模拟MS算法对齐位域;
    int pragma_pack = *全局虚拟机->包_栈_ptr;
    符号ST *f;

    maxalign = 1;
    offset = 0;
    c = 0;
    bit_pos = 0;
    prevbt = 数据类型_结构体; /* make it never match */
    prev_位大小 = 0;

//#define BF_DEBUG

    for (f = 类型->引用符号->next; f; f = f->next) {
        if (f->类型.数据类型 & 存储类型_位域)
            位大小 = BIT_SIZE(f->类型.数据类型);
        else
            位大小 = -1;
        大小 = 类型_大小(&f->类型, &对齐);
        a = f->a.aligned ? 1 << (f->a.aligned - 1) : 0;
        packed = 0;

        if (pcc && 位大小 == 0) {
            /* in pcc mode, packing does not affect zero-width bitfields */

        } else {
            /* in pcc mode, attribute packed overrides if set. */
            if (pcc && (f->a.packed || ad->a.packed))
                对齐 = packed = 1;

            /* pragma pack overrides 对齐 if lesser and packs bitfields always */
            if (pragma_pack) {
                packed = 1;
                if (pragma_pack < 对齐)
                    对齐 = pragma_pack;
                /* in pcc mode pragma pack also overrides individual 对齐 */
                if (pcc && pragma_pack < a)
                    a = 0;
            }
        }
        /* some individual 对齐 was specified */
        if (a)
            对齐 = a;

        if (类型->引用符号->类型.数据类型 == 值的数据类型_共用体) {
	    if (pcc && 位大小 >= 0)
	        大小 = (位大小 + 7) >> 3;
	    offset = 0;
	    if (大小 > c)
	        c = 大小;

	} else if (位大小 < 0) {
            if (pcc)
                c += (bit_pos + 7) >> 3;
	    c = (c + 对齐 - 1) & -对齐;
	    offset = c;
	    if (大小 > 0)
	        c += 大小;
	    bit_pos = 0;
	    prevbt = 数据类型_结构体;
	    prev_位大小 = 0;

	} else {
	    /* A bit-field.  Layout is more complicated.  There are two
	       options: PCC (GCC) compatible and MS compatible */
            if (pcc) {
		/* In PCC layout a bit-field is placed adjacent to the
                   preceding bit-fields, except if:
                   - it has zero-width
                   - an individual alignment was given
                   - it would overflow its base 类型 container and
                     there is no packing */
                if (位大小 == 0) {
            new_field:
		    c = (c + ((bit_pos + 7) >> 3) + 对齐 - 1) & -对齐;
		    bit_pos = 0;
                } else if (f->a.aligned) {
                    goto new_field;
                } else if (!packed) {
                    int a8 = 对齐 * 8;
	            int ofs = ((c * 8 + bit_pos) % a8 + 位大小 + a8 - 1) / a8;
                    if (ofs > 大小 / 对齐)
                        goto new_field;
                }

                /* in pcc mode, long long bitfields have 类型 int if they fit */
                if (大小 == 8 && 位大小 <= 32)
                    f->类型.数据类型 = (f->类型.数据类型 & ~数据类型_基本类型) | 数据类型_整数, 大小 = 4;

                while (bit_pos >= 对齐 * 8)
                    c += 对齐, bit_pos -= 对齐 * 8;
                offset = c;

		/* In PCC layout named bit-fields influence the alignment
		   of the containing struct using the base types alignment,
		   except for packed fields (which here have correct 对齐).  */
		if (f->v & 第一个匿名符号
                    // && 位大小 // ??? gcc on ARM/rpi does that
                    )
		    对齐 = 1;

	    } else {
		bt = f->类型.数据类型 & 数据类型_基本类型;
		if ((bit_pos + 位大小 > 大小 * 8)
                    || (位大小 > 0) == (bt != prevbt)
                    ) {
		    c = (c + 对齐 - 1) & -对齐;
		    offset = c;
		    bit_pos = 0;
		    /* In MS bitfield mode a bit-field run always uses
		       at least as many bits as the underlying 类型.
		       To start a new run it's also required that this
		       or the last bit-field had non-zero width.  */
		    if (位大小 || prev_位大小)
		        c += 大小;
		}
		/* In MS layout the records alignment is normally
		   influenced by the field, except for a zero-width
		   field at the start of a run (but by further zero-width
		   fields it is again).  */
		if (位大小 == 0 && prevbt != bt)
		    对齐 = 1;
		prevbt = bt;
                prev_位大小 = 位大小;
	    }

	    f->类型.数据类型 = (f->类型.数据类型 & ~(0x3f << 值的数据类型_结构体_SHIFT))
		        | (bit_pos << 值的数据类型_结构体_SHIFT);
	    bit_pos += 位大小;
	}
	if (对齐 > maxalign)
	    maxalign = 对齐;

#ifdef BF_DEBUG
	printf("set field %s offset %-2d 大小 %-2d 对齐 %-2d",
	       取单词字符串(f->v & ~符号_字段, NULL), offset, 大小, 对齐);
	if (f->类型.数据类型 & 存储类型_位域) {
	    printf(" pos %-2d bits %-2d",
                    BIT_POS(f->类型.数据类型),
                    BIT_SIZE(f->类型.数据类型)
                    );
	}
	printf("\n");
#endif

        f->c = offset;
	f->寄存qi = 0;
    }

    if (pcc)
        c += (bit_pos + 7) >> 3;

    /* store 大小 and alignment */
    a = bt = ad->a.aligned ? 1 << (ad->a.aligned - 1) : 1;
    if (a < maxalign)
        a = maxalign;
    类型->引用符号->寄存qi = a;
    if (pragma_pack && pragma_pack < maxalign && 0 == pcc) {
        /* can happen if individual 对齐 for some member was given.  In
           this case MSVC ignores maxalign when aligning the 大小 */
        a = pragma_pack;
        if (a < bt)
            a = bt;
    }
    c = (c + a - 1) & -a;
    类型->引用符号->c = c;

#ifdef BF_DEBUG
    printf("struct 大小 %-2d 对齐 %-2d\n\n", c, a), fflush(stdout);
#endif

    /* check whether we can access bitfields by their 类型 */
    for (f = 类型->引用符号->next; f; f = f->next) {
        int s, px, cx, c0;
        类型ST t;

        if (0 == (f->类型.数据类型 & 存储类型_位域))
            continue;
        f->类型.引用符号 = f;
        f->auxtype = -1;
        位大小 = BIT_SIZE(f->类型.数据类型);
        if (位大小 == 0)
            continue;
        bit_pos = BIT_POS(f->类型.数据类型);
        大小 = 类型_大小(&f->类型, &对齐);

        if (bit_pos + 位大小 <= 大小 * 8 && f->c + 大小 <= c
#ifdef 目标_ARM
            && !(f->c & (对齐 - 1))
#endif
            )
            continue;

        /* try to access the field using a different 类型 */
        c0 = -1, s = 对齐 = 1;
        t.数据类型 = 数据类型_字节;
        for (;;) {
            px = f->c * 8 + bit_pos;
            cx = (px >> 3) & -对齐;
            px = px - (cx << 3);
            if (c0 == cx)
                break;
            s = (px + 位大小 + 7) >> 3;
            if (s > 4) {
                t.数据类型 = 数据类型_长长整数;
            } else if (s > 2) {
                t.数据类型 = 数据类型_整数;
            } else if (s > 1) {
                t.数据类型 = 数据类型_短整数;
            } else {
                t.数据类型 = 数据类型_字节;
            }
            s = 类型_大小(&t, &对齐);
            c0 = cx;
        }

        if (px + 位大小 <= s * 8 && cx + s <= c
#ifdef 目标_ARM
            && !(cx & (对齐 - 1))
#endif
            ) {
            /* update offset and bit position */
            f->c = cx;
            bit_pos = px;
	    f->类型.数据类型 = (f->类型.数据类型 & ~(0x3f << 值的数据类型_结构体_SHIFT))
		        | (bit_pos << 值的数据类型_结构体_SHIFT);
            if (s != 大小)
                f->auxtype = t.数据类型;
#ifdef BF_DEBUG
            printf("FIX field %s offset %-2d 大小 %-2d 对齐 %-2d "
                "pos %-2d bits %-2d\n",
                取单词字符串(f->v & ~符号_字段, NULL),
                cx, s, 对齐, px, 位大小);
#endif
        } else {
            /* fall back to load/store single-byte wise */
            f->auxtype = 数据类型_结构体;
#ifdef BF_DEBUG
            printf("FIX field %s : load byte-wise\n",
                 取单词字符串(f->v & ~符号_字段, NULL));
#endif
        }
    }
}

/* 枚举/结构体/共用体 的外部声明。 u 是： 值的数据类型_枚举/数据类型_结构体/值的数据类型_共用体 */
static void 结构体声明(类型ST *类型, int 值的类型)
{
    int v, c, 大小, 对齐, 柔性的;
    int 位大小, bsize, bt;
    符号ST *s, *ss, **ps;
    GNUC属性ST ad, ad1;
    类型ST 类型1, 基本类型;
    memset(&ad, 0, sizeof ad);
    取下个单词();    /*1.此时获取 结构体的名称*/
    解析属性(&ad);
    if (单词编码 != 英_左大括号)
    {
        v = 单词编码; /*2此时V是结构体名称*/
        取下个单词();/* 2.此时取结构体名称后面的 */
        /* 结构已经定义？ 把它返还 */
        if (v < TOK_标识符)
        {
        	应为("（结构体/共用体/枚举）名称");
        }

        s = 结构体符号_查找(v);
        if (s && (s->作用域范围 == 局部_范围 || 单词编码 != 英_左大括号))
        {
            if (值的类型 == s->类型.数据类型)
            {
            	goto 执行_声明;
            }
            if (值的类型 == 值的数据类型_枚举 && 是_枚举(s->类型.数据类型))
            {
            	goto 执行_声明;
            }
            错_误(" '%s'的重复定义", 取单词字符串(v, NULL));
        }
    } else
    {
        v = 匿名符号索引++;
    }
    /* 记录原始 enum/struct/union 标记。 */
    类型1.数据类型 = 值的类型 == 值的数据类型_枚举 ? 值的类型 | 数据类型_整数 | 存储类型_无符号的 : 值的类型;
    类型1.引用符号 = NULL;
    s = 将符号放入符号栈(v | 符号_结构体, &类型1, 0, -1);/*此时的V是结构体名称*/
    s->寄存qi = 0; /* 默认对齐为零作为 gcc */
执行_声明:
    类型->数据类型 = s->类型.数据类型;
    类型->引用符号 = s;

    if (单词编码 == 英_左大括号)/*解析结构体成员列表-开始*/
    {
        取下个单词();/*3.此时取 左大括号 后面的单词*/
        if (s->c != -1)
        {
        	错_误("（结构体/共用体/枚举）已经定义");
        }
        s->c = -2;
        /* 不允许空枚举 */
        ps = &s->next;
        if (值的类型 == 值的数据类型_枚举)
        {
            long long ll = 0, pl = 0, nl = 0;
	        类型ST t;
            t.引用符号 = s;
            /* 枚举符号具有静态存储 */
            t.数据类型 = 数据类型_整数|存储类型_静态|值的数据类型_枚举_VAL;
            for(;;)
            {
                v = 单词编码;
                if (v < TOK_UIDENT)
                {
                	应为("标识符");
                }
                ss = 查找标识符(v);
                if (ss && !局部符号栈)
                {
                	错_误("枚举'%s'的重复定义",取单词字符串(v, NULL));
                }
                取下个单词();
                if (单词编码 == '=')
                {
                    取下个单词();
		            ll = 常量表达式64();
                }
                ss = 将符号放入符号栈(v, &t, 存储类型_VC常量, 0);
                ss->enum_val = ll;
                *ps = ss, ps = &ss->next;
                if (ll < nl)
                    nl = ll;
                if (ll > pl)
                    pl = ll;
                if (单词编码 != 英_逗号)
                    break;
                取下个单词();
                ll++;
                /* 注意：我们接受尾随逗号 */
                if (单词编码 == 英_右大括号)
                    break;
            }
            跳过(英_右大括号);
            /* 设置枚举的整数类型 */
            t.数据类型 = 数据类型_整数;
            if (nl >= 0)
            {
                if (pl != (unsigned)pl)
                    t.数据类型 = (长整数大小==8 ? 数据类型_长长整数|数据类型_长整数 : 数据类型_长长整数);
                t.数据类型 |= 存储类型_无符号的;
            } else if (pl != (int)pl || nl != (int)nl)
                t.数据类型 = (长整数大小==8 ? 数据类型_长长整数|数据类型_长整数 : 数据类型_长长整数);
            s->类型.数据类型 = 类型->数据类型 = t.数据类型 | 值的数据类型_枚举;
            s->c = 0;
            /* 为枚举成员设置类型 */
            for (ss = s->next; ss; ss = ss->next)
            {
                ll = ss->enum_val;
                if (ll == (int)ll) /* 如果合适，默认为 int */
                    continue;
                if (t.数据类型 & 存储类型_无符号的)
                {
                    ss->类型.数据类型 |= 存储类型_无符号的;
                    if (ll == (unsigned)ll)
                        continue;
                }
                ss->类型.数据类型 = (ss->类型.数据类型 & ~数据类型_基本类型)
                    | (长整数大小==8 ? 数据类型_长长整数|数据类型_长整数 : 数据类型_长长整数);
            }
        } else
        {
            c = 0;
            柔性的 = 0;
            while (单词编码 != 英_右大括号)
            {
                if (!解析声明说明符(&基本类型, &ad1))
                {
					跳过(英_分号);
					continue;
				}
                while (1)
                {
					if (柔性的)
					{
						错_误("动态数组成员 '%s' 不在结构体的末尾",取单词字符串(v, NULL));
					}
                    位大小 = -1;
                    v = 0;
                    类型1 = 基本类型;
                    if (单词编码 != 英_冒号)
                    {
						if (单词编码 != 英_分号)
						{
							解析初始声明符列表(&类型1, &ad1, &v, 声明符_直接);
						}
                        if (v == 0)
                        {
                    	    if ((类型1.数据类型 & 数据类型_基本类型) != 数据类型_结构体)
                    	    {
                    	    	应为("标识符");
                    	    }else
                    	    {
								int v = 基本类型.引用符号->v;
								if (!(v & 符号_字段) && (v & ~符号_结构体) < 第一个匿名符号)
								{
									if (全局虚拟机->ms_扩展 == 0)
									{
										应为("标识符");
									}
								}
                    	    }
                        }
                        if (类型_大小(&类型1, &对齐) < 0)
                        {
							if ((值的类型 == 数据类型_结构体) && (类型1.数据类型 & 数据类型_数组) && c)
							{
								柔性的 = 1;
							}else
							{
								错_误("字段 '%s' 的类型不完整",取单词字符串(v, NULL));
							}
                        }
                        if ((类型1.数据类型 & 数据类型_基本类型) == 数据类型_函数 || (类型1.数据类型 & 数据类型_基本类型) == 数据类型_无类型 || (类型1.数据类型 & 值的数据类型_贮存))
                        {
                        	错_误(" '%s'的无效类型",取单词字符串(v, NULL));
                        }
                    }
                    /* 位域结构体:其成员是按照位分配的。为了节省存储空间，按照位存放数据（而不是按照字节存放），一般在单片机中使用。
                     * 结构体 结构体名称
                     * {
                     *     int a:4;
                     *     int :0;  /空域/
                     *     int b:4; /从下一个字节开始存放/
                     *     int c:2;
                     *     int d:3;
                     *     int :3;  /该3位不能使用/
                     *     int e:3;
                     * }
                     * 在这个位域定义中，a占第一字节的前4位，后4位填0表示不使用，b从第二字节开始，占用4位，c占用2位，剩余的位不够，d从第三个字节开始占用3位,下面有3个位是不能使用的，e从第四个字节开始占用3位。
                     * */
                    if (单词编码 == 英_冒号)
                    {
                        取下个单词();
                        位大小 = 常量表达式解析();
                        /* XXX: 处理消息 v = 0 的情况 */
                        if (位大小 < 0)
                        {
                        	错_误("位域 '%s'中的负宽度",取单词字符串(v, NULL));
                        }
                        if (v && 位大小 == 0)
                        {
                        	错_误("位域 '%s'的零宽度",取单词字符串(v, NULL));
                        }
						解析属性(&ad1);
                    }
                    大小 = 类型_大小(&类型1, &对齐);
                    if (位大小 >= 0)
                    {
                        bt = 类型1.数据类型 & 数据类型_基本类型;
                        if (bt != 数据类型_整数 && bt != 数据类型_字节 && bt != 数据类型_短整数 && bt != 数据类型_布尔 && bt != 数据类型_长长整数)
                        {
                        	错_误("位域必须具有标量类型");
                        }
                        bsize = 大小 * 8;
                        if (位大小 > bsize)
                        {
                            错_误(" '%s'的宽度超出其类型",取单词字符串(v, NULL));
                        } else if (位大小 == bsize && !ad.a.packed && !ad1.a.packed)
                        {
                            /* 不需要位域 */
                            ;
                        } else if (位大小 == 64)
                        {
                            错_误("字段宽度 64 未实现");
                        } else
                        {
                            类型1.数据类型 = (类型1.数据类型 & ~值的数据类型_结构体_MASK) | 存储类型_位域 | (位大小 << (值的数据类型_结构体_SHIFT + 6));
                        }
                    }
                    if (v != 0 || (类型1.数据类型 & 数据类型_基本类型) == 数据类型_结构体)
                    {
                        /* 请记住，我们已经看到了一个真实的字段来检查灵活数组成员的位置。*/
						c = 1;
                    }
					/* 如果成员是结构体或位域，则强制放入结构体（匿名）。  */
                    if (v == 0 && ((类型1.数据类型 & 数据类型_基本类型) == 数据类型_结构体 || 位大小 >= 0))
                    {
						v = 匿名符号索引++;
					}
                    if (v)
                    {
                        ss = 将符号放入符号栈(v | 符号_字段, &类型1, 0, 0);
                        ss->a = ad1.a;
                        *ps = ss;
                        ps = &ss->next;
                    }
                    if (单词编码 == 英_分号 || 单词编码 == TOK_文件结尾)
                        break;
                    跳过(英_逗号);
                }
                跳过(英_分号);
            }
            跳过(英_右大括号);
			解析属性(&ad);
            if (ad.清理_函数)
            {
                zhi_警告("属性 '__cleanup__' 在类型上被忽略");
            }
			检查_字段(类型, 1);
			检查_字段(类型, 0);
            结构体布局(类型, &ad);
        }
    }/*解析结构体成员-结束*/
}

static void 符号_合并属性(GNUC属性ST *ad, 符号ST *s)
{
    合并_符号属性(&ad->a, &s->a);
    合并_函数属性(&ad->f, &s->f);
}

/* 给类型添加类型限定符。 如果类型是数组，则在元素类型中添加限定符，复制，因为它可能是一个typedef。 */
static void 解析限定符(类型ST *类型, int 限定符)
{
    while (类型->数据类型 & 数据类型_数组)
    {
        类型->引用符号 = 将符号放入符号栈(符号_字段, &类型->引用符号->类型, 0, 类型->引用符号->c);
        类型 = &类型->引用符号->类型;
    }
    类型->数据类型 |= 限定符;   /*先按位或然后赋值：类型->数据类型 = 0x0103*/
}

/* 返回基本类型(类型->数据类型)并跳过它。如果没有类型声明，则返回0
 * 类型：数据类型
 *声明说明符分为5类：1.存储类说明符；2.类型说明符；3.类型限定符；4.函数说明符；5.属性
 */
static int 解析声明说明符(类型ST *类型, GNUC属性ST *属性)
{
    int 数据类型, 值类型, 基本类型, 符号类型, 找到_类型, 找到_类型规范, 修饰符类型, n;
    符号ST *s;
    类型ST 类型1;

    memset(属性, 0, sizeof(GNUC属性ST));
    找到_类型 = 0;
    找到_类型规范 = 0;
    数据类型 = 数据类型_整数;
    基本类型 = 符号类型 = -1;
    类型->引用符号 = NULL;

    while(1)
    {
        switch(单词编码)
        {
        case 英_扩展:/* 忽略了扩展 */
            取下个单词();
            continue;
        case 英_字符:
        case 中_字符:
            值类型 = 数据类型_字节;
        基本_类型:
            取下个单词();  /*此时 单词编码=数据类型后面的 标识符*/
        基本_类型1:
            if (值类型 == 数据类型_短整数 || 值类型 == 数据类型_长整数)
            {
                if (符号类型 != -1 || (基本类型 != -1 && 基本类型 != 数据类型_整数))
                {
                	太多类型: 错_误("此处有太多的基本数据类型（如：无、字符、整数、短整数、布尔、双精度等不要重复使用。），请合理的删减不必要的数据类型。");
                }
                符号类型 = 值类型;
            } else
            {
                if (基本类型 != -1 || (符号类型 != -1 && 值类型 != 数据类型_整数))
                {
                	goto 太多类型;
                }
                基本类型 = 值类型;
            }
            if (值类型 != 数据类型_整数)
            {
            	数据类型 = (数据类型 & ~(数据类型_基本类型|数据类型_长整数)) | 值类型;
            }
            找到_类型规范 = 1;
            break;
        case 英_无:
        case 中_无:
            值类型 = 数据类型_无类型;
            goto 基本_类型;
        case 英_短整数:
        case 中_短整数:
            值类型 = 数据类型_短整数;
            goto 基本_类型;
        case 英_整数:
        case 中_整数:
            值类型 = 数据类型_整数;
            goto 基本_类型;
        case 英_对齐:
            {
            	  int n;
				  GNUC属性ST ad1;
				  取下个单词();
				  跳过(英_左小括号);
				  memset(&ad1, 0, sizeof(GNUC属性ST));
				  if (解析声明说明符(&类型1, &ad1))
				  {
					  解析初始声明符列表(&类型1, &ad1, &n, 声明符_抽象);
					  if (ad1.a.aligned)
					  {
						  n = 1 << (ad1.a.aligned - 1);
					  }else
					  {
						  类型_大小(&类型1, &n);
					  }
				  } else
				  {
					  n = 常量表达式解析();
					  if (n <= 0 || (n & (n - 1)) != 0)
					  {
						  错_误("对齐必须是 2 的正幂");
					  }
				  }
				  跳过(英_右小括号);
				  属性->a.aligned = 精确_2幂加1(n);
            }
            continue;
        case 英_长整数:
        case 中_长整数:
            if ((数据类型 & 数据类型_基本类型) == 数据类型_双精度)
            {
                数据类型 = (数据类型 & ~(数据类型_基本类型|数据类型_长整数)) | 数据类型_长双精度;
            } else if ((数据类型 & (数据类型_基本类型|数据类型_长整数)) == 数据类型_长整数)
            {
                数据类型 = (数据类型 & ~(数据类型_基本类型|数据类型_长整数)) | 数据类型_长长整数;
            } else
            {
                值类型 = 数据类型_长整数;
                goto 基本_类型;
            }
            取下个单词();
            break;
        case 英_布尔:
        case 中_布尔:
            值类型 = 数据类型_布尔;
            goto 基本_类型;
        case 英_浮点:
        case 中_浮点:
            值类型 = 数据类型_浮点;
            goto 基本_类型;
        case 英_双精度:
        case 中_双精度:
            if ((数据类型 & (数据类型_基本类型|数据类型_长整数)) == 数据类型_长整数)
            {
                数据类型 = (数据类型 & ~(数据类型_基本类型|数据类型_长整数)) | 数据类型_长双精度;
            } else
            {
                值类型 = 数据类型_双精度;
                goto 基本_类型;
            }
            取下个单词();
            break;
        case 英_枚举:
        case 中_枚举:
            结构体声明(&类型1, 值的数据类型_枚举);
        基本_类型2:
            值类型 = 类型1.数据类型;
            类型->引用符号 = 类型1.引用符号;
            goto 基本_类型1;
        case 英_结构体:
        case 中_结构体:
            结构体声明(&类型1, 数据类型_结构体);
            goto 基本_类型2;
        case 英_共用体:
        case 中_共用体:
            结构体声明(&类型1, 值的数据类型_共用体);
            goto 基本_类型2;


        case 英__原子:/* 类型修饰符 */
            取下个单词();
            类型->数据类型 = 数据类型;
            解析限定符(类型, 值的数据类型_原子);
            数据类型 = 类型->数据类型;
            if (单词编码 == 英_左小括号)
            {
                解析_表达式_类型(&类型1);
                /* 删除除typedef之外的所有的贮存修饰符 */
                类型1.数据类型 &= ~(值的数据类型_贮存&~存储类型_别名);
                if (类型1.引用符号)
                {
                	符号_合并属性(属性, 类型1.引用符号);
                }
                goto 基本_类型2;
            }
            break;
        case 英_常量:
        case 中_常量:
            类型->数据类型 = 数据类型;           /*类型->数据类型 = 数据类型_整数*/
            解析限定符(类型, 存储类型_常量);
            数据类型 = 类型->数据类型; /*数据类型 = 存储类型_常量 | 数据类型_整数*/
            取下个单词();  /*此时单词编码 = int 。*/
            break; /*跳出本次switch(),循环进入下一次*/
        case 英_易变:
        case 中_易变:
            类型->数据类型 = 数据类型;
            解析限定符(类型, 存储类型_易变);
            数据类型 = 类型->数据类型;
            取下个单词();
            break;
        case 英_有符号:
        case 中_有符号:
            if ((数据类型 & (存储类型_明确有无符号|存储类型_无符号的)) == (存储类型_明确有无符号|存储类型_无符号的))
            {
            	错_误("有符号和无符号修饰符");
            }

            数据类型 |= 存储类型_明确有无符号;
            取下个单词();
            找到_类型规范 = 1;
            break;
        case 英_寄存器:
        case 中_寄存器:
        case 英_自动:
        case 中_自动:
        case 英_限制:
        case 中_限制:
        case TOK_RESTRICT2:
        case TOK_RESTRICT3:
            取下个单词();
            break;
        case 英_无符号:
        case 中_无符号:
            if ((数据类型 & (存储类型_明确有无符号|存储类型_无符号的)) == 存储类型_明确有无符号)
            {
            	错_误("有符号和无符号修饰符");
            }
            数据类型 |= 存储类型_明确有无符号 | 存储类型_无符号的;
            取下个单词();
            找到_类型规范 = 1;
            break;

            /* 存储类说明符 */
        case 英_外部:
        case 中_外部:
            修饰符类型 = 存储类型_外部;
            goto 贮存;
        case 英_静态:
        case 中_静态:
            修饰符类型 = 存储类型_静态;
            goto 贮存;
        case 英_类型定义:
        case 中_类型定义:
            修饰符类型 = 存储类型_别名;
            goto 贮存;
       贮存:
            if (数据类型 & (存储类型_外部|存储类型_静态|存储类型_别名) & ~修饰符类型)
            {
            	错_误("有太多个存储类型");
            }
            数据类型 |= 修饰符类型;
            取下个单词();
            break;
        case 英_内联:
        case 中_内联:
        case TOK_INLINE2:
        case TOK_INLINE3:
            数据类型 |= 存储类型_内联;
            取下个单词();
            break;
        case TOK_NORETURN3:
            取下个单词();
            属性->f.函数_不返回 = 1;
            break;
            /* GNUC属性 */
        case TOK_ATTRIBUTE1:
        case TOK_ATTRIBUTE2:
            解析属性(属性);
            if (属性->属性_模式)
            {
                值类型 = 属性->属性_模式 -1;
                数据类型 = (数据类型 & ~(数据类型_基本类型|数据类型_长整数)) | 值类型;
            }
            continue;
            /* GNUC typeof */
        case 英_取类型:
        case 中_取类型:
            取下个单词();
            解析_表达式_类型(&类型1);
            类型1.数据类型 &= ~(值的数据类型_贮存&~存储类型_别名);/* 删除除typedef之外的所有贮存修饰符 */
	        if (类型1.引用符号)
	        {
	    	    符号_合并属性(属性, 类型1.引用符号);
	        }
            goto 基本_类型2;
        default:
            if (找到_类型规范)
            {
            	goto 结_束;
            }
            s = 查找标识符(单词编码);
            if (!s || !(s->类型.数据类型 & 存储类型_别名))
            {
            	goto 结_束;
            }
            n = 单词编码, 取下个单词();
            if (单词编码 == 英_冒号 && !in_通用的)
            {
                /* 如果是标签则忽略 */
                退回当前单词(n);
                goto 结_束;
            }
            数据类型 &= ~(数据类型_基本类型|数据类型_长整数);
            值类型 = 数据类型 & ~(存储类型_常量 | 存储类型_易变), 数据类型 ^= 值类型;
            类型->数据类型 = (s->类型.数据类型 & ~存储类型_别名) | 值类型;
            类型->引用符号 = s->类型.引用符号;
            if (数据类型)
            {
            	解析限定符(类型, 数据类型);
            }
            数据类型 = 类型->数据类型;
            /* 从 typedef 获取属性 */
            符号_合并属性(属性, s);
            找到_类型规范 = 1;
            符号类型 = 基本类型 = -2;
            break;
        }
        找到_类型 = 1;
    }
结_束:
    if (全局虚拟机->字符_是_无符号的)
    {
        if ((数据类型 & (存储类型_明确有无符号|数据类型_基本类型)) == 数据类型_字节)
        {
        	数据类型 |= 存储类型_无符号的;
        }
    }
    /*   数据类型_长整数 仅用作 数据类型_整数 / 数据类型_长长整数 的修饰符*/
    基本类型 = 数据类型 & (数据类型_基本类型|数据类型_长整数);
    if (基本类型 == 数据类型_长整数)
    {
    	数据类型 |= 长整数大小 == 8 ? 数据类型_长长整数 : 数据类型_整数;
    }

#ifdef 为长双精度使用双精度M
    if (基本类型 == 数据类型_长双精度)
    {
    	数据类型 = (数据类型 & ~(数据类型_基本类型|数据类型_长整数)) | (数据类型_双精度|数据类型_长整数);
    }
#endif
    类型->数据类型 = 数据类型;
    return 找到_类型;
}

/* 转换函数参数类型（数组到指针和函数到函数指针） */
static inline void 转换参数类型(类型ST *pt)
{
    /* 去掉const和volatile限定符（XXX：const可以用来表示一个const函数参数*/
    pt->数据类型 &= ~(存储类型_常量 | 存储类型_易变);
    /* 数组必须根据 ANSI C 转换为指针 */
    pt->数据类型 &= ~数据类型_数组;
    if ((pt->数据类型 & 数据类型_基本类型) == 数据类型_函数) {
        生成指针类型(pt);
    }
}

静态函数 void 解析_汇编_字符串(动态字符串ST *astr)
{
    跳过(英_左小括号);
    解析多个字符串(astr, "字符串常量");
}

/* 解析一个 asm 标签并返回单词 */
static int asm_label_instr(void)
{
    int v;
    动态字符串ST astr;
    取下个单词();
    解析_汇编_字符串(&astr);
    跳过(英_右小括号);
#ifdef ASM_DEBUG
    printf("汇编_别名: \"%s\"\n", (char *)astr.数据);
#endif
    v = 插入单词(astr.数据, astr.大小 - 1)->单词编码;
    动态字符串_释放(&astr);
    return v;
}

static int 解析声明符后缀(类型ST *类型, GNUC属性ST *ad, int 贮存, int td)
{
    int n, l, t1, arg_size, 对齐, unused_align;
    符号ST **plast, *s, *first;
    GNUC属性ST ad1;
    类型ST pt;

    if (单词编码 == 英_左小括号)
    {
        /* 函数类型或递归声明符（如果是，则返回） */
        取下个单词();
		if (td && !(td & 声明符_抽象))
		{
			return 0;
		}
		if (单词编码 == 英_右小括号)
		{
			l = 0;
		}else if (解析声明说明符(&pt, &ad1))
		{
			l = 函数原型_新;
		}else if (td)
		{
			合并_属性 (ad, &ad1);
			return 0;
		} else
		{
			l = 函数原型_旧;
		}

        first = NULL;
        plast = &first;
        arg_size = 0;
        if (l)
        {
            for(;;)
            {
                /* 读取参数名称并计算偏移量 */
                if (l != 函数原型_旧)
                {
                    if ((pt.数据类型 & 数据类型_基本类型) == 数据类型_无类型 && 单词编码 == 英_右小括号)
                    {
                    	break;
                    }
                    解析初始声明符列表(&pt, &ad1, &n, 声明符_直接 | 声明符_抽象);
                    if ((pt.数据类型 & 数据类型_基本类型) == 数据类型_无类型)
                    {
                    	错_误("参数声明为'无'");
                    }
                } else
                {
                    n = 单词编码;
                    if (n < TOK_UIDENT)
                    {
                    	应为("标识符");
                    }
                    pt.数据类型 = 数据类型_无类型; /* 无效类型 */
                    pt.引用符号 = NULL;
                    取下个单词();
                }
                转换参数类型(&pt);
                arg_size += (类型_大小(&pt, &对齐) + 宏_指针大小 - 1) / 宏_指针大小;
                s = 将符号放入符号栈(n | 符号_字段, &pt, 0, 0);
                *plast = s;
                plast = &s->next;
                if (单词编码 == 英_右小括号)
                {
                	break;
                }
                跳过(英_逗号);
                if (l == 函数原型_新 && 单词编码 == TOK_三个点)
                {
                    l = 函数原型_省略;
                    取下个单词();
                    break;
                }
				if (l == 函数原型_新 && !解析声明说明符(&pt, &ad1))
				{
					错_误("无效类型");
				}
            }
        } else
        {
        	/* 如果没有参数，则旧类型原型 */
        	l = 函数原型_旧;
        }
        跳过(英_右小括号);
        /* 注意：const 在返回类型中被忽略，因为它在 gcc/C++ 中具有特殊含义 */
        类型->数据类型 &= ~存储类型_常量; 
        /* 一些古老的前 K&R C 允许函数返回一个数组，并将数组括号放在参数之后，这样“int c()[]”的意思类似于“int[] c()” */
        if (单词编码 == 英_左中括号)
        {
            取下个单词();
            跳过(英_右中括号); /* 只处理简单的“[]” */
            生成指针类型(类型);
        }
        /* 我们推送一个包含函数原型的匿名符号 */
        ad->f.函数_参数 = arg_size;
        ad->f.函数_类型= l;
        s = 将符号放入符号栈(符号_字段, 类型, 0, 0);
        s->a = ad->a;
        s->f = ad->f;
        s->next = first;
        类型->数据类型 = 数据类型_函数;
        类型->引用符号 = s;
    } else if (单词编码 == 英_左中括号)
    {
		int 保存_无需代码_生成 = 无需生成代码;
        /* 数组定义 */
        取下个单词();
		while (1)
		{
			/* XXX 可选的 类型-quals 和 static 应该只在参数声明中被接受。 '*' 也是如此，然后甚至仅在原型中（不是函数 defs）。 */
			switch (单词编码)
			{
			case 英_限制: case 中_限制: case TOK_RESTRICT2: case TOK_RESTRICT3:
			case 英_常量:
			case 中_常量:
			case 英_易变:
			case 中_易变:
			case 英_静态:
			case 中_静态:
			case '*':
			取下个单词();
			continue;
			default:
			break;
			}
			break;
		}
        n = -1;
        t1 = 0;
        if (单词编码 != 英_右中括号)
        {
            if (!局部符号栈 || (贮存 & 存储类型_静态))
            {
            	整数常量压入栈(常量表达式解析());
            }else
            {
				/* 变长数组（VLA）（只能在使用局部池池 && !值的数据类型_ 颜色时发生）长度必须始终计算，即使在不需要生成代码的情况下，以便初始化其大小槽（例如在 sizeof 或 typeof 下）。 */
				无需生成代码 = 0;
				生成表达式();
			}
            if ((栈顶值->寄存qi & (存储类型_掩码 | 存储类型_左值 | 存储类型_符号)) == 存储类型_VC常量)
            {
                n = 栈顶值->c.i;
                if (n < 0)
                {
                	错_误("无效的数组大小");
                }
            } else
            {
                if (!是_整数_基本类型(栈顶值->类型.数据类型 & 数据类型_基本类型))
                {
                	错_误("变长数组的大小应该是整数");
                }
                n = 0;
                t1 = 存储类型_变长数组;
            }
        }
        跳过(英_右中括号);
        /* 解析下一个post类型 */
        解析声明符后缀(类型, ad, 贮存, 0);

        if ((类型->数据类型 & 数据类型_基本类型) == 数据类型_函数)
        {
        	错_误("函数数组的声明");
        }
        if ((类型->数据类型 & 数据类型_基本类型) == 数据类型_无类型 || 类型_大小(类型, &unused_align) < 0)
        {
        	错_误("不完整类型元素数组的声明");
        }
        t1 |= 类型->数据类型 & 存储类型_变长数组;
        if (t1 & 存储类型_变长数组)
        {
            if (n < 0)
            {
            	错_误("在变长数组（VLA ）中需要明确的内部数组大小");
            }
            局部变量索引 -= 类型_大小(&整数_类型, &对齐);
            局部变量索引 &= -对齐;
            n = 局部变量索引;
            vla_运行时类型大小(类型, &对齐);
            gen_op('*');
            值压入栈(&整数_类型, 存储类型_局部|存储类型_左值, n);
            vswap();
            存储栈顶值();
        }
        if (n != -1)
        {
        	弹出堆栈值();
        }
		无需生成代码 = 保存_无需代码_生成;
        /* 我们推送一个包含数组元素类型的匿名符号 */
        s = 将符号放入符号栈(符号_字段, 类型, 0, n);
        类型->数据类型 = (t1 ? 存储类型_变长数组 : 数据类型_数组) | 数据类型_指针;
        类型->引用符号 = s;
    }
    return 1;
}

/* 解析初始声明符列表：直接声明符，指针声明符，数组，函数，结构体等等。。
 * 类型：数据类型。并在'类型'中返回类型。'类型' 应该包含基本类型。
 * ad：基本类型的属性定义。 可以通过 解析初始声明符列表() 修改。
 * v：单词编号
 * 声明符的种类：直接声明符 | 抽象声明符
 * */
static 类型ST *解析初始声明符列表(类型ST *类型, GNUC属性ST *ad, int *标识符的编码, int 声明符种类)
{
    类型ST *post, *ret;
    int 限定符, 贮存;
    贮存 = 类型->数据类型 & 值的数据类型_贮存;
    类型->数据类型 &= ~值的数据类型_贮存;
    post = ret = 类型;
    while (单词编码 == '*')/*1.返回指针类型*/
    {
        限定符 = 0;
    重做:
        取下个单词();
        switch(单词编码)
        {
        case 英__原子:
            限定符 |= 值的数据类型_原子;
            goto 重做;
        case 英_常量:
        case 中_常量:
            限定符 |= 存储类型_常量;
            goto 重做;
        case 英_易变:
        case 中_易变:
            限定符 |= 存储类型_易变;
            goto 重做;
        case 英_限制:
        case 中_限制:
        case TOK_RESTRICT2:
        case TOK_RESTRICT3:
            goto 重做;
		/* XXX: 阐明属性处理 */
		case TOK_ATTRIBUTE1:
		case TOK_ATTRIBUTE2:
	    解析属性(ad);
	    break;
        }
        生成指针类型(类型);
        类型->数据类型 |= 限定符;
		if (ret == 类型)
		{
			ret = 返回指针类型(类型);/* 最里面指向的类型是第一个派生的类型*/
		}
    }

    if (单词编码 == 英_左小括号)/*2.解析后缀类型声明符*/
    {
		/* 这可能是抽象声明符 ('int ()') 的参数类型列表，使用 解析声明符后缀 进行测试。  */
		if (!解析声明符后缀(类型, ad, 0, 声明符种类))
		{
			解析属性(ad);
			post = 解析初始声明符列表(类型, ad, 标识符的编码, 声明符种类);
			跳过(英_右小括号);
		} else
		{
			goto 抽象的;
		}
    } else if (单词编码 >= TOK_标识符 && (声明符种类 & 声明符_直接))/*3.直接声明符*/
    {
		/* 类型标识符 */
		*标识符的编码 = 单词编码;
		取下个单词();
    } else
    {
	  抽象的:
		if (!(声明符种类 & 声明符_抽象))
		{
			应为("标识符");
		}
		*标识符的编码 = 0;
    }
    解析声明符后缀(post, ad, 贮存, 0); //标识符没有后缀的，此函数不执行任何的动作
    解析属性(ad);   /* 解析 __attribute__((...)) GNUC扩展. 如果没有 __attribute__((...))修饰，就会直接跳过此解析*/
    类型->数据类型 |= 贮存;
    return ret;
}

/* 具有完整错误检查和边界检查的间接 */
静态函数 void 间接(void)
{
    if ((栈顶值->类型.数据类型 & 数据类型_基本类型) != 数据类型_指针)
    {
        if ((栈顶值->类型.数据类型 & 数据类型_基本类型) == 数据类型_函数)
        {
        	return;
        }
        应为("指针");
    }
    if (栈顶值->寄存qi & 存储类型_左值)
    {
    	生成值(RC_INT);
    }
    栈顶值->类型 = *返回指针类型(&栈顶值->类型);
    /* 数组和函数永远不是左值 */
    if (!(栈顶值->类型.数据类型 & (数据类型_数组 | 存储类型_变长数组)) && (栈顶值->类型.数据类型 & 数据类型_基本类型) != 数据类型_函数)
    {
        栈顶值->寄存qi |= 存储类型_左值;
        /* 如果绑定检查，则必须检查引用的指针 */
#ifdef 启用边界检查M
        if (全局虚拟机->使用_边界_检查器)
        {
        	栈顶值->寄存qi |= 存储类型_必须边界检查;
        }
#endif
    }
}

/* 将参数传递给函数并进行类型检查和强制转换 */
static void 函数参数类型检查和转换(符号ST *func, 符号ST *arg)
{
    int 函数_类型;
    类型ST 类型;

    函数_类型= func->f.函数_类型;
    if (函数_类型== 函数原型_旧 ||
        (函数_类型== 函数原型_省略 && arg == NULL)) {
        /* 默认转换：只需要将 float 转换为 double */
        if ((栈顶值->类型.数据类型 & 数据类型_基本类型) == 数据类型_浮点) {
            生成_转换_s(数据类型_双精度);
        } else if (栈顶值->类型.数据类型 & 存储类型_位域) {
            类型.数据类型 = 栈顶值->类型.数据类型 & (数据类型_基本类型 | 存储类型_无符号的);
	    类型.引用符号 = 栈顶值->类型.引用符号;
            生成_转换(&类型);
        } else if (栈顶值->寄存qi & 存储类型_必须转换) {
            强制_字符短整数_转换();
        }
    } else if (arg == NULL) {
        错_误("函数有太多的参数");
    } else {
        类型 = arg->类型;
        类型.数据类型 &= ~存储类型_常量; /* 需要这样做以避免错误警告 */
        生成指定转换(&类型);
    }
}

/*解析一个表达式并返回它的类型，没有任何副作用。 */
static void 表达式_类型(类型ST *类型, void (*expr_fn)(void))
{
    无需生成代码++;
    expr_fn();
    *类型 = 栈顶值->类型;
    弹出堆栈值();
    无需生成代码--;
}

/* 解析“(类型)”或“(expr)”形式的表达式并返回其类型 */
static void 解析_表达式_类型(类型ST *类型)
{
    int n;
    GNUC属性ST ad;

    跳过(英_左小括号);
    if (解析声明说明符(类型, &ad))
    {
        解析初始声明符列表(类型, &ad, &n, 声明符_抽象);
    } else
    {
        表达式_类型(类型, 生成表达式);
    }
    跳过(英_右小括号);
}

static void 解析_类型(类型ST *类型)
{
    GNUC属性ST ad;
    int n;

    if (!解析声明说明符(类型, &ad)) {
        应为("类型");
    }
    解析初始声明符列表(类型, &ad, &n, 声明符_抽象);
}

static void 解析_内置_参数(int nc, const char *args)
{
    char c, sep = 英_左小括号;
    类型ST 类型;
    if (nc)
        无需生成代码++;
    取下个单词();
    if (*args == 0)
	跳过(sep);
    while ((c = *args++)) {
	跳过(sep);
	sep = 英_逗号;
        if (c == 't') {
            解析_类型(&类型);
	    vpush(&类型);
	    continue;
        }
        等于表达式();
        类型.引用符号 = NULL;
        类型.数据类型 = 0;
	switch (c) {
	    case 'e':
		continue;
	    case 'V':
                类型.数据类型 = 存储类型_常量;
	    case 'v':
                类型.数据类型 |= 数据类型_无类型;
                生成指针类型 (&类型);
                break;
	    case 'S':
                类型.数据类型 = 存储类型_常量;
	    case 's':
                类型.数据类型 |= 字符_类型.数据类型;
                生成指针类型 (&类型);
                break;
	    case 'i':
                类型.数据类型 = 数据类型_整数;
                break;
	    case 'l':
                类型.数据类型 = 值的数据类型_SIZE_T;
                break;
	    default:
                break;
	}
        生成指定转换(&类型);
    }
    跳过(英_右小括号);
    if (nc)
        无需生成代码--;
}

static void 解析_原子(int atok)
{
    int 大小, 对齐, arg;
    类型ST *atom, *atom_ptr, ct = {0};
    char buf[40];
    static const char *const templates[] = {
        /*
         * Each entry consists of callback and function template.
         * The template represents argument types and return 类型.
         *
         * ? void (return-only)
         * b bool
         * a atomic
         * A read-only atomic
         * p pointer to memory
         * v value
         * m memory model
         */

        /* keep in order of appearance in 单词.h: */
        /* __atomic_store */            "avm.?",
        /* __atomic_load */             "Am.v",
        /* __atomic_exchange */         "avm.v",
        /* __atomic_compare_exchange */ "apvbmm.b",
        /* __atomic_fetch_add */        "avm.v",
        /* __atomic_fetch_sub */        "avm.v",
        /* __atomic_fetch_or */         "avm.v",
        /* __atomic_fetch_xor */        "avm.v",
        /* __atomic_fetch_and */        "avm.v"
    };
    const char *template = templates[(atok - TOK___atomic_store)];

    atom = atom_ptr = NULL;
    大小 = 0; /* pacify compiler */
    取下个单词();
    跳过(英_左小括号);
    for (arg = 0;;) {
        等于表达式();
        switch (template[arg]) {
        case 'a':
        case 'A':
            atom_ptr = &栈顶值->类型;
            if ((atom_ptr->数据类型 & 数据类型_基本类型) != 数据类型_指针)
                应为("指针");
            atom = 返回指针类型(atom_ptr);
            大小 = 类型_大小(atom, &对齐);
            if (大小 > 8
                || (大小 & (大小 - 1))
                || (atok > TOK___atomic_compare_exchange
                    && (0 == 基本类型_大小(atom->数据类型 & 数据类型_基本类型)
                        || (atom->数据类型 & 数据类型_基本类型) == 数据类型_指针)))
                应为("整数或整数大小的指针目标类型");
            /* GCC does not care either: */
            /* if (!(atom->数据类型 & 值的数据类型_原子))
                zhi_警告("pointer target declaration is missing '_Atomic'"); */
            break;

        case 'p':
            if ((栈顶值->类型.数据类型 & 数据类型_基本类型) != 数据类型_指针
             || 类型_大小(返回指针类型(&栈顶值->类型), &对齐) != 大小)
                错_误("参数 %d中的指针目标类型不匹配", arg + 1);
            生成指定转换(atom_ptr);
            break;
        case 'v':
            生成指定转换(atom);
            break;
        case 'm':
            生成指定转换(&整数_类型);
            break;
        case 'b':
            ct.数据类型 = 数据类型_布尔;
            生成指定转换(&ct);
            break;
        }
        if ('.' == template[++arg])
            break;
        跳过(英_逗号);
    }
    跳过(英_右小括号);

    ct.数据类型 = 数据类型_无类型;
    switch (template[arg + 1]) {
    case 'b':
        ct.数据类型 = 数据类型_布尔;
        break;
    case 'v':
        ct = *atom;
        break;
    }

    sprintf(buf, "%s_%d", 取单词字符串(atok, 0), 大小);
    推送_辅助_函数引用(标记分配常量(buf));
    vrott(arg + 1);
    g函数_调用(arg);

    vpush(&ct);
    将函数返回寄存器放入堆栈值(栈顶值, ct.数据类型);
    if (ct.数据类型 == 数据类型_布尔) {
#ifdef 显式扩展返回值
	栈顶值->寄存qi |= BFVAL(存储类型_必须转换, 1);
#else
	栈顶值->类型.数据类型 = 数据类型_整数;
#endif
    }
}

静态函数 void 一元(void)
{
    int n, t, 对齐, 大小, r, sizeof_caller;
    类型ST 类型;
    符号ST *s;
    GNUC属性ST ad;

    /* generate line number info */
    if (调试_模式)
        zhi_debug_line(全局虚拟机), zhi_tcov_check_line (1);

    sizeof_caller = in_sizeof;
    in_sizeof = 0;
    类型.引用符号 = NULL;
    /* XXX: GCC 2.95.3 does not generate a table although it should be
       better here */
 tok_next:
    switch(单词编码)
    {
    case 英_扩展:
        取下个单词();
        goto tok_next;
    case TOK_LCHAR:
#ifdef 目标_PE
        t = 数据类型_短整数|存储类型_无符号的;
        goto push_tokc;
#endif
    case TOK_整数常量:
    case TOK_字符常量: 
	t = 数据类型_整数;
 push_tokc:
	类型.数据类型 = t;
	常量压入栈(&类型, 存储类型_VC常量, &单词常量);
        取下个单词();
        break;
    case TOK_无符号整数常量:
        t = 数据类型_整数 | 存储类型_无符号的;
        goto push_tokc;
    case TOK_长长整数常量:
        t = 数据类型_长长整数;
	goto push_tokc;
    case TOK_无符号长长整数常量:
        t = 数据类型_长长整数 | 存储类型_无符号的;
	goto push_tokc;
    case TOK_浮点常量:
        t = 数据类型_浮点;
	goto push_tokc;
    case TOK_双精度常量:
        t = 数据类型_双精度;
	goto push_tokc;
    case TOK_长双精度常量:
        t = 数据类型_长双精度;
	goto push_tokc;
    case TOK_长整数常量:
        t = (长整数大小 == 8 ? 数据类型_长长整数 : 数据类型_整数) | 数据类型_长整数;
	goto push_tokc;
    case TOK_无符号长整数常量:
        t = (长整数大小 == 8 ? 数据类型_长长整数 : 数据类型_整数) | 数据类型_长整数 | 存储类型_无符号的;
	goto push_tokc;
    case TOK___FUNCTION__:
        if (!GNU_C_扩展)
            goto tok_identifier;
        /* fall thru */
    case TOK___FUNC__:
        {
            节ST *sec;
            int 长度;
            /* special function name identifier */
            长度 = strlen(函数名) + 1;
            /* generate char[长度] 类型 */
            类型.数据类型 = 字符_类型.数据类型;
            if (全局虚拟机->警告_写_字符串 & 警告模式_开启)
                类型.数据类型 |= 存储类型_常量;
            生成指针类型(&类型);
            类型.数据类型 |= 数据类型_数组;
            类型.引用符号->c = 长度;
            sec = 只读数据_节;
            添加虚拟符号推送对节偏移的引用(&类型, sec, sec->当前数据_偏移量, 长度);
            if (!无需静态数据输出)
                memcpy(节_预留指定大小内存(sec, 长度), 函数名, 长度);
            取下个单词();
        }
        break;
    case TOK_LSTR:
#ifdef 目标_PE
        t = 数据类型_短整数 | 存储类型_无符号的;
#else
        t = 数据类型_整数;
#endif
        goto str_init;
    case TOK_字符串常量:
        /* string parsing */
        t = 字符_类型.数据类型;
    str_init:
        if (全局虚拟机->警告_写_字符串 & 警告模式_开启)
            t |= 存储类型_常量;
        类型.数据类型 = t;
        生成指针类型(&类型);
        类型.数据类型 |= 数据类型_数组;
        memset(&ad, 0, sizeof(GNUC属性ST));
        ad.section = 只读数据_节;
        声明符初始化值分配(&类型, &ad, 存储类型_VC常量, 2, 0, 0);
        break;
    case 英_左小括号:
        取下个单词();
        /* cast ? */
        if (解析声明说明符(&类型, &ad)) {
            解析初始声明符列表(&类型, &ad, &n, 声明符_抽象);
            跳过(英_右小括号);
            /* 检查ISOC99复合文字 */
            if (单词编码 == 英_左大括号) {
                    /* 默认情况下，类型在本地分配 */
                if (全局_表达式)
                    r = 存储类型_VC常量;
                else
                    r = 存储类型_局部;
                /* 除数组外的所有都是左值 */
                if (!(类型.数据类型 & 数据类型_数组))
                    r |= 存储类型_左值;
                memset(&ad, 0, sizeof(GNUC属性ST));
                声明符初始化值分配(&类型, &ad, r, 1, 0, 0);
            } else {
                if (sizeof_caller) {
                    vpush(&类型);
                    return;
                }
                一元();
                生成_转换(&类型);
            }
        } else if (单词编码 == 英_左大括号) {
	    int 保存_无需代码_生成 = 无需生成代码;
            if (需要_常量 && !(无需生成代码 & 未赋值子表达式))
                应为("常量");
            if (0 == 局部_范围)
                错_误("函数外的语句表达式");
            /* 保存所有寄存器 */
            save_regs(0);
            /* 声明表达：我们不接受像GCC那样在内部中断/继续。我们确实保留了无需生成代码状态，因为语句表达式永远不能从外部输入，
             * 所以代码发射的任何重新激活（来自标签或循环头）都可以在结束后再次禁用。 */
            块(1);
	    无需生成代码 = 保存_无需代码_生成;
            跳过(英_右小括号);
        } else {
            生成表达式();
            跳过(英_右小括号);
        }
        break;
    case '*':
        取下个单词();
        一元();
        间接();
        break;
    case '&':
        取下个单词();
        一元();
        /* functions names must be treated as function pointers,
           except for 一元 '&' and sizeof. Since we consider that
           functions are not lvalues, we only have to handle it
           there and in function calls. */
        /* arrays can also be used although they are not lvalues */
        if ((栈顶值->类型.数据类型 & 数据类型_基本类型) != 数据类型_函数 &&
            !(栈顶值->类型.数据类型 & 数据类型_数组))
            测试_左值();
        if (栈顶值->sym)
          栈顶值->sym->a.addrtaken = 1;
        生成指针类型(&栈顶值->类型);
        gaddrof();
        break;
    case 英_叹号:
        取下个单词();
        一元();
        生成零或非零测试(TOK_等于);
        break;
    case '~':
        取下个单词();
        一元();
        整数常量压入栈(-1);
        gen_op('^');
        break;
    case '+':
        取下个单词();
        一元();
        if ((栈顶值->类型.数据类型 & 数据类型_基本类型) == 数据类型_指针)
            错_误("一元加不接受指针");
        /* In order to force cast, we add zero, except for floating point
	   where we really need an noop (otherwise -0.0 will be transformed
	   into +0.0).  */
	if (!是_浮点(栈顶值->类型.数据类型)) {
	    整数常量压入栈(0);
	    gen_op('+');
	}
        break;
    case 英_取大小:
    case 中_取大小:
    case TOK_ALIGNOF1:
    case TOK_ALIGNOF2:
    case TOK_ALIGNOF3:
        t = 单词编码;
        取下个单词();
        in_sizeof++;
        表达式_类型(&类型, 一元); /* Perform a in_sizeof = 0; */
        s = NULL;
        if (栈顶值[1].寄存qi & 存储类型_符号)
            s = 栈顶值[1].sym; /* hack: accessing previous 栈顶值 */
        大小 = 类型_大小(&类型, &对齐);
        if (s && s->a.aligned)
            对齐 = 1 << (s->a.aligned - 1);
        if (t == 英_取大小 || t == 中_取大小) {
            if (!(类型.数据类型 & 存储类型_变长数组)) {
                if (大小 < 0)
                    错_误("应用于不完整类型的sizeof");
                指针大小的常量压入栈(大小);
            } else {
                vla_运行时类型大小(&类型, &对齐);
            }
        } else {
            指针大小的常量压入栈(对齐);
        }
        栈顶值->类型.数据类型 |= 存储类型_无符号的;
        break;

    case TOK_builtin_expect:
	/* __builtin_expect目前没有操 */
	解析_内置_参数(0, "ee");
	弹出堆栈值();
        break;
    case TOK_builtin_types_compatible_p:
	解析_内置_参数(0, "tt");
	栈顶值[-1].类型.数据类型 &= ~(存储类型_常量 | 存储类型_易变);
	栈顶值[0].类型.数据类型 &= ~(存储类型_常量 | 存储类型_易变);
	n = 是兼容类型(&栈顶值[-1].类型, &栈顶值[0].类型);
	栈顶值 -= 2;
	整数常量压入栈(n);
        break;
    case TOK_builtin_choose_expr:
	{
	    int64_t c;
	    取下个单词();
	    跳过(英_左小括号);
	    c = 常量表达式64();
	    跳过(英_逗号);
	    if (!c) {
		无需生成代码++;
	    }
	    等于表达式();
	    if (!c) {
		弹出堆栈值();
		无需生成代码--;
	    }
	    跳过(英_逗号);
	    if (c) {
		无需生成代码++;
	    }
	    等于表达式();
	    if (c) {
		弹出堆栈值();
		无需生成代码--;
	    }
	    跳过(英_右小括号);
	}
        break;
    case TOK_builtin_constant_p:
	解析_内置_参数(1, "e");
	n = (栈顶值->寄存qi & (存储类型_掩码 | 存储类型_左值)) == 存储类型_VC常量 &&
            !((栈顶值->寄存qi & 存储类型_符号) && 栈顶值->sym->a.addrtaken);
	栈顶值--;
	整数常量压入栈(n);
        break;
    case TOK_builtin_frame_address:
    case TOK_builtin_return_address:
        {
            int tok1 = 单词编码;
            int level;
            取下个单词();
            跳过(英_左小括号);
            if (单词编码 != TOK_整数常量) {
                错_误("%s 只取正整数",tok1 == TOK_builtin_return_address ? "__builtin_return_address" : "__builtin_frame_address");
            }
            level = (uint32_t)单词常量.i;
            取下个单词();
            跳过(英_右小括号);
            类型.数据类型 = 数据类型_无类型;
            生成指针类型(&类型);
            值压入栈(&类型, 存储类型_局部, 0);       /* local frame */
            while (level--) {
#ifdef 目标_RISCV64
                整数常量压入栈(2*宏_指针大小);
                gen_op('-');
#endif
                生成指针类型(&栈顶值->类型);
                间接();                    /* -> parent frame */
            }
            if (tok1 == TOK_builtin_return_address) {
                // 假设返回地址刚好在堆栈上的帧指针之上
#ifdef 目标_ARM
                整数常量压入栈(2*宏_指针大小);
                gen_op('+');
#elif defined 目标_RISCV64
                整数常量压入栈(宏_指针大小);
                gen_op('-');
#else
                整数常量压入栈(宏_指针大小);
                gen_op('+');
#endif
                生成指针类型(&栈顶值->类型);
                间接();
            }
        }
        break;
#ifdef 目标_RISCV64
    case TOK_builtin_va_start:
        解析_内置_参数(0, "ee");
        r = 栈顶值->寄存qi & 存储类型_掩码;
        if (r == 存储类型_LLOCAL)
            r = 存储类型_局部;
        if (r != 存储类型_局部)
            错_误("__builtin_va_start 应该是一个局部变量");
        gen_va_start();
	存储栈顶值();
        break;
#endif
#ifdef 目标_X86_64
#ifdef 目标_PE
    case TOK_builtin_va_start:
	解析_内置_参数(0, "ee");
        r = 栈顶值->寄存qi & 存储类型_掩码;
        if (r == 存储类型_LLOCAL)
            r = 存储类型_局部;
        if (r != 存储类型_局部)
            错_误("__builtin_va_start 应该是一个局部变量");
        栈顶值->寄存qi = r;
	栈顶值->类型 = 字符_指针_类型;
	栈顶值->c.i += 8;
	存储栈顶值();
        break;
#else
    case TOK_builtin_va_arg_types:
	解析_内置_参数(0, "t");
	整数常量压入栈(classify_x86_64_va_arg(&栈顶值->类型));
	vswap();
	弹出堆栈值();
	break;
#endif
#endif

#ifdef 目标_ARM64
    case TOK_builtin_va_start: {
	解析_内置_参数(0, "ee");
        //xx check types
        gen_va_start();
        整数常量压入栈(0);
        栈顶值->类型.数据类型 = 数据类型_无类型;
        break;
    }
    case TOK_builtin_va_arg: {
	解析_内置_参数(0, "et");
	类型 = 栈顶值->类型;
	弹出堆栈值();
        //xx check types
        gen_va_arg(&类型);
        栈顶值->类型 = 类型;
        break;
    }
    case TOK___arm64_clear_cache: {
	解析_内置_参数(0, "ee");
        gen_clear_cache();
        整数常量压入栈(0);
        栈顶值->类型.数据类型 = 数据类型_无类型;
        break;
    }
#endif

    /* atomic operations */
    case TOK___atomic_store:
    case TOK___atomic_load:
    case TOK___atomic_exchange:
    case TOK___atomic_compare_exchange:
    case TOK___atomic_fetch_add:
    case TOK___atomic_fetch_sub:
    case TOK___atomic_fetch_or:
    case TOK___atomic_fetch_xor:
    case TOK___atomic_fetch_and:
        解析_原子(单词编码);
        break;

    /* pre operations */
    case TOK_自加:
    case TOK_自减:
        t = 单词编码;
        取下个单词();
        一元();
        inc(0, t);
        break;
    case '-':
        取下个单词();
        一元();
	if (是_浮点(栈顶值->类型.数据类型)) {
            gen_opif(TOK_NEG);
	} else {
            整数常量压入栈(0);
            vswap();
            gen_op('-');
        }
        break;
    case TOK_逻辑与:
        if (!GNU_C_扩展)
            goto tok_identifier;
        取下个单词();
        /* allow to take the address of a label */
        if (单词编码 < TOK_UIDENT)
            应为("标签标识符");
        s = 查找标签(单词编码);
        if (!s) {
            s = 标签推送(&全局_标签_栈, 单词编码, 标签_向前的);
        } else {
            if (s->寄存qi == 标签_已声明)
                s->寄存qi = 标签_向前的;
        }
        if (!s->类型.数据类型) {
            s->类型.数据类型 = 数据类型_无类型;
            生成指针类型(&s->类型);
            s->类型.数据类型 |= 存储类型_静态;
        }
        类型符号值压入栈(&s->类型, s);
        取下个单词();
        break;

    case TOK_GENERIC:
    {
	类型ST controlling_type;
	int has_default = 0;
	int has_match = 0;
	int learn = 0;
	单词字符串ST *字符串 = NULL;
	int saved_const_wanted = 需要_常量;

	取下个单词();
	跳过(英_左小括号);
	需要_常量 = 0;
	表达式_类型(&controlling_type, 等于表达式);
	controlling_type.数据类型 &= ~(存储类型_常量 | 存储类型_易变 | 数据类型_数组);
	if ((controlling_type.数据类型 & 数据类型_基本类型) == 数据类型_函数)
	  生成指针类型(&controlling_type);
	需要_常量 = saved_const_wanted;
	for (;;) {
	    learn = 0;
	    跳过(英_逗号);
	    if (单词编码 == 英_默认 || 单词编码 == 中_默认) {
		if (has_default)
		    错_误("太多 'default'");
		has_default = 1;
		if (!has_match)
		    learn = 1;
		取下个单词();
	    } else {
	        GNUC属性ST ad_tmp;
		int itmp;
	        类型ST cur_type;

                in_通用的++;
		解析声明说明符(&cur_type, &ad_tmp);
                in_通用的--;

		解析初始声明符列表(&cur_type, &ad_tmp, &itmp, 声明符_抽象);
		if (对比类型(&controlling_type, &cur_type, 0)) {
		    if (has_match) {
		      错_误("类型匹配两次");
		    }
		    has_match = 1;
		    learn = 1;
		}
	    }
	    跳过(英_冒号);
	    if (learn) {
		if (字符串)
		    单词字符串_释放(字符串);
		跳过或保存块(&字符串);
	    } else {
		跳过或保存块(NULL);
	    }
	    if (单词编码 == 英_右小括号)
		break;
	}
	if (!字符串) {
	    char buf[60];
	    type_to_str(buf, sizeof buf, &controlling_type, NULL);
	    错_误("类型 '%s' 与任何关联都不匹配", buf);
	}
	开始_宏扩展(字符串, 1);
	取下个单词();
	等于表达式();
	if (单词编码 != TOK_文件结尾)
	    应为(",");
	结束_宏扩展();
        取下个单词();
	break;
    }
    // special qnan , snan and infinity values
    case TOK___NAN__:
        n = 0x7fc00000;
special_math_val:
	整数常量压入栈(n);
	栈顶值->类型.数据类型 = 数据类型_浮点;
        取下个单词();
        break;
    case TOK___SNAN__:
	n = 0x7f800001;
	goto special_math_val;
    case TOK___INF__:
	n = 0x7f800000;
	goto special_math_val;

    default:
    tok_identifier:
        t = 单词编码;
        取下个单词();
        if (t < TOK_UIDENT)
            应为("标识符");
        s = 查找标识符(t);
        if (!s || IS_ASM_SYM(s)) {
            const char *name = 取单词字符串(t, NULL);
            if (单词编码 != 英_左小括号)
                错_误("'%s' 未定义", name);
            /* 对于简单的函数调用，我们容忍对 int() 函数的未声明的外部引用 */
            zhi_警告_c(隐式函数声明警告)("函数 '%s' 的隐式声明", name);
            s = 外部_全局_符号(t, &函数_旧_类型);
        }

        r = s->寄存qi;
        /* A symbol that has a register is a local register variable,
           which starts out as 存储类型_局部 value.  */
        if ((r & 存储类型_掩码) < 存储类型_VC常量)
            r = (r & ~存储类型_掩码) | 存储类型_局部;

        值压入栈(&s->类型, r, s->c);
        /* Point to s as backpointer (even without r&存储类型_符号).
	   Will be used by at least the x86 inline asm parser for
	   regvars.  */
	栈顶值->sym = s;

        if (r & 存储类型_符号) {
            栈顶值->c.i = 0;
        } else if (r == 存储类型_VC常量 && IS_ENUM_VAL(s->类型.数据类型)) {
            栈顶值->c.i = s->enum_val;
        }
        break;
    }
    
    /* post operations 后缀表达式解析*/
    while (1) {
        if (单词编码 == TOK_自加 || 单词编码 == TOK_自减) {
            inc(1, 单词编码);
            取下个单词();
        } else if (单词编码 == '.' || 单词编码 == TOK_箭头 || 单词编码 == TOK_双精度常量) {
            int 限定符, cumofs = 0;
            /* field */ 
            if (单词编码 == TOK_箭头) 
                间接();
            限定符 = 栈顶值->类型.数据类型 & (存储类型_常量 | 存储类型_易变);
            测试_左值();
            gaddrof();
            /* 应为 pointer on structure */
            if ((栈顶值->类型.数据类型 & 数据类型_基本类型) != 数据类型_结构体)
                应为("结构体 or 共用体");
            if (单词编码 == TOK_双精度常量)
                应为("字段名称");
            取下个单词();
            if (单词编码 == TOK_整数常量 || 单词编码 == TOK_无符号整数常量)
                应为("字段名称");
	    s = 查找_字段(&栈顶值->类型, 单词编码, &cumofs);
            if (!s)
                错_误("没有找到字段: %s",  取单词字符串(单词编码 & ~符号_字段, &单词常量));
            /* add field offset to pointer */
            栈顶值->类型 = 字符_指针_类型; /* change 类型 to 'char *' */
            整数常量压入栈(cumofs + s->c);
            gen_op('+');
            /* change 类型 to field 类型, and set to lvalue */
            栈顶值->类型 = s->类型;
            栈顶值->类型.数据类型 |= 限定符;
            /* an array is never an lvalue */
            if (!(栈顶值->类型.数据类型 & 数据类型_数组)) {
                栈顶值->寄存qi |= 存储类型_左值;
#ifdef 启用边界检查M
                /* if bound checking, the referenced pointer must be checked */
                if (全局虚拟机->使用_边界_检查器)
                    栈顶值->寄存qi |= 存储类型_必须边界检查;
#endif
            }
            取下个单词();
        } else if (单词编码 == 英_左中括号) {
            取下个单词();
            生成表达式();
            gen_op('+');
            间接();
            跳过(英_右中括号);
        } else if (单词编码 == 英_左小括号) {
            栈值ST ret;
            符号ST *sa;
            int nb_args, ret_nregs, ret_align, regsize, variadic;

            /* function call  */
            if ((栈顶值->类型.数据类型 & 数据类型_基本类型) != 数据类型_函数) {
                /* pointer test (no array accepted) */
                if ((栈顶值->类型.数据类型 & (数据类型_基本类型 | 数据类型_数组)) == 数据类型_指针) {
                    栈顶值->类型 = *返回指针类型(&栈顶值->类型);
                    if ((栈顶值->类型.数据类型 & 数据类型_基本类型) != 数据类型_函数)
                        goto 错误_函数;
                } else {
                错误_函数:
                    应为("函数指针");
                }
            } else {
                栈顶值->寄存qi &= ~存储类型_左值; /* no lvalue */
            }
            /* get return 类型 */
            s = 栈顶值->类型.引用符号;
            取下个单词();
            sa = s->next; /* first parameter */
            nb_args = regsize = 0;
            ret.r2 = 存储类型_VC常量;
            /* compute first implicit argument if a structure is returned */
            if ((s->类型.数据类型 & 数据类型_基本类型) == 数据类型_结构体) {
                variadic = (s->f.函数_类型== 函数原型_省略);
                ret_nregs = gfunc_sret(&s->类型, variadic, &ret.类型,
                                       &ret_align, &regsize);
                if (ret_nregs <= 0) {
                    /* get some space for the returned structure */
                    大小 = 类型_大小(&s->类型, &对齐);
#ifdef 目标_ARM64
                /* On arm64, a small struct is return in registers.
                   It is much easier to write it to memory if we know
                   that we are allowed to write some extra bytes, so
                   round the allocated space up to a power of 2: */
                if (大小 < 16)
                    while (大小 & (大小 - 1))
                        大小 = (大小 | (大小 - 1)) + 1;
#endif
                    局部变量索引 = (局部变量索引 - 大小) & -对齐;
                    ret.类型 = s->类型;
                    ret.寄存qi = 存储类型_局部 | 存储类型_左值;
                    /* pass it as 'int' to avoid structure arg passing
                       problems */
                    vseti(存储类型_局部, 局部变量索引);
#ifdef 启用边界检查M
                    if (全局虚拟机->使用_边界_检查器)
                        --局部变量索引;
#endif
                    ret.c = 栈顶值->c;
                    if (ret_nregs < 0)
                      栈顶值--;
                    else
                      nb_args++;
                }
            } else {
                ret_nregs = 1;
                ret.类型 = s->类型;
            }

            if (ret_nregs > 0) {
                /* return in register */
                ret.c.i = 0;
                将函数返回寄存器放入堆栈值(&ret, ret.类型.数据类型);
            }
            if (单词编码 != 英_右小括号) {
                for(;;) {
                    等于表达式();
                    函数参数类型检查和转换(s, sa);
                    nb_args++;
                    if (sa)
                        sa = sa->next;
                    if (单词编码 == 英_右小括号)
                        break;
                    跳过(英_逗号);
                }
            }
            if (sa)
                错_误("函数的参数太少");
            跳过(英_右小括号);
            g函数_调用(nb_args);

            if (ret_nregs < 0) {
                常量压入栈(&ret.类型, ret.寄存qi, &ret.c);
#ifdef 目标_RISCV64
                arch_transfer_ret_regs(1);
#endif
            } else {
                /* return value */
                for (r = ret.寄存qi + ret_nregs + !ret_nregs; r-- > ret.寄存qi;) {
                    常量压入栈(&ret.类型, r, &ret.c);
                    栈顶值->r2 = ret.r2; /* Loop only happens when r2 is 存储类型_VC常量 */
                }

                /* handle packed struct return */
                if (((s->类型.数据类型 & 数据类型_基本类型) == 数据类型_结构体) && ret_nregs) {
                    int addr, offset;

                    大小 = 类型_大小(&s->类型, &对齐);
                    /* We're writing whole regs often, make sure there's enough
                       space.  Assume register 大小 is power of 2.  */
                    if (regsize > 对齐)
                      对齐 = regsize;
                    局部变量索引 = (局部变量索引 - 大小) & -对齐;
                    addr = 局部变量索引;
                    offset = 0;
                    for (;;) {
                        值压入栈(&ret.类型, 存储类型_局部 | 存储类型_左值, addr + offset);
                        vswap();
                        存储栈顶值();
                        栈顶值--;
                        if (--ret_nregs == 0)
                          break;
                        offset += regsize;
                    }
                    值压入栈(&s->类型, 存储类型_局部 | 存储类型_左值, addr);
                }

                /* Promote char/short return values. This is matters only
                   for calling function that were not compiled by ZHI and
                   only on some architectures.  For those where it doesn't
                   matter we 应为 things to be already promoted to int,
                   but not larger.  */
                t = s->类型.数据类型 & 数据类型_基本类型;
                if (t == 数据类型_字节 || t == 数据类型_短整数 || t == 数据类型_布尔) {
#ifdef 显式扩展返回值
                    栈顶值->寄存qi |= BFVAL(存储类型_必须转换, 1);
#else
                    栈顶值->类型.数据类型 = 数据类型_整数;
#endif
                }
            }
            if (s->f.函数_不返回) {
                if (调试_模式)
	            zhi_tcov_block_end (tcov_data.line);
                代码_关();
	    }
        } else {
            break;
        }
    }
}

#ifndef 优先_解析 /* 原始自顶向下解析器 */

static void expr_prod(void)
{
    int t;

    一元();
    while ((t = 单词编码) == '*' || t == '/' || t == '%') {
        取下个单词();
        一元();
        gen_op(t);
    }
}

static void 表达式_数字(void)
{
    int t;

    expr_prod();
    while ((t = 单词编码) == '+' || t == '-') {
        取下个单词();
        expr_prod();
        gen_op(t);
    }
}

static void 表达式_转换(void)
{
    int t;

    表达式_数字();
    while ((t = 单词编码) == TOK_左移 || t == TOK_右移) {
        取下个单词();
        表达式_数字();
        gen_op(t);
    }
}

static void 表达式_对比(void)
{
    int t;

    表达式_转换();
    while (((t = 单词编码) >= TOK_ULE && t <= TOK_大于) ||
           t == TOK_ULT || t == TOK_UGE) {
        取下个单词();
        表达式_转换();
        gen_op(t);
    }
}

static void expr_cmpeq(void)
{
    int t;

    表达式_对比();
    while ((t = 单词编码) == TOK_等于 || t == TOK_不等于) {
        取下个单词();
        表达式_对比();
        gen_op(t);
    }
}

static void 表达式_与(void)
{
    expr_cmpeq();
    while (单词编码 == '&') {
        取下个单词();
        expr_cmpeq();
        gen_op('&');
    }
}

static void 表达式_异或(void)
{
    表达式_与();
    while (单词编码 == '^') {
        取下个单词();
        表达式_与();
        gen_op('^');
    }
}

static void 表达式_或(void)
{
    表达式_异或();
    while (单词编码 == '|') {
        取下个单词();
        表达式_异或();
        gen_op('|');
    }
}

static void 表达式_landor(int op);

static void expr_land(void)
{
    表达式_或();
    if (单词编码 == TOK_逻辑与)
        表达式_landor(单词编码);
}

static void expr_lor(void)
{
    expr_land();
    if (单词编码 == TOK_逻辑或)
        表达式_landor(单词编码);
}

# define expr_landor_next(op) op == TOK_逻辑与 ? 表达式_或() : expr_land()
#else /* defined 优先_解析 */
# define expr_landor_next(op) 一元(), 中缀表达式(优先级(op) + 1)
# define expr_lor() 一元(), 中缀表达式(1)

static int 优先级(int 单词编码)
{
    switch (单词编码)
    {
	case TOK_逻辑或: return 1;
	case TOK_逻辑与: return 2;
	case '|': return 3;
	case '^': return 4;
	case '&': return 5;
	case TOK_等于: case TOK_不等于: return 6;
relat:
	case TOK_ULT: case TOK_UGE: return 7;
	case TOK_左移: case TOK_右移: return 8;
	case '+': case '-': return 9;
	case '*': case '/': case '%': return 10;
	default:
	    if (单词编码 >= TOK_ULE && 单词编码 <= TOK_大于)
	    {
	    	goto relat;
	    }
	    return 0;
    }
}
static unsigned char 优先[256];
static void 初始化_优先级(void)
{
    int i;
    for (i = 0; i < 256; i++)
    {
    	优先[i] = 优先级(i);
    }
}
#define 优先级(i) ((unsigned)i < 256 ? 优先[i] : 0)

static void 表达式_landor(int op);

static void 中缀表达式(int p)
{
    int t = 单词编码, p2;
    while ((p2 = 优先级(t)) >= p)
    {
        if (t == TOK_逻辑或 || t == TOK_逻辑与)
        {
            表达式_landor(t);
        } else
        {
            取下个单词();
            一元();
            if (优先级(单词编码) > p2)
            {
            	中缀表达式(p2 + 1);
            }
            gen_op(t);
        }
        t = 单词编码;
    }
}
#endif

/* 假设栈顶值是在条件上下文中使用的值（即与零相比），如果为假则返回 0，如果为真则返回 1，如果无法静态确定则返回 -1。  */
static int 条件_3个结果(void)
{
    int c = -1;
    if ((栈顶值->寄存qi & (存储类型_掩码 | 存储类型_左值)) == 存储类型_VC常量 &&
	(!(栈顶值->寄存qi & 存储类型_符号) || !栈顶值->sym->a.weak)) {
	vdup();
        生成_转换_s(数据类型_布尔);
	c = 栈顶值->c.i;
	弹出堆栈值();
    }
    return c;
}

static void 表达式_landor(int op)
{
    int t = 0, cc = 1, f = 0, i = op == TOK_逻辑与, c;
    for(;;) {
        c = f ? i : 条件_3个结果();
        if (c < 0)
            save_regs(1), cc = 0;
        else if (c != i)
            无需生成代码++, f = 1;
        if (单词编码 != op)
            break;
        if (c < 0)
            t = 生成值测试(i, t);
        else
            弹出堆栈值();
        取下个单词();
        expr_landor_next(op);
    }
    if (cc || f) {
        弹出堆栈值();
        整数常量压入栈(i ^ f);
        生成符号(t);
        无需生成代码 -= f;
    } else {
        设置_生成值测试(i, t);
    }
}

static int 是_条件_布尔(栈值ST *sv)
{
    if ((sv->寄存qi & (存储类型_掩码 | 存储类型_左值 | 存储类型_符号)) == 存储类型_VC常量
        && (sv->类型.数据类型 & 数据类型_基本类型) == 数据类型_整数)
        return (unsigned)sv->c.i < 2;
    if (sv->寄存qi == 存储类型_标志寄存器)
        return 1;
    return 0;
}

static void 条件表达式(void)
{
    int tt, u, r1, r2, rc, t1, t2, islv, c, g;
    栈值ST sv;
    类型ST 类型;
    int ncw_prev;

    expr_lor();
    if (单词编码 == 英_问号)
    {
        取下个单词();
	c = 条件_3个结果();
        g = (单词编码 == 英_冒号 && GNU_C_扩展);
        tt = 0;
        if (!g)
        {
            if (c < 0)
            {
                save_regs(1);
                tt = 生成值测试(1, 0);
            } else
            {
                弹出堆栈值();
            }
        } else if (c < 0)
        {
            /* 需要避免在每个分支中保存不同的寄存器 */
            save_regs(1);
            堆栈条目转换为寄存器值复制到另个寄存器();
            tt = 生成值测试(0, 0);
        }

        ncw_prev = 无需生成代码;
        if (c == 0)
        {
        	无需生成代码++;
        }

        if (!g)
        {
        	生成表达式();
        }
        if ((栈顶值->类型.数据类型 & 数据类型_基本类型) == 数据类型_函数)
        {
        	生成指针类型(&栈顶值->类型);
        }
        sv = *栈顶值; /* 保存值以便以后处理 */
        栈顶值--; /* 不弹出堆栈值 从而不刷新FP堆栈 */

        if (g)
        {
            u = tt;
        } else if (c < 0)
        {
            u = 生成到标签的跳转(0);
            生成符号(tt);
        } else
        {
        	u = 0;
        }
        无需生成代码 = ncw_prev;
        if (c == 1)
        {
        	无需生成代码++;
        }

        跳过(英_冒号);
        条件表达式();

        if (c < 0 && 是_条件_布尔(栈顶值) && 是_条件_布尔(&sv))
        {
            /* 优化 "if (f ? a > b : c || d) ..." 例如，通常 "a < b" 和 "c || d" 将首先被强制为 "(int)0/1"， 而这段代码直接跳转到 if 的 then/else 分支。 */
            t1 = 生成值测试(0, 0);
            t2 = 生成到标签的跳转(0);
            生成符号(u);
            值压入栈值(&sv);
            /* 将第二个操作的跳转目标与第一个操作的 存储类型_标志寄存器 结合起来  */
            设置_生成值测试(0, t1);
            设置_生成值测试(1, t2);
            无需生成代码 = ncw_prev;
            return;
        }

        if ((栈顶值->类型.数据类型 & 数据类型_基本类型) == 数据类型_函数)
        {
        	生成指针类型(&栈顶值->类型);
        }
        /* 根据ISOC规则将操作数强制转换为正确的类型 */
        if (!combine_types(&类型, &sv, 栈顶值, 英_问号))
        {
        	类型不兼容_错误(&sv.类型, &栈顶值->类型, "条件表达式中的类型不匹配（具有'%s'和'%s'）");
        }
        /* 通过将`（expr？a:b）`转换为`*（expr吗&a:&b）`，使`（exper？a:b`）保持结构的左值。mem`与“期望的左值”不出错 */
        islv = (栈顶值->寄存qi & 存储类型_左值) && (sv.寄存qi & 存储类型_左值) && 数据类型_结构体 == (类型.数据类型 & 数据类型_基本类型);
        /* 现在我们转换第二个操作数 */
        if (c != 1)
        {
            生成_转换(&类型);
            if (islv)
            {
                生成指针类型(&栈顶值->类型);
                gaddrof();
            } else if (数据类型_结构体 == (栈顶值->类型.数据类型 & 数据类型_基本类型))
            {
            	gaddrof();
            }
        }
        rc = 返回t的泛型寄存器类(类型.数据类型);
        /* 对于长时间，我们使用固定寄存器来避免处理复杂的移动 */
        if (USING_TWO_WORDS(类型.数据类型))
        {
        	rc = 返回t的函数寄存器类(类型.数据类型);
        }
        tt = r2 = 0;
        if (c < 0)
        {
            r2 = 生成值(rc);
            tt = 生成到标签的跳转(0);
        }
        生成符号(u);
        无需生成代码 = ncw_prev;
        /* 这很可怕，但我们还必须转换第一个操作数 */
        if (c != 0)
        {
            *栈顶值 = sv;
            生成_转换(&类型);
            if (islv)
            {
                生成指针类型(&栈顶值->类型);
                gaddrof();
            } else if (数据类型_结构体 == (栈顶值->类型.数据类型 & 数据类型_基本类型))
            {
            	gaddrof();
            }
        }
        if (c < 0)
        {
            r1 = 生成值(rc);
            move_reg(r2, r1, islv ? 数据类型_指针 : 类型.数据类型);
            栈顶值->寄存qi = r2;
            生成符号(tt);
        }
        if (islv)
        {
        	间接();
        }
    }
}

static void 等于表达式(void)
{
    int t;
    条件表达式();
    if ((t = 单词编码) == '=' || TOK_分配(t))
    {
        测试_左值();
        取下个单词();
        if (t == '=')
        {
            等于表达式();
        } else {
            vdup();
            等于表达式();
            gen_op(TOK_分配_OP(t));
        }
        存储栈顶值();
    }
}

静态函数 void 生成表达式(void)
{
    while (1)
    {
        等于表达式();
        if (单词编码 != 英_逗号)
            break;
        弹出堆栈值();
        取下个单词();
    }
}

/* 解析栈顶值中的常量表达式和返回值。  */
static void 常量表达式1(void)
{
    需要_常量++;
    无需生成代码 += 未赋值子表达式 + 1;
    条件表达式();
    无需生成代码 -= 未赋值子表达式 + 1;
    需要_常量--;
}

/* 解析一个整数常量并返回它的值。 */
static inline int64_t 常量表达式64(void)
{
    int64_t c;
    常量表达式1();
    if ((栈顶值->寄存qi & (存储类型_掩码 | 存储类型_左值 | 存储类型_符号)) != 存储类型_VC常量)
    {
    	应为("常量表达式解析");
    }
    c = 栈顶值->c.i;
    弹出堆栈值();
    return c;
}

/* 解析一个整数常量并返回它的值。 如果它不适合 32 位（有符号或无符号）就会报错。  */
静态函数 int 常量表达式解析(void)
{
    int c;
    int64_t wc = 常量表达式64();
    c = wc;
    if (c != wc && (unsigned)c != wc)
    {
    	错_误("常量超过 32 位");
    }
    return c;
}

/* ------------------------------------------------------------------------- */
/* 从函数返回 */

#ifndef 目标_ARM64
static void 生成函数_返回(类型ST *函数_类型)
{
    if ((函数_类型->数据类型 & 数据类型_基本类型) == 数据类型_结构体) {
        类型ST 类型, ret_type;
        int ret_align, ret_nregs, regsize;
        ret_nregs = gfunc_sret(函数_类型, 函数_可变参, &ret_type,
                               &ret_align, &regsize);
        if (ret_nregs < 0) {
#ifdef 目标_RISCV64
            arch_transfer_ret_regs(0);
#endif
        } else if (0 == ret_nregs) {
            /* 如果返回结构，必须将其复制到隐式第一个指针 arg 地点 */
            类型 = *函数_类型;
            生成指针类型(&类型);
            值压入栈(&类型, 存储类型_局部 | 存储类型_左值, func_vc);
            间接();
            vswap();
            /* 将结构值复制到指针 */
            存储栈顶值();
        } else {
            /* 返回封装到寄存器中的结构*/
            int 大小, addr, 对齐, rc;
            大小 = 类型_大小(函数_类型,&对齐);
            if ((栈顶值->寄存qi != (存储类型_局部 | 存储类型_左值) ||
                 (栈顶值->c.i & (ret_align-1)))
                && (对齐 & (ret_align-1))) {
                局部变量索引 = (局部变量索引 - 大小) & -ret_align;
                addr = 局部变量索引;
                类型 = *函数_类型;
                值压入栈(&类型, 存储类型_局部 | 存储类型_左值, addr);
                vswap();
                存储栈顶值();
                弹出堆栈值();
                值压入栈(&ret_type, 存储类型_局部 | 存储类型_左值, addr);
            }
            栈顶值->类型 = ret_type;
            rc = 返回t的函数寄存器类(ret_type.数据类型);
            if (ret_nregs == 1)
                生成值(rc);
            else {
                for (;;) {
                    vdup();
                    生成值(rc);
                    弹出堆栈值();
                    if (--ret_nregs == 0)
                      break;
                    /* 我们假设当一个结构体在多个寄存器中返回时，它们的类是s(n) = 2^n套件的连续值 */
                    rc <<= 1;
                    栈顶值->c.i += regsize;
                }
            }
        }
    } else {
        生成值(返回t的函数寄存器类(函数_类型->数据类型));
    }
    栈顶值--; /* NOT 弹出堆栈值() 因为在 x86 上它会刷新 fp 堆栈 */
}
#endif

static void 检查_函数_返回(void)
{
    if ((函数_返回类型.数据类型 & 数据类型_基本类型) == 数据类型_无类型)
        return;
    if (!strcmp (函数名, "开始") || !strcmp (函数名, "main") && (函数_返回类型.数据类型 & 数据类型_基本类型) == 数据类型_整数)
    {
        /* main 默认返回 0 */
        整数常量压入栈(0);
        生成指定转换(&函数_返回类型);
        生成函数_返回(&函数_返回类型);
    } else
    {
        zhi_警告("函数可能不返回值：'%s'", 函数名);
    }
}

/* ------------------------------------------------------------------------- */
/* switch/case */

static int case_cmpi(const void *pa, const void *pb)
{
    int64_t a = (*(struct case_t**) pa)->v1;
    int64_t b = (*(struct case_t**) pb)->v1;
    return a < b ? -1 : a > b;
}

static int case_cmpu(const void *pa, const void *pb)
{
    uint64_t a = (uint64_t)(*(struct case_t**) pa)->v1;
    uint64_t b = (uint64_t)(*(struct case_t**) pb)->v1;
    return a < b ? -1 : a > b;
}

static void gtst_addr(int t, int a)
{
    生成符号_地址(生成值测试(0, t), a);
}

static void gcase(struct case_t **base, int 长度, int *bsym)
{
    struct case_t *p;
    int e;
    int ll = (栈顶值->类型.数据类型 & 数据类型_基本类型) == 数据类型_长长整数;
    while (长度 > 8) {
        /* binary search */
        p = base[长度/2];
        vdup();
	if (ll)
	    长长整数压入栈(p->v2);
	else
	    整数常量压入栈(p->v2);
        gen_op(TOK_小于等于);
        e = 生成值测试(1, 0);
        vdup();
	if (ll)
	    长长整数压入栈(p->v1);
	else
	    整数常量压入栈(p->v1);
        gen_op(TOK_大于等于);
        gtst_addr(0, p->sym); /* v1 <= x <= v2 */
        /* x < v1 */
        gcase(base, 长度/2, bsym);
        /* x > v2 */
        生成符号(e);
        e = 长度/2 + 1;
        base += e; 长度 -= e;
    }
    /* linear scan */
    while (长度--) {
        p = *base++;
        vdup();
	if (ll)
	    长长整数压入栈(p->v2);
	else
	    整数常量压入栈(p->v2);
        if (p->v1 == p->v2) {
            gen_op(TOK_等于);
            gtst_addr(0, p->sym);
        } else {
            gen_op(TOK_小于等于);
            e = 生成值测试(1, 0);
            vdup();
	    if (ll)
	        长长整数压入栈(p->v1);
	    else
	        整数常量压入栈(p->v1);
            gen_op(TOK_大于等于);
            gtst_addr(0, p->sym);
            生成符号(e);
        }
    }
    *bsym = 生成到标签的跳转(*bsym);
}

/* ------------------------------------------------------------------------- */
/* __attribute__((清理(fn))) */

static void 调用域清理(符号ST *stop)
{
    符号ST *cls = 当前域->cl.s;

    for (; cls != stop; cls = cls->ncl) {
	符号ST *fs = cls->next;
	符号ST *vs = cls->定义的前一个同名符号;

	类型符号值压入栈(&fs->类型, fs);
	值压入栈(&vs->类型, vs->寄存qi, vs->c);
	栈顶值->sym = vs;
        生成指针类型(&栈顶值->类型);
	gaddrof();
	g函数_调用(1);
    }
}

static void try_call_cleanup_goto(符号ST *cleanupstate)
{
    符号ST *oc, *cc;
    int ocd, ccd;

    if (!当前域->cl.s)
	return;

    /* search NCA of both 清理 chains given parents and initial depth */
    ocd = cleanupstate ? cleanupstate->v & ~符号_字段 : 0;
    for (ccd = 当前域->cl.n, oc = cleanupstate; ocd > ccd; --ocd, oc = oc->ncl)
      ;
    for (cc = 当前域->cl.s; ccd > ocd; --ccd, cc = cc->ncl)
      ;
    for (; cc != oc; cc = cc->ncl, oc = oc->ncl, --ccd)
      ;

    调用域清理(cc);
}

/* call 'func' for each __attribute__((清理(func))) */
static void 块清理(struct 域S *o)
{
    int jmp = 0;
    符号ST *g, **pg;
    for (pg = &待处理_gotos; (g = *pg) && g->c > o->cl.n;) {
        if (g->定义的前一个同名符号->寄存qi & 标签_向前的) {
            符号ST *pcl = g->next;
            if (!jmp)
                jmp = 生成到标签的跳转(0);
            生成符号(pcl->jnext);
            调用域清理(o->cl.s);
            pcl->jnext = 生成到标签的跳转(0);
            if (!o->cl.n)
                goto 移除待处理;
            g->c = o->cl.n;
            pg = &g->上个;
        } else {
    移除待处理:
            *pg = g->上个;
            sym_free(g);
        }
    }
    生成符号(jmp);
    调用域清理(o->cl.s);
}

/* ------------------------------------------------------------------------- */
/* VLA */

static void vla_恢复(int 局部变量索引)
{
    if (局部变量索引)
        gen_vla_sp_恢复(局部变量索引);
}

static void vla_离开(struct 域S *o)
{
    struct 域S *c = 当前域, *v = NULL;
    for (; c != o && c; c = c->上个)
      if (c->vla.num)
        v = c;
    if (v)
      vla_恢复(v->vla.locorig);
}

/* ------------------------------------------------------------------------- */
/* 局部域 */

static void 新建域(struct 域S *o)
{
    /* 复制并链接上一个范围 */
    *o = *当前域;
    o->上个 = 当前域;
    当前域 = o;
    当前域->vla.num = 0;

    /* 记录本地声明栈位置 */
    o->lstk = 局部符号栈;
    o->llstk = 局部_标签_栈;
    ++局部_范围;

    if (调试_模式)
        zhi_debug_stabn(全局虚拟机, N_LBRAC, 指令在代码节位置 - 函数_索引);
}

static void 上一个域(struct 域S *o, int 是_表达式)
{
    vla_离开(o->上个);

    if (o->cl.s != o->上个->cl.s)
        块清理(o->上个);

    /* 弹出本地定义的标签 */
    弹出标签(&局部_标签_栈, o->llstk, 是_表达式);

    /* In the 是_表达式 case (a statement expression is finished here),
       栈顶值 might refer to symbols on the 局部符号栈.  Either via the
       类型 or via 栈顶值->sym.  We can't pop those nor any that in turn
       might be referred to.  To make it easier we don't roll back
       any symbols in that case; some upper level call to 块() will
       do that.  We do have to remove such symbols from the lookup
       tables, though.  符号弹出 will do that.  */

    /* pop locally defined symbols */
    弹出局部符号(o->lstk, 是_表达式);
    当前域 = o->上个;
    --局部_范围;

    if (调试_模式)
        zhi_debug_stabn(全局虚拟机, N_RBRAC, 指令在代码节位置 - 函数_索引);
}

/* 通过 break/continue(/goto)离开域 */
static void 离开域(struct 域S *o)
{
    if (!o)
        return;
    调用域清理(o->cl.s);
    vla_离开(o);
}

/* ------------------------------------------------------------------------- */
/* 从 'for do while' 循环调用块  */

static void 循环块(int *bsym, int *csym)
{
    struct 域S *lo = 循环域, *co = 当前域;
    int *b = co->bsym, *c = co->csym;
    if (csym) {
        co->csym = csym;
        循环域 = co;
    }
    co->bsym = bsym;
    块(0);
    co->bsym = b;
    if (csym) {
        co->csym = c;
        循环域 = lo;
    }
}

static void 块(int 是_表达式)
{
    int a, b, c, d, e, t;
    struct 域S o;
    符号ST *s;

    if (是_表达式)
    {
        /* 默认返回值是 (void) */
        整数常量压入栈(0);
        栈顶值->类型.数据类型 = 数据类型_无类型;
    }

again:
    t = 单词编码;
    /* 如果单词携带值，取下个单词()可能会将其销毁。仅使用无效代码，如f（）{“123”4；}*/
    if (TOK_哈希_值(t))
    {
    	goto 表达式标签;
    }
    取下个单词();

    if (调试_模式)
    {
    	zhi_tcov_check_line (0), zhi_tcov_块_开始 ();
    }
    if (t == 英_如果 || t == 中_如果)
    {
        跳过(英_左小括号);
        生成表达式();
        跳过(英_右小括号);
        a = 生成值测试(1, 0);
        块(0);
        if (单词编码 == 中_否则 || 单词编码 == 英_否则)
        {
            d = 生成到标签的跳转(0);
            生成符号(a);
            取下个单词();
            块(0);
            生成符号(d); /* 修补else跳转 */
        } else
        {
            生成符号(a);
        }

    } else if (t == 英_判断 || t == 中_判断) {
        d = 生成索引();
        跳过(英_左小括号);
        生成表达式();
        跳过(英_右小括号);
        a = 生成值测试(1, 0);
        b = 0;
        循环块(&a, &b);
        生成跳转_到固定地址(d);
        生成符号_地址(b, d);
        生成符号(a);

    } else if (t == 英_左大括号) {
        新建域(&o);

        /* 处理局部标签声明 */
        while (单词编码 == TOK_LABEL) {
            do {
                取下个单词();
                if (单词编码 < TOK_UIDENT)
                    应为("标签标识符");
                标签推送(&局部_标签_栈, 单词编码, 标签_已声明);
                取下个单词();
            } while (单词编码 == 英_逗号);
            跳过(英_分号);
        }

        while (单词编码 != 英_右大括号) {
	    外部声明(存储类型_局部, 0, NULL);
            if (单词编码 != 英_右大括号) {
                if (是_表达式)
                    弹出堆栈值();
                块(是_表达式);
            }
        }

        上一个域(&o, 是_表达式);
        if (局部_范围)
            取下个单词();
        else if (!无需生成代码)
            检查_函数_返回();

    } else if (t == 英_返回 || t == 中_返回) {
        b = (函数_返回类型.数据类型 & 数据类型_基本类型) != 数据类型_无类型;
        if (单词编码 != 英_分号) {
            生成表达式();
            if (b) {
                生成指定转换(&函数_返回类型);
            } else {
                if (栈顶值->类型.数据类型 != 数据类型_无类型)
                    zhi_警告("无返回值（void）函数返回了一个值");
                栈顶值--;
            }
        } else if (b) {
            zhi_警告("'返回(return)' 没有返回值");
            b = 0;
        }
        离开域(根域);
        if (b)
            生成函数_返回(&函数_返回类型);
        跳过(英_分号);
        /* 除非顶层块中的最后一个stmt，否则跳转 */
        if (单词编码 != 英_右大括号 || 局部_范围 != 1)
            返回符号 = 生成到标签的跳转(返回符号);
        if (调试_模式)
	    zhi_tcov_block_end (tcov_data.line);
        代码_关();

    } else if (t == 英_跳出 || t == 中_跳出) {
        /* 计算跳转 */
        if (!当前域->bsym)
            错_误("无法跳出（break）");
        if (当前switch && 当前域->bsym == 当前switch->bsym)
            离开域(当前switch->域S);
        else
            离开域(循环域);
        *当前域->bsym = 生成到标签的跳转(*当前域->bsym);
        跳过(英_分号);

    } else if (t == 英_继续 || t == 中_继续) {
        /* 计算跳转 */
        if (!当前域->csym)
            错_误("无法继续（continue）");
        离开域(循环域);
        *当前域->csym = 生成到标签的跳转(*当前域->csym);
        跳过(英_分号);

    } else if (t == 英_循环 || t == 中_循环) {
        新建域(&o);

        跳过(英_左小括号);
        if (单词编码 != 英_分号) {
            /* 循环init decl的c99？ */
            if (!外部声明(存储类型_局部, 1, NULL)) {
                /* 否，循环init-expr的常规 */
                生成表达式();
                弹出堆栈值();
            }
        }
        跳过(英_分号);
        a = b = 0;
        c = d = 生成索引();
        if (单词编码 != 英_分号) {
            生成表达式();
            a = 生成值测试(1, 0);
        }
        跳过(英_分号);
        if (单词编码 != 英_右小括号) {
            e = 生成到标签的跳转(0);
            d = 生成索引();
            生成表达式();
            弹出堆栈值();
            生成跳转_到固定地址(c);
            生成符号(e);
        }
        跳过(英_右小括号);
        循环块(&a, &b);
        生成跳转_到固定地址(d);
        生成符号_地址(b, d);
        生成符号(a);
        上一个域(&o, 0);

    } else if (t == 英_执行 || t == 中_执行) {
        a = b = 0;
        d = 生成索引();
        循环块(&a, &b);
        生成符号(b);
        /*此处只能用tok而不能用t*/
        if(单词编码 == 英_判断)
        {
        	跳过(英_判断);
        }
        else if(单词编码 == 中_判断)
        {
        	跳过(中_判断);
        }
        跳过(英_左小括号);
	生成表达式();
        跳过(英_右小括号);
        跳过(英_分号);
	c = 生成值测试(0, 0);
	生成符号_地址(c, d);
        生成符号(a);

    } else if (t == 英_选择 || t == 中_选择) {
        struct 选择S *sw;

        sw = 初始化内存(sizeof *sw);
        sw->bsym = &a;
        sw->域S = 当前域;
        sw->上个 = 当前switch;
        当前switch = sw;

        跳过(英_左小括号);
        生成表达式();
        跳过(英_右小括号);
        sw->sv = *栈顶值--; /* 保存switch值 */

        a = 0;
        b = 生成到标签的跳转(0); /* 跳到第一个分支 */
        循环块(&a, NULL);
        a = 生成到标签的跳转(a); /* 添加隐式中断 */
        /* 分支查找 */
        生成符号(b);

        if (sw->sv.类型.数据类型 & 存储类型_无符号的)
            qsort(sw->p, sw->n, sizeof(void*), case_cmpu);
        else
            qsort(sw->p, sw->n, sizeof(void*), case_cmpi);

        for (b = 1; b < sw->n; b++)
            if (sw->sv.类型.数据类型 & 存储类型_无符号的 ? (uint64_t)sw->p[b - 1]->v2 >= (uint64_t)sw->p[b]->v1 : sw->p[b - 1]->v2 >= sw->p[b]->v1)
                错_误("重复的分支(case)值");
        值压入栈值(&sw->sv);
        生成值(RC_INT);
        d = 0, gcase(sw->p, sw->n, &d);
        弹出堆栈值();
        if (sw->默认_符号)
            生成符号_地址(d, sw->默认_符号);
        else
            生成符号(d);
        /* break label */
        生成符号(a);

        动态数组_重置(&sw->p, &sw->n);
        当前switch = sw->上个;
        zhi_释放(sw);

    } else if (t == 英_分支 || t == 中_分支) {
        struct case_t *cr = zhi_分配内存(sizeof(struct case_t));
        if (!当前switch)
            应为("switch");
        cr->v1 = cr->v2 = 常量表达式64();
        if (GNU_C_扩展 && 单词编码 == TOK_三个点) {
            取下个单词();
            cr->v2 = 常量表达式64();
            if ((!(当前switch->sv.类型.数据类型 & 存储类型_无符号的) && cr->v2 < cr->v1)
                || (当前switch->sv.类型.数据类型 & 存储类型_无符号的 && (uint64_t)cr->v2 < (uint64_t)cr->v1))
                zhi_警告("empty case range");
        }
        tcov_data.指令在代码节位置 = 0;
        cr->sym = 生成索引();
        动态数组_追加元素(&当前switch->p, &当前switch->n, cr);
        跳过(英_冒号);
        是_表达式 = 0;
        goto block_after_label;

    } else if (t == 英_默认 || t == 中_默认) {
        if (!当前switch)
            应为("switch");
        if (当前switch->默认_符号)
            错_误("太多 'default'");
        tcov_data.指令在代码节位置 = 0;
        当前switch->默认_符号 = 生成索引();
        跳过(英_冒号);
        是_表达式 = 0;
        goto block_after_label;

    } else if (t == 英_转到 || t == 中_转到) {
        if (当前域->vla.num)
          vla_恢复(当前域->vla.locorig);
        if (单词编码 == '*' && GNU_C_扩展) {
            /* computed goto */
            取下个单词();
            生成表达式();
            if ((栈顶值->类型.数据类型 & 数据类型_基本类型) != 数据类型_指针)
                应为("指针");
            ggoto();

        } else if (单词编码 >= TOK_UIDENT) {
	    s = 查找标签(单词编码);
	    /* put forward definition if needed */
            if (!s)
              s = 标签推送(&全局_标签_栈, 单词编码, 标签_向前的);
            else if (s->寄存qi == 标签_已声明)
              s->寄存qi = 标签_向前的;

	    if (s->寄存qi & 标签_向前的) {
		/* start new goto chain for cleanups, linked via label->next */
		if (当前域->cl.s && !无需生成代码) {
                    将符号放入符号栈2(&待处理_gotos, 符号_字段, 0, 当前域->cl.n);
                    待处理_gotos->定义的前一个同名符号 = s;
                    s = 将符号放入符号栈2(&s->next, 符号_字段, 0, 0);
                    待处理_gotos->next = s;
                }
		s->jnext = 生成到标签的跳转(s->jnext);
	    } else {
		try_call_cleanup_goto(s->cleanupstate);
		生成跳转_到固定地址(s->jnext);
	    }
	    取下个单词();

        } else {
            应为("标签标识符");
        }
        跳过(英_分号);

    } else if (t == TOK_ASM1 || t == TOK_ASM2 || t == TOK_ASM3) {
        asm_instr();

    } else {
        if (单词编码 == 英_冒号 && t >= TOK_UIDENT) {
            /* label case */
	    取下个单词();
            s = 查找标签(t);
            if (s) {
                if (s->寄存qi == 标签_已定义)
                    错_误("重复标签 '%s'", 取单词字符串(s->v, NULL));
                s->寄存qi = 标签_已定义;
		if (s->next) {
		    符号ST *pcl; /* pending 清理 goto */
		    for (pcl = s->next; pcl; pcl = pcl->上个)
		      生成符号(pcl->jnext);
		    符号弹出(&s->next, NULL, 0);
		} else
		  生成符号(s->jnext);
            } else {
                s = 标签推送(&全局_标签_栈, t, 标签_已定义);
            }
            s->jnext = 生成索引();
            s->cleanupstate = 当前域->cl.s;

    block_after_label:
            vla_恢复(当前域->vla.局部变量索引);
            if (单词编码 != 英_右大括号)
                goto again;
            /* 我们接受这一点，但这是一个错误 */
            zhi_警告_c(警告_所有)("不推荐在复合语句末尾使用标签");

        } else {
            /* 表达式大小写 */
            if (t != 英_分号) {
                退回当前单词(t);
    表达式标签:
                if (是_表达式) {
                    弹出堆栈值();
                    生成表达式();
                } else
                {
                    生成表达式();
                    弹出堆栈值();
                }
                跳过(英_分号);
            }
        }
    }

    if (调试_模式)
        zhi_tcov_check_line (0), zhi_tcov_block_end (0);
}

/* 这将跳过包含平衡的｛｝和（）对的令牌流，停止在外部“，”和“｝”（或者如果我们以“｛”开头，则匹配“｝“）。
 * 如果STR然后将跳过的令牌分配并存储在*STR中。这不会检查（）和｛｝是否正确嵌套，即接受“（｛）｝”。  */
static void 跳过或保存块(单词字符串ST **字符串)
{
    int braces = 单词编码 == 英_左大括号;
    int level = 0;
    if (字符串)
      *字符串 = 单词字符串_分配();

    while ((level > 0 || (单词编码 != 英_右大括号 && 单词编码 != 英_逗号 && 单词编码 != 英_分号 && 单词编码 != 英_右小括号)))
    {
	int t;
	if (单词编码 == TOK_文件结尾)
	{
	     if (字符串 || level > 0)
	       错_误("不应该的文件结尾");
	     else
	       break;
	}
	if (字符串)
	  在单词字符串s中添加当前解析单词(*字符串);
	t = 单词编码;
	取下个单词();
	if (t == 英_左大括号 || t == 英_左小括号) {
	    level++;
	} else if (t == 英_右大括号 || t == 英_右小括号) {
	    level--;
	    if (level == 0 && braces && t == 英_右大括号)
	      break;
	}
    }
    if (字符串) {
	单词字符串_增加(*字符串, -1);
	单词字符串_增加(*字符串, 0);
    }
}

#define EXPR_CONST 1
#define EXPR_ANY   2

static void 解析初始化元素(int 表达式_类型)
{
    int saved_global_expr;
    switch(表达式_类型) {
    case EXPR_CONST:
        /* compound literals must be allocated globally in this case */
        saved_global_expr = 全局_表达式;
        全局_表达式 = 1;
        常量表达式1();
        全局_表达式 = saved_global_expr;
        /* NOTE: symbols are accepted, as well as lvalue for anon symbols
	   (compound literals).  */
        if (((栈顶值->寄存qi & (存储类型_掩码 | 存储类型_左值)) != 存储类型_VC常量
             && ((栈顶值->寄存qi & (存储类型_符号|存储类型_左值)) != (存储类型_符号|存储类型_左值)
                 || 栈顶值->sym->v < 第一个匿名符号))
#ifdef 目标_PE
                 || ((栈顶值->寄存qi & 存储类型_符号) && 栈顶值->sym->a.dllimport)
#endif
           )
            错_误("初始值设定项元素不是常量");
        break;
    case EXPR_ANY:
        等于表达式();
        break;
    }
}

#if 1
static void 初始化断言(初始参数S *p, int offset)
{
    if (p->sec ? !无需静态数据输出 && offset > p->sec->当前数据_偏移量
               : !无需生成代码 && offset > p->局部_偏移量)
        zhi_internal_error("初始值设定项溢出");
}
#else
#define 初始化断言(sec, offset)
#endif

/* 为基于变量的初始化设置零 */
static void 变量初始化为0(初始参数S *p, unsigned long c, int 大小)
{
    初始化断言(p, c + 大小);
    if (p->sec) {
        /* 无事可做，因为全局变量已经设置为零 */
    } else {
        推送_辅助_函数引用(TOK_memset);
        vseti(存储类型_局部, c);
#ifdef 目标_ARM
        指针大小的常量压入栈(大小);
        整数常量压入栈(0);
#else
        整数常量压入栈(0);
        指针大小的常量压入栈(大小);
#endif
        g函数_调用(3);
    }
}

#define DIF_FIRST     1
#define DIF_SIZE_ONLY 2
#define DIF_HAVE_ELEM 4
#define DIF_CLEAR     8

/* 删除指定范围 c ... c + 大小的重定位。 不幸的是，在非常特殊的情况下，重定位可能会发生无序*/
static void 删除声明符指定范围重定位(节ST *sec, int c, int 大小)
{
    ElfW_Rel *rel, *rel2, *rel_end;
    if (!sec || !sec->重定位)
        return;
    rel = rel2 = (ElfW_Rel*)sec->重定位->数据;
    rel_end = (ElfW_Rel*)(sec->重定位->数据 + sec->重定位->当前数据_偏移量);
    while (rel < rel_end) {
        if (rel->r_offset >= c && rel->r_offset < c + 大小) {
            sec->重定位->当前数据_偏移量 -= sizeof *rel;
        } else {
            if (rel2 != rel)
                memcpy(rel2, rel, sizeof *rel);
            ++rel2;
        }
        ++rel;
    }
}

static void 柔性声明符(初始参数S *p, 符号ST *引用符号, int index)
{
    if (引用符号 == p->弹性_数组_参考)
    {
        if (index >= 引用符号->c)
            引用符号->c = index + 1;
    } else if (引用符号->c < 0)
        错_误("在这种情况下，动态数组的大小为零");
}

/* t 是数组或结构类型。 c 是数组或结构地址。 当前_成员字段 是指向当前字段的指针，
 * 对于数组，'c' 成员包含当前的起始索引。 'flags' 与声明初始化相同。
 * 'al' 包含当前容器的已初始化长度（从 c 开始）。 这将返回它的新长度。 */
static int 声明符(初始参数S *p, 类型ST *类型, unsigned long c,符号ST **当前_成员字段, int flags, int al)
{
    符号ST *s, *f;
    int index, index_last, 对齐, l, nb_elems, 元素_大小;
    unsigned long corig = c;

    元素_大小 = 0;
    nb_elems = 1;

    if (flags & DIF_HAVE_ELEM)
        goto 没有_代号;

    if (GNU_C_扩展 && 单词编码 >= TOK_UIDENT)
    {
        l = 单词编码, 取下个单词();
        if (单词编码 == 英_冒号)
            goto 结构_字段;
        退回当前单词(l);
    }

    /* 注意：我们只支持最后一个指示符的范围 */
    while (nb_elems == 1 && (单词编码 == 英_左中括号 || 单词编码 == '.'))/*解析 数组声明符和结构体成员声明符*/
    {
        if (单词编码 == 英_左中括号)
        {
            if (!(类型->数据类型 & 数据类型_数组))
                应为("数组类型");
            取下个单词();
            index = index_last = 常量表达式解析();
            if (单词编码 == TOK_三个点 && GNU_C_扩展)
            {
                取下个单词();
                index_last = 常量表达式解析();
            }
            跳过(英_右中括号);
            s = 类型->引用符号;
            柔性声明符(p, s, index_last);
            if (index < 0 || index_last >= s->c || index_last < index)
	        错_误("索引超出数组边界或范围为空");
            if (当前_成员字段)
				(*当前_成员字段)->c = index_last;
            类型 = 返回指针类型(类型);
            元素_大小 = 类型_大小(类型, &对齐);
            c += index * 元素_大小;
            nb_elems = index_last - index + 1;
        } else
        {
            int cumofs;
            取下个单词();
            l = 单词编码;
        结构_字段:
            取下个单词();
            if ((类型->数据类型 & 数据类型_基本类型) != 数据类型_结构体)
                应为("结构体/共用体 类型");
            cumofs = 0;
			f = 查找_字段(类型, l, &cumofs);
            if (!f)
                应为("成员字段（field）");
            if (当前_成员字段)
                *当前_成员字段 = f;
			类型 = &f->类型;
            c += cumofs + f->c;
        }
        当前_成员字段 = NULL;
    }
    if (!当前_成员字段)
    {
        if (单词编码 == '=')
        {
            取下个单词();
        } else if (!GNU_C_扩展)
        {
	    应为("=");
        }
    } else
    {
    没有_代号:
        if (类型->数据类型 & 数据类型_数组)
        {
	    index = (*当前_成员字段)->c;
            s = 类型->引用符号;
            柔性声明符(p, s, index);
            if (index >= s->c)
                错_误("初始化者太多");
            类型 = 返回指针类型(类型);
            元素_大小 = 类型_大小(类型, &对齐);
            c += index * 元素_大小;
        } else
        {
            f = *当前_成员字段;
	    while (f && (f->v & 第一个匿名符号) && (f->类型.数据类型 & 存储类型_位域))
	        *当前_成员字段 = f = f->next;
            if (!f)
                错_误("初始化者太多");
	    类型 = &f->类型;
            c += f->c;
        }
    }

    if (!元素_大小) /* 结构体 */
        元素_大小 = 类型_大小(类型, &对齐);

    /* 使用指示符可以多次初始化同一元素。在这种情况下，我们需要删除可能已经存在的重新定位。*/
    if (!(flags & DIF_SIZE_ONLY) && c - corig < al)
    {
        删除声明符指定范围重定位(p->sec, c, 元素_大小 * nb_elems);
        flags &= ~DIF_CLEAR; /* 标记堆栈也脏 */
    }

    声明初始化(p, 类型, c, flags & ~DIF_FIRST);

    if (!(flags & DIF_SIZE_ONLY) && nb_elems > 1)
    {
        符号ST aref = {0};
        类型ST t1;
        int i;
        if (p->sec || (类型->数据类型 & 数据类型_数组))
        {
            /* 让初始初始值/存储栈顶值相信它是一个结构体 */
            aref.c = 元素_大小;
            t1.数据类型 = 数据类型_结构体, t1.引用符号 = &aref;
            类型 = &t1;
        }
        if (p->sec)
            添加虚拟符号推送对节偏移的引用(类型, p->sec, c, 元素_大小);
        else
	    值压入栈(类型, 存储类型_局部|存储类型_左值, c);
        for (i = 1; i < nb_elems; i++)
        {
            vdup();
            初始存储值(p, 类型, c + 元素_大小 * i);
		}
        弹出堆栈值();
    }

    c += nb_elems * 元素_大小;
    if (c - corig > al)
      al = c - corig;
    return al;
}

/* 将值或表达式直接存储在全局数据或本地数组中 */
static void 初始存储值(初始参数S *p, 类型ST *类型, unsigned long c)
{
    int bt;
    void *ptr;
    类型ST dtype;
    int 大小, 对齐;
    节ST *sec = p->sec;
    uint64_t val;

    dtype = *类型;
    dtype.数据类型 &= ~存储类型_常量; /* 需要这样做以避免错误警告 */

    大小 = 类型_大小(类型, &对齐);
    if (类型->数据类型 & 存储类型_位域)
        大小 = (BIT_POS(类型->数据类型) + BIT_SIZE(类型->数据类型) + 7) / 8;
    初始化断言(p, c + 大小);

    if (sec)
    {
        /* XXX: 不可携带 */
        /* XXX: 如果重新定位不正确，则生成错误 */
        生成指定转换(&dtype);
        bt = 类型->数据类型 & 数据类型_基本类型;

        if ((栈顶值->寄存qi & 存储类型_符号) && bt != 数据类型_指针 && (bt != (宏_指针大小 == 8 ? 数据类型_长长整数 : 数据类型_整数) || (类型->数据类型 & 存储类型_位域)) && !((栈顶值->寄存qi & 存储类型_VC常量) && 栈顶值->sym->v >= 第一个匿名符号) )
            错_误("初始化元素在加载时不可计算");
        if (无需静态数据输出)
        {
            栈顶值--;
            return;
        }

        ptr = sec->数据 + c;
        val = 栈顶值->c.i;

        /* XXX: 让代码更快？ */
	    /* XXX 这拒绝复合文字，如 '(void *){ptr}'。 问题是 '&sym' 的表示方式相同，上面的第一个匿名符号检查将排除这种情况，
	     * 但 'char *p = "string"' 中的 '"string"' 表示与 类型为VT_轨迹，符号为匿名符号。
	     * 即'(void *){x}'和'&(void *){x}'的栈顶值没有区别。 此处忽略指针类型实体。
	     * 希望没有真正的代码会使用标量类型的复合文字。  */
		if ((栈顶值->寄存qi & (存储类型_符号|存储类型_VC常量)) == (存储类型_符号|存储类型_VC常量) && 栈顶值->sym->v >= 第一个匿名符号 && (栈顶值->类型.数据类型 & 数据类型_基本类型) != 数据类型_指针)
		{
			/* 这些来自复合文字，memcpy 的东西。 */
			节ST *ssec;
			ElfSym *esym;
			ElfW_Rel *rel;
			esym = elfsym(栈顶值->sym);
			ssec = 全局虚拟机->节数组[esym->st_shndx];
			memmove (ptr, ssec->数据 + esym->st_value + (int)栈顶值->c.i, 大小);
			if (ssec->重定位)
			{
			/* 我们需要复制所有内存内容，包括重定位。 使用 relocs 是按顺序创建的事实，所以从 relocs 的末尾看，直到我们在复制的区域之前找到一个。  */
				unsigned long relofs = ssec->重定位->当前数据_偏移量;
				while (relofs >= sizeof(*rel))
				{
					relofs -= sizeof(*rel);
					rel = (ElfW_Rel*)(ssec->重定位->数据 + relofs);
					if (rel->r_offset >= esym->st_value + 大小)
					    continue;
					if (rel->r_offset < esym->st_value)
					    break;
					添加新的elf重定位信息(节符号表_数组, sec,c + rel->r_offset - esym->st_value,ELFW(R_TYPE)(rel->r_info),ELFW(R_SYM)(rel->r_info),
		#if 宏_指针大小 == 8
						   rel->r_addend
		#else
						   0
		#endif
						  );
				}
			}
		} else
		{
            if (类型->数据类型 & 存储类型_位域)
            {
                int bit_pos, 位大小, bits, n;
                unsigned char *p, v, m;
                bit_pos = BIT_POS(栈顶值->类型.数据类型);
                位大小 = BIT_SIZE(栈顶值->类型.数据类型);
                p = (unsigned char*)ptr + (bit_pos >> 3);
                bit_pos &= 7, bits = 0;
                while (位大小)
                {
                    n = 8 - bit_pos;
                    if (n > 位大小)
                        n = 位大小;
                    v = val >> bits << bit_pos;
                    m = ((1 << n) - 1) << bit_pos;
                    *p = (*p & ~m) | (v & m);
                    bits += n, 位大小 -= n, bit_pos = 0, ++p;
                }
            } else
				switch(bt)
				{
				case 数据类型_布尔:

					*(char *)ptr = val != 0;
					break;
				case 数据类型_字节:
					*(char *)ptr = val;
					break;
				case 数据类型_短整数:
					write16le(ptr, val);
					break;
				case 数据类型_浮点:
					write32le(ptr, val);
					break;
				case 数据类型_双精度:
					write64le(ptr, val);
					break;
				case 数据类型_长双精度:
#if defined ZHI_IS_NATIVE_387
					/* 主机和目标平台可能不同，但都具有 x87。 在 Windows 上，zhi 不使用 数据类型_长双精度，除非它是交叉编译器。 在这种情况下，作为主机编译器的 mingw gcc 带有 10 字节长双精度，
					 * 而 msvc 或 zhi 不会。 zhi 本身仍然可以通过 asm 进行翻译。 在任何情况下，我们都避免使用可能的随机字节 11 和 12。
					*/
					if (sizeof (long double) >= 10)
						memcpy(ptr, &栈顶值->c.ld, 10);
#ifdef __ZHIXIN__
					else if (sizeof (long double) == sizeof (double))
						__asm__("fldl %1\nfstpt %0\n" : "=m" (*ptr) : "m" (栈顶值->c.ld));
#endif
					else if (栈顶值->c.ld == 0.0)
						;
					else
#endif
					/* 对于其他平台，它应该可以在本机运行，但可能不适用于交叉编译器 */
					if (sizeof(long double) == 长双精度大小)
						memcpy(ptr, &栈顶值->c.ld, 长双精度大小);
					else if (sizeof(double) == 长双精度大小)
						memcpy(ptr, &栈顶值->c.ld, 长双精度大小);
#ifndef ZHI_CROSS_TEST
					else
						错_误("不能交叉编译长双精度常量");
#endif
			        break;

#if 宏_指针大小 == 8
				/* intptr_t 可能也需要重定位，见 zhitest.c:relocation_test()*/
				case 数据类型_长长整数:
				case 数据类型_指针:
					if (栈顶值->寄存qi & 存储类型_符号)
					  添加新的重定位条目(sec, 栈顶值->sym, c, R_DATA_PTR, val);
					else
					  write64le(ptr, val);
					break;
				case 数据类型_整数:
					write32le(ptr, val);
					break;
#else
				case 数据类型_长长整数:
						write64le(ptr, val);
						break;
					case 数据类型_指针:
					case 数据类型_整数:
					if (栈顶值->寄存qi & 存储类型_符号)
					  greloc(sec, 栈顶值->sym, c, R_DATA_PTR);
					write32le(ptr, val);
					break;
#endif
				default:
						//zhi_internal_error("unexpected 类型");
						break;
				}
		}
        栈顶值--;
    } else {
        值压入栈(&dtype, 存储类型_局部|存储类型_左值, c);
        vswap();
        存储栈顶值();
        弹出堆栈值();
    }
}

/* 't' 包含类型和存储信息。 “c”是“sec”部分中对象的偏移量。 如果 'sec' 为 NULL，则表示基于堆栈的分配。
 * 如果必须读取数组英_左大括号，则“flags & DIF_FIRST”为真（多维隐式数组初始化处理）。
 * 'flags & DIF_SIZE_ONLY' 如果只需要评估大小（仅适用于数组），则为真。 */
static void 声明初始化(初始参数S *p, 类型ST *类型, unsigned long c, int flags)
{
    int 长度, n, no_oblock, i;
    int size1, align1;
    符号ST *s, *f;
    符号ST indexsym;
    类型ST *t1;

    /* 生成行号信息 */
    if (调试_模式 && !p->sec)
        zhi_debug_line(全局虚拟机), zhi_tcov_check_line (1);

    if (!(flags & DIF_HAVE_ELEM) && 单词编码 != 英_左大括号 &&
	/* 在字符串的情况下，我们对数组有特殊处理，所以不要将它们用作初始化值（这会将它们提交给某个匿名符号）。 */
	单词编码 != TOK_LSTR && 单词编码 != TOK_字符串常量 &&
	!(flags & DIF_SIZE_ONLY))
    {
		解析初始化元素(!p->sec ? EXPR_ANY : EXPR_CONST);
        flags |= DIF_HAVE_ELEM;
    }

    if ((flags & DIF_HAVE_ELEM) &&
	!(类型->数据类型 & 数据类型_数组) &&
	/* 使用i_c_parameter_t，剥离顶层限定符。 源类型可能设置了 存储类型_常量，这当然可以分配给非常量元素。  */
	是兼容的非限定类型(类型, &栈顶值->类型)) {
        goto 初始存储值;

    } else if (类型->数据类型 & 数据类型_数组)
    {
        no_oblock = 1;
        if (((flags & DIF_FIRST) && 单词编码 != TOK_LSTR && 单词编码 != TOK_字符串常量) || 单词编码 == 英_左大括号)
        {
            跳过(英_左大括号);
            no_oblock = 0;
        }

        s = 类型->引用符号;
        n = s->c;
        t1 = 返回指针类型(类型);
        size1 = 类型_大小(t1, &align1);

        /* 如果类型正确，则仅在此处解析字符串（否则：将它们处理为 ((w)char *) 表达式 */
        if ((单词编码 == TOK_LSTR && 
#ifdef 目标_PE
             (t1->数据类型 & 数据类型_基本类型) == 数据类型_短整数 && (t1->数据类型 & 存储类型_无符号的)
#else
             (t1->数据类型 & 数据类型_基本类型) == 数据类型_整数
#endif
            ) || (单词编码 == TOK_字符串常量 && (t1->数据类型 & 数据类型_基本类型) == 数据类型_字节)) {
	    长度 = 0;
            动态字符串_重置(&初始_字符串);
            if (size1 != (单词编码 == TOK_字符串常量 ? 1 : sizeof(nwchar_t)))
              错_误("未处理的字符串文字合并");
            while (单词编码 == TOK_字符串常量 || 单词编码 == TOK_LSTR) {
                if (初始_字符串.大小)
                  初始_字符串.大小 -= size1;
                if (单词编码 == TOK_字符串常量)
                  长度 += 单词常量.字符串.大小;
                else
                  长度 += 单词常量.字符串.大小 / sizeof(nwchar_t);
                长度--;
                动态字符串_拼接(&初始_字符串, 单词常量.字符串.数据, 单词常量.字符串.大小);
                取下个单词();
            }
            if (单词编码 != 英_右小括号 && 单词编码 != 英_右大括号 && 单词编码 != 英_逗号 && 单词编码 != 英_分号 && 单词编码 != TOK_文件结尾)
            {
                /* 不是一个单独的文字，而是一个更大的表达的一部分。  */
                退回当前单词(size1 == 1 ? TOK_字符串常量 : TOK_LSTR);
                单词常量.字符串.大小 = 初始_字符串.大小;
                单词常量.字符串.数据 = 初始_字符串.数据;
                goto do_init_array;
            }

            柔性声明符(p, s, 长度);
            if (!(flags & DIF_SIZE_ONLY)) {
                int nb = n;
                if (长度 < nb)
                    nb = 长度;
                if (长度 > nb)
                  zhi_警告("数组的初始值设定项字符串太长");
                /* 为了更快地处理常见情况（全局变量中的字符串，我们专门处理它 */
                if (p->sec && size1 == 1) {
                    初始化断言(p, c + nb);
                    if (!无需静态数据输出)
                      memcpy(p->sec->数据 + c, 初始_字符串.数据, nb);
                } else {
                    for(i=0;i<n;i++) {
                        if (i >= nb) {
                          /* 如果存储空间足够，只添加尾随零（在这种情况下没有警告，因为它是标准的） */
                          if (flags & DIF_CLEAR)
                            break;
                          if (n - i >= 4) {
                            变量初始化为0(p, c + i * size1, (n - i) * size1);
                            break;
                          }
                          当前字符 = 0;
                        } else if (size1 == 1)
                          当前字符 = ((unsigned char *)初始_字符串.数据)[i];
                        else
                          当前字符 = ((nwchar_t *)初始_字符串.数据)[i];
                        整数常量压入栈(当前字符);
                        初始存储值(p, t1, c + i * size1);
                    }
                }
            }
        } else {

          do_init_array:
	    indexsym.c = 0;
	    f = &indexsym;

          do_init_list:
            /* 提前一次清零 */
            if (!(flags & (DIF_CLEAR | DIF_SIZE_ONLY))) {
                变量初始化为0(p, c, n*size1);
                flags |= DIF_CLEAR;
            }

	    长度 = 0;
	    while (单词编码 != 英_右大括号 || (flags & DIF_HAVE_ELEM)) {
		长度 = 声明符(p, 类型, c, &f, flags, 长度);
		flags &= ~DIF_HAVE_ELEM;
		if (类型->数据类型 & 数据类型_数组) {
		    ++indexsym.c;
		    /* 多维数组的特殊测试（如果同时使用指示符，则可能不完全正确） */
		    if (no_oblock && 长度 >= n*size1)
		        break;
		} else {
		    if (s->类型.数据类型 == 值的数据类型_共用体)
		        f = NULL;
		    else
		        f = f->next;
		    if (no_oblock && f == NULL)
		        break;
		}

		if (单词编码 == 英_右大括号)
		    break;
		跳过(英_逗号);
	    }
        }
        if (!no_oblock)
            跳过(英_右大括号);
    } else if ((类型->数据类型 & 数据类型_基本类型) == 数据类型_结构体) {
        no_oblock = 1;
        if ((flags & DIF_FIRST) || 单词编码 == 英_左大括号) {
            跳过(英_左大括号);
            no_oblock = 0;
        }
        s = 类型->引用符号;
        f = s->next;
        n = s->c;
        size1 = 1;
	goto do_init_list;
    } else if (单词编码 == 英_左大括号)
    {
        if (flags & DIF_HAVE_ELEM)
          跳过(英_分号);
        取下个单词();
        声明初始化(p, 类型, c, flags & ~DIF_HAVE_ELEM);
        跳过(英_右大括号);
    } else if ((flags & DIF_SIZE_ONLY))
    {
	/* 如果我们只支持ISO C，那么如果DIF_SIZE_only（甚至只在最外层，所以不需要递归），我们就不必接受在数组之外的任何事情上调用它，因为不支持初始化flex数组成员。
	 * 但是GNU C支持它，所以当设置DIF_SIZE_ONLY时，我们甚至需要递归到结构和数组的子字段中。*//*仅跳转表达式 */
        跳过或保存块(NULL);
    } else {
	if (!(flags & DIF_HAVE_ELEM)) {
	    /* 只有当我们还没有解析上面的init元素时，才应该发生这种情况，因为我们担心过早地将字符串常量提交到内存中。 */
	    if (单词编码 != TOK_字符串常量 && 单词编码 != TOK_LSTR)
	      应为("字符串常量");
	    解析初始化元素(!p->sec ? EXPR_ANY : EXPR_CONST);
	}
    初始存储值:
        if (!p->sec && (flags & DIF_CLEAR) /* 容器已为零 */
            && (栈顶值->寄存qi & (存储类型_掩码 | 存储类型_左值 | 存储类型_符号)) == 存储类型_VC常量
            && 栈顶值->c.i == 0
            && 基本类型_大小(类型->数据类型 & 数据类型_基本类型) /* 不适用于fp常量 */
            )
            弹出堆栈值();
        else
            初始存储值(p, 类型, c);
    }
}

/* 如果 '初始哈希值' 非零，则解析类型为 '类型' 的初始化程序，并在本地或全局数据空间中分配空间（'r' 是 存储类型_局部 或
 * 存储类型_VC常量）。 如果“v”不为零，则在解析初始化之前声明域“域S”的关联变量“v”。 如果“v”为零，则对新对象的引用将放入值堆栈中。
 *  如果 '初始哈希值' 为 2，则进行特殊解析以处理字符串常量。 */
static void 声明符初始化值分配(类型ST *类型, GNUC属性ST *ad, int r,int 初始哈希值, int v, int 域S)
{
    int 大小, 对齐, addr;
    单词字符串ST *init_str = NULL;

    节ST *sec;
    符号ST *柔性的_array;
    符号ST *sym;
    int 保存_无需代码_生成 = 无需生成代码;
#ifdef 启用边界检查M
    int bcheck = 全局虚拟机->使用_边界_检查器 && !无需静态数据输出;
#endif
    初始参数S p = {0};
    /* 始终分配静态或全局变量 */
    if (v && (r & 存储类型_掩码) == 存储类型_VC常量)
    {
    	无需生成代码 |= 0x80000000;
    }

    柔性的_array = NULL;
    大小 = 类型_大小(类型, &对齐);
    /* 只能初始化一个可变的数组，即顶层数组或顶层结构的最后一个成员 */
    if (大小 < 0)
    {
        /* 如果基类型本身是未指定大小的数组类型（如“typedef int arr[]；arr x=｛1｝；”），则我们将用此decl的实数覆盖未知大小。我们需要取消共享保持该大小的引用符号符号。 */
        类型->引用符号 = 将符号放入符号栈(符号_字段, &类型->引用符号->类型, 0, 类型->引用符号->c);
        p.弹性_数组_参考 = 类型->引用符号;

    } else if (初始哈希值 && (类型->数据类型 & 数据类型_基本类型) == 数据类型_结构体)
    {
        符号ST *field = 类型->引用符号->next;
        if (field) {
            while (field->next)
                field = field->next;
            if (field->类型.数据类型 & 数据类型_数组 && field->类型.引用符号->c < 0)
            {
                柔性的_array = field;
                p.弹性_数组_参考 = field->类型.引用符号;
                大小 = -1;
            }
        }
    }

    if (大小 < 0)
    {
        /* 如果大小未知，请进行第一次试运行 */
        if (!初始哈希值)
        {
        	错_误("未知 类型 大小");
        }
        if (初始哈希值 == 2)
        {
            /* 只获取字符串 */
            init_str = 单词字符串_分配();
            while (单词编码 == TOK_字符串常量 || 单词编码 == TOK_LSTR)
            {
                在单词字符串s中添加当前解析单词(init_str);
                取下个单词();
            }
            单词字符串_增加(init_str, -1);
            单词字符串_增加(init_str, 0);
        } else
        {
        	跳过或保存块(&init_str);
        }
        退回当前单词(0);
        /* 计算大小 */
        开始_宏扩展(init_str, 1);
        取下个单词();
        声明初始化(&p, 类型, 0, DIF_FIRST | DIF_SIZE_ONLY);
        /* 准备第二个初始值设定项解析 */
        没宏替换的字符串指针 = init_str->字符串;
        取下个单词();
        /* 如果大小仍然未知，则出错 */
        大小 = 类型_大小(类型, &对齐);
        if (大小 < 0)
        {
        	错_误("未知 类型 大小");
        }
        /* 如果有一个柔性成员，并且它在初始值设定项中使用，请调整大小。  */
        if (柔性的_array && 柔性的_array->类型.引用符号->c > 0)
        {
        	大小 += 柔性的_array->类型.引用符号->c * pointed_size(&柔性的_array->类型);
        }
    }
    /* 如果较大，则考虑指定的对齐方式 */
    if (ad->a.aligned)
    {
		int speca = 1 << (ad->a.aligned - 1);
        if (speca > 对齐)
        {
        	对齐 = speca;
        }
    } else if (ad->a.packed)
    {
        对齐 = 1;
    }
    if (!v && 无需静态数据输出)
    {
    	大小 = 0, 对齐 = 1;
    }
    if ((r & 存储类型_掩码) == 存储类型_局部)
    {
        sec = NULL;
#ifdef 启用边界检查M
        if (bcheck && v)
        {
            /* 在绑定检查的堆栈变量之间添加填充 */
            局部变量索引 -= 对齐;
        }
#endif
        局部变量索引 = (局部变量索引 - 大小) & -对齐;
        addr = 局部变量索引;
        p.局部_偏移量 = addr + 大小;
#ifdef 启用边界检查M
        if (bcheck && v)
        {
            /* 在绑定检查的堆栈变量之间添加填充 */
            局部变量索引 -= 对齐;
        }
#endif
        if (v)
        {
            /* 局部变量 */
#ifdef CONFIG_ZHI_ASM
			if (ad->asm_label)
			{
				int reg = asm_parse_regvar(ad->asm_label);
				if (reg >= 0)
					r = (r & ~存储类型_掩码) | reg;
			}
#endif
            sym = 将符号放入符号栈(v, 类型, r, addr);
			if (ad->清理_函数)
			{
				符号ST *cls = 将符号放入符号栈2(&所有_清理,符号_字段 | ++当前域->cl.n, 0, 0);
				cls->定义的前一个同名符号 = sym;
				cls->next = ad->清理_函数;
				cls->ncl = 当前域->cl.s;
				当前域->cl.s = cls;
			}
            sym->a = ad->a;
        } else
        {
            /* 推送局部引用 */
            值压入栈(类型, r, addr);
        }
    } else
    {
		sym = NULL;
        if (v && 域S == 存储类型_VC常量)
        {
            /* 查看符号是否已定义 */
            sym = 查找标识符(v);
            if (sym)
            {
                if (p.弹性_数组_参考 && (sym->类型.数据类型 & 类型->数据类型 & 数据类型_数组)
                    && sym->类型.引用符号->c > 类型->引用符号->c)
                {
                    /* 柔性数组已经用显式大小extern int arr[10]声明； int arr[]=｛1,2,3｝；*/
                    类型->引用符号->c = sym->类型.引用符号->c;
                    大小 = 类型_大小(类型, &对齐);
                }
                patch_storage(sym, ad, 类型);
                /* 我们接受同一全局变量的几个定义。*/
                if (!初始哈希值 && sym->c && elfsym(sym)->st_shndx != SHN_未定义)
                    goto no_alloc;
            }
        }
        /* 在相应部分中分配符号 */
        sec = ad->section;
        if (!sec)
        {
            类型ST *tp = 类型;
            while ((tp->数据类型 & (数据类型_基本类型|数据类型_数组)) == (数据类型_指针|数据类型_数组))
                tp = &tp->引用符号->类型;
            if (tp->数据类型 & 存储类型_常量)
            {
				sec = 只读数据_节;
            } else if (初始哈希值)
            {
				sec = 数据_节;
                /*if (全局虚拟机->g_debug & 4)
                    zhi_警告("rw 数据: %s", 取单词字符串(v, 0));*/
            } else if (全局虚拟机->bss不使用通用符号)
                sec = 未初始化或初始化为0的数据_节;
        }
        if (sec)
        {
			addr = 节_预留指定大小内存且按照align对齐(sec, 大小, 对齐);
#ifdef 启用边界检查M
            /* 如果绑定则添加填充检查 */
            if (bcheck)
                节_预留指定大小内存且按照align对齐(sec, 1, 1);
#endif
        } else
        {
            addr = 对齐; /* SHN_COMMON是特殊的，符号值是对齐的 */
			sec = 完全未初始化数据_节;
        }

        if (v)
        {
            if (!sym)
            {
                sym = 将符号放入符号栈(v, 类型, r | 存储类型_符号, 0);
                patch_storage(sym, ad, NULL);
            }
            /* 更新符号定义 */
			取外部符号(sym, sec, addr, 大小);
        } else
        {
            /* 推送全局引用 */
            添加虚拟符号推送对节偏移的引用(类型, sec, addr, 大小);
            sym = 栈顶值->sym;
			栈顶值->寄存qi |= r;
        }

#ifdef 启用边界检查M
        /* 现在处理边界，因为必须在重新定位之前定义符号 */
        if (bcheck)
        {
            addr_t *bounds_ptr;
            添加新的重定位条目(全局边界_节, sym, 全局边界_节->当前数据_偏移量, R_DATA_PTR, 0);
            /* 然后添加全局绑定信息 */
            bounds_ptr = 节_预留指定大小内存(全局边界_节, 2 * sizeof(addr_t));
            bounds_ptr[0] = 0; /* 重定位 */
            bounds_ptr[1] = 大小;
        }
#endif
    }

    if (类型->数据类型 & 存储类型_变长数组)
    {
        int a;
        if (无需静态数据输出)
            goto no_alloc;
        /* 如果需要，保存 before-VLA 堆栈指针 */
        if (当前域->vla.num == 0)
        {
            if (当前域->上个 && 当前域->上个->vla.num)
            {
                当前域->vla.locorig = 当前域->上个->vla.局部变量索引;
            } else {
                gen_vla_sp_save(局部变量索引 -= 宏_指针大小);
                当前域->vla.locorig = 局部变量索引;
            }
        }
        vla_运行时类型大小(类型, &a);
        gen_vla_alloc(类型, a);
#if defined 目标_PE && defined 目标_X86_64
        /* 在_WIN64上，由于函数args的scratch area，alloca的结果与RSP不同，在RAX中返回。  */
        gen_vla_result(addr), addr = (局部变量索引 -= 宏_指针大小);
#endif
        gen_vla_sp_save(addr);
        当前域->vla.局部变量索引 = addr;
        当前域->vla.num++;
    } else if (初始哈希值)
    {
        p.sec = sec;
        声明初始化(&p, 类型, addr, DIF_FIRST);
        /* 将灵活的数组成员大小修补回 -1， */
        /* 对于可能的后续类似声明 */
        if (柔性的_array)
        {
        	柔性的_array->类型.引用符号->c = -1;
        }
    }
 no_alloc:
    /* 如果需要，恢复解析状态 */
    if (init_str)
    {
        结束_宏扩展();
        取下个单词();
    }

    无需生成代码 = 保存_无需代码_生成;
}

/* 解析由符号“sym”定义的函数并在“当前代码节”中生成其代码 */
static void 函数体(符号ST *sym)
{
    struct 域S f = { 0 };
    当前域 = 根域 = &f;
    无需生成代码 = 0;
    指令在代码节位置 = 当前代码节->当前数据_偏移量;
    if (sym->a.aligned)
    {
		size_t newoff = 节_预留指定大小内存且按照align对齐(当前代码节, 0,1 << (sym->a.aligned - 1));
		gen_fill_nops(newoff - 指令在代码节位置);
    }
    /* NOTE: 我们稍后修补符号大小 */
    取外部符号(sym, 当前代码节, 指令在代码节位置, 0);
    if (sym->类型.引用符号->f.函数_构造函数)
    {
    	add_array (全局虚拟机, ".init_array", sym->c);
    }
    if (sym->类型.引用符号->f.函数_析构函数)
    {
    	add_array (全局虚拟机, ".fini_array", sym->c);
    }
    函数名 = 取单词字符串(sym->v, NULL);
    函数_索引 = 指令在代码节位置;
    函数_返回类型 = sym->类型.引用符号->类型;
    函数_可变参 = sym->类型.引用符号->f.函数_类型== 函数原型_省略;
    取函数符号(全局虚拟机, sym);
    /* 推送一个虚拟符号以启用本地符号存储 */
    将符号放入符号栈2(&局部符号栈, 符号_字段, 0, 0);
    局部_范围 = 1; /* 对于函数参数 */
    生成函数开头代码(sym);
    局部_范围 = 0;
    返回符号 = 0;
    清除_临时_局部_变量列表();
    块(0);
    生成符号(返回符号);
    无需生成代码 = 0;
    /* 重置局部栈 */
    弹出局部符号(NULL, 0);
    生成函数结尾代码();
    当前代码节->当前数据_偏移量 = 指令在代码节位置;
    局部_范围 = 0;
    弹出标签(&全局_标签_栈, NULL, 0);
    符号弹出(&所有_清理, NULL, 0);
    /* 修补符号大小 */
    elfsym(sym)->st_size = 指令在代码节位置 - 函数_索引;
    函数结束(全局虚拟机, 指令在代码节位置 - 函数_索引);
    /* 崩溃总比生成错误代码好 */
    当前代码节 = NULL;
    函数名 = ""; /* for safety */
    函数_返回类型.数据类型 = 数据类型_无类型; /* for safety */
    函数_可变参 = 0; /* for safety */
    指令在代码节位置 = 0; /* for safety */
    无需生成代码 = 0x80000000;
    检查栈值();
    /* 在函数结束调试信息后执行此操作 */
    取下个单词();
}

static void 生成内联函数(虚拟机ST *s)
{
    符号ST *sym;
    int 内联_生成, i;
    struct 内联函数S *fn;
    新建源文件缓冲区(s, ":inline:", 0);
    /* 在引用内联函数时进行迭代*/
    do {
        内联_生成 = 0;
        for (i = 0; i < s->内联函数数量; ++i)
        {
            fn = s->内联函数数组[i];
            sym = fn->sym;
            if (sym && (sym->c || !(sym->类型.数据类型 & 存储类型_内联)))
            {
                /* 该函数被使用或强制（然后不是内部）：生成其代码并将其转换为普通函数 */
                fn->sym = NULL;
                zhi_debug_putfile(s, fn->文件名);
                开始_宏扩展(fn->函数体字符串, 1);
                取下个单词();
                当前代码节 = 代码_节;
                函数体(sym);
                结束_宏扩展();
                内联_生成 = 1;
            }
        }
    } while (内联_生成);
    关闭打开的文件();
}
/* 释放未使用的内联函数的单词 */
static void 释放内联函数(虚拟机ST *s)
{
    int i;
    for (i = 0; i < s->内联函数数量; ++i)
    {
        struct 内联函数S *fn = s->内联函数数组[i];
        if (fn->sym)
            单词字符串_释放(fn->函数体字符串);
    }
    动态数组_重置(&s->内联函数数组, &s->内联函数数量);
}

/* 功能：外部声明（变量声明，变量定义，函数声明，函数定义等统称为“外部声明”）
 * l:存储类型，是VT_LOCAL或VT_CONST以定义默认存储类型，如果是解析旧样式参数decl列表（然后设置FUNC_SYM），则为VT_CMP*/
static int 外部声明(int 存储的类型, int 是_for_循环_初始化, 符号ST *函数符号)
{
	/* C语言语句类型：
	 * extern int         a;        //带有修饰符的。如：static,const等
	 * int                b;
	 * int                c = 10;
	 * int                d,e;
	 * long long          d,e;
	 * static long long   d,e;
	 * char               *f;
	 * int                array[]
	 * struct             g;
	 * struct             h{int a;char *d};
	 * typedef struct     h{};
	 * int                i(int a,int b);
	 * int                j(int a,int b){int a =1;a=a+1;}
	 * struct name        i(int a,int b);
	 * */
    int v, 初始哈希值, r, 旧整数;
    类型ST 类型, 获取的数据类型;
    符号ST *sym;
    GNUC属性ST ad, 获取的属性;

    while (1)/*一直到结束就此一个循环语句：循环一次解析一个语句*/
    {
    	/*1.静态断言（又叫编译期断言）：
    	 * const int i = 22;
    	 * static_assert(i != 22, "i equals to 22");
    	 * */
		if (单词编码 == 英_静态_断言 || 单词编码 == 中_静态_断言)
		{
			动态字符串ST 错误提示字符串;
			int 常量表达式的值;
			取下个单词();
			跳过(英_左小括号);
			常量表达式的值 = 常量表达式解析();
			if (单词编码 == 英_右小括号)
			{
				if (!常量表达式的值)
				{
					错_误("_静态_断言 失败");
				}

				取下个单词();
				goto 静态断言_出;
			}
			跳过(英_逗号);
			解析多个字符串(&错误提示字符串, "字符串常量");
			if (常量表达式的值 == 0)
			{
				错_误("%s", (char *)错误提示字符串.数据);
			}

			动态字符串_释放(&错误提示字符串);
			跳过(英_右小括号);
		  静态断言_出:
				跳过(英_分号);
			continue;
		}
        旧整数 = 0;
        /*2.声明说明符*/
        if (!解析声明说明符(&获取的数据类型, &获取的属性))  /*声明说明符分为5类：1.存储类说明符；2.类型说明符；3.类型限定符；4.函数说明符；5.属性*/
        {
            if (是_for_循环_初始化)
            {
            	return 0;
            }

            /* 跳过多余的“英_分号” 如果不在旧参数声明范围内 */
            if (单词编码 == 英_分号 && 存储的类型 != 存储类型_标志寄存器)
            {
                取下个单词();
                continue;
            }
            if (存储的类型 != 存储类型_VC常量)
            {
            	break;
            }

            if (单词编码 == TOK_ASM1 || 单词编码 == TOK_ASM2 || 单词编码 == TOK_ASM3)
            {
                /* 全局 asm 块 */
                asm_全局_instr();
                continue;
            }
            if (单词编码 >= TOK_UIDENT)
            {
               /* 对没有显式 int 类型的旧 K&R 原型的特殊测试。 仅在定义全局数据时接受 */
                获取的数据类型.数据类型 = 数据类型_整数;
                旧整数 = 1;
            } else
            {
                if (单词编码 != TOK_文件结尾)
                {
                	应为("外部声明");
                }
                break;
            }
        }
        /*3.解析匿名结构体共用体和枚举*/
        if (单词编码 == 英_分号)
        {
			if ((获取的数据类型.数据类型 & 数据类型_基本类型) == 数据类型_结构体)
			{
				v = 获取的数据类型.引用符号->v;
				if (!(v & 符号_字段) && (v & ~符号_结构体) >= 第一个匿名符号)
				{
					zhi_警告("定义的匿名struct/union 没有实例");
				}
				取下个单词();
				continue;
			}
            if (是_枚举(获取的数据类型.数据类型))
            {
                取下个单词();
                continue;
            }
        }

        while (1)/*一直到本函数结束*/
        { /* 4.开始解析声明符。*/
            类型 = 获取的数据类型;
	        ad = 获取的属性;
            解析初始声明符列表(&类型, &ad, &v, 声明符_直接);/*直接声明符，指针声明符，数组，函数，结构体等等。*/
            /*4.解析函数*/
            if ((类型.数据类型 & 数据类型_基本类型) == 数据类型_函数)
            {
                if ((类型.数据类型 & 存储类型_静态) && (存储的类型 == 存储类型_局部))
                {
                	错_误("没有文件作用域的函数不能是静态的");
                }
                /* 如果旧式函数原型，我们接受声明列表 */
                sym = 类型.引用符号;
                if (sym->f.函数_类型== 函数原型_旧 && 存储的类型 == 存储类型_VC常量)
                {
                	外部声明(存储类型_标志寄存器, 0, sym);
                }
                /* 总是编译 'extern inline' */
                if (类型.数据类型 & 存储类型_外部)
                {
                	类型.数据类型 &= ~存储类型_内联;
                }
            } else if (旧整数)
            {
                zhi_警告("类型默认为 int");
            }
            /*5.汇编*/
            if (GNU_C_扩展 && (单词编码 == TOK_ASM1 || 单词编码 == TOK_ASM2 || 单词编码 == TOK_ASM3))
            {
                ad.asm_label = asm_label_instr();
                /* 解析最后一个属性列表，在 asm 标签之后 */
                解析属性(&ad);
            }

#ifdef 目标_PE
            /*6.动态链接库导入导出*/
            if (ad.a.dllimport || ad.a.dllexport)
            {
                if (类型.数据类型 & 存储类型_静态)
                {
                	错_误("不能用静态的方式链接动态链接库DLL");
                }
                if (类型.数据类型 & 存储类型_别名)
                {
                    zhi_警告("已忽略typedef的'%s' 属性",ad.a.dllimport ? (ad.a.dllimport = 0, "dllimport") :(ad.a.dllexport = 0, "dllexport"));
                } else if (ad.a.dllimport)
                {
                    if ((类型.数据类型 & 数据类型_基本类型) == 数据类型_函数)
                    {
                    	ad.a.dllimport = 0;
                    }else
                    {
                    	类型.数据类型 |= 存储类型_外部;
                    }
                }
            }
#endif
            /*7.解析函数定义*/
            if (单词编码 == 英_左大括号) /*函数定义。左大括号★☆★★☆★★☆★★☆★★☆★★☆★☆★★☆★★☆★★☆★★☆★★☆*/
            {
                if (存储的类型 != 存储类型_VC常量)
                {
                	错_误("不支持函数嵌套定义");
                }

                if ((类型.数据类型 & 数据类型_基本类型) != 数据类型_函数)
                {
                	应为("函数定义");
                }
                /* 拒绝函数定义中的抽象声明符使没有 decl 的旧式参数具有 int 类型 */
                sym = 类型.引用符号;
                while ((sym = sym->next) != NULL)
                {
                    if (!(sym->v & ~符号_字段))
                    {
                    	应为("标识符");
                    }
                    if (sym->类型.数据类型 == 数据类型_无类型)
                    {
                    	sym->类型 = 整数_类型;
                    }
                }
                /* 应用声明后属性*/
                合并_函数属性(&类型.引用符号->f, &ad.f);
                /* 放置函数符号 */
                类型.数据类型 &= ~存储类型_外部;
                sym = 外部_符号(v, &类型, 0, &ad);
                /* 静态内联函数只是被记录为一种宏。 只有在使用它们时，它们的代码才会在编译单元的末尾发出 */
                if (sym->类型.数据类型 & 存储类型_内联)
                {
                    struct 内联函数S *fn;
                    fn = zhi_分配内存(sizeof *fn + strlen(文件->文件名));
                    strcpy(fn->文件名, 文件->文件名);
                    fn->sym = sym;
		            跳过或保存块(&fn->函数体字符串);
                    动态数组_追加元素(&全局虚拟机->内联函数数组,&全局虚拟机->内联函数数量, fn);
                } else
                {
                    /* 计算机代码节 */
                    当前代码节 = ad.section;
                    if (!当前代码节)
                    {
                    	当前代码节 = 代码_节;
                    }
                    函数体(sym);
                }
                break;
            } else
            {/*以下为除函数定义之外的其他声明：变量声明，函数声明*/
				if (存储的类型 == 存储类型_标志寄存器)
				{
					/* 在函数参数列表中查找参数 */
					for (sym = 函数符号->next; sym; sym = sym->next)
					{
						if ((sym->v & ~符号_字段) == v)
						{
							goto found;
						}
					}
					错_误("声明了参数 '%s'，但是定义中没有此参数",取单词字符串(v, NULL));
		found:
					if (类型.数据类型 & 值的数据类型_贮存) /* 'register'没问题 */
					{
						错_误("为'%s'指定的存储类",取单词字符串(v, NULL));
					}
					if (sym->类型.数据类型 != 数据类型_无类型)
					{
						错_误("参数'%s'的重复定义",取单词字符串(v, NULL));
					}
					转换参数类型(&类型);
					sym->类型 = 类型;
				} else if (类型.数据类型 & 存储类型_别名)
				{/*别名声明★☆★★☆★★☆★★☆★★☆★★☆★☆★★☆★★☆★★☆★★☆★★☆★☆★★☆★★☆★★☆★★☆★★☆*/
					sym = 查找标识符(v);
					if (sym && sym->作用域范围 == 局部_范围)
					{
						if (!是兼容类型(&sym->类型, &类型) || !(sym->类型.数据类型 & 存储类型_别名))
						{
							错_误("'%s'的重复定义不兼容",取单词字符串(v, NULL));
						}
						sym->类型 = 类型;
					} else
					{
						sym = 将符号放入符号栈(v, &类型, 0, 0);
					}
					sym->a = ad.a;
					sym->f = ad.f;
					if (调试_模式)
					{
						zhi_debug_typedef (全局虚拟机, sym);
					}

				} else if ((类型.数据类型 & 数据类型_基本类型) == 数据类型_无类型 && !(类型.数据类型 & 存储类型_外部))
				{
					错_误("无效对象声明");
				} else
				{/*函数声明★☆★★☆★★☆★★☆★★☆★★☆★☆★★☆★★☆★★☆★★☆★★☆★☆★★☆★★☆★★☆★★☆★★☆*/
					r = 0;
					if ((类型.数据类型 & 数据类型_基本类型) == 数据类型_函数)
					{
						类型.引用符号->f = ad.f;
					} else if (!(类型.数据类型 & 数据类型_数组))
					{
						/* 如果数组不是左值 */
						r |= 存储类型_左值;
					}
					初始哈希值 = (单词编码 == '=');
					if (初始哈希值 && (类型.数据类型 & 存储类型_变长数组))
					{
						错_误("变长数组无法初始化");
					}
					/* 与 GCC 一样，没有大小的未初始化全局数组被认为是外部的：*/
					if (((类型.数据类型 & 存储类型_外部) && (!初始哈希值 || 存储的类型 != 存储类型_VC常量)) || (类型.数据类型 & 数据类型_基本类型) == 数据类型_函数 || ((类型.数据类型 & 数据类型_数组) && !初始哈希值 && 存储的类型 == 存储类型_VC常量 && 类型.引用符号->c < 0) )
					{
						/* 外部变量或函数 */
						类型.数据类型 |= 存储类型_外部;
						sym = 外部_符号(v, &类型, r, &ad);
						if (ad.别名_目标)
						{
							/* 别名需要在其目标符号被发出时发出，即使可能未被引用。
							 * 我们只支持基类已经定义的情况，否则我们需要推迟到编译单元结束才发出别名。  */
							符号ST *别名_目标 = 查找标识符(ad.别名_目标);
							ElfSym *esym = elfsym(别名_目标);
							if (!esym)
							{
								错_误("不支持 __alias__ 的前向属性");
							}
							取外部符号2(sym, esym->st_shndx,esym->st_value, esym->st_size, 1);
						}
					} else /*直接声明符赋值*/
					{
						if (类型.数据类型 & 存储类型_静态)
						{
							r |= 存储类型_VC常量;
						}else
						{
							r |= 存储的类型;
						}
						if (初始哈希值)
						{
							取下个单词();
						}else if (存储的类型 == 存储类型_VC常量)
						{
							类型.数据类型 |= 存储类型_外部;/* 未初始化的全局变量可能会被覆盖 */
						}
						声明符初始化值分配(&类型, &ad, r, 初始哈希值, v, 存储的类型);
					}
				}
				if (单词编码 != 英_逗号)
				{
					if (是_for_循环_初始化)
					{
						return 1;
					}
					跳过(英_分号);
					break;
				}
				取下个单词();
            }
        }
    }
    return 0;
}
/* ------------------------------------------------------------------------- */
#undef 生成跳转_到固定地址
#undef 生成到标签的跳转
/* ------------------------------------------------------------------------- */
