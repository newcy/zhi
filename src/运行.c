/****************************************************************************************************
 * 名称：-run的支持
 * Copyright (c) 2001-204 Fabrice Bellard
 ****************************************************************************************************/
#include "zhi.h"

/* 仅本编译器支持 -run */
#ifdef 运行脚本M

#ifdef 启用内置堆栈回溯M
typedef struct 运行时上下文S
{
    /* --> Elf.c:zhi_add_btstub 需要以下顺序：*/
    字符串表符号ST *stab_sym, *stab_sym_end;
    char *stab_str;
    ElfW(Sym) *esym_start, *esym_end;
    char *elf_str;
    addr_t prog_base;
    void *bounds_start;
    struct 运行时上下文S *next;
    /* <-- */
    int num_callers;
    addr_t ip, fp, sp;
    void *top_func;
    jmp_buf jmp_buf;
    char do_jmp;
} 运行时上下文ST;

static 运行时上下文ST g_rtctxt;
static void 设置异常处理(void);
static int _rt_error(void *fp, void *ip, const char *fmt, va_list ap);
static void rt_exit(int code);
#endif /* 启用内置堆栈回溯M */

/* 在lib/bt exe.c中包含时定义 */
#ifndef CONFIG_ZHI_BACKTRACE_ONLY

#ifndef _WIN32
# include <sys/mman.h>
#endif

static void set_pages_executable(虚拟机ST *s1, int mode, void *ptr, unsigned long length);
static int zhi_relocate_ex(虚拟机ST *s1, void *ptr, addr_t ptr_diff);

#ifdef _WIN64
static void *win64_add_function_table(虚拟机ST *s1);
static void win64_del_function_table(void *);
#endif

/* ------------------------------------------------------------- */
/* 执行所有重定位（在使用 取符号值() 之前需要） 出错时返回 -1。 */

核心库接口 int zhi_重定位(虚拟机ST *s1, void *ptr)
{
    int 大小;
    addr_t ptr_diff = 0;

    if (自动_重定位 != ptr)
        return zhi_relocate_ex(s1, ptr, 0);

    大小 = zhi_relocate_ex(s1, NULL, 0);
    if (大小 < 0)
        return -1;

#ifdef HAVE_SELINUX
{
    /* Using mmap instead of malloc */
    void *prx;
    char tmpfname[] = "/tmp/.zhirunXXXXXX";
    int 文件描述 = mkstemp(tmpfname);
    unlink(tmpfname);
    ftruncate(文件描述, 大小);

    大小 = (大小 + (页大小M-1)) & ~(页大小M-1);
    ptr = mmap(NULL, 大小 * 2, PROT_READ|PROT_WRITE, MAP_SHARED, 文件描述, 0);
    /* mmap RX memory at a fixed distance */
    prx = mmap((char*)ptr + 大小, 大小, PROT_READ|PROT_EXEC, MAP_SHARED|MAP_FIXED, 文件描述, 0);
    if (ptr == MAP_FAILED || prx == MAP_FAILED)
	错_误("zhirun: 无法映射内存");
    ptr_diff = (char*)prx - (char*)ptr;
    close(文件描述);
    //printf("map %p %p %p\n", ptr, prx, (void*)ptr_diff);
}
#else
    ptr = zhi_分配内存(大小);
#endif
    zhi_relocate_ex(s1, ptr, ptr_diff); /* no more errors expected */
    动态数组_追加元素(&s1->运行时_内存, &s1->运行时_内存_数量, (void*)(addr_t)大小);
    动态数组_追加元素(&s1->运行时_内存, &s1->运行时_内存_数量, ptr);
    return 0;
}

静态函数 void zhi_run_free(虚拟机ST *s1)
{
    int i;

    for (i = 0; i < s1->运行时_内存_数量; i += 2) {
        unsigned 大小 = (unsigned)(addr_t)s1->运行时_内存[i];
        void *ptr = s1->运行时_内存[i+1];
#ifdef HAVE_SELINUX
        munmap(ptr, 大小 * 2);
#else
        /* unprotect memory to make it usable for malloc again */
        set_pages_executable(s1, 2, ptr, 大小);
#ifdef _WIN64
        win64_del_function_table(*(void**)ptr);
#endif
        zhi_释放(ptr);
#endif
    }
    zhi_释放(s1->运行时_内存);
}

static void 运行光盘(虚拟机ST *s1, const char *start, const char *end,int argc, char **argv, char **envp)
{
    void **a = (void **)获取符号地址(s1, start, 0, 0);
    void **b = (void **)获取符号地址(s1, end, 0, 0);
    while (a != b)
    {
    	((void(*)(int, char **, char **))*a++)(argc, argv, envp);
    }
}

/* 使用给定的参数启动编译的程序， 链接并运行 main() 函数并返回其值。 之前不要调用 zhi_重定位()。*/
核心库接口 int 运行脚本(虚拟机ST *s1, int argc, char **argv)
{
	void *主函数地址, *main地址 ;
    int (*主函数的指针)(int, char **, char **), ret;
#ifdef 启用内置堆栈回溯M
    运行时上下文ST *rc = &g_rtctxt;
#endif
    char **envp = environ;
    主函数地址 = 取符号值(s1, "开始");
    main地址 = 取符号值(s1, "main");

    if(主函数地址 != NULL)
    {
    	s1->运行时_主函数 = s1->不添加标准库 ? "_start" : "开始";

    }else
    {
    	s1->运行时_主函数 = s1->不添加标准库 ? "_start" : "main";
    }

    if ((s1->dflag & 16) && (addr_t)-1 == 获取符号地址(s1, s1->运行时_主函数, 0, 1))
    {
    	return 0;
    }
#ifdef 启用内置堆栈回溯M
    if (s1->做_调试)
    {
    	zhi_添加_符号(s1, "exit", rt_exit);
    }
#endif
    if (zhi_重定位(s1, 自动_重定位) < 0)
    {
    	return -1;
    }
    主函数的指针 = (void*)获取符号地址(s1, s1->运行时_主函数, 1, 1);

#ifdef 启用内置堆栈回溯M
    memset(rc, 0, sizeof *rc);
    if (s1->做_调试)
    {
        void *p;
        rc->stab_sym = (字符串表符号ST *)调试节_数组->数据;
        rc->stab_sym_end = (字符串表符号ST *)(调试节_数组->数据 + 调试节_数组->当前数据_偏移量);
        rc->stab_str = (char *)调试节_数组->link->数据;
        rc->esym_start = (ElfW(Sym) *)(节符号表_数组->数据);
        rc->esym_end = (ElfW(Sym) *)(节符号表_数组->数据 + 节符号表_数组->当前数据_偏移量);
        rc->elf_str = (char *)节符号表_数组->link->数据;
#if 宏_指针大小 == 8
        rc->prog_base = 代码_节->sh_addr & 0xffffffff00000000ULL;
#endif

        if(主函数地址 != NULL)
        {
        	rc->top_func = 主函数地址;

        }else
        {
        	rc->top_func = main地址;
        }

        rc->num_callers = s1->返回调用数量;
        rc->do_jmp = 1;
        if ((p = 取符号值(s1, "__rt_error")))
            *(void**)p = _rt_error;
#ifdef 启用边界检查M
        if (s1->使用_边界_检查器)
        {
            rc->bounds_start = (void*)全局边界_节->sh_addr;
            if ((p = 取符号值(s1, "__bound_init")))
            {
            	((void(*)(void*,int))p)(rc->bounds_start, 1);
            }
        }
#endif
        设置异常处理();
    }
#endif

    errno = 0; /* 清除 errno 的值 */
    fflush(stdout);
    fflush(stderr);
    /* 这些不是C符号，因此不需要前导下划线处理。  */
    运行光盘(s1, "__init_array_start", "__init_array_end", argc, argv, envp);
#ifdef 启用内置堆栈回溯M
    if (!rc->do_jmp || !(ret = setjmp(rc->jmp_buf)))
#endif
    {
        ret = 主函数的指针(argc, argv, envp);
    }
    运行光盘(s1, "__fini_array_start", "__fini_array_end", 0, NULL, NULL);
    if ((s1->dflag & 16) && ret)
    {
    	fprintf(s1->标准的输出文件, "[returns %d]\n", ret), fflush(s1->标准的输出文件);
    }
    return ret;
}

/* 启用 rx/ro/rw 权限 */
#define CONFIG_RUNMEM_RO 1

#if CONFIG_RUNMEM_RO
# define PAGE_ALIGN 页大小M
#elif defined 目标_I386 || defined 目标_X86_64
/* 为避免 x86 处理器每次写入数据时都会重新加载缓存的指令，我们需要确保代码和数据不会共享相同的 64 字节单元 */
# define PAGE_ALIGN 64
#else
# define PAGE_ALIGN 1
#endif

/* 重定位代码。 出错时返回 -1，如果 ptr 为 NULL，则需要大小，否则将代码复制到调用者传递的缓冲区中 */
static int zhi_relocate_ex(虚拟机ST *s1, void *ptr, addr_t ptr_diff)
{
    节ST *s;
    unsigned offset, length, 对齐, max_align, i, k, f;
    unsigned n, copy;
    addr_t mem, addr;

    if (NULL == ptr)
    {
        s1->错误_数量 = 0;
#ifdef 目标_PE
        生成PE文件(s1, NULL);
#else
        zhi_添加运行时库(s1);
	解析_公共_符号(s1);
        build_got_entries(s1);
#endif
        if (s1->错误_数量)
            return -1;
    }

    offset = max_align = 0, mem = (addr_t)ptr;
#ifdef _WIN64
    offset += sizeof (void*); /* space for function_table pointer */
#endif
    copy = 0;
重做:
    for (k = 0; k < 3; ++k) { /* 0:rx, 1:ro, 2:rw 节数组 */
        n = 0; addr = 0;
        for(i = 1; i < s1->节的数量; i++) {
            static const char shf[] = {
                SHF_执行时占用内存|SHF_可执行, SHF_执行时占用内存, SHF_执行时占用内存|SHF_可写
                };
            s = s1->节数组[i];
            if (shf[k] != (s->sh_flags & (SHF_执行时占用内存|SHF_可写|SHF_可执行)))
                continue;
            length = s->当前数据_偏移量;
            if (copy) {
                if (addr == 0)
                    addr = s->sh_addr;
                n = (s->sh_addr - addr) + length;
                ptr = (void*)s->sh_addr;
                if (k == 0)
                    ptr = (void*)(s->sh_addr - ptr_diff);
                if (NULL == s->数据 || s->sh_type == SHT_无数据)
                    memset(ptr, 0, length);
                else
                    memcpy(ptr, s->数据, length);
#ifdef _WIN64
                if (s == s1->uw_pdata)
                    *(void**)mem = win64_add_function_table(s1);
#endif
                if (s->数据) {
                    zhi_释放(s->数据);
                    s->数据 = NULL;
                    s->data_allocated = 0;
                }
                s->当前数据_偏移量 = 0;
                continue;
            }
            对齐 = s->sh_地址对齐 - 1;
            if (++n == 1 && 对齐 < (PAGE_ALIGN - 1))
                对齐 = (PAGE_ALIGN - 1);
            if (max_align < 对齐)
                max_align = 对齐;
            addr = k ? mem : mem + ptr_diff;
            offset += -(addr + offset) & 对齐;
            s->sh_addr = mem ? addr + offset : 0;
            offset += length;
        }
        if (copy) { /* set permissions */
            if (k == 0 && ptr_diff)
                continue; /* not with HAVE_SELINUX */
            f = k;
#if !CONFIG_RUNMEM_RO
            if (f != 0)
                continue;
            f = 3; /* change only SHF_可执行 to rwx */
#endif
            if (n)
                set_pages_executable(s1, f, (void*)addr, n);
        }
    }

    if (copy)
        return 0;

    /* 重新定位 symbols */
    重定位符号地址(s1, s1->符号表节, !(s1->不添加标准库));
    if (s1->错误_数量)
        return -1;
    if (0 == mem)
        return offset + max_align;

#ifdef 目标_PE
    s1->pe_优先装载地址 = mem;
#endif

    /* 重新定位 节数组 */
#ifndef 目标_PE
    relocate_plt(s1);
#endif
    重定位_所有节(s1);
    copy = 1;
    goto 重做;
}

/* ------------------------------------------------------------- */
/* allow to run code in memory */

static void set_pages_executable(虚拟机ST *s1, int mode, void *ptr, unsigned long length)
{
#ifdef _WIN32
    static const unsigned char protect[] = {
        PAGE_EXECUTE_READ,
        PAGE_READONLY,
        PAGE_READWRITE,
        PAGE_EXECUTE_READWRITE
        };
    DWORD old;
    VirtualProtect(ptr, length, protect[mode], &old);
#else
    static const unsigned char protect[] = {
        PROT_READ | PROT_EXEC,
        PROT_READ,
        PROT_READ | PROT_WRITE,
        PROT_READ | PROT_WRITE | PROT_EXEC
        };
    addr_t start, end;
    start = (addr_t)ptr & ~(页大小M - 1);
    end = (addr_t)ptr + length;
    end = (end + 页大小M - 1) & ~(页大小M - 1);
    if (mprotect((void *)start, end - start, protect[mode]))
        错_误("mprotect failed: did you mean to 配置脚本 --with-selinux?");

/* XXX: BSD sometimes dump core with bad system call */
# if (目标_ARM && !目标系统_BSD) || 目标_ARM64
    if (mode == 0 || mode == 3) {
        void __clear_cache(void *beginning, void *end);
        __clear_cache(ptr, (char *)ptr + length);
    }
# endif

#endif
}

#ifdef _WIN64
static void *win64_add_function_table(虚拟机ST *s1)
{
    void *p = NULL;
    if (s1->uw_pdata) {
        p = (void*)s1->uw_pdata->sh_addr;
        RtlAddFunctionTable(
            (RUNTIME_FUNCTION*)p,
            s1->uw_pdata->当前数据_偏移量 / sizeof (RUNTIME_FUNCTION),
            s1->pe_优先装载地址
            );
        s1->uw_pdata = NULL;
    }
    return p;
}

static void win64_del_function_table(void *p)
{
    if (p) {
        RtlDeleteFunctionTable((RUNTIME_FUNCTION*)p);
    }
}
#endif
#endif //ndef CONFIG_ZHI_BACKTRACE_ONLY
/* ------------------------------------------------------------- */
#ifdef 启用内置堆栈回溯M

static int rt_vprintf(const char *fmt, va_list ap)
{
    int ret = vfprintf(stderr, fmt, ap);
    fflush(stderr);
    return ret;
}

static int rt_printf(const char *fmt, ...)
{
    va_list ap;
    int r;
    va_start(ap, fmt);
    r = rt_vprintf(fmt, ap);
    va_end(ap);
    return r;
}

#define INCLUDE_栈_大小 32

/* print the position in the source 文件 of PC value 'pc' by reading
   the stabs debug information */
static addr_t rt_printline (运行时上下文ST *rc, addr_t wanted_pc,
    const char *msg, const char *跳过)
{
    char func_name[128];
    addr_t 函数_地址, last_pc, pc;
    const char *include_文件数[INCLUDE_栈_大小];
    int include_索引, 最后一个_include_索引, 长度, 最后_行号, i;
    const char *字符串, *p;
    ElfW(Sym) *esym;
    字符串表符号ST *sym;

next:
    func_name[0] = '\0';
    函数_地址 = 0;
    include_索引 = 0;
    last_pc = (addr_t)-1;
    最后_行号 = 1;
    最后一个_include_索引 = 0;

    for (sym = rc->stab_sym + 1; sym < rc->stab_sym_end; ++sym) {
        字符串 = rc->stab_str + sym->字符串_索引;
        pc = sym->符号_值;

        switch(sym->符号_类型) {
        case N_SLINE:
            if (函数_地址)
                goto rel_pc;
        case N_SO:
        case N_SOL:
            goto abs_pc;
        case N_FUN:
            if (sym->字符串_索引 == 0) /* end of function */
                goto rel_pc;
        abs_pc:
#if 宏_指针大小 == 8
            /* 字符串表符号ST.符号_值 is only 32bits */
            pc += rc->prog_base;
#endif
            goto check_pc;
        rel_pc:
            pc += 函数_地址;
        check_pc:
            if (pc >= wanted_pc && wanted_pc >= last_pc)
                goto found;
            break;
        }

        switch(sym->符号_类型) {
            /* function start or end */
        case N_FUN:
            if (sym->字符串_索引 == 0)
                goto reset_func;
            p = strchr(字符串, 英_冒号);
            if (0 == p || (长度 = p - 字符串 + 1, 长度 > sizeof func_name))
                长度 = sizeof func_name;
            截取前n个字符(func_name, 长度, 字符串);
            函数_地址 = pc;
            break;
            /* 行号信息 */
        case N_SLINE:
            last_pc = pc;
            最后_行号 = sym->描述_字段;
            最后一个_include_索引 = include_索引;
            break;
            /* include files */
        case N_BINCL:
            if (include_索引 < INCLUDE_栈_大小)
                include_文件数[include_索引++] = 字符串;
            break;
        case N_EINCL:
            if (include_索引 > 1)
                include_索引--;
            break;
            /* 翻译单元的开始/结束 */
        case N_SO:
            include_索引 = 0;
            if (sym->字符串_索引) {
                /* 不要添加路径 */
                长度 = strlen(字符串);
                if (长度 > 0 && 字符串[长度 - 1] != '/')
                    include_文件数[include_索引++] = 字符串;
            }
        reset_func:
            func_name[0] = '\0';
            函数_地址 = 0;
            last_pc = (addr_t)-1;
            break;
            /* alternative 文件 name (from #line or #include directives) */
        case N_SOL:
            if (include_索引)
                include_文件数[include_索引-1] = 字符串;
            break;
        }
    }

    func_name[0] = '\0';
    函数_地址 = 0;
    最后一个_include_索引 = 0;

    /* we try 符号表节 symbols (no line number info) */
    for (esym = rc->esym_start + 1; esym < rc->esym_end; ++esym) {
        int 类型 = ELFW(ST_TYPE)(esym->st_info);
        if (类型 == STT_FUNC || 类型 == STT_GNU_IFUNC) {
            if (wanted_pc >= esym->st_value &&
                wanted_pc < esym->st_value + esym->st_size) {
                截取前n个字符(func_name, sizeof(func_name),
                    rc->elf_str + esym->st_name);
                函数_地址 = esym->st_value;
                goto found;
            }
        }
    }

    if ((rc = rc->next))
        goto next;

found:
    i = 最后一个_include_索引;
    if (i > 0) {
        字符串 = include_文件数[--i];
        if (跳过[0] && strstr(字符串, 跳过))
            return (addr_t)-1;
        rt_printf("%s:%d: ", 字符串, 最后_行号);
    } else
        rt_printf("%08llx : ", (long long)wanted_pc);
    rt_printf("%s %s", msg, func_name[0] ? func_name : "???");
    return 函数_地址;
}

static int rt_get_caller_pc(addr_t *paddr, 运行时上下文ST *rc, int level);

static int _rt_error(void *fp, void *ip, const char *fmt, va_list ap)
{
    运行时上下文ST *rc = &g_rtctxt;
    addr_t pc = 0;
    char 跳过[100];
    int i, level, ret, n;
    const char *a, *b, *msg;

    if (fp) {
        /* we're called from zhi_backtrace. */
        rc->fp = (addr_t)fp;
        rc->ip = (addr_t)ip;
        msg = "";
    } else {
        /* we're called from signal/exception handler */
        msg = "RUNTIME ERROR: ";
    }

    跳过[0] = 0;
    /* If fmt is like "^文件.c^..." then 跳过 calls from '文件.c' */
    if (fmt[0] == '^' && (b = strchr(a = fmt + 1, fmt[0]))) {
        memcpy(跳过, a, b - a), 跳过[b - a] = 0;
        fmt = b + 1;
    }

    n = rc->num_callers ? rc->num_callers : 6;
    for (i = level = 0; level < n; i++) {
        ret = rt_get_caller_pc(&pc, rc, i);
        a = "%s";
        if (ret != -1) {
            pc = rt_printline(rc, pc, level ? "by" : "at", 跳过);
            if (pc == (addr_t)-1)
                continue;
            a = ": %s";
        }
        if (level == 0) {
            rt_printf(a, msg);
            rt_vprintf(fmt, ap);
        } else if (ret == -1)
            break;
        rt_printf("\n");
        if (ret == -1 || (pc == (addr_t)rc->top_func && pc))
            break;
        ++level;
    }

    rc->ip = rc->fp = 0;
    return 0;
}

/* emit a run time error at position 'pc' */
static int rt_error(const char *fmt, ...)
{
    va_list ap;
    int ret;
    va_start(ap, fmt);
    ret = _rt_error(0, 0, fmt, ap);
    va_end(ap);
    return ret;
}

static void rt_exit(int code)
{
    运行时上下文ST *rc = &g_rtctxt;
    if (rc->do_jmp)
        longjmp(rc->jmp_buf, code ? code : 256);
    exit(code);
}

/* ------------------------------------------------------------- */

#ifndef _WIN32
# include <signal.h>
# ifndef __OpenBSD__
#  include <sys/ucontext.h>
# endif
#else
# define ucontext_t CONTEXT
#endif

/* translate from ucontext_t* to internal 运行时上下文ST * */
static void rt_getcontext(ucontext_t *uc, 运行时上下文ST *rc)
{
#if defined _WIN64
    rc->ip = uc->Rip;
    rc->fp = uc->Rbp;
    rc->sp = uc->Rsp;
#elif defined _WIN32
    rc->ip = uc->Eip;
    rc->fp = uc->Ebp;
    rc->sp = uc->Esp;
#elif defined __i386__
# if defined(__APPLE__)
    rc->ip = uc->uc_mcontext->__ss.__eip;
    rc->fp = uc->uc_mcontext->__ss.__ebp;
# elif defined(__FreeBSD__) || defined(__FreeBSD_kernel__) || defined(__DragonFly__)
    rc->ip = uc->uc_mcontext.mc_eip;
    rc->fp = uc->uc_mcontext.mc_ebp;
# elif defined(__dietlibc__)
    rc->ip = uc->uc_mcontext.eip;
    rc->fp = uc->uc_mcontext.ebp;
# elif defined(__NetBSD__)
    rc->ip = uc->uc_mcontext.__gregs[_REG_EIP];
    rc->fp = uc->uc_mcontext.__gregs[_REG_EBP];
# elif defined(__OpenBSD__)
    rc->ip = uc->sc_eip;
    rc->fp = uc->sc_ebp;
# elif !defined REG_EIP && defined EIP /* fix for glibc 2.1 */
    rc->ip = uc->uc_mcontext.gregs[EIP];
    rc->fp = uc->uc_mcontext.gregs[EBP];
# else
    rc->ip = uc->uc_mcontext.gregs[REG_EIP];
    rc->fp = uc->uc_mcontext.gregs[REG_EBP];
# endif
#elif defined(__x86_64__)
# if defined(__APPLE__)
    rc->ip = uc->uc_mcontext->__ss.__rip;
    rc->fp = uc->uc_mcontext->__ss.__rbp;
# elif defined(__FreeBSD__) || defined(__FreeBSD_kernel__) || defined(__DragonFly__)
    rc->ip = uc->uc_mcontext.mc_rip;
    rc->fp = uc->uc_mcontext.mc_rbp;
# elif defined(__NetBSD__)
    rc->ip = uc->uc_mcontext.__gregs[_REG_RIP];
    rc->fp = uc->uc_mcontext.__gregs[_REG_RBP];
# elif defined(__OpenBSD__)
    rc->ip = uc->sc_rip;
    rc->fp = uc->sc_rbp;
# else
    rc->ip = uc->uc_mcontext.gregs[REG_RIP];
    rc->fp = uc->uc_mcontext.gregs[REG_RBP];
# endif
#elif defined(__arm__) && defined(__NetBSD__)
    rc->ip = uc->uc_mcontext.__gregs[_REG_PC];
    rc->fp = uc->uc_mcontext.__gregs[_REG_FP];
#elif defined(__arm__) && defined(__OpenBSD__)
    rc->ip = uc->sc_pc;
    rc->fp = uc->sc_r11;
#elif defined(__arm__) && defined(__FreeBSD__)
    rc->ip = uc->uc_mcontext.__gregs[_REG_PC];
    rc->fp = uc->uc_mcontext.__gregs[_REG_FP];
#elif defined(__arm__)
    rc->ip = uc->uc_mcontext.arm_pc;
    rc->fp = uc->uc_mcontext.arm_fp;
#elif defined(__aarch64__) && defined(__APPLE__)
    // see:
    // /Library/Developer/CommandLineTools/SDKs/MacOSX11.1.sdk/usr/include/mach/arm/_structs.h
    rc->ip = uc->uc_mcontext->__ss.__pc;
    rc->fp = uc->uc_mcontext->__ss.__fp;
#elif defined(__aarch64__) && defined(__FreeBSD__)
    rc->ip = uc->uc_mcontext.mc_gpregs.gp_elr; /* aka REG_PC */
    rc->fp = uc->uc_mcontext.mc_gpregs.gp_x[29];
#elif defined(__aarch64__) && defined(__NetBSD__)
    rc->ip = uc->uc_mcontext.__gregs[_REG_PC];
    rc->fp = uc->uc_mcontext.__gregs[_REG_FP];
#elif defined(__aarch64__) && defined(__OpenBSD__)
    rc->ip = uc->sc_elr;
    rc->fp = uc->sc_x[29];
#elif defined(__aarch64__)
    rc->ip = uc->uc_mcontext.pc;
    rc->fp = uc->uc_mcontext.regs[29];
#elif defined(__riscv) && defined(__OpenBSD__)
    rc->ip = uc->sc_sepc;
    rc->fp = uc->sc_s[0];
#elif defined(__riscv)
    rc->ip = uc->uc_mcontext.__gregs[REG_PC];
    rc->fp = uc->uc_mcontext.__gregs[REG_S0];
#endif
}

/* ------------------------------------------------------------- */
#ifndef _WIN32
/* signal handler for fatal errors */
static void sig_error(int signum, siginfo_t *siginf, void *puc)
{
    运行时上下文ST *rc = &g_rtctxt;
    rt_getcontext(puc, rc);

    switch(signum) {
    case SIGFPE:
        switch(siginf->si_code) {
        case FPE_INTDIV:
        case FPE_FLTDIV:
            rt_error("division by zero");
            break;
        default:
            rt_error("floating point exception");
            break;
        }
        break;
    case SIGBUS:
    case SIGSEGV:
        rt_error("invalid memory access");
        break;
    case SIGILL:
        rt_error("illegal instruction");
        break;
    case SIGABRT:
        rt_error("abort() called");
        break;
    default:
        rt_error("caught signal %d", signum);
        break;
    }
    rt_exit(255);
}

#ifndef SA_SIGINFO
# define SA_SIGINFO 0x00000004u
#endif

/* Generate a stack backtrace when a CPU exception occurs. */
static void 设置异常处理(void)
{
    struct sigaction sigact;
    /* install ZHI signal handlers to print debug info on fatal
       runtime errors */
    sigemptyset (&sigact.sa_mask);
    sigact.sa_flags = SA_SIGINFO | SA_RESETHAND;
    sigact.sa_sigaction = sig_error;
    sigemptyset(&sigact.sa_mask);
    sigaction(SIGFPE, &sigact, NULL);
    sigaction(SIGILL, &sigact, NULL);
    sigaction(SIGSEGV, &sigact, NULL);
    sigaction(SIGBUS, &sigact, NULL);
    sigaction(SIGABRT, &sigact, NULL);
}

#else /* WIN32 */

/* signal handler for fatal errors */
static long __stdcall cpu_exception_handler(EXCEPTION_POINTERS *ex_info)
{
    运行时上下文ST *rc = &g_rtctxt;
    unsigned code;
    rt_getcontext(ex_info->ContextRecord, rc);

    switch (code = ex_info->ExceptionRecord->ExceptionCode) {
    case EXCEPTION_ACCESS_VIOLATION:
	rt_error("invalid memory access");
        break;
    case EXCEPTION_STACK_OVERFLOW:
        rt_error("stack overflow");
        break;
    case EXCEPTION_INT_DIVIDE_BY_ZERO:
        rt_error("division by zero");
        break;
    case EXCEPTION_BREAKPOINT:
    case EXCEPTION_SINGLE_STEP:
        rc->ip = *(addr_t*)rc->sp;
        rt_error("breakpoint/single-step exception:");
        return EXCEPTION_CONTINUE_SEARCH;
    default:
        rt_error("caught exception %08x", code);
        break;
    }
    if (rc->do_jmp)
        rt_exit(255);
    return EXCEPTION_EXECUTE_HANDLER;
}

/* 发生CPU异常时生成堆栈回溯。 */
static void 设置异常处理(void)
{
    SetUnhandledExceptionFilter(cpu_exception_handler);
}

#endif

/* ------------------------------------------------------------- */
/* return the PC at frame level 'level'. Return negative if not found */
#if defined(__i386__) || defined(__x86_64__)
static int rt_get_caller_pc(addr_t *paddr, 运行时上下文ST *rc, int level)
{
    addr_t ip, fp;
    if (level == 0) {
        ip = rc->ip;
    } else {
        ip = 0;
        fp = rc->fp;
        while (--level) {
            /* XXX: check address validity with program info */
            if (fp <= 0x1000)
                break;
            fp = ((addr_t *)fp)[0];
        }
        if (fp > 0x1000)
            ip = ((addr_t *)fp)[1];
    }
    if (ip <= 0x1000)
        return -1;
    *paddr = ip;
    return 0;
}

#elif defined(__arm__)
static int rt_get_caller_pc(addr_t *paddr, 运行时上下文ST *rc, int level)
{
    /* XXX: only supports linux/bsd */
#if !defined(__linux__) && \
    !defined(__FreeBSD__) && !defined(__OpenBSD__) && !defined(__NetBSD__)
    return -1;
#else
    if (level == 0) {
        *paddr = rc->ip;
    } else {
        addr_t fp = rc->fp;
        while (--level)
            fp = ((addr_t *)fp)[0];
        *paddr = ((addr_t *)fp)[2];
    }
    return 0;
#endif
}

#elif defined(__aarch64__)
static int rt_get_caller_pc(addr_t *paddr, 运行时上下文ST *rc, int level)
{
    if (level == 0) {
        *paddr = rc->ip;
    } else {
        addr_t *fp = (addr_t*)rc->fp;
        while (--level)
            fp = (addr_t *)fp[0];
        *paddr = fp[1];
    }
    return 0;
}

#elif defined(__riscv)
static int rt_get_caller_pc(addr_t *paddr, 运行时上下文ST *rc, int level)
{
    if (level == 0) {
        *paddr = rc->ip;
    } else {
        addr_t *fp = (addr_t*)rc->fp;
        while (--level && fp >= (addr_t*)0x1000)
            fp = (addr_t *)fp[-2];
        if (fp < (addr_t*)0x1000)
          return -1;
        *paddr = fp[-1];
    }
    return 0;
}

#else
#warning add arch specific rt_get_caller_pc()
static int rt_get_caller_pc(addr_t *paddr, 运行时上下文ST *rc, int level)
{
    return -1;
}

#endif
#endif /* 启用内置堆栈回溯M */
/* ------------------------------------------------------------- */
#ifdef 配置_ZHI_静态

/* dummy function for profiling */
静态函数 void *dlopen(const char *文件名, int flag)
{
    return NULL;
}

静态函数 void dlclose(void *p)
{
}

静态函数 const char *dlerror(void)
{
    return "error";
}

typedef struct ZHISyms {
    char *字符串;
    void *ptr;
} ZHISyms;


/* add the symbol you want here if no dynamic linking is 完成 */
static ZHISyms zhi_syms[] = {
#if !defined(CONFIG_ZHIBOOT)
#define ZHISYM(a) { #a, &a, },
    ZHISYM(printf)
    ZHISYM(fprintf)
    ZHISYM(fopen)
    ZHISYM(fclose)
#undef ZHISYM
#endif
    { NULL, NULL },
};

静态函数 void *dlsym(void *handle, const char *symbol)
{
    ZHISyms *p;
    p = zhi_syms;
    while (p->字符串 != NULL) {
        if (!strcmp(p->字符串, symbol))
            return p->ptr;
        p++;
    }
    return NULL;
}

#endif /* 配置_ZHI_静态 */
#endif /* 运行脚本M */
/* ------------------------------------------------------------- */
