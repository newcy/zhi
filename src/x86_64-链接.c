
#ifdef TARGET_DEFS_ONLY

#define EM_目标机 EM_X86_64

/* relocation 类型 for 32 bit 数据 relocation */
#define R_DATA_32   R_X86_64_32S
#define R_DATA_PTR  R_X86_64_64
#define R_JMP_SLOT  R_X86_64_JUMP_SLOT
#define R_GLOB_DAT  R_X86_64_GLOB_DAT
#define R_COPY      R_X86_64_COPY
#define R_RELATIVE  R_X86_64_RELATIVE

#define R_NUM       R_X86_64_NUM

#define ELF_START_ADDR 0x400000
#define ELF_PAGE_SIZE  0x200000

#define PCRELATIVE_DLLPLT 1
#define RELOCATE_DLLPLT 1

#else /* !TARGET_DEFS_ONLY */

#include "zhi.h"

#if !defined(ELF_OBJ_ONLY) || defined(目标_MACHO)
/* Returns 1 for a code relocation, 0 for a 数据 relocation. For unknown
   relocations, returns -1. */
int code_reloc (int reloc_type)
{
    switch (reloc_type) {
        case R_X86_64_32:
        case R_X86_64_32S:
        case R_X86_64_64:
        case R_X86_64_GOTPC32:
        case R_X86_64_GOTPC64:
        case R_X86_64_GOTPCREL:
        case R_X86_64_GOTPCRELX:
        case R_X86_64_REX_GOTPCRELX:
        case R_X86_64_GOTTPOFF:
        case R_X86_64_GOT32:
        case R_X86_64_GOT64:
        case R_X86_64_GLOB_DAT:
        case R_X86_64_COPY:
        case R_X86_64_RELATIVE:
        case R_X86_64_GOTOFF64:
        case R_X86_64_TLSGD:
        case R_X86_64_TLSLD:
        case R_X86_64_DTPOFF32:
        case R_X86_64_TPOFF32:
            return 0;

        case R_X86_64_PC32:
        case R_X86_64_PC64:
        case R_X86_64_PLT32:
        case R_X86_64_PLTOFF64:
        case R_X86_64_JUMP_SLOT:
            return 1;
    }
    return -1;
}

/* Returns an enumerator to describe whether and when the relocation needs a
   GOT and/or PLT entry to be created. See zhi.h for a description of the
   different values. */
int gotplt_entry_type (int reloc_type)
{
    switch (reloc_type) {
        case R_X86_64_GLOB_DAT:
        case R_X86_64_JUMP_SLOT:
        case R_X86_64_COPY:
        case R_X86_64_RELATIVE:
            return NO_GOTPLT_ENTRY;

	/* The following relocs wouldn't normally need GOT or PLT
	   slots, but we need them for simplicity in the link
	   editor part.  See our caller for comments.  */
        case R_X86_64_32:
        case R_X86_64_32S:
        case R_X86_64_64:
        case R_X86_64_PC32:
        case R_X86_64_PC64:
            return AUTO_GOTPLT_ENTRY;

        case R_X86_64_GOTTPOFF:
            return BUILD_GOT_ONLY;

        case R_X86_64_GOT32:
        case R_X86_64_GOT64:
        case R_X86_64_GOTPC32:
        case R_X86_64_GOTPC64:
        case R_X86_64_GOTOFF64:
        case R_X86_64_GOTPCREL:
        case R_X86_64_GOTPCRELX:
        case R_X86_64_TLSGD:
        case R_X86_64_TLSLD:
        case R_X86_64_DTPOFF32:
        case R_X86_64_TPOFF32:
        case R_X86_64_REX_GOTPCRELX:
        case R_X86_64_PLT32:
        case R_X86_64_PLTOFF64:
            return ALWAYS_GOTPLT_ENTRY;
    }

    return -1;
}

#if !defined(目标_MACHO) || defined 运行脚本M
静态函数 unsigned create_plt_entry(虚拟机ST *s1, unsigned got_偏移, struct 额外符号属性S *attr)
{
    节ST *plt = s1->plt;
    uint8_t *p;
    int modrm;
    unsigned plt_偏移, relofs;

    modrm = 0x25;

    /* empty PLT: create PLT0 entry that pushes the library identifier
       (GOT + 宏_指针大小) and jumps to ld.so resolution routine
       (GOT + 2 * 宏_指针大小) */
    if (plt->当前数据_偏移量 == 0) {
        p = 节_预留指定大小内存(plt, 16);
        p[0] = 0xff; /* pushl got + 宏_指针大小 */
        p[1] = modrm + 0x10;
        write32le(p + 2, 宏_指针大小);
        p[6] = 0xff; /* jmp *(got + 宏_指针大小 * 2) */
        p[7] = modrm;
        write32le(p + 8, 宏_指针大小 * 2);
    }
    plt_偏移 = plt->当前数据_偏移量;

    /* The PLT slot refers to the relocation entry it needs via offset.
       The 重定位 entry is created below, so its offset is the current
       当前数据_偏移量 */
    relofs = s1->plt->重定位 ? s1->plt->重定位->当前数据_偏移量 : 0;

    /* Jump to GOT entry where ld.so initially put the address of ip + 4 */
    p = 节_预留指定大小内存(plt, 16);
    p[0] = 0xff; /* jmp *(got + x) */
    p[1] = modrm;
    write32le(p + 2, got_偏移);
    p[6] = 0x68; /* push $xxx */
    /* On x86-64, the relocation is referred to by _index_ */
    write32le(p + 7, relofs / sizeof (ElfW_Rel) - 1);
    p[11] = 0xe9; /* jmp plt_start */
    write32le(p + 12, -(plt->当前数据_偏移量));
    return plt_偏移;
}

/* 重新定位 the PLT: compute addresses and offsets in the PLT now that final
   address for PLT and GOT are known (see fill_program_header) */
静态函数 void relocate_plt(虚拟机ST *s1)
{
    uint8_t *p, *p_end;

    if (!s1->plt)
      return;

    p = s1->plt->数据;
    p_end = p + s1->plt->当前数据_偏移量;

    if (p < p_end) {
        int x = s1->got->sh_addr - s1->plt->sh_addr - 6;
        add32le(p + 2, x);
        add32le(p + 8, x - 6);
        p += 16;
        while (p < p_end) {
            add32le(p + 2, x + (s1->plt->数据 - p));
            p += 16;
        }
    }

    if (s1->plt->重定位) {
        ElfW_Rel *rel;
        int x = s1->plt->sh_addr + 16 + 6;
        p = s1->got->数据;
        for_each_elem(s1->plt->重定位, 0, rel, ElfW_Rel) {
            write64le(p + rel->r_offset, x);
            x += 16;
        }
    }
}
#endif
#endif

void 重新定位(虚拟机ST *s1, ElfW_Rel *rel, int 类型, unsigned char *ptr, addr_t addr, addr_t val)
{
    int 符号_索引, esym_index;

    符号_索引 = ELFW(R_SYM)(rel->r_info);

    switch (类型) {
        case R_X86_64_64:
            if (s1->输出类型 == 输出_DLL) {
                esym_index = 获取符号属性(s1, 符号_索引, 0)->dyn_索引;
                qrel->r_offset = rel->r_offset;
                if (esym_index) {
                    qrel->r_info = ELFW(R_INFO)(esym_index, R_X86_64_64);
                    qrel->r_addend = rel->r_addend;
                    qrel++;
                    break;
                } else {
                    qrel->r_info = ELFW(R_INFO)(0, R_X86_64_RELATIVE);
                    qrel->r_addend = read64le(ptr) + val;
                    qrel++;
                }
            }
            add64le(ptr, val);
            break;
        case R_X86_64_32:
        case R_X86_64_32S:
            if (s1->输出类型 == 输出_DLL) {
                /* XXX: this logic may depend on ZHI's codegen
                   now ZHI uses R_X86_64_32 even for a 64bit pointer */
                qrel->r_offset = rel->r_offset;
                qrel->r_info = ELFW(R_INFO)(0, R_X86_64_RELATIVE);
                /* Use sign extension! */
                qrel->r_addend = (int)read32le(ptr) + val;
                qrel++;
            }
            add32le(ptr, val);
            break;

        case R_X86_64_PC32:
            if (s1->输出类型 == 输出_DLL) {
                /* DLL relocation */
                esym_index = 获取符号属性(s1, 符号_索引, 0)->dyn_索引;
                if (esym_index) {
                    qrel->r_offset = rel->r_offset;
                    qrel->r_info = ELFW(R_INFO)(esym_index, R_X86_64_PC32);
                    /* Use sign extension! */
                    qrel->r_addend = (int)read32le(ptr) + rel->r_addend;
                    qrel++;
                    break;
                }
            }
            goto plt32pc32;

        case R_X86_64_PLT32:
            /* fallthrough: val already holds the PLT slot address */

        plt32pc32:
        {
            long long diff;
            diff = (long long)val - addr;
            if (diff < -2147483648LL || diff > 2147483647LL) {
                错_误("内部错误: 重定位失败");
            }
            add32le(ptr, diff);
        }
            break;

        case R_X86_64_PLTOFF64:
            add64le(ptr, val - s1->got->sh_addr + rel->r_addend);
            break;

        case R_X86_64_PC64:
            if (s1->输出类型 == 输出_DLL) {
                /* DLL relocation */
                esym_index = 获取符号属性(s1, 符号_索引, 0)->dyn_索引;
                if (esym_index) {
                    qrel->r_offset = rel->r_offset;
                    qrel->r_info = ELFW(R_INFO)(esym_index, R_X86_64_PC64);
                    qrel->r_addend = read64le(ptr) + rel->r_addend;
                    qrel++;
                    break;
                }
            }
            add64le(ptr, val - addr);
            break;

        case R_X86_64_GLOB_DAT:
        case R_X86_64_JUMP_SLOT:
            /* They don't need addend */
            write64le(ptr, val - rel->r_addend);
            break;
        case R_X86_64_GOTPCREL:
        case R_X86_64_GOTPCRELX:
        case R_X86_64_REX_GOTPCRELX:
            add32le(ptr, s1->got->sh_addr - addr +
                         获取符号属性(s1, 符号_索引, 0)->got_偏移 - 4);
            break;
        case R_X86_64_GOTPC32:
            add32le(ptr, s1->got->sh_addr - addr + rel->r_addend);
            break;
        case R_X86_64_GOTPC64:
            add64le(ptr, s1->got->sh_addr - addr + rel->r_addend);
            break;
        case R_X86_64_GOTTPOFF:
            add32le(ptr, val - s1->got->sh_addr);
            break;
        case R_X86_64_GOT32:
            /* we load the got offset */
            add32le(ptr, 获取符号属性(s1, 符号_索引, 0)->got_偏移);
            break;
        case R_X86_64_GOT64:
            /* we load the got offset */
            add64le(ptr, 获取符号属性(s1, 符号_索引, 0)->got_偏移);
            break;
        case R_X86_64_GOTOFF64:
            add64le(ptr, val - s1->got->sh_addr);
            break;
        case R_X86_64_TLSGD:
            {
                static const unsigned char 应为[] = {
                    /* .byte 0x66; lea 0(%rip),%rdi */
                    0x66, 0x48, 0x8d, 0x3d, 0x00, 0x00, 0x00, 0x00,
                    /* .word 0x6666; rex64; call __tls_get_addr@PLT */
                    0x66, 0x66, 0x48, 0xe8, 0x00, 0x00, 0x00, 0x00 };
                static const unsigned char replace[] = {
                    /* mov %fs:0,%rax */
                    0x64, 0x48, 0x8b, 0x04, 0x25, 0x00, 0x00, 0x00, 0x00,
                    /* lea -4(%rax),%rax */
                    0x48, 0x8d, 0x80, 0x00, 0x00, 0x00, 0x00 };

                if (memcmp (ptr-4, 应为, sizeof(应为)) == 0) {
                    ElfW(Sym) *sym;
                    节ST *sec;
                    int32_t x;

                    memcpy(ptr-4, replace, sizeof(replace));
                    rel[1].r_info = ELFW(R_INFO)(0, R_X86_64_NONE);
                    sym = &((ElfW(Sym) *)节符号表_数组->数据)[符号_索引];
                    sec = s1->节数组[sym->st_shndx];
                    x = sym->st_value - sec->sh_addr - sec->当前数据_偏移量;
                    add32le(ptr + 8, x);
                }
                else
                    错_误("意外的 R_X86_64_TLSGD 模式");
            }
            break;
        case R_X86_64_TLSLD:
            {
                static const unsigned char 应为[] = {
                    /* lea 0(%rip),%rdi */
                    0x48, 0x8d, 0x3d, 0x00, 0x00, 0x00, 0x00,
                    /* call __tls_get_addr@PLT */
                    0xe8, 0x00, 0x00, 0x00, 0x00 };
                static const unsigned char replace[] = {
                    /* data16 data16 data16 mov %fs:0,%rax */
                    0x66, 0x66, 0x66, 0x64, 0x48, 0x8b, 0x04, 0x25,
                    0x00, 0x00, 0x00, 0x00 };

                if (memcmp (ptr-3, 应为, sizeof(应为)) == 0) {
                    memcpy(ptr-3, replace, sizeof(replace));
                    rel[1].r_info = ELFW(R_INFO)(0, R_X86_64_NONE);
                }
                else
                    错_误("意外的 R_X86_64_TLSLD 模式");
            }
            break;
        case R_X86_64_DTPOFF32:
        case R_X86_64_TPOFF32:
            {
                ElfW(Sym) *sym;
                节ST *sec;
                int32_t x;

                sym = &((ElfW(Sym) *)节符号表_数组->数据)[符号_索引];
                sec = s1->节数组[sym->st_shndx];
                x = val - sec->sh_addr - sec->当前数据_偏移量;
                add32le(ptr, x);
            }
            break;
        case R_X86_64_NONE:
            break;
        case R_X86_64_RELATIVE:
#ifdef 目标_PE
            add32le(ptr, val - s1->pe_优先装载地址);
#endif
            /* do nothing */
            break;
    }
}

#endif /* !TARGET_DEFS_ONLY */
