/****************************************************************************************************
 * 名称：CIL操作码生成
 * Copyright (c) 2001-2022 Fabrice Bellard
 ****************************************************************************************************/
#error this code has bit-rotted since 2003

/* 可用寄存器的数量 */
#define NB_REGS             3

/* 一个寄存器可以属于多个类。 类必须按从更一般到更精确的顺序进行排序（参见 生成值2() 代码，其中对其进行了假设）。 */
#define RC_ST      0x0001  /* 任何堆栈条目 */
#define RC_ST0     0x0002  /* 栈顶 */
#define RC_ST1     0x0004  /* 栈顶 - 1 */

#define RC_INT     RC_ST
#define RC_FLOAT   RC_ST
#define RC_IRET    RC_ST0 /* 函数返回：整数寄存器 */
#define RC_LRET    RC_ST0 /* 函数返回：第二个整数寄存器 */
#define RC_FRET    RC_ST0 /* 函数返回：浮点寄存器 */

/* 寄存器的漂亮名字 */
enum {
    REG_ST0 = 0,
    REG_ST1,
    REG_ST2,
};

const int reg_classes[NB_REGS] = {
    /* ST0 */ RC_ST | RC_ST0,
    /* ST1 */ RC_ST | RC_ST1,
    /* ST2 */ RC_ST,
};

/* 函数的返回寄存器 */
#define REG_IRET REG_ST0 /* word int 返回寄存器 */
#define REG_LRET REG_ST0 /* 两个 word 返回整数 (为了 long long) */
#define REG_FRET REG_ST0 /* 浮点返回寄存器 */

/* 如果必须以相反的顺序评估函数参数，则定义 */
/* #define 反转函数参数 */

/* 如果结构作为指针传递，则定义。 否则，结构将直接压入堆栈。 */
/* #define FUNC_STRUCT_PARAM_AS_PTR */

/* 指针大小，以字节为单位 */
#define 宏_指针大小 4

/* long double 大小和对齐方式，以字节为单位 */
#define 长双精度大小  8
#define 长双精度对齐单位 8

/* function call context */
typedef struct GFuncContext {
    int 函数调用约定; /* func call 类型 (FUNC_STDCALL or FUNC_CDECL) */
} GFuncContext;

/******************************************************/
/* 操作码定义 */

#define IL_OP_PREFIX 0xFE

enum ILOPCodes {
#define OP(name, 字符串, n) IL_OP_ ## name = n,
#include "CIL操作码.h"
#undef OP
};

char *il_opcodes_str[] = {
#define OP(name, 字符串, n) [n] = 字符串,
#include "CIL操作码.h"
#undef OP
};

/******************************************************/

/* 参数变量号从那里开始 */
#define ARG_BASE 0x70000000

static FILE *il_outfile;

static void out_byte(int c)
{
    *(char *)指令在代码节位置++ = c;
}

static void out_le32(int c)
{
    out_byte(c);
    out_byte(c >> 8);
    out_byte(c >> 16);
    out_byte(c >> 24);
}

static void init_outfile(void)
{
    if (!il_outfile) {
        il_outfile = stdout;
        fprintf(il_outfile, 
                ".assembly extern mscorlib\n"
                "{\n"
                ".ver 1:0:2411:0\n"
                "}\n\n");
    }
}

static void out_op1(int op)
{
    if (op & 0x100)
        out_byte(IL_OP_PREFIX);
    out_byte(op & 0xff);
}

/* 输出带前缀的操作码 */
static void out_op(int op)
{
    out_op1(op);
    fprintf(il_outfile, " %s\n", il_opcodes_str[op]);
}

static void out_opb(int op, int c)
{
    out_op1(op);
    out_byte(c);
    fprintf(il_outfile, " %s %d\n", il_opcodes_str[op], c);
}

static void out_opi(int op, int c)
{
    out_op1(op);
    out_le32(c);
    fprintf(il_outfile, " %s 0x%x\n", il_opcodes_str[op], c);
}

/* XXX: not complete */
static void il_type_to_str(char *buf, int buf_size, 
                           int t, const char *varstr)
{
    int bt;
    符号ST *s, *sa;
    char buf1[256];
    const char *tstr;

    t = t & 值的数据类型_TYPE;
    bt = t & 数据类型_基本类型;
    buf[0] = '\0';
    if (t & 存储类型_无符号的)
        截取后拼接(buf, buf_size, "unsigned ");
    switch(bt) {
    case 数据类型_无类型:
        tstr = "void";
        goto add_tstr;
    case 数据类型_布尔:
        tstr = "bool";
        goto add_tstr;
    case 数据类型_字节:
        tstr = "int8";
        goto add_tstr;
    case 数据类型_短整数:
        tstr = "int16";
        goto add_tstr;
    case 值的数据类型_枚举:
    case 数据类型_整数:
    case 数据类型_长整数:
        tstr = "int32";
        goto add_tstr;
    case 数据类型_长长整数:
        tstr = "int64";
        goto add_tstr;
    case 数据类型_浮点:
        tstr = "float32";
        goto add_tstr;
    case 数据类型_双精度:
    case 数据类型_长双精度:
        tstr = "float64";
    add_tstr:
        截取后拼接(buf, buf_size, tstr);
        break;
    case 数据类型_结构体:
        错_误("尚未处理的结构");
        break;
    case 数据类型_函数:
        s = 查找标识符((unsigned)t >> 值的数据类型_结构体_SHIFT);
        il_type_to_str(buf, buf_size, s->数据类型, varstr);
        截取后拼接(buf, buf_size, "(");
        sa = s->next;
        while (sa != NULL) {
            il_type_to_str(buf1, sizeof(buf1), sa->数据类型, NULL);
            截取后拼接(buf, buf_size, buf1);
            sa = sa->next;
            if (sa)
                截取后拼接(buf, buf_size, ", ");
        }
        截取后拼接(buf, buf_size, ")");
        goto no_var;
    case 数据类型_指针:
        s = 查找标识符((unsigned)t >> 值的数据类型_结构体_SHIFT);
        截取前n个字符(buf1, sizeof(buf1), "*");
        if (varstr)
            截取后拼接(buf1, sizeof(buf1), varstr);
        il_type_to_str(buf, buf_size, s->数据类型, buf1);
        goto no_var;
    }
    if (varstr) {
        截取后拼接(buf, buf_size, " ");
        截取后拼接(buf, buf_size, varstr);
    }
 no_var: ;
}


/* 使用值 'val' 修补重定位条目 */
void greloc_patch1(Reloc *p, int val)
{
}

/* 输出一个符号并修补对它的所有调用 */
void 生成符号_地址(t, a)
{
}

/* output jump and 返回符号 */
static int out_opj(int op, int c)
{
    out_op1(op);
    out_le32(0);
    if (c == 0) {
        c = 指令在代码节位置 - (int)当前代码节->数据;
    }
    fprintf(il_outfile, " %s L%d\n", il_opcodes_str[op], c);
    return c;
}

void 生成符号(int t)
{
    fprintf(il_outfile, "L%d:\n", t);
}

/* load 'r' from value 'sv' */
void load(int r, 栈值ST *sv)
{
    int v, fc, ft;

    v = sv->寄存qi & 存储类型_掩码;
    fc = sv->c.i;
    ft = sv->数据类型;

    if (sv->寄存qi & 存储类型_左值) {
        if (v == 存储类型_局部) {
            if (fc >= ARG_BASE) {
                fc -= ARG_BASE;
                if (fc >= 0 && fc <= 4) {
                    out_op(IL_OP_LDARG_0 + fc);
                } else if (fc <= 0xff) {
                    out_opb(IL_OP_LDARG_S, fc);
                } else {
                    out_opi(IL_OP_LDARG, fc);
                }
            } else {
                if (fc >= 0 && fc <= 4) {
                    out_op(IL_OP_LDLOC_0 + fc);
                } else if (fc <= 0xff) {
                    out_opb(IL_OP_LDLOC_S, fc);
                } else {
                    out_opi(IL_OP_LDLOC, fc);
                }
            }
        } else if (v == 存储类型_VC常量) {
                /* XXX: handle globals */
                out_opi(IL_OP_LDSFLD, 0);
        } else {
            if ((ft & 数据类型_基本类型) == 数据类型_浮点) {
                out_op(IL_OP_LDIND_R4);
            } else if ((ft & 数据类型_基本类型) == 数据类型_双精度) {
                out_op(IL_OP_LDIND_R8);
            } else if ((ft & 数据类型_基本类型) == 数据类型_长双精度) {
                out_op(IL_OP_LDIND_R8);
            } else if ((ft & 值的数据类型_TYPE) == 数据类型_字节)
                out_op(IL_OP_LDIND_I1);
            else if ((ft & 值的数据类型_TYPE) == (数据类型_字节 | 存储类型_无符号的))
                out_op(IL_OP_LDIND_U1);
            else if ((ft & 值的数据类型_TYPE) == 数据类型_短整数)
                out_op(IL_OP_LDIND_I2);
            else if ((ft & 值的数据类型_TYPE) == (数据类型_短整数 | 存储类型_无符号的))
                out_op(IL_OP_LDIND_U2);
            else
                out_op(IL_OP_LDIND_I4);
        } 
    } else {
        if (v == 存储类型_VC常量) {
            /* XXX: handle globals */
            if (fc >= -1 && fc <= 8) {
                out_op(IL_OP_LDC_I4_M1 + fc + 1); 
            } else {
                out_opi(IL_OP_LDC_I4, fc);
            }
        } else if (v == 存储类型_局部) {
            if (fc >= ARG_BASE) {
                fc -= ARG_BASE;
                if (fc <= 0xff) {
                    out_opb(IL_OP_LDARGA_S, fc);
                } else {
                    out_opi(IL_OP_LDARGA, fc);
                }
            } else {
                if (fc <= 0xff) {
                    out_opb(IL_OP_LDLOCA_S, fc);
                } else {
                    out_opi(IL_OP_LDLOCA, fc);
                }
            }
        } else {
            /* XXX: do it */
        }
    }
}

/* store register 'r' in lvalue 'v' */
void store(int r, 栈值ST *sv)
{
    int v, fc, ft;

    v = sv->寄存qi & 存储类型_掩码;
    fc = sv->c.i;
    ft = sv->数据类型;
    if (v == 存储类型_局部) {
        if (fc >= ARG_BASE) {
            fc -= ARG_BASE;
            /* XXX: 检查 IL arg 存储语义 */
            if (fc <= 0xff) {
                out_opb(IL_OP_STARG_S, fc);
            } else {
                out_opi(IL_OP_STARG, fc);
            }
        } else {
            if (fc >= 0 && fc <= 4) {
                out_op(IL_OP_STLOC_0 + fc);
            } else if (fc <= 0xff) {
                out_opb(IL_OP_STLOC_S, fc);
            } else {
                out_opi(IL_OP_STLOC, fc);
            }
        }
    } else if (v == 存储类型_VC常量) {
        /* XXX: 处理全局变量 */
        out_opi(IL_OP_STSFLD, 0);
    } else {
        if ((ft & 数据类型_基本类型) == 数据类型_浮点)
            out_op(IL_OP_STIND_R4);
        else if ((ft & 数据类型_基本类型) == 数据类型_双精度)
            out_op(IL_OP_STIND_R8);
        else if ((ft & 数据类型_基本类型) == 数据类型_长双精度)
            out_op(IL_OP_STIND_R8);
        else if ((ft & 数据类型_基本类型) == 数据类型_字节)
            out_op(IL_OP_STIND_I1);
        else if ((ft & 数据类型_基本类型) == 数据类型_短整数)
            out_op(IL_OP_STIND_I2);
        else
            out_op(IL_OP_STIND_I4);
    }
}

/* 启动函数调用并返回函数调用上下文 */
void gfunc_start(GFuncContext *c, int 函数调用约定)
{
    c->函数调用约定 = 函数调用约定;
}

/*  (栈顶值->数据类型, 栈顶值->c)中的 push 函数参数。 然后弹出堆栈条目 */
void gfunc_param(GFuncContext *c)
{
    if ((栈顶值->数据类型 & 数据类型_基本类型) == 数据类型_结构体) {
        错_误("作为值传递的结构尚未处理");
    } else {
        /* 简单地推入堆栈 */
        生成值(RC_ST0);
    }
    栈顶值--;
}

/*  使用 (栈顶值->数据类型, 栈顶值->c)中的地址和自由函数上下文生成函数调用。 堆栈条目被弹出 */
void g函数_调用(GFuncContext *c)
{
    char buf[1024];

    if ((栈顶值->寄存qi & (存储类型_掩码 | 存储类型_左值)) == 存储类型_VC常量) {
        /* XXX: zhi需要更多信息 */
        il_type_to_str(buf, sizeof(buf), 栈顶值->数据类型, "xxx");
        fprintf(il_outfile, " call %s\n", buf);
    } else {
        /* 间接调用 */
        生成值(RC_INT);
        il_type_to_str(buf, sizeof(buf), 栈顶值->数据类型, NULL);
        fprintf(il_outfile, " calli %s\n", buf);
    }
    栈顶值--;
}

/* 生成“t”类型的函数序言 */
void 生成函数开头代码(int t)
{
    int addr, u, 函数调用约定;
    符号ST *sym;
    char buf[1024];

    init_outfile();

    /* XXX: 将函数名传递给 生成函数开头代码*/
    il_type_to_str(buf, sizeof(buf), t, 函数名);
    fprintf(il_outfile, ".method static %s il managed\n", buf);
    fprintf(il_outfile, "{\n");
    fprintf(il_outfile, " .maxstack %d\n", NB_REGS);
    fprintf(il_outfile, " .locals (int32, int32, int32, int32, int32, int32, int32, int32)\n");
    
    if (!strcmp(函数名, "开始") || !strcmp(函数名, "main"))
    {
    	fprintf(il_outfile, " .entrypoint\n");
    }

        
    sym = 查找标识符((unsigned)t >> 值的数据类型_结构体_SHIFT);
    函数调用约定 = sym->寄存qi;

    addr = ARG_BASE;
    /* 如果函数返回一个结构，则添加一个隐式指针参数 */
    函数_返回类型 = sym->数据类型;
    函数_可变参 = (sym->c == 函数原型_省略);
    if ((函数_返回类型 & 数据类型_基本类型) == 数据类型_结构体) {
        func_vc = addr;
        addr++;
    }
    /* 定义参数 */
    while ((sym = sym->next) != NULL) {
        u = sym->数据类型;
        将符号放入符号栈(sym->v & ~符号_字段, u,
                 存储类型_局部 | lvalue_type(sym->类型.数据类型), addr);
        addr++;
    }
}

/* 生成函数结语 */
void 生成函数结尾代码(void)
{
    out_op(IL_OP_RET);
    fprintf(il_outfile, "}\n\n");
}

int 生成到标签的跳转(int t)
{
    return out_opj(IL_OP_BR, t);
}

void 生成跳转_到固定地址(int a)
{
    /* XXX: handle syms */
    out_opi(IL_OP_BR, a);
}

/* generate a test. set 'inv' to invert test. Stack entry is popped */
int gtst(int inv, int t)
{
    int v, *p, c;

    v = 栈顶值->寄存qi & 存储类型_掩码;
    if (v == 存储类型_标志寄存器) {
        c = 栈顶值->c.i ^ inv;
        switch(c) {
        case TOK_等于:
            c = IL_OP_BEQ;
            break;
        case TOK_不等于:
            c = IL_OP_BNE_UN;
            break;
        case TOK_小于:
            c = IL_OP_BLT;
            break;
        case TOK_小于等于:
            c = IL_OP_BLE;
            break;
        case TOK_大于:
            c = IL_OP_BGT;
            break;
        case TOK_大于等于:
            c = IL_OP_BGE;
            break;
        case TOK_ULT:
            c = IL_OP_BLT_UN;
            break;
        case TOK_ULE:
            c = IL_OP_BLE_UN;
            break;
        case TOK_UGT:
            c = IL_OP_BGT_UN;
            break;
        case TOK_UGE:
            c = IL_OP_BGE_UN;
            break;
        }
        t = out_opj(c, t);
    } else if (v == 存储类型_JMP || v == 存储类型_JMPI) {
        /* && or || optimization */
        if ((v & 1) == inv) {
            /* insert 栈顶值->c jump list in t */
            p = &栈顶值->c.i;
            while (*p != 0)
                p = (int *)*p;
            *p = t;
            t = 栈顶值->c.i;
        } else {
            t = 生成到标签的跳转(t);
            生成符号(栈顶值->c.i);
        }
    }
    栈顶值--;
    return t;
}

/* 生成整数二元运算 */
void gen_opi(int op)
{
    生成值2(RC_ST1, RC_ST0);
    switch(op) {
    case '+':
        out_op(IL_OP_ADD);
        goto std_op;
    case '-':
        out_op(IL_OP_SUB);
        goto std_op;
    case '&':
        out_op(IL_OP_AND);
        goto std_op;
    case '^':
        out_op(IL_OP_XOR);
        goto std_op;
    case '|':
        out_op(IL_OP_OR);
        goto std_op;
    case '*':
        out_op(IL_OP_MUL);
        goto std_op;
    case TOK_左移:
        out_op(IL_OP_SHL);
        goto std_op;
    case TOK_SHR:
        out_op(IL_OP_SHR_UN);
        goto std_op;
    case TOK_右移:
        out_op(IL_OP_SHR);
        goto std_op;
    case '/':
    case TOK_PDIV:
        out_op(IL_OP_DIV);
        goto std_op;
    case TOK_UDIV:
        out_op(IL_OP_DIV_UN);
        goto std_op;
    case '%':
        out_op(IL_OP_REM);
        goto std_op;
    case TOK_UMOD:
        out_op(IL_OP_REM_UN);
    std_op:
        栈顶值--;
        栈顶值[0].寄存qi = REG_ST0;
        break;
    case TOK_等于:
    case TOK_不等于:
    case TOK_小于:
    case TOK_小于等于:
    case TOK_大于:
    case TOK_大于等于:
    case TOK_ULT:
    case TOK_ULE:
    case TOK_UGT:
    case TOK_UGE:
        栈顶值--;
        栈顶值[0].寄存qi = 存储类型_标志寄存器;
        栈顶值[0].c.i = op;
        break;
    }
}

/* 生成浮点运算'v = t1 op t2'指令。 保证两个操作数具有相同的浮点类型 */
void gen_opf(int op)
{
    /* 与整数相同 */
    gen_opi(op);
}

/* 将整数转换为 fp 't' 类型。 必须处理“int”、“unsigned int”和“long long”情况。 */
void gen_cvt_itof(int t)
{
    生成值(RC_ST0);
    if (t == 数据类型_浮点)
        out_op(IL_OP_CONV_R4);
    else
        out_op(IL_OP_CONV_R8);
}

/* convert fp to int 't' 类型 */
/* XXX: 处理 long long 情况 */
void gen_cvt_ftoi(int t)
{
    生成值(RC_ST0);
    switch(t) {
    case 数据类型_整数 | 存储类型_无符号的:
        out_op(IL_OP_CONV_U4);
        break;
    case 数据类型_长长整数:
        out_op(IL_OP_CONV_I8);
        break;
    case 数据类型_长长整数 | 存储类型_无符号的:
        out_op(IL_OP_CONV_U8);
        break;
    default:
        out_op(IL_OP_CONV_I4);
        break;
    }
}

/* 从一种浮点类型转换为另一种 */
void gen_cvt_ftof(int t)
{
    生成值(RC_ST0);
    if (t == 数据类型_浮点) {
        out_op(IL_OP_CONV_R4);
    } else {
        out_op(IL_OP_CONV_R8);
    }
}

/* CIL 代码生成器结束 */
/*************************************************************/

