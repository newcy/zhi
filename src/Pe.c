/****************************************************************************************************
 * 名称：PE文件输出
 * 版权 (c) 2005-2007 grischka
 * 版权(C) 2019-2023 位中原
 * 网址：www.880.xin
 ****************************************************************************************************/

/* PE可执行文件头
 * ----------------------------------------
 * |        |         |DOS MZ头（64字节）     |
 * |        | DOS头    |--------------------|
 * |        |         |DOS Stub(空间大小不确定)|
 * |        |------------------------------|
 * |PE文件头部|         |  PE签名             |
 * |        |         |--------------------|
 * |        |NT头(PE头)| PE文件头             |
 * |        |         |--------------------|
 * |        |         |  PE可选头            |
 * |---------------------------------------|
 * |        | 节表（节的数量X40个字节）          | PE中所有节的目录
 * |        |------------------------------|
 * |        |         |.idata节（导入表节）    |
 * |PE数据区  |         |--------------------|
 * |        | 节内容    |.text节（代码）      |
 * |        |         |--------------------|
 * |        |         |.data节（数据节）      |
 * ----------------------------------------
 *
 * .bss : 未初始化的全局变量，静态变量的内存区域，初始化之后.bss节将会清零，属于静态内存分配，即程序一开始就将其清零了。.bss在应用程序的二进制映像文件中并不存在，不占用可执行文件的空间。
 * .data : 程序中已被初始化的全局变量，常量，静态变量的内存区域。也就是静态存储区。data段为数据分配空间，数据保存在目标文件中。
 * .txt : 代码
 * .rodata : 常量节（只读数据节）
 *
 * #include <stdio.h>
 * int value1;                //全局的未被初始化的变量，处于bss段
 * static void *value2 = NULL //全局的指针变量，处于bss段
 * int value3 = 5             //全局的被初始化的变量，处于data段
 * const int value4 = 5;      //加上const变为只读的常量，处于rodata段
 * main()
 * {
 *     return 0;
 * }
 * */
#include "zhi.h"

#define PE_合并_数据 1

#ifndef _WIN32
#define stricmp strcasecmp/*判断字符串是否相等(忽略大小写)*/
#define strnicmp strncasecmp /*strncasecmp()用来比较参数s1 和s2 字符串前n个字符，比较时会自动忽略大小写的差异。*/
#include <sys/stat.h> /* chmod() */
#endif

#ifdef 目标_X86_64
# define ADDR3264 ULONGLONG
# define PE_IMAGE_REL IMAGE_REL_BASED_DIR64
# define REL_TYPE_DIRECT R_X86_64_64
# define R_XXX_THUNKFIX R_X86_64_PC32
# define R_XXX_RELATIVE R_X86_64_RELATIVE
# define R_XXX_FUNCCALL R_X86_64_PC32
# define IMAGE_FILE_MACHINE 0x8664
# define RSRC_RELTYPE 3

#elif defined 目标_ARM
# define ADDR3264 DWORD
# define PE_IMAGE_REL IMAGE_REL_BASED_HIGHLOW
# define REL_TYPE_DIRECT R_ARM_ABS32
# define R_XXX_THUNKFIX R_ARM_ABS32
# define R_XXX_RELATIVE R_ARM_RELATIVE
# define R_XXX_FUNCCALL R_ARM_PC24
# define R_XXX_FUNCCALL2 R_ARM_ABS32
# define IMAGE_FILE_MACHINE 0x01C0
# define RSRC_RELTYPE 7 /* ??? (not tested) */

#elif defined 目标_I386
# define ADDR3264 DWORD
# define PE_IMAGE_REL IMAGE_REL_BASED_HIGHLOW
# define REL_TYPE_DIRECT R_386_32
# define R_XXX_THUNKFIX R_386_32
# define R_XXX_RELATIVE R_386_RELATIVE
# define R_XXX_FUNCCALL R_386_PC32
# define IMAGE_FILE_MACHINE 0x014C
# define RSRC_RELTYPE 7 /* DIR32NB */

#endif

#ifndef IMAGE_NT_SIGNATURE
/* ----------------------------------------------------------- */
/* 下面的定义来自 winnt.h */

typedef unsigned char BYTE;
typedef unsigned short WORD;
typedef unsigned int DWORD;
typedef unsigned long long ULONGLONG;
#pragma pack(push, 1)

typedef struct _IMAGE_DOS_HEADER {  /* DOS .EXE header */
    WORD e_magic;         /* 0000h-EXE标志，“MZ” （Mark Zbikowski,他是DOS操作系统的开发者之一）。*/
    WORD e_cblp;          /* 0002h-文件最后一页的字节数 */
    WORD e_cp;            /* 0004h-文件中的全部和部分页数 */
    WORD e_crlc;          /* 0006h-重定位表中的指针数 */
    WORD e_cparhdr;       /* 0008h-头部尺寸，以段落为单位。（段落标题的大小） */
    WORD e_minalloc;      /* 000ah-所需的最小附加段 */
    WORD e_maxalloc;      /* 000ch-所需的最大附加段 */
    WORD e_ss;            /* 000eh-初始的SS值（相对偏移量） */
    WORD e_sp;            /* 0010h-初始的SP值 */
    WORD e_csum;          /* 0012h-补码效验值 */
    WORD e_ip;            /* 0014h-初始的IP值 */
    WORD e_cs;            /* 0016h-初始的CS值 */
    WORD e_lfarlc;        /* 0018h-重定位表的字节偏移量（重定位表的文件地址） */
    WORD e_ovno;          /* 001ah-覆盖号 */
    WORD e_res[4];        /* 001ch-保留字 */
    WORD e_oemid;         /* 0024h-OEM标识符（用于 e_oeminfo） */
    WORD e_oeminfo;       /* 0026h-OEM信息 */
    WORD e_res2[10];      /* 0028h-保留字 */
    DWORD e_lfanew;       /* 003ch-PE头相对于文件的偏移地址.是定位PE头的起始位置 */
    /* PE_star=DOS MZ基地址+IMAGE_DOS_HEADER.e_lfanew
     *        =13B7:0100+000000B0H
     *        =13B7:01B0
     * */
} IMAGE_DOS_HEADER, *PIMAGE_DOS_HEADER;

#define IMAGE_NT_SIGNATURE  0x00004550  /* PE头标识，该标识位于指针IMAGE_DOS_HEADER.e_lfanew指向的位置。其内容为：PE00 */
#define SIZE_OF_NT_SIGNATURE 4   /*PE头标识大小为4个字节*/

typedef struct _IMAGE_FILE_HEADER { /*标准PE头*/
    WORD    Machine;             /*运行平台*/
    WORD    NumberOfSections;    /*节数量*/
    DWORD   TimeDateStamp;       /*文件创建日趋和时间*/
    DWORD   PointerToSymbolTable;/*符号表指针（用于调试）*/
    DWORD   NumberOfSymbols;     /*符号表中的符号数量*/
    WORD    SizeOfOptionalHeader;/*扩展头结构的长度*/
    WORD    Characteristics;     /*节文件属性*/
} IMAGE_FILE_HEADER, *PIMAGE_FILE_HEADER;


#define IMAGE_SIZEOF_FILE_HEADER 20

typedef struct _IMAGE_DATA_DIRECTORY { /*16中数据类型*/
    DWORD   VirtualAddress;   /*数据的起始RVA(相对虚拟地址)*/
    DWORD   Size;             /*数据块的长度*/
} IMAGE_DATA_DIRECTORY, *PIMAGE_DATA_DIRECTORY;

/*PE可选头（扩展PE头）*/
typedef struct _IMAGE_OPTIONAL_HEADER {
    /* Standard fields. */
    WORD    Magic; /*文件的类型，如果为010BH，则标识该文件为PE32;如果为0107h,则表示文件为ROM映像文件；如果为020BH，则标识该文件为PE32+，即64位PE文件*/
    BYTE    MajorLinkerVersion;/*连接器版本号*/
    BYTE    MinorLinkerVersion;/*链接器版本号*/
    DWORD   SizeOfCode;/*所有代码节的总和（以字节计算，文件对齐后的大小）*/
    DWORD   SizeOfInitializedData;/*所有包含已经初始化的数据的节的总大小*/
    DWORD   SizeOfUninitializedData;/*所有包含未初始化的数据的节的总大小*/
    DWORD   AddressOfEntryPoint; /*程序执行入口RVA（相对虚拟地址）。病毒，加密，补丁都会劫持这里的值，十七指向其他用途的代码的地址。*/
    DWORD   BaseOfCode;/*代码节的起始RVA（相对虚拟地址）*/
#ifndef 目标_X86_64
    DWORD   BaseOfData;/*数据节的起始RVA（相对虚拟地址）*/
#endif
    /* NT additional fields. */
    ADDR3264 ImageBase;/*PE映像的优先装载地址，也就是AddressOfEntryPoint中说的程序被加载到内存后的起始VA（虚拟地址）。*/
    DWORD   SectionAlignment;/*被装载内存中节的对齐粒度*/
    DWORD   FileAlignment;/*文件中节的对齐粒度*/
    WORD    MajorOperatingSystemVersion;/*标识操作系统的版本号，主版本号*/
    WORD    MinorOperatingSystemVersion;/*标识操作系统的版本号，次版本号*/
    WORD    MajorImageVersion;/*PE文件映像的版本号，主版本号*/
    WORD    MinorImageVersion;/*PE文件映像的版本号，次版本号*/
    WORD    MajorSubsystemVersion;/*子系统的版本号，主版本号*/
    WORD    MinorSubsystemVersion;/*子系统的版本号，次版本号*/
    DWORD   Win32VersionValue;/*子系统版本的值，暂时保留未用，必须设置为0*/
    DWORD   SizeOfImage;/*内存中整个PE文件的映像尺寸*/
    DWORD   SizeOfHeaders;/*所有头+节表按照文件对齐粒度对齐后的大小*/
    DWORD   CheckSum;/*校验和，大多数的PE文件中，该值是0*/
    WORD    Subsystem;/*指定使用界面的子系统，如：窗口界面系统，控制台系统等*/
    WORD    DllCharacteristics;/*DLL文件属性*/
    ADDR3264 SizeOfStackReserve;/*初始化时保留栈的大小*/
    ADDR3264 SizeOfStackCommit;/*初始化时实际提交的栈的大小*/
    ADDR3264 SizeOfHeapReserve;/*初始化时保留的堆的大小*/
    ADDR3264 SizeOfHeapCommit;/*初始化时实际提交的堆的大小*/
    DWORD   LoaderFlags;/*加载标志*/
    DWORD   NumberOfRvaAndSizes;/*定义数据目录结构的数量，一般为00000010h,即16个*/
    IMAGE_DATA_DIRECTORY DataDirectory[16];/*PE文件的16中数据类型*/
} IMAGE_OPTIONAL_HEADER32, IMAGE_OPTIONAL_HEADER64, IMAGE_OPTIONAL_HEADER;

#define IMAGE_DIRECTORY_ENTRY_EXPORT          0   /* Export Directory */
#define IMAGE_DIRECTORY_ENTRY_IMPORT          1   /* Import Directory */
#define IMAGE_DIRECTORY_ENTRY_RESOURCE        2   /* Resource Directory */
#define IMAGE_DIRECTORY_ENTRY_EXCEPTION       3   /* Exception Directory */
#define IMAGE_DIRECTORY_ENTRY_SECURITY        4   /* Security Directory */
#define IMAGE_DIRECTORY_ENTRY_BASERELOC       5   /* Base Relocation Table */
#define IMAGE_DIRECTORY_ENTRY_DEBUG           6   /* Debug Directory */
/*      IMAGE_DIRECTORY_ENTRY_COPYRIGHT       7      (X86 usage) */
#define IMAGE_DIRECTORY_ENTRY_ARCHITECTURE    7   /* Architecture Specific Data */
#define IMAGE_DIRECTORY_ENTRY_GLOBALPTR       8   /* RVA of GP */
#define IMAGE_DIRECTORY_ENTRY_TLS             9   /* TLS Directory */
#define IMAGE_DIRECTORY_ENTRY_LOAD_CONFIG    10   /* Load Configuration Directory */
#define IMAGE_DIRECTORY_ENTRY_BOUND_IMPORT   11   /* Bound Import Directory in headers */
#define IMAGE_DIRECTORY_ENTRY_IAT            12   /* Import Address Table */
#define IMAGE_DIRECTORY_ENTRY_DELAY_IMPORT   13   /* Delay Load Import Descriptors */
#define IMAGE_DIRECTORY_ENTRY_COM_DESCRIPTOR 14   /* COM Runtime descriptor */

/* 节ST header format. */
#define IMAGE_SIZEOF_SHORT_NAME         8
/*节表*/
typedef struct _IMAGE_SECTION_HEADER {
    BYTE    Name[IMAGE_SIZEOF_SHORT_NAME];/*8个字节的节名*/
    union {
            DWORD   PhysicalAddress;
            DWORD   VirtualSize;          /*节区的尺寸*/
    } Misc;
    DWORD   VirtualAddress;               /*节区的RVA(相对虚拟地址)*/
    DWORD   SizeOfRawData;                /*在文件中对齐后的尺寸*/
    DWORD   PointerToRawData;             /*在文件中的偏移*/
    DWORD   PointerToRelocations;         /*在OBJ文件中使用*/
    DWORD   PointerToLinenumbers;         /*行号表的位置*/
    WORD    NumberOfRelocations;          /*在OBJ文件中使用*/
    WORD    NumberOfLinenumbers;          /*行号表中行号的数量*/
    DWORD   Characteristics;              /*节的属性*/
} IMAGE_SECTION_HEADER, *PIMAGE_SECTION_HEADER;

#define IMAGE_SIZEOF_SECTION_HEADER     40

typedef struct _IMAGE_EXPORT_DIRECTORY {
    DWORD Characteristics;
    DWORD TimeDateStamp;
    WORD MajorVersion;
    WORD MinorVersion;
    DWORD Name;
    DWORD Base;
    DWORD NumberOfFunctions;
    DWORD NumberOfNames;
    DWORD AddressOfFunctions;
    DWORD AddressOfNames;
    DWORD AddressOfNameOrdinals;
} IMAGE_EXPORT_DIRECTORY,*PIMAGE_EXPORT_DIRECTORY;

typedef struct _IMAGE_IMPORT_DESCRIPTOR {
    union {
        DWORD Characteristics;
        DWORD OriginalFirstThunk;
    };
    DWORD TimeDateStamp;
    DWORD ForwarderChain;
    DWORD Name;
    DWORD FirstThunk;
} IMAGE_IMPORT_DESCRIPTOR;

typedef struct _IMAGE_BASE_RELOCATION {
    DWORD   VirtualAddress;
    DWORD   SizeOfBlock;
//  WORD    TypeOffset[1];
} IMAGE_BASE_RELOCATION;

#define IMAGE_SIZEOF_BASE_RELOCATION     8

#define IMAGE_REL_BASED_ABSOLUTE         0
#define IMAGE_REL_BASED_HIGH             1
#define IMAGE_REL_BASED_LOW              2
#define IMAGE_REL_BASED_HIGHLOW          3
#define IMAGE_REL_BASED_HIGHADJ          4
#define IMAGE_REL_BASED_MIPS_JMPADDR     5
#define IMAGE_REL_BASED_SECTION          6
#define IMAGE_REL_BASED_REL32            7
#define IMAGE_REL_BASED_DIR64           10

#define IMAGE_SCN_CNT_CODE                  0x00000020
#define IMAGE_SCN_CNT_INITIALIZED_DATA      0x00000040
#define IMAGE_SCN_CNT_UNINITIALIZED_DATA    0x00000080
#define IMAGE_SCN_MEM_DISCARDABLE           0x02000000
#define IMAGE_SCN_MEM_SHARED                0x10000000
#define IMAGE_SCN_MEM_EXECUTE               0x20000000
#define IMAGE_SCN_MEM_READ                  0x40000000
#define IMAGE_SCN_MEM_WRITE                 0x80000000

#pragma pack(pop)

/* ----------------------------------------------------------- */
#endif /* ndef IMAGE_NT_SIGNATURE */
/* ----------------------------------------------------------- */

#ifndef IMAGE_REL_BASED_DIR64
# define IMAGE_REL_BASED_DIR64 10
#endif

#pragma pack(push, 1)
struct PE文件头S
{
    IMAGE_DOS_HEADER doshdr;   /*DOS头*/
    BYTE dosstub[0x40];
    DWORD nt_sig;
    IMAGE_FILE_HEADER filehdr; /*标准PE头*/
#ifdef 目标_X86_64
    IMAGE_OPTIONAL_HEADER64 opthdr;
#else
#ifdef _WIN64
    IMAGE_OPTIONAL_HEADER32 opthdr;
#else
    IMAGE_OPTIONAL_HEADER opthdr;/*扩展PE头*/
#endif
#endif
};

struct PE重定位头S {
    DWORD offset;
    DWORD 大小;
};

struct PE资源头S {
    struct _IMAGE_FILE_HEADER filehdr;
    struct _IMAGE_SECTION_HEADER sectionhdr;
};

struct PE资源重定位S {
    DWORD offset;
    DWORD 大小;
    WORD 类型;
};
#pragma pack(pop)

/* ------------------------------------------------------------- */
/* 内部临时结构 */

enum {
    sec_text = 0,
    sec_rdata ,
    sec_data ,
    sec_bss ,
    sec_idata ,
    sec_pdata ,
    sec_other ,
    sec_rsrc ,
    sec_stab ,
    sec_stabstr ,
    sec_reloc ,
    sec_last
};

struct 节信息S {
    int cls;
    char name[32];
    DWORD sh_addr;
    DWORD sh_size;
    DWORD pe_flags;
    节ST *sec;
    DWORD data_size;
    IMAGE_SECTION_HEADER ish;
};

struct 导入符号ST {
    int 符号_索引;
    int iat_index;
    int thk_offset;
};

struct PE导入信息S {
    int dll_index;
    int sym_count;
    struct 导入符号ST **symbols;
};

struct PE信息S {
    虚拟机ST *s1;
    节ST *重定位;
    节ST *thunk;
    const char *输出的文件名称;  /*如：输出的文件名称.exe*/
    int 类型;
    DWORD sizeofheaders;
    ADDR3264 优先装载地址;
    const char *入口符号;/*开始，main，winmain 等*/
    DWORD 第一条指令的相对虚拟地址;
    DWORD imp_offs;
    DWORD imp_size;
    DWORD iat_offs;
    DWORD iat_size;
    DWORD exp_offs;
    DWORD exp_size;
    int 子系统;
    DWORD 节_对齐;
    DWORD 文件_对齐;
    struct 节信息S **节_信息;
    int 节_数量;
    struct PE导入信息S **imp_info;
    int imp_count;
};

#define PE_NUL 0
#define PE_DLL 1
#define PE_GUI 2
#define PE_EXE 3
#define PE_RUN 4

/* --------------------------------------------*/

static const char *pe_导出_名称(虚拟机ST *s1, ElfW(Sym) *sym)
{
    const char *name = (char*)节符号表_数组->link->数据 + sym->st_name;
    if (s1->前导下划线 && name[0] == '_' && !(sym->st_other & ST_PE_STDCALL))
        return name + 1;
    return name;
}

static int pe_查找_导入(虚拟机ST * s1, ElfW(Sym) *sym)
{
    char 缓冲[200];
    const char *s, *p;
    int 符号_索引 = 0, n = 0;
    int a, err = 0;

    do {
        s = pe_导出_名称(s1, sym);
        a = 0;
        if (n) {
            /* second try: */
	    if (sym->st_other & ST_PE_STDCALL) {
                /* try w/0 stdcall deco (windows API convention) */
	        p = strrchr(s, '@');
	        if (!p || s[0] != '_')
	            break;
	        strcpy(缓冲, s+1)[p-s-1] = 0;
	    } else if (s[0] != '_') { /* try non-ansi function */
	        缓冲[0] = '_', strcpy(缓冲 + 1, s);
	    } else if (0 == memcmp(s, "__imp_", 6)) { /* mingw 2.0 */
	        strcpy(缓冲, s + 6), a = 1;
	    } else if (0 == memcmp(s, "_imp__", 6)) { /* mingw 3.7 */
	        strcpy(缓冲, s + 6), a = 1;
	    } else {
	        continue;
	    }
	    s = 缓冲;
        }
        符号_索引 = 查找elf符号(s1->临时动态符号表_节, s);
        // printf("find (%d) %d %s\n", n, 符号_索引, s);
        if (符号_索引
            && ELFW(ST_TYPE)(sym->st_info) == STT_OBJECT
            && 0 == (sym->st_other & ST_PE_IMPORT)
            && 0 == a
            ) err = -1, 符号_索引 = 0;
    } while (0 == 符号_索引 && ++n < 2);
    return n == 2 ? err : 符号_索引;
}

/*----------------------------------------------------------------------------*/

static int 动态数组_关联(void **pp, int n, int key)
{
    int i;
    for (i = 0; i < n; ++i, ++pp)
    if (key == **(int **) pp)
        return i;
    return -1;
}

static DWORD 返回小值(DWORD a, DWORD b)
{
    return a < b ? a : b;
}

static DWORD 返回大值(DWORD a, DWORD b)
{
    return a < b ? b : a;
}

static DWORD pe_文件_对齐(struct PE信息S *pe, DWORD n)
{
    return (n + (pe->文件_对齐 - 1)) & ~(pe->文件_对齐 - 1);
}

static DWORD pe_虚拟_对齐(struct PE信息S *pe, DWORD n)
{
    return (n + (pe->节_对齐 - 1)) & ~(pe->节_对齐 - 1);
}

static void pe_对齐节(节ST *s, int a)
{
    int i = s->当前数据_偏移量 & (a-1);
    if (i)
        节_预留指定大小内存(s, a - i);
}

static void pe_设置_数据目录(struct PE文件头S *hdr, int dir, DWORD addr, DWORD 大小)
{
    hdr->opthdr.DataDirectory[dir].VirtualAddress = addr;
    hdr->opthdr.DataDirectory[dir].Size = 大小;
}

struct PE文件S {
    FILE *op;
    DWORD sum;
    unsigned pos;
};
/*功能：写出EXE文件
 * 数据：
 * 长度：
 * pf：
 * */
static int pe_写文件(void *数据, int 长度, struct PE文件S *pf)
{
    WORD *p = 数据;
    DWORD sum;
    int ret, i;
    /*
     * 数据:这是指向要被写入的元素数组的指针。
     * 1:这是要被写入的每个元素的大小，以字节为单位。
     * 长度:这是元素的个数，每个元素的大小为 1 字节。
     * pf->op:这是指向 FILE 对象的指针，该 FILE 对象指定了一个输出流。
     * */
    pf->pos += (ret = fwrite(数据, 1, 长度, pf->op));
    sum = pf->sum;
    for (i = 长度; i > 0; i -= 2) {
        sum += (i >= 2) ? *p++ : *(BYTE*)p;
        sum = (sum + (sum >> 16)) & 0xFFFF;
    }
    pf->sum = sum;
    return 长度 == ret ? 0 : -1;
}

static void pe_fpad(struct PE文件S *pf, DWORD new_pos)
{
    char buf[256];
    int n, diff = new_pos - pf->pos;
    memset(buf, 0, sizeof buf);
    while (diff > 0)
    {
        diff -= n = 返回小值(diff, sizeof buf);
        fwrite(buf, n, 1, pf->op);
    }
    pf->pos = new_pos;
}

/*----------------------------------------------------------------------------*/
static int pe_写(struct PE信息S *pe)
{
    static const struct PE文件头S PE文件头_模板 =
    {
    	/* 一、DOS MZ头：IMAGE_DOS_HEADER doshdr */
		{
		0x5A4D, /*WORD e_magic;*/
		0x0090, /*WORD e_cblp; */
		0x0003, /*WORD e_cp; */
		0x0000, /*WORD e_crlc; */

		0x0004, /*WORD e_cparhdr; */
		0x0000, /*WORD e_minalloc;*/
		0xFFFF, /*WORD e_maxalloc;  */
		0x0000, /*WORD e_ss;  */

		0x00B8, /*WORD e_sp;   */
		0x0000, /*WORD e_csum; */
		0x0000, /*WORD e_ip; */
		0x0000, /*WORD e_cs; */
		0x0040, /*WORD e_lfarlc;  */
		0x0000, /*WORD e_ovno; */
		{0,0,0,0}, /*WORD e_res[4]; */
		0x0000, /*WORD e_oemid;*/
		0x0000, /*WORD e_oeminfo;  */
		{0,0,0,0,0,0,0,0,0,0}, /*WORD e_res2[10]; */
		0x00000080  /*DWORD   e_lfanew;   */
		},
		/* 二、DOS Stub:BYTE dosstub[0x40] */
		{
		/* 14 个代码字节 + "此程序不能在 DOS 模式下运行。\r\r\n$" + 6 * 0x00*/
		0x0e,0x1f,0xba,0x0e,0x00,0xb4,0x09,0xcd,0x21,0xb8,0x01,0x4c,0xcd,0x21,0x54,0x68,
		0x69,0x73,0x20,0x70,0x72,0x6f,0x67,0x72,0x61,0x6d,0x20,0x63,0x61,0x6e,0x6e,0x6f,
		0x74,0x20,0x62,0x65,0x20,0x72,0x75,0x6e,0x20,0x69,0x6e,0x20,0x44,0x4f,0x53,0x20,
		0x6d,0x6f,0x64,0x65,0x2e,0x0d,0x0d,0x0a,0x24,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		},
		/* 三、PE头标识：DWORD nt_sig = IMAGE_NT_SIGNATURE */
		0x00004550,
		/* 四、标准PE头：IMAGE_FILE_HEADER filehdr */
		{
		IMAGE_FILE_MACHINE, /*WORD    Machine; */
		0x0003, /*WORD    NumberOfSections; */
		0x00000000, /*DWORD   TimeDateStamp; */
		0x00000000, /*DWORD   PointerToSymbolTable; */
		0x00000000, /*DWORD   NumberOfSymbols; */
	#if defined(目标_X86_64)
		0x00F0, /*WORD    SizeOfOptionalHeader; */
		0x022F  /*WORD    Characteristics; */
	#define CHARACTERISTICS_DLL 0x222E
	#elif defined(目标_I386)
		0x00E0, /*WORD    SizeOfOptionalHeader; */
		0x030F  /*WORD    Characteristics; */
	#define CHARACTERISTICS_DLL 0x230E
	#elif defined(目标_ARM)
		0x00E0, /*WORD    SizeOfOptionalHeader; */
		0x010F, /*WORD    Characteristics; */
	#define CHARACTERISTICS_DLL 0x230F
	#endif
	    },
		/* 五、扩展PE头：IMAGE_OPTIONAL_HEADER opthdr */
	    {
		/* Standard fields. */
	#ifdef 目标_X86_64
		0x020B, /*WORD    Magic; */
	#else
		0x010B, /*WORD    Magic; */
	#endif
		0x06, /*BYTE    MajorLinkerVersion; */
		0x00, /*BYTE    MinorLinkerVersion; */
		0x00000000, /*DWORD   SizeOfCode; */
		0x00000000, /*DWORD   SizeOfInitializedData; */
		0x00000000, /*DWORD   SizeOfUninitializedData; */
		0x00000000, /*DWORD   AddressOfEntryPoint; */
		0x00000000, /*DWORD   BaseOfCode; */
	#ifndef 目标_X86_64
		0x00000000, /*DWORD   BaseOfData; */
	#endif
		/* NT additional fields. */
	#if defined(目标_ARM)
		0x00100000,	    /*DWORD   ImageBase; */
	#else
		0x00400000,	    /*DWORD   ImageBase; */
	#endif
		0x00001000, /*DWORD   SectionAlignment; */
		0x00000200, /*DWORD   FileAlignment; */
		0x0004, /*WORD    MajorOperatingSystemVersion; */
		0x0000, /*WORD    MinorOperatingSystemVersion; */
		0x0000, /*WORD    MajorImageVersion; */
		0x0000, /*WORD    MinorImageVersion; */
		0x0004, /*WORD    MajorSubsystemVersion; */
		0x0000, /*WORD    MinorSubsystemVersion; */
		0x00000000, /*DWORD   Win32VersionValue; */
		0x00000000, /*DWORD   SizeOfImage; */
		0x00000200, /*DWORD   SizeOfHeaders; */
		0x00000000, /*DWORD   CheckSum; */
		0x0002, /*WORD    Subsystem; */
		0x0000, /*WORD    DllCharacteristics; */
		0x00100000, /*DWORD   SizeOfStackReserve; */
		0x00001000, /*DWORD   SizeOfStackCommit; */
		0x00100000, /*DWORD   SizeOfHeapReserve; */
		0x00001000, /*DWORD   SizeOfHeapCommit; */
		0x00000000, /*DWORD   LoaderFlags; */
		0x00000010, /*DWORD   NumberOfRvaAndSizes; */

		/* IMAGE_DATA_DIRECTORY DataDirectory[16]; */
		{{0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0},
		 {0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0}}
		}
    };

    struct PE文件头S PE文件头 = PE文件头_模板;

    int i;
    struct PE文件S pf = {0};
    DWORD file_offset;
    struct 节信息S *si;
    IMAGE_SECTION_HEADER *psh;
    虚拟机ST *s1 = pe->s1;

    pf.op = fopen(pe->输出的文件名称, "wb");
    if (NULL == pf.op) {
        错误_不中止("could not write '%s': %s", pe->输出的文件名称, strerror(errno));
        return -1;
    }
    pe->sizeofheaders = pe_文件_对齐(pe,sizeof (struct PE文件头S) + pe->节_数量 * sizeof (IMAGE_SECTION_HEADER));
    file_offset = pe->sizeofheaders;
    if (2 == pe->s1->显示详情)
        printf("-------------------------------\n  virt   文件   大小  section" "\n");
    for (i = 0; i < pe->节_数量; ++i) {
        DWORD addr, 大小;
        const char *sh_name;

        si = pe->节_信息[i];
        sh_name = si->name;
        addr = si->sh_addr - pe->优先装载地址;
        大小 = si->sh_size;
        psh = &si->ish;

        if (2 == pe->s1->显示详情)
            printf("%6x %6x %6x  %s\n",
                (unsigned)addr, (unsigned)file_offset, (unsigned)大小, sh_name);

        switch (si->cls)
        {
            case sec_text:
                if (!PE文件头.opthdr.BaseOfCode)
                    PE文件头.opthdr.BaseOfCode = addr;
                break;
            case sec_data:
#ifndef 目标_X86_64
                if (!PE文件头.opthdr.BaseOfData)
                    PE文件头.opthdr.BaseOfData = addr;
#endif
                break;
            case sec_bss:
                break;
            case sec_reloc:
                pe_设置_数据目录(&PE文件头, IMAGE_DIRECTORY_ENTRY_BASERELOC, addr, 大小);
                break;

            case sec_rsrc:
                pe_设置_数据目录(&PE文件头, IMAGE_DIRECTORY_ENTRY_RESOURCE, addr, 大小);
                break;
            case sec_pdata:
                pe_设置_数据目录(&PE文件头, IMAGE_DIRECTORY_ENTRY_EXCEPTION, addr, 大小);
                break;
        }

        if (pe->imp_size)
        {
            pe_设置_数据目录(&PE文件头, IMAGE_DIRECTORY_ENTRY_IMPORT,
                pe->imp_offs, pe->imp_size);
            pe_设置_数据目录(&PE文件头, IMAGE_DIRECTORY_ENTRY_IAT,
                pe->iat_offs, pe->iat_size);
        }
        if (pe->exp_size) {
            pe_设置_数据目录(&PE文件头, IMAGE_DIRECTORY_ENTRY_EXPORT,
                pe->exp_offs, pe->exp_size);
        }

        memcpy(psh->Name, sh_name, 返回小值(strlen(sh_name), sizeof psh->Name));

        psh->Characteristics = si->pe_flags;
        psh->VirtualAddress = addr;
        psh->Misc.VirtualSize = 大小;
        PE文件头.opthdr.SizeOfImage =
            返回大值(pe_虚拟_对齐(pe, 大小 + addr), PE文件头.opthdr.SizeOfImage);

        if (si->data_size) {
            psh->PointerToRawData = file_offset;
            file_offset = pe_文件_对齐(pe, file_offset + si->data_size);
            psh->SizeOfRawData = file_offset - psh->PointerToRawData;
            if (si->cls == sec_text)
                PE文件头.opthdr.SizeOfCode += psh->SizeOfRawData;
            else
                PE文件头.opthdr.SizeOfInitializedData += psh->SizeOfRawData;
        }
    }
    PE文件头.filehdr.NumberOfSections = pe->节_数量;
    PE文件头.opthdr.AddressOfEntryPoint = pe->第一条指令的相对虚拟地址;
    PE文件头.opthdr.SizeOfHeaders = pe->sizeofheaders;
    PE文件头.opthdr.ImageBase = pe->优先装载地址;
    PE文件头.opthdr.Subsystem = pe->子系统;
    if (pe->s1->PE栈大小)
        PE文件头.opthdr.SizeOfStackReserve = pe->s1->PE栈大小;
    if (PE_DLL == pe->类型)
        PE文件头.filehdr.Characteristics = CHARACTERISTICS_DLL;
    PE文件头.filehdr.Characteristics |= pe->s1->PE特性;

    pe_写文件(&PE文件头, sizeof PE文件头, &pf);
    for (i = 0; i < pe->节_数量; ++i)
        pe_写文件(&pe->节_信息[i]->ish, sizeof(IMAGE_SECTION_HEADER), &pf);

    file_offset = pe->sizeofheaders;
    for (i = 0; i < pe->节_数量; ++i) {
        节ST *s;
        si = pe->节_信息[i];
        if (!si->data_size)
            continue;
        for (s = si->sec; s; s = s->上个) {
            pe_fpad(&pf, file_offset);
            pe_写文件(s->数据, s->当前数据_偏移量, &pf);
            if (s->上个)
                file_offset += s->上个->sh_addr - s->sh_addr;
        }
        file_offset = si->ish.PointerToRawData + si->ish.SizeOfRawData;
        pe_fpad(&pf, file_offset);
    }

    pf.sum += file_offset;
    fseek(pf.op, offsetof(struct PE文件头S, opthdr.CheckSum), SEEK_SET);
    pe_写文件(&pf.sum, sizeof (DWORD), &pf);

    fclose (pf.op);
#ifndef _WIN32
    chmod(pe->文件名, 0777);
#endif

    if (2 == pe->s1->显示详情)
        printf("-------------------------------\n");
    if (pe->s1->显示详情)
        printf("<- %s (%u bytes)\n", pe->输出的文件名称, (unsigned)file_offset);

    return 0;
}

/*----------------------------------------------------------------------------*/

static struct 导入符号ST *pe_添加_导入(struct PE信息S *pe, int 符号_索引)
{
    int i;
    int dll_index;
    struct PE导入信息S *p;
    struct 导入符号ST *s;
    ElfW(Sym) *isym;

    isym = (ElfW(Sym) *)pe->s1->临时动态符号表_节->数据 + 符号_索引;
    dll_index = isym->st_size;

    i = 动态数组_关联 ((void**)pe->imp_info, pe->imp_count, dll_index);
    if (-1 != i) {
        p = pe->imp_info[i];
        goto found_dll;
    }
    p = 初始化内存(sizeof *p);
    p->dll_index = dll_index;
    动态数组_追加元素(&pe->imp_info, &pe->imp_count, p);

found_dll:
    i = 动态数组_关联 ((void**)p->symbols, p->sym_count, 符号_索引);
    if (-1 != i)
        return p->symbols[i];

    s = 初始化内存(sizeof *s);
    动态数组_追加元素(&p->symbols, &p->sym_count, s);
    s->符号_索引 = 符号_索引;
    return s;
}

void pe释放导入(struct PE信息S *pe)
{
    int i;
    for (i = 0; i < pe->imp_count; ++i) {
        struct PE导入信息S *p = pe->imp_info[i];
        动态数组_重置(&p->symbols, &p->sym_count);
    }
    动态数组_重置(&pe->imp_info, &pe->imp_count);
}

/*----------------------------------------------------------------------------*/
static void pe创建导入(struct PE信息S *pe)
{
    int thk_ptr, ent_ptr, dll_ptr, sym_cnt, i;
    DWORD rva_base = pe->thunk->sh_addr - pe->优先装载地址;
    int ndlls = pe->imp_count;
    虚拟机ST *s1 = pe->s1;

    for (sym_cnt = i = 0; i < ndlls; ++i)
        sym_cnt += pe->imp_info[i]->sym_count;

    if (0 == sym_cnt)
        return;

    pe_对齐节(pe->thunk, 16);
    pe->imp_size = (ndlls + 1) * sizeof(IMAGE_IMPORT_DESCRIPTOR);
    pe->iat_size = (sym_cnt + ndlls) * sizeof(ADDR3264);
    dll_ptr = pe->thunk->当前数据_偏移量;
    thk_ptr = dll_ptr + pe->imp_size;
    ent_ptr = thk_ptr + pe->iat_size;
    pe->imp_offs = dll_ptr + rva_base;
    pe->iat_offs = thk_ptr + rva_base;
    节_预留指定大小内存(pe->thunk, pe->imp_size + 2*pe->iat_size);

    for (i = 0; i < pe->imp_count; ++i) {
        IMAGE_IMPORT_DESCRIPTOR *hdr;
        int k, n, dllindex;
        ADDR3264 v;
        struct PE导入信息S *p = pe->imp_info[i];
        const char *name;
        DLL引用ST *dllref;

        dllindex = p->dll_index;
        if (dllindex)
            name = 取文件名含扩展名((dllref = pe->s1->加载的DLL数组[dllindex-1])->name);
        else
            name = "", dllref = NULL;

        /* put the dll name into the import header */
        v = 放置elf字符串(pe->thunk, name);
        hdr = (IMAGE_IMPORT_DESCRIPTOR*)(pe->thunk->数据 + dll_ptr);
        hdr->FirstThunk = thk_ptr + rva_base;
        hdr->OriginalFirstThunk = ent_ptr + rva_base;
        hdr->Name = v + rva_base;

        for (k = 0, n = p->sym_count; k <= n; ++k) {
            if (k < n) {
                int iat_index = p->symbols[k]->iat_index;
                int 符号_索引 = p->symbols[k]->符号_索引;
                ElfW(Sym) *imp_sym = (ElfW(Sym) *)pe->s1->临时动态符号表_节->数据 + 符号_索引;
                ElfW(Sym) *org_sym = (ElfW(Sym) *)节符号表_数组->数据 + iat_index;
                const char *name = (char*)pe->s1->临时动态符号表_节->link->数据 + imp_sym->st_name;
                int ordinal;

                org_sym->st_value = thk_ptr;
                org_sym->st_shndx = pe->thunk->ELF节数量;

                if (dllref)
                    v = 0, ordinal = imp_sym->st_value; /* ordinal from pe_加载_def */
                else
                    ordinal = 0, v = imp_sym->st_value; /* address from zhi_添加_符号() */

#ifdef 运行脚本M
                if (pe->类型 == PE_RUN) {
                    if (dllref) {
                        if ( !dllref->handle )
                            dllref->handle = LoadLibrary(dllref->name);
                        v = (ADDR3264)GetProcAddress(dllref->handle, ordinal?(char*)0+ordinal:name);
                    }
                    if (!v)
                        错误_不中止("无法解析符号 '%s'", name);
                } else
#endif
                if (ordinal) {
                    v = ordinal | (ADDR3264)1 << (sizeof(ADDR3264)*8 - 1);
                } else {
                    v = pe->thunk->当前数据_偏移量 + rva_base;
                    节_预留指定大小内存(pe->thunk, sizeof(WORD)); /* hint, not used */
                    放置elf字符串(pe->thunk, name);
                }

            } else {
                v = 0; /* last entry is zero */
            }

            *(ADDR3264*)(pe->thunk->数据+thk_ptr) =
            *(ADDR3264*)(pe->thunk->数据+ent_ptr) = v;
            thk_ptr += sizeof (ADDR3264);
            ent_ptr += sizeof (ADDR3264);
        }
        dll_ptr += sizeof(IMAGE_IMPORT_DESCRIPTOR);
    }
}

/* ------------------------------------------------------------- */

struct pe_sort_sym
{
    int index;
    const char *name;
};

static int sym_cmp(const void *va, const void *vb)
{
    const char *ca = (*(struct pe_sort_sym**)va)->name;
    const char *cb = (*(struct pe_sort_sym**)vb)->name;
    return strcmp(ca, cb);
}

static void pe_创建_导出(struct PE信息S *pe)
{
    ElfW(Sym) *sym;
    int 符号_索引, sym_end;
    DWORD rva_base, base_o, func_o, name_o, ord_o, str_o;
    IMAGE_EXPORT_DIRECTORY *hdr;
    int sym_count, ord;
    struct pe_sort_sym **sorted, *p;
    虚拟机ST *s1 = pe->s1;

    FILE *op;
    char buf[260];
    const char *dllname;
    const char *name;

    rva_base = pe->thunk->sh_addr - pe->优先装载地址;
    sym_count = 0, sorted = NULL, op = NULL;

    sym_end = 节符号表_数组->当前数据_偏移量 / sizeof(ElfW(Sym));
    for (符号_索引 = 1; 符号_索引 < sym_end; ++符号_索引) {
        sym = (ElfW(Sym)*)节符号表_数组->数据 + 符号_索引;
        name = pe_导出_名称(pe->s1, sym);
        if (sym->st_other & ST_PE_EXPORT) {
            p = zhi_分配内存(sizeof *p);
            p->index = 符号_索引;
            p->name = name;
            动态数组_追加元素(&sorted, &sym_count, p);
        }
    }

    if (0 == sym_count)
        return;

    qsort (sorted, sym_count, sizeof *sorted, sym_cmp);

    pe_对齐节(pe->thunk, 16);
    dllname = 取文件名含扩展名(pe->输出的文件名称);

    base_o = pe->thunk->当前数据_偏移量;
    func_o = base_o + sizeof(IMAGE_EXPORT_DIRECTORY);
    name_o = func_o + sym_count * sizeof (DWORD);
    ord_o = name_o + sym_count * sizeof (DWORD);
    str_o = ord_o + sym_count * sizeof(WORD);

    hdr = 节_预留指定大小内存(pe->thunk, str_o - base_o);
    hdr->Characteristics        = 0;
    hdr->Base                   = 1;
    hdr->NumberOfFunctions      = sym_count;
    hdr->NumberOfNames          = sym_count;
    hdr->AddressOfFunctions     = func_o + rva_base;
    hdr->AddressOfNames         = name_o + rva_base;
    hdr->AddressOfNameOrdinals  = ord_o + rva_base;
    hdr->Name                   = str_o + rva_base;
    放置elf字符串(pe->thunk, dllname);

#if 1
    /* 自动将导出写入 <output-文件名>.def */
    截取前n个字符(buf, sizeof buf, pe->输出的文件名称);
    strcpy(取文件扩展名(buf), ".def");
    op = fopen(buf, "wb");
    if (NULL == op) {
        错误_不中止("无法创建 '%s': %s", buf, strerror(errno));
    } else {
        fprintf(op, "LIBRARY %s\n\nEXPORTS\n", dllname);
        if (pe->s1->显示详情)
            printf("<- %s (%d symbol%s)\n", buf, sym_count, &"s"[sym_count < 2]);
    }
#endif

    for (ord = 0; ord < sym_count; ++ord)
    {
        p = sorted[ord], 符号_索引 = p->index, name = p->name;
        /* 稍后在重定位_所有节()中插入实际地址 */
        elf重定位(节符号表_数组, pe->thunk,
            func_o, R_XXX_RELATIVE, 符号_索引);
        *(DWORD*)(pe->thunk->数据 + name_o)
            = pe->thunk->当前数据_偏移量 + rva_base;
        *(WORD*)(pe->thunk->数据 + ord_o)
            = ord;
        放置elf字符串(pe->thunk, name);
        func_o += sizeof (DWORD);
        name_o += sizeof (DWORD);
        ord_o += sizeof (WORD);
        if (op)
            fprintf(op, "%s\n", name);
    }

    pe->exp_offs = base_o + rva_base;
    pe->exp_size = pe->thunk->当前数据_偏移量 - base_o;
    动态数组_重置(&sorted, &sym_count);
    if (op)
        fclose(op);
}

/* ------------------------------------------------------------- */
static void pe_创建_重定位 (struct PE信息S *pe)
{
    DWORD offset, block_ptr, sh_addr, addr;
    int count, i;
    ElfW_Rel *rel, *rel_end;
    节ST *s = NULL, *sr;
    struct PE重定位头S *hdr;

    sh_addr = offset = block_ptr = count = i = 0;
    rel = rel_end = NULL;

    for(;;) {
        if (rel < rel_end) {
            int 类型 = ELFW(R_TYPE)(rel->r_info);
            addr = rel->r_offset + sh_addr;
            ++ rel;
            if (类型 != REL_TYPE_DIRECT)
                continue;
            if (count == 0) { /* new 块 */
                block_ptr = pe->重定位->当前数据_偏移量;
                节_预留指定大小内存(pe->重定位, sizeof(struct PE重定位头S));
                offset = addr & 0xFFFFFFFF<<12;
            }
            if ((addr -= offset)  < (1<<12)) { /* one 块 spans 4k addresses */
                WORD *wp = 节_预留指定大小内存(pe->重定位, sizeof (WORD));
                *wp = addr | PE_IMAGE_REL<<12;
                ++count;
                continue;
            }
            -- rel;

        } else if (s) {
            sr = s->重定位;
            if (sr) {
                rel = (ElfW_Rel *)sr->数据;
                rel_end = (ElfW_Rel *)(sr->数据 + sr->当前数据_偏移量);
                sh_addr = s->sh_addr;
            }
            s = s->上个;
            continue;

        } else if (i < pe->节_数量) {
            s = pe->节_信息[i]->sec, ++i;
            continue;

        } else if (!count)
            break;

        /* fill the last 块 and ready for a new one */
        if (count & 1) /* 对齐 for DWORDS */
            节_预留指定大小内存(pe->重定位, sizeof(WORD)), ++count;
        hdr = (struct PE重定位头S *)(pe->重定位->数据 + block_ptr);
        hdr -> offset = offset - pe->优先装载地址;
        hdr -> 大小 = count * sizeof(WORD) + sizeof(struct PE重定位头S);
        count = 0;
    }
}

/* ------------------------------------------------------------- */
static int pe_section_class(节ST *s)
{
    int 类型, flags;
    const char *name;

    类型 = s->sh_type;
    flags = s->sh_flags;
    name = s->name;
    if (flags & SHF_执行时占用内存) {
        if (类型 == SHT_程序数据) {
            if (flags & SHF_可执行)
                return sec_text;
            if (flags & SHF_可写)
                return sec_data;
            if (0 == strcmp(name, ".rsrc"))
                return sec_rsrc;
            if (0 == strcmp(name, ".iedat"))
                return sec_idata;
            if (0 == strcmp(name, ".pdata"))
                return sec_pdata;
            return sec_rdata;
        } else if (类型 == SHT_无数据) {
            if (flags & SHF_可写)
                return sec_bss;
        }
    } else {
        if (0 == strcmp(name, ".重定位"))
            return sec_reloc;
    }
    if (0 == memcmp(name, ".stab", 5))
        return name[5] ? sec_stabstr : sec_stab;
    if (flags & SHF_执行时占用内存)
        return sec_other;
    return -1;
}

static int 计算节区的RVA地址 (struct PE信息S *pe)
{
    int i, k, o, c;
    DWORD addr;
    int *section_order;
    struct 节信息S *si;
    节ST *s;
    虚拟机ST *s1 = pe->s1;

    if (PE_DLL == pe->类型)
        pe->重定位 = 节_新建(pe->s1, ".重定位", SHT_程序数据, 0);
    section_order = zhi_分配内存(pe->s1->节的数量 * sizeof (int));
    for (o = k = 0 ; k < sec_last; ++k)
    {
        for (i = 1; i < s1->节的数量; ++i)
        {
            s = s1->节数组[i];
            if (k == pe_section_class(s))
                section_order[o++] = i;
        }
    }
    si = NULL;
    addr = pe->优先装载地址 + 1;
    for (i = 0; i < o; ++i)
    {
        k = section_order[i];
        s = s1->节数组[k];
        c = pe_section_class(s);
        if ((c == sec_stab || c == sec_stabstr) && 0 == s1->做_调试)
            continue;
        if (PE_合并_数据 && c == sec_bss)
            c = sec_data;
        if (si && c == si->cls)
        {
            /* 与上一节合并*/
            s->sh_addr = addr = ((addr - 1) | (16 - 1)) + 1;
        } else {
            si = NULL;
            s->sh_addr = addr = pe_虚拟_对齐(pe, addr);
        }

        if (NULL == pe->thunk
            && c == (数据_节 == 只读数据_节 ? sec_data : sec_rdata))
            pe->thunk = s;

        if (s == pe->thunk)
        {
            pe创建导入(pe);
            pe_创建_导出(pe);
        }
        if (s == pe->重定位)
            pe_创建_重定位 (pe);

        if (0 == s->当前数据_偏移量)
            continue;

        if (si)
            goto add_section;

        si = 初始化内存(sizeof *si);
        动态数组_追加元素(&pe->节_信息, &pe->节_数量, si);

        strcpy(si->name, s->name);
        si->cls = c;
        si->sh_addr = addr;

        si->pe_flags = IMAGE_SCN_MEM_READ;
        if (s->sh_flags & SHF_可执行)
            si->pe_flags |= IMAGE_SCN_MEM_EXECUTE | IMAGE_SCN_CNT_CODE;
        else if (s->sh_type == SHT_无数据)
            si->pe_flags |= IMAGE_SCN_CNT_UNINITIALIZED_DATA;
        else
            si->pe_flags |= IMAGE_SCN_CNT_INITIALIZED_DATA;
        if (s->sh_flags & SHF_可写)
            si->pe_flags |= IMAGE_SCN_MEM_WRITE;
        if (0 == (s->sh_flags & SHF_执行时占用内存))
            si->pe_flags |= IMAGE_SCN_MEM_DISCARDABLE;

add_section:
        addr += s->当前数据_偏移量;
        si->sh_size = addr - si->sh_addr;
        if (s->sh_type != SHT_无数据) {
            节ST **ps = &si->sec;
            while (*ps)
                ps = &(*ps)->上个;
            *ps = s, s->上个 = NULL;
            si->data_size = si->sh_size;
        }
    }
    zhi_释放(section_order);
    return 0;
}

/*----------------------------------------------------------------------------*/

static int pe_是一个函数(虚拟机ST *s1, int 符号_索引)
{
    节ST *sr = 代码_节->重定位;
    ElfW_Rel *rel, *rel_end;
    ElfW(Addr)info = ELFW(R_INFO)(符号_索引, R_XXX_FUNCCALL);
#ifdef R_XXX_FUNCCALL2
    ElfW(Addr)info2 = ELFW(R_INFO)(符号_索引, R_XXX_FUNCCALL2);
#endif
    if (!sr)
        return 0;
    rel_end = (ElfW_Rel *)(sr->数据 + sr->当前数据_偏移量);
    for (rel = (ElfW_Rel *)sr->数据; rel < rel_end; rel++) {
        if (rel->r_info == info)
            return 1;
#ifdef R_XXX_FUNCCALL2
        if (rel->r_info == info2)
            return 1;
#endif
    }
    return 0;
}

/*----------------------------------------------------------------------------*/
/*
 * 功能：查到符号返回0，未找到符号则返回-1*/
static int pe_检查符号表(struct PE信息S *pe)
{
    ElfW(Sym) *sym;
    int 符号_索引, sym_end;
    int ret = 0;
    虚拟机ST *s1 = pe->s1;
    pe_对齐节(代码_节, 8);
    sym_end = 节符号表_数组->当前数据_偏移量 / sizeof(ElfW(Sym));
    for (符号_索引 = 1; 符号_索引 < sym_end; ++符号_索引)
    {
        sym = (ElfW(Sym) *)节符号表_数组->数据 + 符号_索引;
        if (sym->st_shndx == SHN_未定义)
        {
            const char *name = (char*)节符号表_数组->link->数据 + sym->st_name;
            unsigned 类型 = ELFW(ST_TYPE)(sym->st_info);
            int imp_sym = pe_查找_导入(pe->s1, sym);
            struct 导入符号ST *is;
            if (imp_sym <= 0)
                goto not_found;
            if (类型 == STT_NOTYPE)
            {
                if (pe_是一个函数(s1, 符号_索引))
                    类型 = STT_FUNC;
                else
                    类型 = STT_OBJECT;
            }

            is = pe_添加_导入(pe, imp_sym);
            if (类型 == STT_FUNC)
            {
                unsigned long offset = is->thk_offset;
                if (offset)
                {
                } else
                {
                    char 缓冲[100];
                    unsigned char *p;
                    /* 添加一个辅助符号，稍后会在pe_创建_导入中修补 */
                    sprintf(缓冲, "IAT.%s", name);
                    is->iat_index = 取符号索引(节符号表_数组, 0, sizeof(DWORD),ELFW(ST_INFO)(STB_GLOBAL, STT_OBJECT),0, SHN_未定义, 缓冲);
                    offset = 代码_节->当前数据_偏移量;
                    is->thk_offset = offset;

                    p = 节_预留指定大小内存(代码_节, 8);
                    write16le(p, 0x25FF);
#ifdef 目标_X86_64
                    write32le(p + 2, (DWORD)-4);
#endif
                    elf重定位(节符号表_数组, 代码_节,offset + 2, R_XXX_THUNKFIX, is->iat_index);

                }
                /* zhi_重新分配 might have altered sym's address */
                sym = (ElfW(Sym) *)节符号表_数组->数据 + 符号_索引;

                /* patch the original symbol */
                sym->st_value = offset;
                sym->st_shndx = 代码_节->ELF节数量;
                sym->st_other &= ~ST_PE_EXPORT; /* do not export */
                continue;
            }

            if (类型 == STT_OBJECT)
            { /* 数据, ptr to that should be */
                if (0 == is->iat_index)
                {
                    /* original symbol will be patched later in pe创建导入 */
                    is->iat_index = 符号_索引;
                    continue;
                }
            }

        not_found:
            if (ELFW(ST_BIND)(sym->st_info) == STB_WEAK)
                /* 接受 STB_WEAK 未定义符号 */
                continue;
            错误_不中止("未定义的符号 '%s'%s", name,imp_sym < 0 ? ", 缺失 __declspec(dllimport)?":"");
            ret = -1;

        } else if (pe->s1->导出所有符号
                   && ELFW(ST_BIND)(sym->st_info) != STB_LOCAL) {
            /* 如果 - 导出所有符号选项，则导出所有非局部符号 */
            sym->st_other |= ST_PE_EXPORT;
        }
    }
    return ret;
}

/* ------------------------------------------------------------- */
/* 加载/存储的辅助函数以插入一个更多的间接 */

#if defined 目标_I386 || defined 目标_X86_64
静态函数 栈值ST *pe_getimport(栈值ST *sv, 栈值ST *v2)
{
    int r2;
    if ((sv->寄存qi & (存储类型_掩码|存储类型_符号)) != (存储类型_VC常量|存储类型_符号) || (sv->r2 != 存储类型_VC常量))
        return sv;
    if (!sv->sym->a.dllimport)
        return sv;
    // printf("import %04x %04x %04x %s\n", sv->类型.数据类型, sv->sym->类型.数据类型, sv->寄存qi, 取单词字符串(sv->sym->v, NULL));
    memset(v2, 0, sizeof *v2);
    v2->类型.数据类型 = 数据类型_指针;
    v2->寄存qi = 存储类型_VC常量 | 存储类型_符号 | 存储类型_左值;
    v2->sym = sv->sym;

    r2 = get_reg(RC_INT);
    load(r2, v2);
    v2->寄存qi = r2;
    if ((uint32_t)sv->c.i) {
        值压入栈值(v2);
        整数常量压入栈(sv->c.i);
        gen_opi('+');
        *v2 = *栈顶值--;
    }
    v2->类型.数据类型 = sv->类型.数据类型;
    v2->寄存qi |= sv->寄存qi & 存储类型_左值;
    return v2;
}
#endif

静态函数 int pe_putimport(虚拟机ST *s1, int dllindex, const char *name, addr_t value)
{
    return 设置elf符号(
        s1->临时动态符号表_节,
        value,
        dllindex, /* st_size */
        ELFW(ST_INFO)(STB_GLOBAL, STT_NOTYPE),
        0,
        value ? SHN_ABS : SHN_未定义,
        name
        );
}

static int pe_添加_dll引用(虚拟机ST *s1, const char *dllname)
{
    int i;
    for (i = 0; i < s1->加载的DLL数量; ++i)
        if (0 == strcmp(s1->加载的DLL数组[i]->name, dllname))
            return i + 1;
    添加DLL引用(s1, dllname);
    return s1->加载的DLL数量;
}
/*功能：返回读取文件的字节数；
 * 文件描述：打开的文件流；
 * offset:文件偏移字节数
 * 缓冲:开始的缓冲区
 * 长度:指定的读取长度
 * */
static int 读取_文件字节数(int 文件描述, unsigned offset, void *缓冲, unsigned 长度)
{
    lseek(文件描述, offset, SEEK_SET);
    return 长度 == read(文件描述, 缓冲, 长度);
}

/* ------------------------------------------------------------- */

static int 获取_dll导出(int 文件描述, char **pp)
{
    int l, i, n, n0, ret;
    char *p;

    IMAGE_SECTION_HEADER ish;
    IMAGE_EXPORT_DIRECTORY ied;
    IMAGE_DOS_HEADER dh;
    IMAGE_FILE_HEADER ih;
    DWORD sig, 引用符号, addr, ptr, namep;

    int pef_hdroffset, opt_hdroffset, sec_hdroffset;

    n = n0 = 0;
    p = NULL;
    ret = 1;
    if (!读取_文件字节数(文件描述, 0, &dh, sizeof dh))
        goto 结_束;
    if (!读取_文件字节数(文件描述, dh.e_lfanew, &sig, sizeof sig))
        goto 结_束;
    if (sig != 0x00004550)
        goto 结_束;
    pef_hdroffset = dh.e_lfanew + sizeof sig;
    if (!读取_文件字节数(文件描述, pef_hdroffset, &ih, sizeof ih))
        goto 结_束;
    opt_hdroffset = pef_hdroffset + sizeof ih;
    if (ih.Machine == 0x014C) {
        IMAGE_OPTIONAL_HEADER32 oh;
        sec_hdroffset = opt_hdroffset + sizeof oh;
        if (!读取_文件字节数(文件描述, opt_hdroffset, &oh, sizeof oh))
            goto 结_束;
        if (IMAGE_DIRECTORY_ENTRY_EXPORT >= oh.NumberOfRvaAndSizes)
            goto the_end_0;
        addr = oh.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress;
    } else if (ih.Machine == 0x8664) {
        IMAGE_OPTIONAL_HEADER64 oh;
        sec_hdroffset = opt_hdroffset + sizeof oh;
        if (!读取_文件字节数(文件描述, opt_hdroffset, &oh, sizeof oh))
            goto 结_束;
        if (IMAGE_DIRECTORY_ENTRY_EXPORT >= oh.NumberOfRvaAndSizes)
            goto the_end_0;
        addr = oh.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress;
    } else
        goto 结_束;

    //printf("addr: %08x\n", addr);
    for (i = 0; i < ih.NumberOfSections; ++i) {
        if (!读取_文件字节数(文件描述, sec_hdroffset + i * sizeof ish, &ish, sizeof ish))
            goto 结_束;
        //printf("vaddr: %08x\n", ish.VirtualAddress);
        if (addr >= ish.VirtualAddress && addr < ish.VirtualAddress + ish.SizeOfRawData)
            goto found;
    }
    goto the_end_0;

found:
    引用符号 = ish.VirtualAddress - ish.PointerToRawData;
    if (!读取_文件字节数(文件描述, addr - 引用符号, &ied, sizeof ied))
        goto 结_束;

    namep = ied.AddressOfNames - 引用符号;
    for (i = 0; i < ied.NumberOfNames; ++i) {
        if (!读取_文件字节数(文件描述, namep, &ptr, sizeof ptr))
            goto 结_束;
        namep += sizeof ptr;
        for (l = 0;;) {
            if (n+1 >= n0)
                p = zhi_重新分配(p, n0 = n0 ? n0 * 2 : 256);
            if (!读取_文件字节数(文件描述, ptr - 引用符号 + l++, p + n, 1)) {
                zhi_释放(p), p = NULL;
                goto 结_束;
            }
            if (p[n++] == 0)
                break;
        }
    }
    if (p)
        p[n] = 0;
the_end_0:
    ret = 0;
结_束:
    *pp = p;
    return ret;
}
static char *修剪前(char *p)
{
    while ((unsigned char)*p <= ' ' && *p && *p != '\n')
	++p;
    return p;
}

static char *获取_token(char **s, char *f)
{
    char *p = *s, *e;
    p = e = 修剪前(p);
    while ((unsigned char)*e > ' ')
        ++e;
    *s = 修剪前(e);
    *f = **s; *e = 0;
    return p;
}
/* -------------------------------------------------------------
 *  这适用于由“windres.exe -O coff ...”生成的“coff”格式的已编译Windows资源。
 */

static int pe_加载_资源(虚拟机ST *s1, int 文件描述)
{
    struct PE资源头S hdr;
    节ST *rsrc_section;
    int i, ret = -1, 符号_索引;
    BYTE *ptr;
    unsigned offs;

    if (!读取_文件字节数(文件描述, 0, &hdr, sizeof hdr))
        goto quit;

    if (hdr.filehdr.Machine != IMAGE_FILE_MACHINE
        || hdr.filehdr.NumberOfSections != 1
        || strcmp((char*)hdr.sectionhdr.Name, ".rsrc") != 0)
        goto quit;

    rsrc_section = 节_新建(s1, ".rsrc", SHT_程序数据, SHF_执行时占用内存);
    ptr = 节_预留指定大小内存(rsrc_section, hdr.sectionhdr.SizeOfRawData);
    offs = hdr.sectionhdr.PointerToRawData;
    if (!读取_文件字节数(文件描述, offs, ptr, hdr.sectionhdr.SizeOfRawData))
        goto quit;
    offs = hdr.sectionhdr.PointerToRelocations;
    符号_索引 = 取符号索引(节符号表_数组, 0, 0, 0, 0, rsrc_section->ELF节数量, ".rsrc");
    for (i = 0; i < hdr.sectionhdr.NumberOfRelocations; ++i) {
        struct PE资源重定位S rel;
        if (!读取_文件字节数(文件描述, offs, &rel, sizeof rel))
            goto quit;
        // printf("rsrc_reloc: %x %x %x\n", rel.offset, rel.大小, rel.类型);
        if (rel.类型 != RSRC_RELTYPE)
            goto quit;
        elf重定位(节符号表_数组, rsrc_section,
            rel.offset, R_XXX_RELATIVE, 符号_索引);
        offs += sizeof rel;
    }
    ret = 0;
quit:
    return ret;
}

/* ------------------------------------------------------------- */


static int pe_加载_def(虚拟机ST *s1, int 文件描述)
{
    int state = 0, ret = -1, dllindex = 0, ord;
    char dllname[80], *buf, *line, *p, *x, next;

    buf = zhi_load_text(文件描述);
    for (line = buf;; ++line)  {
        p = 获取_token(&line, &next);
        if (!(*p && *p != 英_分号))
            goto 跳过;
        switch (state) {
        case 0:
            if (0 != stricmp(p, "LIBRARY") || next == '\n')
                goto quit;
            截取前n个字符(dllname, sizeof dllname, 获取_token(&line, &next));
            ++state;
            break;
        case 1:
            if (0 != stricmp(p, "EXPORTS"))
                goto quit;
            ++state;
            break;
        case 2:
            dllindex = pe_添加_dll引用(s1, dllname);
            ++state;
            /* fall through */
        default:
            /* get ordinal and will store in sym->st_value */
            ord = 0;
            if (next == '@') {
                x = 获取_token(&line, &next);
                ord = (int)strtol(x + 1, &x, 10);
            }
            //printf("token %s ; %s : %d\n", dllname, p, ord);
            pe_putimport(s1, dllindex, p, ord);
            break;
        }
跳过:
        while ((unsigned char)next > ' ')
            获取_token(&line, &next);
        if (next != '\n')
            break;
    }
    ret = 0;
quit:
    zhi_释放(buf);
    return ret;
}

/* ------------------------------------------------------------- */

static int pe_加载_dll(虚拟机ST *s1, int 文件描述, const char *文件名)
{
    char *p, *q;
    int index, ret;

    ret = 获取_dll导出(文件描述, &p);
    if (ret) {
        return -1;
    } else if (p) {
        index = pe_添加_dll引用(s1, 文件名);
        for (q = p; *q; q += 1 + strlen(q))
            pe_putimport(s1, index, q, 0);
        zhi_释放(p);
    }
    return 0;
}

静态函数 int pe_加载_文件(虚拟机ST *s1, int 文件描述, const char *文件名)
{
    int ret = -1;
    char buf[10];
    if (0 == strcmp(取文件扩展名(文件名), ".def"))
        ret = pe_加载_def(s1, 文件描述);
    else if (pe_加载_资源(s1, 文件描述) == 0)
        ret = 0;
    else if (读取_文件字节数(文件描述, 0, buf, 4) && 0 == memcmp(buf, "MZ", 2))
        ret = pe_加载_dll(s1, 文件描述, 文件名);
    return ret;
}

公用函数 int zhi_获取_dll导出(const char *文件名, char **pp)
{
    int ret, 文件描述 = open(文件名, O_RDONLY | O_BINARY);
    if (文件描述 < 0)
        return -1;
    ret = 获取_dll导出(文件描述, pp);
    close(文件描述);
    return ret;
}

/* ------------------------------------------------------------- */
#ifdef 目标_X86_64
static unsigned pe_添加_展开_信息(虚拟机ST *s1)
{
    if (NULL == s1->uw_pdata) {
        s1->uw_pdata = 查找_节_创建(s1, ".pdata", 1);
        s1->uw_pdata->sh_地址对齐 = 4;
    }
    if (0 == s1->uw_sym)
        s1->uw_sym = 取符号索引(节符号表_数组, 0, 0, 0, 0, 代码_节->ELF节数量, ".uw_base");
    if (0 == s1->uw_offs) {
        /* As our functions all have the same stackframe, we use one entry for all */
        static const unsigned char uw_info[] = {
            0x01, // UBYTE: 3 Version , UBYTE: 5 Flags
            0x04, // UBYTE Size of prolog
            0x02, // UBYTE Count of unwind codes
            0x05, // UBYTE: 4 Frame Register (rbp), UBYTE: 4 Frame Register offset (scaled)
            // USHORT * n Unwind codes array
            // 0x0b, 0x01, 0xff, 0xff, // stack 大小
            0x04, 0x03, // set frame ptr (mov rsp -> rbp)
            0x01, 0x50  // push reg (rbp)
        };

        节ST *s = 代码_节;
        unsigned char *p;

        节_预留指定大小内存(s, -s->当前数据_偏移量 & 3); /* 对齐 */
        s1->uw_offs = s->当前数据_偏移量;
        p = 节_预留指定大小内存(s, sizeof uw_info);
        memcpy(p, uw_info, sizeof uw_info);
    }

    return s1->uw_offs;
}

静态函数 void pe_添加_展开_数据(unsigned start, unsigned end, unsigned stack)
{
    虚拟机ST *s1 = 全局虚拟机;
    节ST *pd;
    unsigned o, n, d;
    struct /* _RUNTIME_FUNCTION */ {
      DWORD BeginAddress;
      DWORD EndAddress;
      DWORD UnwindData;
    } *p;

    d = pe_添加_展开_信息(s1);
    pd = s1->uw_pdata;
    o = pd->当前数据_偏移量;
    p = 节_预留指定大小内存(pd, sizeof *p);

    /* record this function */
    p->BeginAddress = start;
    p->EndAddress = end;
    p->UnwindData = d;

    /* put relocations on it */
    for (n = o + sizeof *p; o < n; o += sizeof p->BeginAddress)
        elf重定位(节符号表_数组, pd, o, R_XXX_RELATIVE, s1->uw_sym);
}
#endif
/* ------------------------------------------------------------- */
#ifdef 目标_X86_64
#define PE标准符号(n,s) n
#else
#define PE标准符号(n,s) "_" n s
#endif

static void 添加支持(虚拟机ST *s1, const char *文件名)
{
    if (zhi_添加_dll(s1, 文件名, 0) < 0)
        错误_不中止("%s not found", 文件名);
}
/*功能：计算程序入口点，获取PE文件入口符号地址和PE文件类型
 *pe:PE信息存储结构指针
 * */
static void 计算程序入口点(虚拟机ST *s1, struct PE信息S *pe)
{
    const char *入口符号;
    int PE文件类型;

    if (输出_DLL == s1->输出类型)
    {
        PE文件类型 = PE_DLL;
        入口符号 = PE标准符号("__dllstart","@12");
    } else
    {
        const char *运行符号;
        if (查找elf符号(节符号表_数组, PE标准符号("WinMain","@16")))   /*是WINDOWS的GUI程序*/
        {
            入口符号 = "__winstart";
            运行符号 = "__runwinmain";
            PE文件类型 = PE_GUI;
        } else if (查找elf符号(节符号表_数组, PE标准符号("wWinMain","@16")))   /*是WINDOWS的GUI程序*/
        {
            入口符号 = "__wwinstart";
            运行符号 = "__runwwinmain";
            PE文件类型 = PE_GUI;
        } else if (查找elf符号(节符号表_数组, "wmain"))  /*是WINDOWS的控制台程序（32BIT）或DOS程序（16BIT）。wmain也是main的另一个别名,是为了支持二个字节的语言环境。*/
        {
            入口符号 = "__wstart";
            运行符号 = "__runwmain";
            PE文件类型 = PE_EXE;
        } else
        {
            入口符号 =  "__start";
            运行符号 = "__runmain";
            PE文件类型 = PE_EXE;

        }
        if (输出_内存 == s1->输出类型)
        {
        	入口符号 = 运行符号;
        }

    }
    pe->入口符号 = 入口符号 + 1;
    if (!s1->前导下划线 || strchr(入口符号, '@')) /*在“入口符号”中第一次出现'@'符号的位置*/
    {
    	++入口符号;
    }

#ifdef 启用内置堆栈回溯M
    if (s1->执行_回溯)
    {
#ifdef 启用边界检查M
        if (s1->使用_边界_检查器 && s1->输出类型 != 输出_DLL)
            添加支持(s1, "bcheck.o");
#endif
        if (s1->输出类型 == 输出_EXE)
        {
        	添加支持(s1, "bt-exe.o");
        }

        if (s1->输出类型 == 输出_DLL)
        {
        	添加支持(s1, "bt-dll.o");
        }

        if (s1->输出类型 != 输出_DLL)
        {
        	添加支持(s1, "bt-log.o");
        }

        if (s1->输出类型 != 输出_内存)
        {
        	zhi_add_btstub(s1);
        }

    }
#endif

    /* 从 运行时库.a 中获取启动代码 */
#ifdef 运行脚本M
    if (输出_内存 != s1->输出类型 || s1->运行时_主函数)
#endif
    设置全局符号(s1, 入口符号, NULL, 0);

    if (0 == s1->不添加标准库)
    {
        static const char * const 库组[] =
        {
            "msvcrt",
			"kernel32",
			"",
			"user32",
			"gdi32",
			NULL
        };
        const char * const *pp, *p;
        if (运行时库M[0])
        {
        	添加支持(s1, 运行时库M);
        }

        for (pp = 库组; 0 != (p = *pp); ++pp)
        {
            if (*p)
            {
            	添加库_有错误提示(s1, p);
            }else if (PE_DLL != PE文件类型 && PE_GUI != PE文件类型)
            {
            	break;
            }

        }
    }
    if (输出_DLL == s1->输出类型)
    {
    	s1->输出类型 = 输出_EXE;
    }
    if (输出_内存 == s1->输出类型)
    {
    	PE文件类型 = PE_RUN;
    }
    pe->类型 = PE文件类型;
}

static void 设置PE优先装载地址和子系统(虚拟机ST * s1, struct PE信息S *pe)
{
    if (PE_DLL == pe->类型)
    {
        pe->优先装载地址 = 0x10000000;
    } else
    {
#if defined(目标_ARM)
        pe->优先装载地址 = 0x00010000;
#else
        pe->优先装载地址 = 0x00400000;
#endif
    }

#if defined(目标_ARM)
    pe->子系统 = 9;
#else
    if (PE_DLL == pe->类型 || PE_GUI == pe->类型)
    {
    	pe->子系统 = 2;
    }else
    {
    	pe->子系统 = 3;
    }

#endif
    if (s1->PE_子系统 != 0)
    {
    	pe->子系统 = s1->PE_子系统;
    }
    if (pe->子系统 == 1)
    {
        pe->节_对齐 = 0x20;
        pe->文件_对齐 = 0x20;
    } else
    {
        pe->节_对齐 = 0x1000;
        pe->文件_对齐 = 0x200;
    }

    if (s1->节_对齐 != 0)
        pe->节_对齐 = s1->节_对齐;
    if (s1->pe_文件_对齐 != 0)
        pe->文件_对齐 = s1->pe_文件_对齐;

    if ((pe->子系统 >= 10) && (pe->子系统 <= 12))
        pe->优先装载地址 = 0;

    if (s1->has_代码节地址)
        pe->优先装载地址 = s1->代码节地址;
}

静态函数 int 生成PE文件(虚拟机ST *s1, const char *输出文件名)
{
    int 返回值;
    struct PE信息S pe;

    memset(&pe, 0, sizeof pe);
    pe.输出的文件名称 = 输出文件名;
    pe.s1 = s1;
    s1->源文件类型 = 0;

#ifdef 启用边界检查M
    添加_边界检查(s1);
#endif
    加载静态链接库(s1);
    计算程序入口点(s1, &pe);
    解析_公共_符号(s1);
    设置PE优先装载地址和子系统(s1, &pe);
    返回值 = pe_检查符号表(&pe);
    if (返回值)
    {
    	;
    } else if (输出文件名)
    {
        计算节区的RVA地址(&pe);
        重定位符号地址(s1, s1->符号表节, 0);
        s1->pe_优先装载地址 = pe.优先装载地址;
        重定位_所有节(s1);
        pe.第一条指令的相对虚拟地址 = (DWORD)(获取符号地址(s1, pe.入口符号, 1, 1) - pe.优先装载地址);
        if (s1->错误_数量)
        {
        	返回值 = -1;
        }else
        {
        	返回值 = pe_写(&pe);/*写出EXE文件*/
        }
        动态数组_重置(&pe.节_信息, &pe.节_数量);
    } else
    {
#ifdef 运行脚本M
        pe.thunk = 数据_节;
        pe创建导入(&pe);
        s1->运行时_主函数 = pe.入口符号;
#ifdef 目标_X86_64
        s1->uw_pdata = 查找_节_创建(s1, ".pdata", 1);
#endif
#endif
    }

    pe释放导入(&pe);
    return 返回值;
}

