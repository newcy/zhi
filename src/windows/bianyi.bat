@rem ------------------------------------------------------
@rem 自举完成构建
@rem ------------------------------------------------------

@echo off                                                                            %关闭回显%
setlocal                                                                             %临时改变系统变量,只限于本批处理文件% %endlocal结束临时系统变量的改变%
if (%1)==(-clean) goto :清理                                                         
set CC=zhi
set /p VERSION= < ..\版本设置                                                                                                                                                       %set /p设置一个外部读取的变量，也可以做输出用例，设置变量：set /p str=输入str的值，可以是数字也可以是字符%
set INST=
set DOC=no
set EXES_ONLY=no
goto :a0
:a2
shift
:a3
shift
:a0
if not (%1)==(-c) goto :a1
set CC=%~2
if (%2)==(cl) set CC=@call :cl
goto :a2
:a1
if (%1)==(-t) set T=%2&& goto :a2
if (%1)==(-v) set VERSION=%~2&& goto :a2
if (%1)==(-i) set INST=%2&& goto :a2
if (%1)==(-d) set DOC=yes&& goto :a3
if (%1)==(-x) set EXES_ONLY=yes&& goto :a3
if (%1)==() goto :p1
:用法
echo 用法: bianyi.bat [ 选项 ... ]
echo 选项:
echo   -c prog              使用 prog (gcc/zhi/cl) 编译 zhi
echo   -c "prog 选项"        使用带有选项的 prog 来编译 zhi
echo   -t 32/64             强制 32/64 位默认目标
echo   -v "version"         设置zhi版本
echo   -i zhidir            将zhi安装到zhidir
echo   -d                   也创建zhi-文档.html（需要makeinfo）
echo   -x                   只需创建可执行文件
echo   -clean               删除所有以前生成的文件和目录
exit /B 1

@rem ------------------------------------------------------
@rem 子程序

:清理
set LOG=echo
%LOG% 清除文件:
for %%f in (*zhi.exe 核心.dll 库\*.a) do call :删除_文件 %%f
for %%f in (..\配置.h ..\config.texi) do call :删除_文件 %%f
for %%f in (include\*.h) do @if exist ..\%%f call :删除_文件 %%f
for %%f in (include\简库.h 例子\core_test.c) do call :删除_文件 %%f
for %%f in (库\*.o *.o *.obj *.def *.pdb *.库 *.exp *.ilk) do call :删除_文件 %%f            
%LOG% 清除目录:
for %%f in (文档 核心库) do call :删除_目录 %%f
%LOG% 完成.
exit /B 0
:删除_文件
if exist %1 del %1 && %LOG%   %1
exit /B 0
:删除_目录
if exist %1 rmdir /Q/S %1 && %LOG%   %1
exit /B 0

:cl
@echo off
set CMD=cl
:c0
set ARG=%1
set ARG=%ARG:.dll=.库%
if (%1)==(-shared) set ARG=-LD
if (%1)==(-o) shift && set ARG=-Fe%2
set CMD=%CMD% %ARG%
shift
if not (%1)==() goto :c0
echo on
%CMD% -O2 -W2 -Zi -MT -GS- -nologo %DEF_GITHASH% -link -opt:ref,icf
@exit /B %ERRORLEVEL%

@rem ------------------------------------------------------
@rem 主程序

:p1
if not %T%_==_ goto :p2
set T=32
if %PROCESSOR_ARCHITECTURE%_==AMD64_ set T=64
if %PROCESSOR_ARCHITEW6432%_==AMD64_ set T=64
:p2
if "%CC:~-3%"=="gcc" set CC=%CC% -O2 -s -static %DEF_GITHASH%
set D32=-D目标_PE -D目标_I386
set D64=-D目标_PE -D目标_X86_64
set P32=i386-win32
set P64=x86_64-win32
if %T%==64 goto :t64
set D=%D32%
set DX=%D64%
set PX=%P64%
set TX=64
goto :p3
:t64
set D=%D64%
set DX=%D32%
set PX=%P32%
set TX=32
goto :p3

:p3
@echo on

:配置.h
echo>..\配置.h #定义 版本号 "%VERSION%"
echo>> ..\配置.h #如果已定义 目标_X86_64
echo>> ..\配置.h #定义 运行时库M "运行时库-64.a"
echo>> ..\配置.h #否则
echo>> ..\配置.h #定义 运行时库M "运行时库-32.a"
echo>> ..\配置.h #结束如果

for %%f in (*zhi.exe *zhi.dll) do @del %%f

@if _%ZHI_C%_==__ goto 编译器2部分
@rem 如果定义了 ZHI_C 则只构建 zhi.exe
%CC% -o zhi.exe %ZHI_C% %D%
@goto :编译器_完成

:编译器2部分
@if _%core_C%_==__ set core_C=..\核心.c                                         
%CC% -o 核心.dll -shared %core_C% %D% -D核心作为动态库M                                      
@if errorlevel 1 goto :结_束
%CC% -o zhi.exe ..\zhi.c 核心.dll %D% -D是源码M"=0"                                       
%CC% -o %PX%-zhi.exe ..\zhi.c %DX%                                                     
:编译器_完成
@if (%EXES_ONLY%)==(yes) goto :文件_完成

if not exist 核心库 mkdir 核心库                                                                                                                                                          
if not exist 文档 mkdir 文档
copy>nul ..\include\*.h include
copy>nul ..\简库.h include
copy>nul ..\核心.h 核心库
copy>nul ..\tests\core_test.c 例子
copy>nul zhi-win32.txt 文档

if exist 核心.dll .\zhi -impdef 核心.dll -o 核心库\核心.def
@if errorlevel 1 goto :结_束

:运行时库.a
call :生成库 %T%
@if errorlevel 1 goto :结_束
@if exist %PX%-zhi.exe call :生成库 %TX%
@if errorlevel 1 goto :结_束
.\zhi -m%T% -c ../库/bcheck.c -o 库/bcheck.o -g
.\zhi -m%T% -c ../库/bt-exe.c -o 库/bt-exe.o
.\zhi -m%T% -c ../库/bt-log.c -o 库/bt-log.o
.\zhi -m%T% -c ../库/bt-dll.c -o 库/bt-dll.o

:zhi-文档.html
@if not (%DOC%)==(yes) goto :文档-完成
echo>..\config.texi @set VERSION %VERSION%
cmd /c makeinfo --html --no-split ../zhi-文档.texi -o 文档/zhi-文档.html
:文档-完成

:文件_完成
for %%f in (*.o *.def) do @del %%f

:复制-安装
@if (%INST%)==() goto :结_束
if not exist %INST% mkdir %INST%
for %%f in (*zhi.exe *zhi.dll) do @copy>nul %%f %INST%\%%f
@if not exist %INST%\库 mkdir %INST%\库
for %%f in (库\*.a 库\*.o 库\*.def) do @copy>nul %%f %INST%\%%f
for %%f in (include 例子 核心库 文档) do @xcopy>nul /s/i/q/y %%f %INST%\%%f

:结_束
exit /B %ERRORLEVEL%

:生成库
.\zhi -m%1 -c ../库/运行时库.c
.\zhi -m%1 -c 库/crt1.c
.\zhi -m%1 -c 库/crt1w.c
.\zhi -m%1 -c 库/wincrt1.c
.\zhi -m%1 -c 库/wincrt1w.c
.\zhi -m%1 -c 库/dllcrt1.c
.\zhi -m%1 -c 库/dllmain.c
.\zhi -m%1 -c 库/chkstk.S
.\zhi -m%1 -c ../库/alloca.S
.\zhi -m%1 -c ../库/alloca-bt.S
.\zhi -m%1 -c ../库/stdatomic.c
.\zhi -m%1 -ar 库/运行时库-%1.a 运行时库.o crt1.o crt1w.o wincrt1.o wincrt1w.o dllcrt1.o dllmain.o chkstk.o alloca.o alloca-bt.o stdatomic.o
exit /B %ERRORLEVEL%

@goto bat文件教程块

bat文件教程：
0.echo   具有回显（打印到命令行）功能
1.@ 符号 有隐藏功能。
2.@echo 可以实现单行顶格注释。
3.:: 可以实现单行顶格注释（其实是:的隐藏注释，但是不建议直接使用：实现注释，这样容易与标签产生冲突）与@echo类似。
4.%% 可以实现单行顶格注释或单行行尾注释。
5.利用标签跳转，在永远不被执行到的块中实现多行注释
6.set xxx="中文编程"  定义变量xxx，并把其值设定为"中文编程"
7.set x  查询所有以x开头的变量
8.set xxx=   删除xxx这个变量
9.set /a sum = 0 设置整数变量sum的值为0，注意在整数变量前面的/a不能少。
10.echo sum is %sum%   回显sum的值
11.set /a sum = sum + 1  使sum自加1，然后值赋值给自己
12.echo sum is %sum%  回显sum的值
13.set sum =   删除变量sum
14.bianyi.bat zhi -run win.z  %0表示bianyi.bat，%1表示zhi，%2表示win.z 。％0 －％9表示命令行参数，%0表示bat文件名本身，％1－％9表示其后的参数

:bat文件教程块