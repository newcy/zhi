/*
 * 封装：
 * 继承：
 * 多态：重载，重写。
 *
 * 枚举，结构体，联合体，当成特殊类处理。
 * */
class Father
{
	private char 姓名;
	private int 年龄;
	private int 身高
	protected void say()
	{
		系统.打印("我是"+名字+"今年"+年龄+“岁”+“身高”+身高+“米”);
	}
}
class Son extends Father
{
	string 年级;
	private string 跳(int chang)
	{
		string 运动名称 = “跳远”;
		return 运动名称+“跳了”+chang+“米” ;
	}
}
class test
{
	public static void main()
	{
		Son 儿子 = new Son();
		儿子.姓名 = “夏明”;
		儿子.年龄 = 19;
		儿子.身高 = 1.8;
		儿子.say();
		系统.打印(儿子.跳());
	}
}
typedef struct 类S
{
	char* 类名;
	struct 类S 父类;
	void* 成员变量;
	void* 方法;
}类TS;

typedef struct 对象S
{
	类TS 对象所属的类;
	char* 对象名称;
}对象TS;

void 开始解析类体()
{
	外部声明解析();
}
void 解析类()
{
	取下个单词();
	if(token = "{" || token = "extends")
	{
		解析匿名类();
	}
	else{
		类的名称 = token;
	}

	取下个单词();
	if(token = "extends")
	{
		解析继承();
	}else if(token = "{")
	{
		开始解析类体();
	}
}
void 语法分析()
{
	if(token = "class")
	{
		解析类();
	}
}



